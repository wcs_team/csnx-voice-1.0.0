'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. Main                                   *
*    Overrides Module.............                                        *
*    Created by................... Vasu Kaveri                            *
*    First changes made by........ Vasu Kaveri                            *
*    Date created................. Circa 2010                             *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 17JUL15 |VA-0001 | SwiftC     | Additional regions message after the    *
*                               | get assignment message.                 *
* 28AUG15 |VA-0004 | SwiftC     | Condition the "debug" messages put in by*
*                               | developers.                             *
***************************************************************************
'''

import sys
import CustomCore.CoreTaskCustom #@UnusedImport

import CustomSelection.SelectionTaskCustom #@UnusedImport
import CustomCore.VehicleTaskCustom #@UnusedImport
import CustomSelection.SelectionVocabularyCustom #@UnusedImport
import CustomSelection.PickPromptSingleCustom #@UnusedImport
import CustomSelection.PickPromptMultipleCustom #@UnusedImport
#Begin VA-0001
import CustomSelection.GetAssignmentCustom #@UnusedImport
#End VA-0001

import common.VoiceLinkLut

from core.VoiceLink import voicelink_startup

#Begin VA-0004
import GlobalVariables
from voice import get_voice_application_property

if (get_voice_application_property('ShowDebugOutput') == None):
    GlobalVariables.show_debug_output = False
else:
    GlobalVariables.show_debug_output = (get_voice_application_property('ShowDebugOutput') in ("true"))
#End   VA-0004

#set override lut definitions
common.VoiceLinkLut.lut_def_files.append('LUTDefinitionCustom.properties')

def main():
#Begin VA-0004
    if GlobalVariables.show_debug_output:
        sys.stdout.write('customization main')
        sys.stdout.flush()
#End   VA-0004        
    
    voicelink_startup()
    
