'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. CoreTaskCustom                         *
*    Overrides Module............. CoreTask                               *
*    Created by................... Vasu Kaveri                            *
*    First changes made by........ Vasu Kaveri                            *
*    Date created................. Circa 2010                             *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 28AUG15 |VA-0003 | SwiftC     | Shift handover collection changes       *
* 28AUG15 |VA-0004 | SwiftC     | Condition the "debug" messages put in by*
*                               | developers.                             *
***************************************************************************
'''

import sys

from vocollect_core import obj_factory, itext
from core.CoreTask import CoreTask, ConfigurationLut, SignOffLut #@UnusedImport
from core.CoreTask import EXECUTE_FUNCTIONS, REQUEST_FUNCTIONS, REQUEST_VEHICLES, REQUEST_CONFIG, REQUEST_BREAKS, GET_WELCOME, REQUEST_SIGNON
from dialogs import prompt_digits, prompt_words, prompt_ready, prompt_only, prompt_list_lut_auth
from voice import getenv
from voice import globalwords as gw
from core.VehicleTask import VehicleTask #@UnusedImport
from common.VoiceLinkLut import VoiceLinkLut #@UnusedImport
from core.SharedConstants import CORE_TASK_NAME #@UnusedImport

#Tasks
from core.VehicleTask import VehicleTask #@Reimport
from core.TakeBreakTask import TakeBreakTask #@UnusedImport
from backstocking.BackStockingTask import BackstockingTask
from lineloading.LineLoadingTask import LineLoadingTask
from loading.LoadingTask import LoadingTask
from forkapps.PutawayTask import PutawayTask
from forkapps.ReplenishmentTask import ReplenishmentTask
from puttostore.PutToStoreTask import PutToStoreTask
from selection.SelectionTask import SelectionTask
from cyclecounting.CycleCountingTask import CycleCountingTask
from receiving.ReceivingTask import ReceivingTask

#Begin VA-0004
import GlobalVariables
#End   VA-0004

#Begin VA-0003
#Add a constant for the new state.
CHECK_HANDOVER = 'checkHandover'
#End   VA-0003

class CoreTask_CSnx(CoreTask):

    #----------------------------------------------------------
    # To override the __init__ method, you have to call the parent
    #  class' __init__ method and add whatever else you need after 
    #  the call... 
    def __init__(self, taskRunner = None, callingTask = None):
        super(CoreTask_CSnx, self).__init__(taskRunner, callingTask)

#Begin VA-0003
        self._check_handover_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCheckHandover')
#End   VA-0003

  
    def initializeStates(self):
#Begin VA-0004
#        sys.stdout.write('customization : initializeStates')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('customization : initializeStates')
            sys.stdout.flush()
#End   VA-0004
        self.addState(REQUEST_CONFIG,    self.request_configurations)
        self.addState(REQUEST_BREAKS,    self.request_breaks)
        self.addState(GET_WELCOME,       self.welcome)
        self.addState(REQUEST_SIGNON,    self.request_signon)
        self.addState(REQUEST_FUNCTIONS, self.request_functions)
#Begin VA-0003
        self.addState(CHECK_HANDOVER,    self.check_handover)
#End   VA-0003        
        self.addState(REQUEST_VEHICLES,  self.vehicles)
        self.addState(EXECUTE_FUNCTIONS, self.execute_function)
       
    def vehicles(self):
        ''' Run vehicle types task, return to request function '''
        #EXAMPLE: Launch task from another task using launch method
        #         when calling this way, user must set next state
        #         before calling launch, otherwise after launched task
        #         completes the state is repeated (execute_function is 
        #         example of state being repeated)
#Begin VA-0004
#        sys.stdout.write('customization : vehicles')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('customization : vehicles')
            sys.stdout.flush()
#End   VA-0004
        
        self.launch(obj_factory.get(VehicleTask, self.taskRunner))

    #----------------------------------------------------------
    def request_functions(self):
        ''' request function from host '''
        
#Begin VA-0004        
#        sys.stdout.write('customization : request_function')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('customization : request_function')
            sys.stdout.flush()
#End   VA-0004
        self.sign_off_allowed = True
        if not self.xmit_functions():
            self.next_state = REQUEST_FUNCTIONS
        
        ''' prompt for function and execute it '''
        self.sign_off_allowed = True
        
        #if only 1 record returned then automatically select that function
        self.function = None
        if len(self._functions_lut) == 1:
            self.function = self._functions_lut[0]['number']
            prompt_only(self._functions_lut[0]['description'])
        #else prompt user to select function
        else:
            self.function = prompt_list_lut_auth(self._functions_lut, 
                                            'number', 'description',  
                                            itext('core.function.prompt'), 
                                            itext('core.function.help'))
        
        self.function = str(self.function)

#Begin VA-0003
# Somewhat redundant anyway..??        
##      self.next_state = REQUEST_VEHICLES
#End   VA-0003        
                
    #----------------------------------------------------------
    def execute_function(self):
#Begin VA-0004        
#        sys.stdout.write('customization : execute_function')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('customization : execute_function')
            sys.stdout.flush()
#End   VA-0004
        #Execute selected function
        if self.function == '1': #PutAway
            self.launch(obj_factory.get(PutawayTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '2': #Replenishment
            self.launch(obj_factory.get(ReplenishmentTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function in ['3', '4', '6']: #Selection
            self.launch(obj_factory.get(SelectionTask, self.function, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '7': #Line Loading
            self.launch(obj_factory.get(LineLoadingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '8': #Put to store
            self.launch(obj_factory.get(PutToStoreTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '9': #Cycle Counting
            self.launch(obj_factory.get(CycleCountingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '10': #Loading
            self.launch(obj_factory.get(LoadingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '11': #Back Stocking
            self.launch(obj_factory.get(BackstockingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '12': #Receiving
            self.launch(obj_factory.get(ReceivingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        else: #Function specified not implemented
            prompt_ready('function ' + str(self.function) + ' not implemented, say ready to try again')
            self.next_state = REQUEST_FUNCTIONS 
            
    #----------------------------------------------------------
    def xmit_functions(self):
        ''' request function from host '''
        #get list of valid functions
        if self._functions_lut.do_transmit(0) != 0:
            return False
        else:
            #check if any valid functions were received.
            count = 0
            for rec in self._functions_lut:
                if rec['number'] > 0:
                    count += 1
            
            #if no valid functions
            if count == 0:
                result = prompt_words(itext("core.noFunctions.prompt"), False, {'sign off':False} )
                if result == 'sign off':
                    self.sign_off()

        return True

        
    def request_signon(self):
        ''' Sign on process '''
#Begin VA-0004        
#        sys.stdout.write('customization : signon')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('customization : signon')
            sys.stdout.flush()
#End   VA-0004
        pass_word = prompt_digits(itext('core.password.prompt'), 
                                      itext('core.password.help'), 
                                      3, 10, 
                                      self._config_lut[0]['confirmPassword'] > 0)
        
        #check if configuration lut needs resent
#Begin VA-0004
#        sys.stdout.write('customization : signon'+getenv('Operator.Id', ''))
#        sys.stdout.flush()
#        sys.stdout.write('customization : signon'+getenv('Operator.Id', '1000001'))
#        sys.stdout.flush()
#        sys.stdout.write('customization : signon'+self._config_lut[0]['operatorId'])
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('customization : signon'+getenv('Operator.Id', ''))
            sys.stdout.flush()
            sys.stdout.write('customization : signon'+getenv('Operator.Id', '1000001'))
            sys.stdout.flush()
            sys.stdout.write('customization : signon'+self._config_lut[0]['operatorId'])
            sys.stdout.flush()
#End   VA-0004
        if self._config_lut[0]['operatorId'] != getenv('Operator.Id', ''):
            while not self._transmit_configuration():
                pass
            
        result = self._signon_lut.do_transmit(pass_word)        
        if result == 0 :
            self.password = pass_word
            gw.words['sign off'].enabled = True
            gw.words['take a break'].enabled = True
            # if get_voice_application_property('CollectVehicleInfo') != 'true':
            self.next_state = REQUEST_FUNCTIONS
        else:
            self.password = None
            self.next_state = REQUEST_SIGNON
            
#Begin VA-0003
    #----------------------------------------------------------
    def check_handover(self):
        ''' Send a message to the host to request if there are any handover
            assignments that are available for collection.  The host will 
            reply with 6 values: the number, location and location verification 
            of assignments that do not have equipment, and the number, location 
            and location verification of assignments that have equipment. '''
        
        if self._check_handover_lut.do_transmit() != 0:
            pass
        else:
            _handover_response = self._check_handover_lut[0]
            # You have to qualify the location of the global variable, otherwise,
            #  a new local variable will get created...  No value will be put into
            #  the global variable and none of the fields will be useful elsewhere.
            GlobalVariables.no_equip_count = _handover_response['noEquipCount']
            GlobalVariables.no_equip_loc = _handover_response['noEquipLoc']
            GlobalVariables.no_equip_loc_verf = _handover_response['noEquipLocVerf']
            GlobalVariables.with_equip_count = _handover_response['withEquipCount']
            GlobalVariables.with_equip_loc = _handover_response['withEquipLoc']
            GlobalVariables.with_equip_loc_verf = _handover_response['withEquipLocVerf']
            
        if GlobalVariables.with_equip_count > 0: 
            self.next_state = EXECUTE_FUNCTIONS

#End   VA-0003            
        
obj_factory.set_override(CoreTask, CoreTask_CSnx)