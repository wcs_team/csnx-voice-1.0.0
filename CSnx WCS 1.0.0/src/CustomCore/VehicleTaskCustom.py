'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. SelectionTaskCustom                    *
*    Overrides Module............. SelectionTask                          *
*    Created by................... Vasu Kaveri                            *
*    First changes made by........ Vasu Kaveri                            *
*    Date created................. Circa 2010                             *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 28AUG15 |VA-0003 | SwiftC     | Shift handover collection changes       *
* 28AUG15 |VA-0004 | SwiftC     | Condition the "debug" messages put in by*
*                               | developers.                             *
***************************************************************************
'''
from dialogs import prompt_digits, prompt_ready, prompt_only, prompt_yes_no, prompt_list_lut_auth
from vocollect_core import itext
from common.VoiceLinkLut import VoiceLinkLut #@UnusedImport
from core.VehicleTask import VehicleTask
from vocollect_core import obj_factory

import sys

#Begin VA-0004
import GlobalVariables
#End   VA-0004

#States - Vehicle Types
REQUEST_VEHTYPE      = "requestVehType"
REQUEST_VEHICLEID    = "requestVehicle"
REQUEST_XMIT_VEHID   = "xmitVehicleId"
NEXT_SAFETY_CHECK    = "nextSafetyCheck"
EXECUTE_SAFETYCHECK  = "executeSafetyCheck"
COMPLETE_SAFETYCHECK = "completeSafetyCheck"


class VehicleTask_CSnx(VehicleTask):
    
        
    def request_vehicle_id(self):
        ''' Prompt operator for Vehicle type and ID if needed '''
        
        #Prompt user for vehicle
        self.vehicle_type = ''
        self.vehicle_id = ''
        self.vehicle_code = ''
        self.vehicle_type = prompt_list_lut_auth(self._veh_types_lut, 
                                                 'type', 'description',  
                                                 itext("core.vehicleType.prompt"),
                                                 itext("core.vehicleType.help"))
        
        #get record of vehicle selected
        veh_rec = None
        for veh in self._veh_types_lut:
            if veh['type'] == int(self.vehicle_type):
                veh_rec = veh
                break
        
        if veh_rec is not None:    
            #Get ID if required
            self.vehicle_code = veh_rec['code']
            if veh_rec['captureId']:
                #TODOD: DYNAMIC - Needs A-Z
                self.vehicle_id = prompt_digits(itext("core.vehicleID.prompt"),
                                                itext("core.vehicleID.help"), 
                                                1, 10, 
                                                True)
#Begin VA-0003
                GlobalVariables.task_vehicle_id = self.vehicle_id
#End   VA-0003                 
        else:
            self.next_state = ''

    #----------------------------------------------------------
    def xmit_veh_id(self):
        #send type and id back to host
        
        result = self._veh_id_lut.do_transmit(self.vehicle_type, self.vehicle_id, self.vehicle_code)
        if result < 0:
            self.next_state = REQUEST_XMIT_VEHID
        elif result > 0:
            self.next_state = REQUEST_VEHICLEID
        else:
            #check if safety check is required
            self.next_state = ''
            if self._veh_id_lut[0]['safetyCheck'] != '':
                self._safety_check_iter = iter(self._veh_id_lut)
                prompt_only(itext("core.startSafetyCheck.prompt"))
                self.next_state = NEXT_SAFETY_CHECK
              
    def execute_safety_check(self):
        ''' Execute safety check list '''
#Begin VA-0004
#        sys.stdout.write('VehicleTask_EWMSCustom : execute_safety_check')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('VehicleTask_EWMSCustom : execute_safety_check')
            sys.stdout.flush()
#End VA-0004
        if self._curr_saftey_check['answerType'] == 'N':
            mtr_reading = prompt_digits(self._curr_saftey_check['safetyCheck'], 
                          self._curr_saftey_check['safetyCheck'], 
                          self._curr_saftey_check['answerLen'], 
                          self._curr_saftey_check['answerLen'])
            if int(mtr_reading) <= self._curr_saftey_check['treshold']:
                prompt_only(itext("core.safetyCheckAction3.prompt"))
             
            while self._veh_safety_lut.do_transmit(self._curr_saftey_check['questionId'], self._curr_saftey_check['answerType'], mtr_reading, self._curr_saftey_check['vehicleType'], self._curr_saftey_check['vehicleId']) != 0:
                                pass               
            #self._veh_meter_reading_lut.do_transmit(mtr_reading)
            self.next_state = NEXT_SAFETY_CHECK
        else: 
            if self._curr_saftey_check['answerType'] == 'B':
                if prompt_yes_no(self._curr_saftey_check['safetyCheck'], True):
                    self.next_state = NEXT_SAFETY_CHECK
                else:
                    #if failed, confirm
                    self.next_state = EXECUTE_SAFETYCHECK
                    if prompt_yes_no(itext("core.startSafetyCheckFail.Confirm",
                                            self._curr_saftey_check['safetyCheck']), True):
                        #if confirmed, ask if quick fix can be done
                        if prompt_yes_no(itext('core.safetyCheckFailed'), True):
                            #while self._veh_safety_lut.do_transmit(0, self._curr_saftey_check['safetyCheck'], 2) != 0:
                            #   pass                        
                            prompt_ready(itext("core.safetyQuickRepair.prompt"), True)
                        else:
                            while self._veh_safety_lut.do_transmit(self._curr_saftey_check['questionId'], self._curr_saftey_check['answerType'], 'No', self._curr_saftey_check['vehicleType'], self._curr_saftey_check['vehicleId']) != 0:
                                pass    
                            if self._curr_saftey_check['action'] == 2:                    
                                prompt_only(itext('core.safetyCheckAction2.prompt'))
                            elif self._curr_saftey_check['action'] == 3:                    
                                prompt_only(itext('core.safetyCheckAction3.prompt'))
                            elif self._curr_saftey_check['action'] == 4:                    
                                prompt_only(itext('core.safetyCheckAction4.prompt'))
                                
                            self.next_state = REQUEST_VEHTYPE
                            return
                  
    def complete_safety_check(self):
        ''' completed all safety checks so inform host '''
        
        #If here, then all checks passed or where fixed
                      
    
    
obj_factory.set_override(VehicleTask, VehicleTask_CSnx)