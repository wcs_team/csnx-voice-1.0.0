'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. GetAssignmentCustom                    *
*    Overrides Module............. GetAssignment                          *
*    Created by................... Chris Swift                            *
*    First changes made by........ Chris Swift                            *
*    Date created................. July 17, 2015                          *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 17JUL15 |VA-0001 | SwiftC     | Additional regions message after the    *
*                               | get assignment message.                 *
***************************************************************************
'''

#Imports  
from vocollect_core import obj_factory 
from common.VoiceLinkLut import VoiceLinkLut #@UnusedImport
from selection.GetAssignment import GetAssignmentBase, GetAssignmentAuto
from selection.SharedConstants import GET_ASSIGN_XMIT_ASSIGNMENT,\
    GET_ASSIGN_START
from vocollect_core.dialog.functions import prompt_ready
from vocollect_core.utilities.localization import itext
from selection.SelectionLuts import RegionConfig

#Class overrides
class GetAssignmentBase_CSnx(GetAssignmentBase):

    def xmit_assignments(self):
        ''' transmit get assignment '''
        
        work_ids = ''
        if self._region_config_rec['autoAssign'] == '1':
            work_ids = self._number_work_ids
            
        result = self._assignment_lut.do_transmit(work_ids, 
                                                  self._region_config_rec['assignmentType'])
        if result < 0:
            self.next_state = GET_ASSIGN_XMIT_ASSIGNMENT
        elif result == 2:
            self.next_state = ''
        elif result > 0:
            self.next_state = GET_ASSIGN_START
            
#Begin VA-0001
# If result is = 0, resend the region config message to get the 
#   values for the region being picked. The resend error should
#   not have any errors.  We'll ignore anyway...
        if result == 0:
            _resend_error = 0
            _resend_error = self._region_config_lut.do_transmit(self._selected_region, 
                                                   self.callingTask.function)
#End VA-001            
            
    #----------------------------------------------------------

#Class overrides
class GetAssignmentAuto_CSnx(GetAssignmentAuto):

#Begin VA-0001
#The __init__ method had to be overridden to include the definitions for the
#  regions LUTs.
#End VA-0001    
    def __init__(self, region_lut, assignment_lut, picks_lut, pick_only,
                 taskRunner = None, callingTask = None):
        super(GetAssignmentAuto, self).__init__(region_lut, 
                                                assignment_lut, 
                                                picks_lut, 
                                                pick_only,
                                                taskRunner, callingTask)
        #Set task name
        self.name = "getAssignmentAuto"
        
#Begin VA-0001
        self._region_config_lut = obj_factory.get(RegionConfig, 'prTaskLUTPickingRegion')
        self.function = callingTask.function #@UnusedVariable
        self._selected_region = None
#End VA-0001        

    #----------------------------------------------------------
    
    def xmit_assignments(self):
        ''' transmit assignment request '''
#Begin VA-0001        
# Had to override because the target method is, in itself, overridden.
# The invocation of the xmit_assignments needs to change to the overridden
#  class qualification.
##      GetAssignmentBase.xmit_assignments(self)
        GetAssignmentBase_CSnx.xmit_assignments(self)
#End VA-0001        

        if not self._inprogress_work and self.next_state is None:
            max_ids = self._region_config_rec['maxNumberWordID']
            assignments = self._assignment_lut.number_of_assignments()
            if max_ids > 1 and assignments < self._number_work_ids:
                if assignments == 1:
                    prompt_ready(itext('selection.only.available.single', assignments))
                else:
                    prompt_ready(itext('selection.only.available.plural', assignments))
                     
    #----------------------------------------------------------



#Object factory overrides
obj_factory.set_override(GetAssignmentBase, GetAssignmentBase_CSnx) 
obj_factory.set_override(GetAssignmentAuto, GetAssignmentAuto_CSnx)