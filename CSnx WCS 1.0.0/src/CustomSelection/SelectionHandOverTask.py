'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. SelectionHandOver                      *
*    Overrides Module............. None                                   *
*    Created by................... Chris Swift                            *
*    First changes made by........ Chris Swift                            *
*    Date created................. 09 Sep 2015                            *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 28AUG15 |VA-0003 | SwiftC     | Shift handover collection changes       *
***************************************************************************
'''
from vocollect_core.task.task import TaskBase
from vocollect_core.utilities.localization import itext
from vocollect_core.dialog.functions import prompt_ready, prompt_alpha_numeric
from vocollect_core.utilities import obj_factory
from core.VehicleTask import VehicleTask
from common.VoiceLinkLut import VoiceLinkLut
import GlobalVariables 

#States:
EQUIPMENT_STATUS   = 'equipmentStatus'
HANDOVER_LOCATION  = 'handoverLocation'
COLLECT_HANDOVER   = 'collectHandOver'        

class SelectionHandOverTask(TaskBase):
    '''
    New task to manage the Hand Over collection process.  Bear in mind that the
    user is here because there are handover assignments.  There may be no 
    assignments with equipment, or there may be no assignments without 
    equipment, but, there will be at least one assignment here. 
    '''

    #-------------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        super(SelectionHandOverTask, self).__init__(taskRunner, callingTask)
        
        #Localise the global variables
        self._task_vehicle_id = GlobalVariables.task_vehicle_id
        self._no_equip_count = GlobalVariables.no_equip_count
        self._no_equip_loc = GlobalVariables.no_equip_loc
        self._no_equip_loc_verf = GlobalVariables.no_equip_loc_verf
        self._with_equip_count = GlobalVariables.with_equip_count
        self._with_equip_loc = GlobalVariables.with_equip_loc
        self._with_equip_loc_verf = GlobalVariables.with_equip_loc_verf
        
        #Define new LUTs
        self._handover_assign_from_equip_lut = obj_factory.get(VoiceLinkLut, "prTaskLUTAssignmentFromEquipment")
        self._handover_assign_from_cntr_lut = obj_factory.get(VoiceLinkLut, "prTaskLUTAssignmentFromContainer")
        
    #--------------------------------------------------------------
    def initializeStates(self):
        ''' Initialise states '''
        
        self.addState(EQUIPMENT_STATUS, self.equipment_status)
        self.addState(HANDOVER_LOCATION, self.handover_location)
        self.addState(COLLECT_HANDOVER, self.collect_handover)
        
    #--------------------------------------------------------------
    def equipment_status(self):
        ''' 
        Determine from the global fields that pertain to handover information,
        where do they need to go within the handover area:
        
            - Does the user have an equipment ID associated to them
            - If yes
                - Are there any assignments with no equipment
                    - If yes, send the user to the no equipment location
                    - If no
                        - Send the user to drop off their current equipment
                        - Send the user to the with equipment location
            - If no
                - Are there any assignments with equipment
                    If yes, send the user to the with equipment location
                    If no
                        - Call VehicleTaskCustom
                        - Send the user to the no equipment location
        '''
        self._user_with_equip = False
        self._user_with_no_equip = False
        
        #Equipment associated...
        if self._task_vehicle_id != None: 
            if self._no_equip_count > 0:
                self._user_with_equip = True
            else:
                _drop_location = "the battery bay"
                _response = prompt_ready(itext("selection.handover.prompt.drop.equipment", 
                                              _drop_location))
                if _response == "ready":
                    self._user_with_no_equip = True
                    self._task_vehicle_id = None
                    GlobalVariables.task_vehicle_id = None
                else:
                    self.next_state = EQUIPMENT_STATUS
                            
        #No equipment associated
        else:
            if self._with_equip_count > 0:
                self._user_with_no_equip = True 
            else:
                self.launch(obj_factory.get(VehicleTask, self.taskRunner))
                if GlobalVariables.task_vehicle_id == None:
                    self.next_state = EQUIPMENT_STATUS
                else:    
                    self._user_with_equip = True
                    
    #--------------------------------------------------------------
    def handover_location(self):
        
        '''  
        Prompt the user to go to the respective handover area.  Once
        there, the verification code will need to be spoken.
        '''
                
        #Send the user to the respective handover location
        # - user with equipment to the assignments with no equipment
        # - user without equipment to the assignments with equipment
        if self._user_with_equip:
            _collection_location = self._no_equip_loc
        if self._user_with_no_equip:
            _collection_location = self._with_equip_loc
        
        _response = prompt_alpha_numeric(itext("selection.handover.prompt.collect.assignment", 
                                              _collection_location),
                                        itext("selection.handover.prompt.collect.assignment.help",
                                              _collection_location),
                                         1, 10, False)
        
        if ((self._user_with_equip and _response != self._no_equip_loc_verf)
            or (self._user_with_no_equip and _response != self._with_equip_loc_verf)):
            prompt_ready(itext("selection.handover.prompt.collect.wrong.location"))
            self.next_state = self.current_state
                                        
    
    #--------------------------------------------------------------
    def collect_handover(self):
        
        ''' 
        If the user has equipment, they must identify an assignment from
        the assignments with no equipment.
        If the user does not have equipment, they must identify an 
        assignment from the assignments with equipment. 
        '''
        
        if self._user_with_equip:
            self.collect_from_no_equipment()
        else:
            self.collect_from_with_equipment()

    #--------------------------------------------------------------
    def collect_from_with_equipment(self):
        
        '''  
        Identify an assignment from the equipment ID.
        '''
        
        _response = prompt_alpha_numeric(itext("selection.handover.prompt.collect.equipment.id"), 
                                         itext("selection.handover.prompt.collect.equipment.id.help"),
                                         1, 10, False)
        
        #Send the response off to the host to validate the equipment ID
        _result = self._handover_assign_from_equip_lut.do_transmit(_response)
        if _result == 0:
            GlobalVariables.handover_collected_assignemnt = self._handover_assign_from_equip_lut[0]["assignmentNumber"]
            self.next_state = ''
        elif _result < 0:
            self.next_state = self.current_state
        else:
            self.next_state = COLLECT_HANDOVER

    #--------------------------------------------------------------
    def collect_from_no_equipment(self): 
        
        ''' 
        Identify an assignment from any of the container ID's for an
        assignment.
        '''
        
        _response = prompt_alpha_numeric(itext("selection.handover.prompt.collect.container.id"), 
                                         itext("selection.handover.prompt.collect.container.id.help"),
                                         1, 15, False)
        
        #Send the response off to the host to validate the equipment ID
        _result = self._handover_assign_from_cntr_lut.do_transmit(_response)
        if _result == 0:
            GlobalVariables.handover_collected_assignemnt = self._handover_assign_from_cntr_lut[0]["assignmentNumber"]
            self.next_state = ''
        elif _result < 0:
            self.next_state = self.current_state
        else:
            self.next_state = COLLECT_HANDOVER

