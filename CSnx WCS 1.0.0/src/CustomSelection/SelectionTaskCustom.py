'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. SelectionTaskCustom                    *
*    Overrides Module............. SelectionTask                          *
*    Created by................... Vasu Kaveri                            *
*    First changes made by........ Vasu Kaveri                            *
*    Date created................. Circa 2010                             *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 28AUG15 |VA-0003 | SwiftC     | Shift handover collection changes       *
* 28AUG15 |VA-0004 | SwiftC     | Condition the "debug" messages put in by*
*                               | developers.                             *
***************************************************************************
'''

from vocollect_core import obj_factory
from common.VoiceLinkLut import VoiceLinkLut #@UnusedImport


from selection.SelectionLuts import IN_PROGRESS_ANOTHER_FUNCTION #@UnusedImport

import sys
from selection.SelectionTask import SelectionRegionTask, SelectionTask #@UnusedImport
from common.RegionSelectionTasks import MultipleRegionTask

from selection.SharedConstants import REGIONS, VALIDATE_REGION,\
    GET_ASSIGNMENT, CHECK_ASSIGNMENT, BEGIN_ASSIGNMENT,\
    PICK_ASSIGNMENT, PICK_REMAINING, PRINT_LABEL, DELIVER_ASSIGNMENT,\
    COMPLETE_ASSIGNMENT, PROMPT_ASSIGNMENT_COMPLETE
   
#Begin VA-0004
import GlobalVariables
#End   VA-0004
#Begin VA-0003
from CustomSelection.SelectionHandOverTask import SelectionHandOverTask
#End   VA-0003

#States - Selection
## Comment out these constants as they now come from SharedConstants...
#REGIONS             = "regions"
#VALIDATE_REGION     = "validateRegion"
#GET_ASSIGNMENT      = "getAssignment"
#CHECK_ASSIGNMENT    = "checkAssignment"
#BEGIN_ASSIGNMENT    = "beginAssignment"
#PICK_ASSIGNMENT     = "pickAssignment"
#PICK_REMAINING      = "pickRemianing"
#PRINT_LABEL         = "printLabel"
#DELIVER_ASSIGNMENT  = "deliverAssignment"
#COMPLETE_ASSIGNMENT = "completeAssignment"
#PROMPT_ASSIGNMENT_COMPLETE = "promptAssignmentComplete"
#Begin VA-0003
GET_HANDOVER         = "getHandover"
#End   VA-0003

class SelectionTask_CSnx(SelectionTask):

    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        #get region states
        self.addState(REGIONS, self.regions)
        self.addState(VALIDATE_REGION, self.validate_regions)
#Begin VA-0003
        self.addState(GET_HANDOVER, self.get_handover)
#End   VA-0003        
        self.addState(GET_ASSIGNMENT, self.get_assignment)
        self.addState(CHECK_ASSIGNMENT, self.check_assignment)
        self.addState(BEGIN_ASSIGNMENT, self.begin_assignment)
        self.addState(PICK_ASSIGNMENT, self.pick_assignment)
        self.addState(PICK_REMAINING, self.pick_remaining)
        self.addState(PRINT_LABEL, self.print_label)
        self.addState(DELIVER_ASSIGNMENT, self.deliver_assignment)
        self.addState(COMPLETE_ASSIGNMENT, self.complete_assignment)
        self.addState(PROMPT_ASSIGNMENT_COMPLETE, self.prompt_assignment_complete)
        
#Begin VA-0003
    def get_handover(self):
        ''' If there are any handover assignments to collect, call a new
            module/taskrunner function to manage the whole process.  Once
            collected, return here to move on to getting the assignment. '''
        
        if (GlobalVariables.no_equip_count != 0
            or GlobalVariables.with_equip_count != 0):
            self.launch(obj_factory.get(SelectionHandOverTask,
                                        self.taskRunner))
#End   VA-0003 

class SelectionRegionTask_CSnx(SelectionRegionTask):
    
    def request_regions(self):
        ''' prompt operator for a region '''
        
#Begin VA-0004
#        sys.stdout.write('SelectionRegionTask_EWMSCustom : request_regions')
#        sys.stdout.flush()
        
#        sys.stdout.write('SelectionRegionTask_EWMSCustom End : request_regions')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:        
            sys.stdout.write('SelectionRegionTask_EWMSCustom : request_regions')
            sys.stdout.flush()

        
        
            sys.stdout.write('SelectionRegionTask_EWMSCustom End : request_regions')
            sys.stdout.flush()
#End   VA-0004

class MultipleRegionTask_CSnx(MultipleRegionTask):
    
    def request_regions(self):
        ''' prompt operator for a region '''

#Begin VA-0004        
#        sys.stdout.write('MultipleRegionTask_EWMSCustom : request_regions')
#        sys.stdout.flush()
        
        
#        sys.stdout.write('MultipleRegionTask_EWMSCustom End : request_regions')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('MultipleRegionTask_EWMSCustom : request_regions')
            sys.stdout.flush()
            
            
            sys.stdout.write('MultipleRegionTask_EWMSCustom End : request_regions')
            sys.stdout.flush()
#End   VA-0004        

obj_factory.set_override(SelectionTask, SelectionTask_CSnx)
obj_factory.set_override(SelectionRegionTask, SelectionRegionTask_CSnx)
obj_factory.set_override(MultipleRegionTask, MultipleRegionTask_CSnx)