'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. SelectionVocabularyCustom              *
*    Overrides Module............. SelectionVocabulary                    *
*    Created by................... Vasu Kaveri                            *
*    First changes made by........ Vasu Kaveri                            *
*    Date created................. Circa 2010                             *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 18AUG15 |VA-0002 | SwiftC     | - Allow the user to say the destination *
*                               |   container with the quantity (1 alpha).*
*                               | - Allow the user to request the size of *
*                               |   the item being picked.                *
* 28AUG15 |VA-0004 | SwiftC     | Condition the "debug" messages put in by*
*                               | developers.                             *
***************************************************************************
'''

import sys
from dialogs import prompt_only, prompt_yes_no, prompt_ready, prompt_digits #@UnusedImport
from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary #@UnusedImport
from common.VoiceLinkLut import VoiceLinkLutOdr #@UnusedImport
from vocollect_core import itext, obj_factory
from selection.NewContainer import NewContainer #@UnusedImport
from selection.CloseContainer import CloseContainer #@UnusedImport
from selection.SelectionPrint import SelectionPrintTask #@UnusedImport
from selection.PickPrompt import LOT_TRACKING #@UnusedImport
from selection import PickPrompt
from selection.SelectionVocabulary import SelectionVocabulary
from selection.ReviewContents import ReviewContents #@UnusedImport

#Begin VA-0004
import GlobalVariables
#End   VA-0004

class SelectionVocabulary_CSnx(SelectionVocabulary):
    ''' Definition of additional vocabulary used throughout VoiceLink selection
    
    Parameters
            runner - Task runner object
    '''
    
    def __init__(self, runner):
        
#Begin VA-0004        
#        sys.stdout.write('SelectionVocabulary_EWMSCustom : __init__')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('SelectionVocabulary_EWMSCustom : __init__')
            sys.stdout.flush()
#End   VA-0004
        
        self.vocabs = {'UPC':              Vocabulary('UPC', self._upc, False),
                       'how much more':    Vocabulary('how much more', self._how_much_more, False),
                       'store number':     Vocabulary('store number', self._store_number, False),
                       'route number':     Vocabulary('route number', self._route_number, False),
                       'item number':      Vocabulary('item number', self._item_number, False),
                       'description':      Vocabulary('description', self._description, False),
#Begin VA-0002
                       'size':             Vocabulary('size', self._size, False),
#End   VA-0002
                       'location':         Vocabulary('location', self._location, False),
                       'quantity':         Vocabulary('quantity', self._quantity, False),
                       'repeat last pick': Vocabulary('repeat_last_pick', self._repeat_last_pick, False),
                       'repick skips':     Vocabulary('repick skips', self._repick_skips, False),
                       'new container':    Vocabulary('new container', self._new_container, False),
                       'close container':  Vocabulary('close container', self._close_container, False),
                       'reprint labels':   Vocabulary('reprint_labels', self._reprint_labels, False),
                       'review contents':  Vocabulary('review_contents', self._review_contents, False),
                       'review cluster':   Vocabulary('review_cluster', self._review_cluster, False),
                       'pass assignment':  Vocabulary('pass assignment', self._pass_assignment, False),
                       'count down':       Vocabulary('count down', self._count_down, False)
                       }

        self.runner = runner

        #previous pick information
        self.previous_picks = []
        self.pick_tasks = []
        self.clear()

        self._pass_inprogress = False
        
                            
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''
        current_task = self.runner.get_current_task().name
        
        #available while picking
        if vocab == 'store number':
            return len(self.current_picks) > 0
        
        elif vocab == 'route number':
            return len(self.current_picks) > 0

        elif vocab == 'how much more':
            return len(self.current_picks) > 0

        elif vocab == 'repeat last pick':
            return len(self.current_picks) > 0 or len(self.previous_picks) 
        
        elif vocab == 'new container':
            return len(self.current_picks) > 0
                                
        elif vocab == 'close container':
            #operator not allowed to close container after entering a quantity
            after_qty_prompt = False
            pick_prompt = self.runner.findTask('pickPrompt')
            if pick_prompt is not None:
                after_qty_prompt = (pick_prompt.current_state not in [PickPrompt.SLOT_VERIFICATION,
                                                                     PickPrompt.CASE_LABEL_CD,
                                                                     PickPrompt.ENTER_QTY,
                                                                     PickPrompt.QUANTITY_VERIFICATION])

            return (len(self.current_picks) > 0 
                    and self.region_config_rec['containerType'] != 0
                    and not after_qty_prompt)
        
        elif vocab == 'reprint labels':
            return len(self.current_picks) > 0
               
        elif vocab == 'repick skips':
            return len(self.current_picks) > 0 
        
        elif vocab == 'review cluster':
            return len(self.current_picks) > 0 

        elif vocab == 'review contents':
            return len(self.current_picks) > 0 
                            
        elif vocab == 'UPC':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)
        
        elif vocab == 'item number':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)

        elif vocab == 'description':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)

#Begin VA-0002
#Add "size" to the vocabulary in voiceconfig.xml
        elif vocab == 'size':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)
#End   VA-0002

        elif vocab == 'location':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)
        
        elif vocab == 'quantity':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)
        
        elif vocab == 'pass assignment':
            return len(self.current_picks) > 0 
        
        elif vocab == 'count down':
            return len(self.current_picks) > 0 

        return False
    
        
    #Functions to execute words? 

    
    def _count_down(self):
        ''' speak count down '''
#Begin VA-0004
#        sys.stdout.write('SelectionVocabulary_EWMSCustom : _count_down')
#        sys.stdout.flush()
        if main.show_debug_output:
            sys.stdout.write('SelectionVocabulary_EWMSCustom : _count_down')
            sys.stdout.flush()
#End   VA-0004
        
        prompt_only(itext('generic.not.available', self.word.vocab))
           
        #self._picked_quantity = pickQuantity
        #self.next_state=LOT_TRACKING
        return True 
    
#Begin VA-0002
    def _size(self):
        ''' speak size information '''
        if self.current_picks[0]['size'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_picks[0]['size'].lower())
        return True 

#End   VA-0002    
        
obj_factory.set_override(SelectionVocabulary, SelectionVocabulary_CSnx)