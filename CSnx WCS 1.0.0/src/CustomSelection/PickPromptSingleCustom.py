'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. PickPromptSingleCustom                 *
*    Overrides Module............. PickPromptSingle                       *
*    Created by................... Vasu Kaveri                            *
*    First changes made by........ Vasu Kaveri                            *
*    Date created................. Circa 2010                             *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 18AUG15 |VA-0002 | SwiftC     | - Allow the user to say the destination *
*                               |   container with the quantity (1 alpha).*
*                               | - Allow the user to request the size of *
*                               |   the item being picked.                *
* 28AUG15 |VA-0004 | SwiftC     | Condition the "debug" messages put in by*
*                               | developers.                             *
***************************************************************************
'''

'''  PLEASE NOTE: Any changes made to this module may also need to be made 
                  in the PickPromptMultipleCustom module.
'''  

from dialogs import prompt_digits, prompt_only, prompt_yes_no #@UnusedImport
import sys
from vocollect_core import itext, obj_factory
from selection.PickPromptSingle import PickPromptSingleTask, ENTER_QTY,\
    QUANTITY_VERIFICATION, LOT_TRACKING
from vocollect_core.dialog.functions import prompt_alpha_numeric
from selection.SharedConstants import WEIGHT_SERIAL, PUT_PROMPT
from VoiceLinkDialogs import prompt_alpha_numeric_suggested

#Begin VA-0004
import GlobalVariables
#End   VA-0004

#----------------------------------------------------------
#Single pick prompt task
#----------------------------------------------------------
class PickPromptSingleTask_CSnx(PickPromptSingleTask):
    '''Pick prompts for regions with single pick prompt defined
     Extends PickPromptTask
     
     Steps:
            1. Slot verification (Overridden)
            2. Enter Quantity (Overridden)
            
            Remaining steps are the same as base
          
     Parameters
             Same as base class
    '''
    
    def enter_qty(self):
        '''override to enter quantity to just prompt for quantity'''

#Begin VA-0002
        self._container_already_spoken = False
#End   VA-0002

        #Quantity verification on, or shorting, or partial, or qty verification failed        
        if (self._region["qtyVerification"] == "1"
              or self.previous_state in [QUANTITY_VERIFICATION, ENTER_QTY] #Qty verification must of failed, so reprompt quantity
              or self._short_product 
              or self._partial):
            
            additional_vocabulary={'skip slot' : False, 'partial' : False, 'count down' : False}
            
            if self._short_product:
                prompt = itext('selection.pick.prompt.short.product.quantity')
            elif self._partial:
                prompt = itext('selection.pick.prompt.partial.quantity')
            else:
                prompt = itext("selection.pick.prompt.single.prompt.quantity")

#Begin VA-0002
#           result = prompt_digits(prompt,
#                                  itext("selection.pick.prompt.pick.quantity.help"),
#                                  1, len(str(self._expected_quantity)), 
#                                  False, False,
#                                  additional_vocabulary)
            #Change prompt if both multiple containers and pick to container.
            ##Additional lines to get the current assignment
            assignmentId = None
            self._puts = []
            self._put_quantity = 0
            for pick in self._picks:
                if pick['status'] != 'P': #Non picked picks
                    #check if first pick, or matches first pick
                    if assignmentId is None or assignmentId == pick['assignmentID']:
                        if assignmentId is None: 
                            assignmentId = pick['assignmentID']
                        self._puts.append(pick)
                        self._put_quantity += (pick['qtyToPick'] - pick['qtyPicked'])
            self._curr_assignment = None
            for assignment in self._assignment_lut:
                if assignment['assignmentID'] == assignmentId: 
                    self._curr_assignment = assignment
                    break
            ##End of additional lines...    
            if (self._region['containerType'] == 1 and
                self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID'])):
                result_with_container = prompt_alpha_numeric(prompt,
                                              itext("selection.pick.prompt.pick.quantity.help"),
                                              1, len(str(self._expected_quantity)) + 1,
                                              False, False,
                                              additional_vocabulary)
                #There shouldn't be any trailing blanks, but, to be sure...
                _length_of_result = len(str.rstrip(result_with_container))
                #If the last character spoken was a number, treat that as part of the pick quantity.
                #  The blank container ID will get sorted out in the container validation.
                if str.isnumeric(result_with_container[_length_of_result - 1]):
                    self._container_from_prompt = " "
                    result = result_with_container
                else:    
                    if str.isnumeric(result_with_container[:_length_of_result - 1]):    
                        self._container_from_prompt = result_with_container[_length_of_result - 1]
                        result = result_with_container[:_length_of_result - 1]
                    else:
                        self._container_from_prompt = " "
                        result = result_with_container    
                self._container_already_spoken = True
            else:
                result = prompt_digits(prompt,
                                       itext("selection.pick.prompt.pick.quantity.help"),
                                       1, len(str(self._expected_quantity)), 
                                       False, False,
                                       additional_vocabulary)
#End   VA-0002         
#Begin VA-0004   
#            sys.stdout.write('PickPromptSingleTask_EWMSCustom : result :'+result)
#            sys.stdout.flush()
            if GlobalVariables.show_debug_output:
                sys.stdout.write('PickPromptSingleTask_EWMSCustom : result :'+result)
                sys.stdout.flush()
#End   VA-0004            
            
            if result == 'skip slot':
                self.next_state = ENTER_QTY
                self._skip_slot()
            elif result == 'partial':
                self.next_state = ENTER_QTY
                self._validate_partial(self._expected_quantity, False)
            elif result == 'count down':
#Begin VA-0004  
#                sys.stdout.write('PickPromptSingleTask_EWMSCustom : vocab result count down')
#                sys.stdout.flush()
                if GlobalVariables.show_debug_output:
                    sys.stdout.write('PickPromptSingleTask_EWMSCustom : vocab result count down')
                    sys.stdout.flush()
#End   VA-0004  
                #self._picked_quantity = self._expected_quantity
                self._partial = False
                self._count_down()
            else:
                self._picked_quantity = int(result)
                if self._picked_quantity == self._expected_quantity:
                    self._partial = False
                    
        #No prompting assume expected quantity.
        else:
            self._picked_quantity = self._expected_quantity
            self.next_state=LOT_TRACKING  
            
            
    def _count_down(self):
        ''' speak count down '''
#Begin VA-0004
#        sys.stdout.write('PickPromptSingleTask_EWMSCustom : _count_down')
#        sys.stdout.flush()
        if GlobalVariables.show_debug_output:
            sys.stdout.write('PickPromptSingleTask_EWMSCustom : _count_down')
            sys.stdout.flush()
#Begin VA-0004
        #self._picked_quantity = 0
        quantity = self._expected_quantity
        
        while quantity >0:
            result = prompt_digits(itext("selection.pick.prompt.single.pick.countdown.quantity", quantity),
                                   itext("selection.pick.prompt.single.pick.countdown.quantity.help"),
                                   1, 10, 
                                   False, False)
            if(int(result) == 0):
                quantity = 0
            elif(int(result) > quantity):
                prompt_only(itext("selection.pick.prompt.pick.quantity.greater", int(result), quantity))
            else:
                quantity = quantity - int(result)
                self._picked_quantity = self._picked_quantity + int(result)
#Begin VA-0004
#                sys.stdout.write('PickPromptSingleTask_EWMSCustom : _count_down picked quantity000'+str(self._picked_quantity))
#                sys.stdout.flush()
                if GlobalVariables.show_debug_output:
                    sys.stdout.write('PickPromptSingleTask_EWMSCustom : _count_down picked quantity000'+str(self._picked_quantity))
                    sys.stdout.flush()
#End   VA-0004
        return True     
    
####  METHODS FROM PickPromptTask -- No further overrides required for this
    #----------------------------------------------------------
    def verify_entered_quantity(self):
        '''verifies the entered quantity'''
        #If the quantity verification is set prompt for quantity     
        if(self._picked_quantity > self._expected_quantity):
            prompt_only(itext("selection.pick.prompt.pick.quantity.greater", self._picked_quantity, self._expected_quantity))
            self.next_state = ENTER_QTY
        
        #check if quantity less than expected                                   
        elif self._picked_quantity <= self._expected_quantity and not self._partial:

            #if already doing a short product, then confirm spoken quantity
            if self._short_product:
                if prompt_yes_no(itext("selection.pick.prompt.short.product.verify",  self._picked_quantity)):
                    self._short_product = False
                    self._set_short_product(self._picked_quantity)
                else:
                    self.next_state = ENTER_QTY
            
            
            #not doing short, but quantity less than expected, ask if short
            elif self._picked_quantity < self._expected_quantity:
                self._short_product = prompt_yes_no(itext("selection.pick.prompt.pick.quantity.less",  
                                                          self._picked_quantity, 
                                                          self._expected_quantity ),
                                                    True, 6)
                if self._short_product:
                    self._short_product = False
                    self._set_short_product(self._picked_quantity)
                else:
                    self.next_state = ENTER_QTY
                    
        #quantity 0 and partial spoken
        elif self._picked_quantity == 0 and self._partial:
            prompt_only(itext('selection.pick.prompt.pick.quantity.zero'))
            self.next_state = ENTER_QTY
            self._partial = False
            
#Begin VA-0002
        #If the container ID was given in the prompt, validate the container now
        if (self.current_state == QUANTITY_VERIFICATION and 
            self._container_already_spoken):
            self.put_prompt()
#End   VA-0002            
        
    #---------------------------------------------------------   
    def intialize_puts(self):
        ''' initialize variables for putting. always done even if no put prompt required '''

#Begin VA-0002
#If the container has already been spoken, ignore the rest of this function.  Also, no
#  need to prompt for the container.
        if self._container_already_spoken:
            self.next_state = WEIGHT_SERIAL
            return
#End   VA-0002

        #build put list
        assignmentId = None
        self._curr_container = None
        self._puts = []
        self._put_quantity = 0
        
        for pick in self._picks:
            if pick['status'] != 'P': #Non picked picks
                #check if first pick, or matches first pick
                if assignmentId is None or assignmentId == pick['assignmentID']:
                    if assignmentId is None: 
                        assignmentId = pick['assignmentID']
                    self._puts.append(pick)
                    self._put_quantity += (pick['qtyToPick'] - pick['qtyPicked'])

        #adjust put quantity if greater then lot quantity
        if self._put_quantity > self._lot_quantity:
            self._put_quantity = self._lot_quantity

        #get current assignment record for puts
        self._curr_assignment = None
        for assignment in self._assignment_lut:
            if assignment['assignmentID'] == assignmentId: 
                self._curr_assignment = assignment
                break

        #not picking to containers, no put prompt needed
        if self._region['containerType'] == 0:
            self.next_state = WEIGHT_SERIAL

        #no quantity to put
        if self._put_quantity <= 0:
            self.next_state = WEIGHT_SERIAL
            
#Begin VA-0002
        #If we have the container number from the quantity prompt,
        #  no put prompt needed
        if self._container_already_spoken:
            self.next_state = WEIGHT_SERIAL            
#End   VA-0002            

    #---------------------------------------------------------   
    def put_prompt(self):
        '''Put Prompt'''

        expected_value = []
        scan_value = ''     
        
        #Do put prompt if multiple assignment or multiple open containers
#Begin VA-0002
#       if (self._assignment_lut.has_multiple_assignments() or 
#           self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID'])):
        #Do no prompt if the container ID was already given as part of the quantity prompt
        if ((self._assignment_lut.has_multiple_assignments() or 
             self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID'])) and
             not self._container_already_spoken):
#End   VA-0002            

            open_containers = self._container_lut.get_open_containers(self._curr_assignment['assignmentID'])

            # determine what the expected container value is
            if (self._puts[0]['targetContainer'] != 0):
                if ((self._curr_assignment['activeTargetContainer'] == self._puts[0]['targetContainer']) or
                    (len(open_containers) == 1)):
                    expected_value.append(open_containers[0]['spokenValidation'])
                    scan_value = open_containers[0]['scannedValidation']
                    
            elif (len(open_containers) == 1):
                expected_value.append(open_containers[0]['spokenValidation'])
                scan_value = open_containers[0]['scannedValidation']
                
                
            #indicates at put prompt
            dynamic_vocab = {}
            if not self._partial and self._assignment_lut.has_multiple_assignments():
                dynamic_vocab['partial'] = False
                
            #determine prompts
            if self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID']):
                prompt = itext('selection.put.prompt.for.container')
            elif self._partial:
                prompt = itext('selection.put.prompt.for.container')
            else:
                prompt = itext('selection.put.prompt.for.container.multiple', 
                                    self._put_quantity, self._curr_assignment['position'])

            result, scanned = prompt_alpha_numeric_suggested(prompt,
                                                   itext('selection.put.prompt.for.container.help'),
                                                   expected_value, 
                                                   scan_value,
                                                   self._region['spokenContainerLen'],
                                                   self._region['spokenContainerLen'],
                                                   False, True, 
                                                   dynamic_vocab)


            if result == 'partial':
                self.next_state = PUT_PROMPT
                if self._validate_partial(self._put_quantity, True):
                    self._get_partial_quantity()
            else:
                if not self._validate_container(result, scanned):
                    self.next_state = PUT_PROMPT
                    
#Begin VA-0002                    
        elif ((self._assignment_lut.has_multiple_assignments() or 
               self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID'])) and
               self._container_already_spoken):
            if not self._validate_container(self._container_from_prompt, False):
                self.next_state = ENTER_QTY
#End   VA-0002            
                    
        else:
            #should only be 1 open container for assignment
            self._validate_container(None, False)
    
   
obj_factory.set_override(PickPromptSingleTask, PickPromptSingleTask_CSnx)