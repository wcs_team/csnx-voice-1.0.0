'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores).         *
***************************************************************************
*    Module name..................                                        *
*    Overrides Module.............                                        *
*    Created by...................                                        *
*    First changes made by........                                        *
*    Date created................. MMM dd, 20yy                           *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* NNXXXNN |VA-NNNN |            |                                         *
***************************************************************************
'''

## - Single # comments are to remain
## - Double ## comments are to be deleted once noted...
## - Triple ### comments are to be uncommented and changed accordingly 

#Imports  
##  Initially, all unused to stop any warnings...

from vocollect_core import obj_factory #@UnusedImport
from common.VoiceLinkLut import VoiceLinkLut #@UnusedImport

##Import the overridden module and the classes being overridden

#Class overrides
## Override the class that holds the method being overridden...
###class OriginalClassName_CSnx(OriginalClassName):

##Copy the method from the original class, making the changes within
##  "begin" and "end" markers.


#Object factory overrides
##At the bottom of the module, don't forget the object factory overrides.
## There must be an object override for each class in the custom module.
## DO NOT FORGET...  Add the new custom import to main.py.
###obj_factory.set_override(OriginalClassName, OriginalClassName_CSnx) 