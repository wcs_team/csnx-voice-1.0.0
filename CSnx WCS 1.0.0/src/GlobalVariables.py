'''
***************************************************************************
*  LICENSED MATERIALS - PROPERTY OF WCS (Worldwide Chain Stores.          *
***************************************************************************
*    Module name.................. Main                                   *
*    Overrides Module.............                                        *
*    Created by................... Chris Swift                            *
*    First changes made by........ Chris Swift                            *
*    Date created................. 17 Sep 2015                            *
***************************************************************************

*-------------------------------------------------------------------------*
*                           MODULE CHANGE HISTORY                         *
*-------------------------------------------------------------------------*
* Change  | Change | Programmer |                                         *
*  Date   | Number | Initials   |     Detailed change description         *
*-------------------------------------------------------------------------*
* 28AUG15 |VA-0003 | SwiftC     | Shift handover collection changes       *
* 17SEP15 |VA-0004 | SwiftC     | Create a new file for global variables. *
***************************************************************************

Keep all global variables in this module so that only one import is 
required.  Otherwise, if any import has an import in itself, the import
will get done twice.

'''

show_debug_output = False

#Begin VA-0003
task_vehicle_id = None
no_equip_count = 0
no_equip_loc = ''
no_equip_loc_verf = ''
with_equip_count = 0
with_equip_loc = ''
with_equip_loc_verf = ''
handover_collected_assignemnt = 0
#End   VA-0003
