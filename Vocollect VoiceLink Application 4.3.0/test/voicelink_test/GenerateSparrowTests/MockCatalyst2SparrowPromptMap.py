import re, sys, os

#--------- enter the name of the mock catalyst test here ---------
TEST_FILE_DIR = '../functional_tests/CycleCounting_tests/'
TEST_FILE_NAME = 'testCycleCounting_109874.py'

try:
    catalyst_file = open(TEST_FILE_DIR + TEST_FILE_NAME, 'r')
except:
    print ("Unable to open file")
    sys.exit()



def build_prompt_map(line):
    prompt_index = line.find("#")
#    print (line[prompt_index+2:-1])
    prompt_string = (line[prompt_index+2:-1])

    response_index1 = line.find("'")
    response_index2 = line.find("'",response_index1+1,-1)
#    print (line[response_index1:response_index2+1])
    response_string = (line[response_index1:response_index2+1])

    sparrow_file.writelines("\'" + prompt_string + "\':" + response_string + ",\n")

def generate_sparrow_test_script():
    prompt_map_string = ''
    try:
        prompt_map = open('Prompt_Map.txt', 'r')
        for line in prompt_map:
            prompt_map_string = prompt_map_string + line
#        print(prompt_map_string)
        
        new_file_name = "Sparrow_" + TEST_FILE_NAME
        sparrow_script = open(new_file_name, 'w')
        
        sparrow_template = open('SparrowTestTemplate.py', 'r')
        for line in sparrow_template:
            line = line.replace('<<<< >>>>', prompt_map_string)
            sparrow_script.write(line)
        print (new_file_name + " sparrow script generated successfuly")
        prompt_map.close()
    except:
        print ("Error generating sparrow test script")


print_dialog_response = False
try:
    print("Generating sparrow prompt map.....")
    sparrow_file = open('Prompt_Map.txt', 'w')
    for line in catalyst_file:   
        if (re.search("post_dialog_responses", line)):
            #if we found the beginning of the post_dialog_responses
            #keep printing until we reach a ), which signifies
            #the end of self.post_dialog_responses
            print_dialog_response = True
 
        if print_dialog_response == True:
            #search for a ")" to check if we need to flip 
            #print_dialog_response
            if (re.search("\)", line)):
                #if we find a ) we've reached the end of the
                #self.post_dialog_responses
                print_dialog_response = False
            else:
                #otherwise keep on printin'
                build_prompt_map(line)
                #sparrow_file.writelines(line + "\n")
    print("Sparrow prompt map generated successfully.")
    print("Generating Sparrow test script.....")
    
    sparrow_file.close()
    catalyst_file.close()
    generate_sparrow_test_script()
except:
    print ("Error writing to the sparrow prompt map file")