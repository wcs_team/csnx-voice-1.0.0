from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from selection.NewContainer import NewContainer
from selection.CloseContainer import CloseContainer
from selection.PickAssignment import PickAssignmentTask
from selection.SelectionPrint import SelectionPrintTask
from selection.PickPrompt import PickPromptTask, PUT_PROMPT
from vocollect_core.task.task_runner import Launch
from selection.SelectionTask import SelectionTask
from selection.ReviewContents import ReviewContents
import unittest


class testSelectionVocabulary(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(SelectionTask, 3, VoiceLink())
        self._obj.taskRunner._append(self._obj)
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n"
                                     "N,0,402,893,1,pre 302,right 402,post 502,slot 891,15,'Pack',893,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0, \n\n"
                                     "N,0,403,894,1,pre 303,right 403,post 503,slot 893,16,'Pack',894,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0,\n\n"
                                     "N,1,405,895,1,pre 304,right 404,Post 504,Slot 890,1,'Pack',895,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0,\n\n");

        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,1,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,0,\n\n')
        self._obj._container_lut.receive('1,0000000003,03,12345,Store 123,,O,0,0,\n2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut
        self._obj.dynamic_vocab.assignment_lut = self._obj._assignment_lut
        self._obj.dynamic_vocab.region_config_rec = self._obj._region_config_lut[0]
        self._obj.dynamic_vocab.container_lut = self._obj._container_lut

    
    def test_valid(self):
        
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('store number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('route number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('new container'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('close container'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('reprint labels'), False)
                               
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('store number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('route number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('repeat last pick'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('review contents'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('review cluster'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('reprint labels'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('close container'), True)
                
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)

        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), True)
        
    def test_UPC(self):
        #Test if UPC is defined
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self._obj._picks_lut[0]['UPC'] = 'test UPC'
        
        self._obj.dynamic_vocab.execute_vocab('UPC')

        self.validate_prompts('t e s t   U P C')
        self.validate_server_requests()
        
        #Test blank UPC
        self._obj._picks_lut[0]['UPC'] = ''

        self._obj.dynamic_vocab.execute_vocab('UPC')

        self.validate_prompts('UPC not available')
        self.validate_server_requests()
    
    def test_how_much_more(self):
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        
        #test no picks picked
        self._obj.dynamic_vocab.execute_vocab('how much more')

        self.validate_prompts('42 remaining at 5 line items')
        self.validate_server_requests()
        
        #pick an item
        self._obj._picks_lut[0]['status'] = 'P'
        self._obj.dynamic_vocab.execute_vocab('how much more')

        self.validate_prompts('37 remaining at 4 line items')
        self.validate_server_requests()
        
        #test partial pick
        self._obj._picks_lut[1]['qtyPicked'] = 2
        self._obj.dynamic_vocab.execute_vocab('how much more')

        self.validate_prompts('35 remaining at 4 line items')
        self.validate_server_requests()
        
        #test 1 pick
        self._obj._picks_lut[1]['status'] = 'P'
        self._obj._picks_lut[2]['status'] = 'P'
        self._obj._picks_lut[3]['status'] = 'P'
        self._obj.dynamic_vocab.execute_vocab('how much more')

        self.validate_prompts('1 remaining at 1 line item')
        self.validate_server_requests()
        
        #test 0 pick
        self._obj._picks_lut[4]['status'] = 'P'
        self._obj.dynamic_vocab.execute_vocab('how much more')

        self.validate_prompts('0 remaining at 0 line items')
        self.validate_server_requests()

    def test_store_number(self):
        #test single assignment blank store number
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj._picks_lut[0]['store'] = ''
        
        self._obj.dynamic_vocab.execute_vocab('store number')

        self.validate_prompts('store number not available')
        self.validate_server_requests()

        #test single assignment with store number
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj._picks_lut[0]['store'] = 'test store'
        
        self._obj.dynamic_vocab.execute_vocab('store number')

        self.validate_prompts('t e s t   s t o r e')
        self.validate_server_requests()

        #test multiple assignments with store number
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj._picks_lut[0]['store'] = 'test store'
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                          '1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        
        self._obj.dynamic_vocab.execute_vocab('store number')

        self.validate_prompts('store number not available')
        self.validate_server_requests()

    def test_route_number(self):
        #test blank route
        self._obj._assignment_lut[0]['route'] = ''

        self._obj.dynamic_vocab.execute_vocab('route number')

        self.validate_prompts('route number not available')
        self.validate_server_requests()

        #test route number
        self._obj._assignment_lut[0]['route'] = 'test route'

        self._obj.dynamic_vocab.execute_vocab('route number')

        self.validate_prompts('t e s t   r o u t e')
        self.validate_server_requests()

        #test multiple assignments with store number
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                          '1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        
        self._obj._assignment_lut[0]['route'] = 'test route'

        self._obj.dynamic_vocab.execute_vocab('route number')

        self.validate_prompts('route number not available')
        self.validate_server_requests()

    def test_item_number(self):
        #Test if item number is defined
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self._obj._picks_lut[0]['itemNumber'] = 'test item number'
        
        self._obj.dynamic_vocab.execute_vocab('item number')

        self.validate_prompts('test item number')
        self.validate_server_requests()
        
        #Test blank item number
        self._obj._picks_lut[0]['itemNumber'] = ''

        self._obj.dynamic_vocab.execute_vocab('item number')

        self.validate_prompts('item number not available')
        self.validate_server_requests()
        
    def test_description(self):
        #Test if description is defined
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self._obj._picks_lut[0]['description'] = 'Test Item Description'
        
        self._obj.dynamic_vocab.execute_vocab('description')

        self.validate_prompts('test item description')
        self.validate_server_requests()
        
        #Test blank description
        self._obj._picks_lut[0]['description'] = ''

        self._obj.dynamic_vocab.execute_vocab('description')

        self.validate_prompts('description not available')
        self.validate_server_requests()
        
    def test_location(self):
        #Test location no pre aisle or post aisle
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self._obj._picks_lut[0]['preAisle'] = ''
        self._obj._picks_lut[0]['postAisle'] = ''
        
        self._obj.dynamic_vocab.execute_vocab('location')

        self.validate_prompts('aisle A 1, slot S 1')
        self.validate_server_requests()
        

        #Test location with pre aisle and post aisle
        self._obj._picks_lut[0]['preAisle'] = 'pre aisle'
        self._obj._picks_lut[0]['postAisle'] = 'post aisle'
        
        self._obj.dynamic_vocab.execute_vocab('location')

        self.validate_prompts('pre aisle, Aisle A 1, post aisle, slot S 1')
        self.validate_server_requests()
        #Test no aisle
        self._obj._picks_lut[0]['aisle'] = ''
        self._obj.dynamic_vocab.execute_vocab('location')
        self.validate_prompts('pre aisle, post aisle, slot S 1')
        self.validate_server_requests()
        
    def test_quantity(self):
        #test nothing picked
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)

        self._obj.dynamic_vocab.execute_vocab('quantity')

        self.validate_prompts('total quantity is <spell>42</spell> ')
        self.validate_server_requests()
        
        #test partial picked
        self._obj._picks_lut[1]['qtyPicked'] = 2

        self._obj.dynamic_vocab.execute_vocab('quantity')

        self.validate_prompts('total quantity is <spell>40</spell> ')
        self.validate_server_requests()
        
        #test put quantity
        pickPrompt = PickPromptTask(self._obj._region_config_lut[0], 
                                    self._obj._assignment_lut, 
                                    self._obj._picks_lut, 
                                    self._obj._container_lut, 
                                    False,
                                    self._obj.taskRunner, 
                                    self._obj)
        pickPrompt._curr_assignment = pickPrompt._assignment_lut[0]
        pickPrompt.current_state = PUT_PROMPT
        pickPrompt._put_quantity = 35
        self._obj.taskRunner._insert(pickPrompt)
        
        self._obj.dynamic_vocab.put_prompt = True
        self._obj._put_quantity = 35
        self._obj.dynamic_vocab.execute_vocab('quantity')
        self.validate_prompts('35')
        self.validate_server_requests()
        
    def test_repeat_last_pick(self):
        #test no previous pick
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        
        self._obj.dynamic_vocab.execute_vocab('repeat last pick')

        self.validate_prompts('Repeat last pick not valid.')
        self.validate_server_requests()

        #test previous pick
        self._obj._picks_lut[0]['status'] = 'P'
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        
        self._obj.dynamic_vocab.execute_vocab('repeat last pick')

        self.validate_prompts('last pick was aisle A 1,, slot S 1,, picked 0 of 42,')
        self.validate_server_requests()
        
        #test blank aisle
        self._obj._picks_lut[0]['aisle'] = ''
        self._obj.dynamic_vocab.execute_vocab('repeat last pick')
        self.validate_prompts('last pick was slot S 1,, picked 0 of 42,')
        self.validate_server_requests()
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,A,15,R12,0,0,1,Override summary prompt,0,\n'
                                          '1,0,67890,Store 123,B,15,R12,0,0,1,Override summary prompt,0,\n'
                                          '\n')
        self._obj._picks_lut.receive("P,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,5,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                     "P,0,2,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,5,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                     "P,0,3,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,5,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                     "P,0,4,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,5,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                     "P,0,5,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,5,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                     "\n")
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self._obj.dynamic_vocab.execute_vocab('repeat last pick')
        self.validate_server_requests()
        
    def test_next_pick(self):
        self.assertEquals(self._obj.dynamic_vocab.current_picks, [])
        self.assertEquals(self._obj.dynamic_vocab.previous_picks, [])

        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self.assertEquals(len(self._obj.dynamic_vocab.current_picks), 5)
        self.assertEquals(len(self._obj.dynamic_vocab.previous_picks), 0)
        
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self.assertEquals(len(self._obj.dynamic_vocab.current_picks), 5)
        self.assertEquals(len(self._obj.dynamic_vocab.previous_picks), 0)
        
        self._obj._picks_lut[0]['status'] = 'P'
        self._obj.dynamic_vocab.next_pick(self._obj._picks_lut)
        self.assertEquals(len(self._obj.dynamic_vocab.current_picks), 5)
        self.assertEquals(len(self._obj.dynamic_vocab.previous_picks), 5)
        
        self._obj.dynamic_vocab.next_pick(None)
        self.assertEquals(len(self._obj.dynamic_vocab.current_picks), 0)
        self.assertEquals(len(self._obj.dynamic_vocab.previous_picks), 5)
        
    def test_set_pick_prompt_task(self):
        self.assertEquals(self._obj.dynamic_vocab.pick_tasks, [])
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self.assertEquals(len(self._obj.dynamic_vocab.pick_tasks), 1)
        self.assertEquals(self._obj.dynamic_vocab.pick_tasks[0], self._obj.name)
        #add same name
        self._obj.dynamic_vocab.set_pick_prompt_task(self._obj.name)
        self.assertEquals(len(self._obj.dynamic_vocab.pick_tasks), 1)
        self.assertEquals(self._obj.dynamic_vocab.pick_tasks[0], self._obj.name)
        
    def test_repick_skips(self):
        self.start_server()
        
        #---------------------------------------------------
        #Test not allowed
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('repick skips'))
        
        self.validate_prompts('repick skips not allowed')
        self.validate_server_requests()
        
        #---------------------------------------------------
        #Test allowed response no
        self._obj.dynamic_vocab.region_config_rec['repickSkips'] = '1'
        self._obj.dynamic_vocab.region_config_rec['useLuts'] = 2
        self.post_dialog_responses('no')

        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('repick skips'))
        
        self.validate_prompts('repick skips, correct?')
        self.validate_server_requests()
        
        #---------------------------------------------------
        #Test allowed response yes, no skips
        self.post_dialog_responses('yes')

        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('repick skips'))
        
        self.validate_prompts('repick skips, correct?',
                              'no skips to repick, returning to picking')
        self.validate_server_requests()

        #---------------------------------------------------
        #Test allowed response yes, no skips
        self.set_server_response('0,\n\n')
        ps = PickAssignmentTask(self._obj.taskRunner, self._obj)
        ps.configure(self._obj._region_config_lut[0], 
                     self._obj._assignment_lut, 
                     self._obj._picks_lut, 
                     self._obj._container_lut, 
                     False)
        self._obj.taskRunner._append(ps)
        self.post_dialog_responses('yes')
        self._obj._picks_lut[1]['status'] = 'S'
        self._obj._picks_lut[2]['status'] = 'P'

        self.assertRaises(Launch, 
                          self._obj.dynamic_vocab.execute_vocab, 
                          'repick skips')

        self.validate_prompts('repick skips, correct?')
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','','2','N'])
        self.assertEqual('N', self._obj._picks_lut[1]['status'])
        self.assertEqual('P', self._obj._picks_lut[2]['status'])
        
    def test_new_container(self):
        #test launch new container
        self._obj.current_state = ''
        self.assertRaises(Launch, self._obj.dynamic_vocab._launch_new_container, self._obj._assignment_lut[0])
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, NewContainer))
        self.assertEquals(self._obj.current_state, '')
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'newContainer')
        
        #test launch new container single assignment
        self._obj.taskRunner.task_stack= []
        self._obj.taskRunner._append(self._obj)
        self.assertRaises(Launch, self._obj.dynamic_vocab._new_container)
        
        self._obj._assignment_lut.receive('2,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj.taskRunner.task_stack= []
        self._obj.taskRunner._append(self._obj)
        self.assertEquals(self._obj.dynamic_vocab._new_container(), True)
        self.validate_prompts('New container only allowed at put prompt')
        
        
        #test launch new container multiple assignments
        #TESTLINK 96273 :: Test Case New container not allowed at pick prompt for multiple assignment.
        pickPrompt = PickPromptTask(self._obj._region_config_lut[0], 
                                    self._obj._assignment_lut, 
                                    self._obj._picks_lut, 
                                    self._obj._container_lut, 
                                    False,
                                    self._obj.taskRunner, 
                                    self._obj)
        pickPrompt._curr_assignment = pickPrompt._assignment_lut[0]
        pickPrompt.current_state = PUT_PROMPT

        self._obj._assignment_lut.receive('2,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj.dynamic_vocab.put_prompt = True
        self._obj.assignmentId = '12345'
        self._obj.taskRunner.task_stack= []
        self._obj.taskRunner._append(self._obj)
        self._obj.taskRunner._append(pickPrompt)
        self.assertRaises(Launch, self._obj.dynamic_vocab._new_container)
        
        #TESTLINK 96164 :: Test Case New container not allowed when container type is set to 0
        self._obj._region_config_lut[0]['containerType'] = 0
        self._obj.taskRunner.task_stack= []
        self._obj.taskRunner._append(self._obj)
        self.assertTrue(self._obj.dynamic_vocab._new_container())
        self.validate_prompts('New container not allowed')   
        
    def test_close_container(self):
        #test launch new container
        self._obj.current_state = ''
        self.assertRaises(Launch, self._obj.dynamic_vocab._close_container)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, CloseContainer))
        self.assertEquals(self._obj.current_state, '')
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'closeContainer')        
        
    def test_reprint_labels(self):
        self._obj.current_state = ''
       
        #reprint label not available
        self._obj.dynamic_vocab.region_config_rec['printLabels'] = '0'
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('reprint labels'))
        self.validate_prompts('Reprint labels not allowed.')
        self.validate_server_requests()
        
        #print chase labels when printing is disabled for chase assignmentss
        self._obj._assignment_lut[0]['isChase'] = '1'
        self._obj.dynamic_vocab.region_config_rec['printChaseLabels'] = '0'
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('reprint labels'))
        self.validate_prompts('Reprint labels not allowed.')
        self.validate_server_requests()
        
        #ID 96389 :: Test Case Reprint Labels can be spoken anywhere in the pick prompt.
        self._obj._assignment_lut[0]['isChase'] = '0'
        self._obj.dynamic_vocab.region_config_rec['printLabels'] = '1'
        self._obj.current_state = ''
        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.dynamic_vocab._reprint_labels)
        self.validate_prompts('reprint labels, correct?')
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, '')
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')

        #ID 96389 :: Test Case Reprint Labels can be spoken anywhere in the pick prompt.
        self._obj._assignment_lut[0]['isChase'] = '0'
        self._obj.dynamic_vocab.region_config_rec['printLabels'] = '1'
        self._obj.current_state = ''
        self.post_dialog_responses('no')
        self._obj.dynamic_vocab._reprint_labels()
        self.validate_prompts('reprint labels, correct?')
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, '')
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')
        
    def test_review_contents(self):
        #test review content not valid
        self._obj.dynamic_vocab.region_config_rec['containerType'] = 0
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('review contents'))
        self.validate_prompts('Review contents not valid')
        self.validate_server_requests()
        
        #say no to review contents
        self.post_dialog_responses('no')
        self._obj.dynamic_vocab.region_config_rec['containerType'] = 1
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('review contents'))
        self.validate_prompts('Review contents, correct?')
        self.validate_server_requests()
        
        #say yes to review contents
        self._obj.current_state = ''
        self.post_dialog_responses('yes')
        self._obj.dynamic_vocab.region_config_rec['containerType'] = 1
        self.assertRaises(Launch, self._obj.dynamic_vocab._review_contents)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, ReviewContents))
        self.assertEquals(self._obj.current_state, '')
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'reviewContents')
        self.validate_prompts('Review contents, correct?')
        self.validate_server_requests()
         
    def test_review_cluster(self):
        #test review cluster not valid
        self._obj.dynamic_vocab.region_config_rec['containerType'] = 0
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('review cluster'))
        self.validate_prompts('Review cluster not valid')
        self.validate_server_requests()
        
        #review cluster for multiple assignments
        self.post_dialog_responses('yes', 'ready', 'ready')
        self._obj.dynamic_vocab.region_config_rec['containerType'] = 1
        self._obj._assignment_lut.receive('2,0,12346,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n1,0,12345,Store 125,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('review cluster'))
        self.validate_prompts('Review cluster, correct?', 'ID Store 123 Position 1', 'ID Store 125 Position 1')
        self.validate_server_requests()
        
        #review cluster for multiple assignments
        self.post_dialog_responses('no')
        self._obj.dynamic_vocab.region_config_rec['containerType'] = 1
        self._obj._assignment_lut.receive('2,0,12346,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n1,0,12345,Store 125,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('review cluster'))
        self.validate_prompts('Review cluster, correct?')
        self.validate_server_requests()
    
    def test_pass_assignment(self):
        #test setup
        self._obj.dynamic_vocab.region_config_rec['passAllowed'] = True
        self._obj.dynamic_vocab.region_config_rec['pickByPick'] = False
        self._obj.pick_only = False
        self._obj.dynamic_vocab._pass_inprogress = False
        for pick in self._obj._picks_lut:
            pick['status'] = 'N'
        for assignment in self._obj._assignment_lut:
            assignment['isChase'] = '0'

        #Test no pick task
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment not allowed.')
        self.validate_server_requests()
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)

        #test pass allowed with no
        ps = PickAssignmentTask(self._obj.taskRunner, self._obj)
        ps.configure(self._obj._region_config_lut[0], 
                     self._obj._assignment_lut, 
                     self._obj._picks_lut, 
                     self._obj._container_lut,
                     False)
        self._obj.taskRunner._append(ps)
        
        self.post_dialog_responses('no')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment, correct?')
        self.validate_server_requests()
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)
        
        #Test Region does not allow pass
        self._obj.dynamic_vocab.region_config_rec['passAllowed'] = False
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment not allowed.')
        self.validate_server_requests()
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)
        
        #Test already in pass
        self._obj.dynamic_vocab.region_config_rec['passAllowed'] = True
        self._obj.dynamic_vocab._pass_inprogress = True
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment not allowed.')
        self.validate_server_requests()
        self.assertTrue(self._obj.dynamic_vocab._pass_inprogress)
        
        #Test already in pass
        self._obj.dynamic_vocab._pass_inprogress = False
        self._obj._assignment_lut[0]['isChase'] = '1'
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment not allowed.')
        self.validate_server_requests()
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)
        
        #Test in base pass
        self._obj._assignment_lut[0]['isChase'] = '0'
        self._obj._picks_lut[0]['status'] = 'B'
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment not allowed.')
        self.validate_server_requests()
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)
        
        #Test in go back pass
        self._obj._picks_lut[0]['status'] = 'N'
        self._obj.pick_only = True
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment not allowed.')
        self.validate_server_requests()
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)
        
        #Test in go back pass
        self._obj.dynamic_vocab.region_config_rec['pickByPick'] = True
        self.post_dialog_responses('no')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment, correct?')
        self.validate_server_requests()
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)

        #Test yes response
        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'pass assignment')
        self.validate_prompts('Pass assignment, correct?')
        self.validate_server_requests()
        self.assertTrue(self._obj.dynamic_vocab._pass_inprogress)
        
        #test partial in progress
        self._obj.pick_only = False
        self._obj.dynamic_vocab.region_config_rec['pickByPick'] = False
        pick_prompt = PickPromptTask(self._obj._region_config_lut[0], 
                                     self._obj._assignment_lut, 
                                     self._obj._picks_lut, 
                                     None, False, self._obj.taskRunner, self._obj)
        self._obj.taskRunner._append(pick_prompt)
        pick_prompt._partial = True
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('pass assignment'))
        self.validate_prompts('Pass assignment not allowed.')
        
        
        #test new assignment
        self._obj.dynamic_vocab.new_assignment(None, None, None, None)
        self.assertFalse(self._obj.dynamic_vocab._pass_inprogress)
        
        #test new assignment, with one passed
        self._obj._assignment_lut[0]['passAssignment'] = '1'
        self._obj.dynamic_vocab.new_assignment(None, self._obj._assignment_lut, 
                                               None, None)
        
        
        self.assertTrue(self._obj.dynamic_vocab._pass_inprogress)

        
if __name__ == '__main__':
    unittest.main()

