from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from common.VoiceLinkLut import VoiceLinkLut
from core.VoiceLink import VoiceLink
from selection.CloseContainer import CloseContainer
from selection.NewContainer import NewContainer, CONFIRM_NEW_CONTAINER, \
    CLOSE_CONTAINER, OPEN_CONTAINER, NEW_RETURN_AFTER_DELIVERY
from selection.OpenContainer import OpenContainer
from selection.PickAssignment import PickAssignmentTask
from selection.SelectionTask import SelectionTask
from vocollect_core.task.task_runner import Launch

import unittest


##########################################
# Test New Container                     #
##########################################
class TestNewContainer(BaseVLTestCase):

    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        
        pickAssign = obj_factory.get(PickAssignmentTask,
                                       sel.taskRunner, sel)
        pickAssign.configure(sel._region_config_lut[0],
                             sel._assignment_lut,
                             sel._picks_lut,
                             sel._container_lut,
                             False)
        
        self._obj  = obj_factory.get(NewContainer,
                                       pickAssign._region, 
                                       pickAssign._assignment_lut,
                                       pickAssign._picks_lut, 
                                       pickAssign._container_lut,
                                       False,
                                       None,
                                       pickAssign.taskRunner,
                                       pickAssign)
        
        self._obj.dynamic_vocab = pickAssign.dynamic_vocab
        
        self.tempRegionLut = obj_factory.get(VoiceLinkLut, "prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        
        self.tempAssignmentLut = obj_factory.get(VoiceLinkLut, "prTaskLUTGetAssignment")
        self.tempAssignmentLut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        self._obj._container_lut.receive('1,0000000003,03,12345,Store 123,,O,0,0,\n2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self._obj._region =self.tempRegionLut[0]
        self._obj._assignment = self.tempAssignmentLut[0]
   
    def test_initializeStates(self):
        ''' Initialize States'''
              
        #test name
        self.assertEquals(self._obj.name, 'newContainer')

        #test states
        self.assertEquals(self._obj.states[0], CONFIRM_NEW_CONTAINER)
        self.assertEquals(self._obj.states[1], CLOSE_CONTAINER)
        self.assertEquals(self._obj.states[2], OPEN_CONTAINER)
        self.assertEquals(self._obj.states[3], NEW_RETURN_AFTER_DELIVERY)
        
    def test_confirm_new_container(self):
        #test target containers
        #TESTLINK 96167 :: Test Case New Container not allowed when a target container is specified.
        self._obj._picks[0]['targetContainer'] = 5
        self._obj.runState(CONFIRM_NEW_CONTAINER) 
        self.validate_prompts('New container not allowed when picks have a target container')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #test target containers
        self._obj._picks[0]['targetContainer'] = 0
        self._obj._region['allowMultOpenCont'] = 1
        self.post_dialog_responses('yes')
        self._obj.runState(CONFIRM_NEW_CONTAINER) 
        self.validate_prompts('new container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, OPEN_CONTAINER)
        
        #TESTLINK 96277 :: Test Case Operator response no for new container
        self._obj._picks[0]['targetContainer'] = 0
        self._obj._region['allowMultOpenCont'] = 1
        self.post_dialog_responses('no')
        self._obj.runState(CONFIRM_NEW_CONTAINER) 
        self.validate_prompts('new container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #test target containers
        #TESTLINK 96279 :: Test Case Verify for correct flow if multi open container per assign is set to 0.
        #TESTLINK 96312 :: Test Case New container spoken at put prompt for multiple assignments.
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self.post_dialog_responses('yes')
        self.post_dialog_responses('yes')
        self._obj.runState(CONFIRM_NEW_CONTAINER) 
        self.validate_prompts('new container, correct?','close current container?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CLOSE_CONTAINER)
        
        #test target containers
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self.post_dialog_responses('yes')
        self.post_dialog_responses('no')
        self._obj.runState(CONFIRM_NEW_CONTAINER) 
        self.validate_prompts('new container, correct?','close current container?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, OPEN_CONTAINER)
        
    def test_close_container(self):
        #test launch of close container
        self.assertRaises(Launch, self._obj.runState, CLOSE_CONTAINER)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, CloseContainer))
        self.assertEquals(self._obj.current_state, OPEN_CONTAINER)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'closeContainer')
    
    def test_open_container(self):
        #test launch of close container
        self.assertRaises(Launch, self._obj.runState, OPEN_CONTAINER)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, OpenContainer))
        self.assertEquals(self._obj.current_state, NEW_RETURN_AFTER_DELIVERY)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'openContainer')
        
    def test_return_on_delivery(self):
        '''verify return to deliver launch'''
        #test launch of deliver assignment
        self._obj._region['delContWhenClosed']='1'
        ps = PickAssignmentTask(self._obj.taskRunner, self._obj)
        ps.configure(self._obj._region, 
                     self.tempAssignmentLut, 
                     self._obj._picks, 
                     self._obj._container_lut,
                     False)
        
        vl = VoiceLink()
        vl._append(ps)
        self._obj.taskRunner = vl
           
        self._obj.close_container_command = True
        self.assertRaises(Launch, self._obj.runState, NEW_RETURN_AFTER_DELIVERY)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)  
         
if __name__ == '__main__':
    unittest.main()
           
