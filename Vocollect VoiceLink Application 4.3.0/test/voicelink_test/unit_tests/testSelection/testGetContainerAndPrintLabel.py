from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.SelectionTask import SelectionTask
from selection.BeginAssignment import BeginAssignment
from core.VoiceLink import VoiceLink
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core.task.task_runner import Launch
from selection.GetContainerPrintLabel import GetContainerPrintLabel, START,\
    GET_NEXT_ASSIGNMENT, CHECK_PICKS, CHECK_CHASE_ASSIGNMENT,\
    NO_PICK_TO_CONTAINER, PROMPT_FOR_CONTAINER, NO_PRINT_LABEL,\
    PRE_CREATE_CONTAINERS, GET_CONTAINER, NEW_CONTAINER, PRINT, PRINT_RETURN
from selection.OpenContainer import OpenContainer
from selection.SelectionPrint import SelectionPrintTask

class TestGetContainerPrintLabelTask(BaseVLTestCase):

    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        begin_assign = obj_factory.get(BeginAssignment,
                                         sel._region_config_lut[0],
                                         sel._assignment_lut,
                                         sel._picks_lut,
                                         sel._container_lut,
                                         sel.taskRunner, sel)
        
        self._obj = obj_factory.get(GetContainerPrintLabel,
                                      begin_assign._region ,
                                      begin_assign._assignment_lut, 
                                      begin_assign._picks_lut, 
                                      begin_assign._container_lut,
                                      begin_assign.taskRunner, begin_assign)   
      
        self.tempRegionLut = obj_factory.get(VoiceLinkLut, "prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
                                     
   
    def test_initializeStates(self):
        ''' Initialize States'''
              
        #test name
        self.assertEquals(self._obj.name, 'getContainerPrintLabel')

        #test states
        self.assertEquals(self._obj.states[0], START)
        self.assertEquals(self._obj.states[1], GET_NEXT_ASSIGNMENT)
        self.assertEquals(self._obj.states[2], CHECK_PICKS)
        self.assertEquals(self._obj.states[3], CHECK_CHASE_ASSIGNMENT)
        self.assertEquals(self._obj.states[4], NO_PICK_TO_CONTAINER)
        self.assertEquals(self._obj.states[5], PROMPT_FOR_CONTAINER)
        self.assertEquals(self._obj.states[6], NO_PRINT_LABEL)
        self.assertEquals(self._obj.states[7], PRE_CREATE_CONTAINERS)
        self.assertEquals(self._obj.states[8], GET_CONTAINER)
        self.assertEquals(self._obj.states[9], NEW_CONTAINER)
        self.assertEquals(self._obj.states[10], PRINT)
        self.assertEquals(self._obj.states[11], PRINT_RETURN)
        
        
    def test_start(self):
        self._obj.region = self.tempRegionLut[0]
        self.start_server()
       
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                 "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
                                
        self._obj._set_variables()
         
        self._obj._region['containerType'] = 1
        self.set_server_response(',,,,,,,,0,\n\n')
        self._obj.runState(START)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTContainer','1','','','','','0',''])
        self.validate_prompts()
         
        self._obj._region['containerType'] = 0
        self._obj.runState(START)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
         
    def test_get_assignment(self):
        self._obj.region = self.tempRegionLut[0]
        self.start_server()
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                 "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
                                     
                                 
        self._obj._set_variables()
        self._obj.runState(GET_NEXT_ASSIGNMENT)
        self.assertEquals(self._obj.next_state, CHECK_CHASE_ASSIGNMENT)
        self.validate_prompts()
         
        self._obj.assignment_iterator = None
        self._obj.runState(GET_NEXT_ASSIGNMENT)
        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts()
        
     
    def test_check_picks(self):
        self._obj.region = self.tempRegionLut[0]
        self.start_server()
        
         
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                 "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
                                
        self._obj._set_variables()
         
        self._obj._assignment = next(self._obj._assignment_iterator)
        self._obj.runState(CHECK_PICKS)
        self.assertEquals(self._obj.next_state, NEW_CONTAINER)
        self.validate_server_requests()
        self.validate_prompts()
     
        self._obj._container_lut.receive(',,,,,,,,0,\n\n')
        self._obj.runState(CHECK_PICKS)
        self.assertEquals(self._obj.next_state, CHECK_CHASE_ASSIGNMENT)
        self.validate_server_requests()
        self.validate_prompts()
         
        self._obj._container_lut.receive('1,0000000003,03,12345,Store 123,,O,0,0,\n\n') 
        self._obj.runState(CHECK_PICKS)
        self.assertEquals(self._obj.next_state, GET_NEXT_ASSIGNMENT)
        self.validate_server_requests()
        self.validate_prompts()       
         
        self._obj._picks.receive("P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                 "P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.runState(CHECK_PICKS)
        self.assertEquals(self._obj.next_state, GET_NEXT_ASSIGNMENT)
        self.validate_server_requests()
        self.validate_prompts()                    
             
    def testGetContainer(self):
        # Region Flags
        # 13 - container type 0 - no container 1 - container
        # 10 - printChaseLabels 0 - no 1 - yes
        # 27/29 - promptForContainer ( prompt operator for container id) 0 - don't 1 - prompt
        # 26/29 - printNumLabels(pre-create containers) 0 - no prompt 1 - prompt for labels
        # 9 - printLabels 0 - don't 1 and 2 prompt at beginning and end of the assignment
        
        # Assignment Flags
        # 2 - isChase 
        
        self._obj.region = self.tempRegionLut[0]
        self.start_server()
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
                                
        self._obj._set_variables()
        
        #TESTLLINK 95678 :: Test Case Verify correct behavior if VoiceLink region profile 108 is used
        self._obj._assignment = next(self._obj._assignment_iterator)
        self.post_dialog_responses('55!', 'yes')
        self.set_server_response(',,,,,,,,0,\n\n')
        self._obj.runState(GET_CONTAINER)
        self.assertEquals(self._obj.next_state, PRINT)
        self.validate_server_requests(['prTaskLUTContainer','1','12345','','','','3','55'])
        self.validate_prompts('Number of Labels for ID Store 123',
                              '55, correct?')
        
        #main tests start here
        #chase assignment chase label
        self._obj._picks[0]['targetContainer'] = 1
        self.post_dialog_responses('yes')
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,0,\n\n')
        self._obj.runState(GET_CONTAINER)
        self.assertEquals(self._obj.next_state, PRINT)
        self.validate_server_requests(['prTaskLUTContainer','1','12345','','', '', '3',''])
        self.validate_prompts('Preprint all labels for ID Store 123')
      
        # no on pre -print labels
        self._obj._picks[0]['targetContainer'] = 1
        self.post_dialog_responses('no')
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,0,\n\n')
        self._obj.runState(GET_CONTAINER)
        self.assertEquals(self._obj.next_state, NEW_CONTAINER)
        self.validate_server_requests()
        self.validate_prompts('Preprint all labels for ID Store 123')
        
        #TESTLINK ID 95691 :: Test Case Verify correct behavior is error returned in response to container LUT
        self._obj._picks[0]['targetContainer'] = 1
        self.post_dialog_responses('yes')
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,198,error\n\n')
        self._obj.runState(GET_CONTAINER)
        self.assertEquals(self._obj.next_state, PRINT)
        self.validate_server_requests(['prTaskLUTContainer','1','12345','','', '', '3', ''])
        self.validate_prompts('Preprint all labels for ID Store 123')
      
    def test_chase_label(self):   
         
        self._obj._region = self.tempRegionLut[0]
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
    
        self._obj._set_variables()
        self._obj._assignment = next(self._obj._assignment_iterator)
       
        #Not a chase assignment
        self._obj._region['printChaseLabels'] = '0'
        self._obj._assignment['isChase'] = '1'
        self._obj.runState(CHECK_CHASE_ASSIGNMENT)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #container type not pick to container print label is set
        self._obj._region['printChaseLabels'] = '1'
        self._obj._region['printLabels'] = '1'
        self._obj.runState(CHECK_CHASE_ASSIGNMENT)
        self.assertEquals(self._obj.next_state, PRINT)
        self.validate_prompts()
        self.validate_server_requests()
        
        
    def test_not_pick_to_container(self):
        
        self._obj.region = self.tempRegionLut[0]
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
    
        self._obj._set_variables()
        self._obj.assignment = next(self._obj._assignment_iterator)
       
        #container type not pick to container print label is set
        self._obj._region['printLabels'] = '1'
        self._obj._region['containerType'] = 0
        self._obj.runState(NO_PICK_TO_CONTAINER)
        self.assertEquals(self._obj.next_state, PRINT_RETURN)
        self.validate_prompts()
        self.validate_server_requests()
        
        
        #print labels not set
        self._obj._region['printLabels'] = '1'
        self._obj._region['containerType'] = 1
        self._obj.runState(NO_PICK_TO_CONTAINER)
        self.assertEquals(self._obj.next_state, PRINT_RETURN)
        self.validate_prompts()
        self.validate_server_requests()
        
        #print labels not set
        self._obj._region['printLabels'] = '0'
        self._obj._region['containerType'] = 0
        self._obj.runState(NO_PICK_TO_CONTAINER)
        self.assertEquals(self._obj.next_state, GET_NEXT_ASSIGNMENT)
        self.validate_prompts()
        self.validate_server_requests()
        
    def test_prompt_for_container(self):
        self._obj._region = self.tempRegionLut[0]
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
    
        self._obj._set_variables()
        self._obj.assignment = next(self._obj._assignment_iterator)
       
        self._obj._region['promptForContainer'] = 0
        self._obj.runState(PROMPT_FOR_CONTAINER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #set prompt for container id
        self._obj._region['promptForContainer'] = 1
        self._obj.runState(PROMPT_FOR_CONTAINER)
        self.assertEquals(self._obj.next_state, NEW_CONTAINER)
        self.validate_prompts()
        self.validate_server_requests()
        
        
    def test_no_print_label(self):
        self._obj.region = self.tempRegionLut[0]
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
    
        self._obj._set_variables()
        self._obj.assignment = next(self._obj._assignment_iterator)
        
        self._obj._region['printLabels'] = '1'
        self._obj.runState(NO_PRINT_LABEL)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
     
        self._obj._region['printLabels'] = '0'
        self._obj.runState(NO_PRINT_LABEL)
        self.assertEquals(self._obj.next_state, NEW_CONTAINER)
        self.validate_prompts()
        self.validate_server_requests()
        
    def test_pre_create_containers(self):
        
        self._obj.region = self.tempRegionLut[0]
        
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
    
        self._obj._set_variables()
        self._obj._assignment = next(self._obj._assignment_iterator)
        
        self._obj._region['printNumLabels'] = '1'
        self._obj.runState(PRE_CREATE_CONTAINERS)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        self._obj._region['printNumLabels'] = '0'
        self._obj.runState(PRE_CREATE_CONTAINERS)
        self.assertEquals(self._obj.next_state, NEW_CONTAINER)
        self.validate_prompts()
        self.validate_server_requests()
    
    def test_new_container(self):
        #test launch of new container
        self.assertRaises(Launch, self._obj.runState, NEW_CONTAINER)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, OpenContainer))
        self.assertEquals(self._obj.current_state, GET_NEXT_ASSIGNMENT)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'openContainer')
    
    def test_print_label(self):
        #test print label
        self._obj._print_create = False
        self.assertRaises(Launch, self._obj.runState, PRINT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, GET_NEXT_ASSIGNMENT)
        self.assertEquals(self._obj._print_create, False)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')
       
        self._obj._print_create = True
        self.assertRaises(Launch, self._obj.runState, PRINT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, NEW_CONTAINER)
        self.assertEquals(self._obj._print_create, False)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')
         
    def test_print_return(self):
        #test print label
        self._obj._print_create = False
        self.assertRaises(Launch, self._obj.runState, PRINT_RETURN)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, '')
        self.assertEquals(self._obj._print_create, False)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')
    
