from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.SelectionTask import SelectionTask
from selection.PickAssignment import PickAssignmentTask 
from selection.PickPromptSingle import PickPromptSingleTask
from core.VoiceLink import VoiceLink
from selection.LotTracking import LotTrackingTask, ENTER_LOT, ENTER_QUANTITY,\
    SEND_LOT
from common.VoiceLinkLut import VoiceLinkLut

class TestLotTrackingTask(BaseVLTestCase):

    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        pickassign= obj_factory.get(PickAssignmentTask,
                                      sel.taskRunner, sel) 

        pickassign.configure(sel._region_config_lut[0],
                             sel._assignment_lut,
                             sel._picks_lut,
                             sel._container_lut,
                             False)
        
        pickassign._region = sel._region_config_lut[0]
        
        pickPromptSingle = obj_factory.get(PickPromptSingleTask,
                                             pickassign._region, 
                                             pickassign._assignment_lut, 
                                             pickassign._pickList,
                                             pickassign._container_lut,
                                             False,
                                             pickassign.taskRunner, sel)
        
        self._obj = obj_factory.get(LotTrackingTask,
                                      pickPromptSingle._region,
                                      pickPromptSingle._assignment_lut,
                                      pickPromptSingle._picks,
                                      pickPromptSingle._picked_quantity,
                                      pickPromptSingle.taskRunner,
                                      pickPromptSingle)
        
        self.tempPickLut = obj_factory.get(VoiceLinkLut, "prTaskLUTGetPicks")
        self.tempPickLut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                 "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n"
                                 "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,pick message,0,0,0,0,0,\n\n");
          
    
    def test_initializeStates(self):
        ''' Initialize States'''
              
        #test name
        self.assertEquals(self._obj.name, 'lotTracking')

        #test states
        self.assertEquals(self._obj.states[0], ENTER_LOT)
        self.assertEquals(self._obj.states[1], ENTER_QUANTITY)
        self.assertEquals(self._obj.states[2], SEND_LOT)
        
        #test dynamic vocab set properly
        self.assertTrue(self._obj.name in self._obj.dynamic_vocab.pick_tasks)
        
        
    def test_quantity_to_lot(self):
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
       
        #TESTLINK 95401 :: Test Case Verify that the selector can continue if the LOT text is blank.
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[2]) 
        self._obj._set_variables()
        self._obj._expected_quantity = 10
        self.post_dialog_responses('8!')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('Lot Number')
        self.assertEquals(self._obj._lot_number, '8')
        self.assertEquals(self._obj.next_state, ENTER_QUANTITY)
        self.validate_server_requests()
 
        #initialize picks
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[1])
        self._obj._set_variables()
         
        #test lot text prompt
        self.post_dialog_responses('8!')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text')
        self.assertEquals(self._obj._lot_number, '8')
        self.assertEquals(self._obj.next_state, ENTER_QUANTITY)
        self.validate_server_requests()
         
        #TESLINK 95456 :: Test Case Verify batched assignment with inconsistent values for capture lot flag.
        self._obj._picks[1]['captureLot']=0
        self.post_dialog_responses('8!')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text')
        self.assertEquals(self._obj._lot_number, '8')
        self.assertEquals(self._obj.next_state, ENTER_QUANTITY)
        self.validate_server_requests()
         
        #TESLINK 95423 :: Test Case Verify that alpha-numeric Lot codes can be entered.
        self.post_dialog_responses('ABC8!')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text')
        self.assertEquals(self._obj._lot_number, 'ABC8')
        self.assertEquals(self._obj.next_state, ENTER_QUANTITY)
        self.validate_server_requests()
          
        # test description
        #TESTLINK 95407 - Test Case Verify that the selector can speak 'description' to hear a list of lots
        self.start_server()
        self._obj._lot_number = None
        self.post_dialog_responses('description')
        self.post_dialog_responses('ready')
        self.post_dialog_responses('ready')
        self.set_server_response('1234A, 0,\n\n'+
                                       '1234B, 0,\n\n')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text','1234A', '1234B')
        self.assertEquals(self._obj._lot_number, None)
        self.assertEquals(self._obj.next_state, ENTER_LOT)
        self.validate_server_requests(['prTaskLUTValidLots', '1', '12345', 'L1','ITEM12','1'])
        
        #TESTLINK 95414 :: Test Case Verify correct behavior if error response to Valid LOTS LUT.
        self.post_dialog_responses('description')
        self.post_dialog_responses('stop')
        self.set_server_response('1234A, 0,\n\n')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text','1234A')
        self.assertEquals(self._obj._lot_number, None)
        self.assertEquals(self._obj.next_state, ENTER_LOT)
        self.validate_server_requests(['prTaskLUTValidLots', '1', '12345', 'L1','ITEM12','1'])
         
        #TESTLINK 95414 - Test Case Verify correct behavior if error response to Valid LOTS LUT.
        self.post_dialog_responses('description')
        self.post_dialog_responses('ready')
        self.set_server_response('1234A, 199, error\n\n')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text','error, To continue say ready')
        self.assertEquals(self._obj._lot_number, None)
        self.assertEquals(self._obj.next_state, ENTER_LOT)
        self.validate_server_requests(['prTaskLUTValidLots', '1', '12345', 'L1','ITEM12','1'])
        
        #TELINKID 95425 :: Test Case Verify selector can short a pick at the LOT prompt.
        self.post_dialog_responses('short product')
        self.post_dialog_responses('yes')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text', 'You picked 0 of 10 items.  Is this a short product?')
        self.assertEquals(self._obj._lot_number, None)
        self.assertEquals(self._obj._lot_quantity, 0)
        self.assertEquals(self._obj.next_state, '')
        self.validate_server_requests()
         
        self.post_dialog_responses('short product')
        self.post_dialog_responses('no')
        self._obj.runState(ENTER_LOT) 
        self.validate_prompts('lot text', 'You picked 0 of 10 items.  Is this a short product?')
        self.assertEquals(self._obj._lot_number, None)
        self.assertEquals(self._obj._lot_quantity, 0)
        self.assertEquals(self._obj.next_state, ENTER_LOT)
        self.validate_server_requests()
      
    def test_valid_quantity(self):
         
        #quantity less than expected so valid quantity
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[1])
        self._obj._set_variables()
        self._obj._lot_number=123
        self._obj._expected_quantity=15
        self.post_dialog_responses('10!')
        self._obj.runState(ENTER_QUANTITY)
        self.validate_prompts('Quantity for this lot text')
        self.assertEquals(self._obj._lot_quantity, 10)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
         
        #quantity greater than expected so invalid quantity
        #TESTLINK 95454 :: Test Case Verify lot capture for batched assignments.
        self._obj._lot_number=123
        self._obj._expected_quantity=15
        self.post_dialog_responses('20!')
        self.post_dialog_responses('ready')
        self._obj.runState(ENTER_QUANTITY)
        self.validate_prompts('Quantity for this lot text', 'You said 20, expected 15. Say ready.')
        self.assertEquals(self._obj._lot_quantity, 20)
        self.assertEquals(self._obj.next_state, ENTER_QUANTITY)
        self.validate_server_requests()
         
             
    def test_send_lot(self):
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[1])
        self._obj._set_variables()
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
         
        #send multiple responses verify send lot
        #TESTLINKID 95427 :: Test Case Verify correct behavior if multiple records returned in response to SENDLOT
        self.start_server()
        self.set_server_response('1234A, 0,\n\n'+
                                       '1234B, 0,\n\n')
        self._obj._lot_quantity= 10
        self.post_dialog_responses('no', 'yes')
        self._obj.runState(SEND_LOT)
        self.validate_prompts('1 2 3 4 A', '1 2 3 4 B')
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'])
         
        #TESTLINK 95432 :: Test Case Confirm Failed LOT LUT is sent if three failures on LOT code entry.
        self.set_server_response('1234A, 0,\n\n'+
                                       '1234B, 0,\n\n')
        self._obj._lot_quantity= 10
        self._obj._incorrect_lot_returned_times = 2
        self.post_dialog_responses('no', 'no')
        self._obj.runState(SEND_LOT)
        self.validate_prompts('1 2 3 4 A', '1 2 3 4 B')
        self.assertEquals(self._obj.next_state, '')
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'], ['prTaskLUTFailedLotNumber', '1','12345','1',''])
         
        self.set_server_response('1234A, 199,Error message\n\n')
        self._obj._lot_quantity= 10
        self._obj._incorrect_lot_returned_times = 1
        self.post_dialog_responses('ready')
        self._obj.runState(SEND_LOT)
        self.validate_prompts('Error message, To continue say ready')
        self.assertEquals(self._obj.next_state, ENTER_LOT)
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'])
         
        #TESTLINK 95445 :: Test Case Verify that the SENDLOT LUT is re-transmitted if it times out.
        self.stop_server()
        #self.set_server_response('1234A, -1,Error message\n\n')
        self.post_dialog_responses('ready')
        self._obj._lot_quantity= 10
        self._obj._incorrect_lot_returned_times = 1
        self._obj.runState(SEND_LOT)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.assertEquals(self._obj.next_state, SEND_LOT)
        self.validate_server_requests()
        self.start_server()
               
        #send single responses verify send lot
        self.set_server_response('1234A, 0,\n\n')
        self._obj._incorrect_lot_returned_times = 0
        self._obj._lot_quantity= 10
        self._obj.runState(SEND_LOT)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'])
         
        #TESTLINK 95441 :: Test Case Verify correct behavior if error response to SENDLOT LUT.
        self.set_server_response('1234A, 199,Error message\n\n')
        self._obj._incorrect_lot_returned_times = 0
        self._obj._previous_lot_spoken='55'
        self.post_dialog_responses('ready')
        self._obj._lot_quantity= 10
        self._obj.runState(SEND_LOT)
        self.validate_prompts('Error message, To continue say ready')
        self.assertEquals(self._obj.next_state, ENTER_LOT)
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'])
         
        #verify error condition
        self.set_server_response('1234A, -1, Error message\n\n')
        self.post_dialog_responses('ready')
        self._obj._lot_quantity= 10
        self._obj.runState(SEND_LOT)
        self.assertEquals(self._obj.next_state, SEND_LOT)
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'])
        self.validate_prompts('Error contacting host,  to try again say ready')
         
        #TESLINK 95435 :: Test Case Confirm Failed LOT LUT is sent if error is returned three times
        #verify failed LUT is sent
        self.set_server_response('1234A, 5, Error message\n\n')
        self._obj._lot_number_spoken_no_times_in_row = 2
        self._obj._lot_quantity= 10
        self.post_dialog_responses('ready')
        self._obj.runState(SEND_LOT)
        self.validate_prompts('Error message, To continue say ready')
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(self._obj._lot_number_spoken_no_times_in_row, 3)
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'], ['prTaskLUTFailedLotNumber', '1','12345','1',''])
         
         
        self.set_server_response('1234A, 0,\n\n'+
                                       '1234B, 0,\n\n')
        self._obj._lot_quantity= 10
        self._obj._previous_lot_spoken='test'
        self._obj._incorrect_lot_returned_times = 1
        self.post_dialog_responses('no', 'no')
        self._obj.runState(SEND_LOT)
        self.validate_prompts('1 2 3 4 A', '1 2 3 4 B')
        self.assertEquals(self._obj.next_state, ENTER_LOT)
        self.validate_server_requests(['prTaskLUTSendLot', '', '10', '12345', '1'])
