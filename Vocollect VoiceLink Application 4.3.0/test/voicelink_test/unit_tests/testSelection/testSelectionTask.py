from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from common.RegionSelectionTasks import GET_VALID_REGIONS, GET_REGION_CONFIG
from selection.GetAssignment import GetAssignmentAuto, GetAssignmentManual
from selection.BeginAssignment import BeginAssignment
from selection.PickAssignment import PickAssignmentTask
from selection.DeliverAssignment import DeliverAssignmentTask
from core.VoiceLink import VoiceLink
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from selection.SelectionTask import SelectionTask, REGIONS, VALIDATE_REGION,\
    GET_ASSIGNMENT, BEGIN_ASSIGNMENT, PICK_ASSIGNMENT, DELIVER_ASSIGNMENT,\
    COMPLETE_ASSIGNMENT, PICK_REMAINING, PRINT_LABEL, SelectionRegionTask,\
    CHECK_ASSIGNMENT,PROMPT_ASSIGNMENT_COMPLETE
    
from selection.SelectionPrint import SelectionPrintTask
from core.CoreTask import CoreTask
import unittest


class testSelectionTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        vl = VoiceLink()
        core = obj_factory.get(CoreTask, vl)
        core.function = 1
        vl._append(core)
        self._obj = obj_factory.get(SelectionTask, 3, vl)
        vl._append(self._obj)
        TaskRunnerBase._main_runner = vl
        
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'selection')

        #test states
        self.assertEquals(self._obj.states[0], REGIONS)
        self.assertEquals(self._obj.states[1], VALIDATE_REGION)
        self.assertEquals(self._obj.states[2], GET_ASSIGNMENT)
        self.assertEquals(self._obj.states[3], CHECK_ASSIGNMENT)
        self.assertEquals(self._obj.states[4], BEGIN_ASSIGNMENT)
        self.assertEquals(self._obj.states[5], PICK_ASSIGNMENT)
        self.assertEquals(self._obj.states[6], PICK_REMAINING)
        self.assertEquals(self._obj.states[7], PRINT_LABEL)
        self.assertEquals(self._obj.states[8], DELIVER_ASSIGNMENT)
        self.assertEquals(self._obj.states[9], COMPLETE_ASSIGNMENT)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['assignmentType'].index, 2)
        self.assertEquals(fields['autoAssign'].index, 3)
        self.assertEquals(fields['maxNumberWordID'].index, 4)
        self.assertEquals(fields['skipAisleAllowed'].index, 5)
        self.assertEquals(fields['skipSlotAllowed'].index, 6)
        self.assertEquals(fields['repickSkips'].index, 7)
        self.assertEquals(fields['printLabels'].index, 8)
        self.assertEquals(fields['printChaseLabels'].index, 9)
        self.assertEquals(fields['pickPromptType'].index, 10)
        self.assertEquals(fields['signOffAllowed'].index, 11)
        self.assertEquals(fields['containerType'].index, 12)
        self.assertEquals(fields['delContWhenClosed'].index, 13)
        self.assertEquals(fields['passAllowed'].index, 14)
        self.assertEquals(fields['delivery'].index, 15)
        self.assertEquals(fields['qtyVerification'].index, 16)
        self.assertEquals(fields['workIDLen'].index, 17)
        self.assertEquals(fields['goBackForShorts'].index, 18)
        self.assertEquals(fields['allowReversePicking'].index, 19)
        self.assertEquals(fields['useLuts'].index, 20)
        self.assertEquals(fields['currentPreAisle'].index, 21)
        self.assertEquals(fields['currentAisle'].index, 22)
        self.assertEquals(fields['currentPostAisle'].index, 23)
        self.assertEquals(fields['currentSlot'].index, 24)
        self.assertEquals(fields['printNumLabels'].index, 25)
        self.assertEquals(fields['promptForContainer'].index, 26)
        self.assertEquals(fields['allowMultOpenCont'].index, 27)
        self.assertEquals(fields['spokenContainerLen'].index, 28)
        self.assertEquals(fields['pickByPick'].index, 29)
        self.assertEquals(fields['errorCode'].index, 30)
        self.assertEquals(fields['errorMessage'].index, 31)

        fields = self._obj._assignment_lut.fields
        self.assertEquals(fields['groupID'].index, 0)
        self.assertEquals(fields['isChase'].index, 1)
        self.assertEquals(fields['assignmentID'].index, 2)
        self.assertEquals(fields['idDescription'].index, 3)
        self.assertEquals(fields['position'].index, 4)
        self.assertEquals(fields['goalTime'].index, 5)
        self.assertEquals(fields['route'].index, 6)
        self.assertEquals(fields['activeTargetContainer'].index, 7)
        self.assertEquals(fields['passAssignment'].index, 8)
        self.assertEquals(fields['summaryPromptType'].index, 9)
        self.assertEquals(fields['overridePrompt'].index, 10)
        self.assertEquals(fields['errorCode'].index, 11)
        self.assertEquals(fields['errorMessage'].index, 12)
        
        fields = self._obj._picks_lut.fields
        self.assertEquals(fields['status'].index, 0)
        self.assertEquals(fields['baseItem'].index, 1)
        self.assertEquals(fields['sequence'].index, 2)
        self.assertEquals(fields['locationID'].index, 3)
        self.assertEquals(fields['region'].index, 4)
        self.assertEquals(fields['preAisle'].index, 5)
        self.assertEquals(fields['aisle'].index, 6)
        self.assertEquals(fields['postAisle'].index, 7)
        self.assertEquals(fields['slot'].index, 8)
        self.assertEquals(fields['qtyToPick'].index, 9)
        self.assertEquals(fields['UOM'].index, 10)
        self.assertEquals(fields['itemNumber'].index, 11)
        self.assertEquals(fields['variableWeight'].index, 12)
        self.assertEquals(fields['variableWeightMin'].index, 13)
        self.assertEquals(fields['variableWeightMax'].index, 14)
        self.assertEquals(fields['qtyPicked'].index, 15)
        self.assertEquals(fields['checkDigits'].index, 16)
        self.assertEquals(fields['scannedProdID'].index, 17)
        self.assertEquals(fields['spokenProdID'].index, 18)
        self.assertEquals(fields['description'].index, 19)
        self.assertEquals(fields['size'].index, 20)
        self.assertEquals(fields['UPC'].index, 21)
        self.assertEquals(fields['assignmentID'].index, 22)
        self.assertEquals(fields['idDescription'].index, 23)
        self.assertEquals(fields['deliveryLocation'].index, 24)
        self.assertEquals(fields['combine'].index, 25)
        self.assertEquals(fields['store'].index, 26)
        self.assertEquals(fields['caseLabel'].index, 27)
        self.assertEquals(fields['targetContainer'].index, 28)
        self.assertEquals(fields['captureLot'].index, 29)
        self.assertEquals(fields['lotText'].index, 30)
        self.assertEquals(fields['pickMessage'].index, 31)
        self.assertEquals(fields['verifyLocation'].index, 32)
        self.assertEquals(fields['cycleCount'].index, 33)
        self.assertEquals(fields['serialNumber'].index, 34)
        self.assertEquals(fields['multiItemLocation'].index, 35)
        self.assertEquals(fields['errorCode'].index, 36)
        self.assertEquals(fields['errorMessage'].index, 37)

        fields = self._obj._stop_assignment_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

    def test_regions(self):
        
        #Check that the correct object gets launched and state is set correctly
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut #used to test clear is called on dynamics
        
        self.assertRaises(Launch, self._obj.runState, REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskSingleRegion')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, VALIDATE_REGION)
        self.assertEquals(self._obj.dynamic_vocab.pick_lut, None)
        
    def test_validate_regions(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]

        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj._region_selected = False
        self._obj._valid_regions_lut.receive('-1,not authorized,0,\n\n')

        self._obj.runState(VALIDATE_REGION)

        self.validate_prompts('not authorized for any regions, see your supervisor')
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, False) 
        
        #-----------------------------------------------------------------
        #Test authorized regions auto 1 assignment
        self._obj._region_selected = False
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts('To receive work, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, True) 

        #-----------------------------------------------------------------
        #Test authorized regions auto multiple assignment
        self._obj._region_selected = False
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n\n')
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, True) 

        #-----------------------------------------------------------------
        #Test authorized regions manual 1 assignment
        self._obj._region_selected = False
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, True) 

        #-----------------------------------------------------------------
        #Test authorized regions manual multiple assignment
        self._obj._region_selected = False
        self._obj._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, True) 

        #-----------------------------------------------------------------
        #Test error code 3 in valid region LUT
        self._obj._region_selected = False
        self._obj._valid_regions_lut.receive('1,region 1,3,\n\n')
        
        self.assertRaises(Launch, self._obj.runState, VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, False) 

        #-----------------------------------------------------------------
        #Test error code 3 in region config LUT
        self._obj._region_selected = False
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        
        self.assertRaises(Launch, self._obj.runState, VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, False) 
        
        #-----------------------------------------------------------------
        #Test error code 2 in valid region LUT
        self._obj._region_selected = False
        self._obj.next_state = None
        self._obj._inprogress_work = False
        self._obj._valid_regions_lut.receive('1,region 1,2,In progress work\n\n')
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,0,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        
        self._obj.runState(VALIDATE_REGION)
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        
        self.validate_prompts('getting in progress work')
        self.validate_server_requests()
        self.assertEquals(self._obj._region_selected, True) 
        self.assertEquals(self._obj._inprogress_work, True) 


        #-----------------------------------------------------------------
        #Test change function no
        self._obj.next_state = None
        self.start_server()
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self.post_dialog_responses('change function', 'no')
        
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts('To receive work, say ready',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, VALIDATE_REGION)

        #-----------------------------------------------------------------
        #Test change function yes
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self.post_dialog_responses('change function', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, VALIDATE_REGION)
        
        self.validate_prompts('To receive work, say ready',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #Test change region
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self.post_dialog_responses('change region')
        
        self.assertRaises(Launch, self._obj.runState, VALIDATE_REGION)
        
        self.validate_prompts('To receive work, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        
    def test_get_region_config(self):
        obj = SelectionRegionTask(VoiceLink(), SelectionTask(3))

        self.start_server()

        #---------------------------------------------------------------------------
        #Test function number part of request 
        self.set_server_response('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,0,\n\n', 'prTaskLUTPickingRegion')
        obj.selected_region = 1
        
        obj.runState(GET_REGION_CONFIG)
        
        self.assertEquals(obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTPickingRegion', '1', '3'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #Test Error Code 3 
        self.set_server_response('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,Error Message\n\n', 'prTaskLUTPickingRegion')
        obj.selected_region = 1
        self.post_dialog_responses('ready')
        
        obj.runState(GET_REGION_CONFIG)
        
        self.assertEquals(obj.next_state, '') #Next state should be None
        self.validate_server_requests(['prTaskLUTPickingRegion', '1', '3'])
        self.validate_prompts('Error Message, say ready')
        

    def test_get_valid_regions(self):
        obj = SelectionRegionTask(VoiceLink(), SelectionTask(4))

        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response 1 or more regions
        self.set_server_response('1,region 1,0,\n\n', 'prTaskLUTRegionPermissionsForWorkType')
        
        obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTRegionPermissionsForWorkType', '4'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #Test Error Code 3 
        obj = SelectionRegionTask(VoiceLink(), SelectionTask(4))

        self.set_server_response('1,region 1,3,Error Message\n\n', 'prTaskLUTRegionPermissionsForWorkType')
        self.post_dialog_responses('ready')
        
        obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(obj.next_state, '') #Next state should be None
        self.validate_server_requests(['prTaskLUTRegionPermissionsForWorkType', '4'])
        self.validate_prompts('Error Message, say ready')

        #---------------------------------------------------------------------------
        #Test Error Code 2 
        obj = SelectionRegionTask(VoiceLink(), SelectionTask(4))

        self.set_server_response('1,region 1,2,Warning Message\n\n', 'prTaskLUTRegionPermissionsForWorkType')
        self.post_dialog_responses('ready')
        
        obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTRegionPermissionsForWorkType', '4'])
        self.validate_prompts('Warning Message, say ready')

    def test_get_assignment(self):
        
        #-----------------------------------------------------------------
        #test single assignment auto issuance
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut #used to test clear is called on dynamics
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self.assertRaises(Launch, self._obj.runState, GET_ASSIGNMENT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, GetAssignmentAuto))
        self.assertEquals(self._obj.current_state, CHECK_ASSIGNMENT)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'getAssignmentAuto')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.dynamic_vocab.pick_lut, None)
        
        #-----------------------------------------------------------------
        #test get assignment manual issuance
        self._obj._inprogress_work = True
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut #used to test clear is called on dynamics
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self.assertRaises(Launch, self._obj.runState, GET_ASSIGNMENT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, GetAssignmentManual))
        self.assertEquals(self._obj.current_state, CHECK_ASSIGNMENT)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'getAssignmentManual')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.dynamic_vocab.pick_lut, None)
        self.assertEquals(self._obj._inprogress_work, False)
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj._inprogress_work, True)

        #-----------------------------------------------------------------
        #test reseting region configuration LUT, should not reset when assignment last error = 2
        self._obj._inprogress_work = False
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut #used to test clear is called on dynamics
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n'
                                             '1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._region_config_lut._current_region_rec = 1
        self._obj._assignment_lut.last_error = 2
        
        self.assertRaises(Launch, self._obj.runState, GET_ASSIGNMENT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, GetAssignmentManual))
        self.assertEquals(self._obj.current_state, CHECK_ASSIGNMENT)
        self.assertEquals(1, self._obj._region_config_lut._current_region_rec)
        
        #-----------------------------------------------------------------
        #test reseting region configuration LUT, should not reset when assignment last error = 2
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut #used to test clear is called on dynamics
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n'
                                             '1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._region_config_lut._current_region_rec = 1
        self._obj._assignment_lut.last_error = 0
        
        self.assertRaises(Launch, self._obj.runState, GET_ASSIGNMENT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, GetAssignmentManual))
        self.assertEquals(self._obj.current_state, CHECK_ASSIGNMENT)
        self.assertEquals(0, self._obj._region_config_lut._current_region_rec)
        
    def test_check_assignment(self):
        self._obj._region_config_lut.receive('1,dry grocery,2,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n'
                                             '1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._region_config_lut._current_region_rec = 1
        self._obj._assignment_lut.last_error = 0
        
        #-----------------------------------------------------------------
        #test getting normal assignment with region config available and no error
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj.runState(CHECK_ASSIGNMENT)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test getting chase assignment with region config available and no error
        self._obj._assignment_lut.receive('1,1,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj.runState(CHECK_ASSIGNMENT)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test getting normal assignment with no region config available and no error
        self.start_server()
        self.set_server_response('0,\n\n', 'prTaskLUTCoreSignOff')
        self._obj._region_config_lut.receive('1,dry grocery,2,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.post_dialog_responses('sign off')
        self.assertRaises(Launch, self._obj.runState, CHECK_ASSIGNMENT)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('Assignment received does not match any current region profile, see your supervisor')
        self.validate_server_requests(['prTaskLUTCoreSignOff'])
        
        #-----------------------------------------------------------------
        #test getting chase assignment with no region config available and no error
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._assignment_lut.receive('1,1,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.post_dialog_responses('sign off')
        self.assertRaises(Launch, self._obj.runState, CHECK_ASSIGNMENT)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('Assignment received does not match any current region profile, see your supervisor')
        self.validate_server_requests(['prTaskLUTCoreSignOff'])
        
        #-----------------------------------------------------------------
        #test error code 2 received, and another region available
        self._obj._region_config_lut.receive('1,dry grocery,2,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n'
                                             '1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._assignment_lut.receive('1,1,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,2,Error Message\n\n')
        self._obj._region_config_lut._current_region_rec = 0
        self._obj._assignment_lut.last_error = 2
        self._obj.runState(CHECK_ASSIGNMENT)
        self.assertEqual(self._obj.next_state, GET_ASSIGNMENT)
        self.validate_prompts()
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #test error code 2 received, and no more regions available
        self._obj._region_config_lut.receive('1,dry grocery,2,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n'
                                             '1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._assignment_lut.receive('1,1,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,2,Error Message\n\n')
        self._obj._region_config_lut._current_region_rec = 1
        self._obj._assignment_lut.last_error = 2
        self.post_dialog_responses('ready')
        self._obj.runState(CHECK_ASSIGNMENT)
        self.assertEqual(self._obj.next_state, GET_ASSIGNMENT)
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests()

        
    def test_begin_assignment(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]

        #-----------------------------------------------------------------
        #Test no picks (status all blank)
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive(',0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        
        self._obj.runState(BEGIN_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, PRINT_LABEL) 
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.dynamic_vocab.pick_lut, None)
        
        #-----------------------------------------------------------------
        #Test has picks
        self._obj.next_state = None
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        
        self.assertRaises(Launch, self._obj.runState, BEGIN_ASSIGNMENT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, BeginAssignment))
        self.assertEquals(self._obj.current_state, PICK_ASSIGNMENT)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'beginAssignment')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.dynamic_vocab.pick_lut, self._obj._picks_lut)
                
        #test pick only mode
        self._obj.next_state = None
        self._obj.pick_only = True
        self._obj._region_config_lut[0]['goBackForShorts'] =  1
        self._obj.runState(BEGIN_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_prompts('Picking shorts' )
        self.assertEquals(self._obj._picks_lut[0]['status'], 'N')
        self.validate_server_requests()    
        
        #test pick only mode
        self._obj._picks_lut.receive('S,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        self._obj.next_state = None
        self._obj.pick_only = True
        self._obj._region_config_lut[0]['goBackForShorts'] =  1
        self._obj.runState(BEGIN_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_prompts('Picking skips and shorts' )
        self.validate_server_requests()    
        
    def test_pick_assignment(self):
        #test that pick assignment is launched
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        
        # test that the task is being launched
        self.assertRaises(Launch, self._obj.runState, PICK_ASSIGNMENT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PickAssignmentTask))
        self.assertEquals(self._obj.current_state, PICK_REMAINING)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'pickAssignment')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        
    def test_pick_remaining(self):
        #Not in Go back for shorts or pick  by pick mode
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj.next_state = None
        self._obj.runState(PICK_REMAINING)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #pick by pick mode
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,1,1,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj.runState(PICK_REMAINING)
        self.assertEquals(self._obj.pick_only, True)
        self.assertEquals(self._obj.next_state, GET_ASSIGNMENT)
        self.validate_prompts()
        self.validate_server_requests()
        
        #go back for shorts
        self._obj.pick_only= False
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,1,0,0,,,,,1,0,0,2,0,1,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._picks_lut.receive('P,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        self.post_dialog_responses()
        self._obj.runState(PICK_REMAINING)
        self.assertEquals(self._obj.pick_only, True)
        self.assertEquals(self._obj.next_state, GET_ASSIGNMENT)
        self.validate_prompts('shorts are reported', 
                              'Going back for shorts')
        self.validate_server_requests() 
        
        #go back for shorts and skips
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,1,0,0,,,,,1,0,0,2,0,1,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
        self._obj._picks_lut.receive("P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                               "P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                               "S,0,402,893,1,pre 302,right 402,post 502,slot 891,15,'Pack',893,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0, \n\n")
     
        self.post_dialog_responses()
        self._obj.pick_only= False
        self._obj.runState(PICK_REMAINING)
        self.assertEquals(self._obj.pick_only, True)
        self.assertEquals(self._obj.next_state, GET_ASSIGNMENT)
        self.validate_prompts('shorts are reported', 'Going back for skips and shorts')
        self.validate_server_requests() 
        
    def test_print_label(self):

        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._current_region_rec = self._obj._region_config_lut[0]
                   
        #test picking complete prompt on completing assignment
        self._obj._region_config_lut[0]['pickByPick'] = 1
        self._obj.runState(PRINT_LABEL)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Picking complete')
        self.validate_server_requests()
        
        #test print label at the end of the assignment
        self._obj._region_config_lut[0]['pickByPick'] = 0
        self._obj._region_config_lut[0]['containerType'] = 0
        self._obj._region_config_lut[0]['printLabels'] = '2'
        self.assertRaises(Launch, self._obj.runState, PRINT_LABEL)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, DELIVER_ASSIGNMENT)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')
                        
    def test_deliver_assignment(self):
        # test set data
        self._obj._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,3,\n\n')
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut #used to test clear is called on dynamics
        self._obj._current_region_rec = self._obj._region_config_lut[0]
         
        # test that the task is being launched
        self.assertRaises(Launch, self._obj.runState, DELIVER_ASSIGNMENT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, DeliverAssignmentTask))
        self.assertEquals(self._obj.current_state, DELIVER_ASSIGNMENT)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'deliverAssignment')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
  
        #test target container and deliver at close
        self._obj._picks_lut[0]['targetContainer'] = 1
        self._obj._region_config_lut[0]['delContWhenClosed'] = 1
        self._obj.runState(DELIVER_ASSIGNMENT)
        self.assertEquals(self._obj.next_state, None)


        #test target containers
        self._obj._region_config_lut[0]['delContWhenClosed'] = '1'
        self._obj._picks_lut[0]['targetContainer'] = 1
        self._obj._region_config_lut[0]['pickByPick'] = 0
        self._obj.runState(DELIVER_ASSIGNMENT)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        
        #test pass assignments
        self._obj._region_config_lut[0]['delContWhenClosed'] = '0'
        self._obj._picks_lut[0]['targetContainer'] = 0
        self._obj._assignment_lut[0]['passAssignment'] = '1'
        self._obj.dynamic_vocab._pass_inprogress = True
        self._obj.runState(DELIVER_ASSIGNMENT)
        self.assertEquals(self._obj.next_state, None)
        
        
    def test_complete_assignment(self):
        # test that pick assignment is launched
        self._obj.dynamic_vocab.pick_lut = self._obj._picks_lut #used to test clear is called on dynamics
        self._obj.dynamic_vocab.current_picks = self._obj._picks_lut
        self._obj.dynamic_vocab.previous_picks = self._obj._picks_lut
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')

        # Test no running server
        self.post_dialog_responses('ready')
        #self.set_server_response('0,0,\n\n', 'prTaskLUTCoreSignOn')
         
        self._obj.runState(COMPLETE_ASSIGNMENT)

        #RALLYTC 1215 - Verify the LUT is resent if a timeout occurs        
        self.assertEquals(self._obj.next_state, COMPLETE_ASSIGNMENT) 
        self.validate_prompts("Error contacting host,  to try again say ready")
        self.validate_server_requests()
        self.assertEquals(self._obj.dynamic_vocab.pick_lut, None)
        self.assertEquals(self._obj.dynamic_vocab.current_picks, [])
        self.assertEquals(self._obj.dynamic_vocab.previous_picks, self._obj._picks_lut)

        # RALLYTC 1217 - Verify the user hears an error message on failure
        # Test error response
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response('1,Error Message\n\n')
         
        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, COMPLETE_ASSIGNMENT) 
        self.validate_prompts("Error Message, To continue say ready")
        self.validate_server_requests(['prTaskLUTStopAssignment','1'])

        # Test change_region error response
        #RALLYTC 1220 - Verify the user is directed to change regions after completion.
        self._obj._assignment_lut.receive('2,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        self.set_server_response('2,Error Message\n\n')
         
        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, REGIONS) 
        self.validate_prompts("Error Message, To continue say ready")
        self.validate_server_requests(['prTaskLUTStopAssignment','2'])

        # Test success response
        #RALLYTC 1218 - Verify the correct success response is spoken on success
        #RALLYTC 1216 - Verify correct fields (the error code/message are the only ones)
        self._obj.next_state = None
        self.assertEquals(self._obj.dynamic_vocab.previous_picks, self._obj._picks_lut)
        self._obj._assignment_lut.receive('0,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        #self.set_server_response('0,\n\n')
         
        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.validate_server_requests(['prTaskLUTStopAssignment','0'])
        self.assertEquals(self._obj.dynamic_vocab.previous_picks, [])
        self.assertEquals(self._obj.next_state, None)
         
        #tests for pass
        self.post_dialog_responses('ready')
        self.set_server_response('0,\n\n')
        self._obj.dynamic_vocab._pass_inprogress = True
        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_server_requests(['prTaskLUTStopAssignment','0'])
        self.assertEquals(self._obj.dynamic_vocab.previous_picks, [])
        self.assertEquals(self._obj._assignment_complete_prompt_key,
                          'selection.complete.assignment.confirm')

        #tests for pass
        self.post_dialog_responses('ready')
        self.set_server_response('0,\n\n')
        self._obj.dynamic_vocab._pass_inprogress = True
        self._obj._assignment_lut[0]['passAssignment'] = '1'
        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_server_requests(['prTaskLUTPassAssignment','0'])
        self.assertEquals(self._obj.dynamic_vocab.previous_picks, [])
        self.assertEquals(self._obj._assignment_complete_prompt_key,
                          'selection.pass.assignment.confirm')


    def test_prompt_assignment_complete(self):
        self.start_server()
        
        #----------------------------------------------------------------------
        #Test the complete assignment prompt
        self._obj.next_state = None
        self._obj._assignment_complete_prompt_key = 'selection.complete.assignment.confirm'
        self.post_dialog_responses('ready')
        
        self._obj.runState(PROMPT_ASSIGNMENT_COMPLETE)
        
        self.validate_server_requests()
        self.validate_prompts("Assignment complete.  For next assignment, say ready")
        self.assertEquals(self._obj.next_state, GET_ASSIGNMENT)
        
        #----------------------------------------------------------------------
        #Test the pass assignment prompt
        self._obj.next_state = None
        self._obj._assignment_complete_prompt_key = 'selection.pass.assignment.confirm'
        self.post_dialog_responses('ready')
        
        self._obj.runState(PROMPT_ASSIGNMENT_COMPLETE)
        
        self.validate_server_requests()
        self.validate_prompts("Assignment passed.  For next assignment, say ready")
        self.assertEquals(self._obj.next_state, GET_ASSIGNMENT)
        
        #-----------------------------------------------------------------------
        # Test Change Function no
        self._obj.next_state = None        
        self._obj._assignment_complete_prompt_key = 'selection.complete.assignment.confirm'
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')
        
        self._obj.runState(PROMPT_ASSIGNMENT_COMPLETE)
        
        self.assertEquals(self._obj.next_state, PROMPT_ASSIGNMENT_COMPLETE) 
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts('Assignment complete.  For next assignment, say ready',
                      'change function, correct?')
        
        #-----------------------------------------------------------------------
        # Test Change Function yes
        self._obj.next_state = None        
        self._obj._assignment_complete_prompt_key = 'selection.complete.assignment.confirm'
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, PROMPT_ASSIGNMENT_COMPLETE)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts('Assignment complete.  For next assignment, say ready',
                      'change function, correct?')
        
        #-----------------------------------------------------------------------
        # Test Change Region
        self._obj.next_state = None
        self._obj._region_selected = True
        self.set_server_response('0,Assignment complete\n\n')
        self.post_dialog_responses('change region')
        
        self.assertRaises(Launch, self._obj.runState, PROMPT_ASSIGNMENT_COMPLETE)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_prompts('Assignment complete.  For next assignment, say ready')
        self.validate_server_requests()
        
    def test_change_region(self):
        #TESTLINK 95630 :: Tests for Change Region
        
        #---------------------------------------------------------------
        #Test no region currently selected
        self._obj._region_selected = False
        
        self._obj.change_region()
        
        self.validate_prompts() 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected
        self._obj._region_selected = True
        
        self.assertRaises(Launch, self._obj.change_region)
        
        self.validate_prompts() 
        self.validate_server_requests()
        
if __name__ == '__main__':
    unittest.main()


