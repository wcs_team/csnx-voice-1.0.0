from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from selection.SelectionTask import SelectionTask
from selection.PickAssignment import PickAssignmentTask
from selection.PickPrompt import PickPromptTask
from selection.VarWeightsAndSerialNum import WeightsSerialNumbers,\
    CAPTURE_WEIGHT, CAPTURE_SERIAL_NUMBER, CHECK_MORE

class testVarWeighsAndSerialNum(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        selection = obj_factory.get(SelectionTask, 3, VoiceLink())
        selection._region_config_lut.receive('1,dry grocery,1,1,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,0,\n\n')
        selection._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        selection._picks_lut.receive('N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                     'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                     'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                     'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                     '\n')
        pick_assignment = obj_factory.get(PickAssignmentTask, 
                                            selection.taskRunner, selection)
        pick_assignment.configure(selection._region_config_lut[0], 
                                  selection._assignment_lut, 
                                  selection._picks_lut,
                                  selection._container_lut,
                                  False)
        
        self.pick_prompt = obj_factory.get(PickPromptTask,
                                             pick_assignment._region, 
                                             pick_assignment._assignment_lut, 
                                             pick_assignment._picks_lut,
                                             pick_assignment._container_lut,
                                             False,
                                             pick_assignment.taskRunner, 
                                             pick_assignment)

        self._obj = obj_factory.get(WeightsSerialNumbers,
                                      20, 
                                      self.pick_prompt._picks, 
                                      self.pick_prompt.taskRunner, 
                                      self.pick_prompt)

    def test_initialize(self):
        self.assertEquals(self._obj.name, 'weightsSerialNumbers')

        #test states
        self.assertEquals(self._obj.states[0], CAPTURE_WEIGHT)
        self.assertEquals(self._obj.states[1], CAPTURE_SERIAL_NUMBER)
        self.assertEquals(self._obj.states[2], CHECK_MORE)

    def test_capture_weight(self):
        
        #-----------------------------------------------------------------
        #TESTLINK 95372 :: Not a weight item, only serial number
        #Test not a weight item
        del self._obj._weights[:]
        self._obj.next_state = None
        for pick in self._obj._picks:
            pick['variableWeight'] = '0'
            pick['variableWeightMin'] = '5.0'
            pick['variableWeightMax'] = '5.5'
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._weights), 0) 
        self.validate_prompts()
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95358 :: Step 2 - Valid weight
        #Test valid weight item
        del self._obj._weights[:]
        self._obj.next_state = None
        for pick in self._obj._picks:
            pick['variableWeight'] = '1'
        
        self.post_dialog_responses('5.20!')

        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._weights), 1)
        self.assertEquals(self._obj._weights[0], 5.2) 
        self.validate_prompts('weight 1 of 20')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95358 :: Step 1 - Quantity 0
        #Test weight 0
        del self._obj._weights[:]
        self._obj.next_state = None
        self.post_dialog_responses('0.00!')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 0)
        self.validate_prompts('weight 1 of 20',
                              'you must enter a weight greater than zero, try again')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        # TESTLINK 95370 : weight out of range
        #Test weight out of range low, no override
        del self._obj._weights[:]
        self._obj.next_state = None
        self.post_dialog_responses('4.99!', 'ready')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 0)
        self.validate_prompts('weight 1 of 20',
                              '4.99 out of range')
        self.validate_server_requests()


        #-----------------------------------------------------------------
        # TESTLINK 95370 : weight out of range
        #Test weight out of range high, no override
        del self._obj._weights[:]
        self._obj.next_state = None
        self.post_dialog_responses('5.51', 'ready')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 0)
        self.validate_prompts('weight 1 of 20',
                              '5.51 out of range')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        # TESTLINK 95370 : weight out of range
        #Test weight out of range with override
        del self._obj._weights[:]
        self._obj.next_state = None
        self.post_dialog_responses('5.51', 'description', 'override', 'yes')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._weights), 1)
        self.assertEquals(self._obj._weights[0], 5.51) 
        self.validate_prompts('weight 1 of 20',
                              '5.51 out of range',
                              'minimum is 5.0, maximum is 5.5',
                              '5.51 out of range',
                              'override, correct?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95358 :: Step 1 - Undo Entry not available
        #Test undo entry not available
        del self._obj._weights[:]
        self._obj.next_state = None
        self.post_dialog_responses('undo entry')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 0)
        self.validate_prompts('weight 1 of 20',
                              'not valid at this time')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test undo entry available, with continue
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        
        self.post_dialog_responses('undo entry', 'continue')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('weight 3 of 20',
                              'last entry, all entries, or continue?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        # TESTLINK 95368 :: Test undo last 
        #Test undo entry available, with last entry
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        
        self.post_dialog_responses('undo entry', 
                                   'last entry', 
                                   'no', 
                                   'last entry', 
                                   'yes')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 1)
        self.assertEquals(self._obj._current_capture, 2)
        self.validate_prompts('weight 3 of 20',
                              'last entry, all entries, or continue?',
                              're enter item 2, correct?',
                              'last entry, all entries, or continue?',
                              're enter item 2, correct?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test undo entry available, with all entries
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        
        self.post_dialog_responses('undo entry', 
                                   'all entries', 
                                   'no', 
                                   'all entries', 
                                   'yes')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 0)
        self.assertEquals(self._obj._current_capture, 1)
        self.validate_prompts('weight 3 of 20',
                              'last entry, all entries, or continue?',
                              're enter all items, correct?',
                              'last entry, all entries, or continue?',
                              're enter all items, correct?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95358 :: Step 1 - review last not available
        #Test review last, not available
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._current_capture = 1
        
        self.post_dialog_responses('review last')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 0)
        self.assertEquals(self._obj._current_capture, 1)
        self.validate_prompts('weight 1 of 20',
                              'not valid at this time')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95366 :: Review Last
        #Test review last available
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        
        self.post_dialog_responses('review last', 'ready')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('weight 3 of 20',
                              'Unit 2, weight was 5.2, say ready')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95362 :: Test which item
        #Test which item
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        
        self.post_dialog_responses('which item')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('weight 3 of 20')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test short product not allowed
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        for pick in self._obj._picks:
            pick['captureLot'] = '1'
        
        self.post_dialog_responses('short product')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('weight 3 of 20',
                              'short product not allowed')
        self.validate_server_requests()
        for pick in self._obj._picks:
            pick['captureLot'] = '0'
        
        #-----------------------------------------------------------------
        #TESTLINK 95358 :: Step 1 - Short with No
        #TESTLINK 95388 :: Short product weights
        #Test short product no
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        
        self.post_dialog_responses('short product', 'no')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._weights), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('weight 3 of 20',
                              'you picked 2 of 20 items, is this a short product?')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #TESTLINK 95388 :: Short product weights
        #Test short product yes
        del self._obj._weights[:]
        self._obj.next_state = None
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 3
        
        self.post_dialog_responses('short product', 'yes')
        
        self._obj.runState(CAPTURE_WEIGHT)
        
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(len(self._obj._weights), 2)
        self.assertEquals(self._obj._current_capture, 2)
        self.assertEquals(self._obj._quantity_to_capture, 2)

        self.assertEquals(self._obj.callingTask._picked_quantity, 2)
        self.assertEquals(self._obj.callingTask._lot_quantity, 2)
        self.assertEquals(self._obj.callingTask._put_quantity, 2)
        
        self.validate_prompts('weight 3 of 20',
                              'you picked 2 of 20 items, is this a short product?')
        self.validate_server_requests()
        
    def test_capture_serial_number(self):
        
        #-----------------------------------------------------------------
        #Test not a serial number item
        del self._obj._weights[:]
        self._obj.next_state = None
        for pick in self._obj._picks:
            pick['serialNumber'] = '0'
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._serial_numbers), 0) 
        self.validate_prompts()
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test valid serial number
        del self._obj._weights[:]
        self._obj.next_state = None
        for pick in self._obj._picks:
            pick['serialNumber'] = '1'
        
        self.post_dialog_responses('11111',
                                   'cancel',
                                   '12345',
                                   'ready')

        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._serial_numbers), 1)
        self.assertEquals(self._obj._serial_numbers[0], '12345') 
        self.validate_prompts('serial number 1 of 20',
                              'serial number 1 of 20') #prompt repeated after cancel
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test undo entry not available
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self.post_dialog_responses('undo entry')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_SERIAL_NUMBER)
        self.assertEquals(len(self._obj._serial_numbers), 0)
        self.validate_prompts('serial number 1 of 20',
                              'not valid at this time')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95380 :: Undo Entry - Serial Number
        #Test undo entry available, with continue
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3
        
        self.post_dialog_responses('undo entry', 'continue')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_SERIAL_NUMBER)
        self.assertEquals(len(self._obj._serial_numbers), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('serial number 3 of 20',
                              'last entry, all entries, or continue?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95380 :: Undo Entry - Serial Number
        #TESTLINK 95386 :: Undo Entry - Weight and serial number
        #Test undo entry available, with last entry
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3

        del self._obj._weights[:]
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._weights.append(5.3)
        
        self.post_dialog_responses('undo entry', 
                                   'last entry', 
                                   'no', 
                                   'last entry', 
                                   'yes')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._serial_numbers), 1)
        self.assertEquals(len(self._obj._weights), 1)
        self.assertEquals(self._obj._serial_numbers[0], '12345')
        self.assertEquals(self._obj._weights[0], 5.1)
        self.assertEquals(self._obj._current_capture, 2)
        self.validate_prompts('serial number 3 of 20',
                              'last entry, all entries, or continue?',
                              're enter item 2, correct?',
                              'last entry, all entries, or continue?',
                              're enter item 2, correct?')
        self.validate_server_requests()

        del self._obj._weights[:]

        #-----------------------------------------------------------------
        #TESTLINK 95380 :: Undo Entry - Serial Number
        #Test undo entry available, with all entries
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3
        
        self.post_dialog_responses('undo entry', 
                                   'all entries', 
                                   'no', 
                                   'all entries', 
                                   'yes')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(len(self._obj._serial_numbers), 0)
        self.assertEquals(self._obj._current_capture, 1)
        self.validate_prompts('serial number 3 of 20',
                              'last entry, all entries, or continue?',
                              're enter all items, correct?',
                              'last entry, all entries, or continue?',
                              're enter all items, correct?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test review last, not available
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._current_capture = 1
        
        self.post_dialog_responses('review last')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_SERIAL_NUMBER)
        self.assertEquals(len(self._obj._serial_numbers), 0)
        self.assertEquals(self._obj._current_capture, 1)
        self.validate_prompts('serial number 1 of 20',
                              'not valid at this time')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95366 :: Review Last
        #TESTLINK 95378 :: Review last - Serial Number
        #Test review last available
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3
        
        self.post_dialog_responses('review last', 'ready')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_SERIAL_NUMBER)
        self.assertEquals(len(self._obj._serial_numbers), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('serial number 3 of 20',
                              'Unit 2, serial number was <spell>67890</spell>, say ready')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95366 :: Review Last - Weights
        #TESTLINK 95378 :: Review last - Serial Number
        #TESTLINK 95384 :: Review Last - Weight and serial number
        
        #Test review last available (with weights)
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3

        del self._obj._weights[:]
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._weights.append(5.3)
        
        self.post_dialog_responses('review last', 'ready')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_SERIAL_NUMBER)
        self.assertEquals(len(self._obj._serial_numbers), 2)
        self.assertEquals(len(self._obj._weights), 3)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('serial number 3 of 20',
                              'Unit 2, weight was 5.2, serial number was <spell>67890</spell>, say ready')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        # TESTLINK 9537 :: Serial Number which item
        #Test which item
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3
        
        self.post_dialog_responses('which item')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_SERIAL_NUMBER)
        self.assertEquals(len(self._obj._serial_numbers), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('serial number 3 of 20')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #TESTLINK 95390 :: Short product serial numbers
        #Test short product no
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3
        
        self.post_dialog_responses('short product', 'no')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, CAPTURE_SERIAL_NUMBER)
        self.assertEquals(len(self._obj._serial_numbers), 2)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('serial number 3 of 20',
                              'you picked 2 of 20 items, is this a short product?')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #TESTLINK 95390 :: Short product serial numbers
        #Test short product yes
        del self._obj._serial_numbers[:]
        self._obj.next_state = None
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')
        self._obj._current_capture = 3
        
        del self._obj._weights[:]
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._weights.append(5.3)

        self.post_dialog_responses('short product', 'yes')
        
        self._obj.runState(CAPTURE_SERIAL_NUMBER)
        
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(len(self._obj._serial_numbers), 2)
        self.assertEquals(len(self._obj._weights), 2)
        self.assertEquals(self._obj._current_capture, 2)
        self.assertEquals(self._obj._quantity_to_capture, 2)

        self.assertEquals(self._obj.callingTask._picked_quantity, 2)
        self.assertEquals(self._obj.callingTask._lot_quantity, 2)
        self.assertEquals(self._obj.callingTask._put_quantity, 2)
        
        self.validate_prompts('serial number 3 of 20',
                              'you picked 2 of 20 items, is this a short product?')
        self.validate_server_requests()
        
    def test_check_more(self):
        #-----------------------------------------------------------------
        #test not all entered
        self._obj.next_state = None
        self._obj._current_capture = 1

        self._obj.runState(CHECK_MORE)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(self._obj._current_capture, 2)
        self.validate_prompts()
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        # TESTLINK 95360 :: Test all entered 
        #test all entered
        self._obj.next_state = None
        del self._obj._serial_numbers[:]
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')

        del self._obj._weights[:]
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 20

        self.post_dialog_responses('ready')

        self._obj.runState(CHECK_MORE)
        
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._current_capture, 21)
        self.validate_prompts('Entries complete')
        self.validate_server_requests()
        
        self.assertEquals(len(self._obj.callingTask._weights), 2)
        self.assertEquals(len(self._obj.callingTask._serial_numbers), 2)
        
        #-----------------------------------------------------------------
        # TESTLINK 95368 :: Test undo last 
        #test all entered, undo entry
        self._obj.next_state = None
        del self._obj._serial_numbers[:]
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')

        del self._obj._weights[:]
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 2
        self._obj._quantity_to_capture = 2

        self._obj.callingTask._weights = []
        self._obj.callingTask._serial_numbers = []
        
        self.post_dialog_responses('undo entry', 'last entry', 'yes')

        self._obj.runState(CHECK_MORE)
        
        self.assertEquals(self._obj.next_state, CAPTURE_WEIGHT)
        self.assertEquals(self._obj._current_capture, 2)
        self.validate_prompts('Entries complete',
                              'last entry, all entries, or continue?',
                              're enter item 2, correct?')
        self.validate_server_requests()
        
        self.assertEquals(len(self._obj.callingTask._weights), 0)
        self.assertEquals(len(self._obj.callingTask._serial_numbers), 0)
        
        #-----------------------------------------------------------------
        # TESTLINK 95360 :: Test all entered, review last 
        #TESTLINK 95366 :: Review Last
        #test all entered, review last
        self._obj.next_state = None
        del self._obj._serial_numbers[:]
        self._obj._serial_numbers.append('12345')
        self._obj._serial_numbers.append('67890')

        del self._obj._weights[:]
        self._obj._weights.append(5.1)
        self._obj._weights.append(5.2)
        self._obj._current_capture = 2
        self._obj._quantity_to_capture = 2

        self._obj.callingTask._weights = []
        self._obj.callingTask._serial_numbers = []
        
        self.post_dialog_responses('review last', 'ready')

        self._obj.runState(CHECK_MORE)
        
        self.assertEquals(self._obj.next_state, CHECK_MORE)
        self.assertEquals(self._obj._current_capture, 3)
        self.validate_prompts('Entries complete',
                              'Unit 2, weight was 5.2, serial number was <spell>67890</spell>, say ready')
        self.validate_server_requests()
        
        self.assertEquals(len(self._obj.callingTask._weights), 0)
        self.assertEquals(len(self._obj.callingTask._serial_numbers), 0)
        
                