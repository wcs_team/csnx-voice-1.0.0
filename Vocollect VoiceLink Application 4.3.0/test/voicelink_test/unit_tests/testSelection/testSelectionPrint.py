from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.SelectionTask import SelectionTask
from selection.SelectionLuts import Assignments, RegionConfig
from core.VoiceLink import VoiceLink
from selection.SelectionPrint import SelectionPrintTask, ENTER_PRINTER,\
    CONFIRM_PRINTER, PRINT_LABELS, REPRINT_LABELS
import unittest

############################################
# Test for selection print class              #
############################################
class testSelectionPrint(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        assignment = None
        self._obj = obj_factory.get(SelectionPrintTask,
                                      sel._region_config_lut, 
                                      assignment, 
                                      sel._container_lut, 
                                      0, 
                                      VoiceLink(), sel)
        
        self._obj.taskRunner._append(self._obj)
        self._obj.taskRunner._append(self._obj.callingTask)
        
        self.tempAssignLut = Assignments("prTaskLUTGetAssignment")
        self.tempAssignLut.receive("1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n")
        
        self.tempRegionLut = RegionConfig("prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
    
        #region initialize
       
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'selectionPrint')

        #test states
        self.assertEquals(self._obj.states[0], ENTER_PRINTER)
        self.assertEquals(self._obj.states[1], CONFIRM_PRINTER)
        self.assertEquals(self._obj.states[2], PRINT_LABELS)
        self.assertEquals(self._obj.states[3], REPRINT_LABELS)
        
    def test_enter_printer(self):
        
        #test printer not set
        self._obj.task.printer_number = None
        self.post_dialog_responses('20')
        self._obj.runState(ENTER_PRINTER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        self.validate_prompts('Printer?')
        self.assertEquals(self._obj.task.printer_number, '20')

        #test printer already set
        self._obj.runState(ENTER_PRINTER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.task.printer_number, '20')
        
        self._obj._assignment = self.tempAssignLut[0]   
        self._obj._assignment['isChase'] = '1'
        self._obj._set_variables()
        self.assertEquals(self._obj.operation, 0)
        self.assertEquals(self._obj.is_chase, True)
        self._obj._assignment['isChase'] = '0'
        
    def test_confirm_printer(self):

        #confirm printer no
        #ID 95693 :: Test Case Verify that the VoiceLink application remembers selected printer number
        self._obj.task.printer_number = '12'
        self.post_dialog_responses('no')
        self._obj.runState(CONFIRM_PRINTER)
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        self.validate_prompts('printer <spell>12</spell>, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.task.printer_number, None)
         
        #confirm printer yes
        self._obj.next_state = None
        self._obj.task.printer_number = '12'
        self.post_dialog_responses('yes')
        self._obj.runState(CONFIRM_PRINTER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('printer <spell>12</spell>, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.task.printer_number, '12')
        
    def test_print_it(self):   
        #test print
        self._obj._assignment = self.tempAssignLut[0]   
        self._obj._assignment.parent = self.tempAssignLut
        self._obj._region = self.tempRegionLut[0]
        
        self.start_server()
        
        #TESTLINK ID 95601 :: Test Case Verify correct behavior if containers are not used.
        self._obj.task.printer_number = '15'
        self._obj.runState(PRINT_LABELS)
        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPrint','1','12345','1','','15','0'] )
        self.assertEquals(self._obj.callingTask.printer_number, '15')
        
        #test print error.
        self._obj.task.printer_number = '15'
        self.post_dialog_responses('ready')
        self.set_server_response('199,Error message\n\n')
        self._obj.runState(PRINT_LABELS)
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        self.validate_prompts('Error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPrint','1','12345','1','','15','0'] )
        self.assertEquals(self._obj.callingTask.printer_number, '15')
         
        #test print error.
        self._obj.task.printer_number = '15'
        self.post_dialog_responses('ready')
        self.set_server_response('-1,Error message\n\n')
        self._obj.runState(PRINT_LABELS)
        self.assertEquals(self._obj.next_state, PRINT_LABELS)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests(['prTaskLUTPrint','1','12345','1','','15','0'] )
        self.assertEquals(self._obj.callingTask.printer_number, '15')
        
        self._obj._reprint_label = True
        self._obj.runState(PRINT_LABELS)
        self.assertEquals(self._obj.next_state, REPRINT_LABELS)
        self.validate_prompts()
        self.validate_server_requests()
        
        #test multiple assignments
        self._obj.next_state=''
        self._obj._reprint_label = 0
        self.tempAssignLut.receive("1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\
               1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n")
        self._obj._assignment.parent = self.tempAssignLut
        self._obj.task.printer_number = '15'
        self._obj.runState(PRINT_LABELS)
        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPrint','1','','1','','15','0'] )
        self.assertEquals(self._obj.callingTask.printer_number, '15')
        
    def test_reprint_labels(self):
        ''' Reprint labels'''
        self._obj._assignment = self.tempAssignLut[0]  
        self._obj._region = self.tempRegionLut[0]
        self._obj._assignment.parent = self.tempAssignLut
        self.start_server()
    
        #test for chase assignment container is not prompted and directly goes to printing
        #TESTLINK 96391 :: Test Case Reprint if assignment is a chase.
        self._obj.task.printer_number = '8'
        self._obj._assignment['isChase']= '1'
        self._obj.runState(REPRINT_LABELS)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPrint','1','12345','1','','8','0'] )
        self.assertEquals(self._obj.callingTask.printer_number, '8')
        self.assertEquals(self._obj.next_state, None)

        #test for chase assignment container is not prompted and directly goes to printing
        #TESTLINK 96393 :: Test Case Reprint labels if container type is none.
        self._obj._assignment['isChase']= 0
        self._obj._region['containerType'] = 0
        self._obj.runState(REPRINT_LABELS)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPrint','1','12345','1','','8','0'] )
        self.assertEquals(self._obj.callingTask.printer_number, '8')
        self.assertEquals(self._obj.next_state, None)
        
        #container type pick to containers
        #TESTLINK 96401 :: Test Case Verify for if container ID can be spoken & errr msg for incorrect id
        self._obj._region['containerType'] = 1
        self.post_dialog_responses('99', 'yes')
        self._obj.runState(REPRINT_LABELS)
        self.validate_prompts('Container?',
                              '99, correct?', 
                              'Invalid <spell>99</spell>, Try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, REPRINT_LABELS)
        
        #TESTLINK 96395 :: Test Case Verify if All can be spoken when printing a container.
        self._obj.next_state = ''
        self.post_dialog_responses('all', 'yes')
        self._obj.runState(REPRINT_LABELS)
        self.validate_prompts('Container?', 'All correct?')
        self.validate_server_requests(['prTaskLUTPrint','1','','1','','8','0'] )
        self.assertEquals(self._obj.next_state, '')
        
        self.post_dialog_responses('all', 'no')
        self._obj.runState(REPRINT_LABELS)
        self.validate_prompts('Container?', 'All correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, REPRINT_LABELS)
       
        #TESTLINK 96397 :: Test Case Verify if No More can be spoken 
        self.post_dialog_responses('no more')
        self._obj.runState(REPRINT_LABELS)
        self.validate_prompts('Container?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')

        self._obj._container_lut.receive('1,0000000003,03,12345,Store 123,,O,0,0,\n2,0000000002,02,12346,Store 123,,O,0,0,\n\n')
        self.set_server_response('12, error\n\n')
        self.post_dialog_responses('03', 'yes', 'ready')
        self._obj.runState(REPRINT_LABELS)
        self.validate_prompts('Container?',
                              '03, correct?', 
                              'error, To continue say ready')
        self.validate_server_requests(['prTaskLUTPrint','1','12345','1','1','8','0'] )
        self.assertEquals(self._obj.next_state, '')
        
        # Test Case Reprint labels if container type is none and multiple assignments.
        self._obj._assignment['isChase']= 0
        self._obj._region['containerType'] = 0
        self.tempAssignLut.receive("1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\
               1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n")
        self._obj._assignment.parent = self.tempAssignLut
     
        self._obj.runState(REPRINT_LABELS)
        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPrint','1','','1','','8','0'] )
        self.assertEquals(self._obj.callingTask.printer_number, '8')
        
    def has_multiple_assignments(self):
        return False
        
if __name__ == '__main__':
    unittest.main()
        