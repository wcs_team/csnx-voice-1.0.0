from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from selection.PickAssignment import PickAssignmentTask
from selection.SelectionTask import SelectionTask
from vocollect_core.task.task_runner import Launch
from selection.PickPrompt import CHECK_TARGET_CONTAINER, CLOSE_TARGET_CONT
from selection.PickPromptMultiple import PickPromptMultipleTask
import unittest



#=====================================================
#Base Class for prompt tests
#=====================================================
class PickPromptTargetcontainerTests(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        sel._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        sel._picks_lut.receive("N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,message,0,0,0,0,0,\n"
                               "N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,message,0,0,0,0,0,\n\n"
                               "N,0,1,L2,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,message,0,0,0,0,0,\n\n"
                               "N,0,1,L3,1,,A 1,,S 1,5,,ITEM13,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,message,0,0,0,0,0,\n\n"
                               "N,0,1,L4,1,,A 1,,S 1,5,,ITEM14,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,message,0,0,0,0,0,\n\n"
                               );
        
        self.pickassign = obj_factory.get(PickAssignmentTask,
                                            sel.taskRunner, sel)

        self.pickassign.configure(sel._region_config_lut[0],
                                  sel._assignment_lut,
                                  sel._picks_lut,
                                  sel._container_lut,
                                  False)
        
        self._obj = obj_factory.get(PickPromptMultipleTask,
                                      self.pickassign._region,
                                      self.pickassign._assignment_lut, 
                                      self.pickassign._pickList,
                                      self.pickassign._container_lut,
                                      False,
                                      self.pickassign.taskRunner,
                                      self.pickassign)

        self._obj._picks.append(sel._picks_lut[0]) 
        self._obj._picks.append(sel._picks_lut[1])
        
    
    def testCheckTargetcontainer(self):
        #test not picking target containers
        self._obj.runState(CHECK_TARGET_CONTAINER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #test target containers, and current container active
        self._obj._picks[0]['targetContainer'] = 1
        self._obj._picks[1]['targetContainer'] = 1
        self._obj._assignment_lut[0]['activeTargetContainer'] = 1
        self._obj.runState(CHECK_TARGET_CONTAINER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()

        #test target containers, and current container does not exist
        self._obj._picks[0]['targetContainer'] = 1
        self._obj._picks[1]['targetContainer'] = 1
        self._obj._assignment_lut[0]['activeTargetContainer'] = 2
        self.assertRaises(Launch, self._obj.runState, CHECK_TARGET_CONTAINER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()

        #test target containers, and current container does not exist
        self._obj._container_lut.receive('1,003,03,12345,Store 123,1,O,0,0,\n\n')
        self._obj._picks[0]['targetContainer'] = 1
        self._obj._picks[1]['targetContainer'] = 1
        self._obj._assignment_lut[0]['activeTargetContainer'] = 2
        self._obj.runState(CHECK_TARGET_CONTAINER)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('reopen 03')
        self.validate_server_requests()

    def testCloseTargetContainer(self):
        self._obj._puts.append(self._obj._picks[0])
        self._obj._puts.append(self._obj._picks[1])
        
        #test not picking target containers
        self._obj.runState(CLOSE_TARGET_CONT)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #test not all picks picked for container
        self._obj._picks[0]['targetContainer'] = 1
        self._obj._picks[1]['targetContainer'] = 1
        self._obj._picks[0]['status'] = 'P'
        self._obj._picks[1]['status'] = 'P'
        self._obj._picks[0]['qtyPicked'] = self._obj._picks[0]['qtyToPick']
        self._obj._picks[1]['qtyPicked'] = self._obj._picks[1]['qtyToPick']
        self._obj.callingTask._picks_lut[2]['targetContainer'] = 1
        self._obj.runState(CLOSE_TARGET_CONT)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #test all picked, but short occurred and go back for shorts is on
        self._obj.callingTask._picks_lut[2]['targetContainer'] = 2
        self._obj._region['goBackForShorts'] = True
        self._obj._picks[1]['qtyPicked'] = 0
        self._obj.runState(CLOSE_TARGET_CONT)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #test all picked, but short occurred and go back for shorts is off
        self._obj._region['goBackForShorts'] = False
        self._obj._picks[1]['qtyPicked'] = 0
        self.assertRaises(Launch, self._obj.runState, CLOSE_TARGET_CONT)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #test all picked, no shorts 
        self._obj._region['goBackForShorts'] = False
        self._obj._picks[1]['qtyPicked'] = self._obj._picks[1]['qtyToPick']
        self.assertRaises(Launch, self._obj.runState, CLOSE_TARGET_CONT)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
if __name__ == '__main__':
    unittest.main()

