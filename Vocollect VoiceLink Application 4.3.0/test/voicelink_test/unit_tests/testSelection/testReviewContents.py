from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.SelectionTask import SelectionTask
from common.VoiceLinkLut import VoiceLinkLut
from core.VoiceLink import VoiceLink
from selection.ReviewContents import ReviewContents, REVIEW_PROMPT_FOR_CONTAINER, \
    XMIT_CONTAINER_REVIEW, REVIEW_CONTAINER

##########################################
# Test Close Container                   #
##########################################
class TestReviewContents(BaseVLTestCase):

    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        
        self._obj = obj_factory.get(ReviewContents,
                                      sel._region_config_lut, 
                                      sel._assignment_lut,
                                      sel._container_lut,
                                      sel.taskRunner,
                                      sel)
    
        self.tempRegionLut = obj_factory.get(VoiceLinkLut, "prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,1,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        
        self.tempAssignmentLut = obj_factory.get(VoiceLinkLut, "prTaskLUTGetAssignment")
        self.tempAssignmentLut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._container_lut.receive('1,0000000003,03,12345,Store 123,,O,0,0,\n2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self._obj._region =self.tempRegionLut[0]
        self._obj._assignment = self.tempAssignmentLut[0]
   
    def test_initializeStates(self):
        ''' Initialize States'''
              
        #test name
        self.assertEquals(self._obj.name, 'reviewContents')

        #test states
        self.assertEquals(self._obj.states[0], REVIEW_PROMPT_FOR_CONTAINER)
        self.assertEquals(self._obj.states[1], XMIT_CONTAINER_REVIEW)
        self.assertEquals(self._obj.states[2], REVIEW_CONTAINER)
        
    def test_prompt_for_container(self):
        
        self.post_dialog_responses('03')
        self._obj.runState(REVIEW_PROMPT_FOR_CONTAINER)
        self.validate_prompts('Container?')
        self.validate_server_requests()
        self.assertNotEquals(self._obj.container, None)
        self.assertEquals(self._obj.next_state, None)
        
        #invalid con
        self.post_dialog_responses('56')
        self._obj.runState(REVIEW_PROMPT_FOR_CONTAINER)
        self.validate_prompts('Container?', 
                              '<spell>56</spell> not valid try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.container, None)
        self.assertEquals(self._obj.next_state, REVIEW_PROMPT_FOR_CONTAINER)
        
        #Only one open container
        self._obj.next_state = ''
        self._obj._container_lut.receive('1,0000000003,03,12345,Store 123,,O,0,0,\n2,0000000002,02,12345,Store 123,,A,0,0,\n\n')
        self._obj.runState(REVIEW_PROMPT_FOR_CONTAINER)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertNotEquals(self._obj.container, None)
        self.assertEquals(self._obj.next_state, '')
     
    def test_xmit_container_review(self):
        '''xmit container review tests'''
        self.start_server()
        self.post_dialog_responses('yes')
        self.set_server_response('123,container 123,4,123,011232,0,\n\n')
        self._obj.container= self._obj._container_lut._get_container_open_closed()
        self._obj.runState(XMIT_CONTAINER_REVIEW)
        self.validate_prompts('Review contents for Container 123?')
        self.validate_server_requests(['prTaskLUTContainerReview', '1', '1', '12345'])
        self.assertEquals(self._obj.next_state, None)
        
        self.post_dialog_responses('no')
        self.set_server_response('123,container 123,4,123,011232,0,\n\n')
        self._obj.container= self._obj._container_lut._get_container_open_closed()
        self._obj.runState(XMIT_CONTAINER_REVIEW)
        self.validate_prompts('Review contents for Container 123?')
        self.validate_server_requests(['prTaskLUTContainerReview', '1', '1', '12345'])
        self.assertEquals(self._obj.next_state, '')
        
    def test_review_container(self):
        self._obj._review_contents_lut.receive('123,container 123,4,123,011232,0,\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(REVIEW_CONTAINER)
        self.validate_prompts('container 123 picked 4', 'Review complete')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        self._obj._review_contents_lut.receive('123,container 123,4,123,011232,0,\n\n')
        self.post_dialog_responses('location')
        self.post_dialog_responses('ready')
        self._obj.runState(REVIEW_CONTAINER)
        self.validate_prompts('container 123 picked 4', '123', 'container 123 picked 4', 'Review complete')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        self._obj._review_contents_lut.receive('123,container 123,4,123,011232,0,\n124,container 124,5,126,011232,0,\n\n')
        self.post_dialog_responses('location')
        self.post_dialog_responses('ready')
        self.post_dialog_responses('ready')
        self._obj.runState(REVIEW_CONTAINER)
        self.validate_prompts('container 123 picked 4', '123', 'container 123 picked 4', 'container 124 picked 5', 'Review complete')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        self._obj._review_contents_lut.receive('123,container 123,4,123,011232,0,\n124,container 124,5,126,011232,0,\n\n')
        self.post_dialog_responses('location')
        self.post_dialog_responses('stop')
        self._obj.runState(REVIEW_CONTAINER)
        self.validate_prompts('container 123 picked 4', '123', 'container 123 picked 4')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
