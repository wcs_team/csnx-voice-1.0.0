from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from selection.PickAssignment import PickAssignmentTask
from selection.PickPromptMultiple import PickPromptMultipleTask
from selection.PickPromptSingle import PickPromptSingleTask
from selection.SelectionTask import SelectionTask
from selection.VarWeightsAndSerialNum import WeightsSerialNumbers
from vocollect_core.task.task_runner import Launch
from selection.PickPrompt import SLOT_VERIFICATION, ENTER_QTY,\
    QUANTITY_VERIFICATION, LOT_TRACKING, INTIALIZE_PUT, PUT_PROMPT,\
    WEIGHT_SERIAL, XMIT_PICKS, NEXT_STEP, CHECK_PARTIAL, CASE_LABEL_CD,\
    CHECK_TARGET_CONTAINER, CLOSE_TARGET_CONT, CYCLE_COUNT
from selection.LotTracking import LotTrackingTask
import unittest



#=====================================================
#Base Class for prompt tests
#=====================================================
class BasePickPromptTests(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        sel._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        sel._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                               "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n"
                               "B,0,402,893,1,pre 302,right 402,post 502,slot 891,15,'Pack',893,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0, \n\n"
                               "B,0,403,894,1,pre 303,right 403,post 503,slot 893,16,'Pack',894,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0,\n\n"
                               "N,1,405,895,1,pre 304,right 404,Post 504,Slot 890,1,'Pack',895,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0,\n\n");
        
        self.pickassign = obj_factory.get(PickAssignmentTask,
                                            sel.taskRunner, sel)
        
        self.pickassign.configure(sel._region_config_lut[0],
                                  sel._assignment_lut,
                                  sel._picks_lut,
                                  sel._container_lut,
                                  False)
        
        self.tempPickLut = sel._picks_lut
        
    
    def test_initializeStates(self):

        #only run in main classes
        if self._obj is not None:     
            #test name
            self.assertEquals(self._obj.name, 'pickPrompt')
    
            #test states
            self.assertEquals(self._obj.states[0], CHECK_TARGET_CONTAINER)
            self.assertEquals(self._obj.states[1], SLOT_VERIFICATION)
            self.assertEquals(self._obj.states[2], CASE_LABEL_CD)
            self.assertEquals(self._obj.states[3], ENTER_QTY)
            self.assertEquals(self._obj.states[4], QUANTITY_VERIFICATION)
            self.assertEquals(self._obj.states[5], LOT_TRACKING)
            self.assertEquals(self._obj.states[6], INTIALIZE_PUT)
            self.assertEquals(self._obj.states[7], PUT_PROMPT)
            self.assertEquals(self._obj.states[8], WEIGHT_SERIAL)
            self.assertEquals(self._obj.states[9], XMIT_PICKS)
            self.assertEquals(self._obj.states[10], CHECK_PARTIAL)
            self.assertEquals(self._obj.states[11], CLOSE_TARGET_CONT)
            self.assertEquals(self._obj.states[12], NEXT_STEP)
            self.assertEquals(self._obj.states[13], CYCLE_COUNT)
            
            #test dynamic vocab set properly
            self.assertTrue(self._obj.name in self._obj.dynamic_vocab.pick_tasks)
        
    #===================================================================================================
    #Helper methods for setting up data
    #===================================================================================================
    def _reset_picks(self):
        self.tempPickLut.receive('N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,2,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,0,00,,,Item Description,Size,UPC 13,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,3,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,0,00,,,Item Description,Size,UPC 14,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,5,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,0,00,,,Item Description,Size,UPC 13,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,0,00,,,Item Description,Size,UPC 13,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'N,0,9,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,0,00,,,Item Description,Size,UPC 14,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 '\n')
        
        #Picks for Item 12
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[3])
        self._obj._picks.append(self.tempPickLut[5])
        self._obj._picks.append(self.tempPickLut[6])
        
        self._obj.expected_quantity = 20
        
    def _validate_picks(self, picked, lot, put, status = None, quantities = None):
        self.assertEquals(self._obj._picked_quantity, picked)
        self.assertEquals(self._obj._lot_quantity, lot)
        self.assertEquals(self._obj._put_quantity, put)

        if status is not None:
            self.assertEquals(self.tempPickLut[0]['status'], status[0])
            self.assertEquals(self.tempPickLut[3]['status'], status[1])
            self.assertEquals(self.tempPickLut[5]['status'], status[2])
            self.assertEquals(self.tempPickLut[6]['status'], status[3])
        
        if quantities is not None:
            self.assertEquals(self.tempPickLut[0]['qtyPicked'], quantities[0])
            self.assertEquals(self.tempPickLut[3]['qtyPicked'], quantities[1])
            self.assertEquals(self.tempPickLut[5]['qtyPicked'], quantities[2])
            self.assertEquals(self.tempPickLut[6]['qtyPicked'], quantities[3])
        
    def _transmit_picks(self, expected_next_state):
        self._obj.next_state = None
        count = 0
        while count == 0 or self._obj.next_state in [INTIALIZE_PUT, XMIT_PICKS]:
            self._obj.runState(INTIALIZE_PUT)
            self._obj.next_state = None
            self._obj.runState(XMIT_PICKS)
            self.assertEquals(self._obj.next_state, None)
            self._obj.runState(NEXT_STEP)
            count += 1

        self.assertEquals(self._obj.next_state, expected_next_state)
        
    def _setup_put_data(self):
        self.pickassign._picks_lut.receive("N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,3,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,2,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,3,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,4,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,5,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "\n")
        self._obj._picks = self.pickassign._picks_lut
        self._obj._lot_quantity = 20

        self.pickassign._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '1,0,67890,Store 123,2,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '\n')
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n'
                                         '3,0000000003,03,67890,Store 123,,O,0,0,\n'
                                         '\n')
        self._obj.runState(INTIALIZE_PUT)
        

#=====================================================
#Multiple Pick prompt
#=====================================================
class testMultiplePickPromptTask(BasePickPromptTests):
    '''Test multiple pick prompt as well as all base pick prompt functionality '''
     
    def setUp(self):
        BasePickPromptTests.setUp(self)
        self._obj = obj_factory.get(PickPromptMultipleTask,
                                      self.pickassign._region,
                                      self.pickassign._assignment_lut, 
                                      self.pickassign._pickList,
                                      self.pickassign._container_lut,
                                      False,
                                      self.pickassign.taskRunner,
                                      self.pickassign)

    def test_auto_short(self):
        #test no auto short, just verify obj
        self.assertEqual(self._obj.current_state, None)
        
        #test object create with autoshort
        temp = obj_factory.get(PickPromptMultipleTask,
                                 self.pickassign._region,
                                 self.pickassign._assignment_lut, 
                                 self.pickassign._pickList,
                                 self.pickassign._container_lut,
                                 True,
                                 self.pickassign.taskRunner,
                                 self.pickassign)
        
        self.assertEqual(temp.current_state, INTIALIZE_PUT)
        self.assertEqual(temp._expected_quantity, 0)
        self.assertEqual(temp._picked_quantity, 0)
        
        #verify id description on multiple assignments and no containers      
        temp._region['containerType']=0
        temp._picks.append(self.tempPickLut[0])
        temp._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '1,0,67890,Store 123,2,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '\n')
        temp._picks[0]['idDescription']= 'store 111'
        temp._set_variables()
        self.assertEqual(temp._id_description, 'ID store 111,')
        
    def test_prompts(self):
         
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[1])
   
        #test that user is asked for check digits for slot when Multiple prompts is set
        self.post_dialog_responses("00")
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts("S 1")
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        
        #RALLYTC :1192 - Verify if the pick prompt does not verify the check digits of pick location.
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])
        self._obj._picks[0]["checkDigits"] = '00'
        self.post_dialog_responses('00')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('S 1')
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        
    def test_case_lable(self):    
        ''' Testing for Case label cd done in multiple pick prompt since
        this is how it would usually be configured and there is no real difference
        between single and multiple pick prompt '''
        
        #Test no case label check digits
        self._obj._picks.append(self.tempPickLut[1])
        self._obj.runState(CASE_LABEL_CD)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #Test case label 
        self._obj._picks[0]['caseLabel'] = '01'
        self.post_dialog_responses('00!', '11!', '01!')
        self._obj.runState(CASE_LABEL_CD)
        self.validate_prompts('Case label?',
                              'wrong 00, try again',
                              'wrong 11, try again',
                              'Case label?',)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #Test case label, short product 
        self._obj._region["qtyVerification"] = "0"
        self._obj._short_product = False
        self._obj._partial = False
        self._obj._picks[0]['caseLabel'] = '01'
        self.post_dialog_responses('short product')
        self._obj.runState(CASE_LABEL_CD)
        self.validate_prompts('Case label?',
                              'check digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CASE_LABEL_CD)
        self.assertEquals(self._obj._short_product, True)
        self.assertEquals(self._obj._partial, False)
        self.assertEquals(self._obj._skip_prompt, True)
        
        #Test case label, Partial
        self._obj._short_product = False
        self._obj._partial = False
        self._obj._picks[0]['caseLabel'] = '01'
        self.post_dialog_responses('partial')
        self._obj.runState(CASE_LABEL_CD)
        self.validate_prompts('Partial not allowed when not picking to containers',
                              'check digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CASE_LABEL_CD)
        self.assertEquals(self._obj._short_product, False)
        self.assertEquals(self._obj._partial, False)
        self.assertEquals(self._obj._skip_prompt, True)
        

        #Test case label, skip Slot (main prompt skipped)
        self._obj._short_product = False
        self._obj._partial = False
        self._obj._picks[0]['caseLabel'] = '01'
        self.post_dialog_responses('skip slot', 'no')
        self._obj.runState(CASE_LABEL_CD)
        self.validate_prompts('Skip slot, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CASE_LABEL_CD)
        self.assertEquals(self._obj._short_product, False)
        self.assertEquals(self._obj._partial, False)
        self.assertEquals(self._obj._skip_prompt, False)

        #Test case label, short was previously spoken
        self._obj._short_product = True
        self._obj._partial = False
        self._obj._picks[0]['caseLabel'] = '01'
        self.post_dialog_responses('01!')
        self._obj.runState(CASE_LABEL_CD)
        self.validate_prompts('Case label?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CASE_LABEL_CD)
        self.assertEquals(self._obj._short_product, True)
        self.assertEquals(self._obj._partial, False)

        
    def test_verify_quantity(self):
        #setup for pick prompt multiple
        self.start_server()
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[1])
        
        #initialize pick prompt single
        self._obj._set_variables()
                
        #tests verify quantity
        #TC1178 - Verify the quantity is spoken when Quantity verification =1
        self._obj._expected_quantity = 10
        self.post_dialog_responses("20")
        self._obj.runState(ENTER_QTY)
        self.validate_prompts("Pick 10 ,   pick message")
        self.assertEquals(self._obj._picked_quantity, 20)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
                
        #short product at multiple pick prompt
        self._obj._region['qtyVerification'] = "0"
        self.post_dialog_responses('short product')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Pick 10 ,   pick message')
        self.assertEquals(self._obj._picked_quantity, 20)
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.assertTrue(self._obj._short_product)
        self.validate_server_requests()
        
        self.post_dialog_responses('5!')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('how many did you pick?')
        self.assertEquals(self._obj._picked_quantity, 5)
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.assertTrue(self._obj._short_product)
        self.validate_server_requests()
        
        #test if actual quantity entered, short and partial turned off
        self._obj.next_state = None
        self._obj._short_product = True
        self._obj._partial = True
        self._obj._expected_quantity = 10
        self.post_dialog_responses("10")
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('how many did you pick?')
        self.assertEquals(self._obj._picked_quantity, 10)
        self.assertEquals(self._obj.next_state, None)
        self.assertFalse(self._obj._short_product)
        self.assertFalse(self._obj._partial)
        self.validate_server_requests()
        
        
        #RALLYTC 1181 - Verify if the device speaks appropriate message if operator selects pick more than pick quantity.
        #quantity greater
        self._obj._picked_quantity = 20
        self._obj._expected_quantity = 10
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("You said 20 only asked for 10. Try again.")
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()
        
        self._obj.next_state=''
        self._obj._picked_quantity = 5
        self._obj._expected_quantity = 10
        self._obj._short_product = False
        self.post_dialog_responses('yes')
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("You said 5 asked for 10. Is this a short product?")
        self.assertEquals(self._obj.next_state, '')
        self.validate_server_requests()
        
        self._obj.next_state=''
        self._obj._picked_quantity = 5
        self._obj._expected_quantity = 10
        self.post_dialog_responses('no')
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("You said 5 asked for 10. Is this a short product?")
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()

        self._obj.next_state=''
        self._obj._picked_quantity = 5
        self._obj._expected_quantity = 10
        self._obj._short_product = True
        self.post_dialog_responses('yes')
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("5, correct?")
        self.assertEquals(self._obj.next_state, '')
        self.validate_server_requests()
        
        self._obj.next_state=''
        self._obj._picked_quantity = 5
        self._obj._expected_quantity = 10
        self._obj._short_product = True
        self.post_dialog_responses('no')
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("5, correct?")
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()
        
        #quantity smaller
        #RALLYTC1180 - Verify if the device speaks appropriate message if operator selects pick less than pick quantity.
        self._obj._short_product = False
        self._obj._expected_quantity = 10
        self._obj._picked_quantity=5
        self.post_dialog_responses('yes')
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("You said 5 asked for 10. Is this a short product?")
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()
                        
        #speaking correct quantity
        self._obj._expected_quantity = 10
        self.post_dialog_responses("ready")
        self._obj.runState(ENTER_QTY)
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("Pick 10 ,   pick message")
        self.validate_server_requests()
        
        #verify description
        #RALLYTC:1190 Verify if the device speaks the complete pick details in pick prompt like Quantity, UOM, Message , ID and Description
        self._obj._expected_quantity = 10
        self.post_dialog_responses("ready")
        self._obj._picks[0]['multiItemLocation'] = 1
        self._obj._set_variables()
        self._obj.runState(ENTER_QTY)
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("Pick 10 ,  item description, pick message")
        self.validate_server_requests()
                   
        #quantity false
        #RALLYTC : 1183 Verify if operator does not verify quantities Qantity verification = 0
        self._obj._region["qtyVerification"] = "0"
        self.post_dialog_responses("ready")
        self._obj.runState(ENTER_QTY)
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("Pick 10 ,  item description, pick message")
        self.validate_server_requests()
     
    def test_verify_quantity_partial(self):
        self.start_server()
        self._reset_picks()
        self._obj._set_variables()

        #test speaking partial for first time
        self._obj._short_product = True
        self._obj._region['containerType'] = '1'
        self._obj._region['qtyVerification'] = '1'
        self.post_dialog_responses('partial', 'yes')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('how many did you pick?',
                              'partial, correct?')
        self.validate_server_requests()
        self.assertTrue(self._obj._partial)
        self.assertFalse(self._obj._short_product)
        self.assertEqual(self._obj.next_state, ENTER_QTY)

        #test prompt if partial
        self._obj._partial = True
        self.post_dialog_responses('short product')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Quantity?')
        self.validate_server_requests()
        self.assertFalse(self._obj._partial)
        self.assertEqual(self._obj.next_state, ENTER_QTY)
        
        #TESTLINK 96430 - Verify partial qauntity entered 0
        self._obj._partial = True
        self._obj._picked_quantity = 0
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts('you must enter a quantity greater than zero for a partial')
        self.assertFalse(self._obj._partial)
        self.assertEqual(self._obj.next_state, ENTER_QTY)
        
    def test_lot_tracking(self):
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[1])
        self._obj._set_variables()
        self._obj._picks[0]['captureLot'] = 1
        self._obj._picked_quantity = 10
        
        self.assertRaises(Launch, self._obj.runState, LOT_TRACKING)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, LotTrackingTask))
        self.assertEquals(self._obj.current_state, INTIALIZE_PUT)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'lotTracking')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        
        self._obj._picks[0]['captureLot'] = 0
        self._obj._picked_quantity = 5
        self._obj.runState(LOT_TRACKING)
        self.validate_prompts()
        self.assertEquals(self._obj._lot_quantity, 5)
        
        
    def test_weight_serial_num(self):
        self._obj._picks.append(self.tempPickLut[0]) 
        self._obj._picks.append(self.tempPickLut[1])
        self._obj._set_variables()
        self._obj._put_quantity = 10
        self._obj._picks[0]['variableWeight']=True
        
        self.assertRaises(Launch, self._obj.runState, WEIGHT_SERIAL)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, WeightsSerialNumbers))
        self.assertEquals(self._obj.current_state, XMIT_PICKS)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'weightsSerialNumbers')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        
        self._obj._put_quantity = 0
        self._obj.runState(WEIGHT_SERIAL)
        self.validate_prompts()
        

    def test_initialize_puts(self):
        self.pickassign._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '1,0,67890,Store 123,2,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '\n')
        self.pickassign._picks_lut.receive("N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,3,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,2,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,3,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,4,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,5,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "\n")
        
        self._obj._picks = self.pickassign._picks_lut

        #Test normal first picks
        self._obj._region['containerType'] = 1
        self._obj._lot_quantity = 20
        self._obj.runState(INTIALIZE_PUT)
        self.assertEqual(self._obj._put_quantity, 7)
        self.assertEqual(len(self._obj._puts), 2)
        self.assertEqual(self._obj._puts[0]['sequence'], 1)
        self.assertEqual(self._obj._puts[1]['sequence'], 2)
        self.assertEqual(self._obj._curr_assignment['assignmentID'], '12345')
        self.assertEqual(self._obj.next_state, None)
        
        #TESTLINK ID 95780 :: Test Case Verify if Put prompt is asked for the Lots if Lot capture is enabled.
        #test normal second assignment
        self._obj._picks[0]['status'] = 'P'
        self._obj._picks[1]['status'] = 'P'
        self._obj._picks[3]['status'] = 'S'
        
        self._obj._lot_quantity = 20
        self._obj.runState(INTIALIZE_PUT)
        self.assertEqual(self._obj._put_quantity, 15)
        self.assertEqual(len(self._obj._puts), 3)
        self.assertEqual(self._obj._puts[0]['sequence'], 3)
        self.assertEqual(self._obj._puts[1]['sequence'], 4)
        self.assertEqual(self._obj._puts[2]['sequence'], 5)
        self.assertEqual(self._obj._curr_assignment['assignmentID'], '67890')
        self.assertEqual(self._obj.next_state, None)

        #test partial quantity
        self._obj._lot_quantity = 8
        self._obj.runState(INTIALIZE_PUT)
        self.assertEqual(self._obj._put_quantity, 8)
        self.assertEqual(len(self._obj._puts), 3)
        self.assertEqual(self._obj._puts[0]['sequence'], 3)
        self.assertEqual(self._obj._puts[1]['sequence'], 4)
        self.assertEqual(self._obj._puts[2]['sequence'], 5)
        self.assertEqual(self._obj._curr_assignment['assignmentID'], '67890')
        self.assertEqual(self._obj.next_state, None)

        #TESTLINK 95741 :: Test Case Verify if Qty of the picks are short at pick prompt does not go to Put..
        self._obj._lot_quantity = 0
        self._obj.next_state = None
        self._obj.runState(INTIALIZE_PUT)
        self.assertEqual(self._obj._put_quantity, 0)
        self.assertEqual(len(self._obj._puts), 3)
        self.assertEqual(self._obj._puts[0]['sequence'], 3)
        self.assertEqual(self._obj._puts[1]['sequence'], 4)
        self.assertEqual(self._obj._puts[2]['sequence'], 5)
        self.assertEqual(self._obj._curr_assignment['assignmentID'], '67890')
        self.assertEqual(self._obj.next_state, WEIGHT_SERIAL)
        
        #TESTLINK 95743 :: Test Case Verify if Container type is none does not go to Put flow.
        self._obj._region['containerType'] = 0
        self._obj._lot_quantity = 20
        self._obj.next_state = None
        self._obj.runState(INTIALIZE_PUT)
        self.assertEqual(self._obj._put_quantity, 15)
        self.assertEqual(len(self._obj._puts), 3)
        self.assertEqual(self._obj._puts[0]['sequence'], 3)
        self.assertEqual(self._obj._puts[1]['sequence'], 4)
        self.assertEqual(self._obj._puts[2]['sequence'], 5)
        self.assertEqual(self._obj._curr_assignment['assignmentID'], '67890')
        self.assertEqual(self._obj.next_state, WEIGHT_SERIAL)
        
    def test_put_prompt(self):
        self.pickassign._picks_lut.receive("N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,3,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,2,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,3,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,4,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "N,0,5,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,67890,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n"
                                           "\n")
        
        self._obj._picks = self.pickassign._picks_lut
        self._obj._lot_quantity = 20
        
        #test single assignment single open container
        self.pickassign._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '\n')
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self._obj._region['containerType'] = 0
        self._obj.runState(INTIALIZE_PUT)
        self._obj.next_state = None
        
        self._obj.runState(PUT_PROMPT)
        self.validate_prompts()
        self.assertEqual(self._obj.next_state, None)
        
        #test single assignment multiple open container
        self.pickassign._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '\n')
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n'
                                         '3,0000000002,03,12345,Store 123,,O,0,0,\n'
                                         '\n')
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self.post_dialog_responses('02')
        
        self._obj.runState(PUT_PROMPT)
        self.validate_prompts('Container?')
        self.assertEqual(self._obj.next_state, None)
        
        #TESTLINK 95749 :: Test Case Verify Put prompt for multiple assignments.
        #TESTLINK 95777 :: Test Case Verify if correct container Id digits are accepted.
        #test multiple assignments, single open containers
        self.pickassign._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '1,0,67890,Store 123,2,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '\n')
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n'
                                         '3,0000000002,03,67890,Store 123,,O,0,0,\n'
                                         '\n')
        self._obj._region['containerType'] = 0
        self._obj.runState(INTIALIZE_PUT)
        self._obj.next_state = None

        self.post_dialog_responses('02')
        self._obj.runState(PUT_PROMPT)
        self.validate_prompts('Put 7 in 1')
        self.assertEqual(self._obj.next_state, None)

        #test invalid container
        self.post_dialog_responses('03')
        self._obj.runState(PUT_PROMPT)
        self.validate_prompts('Put 7 in 1',
                              'Wrong 03. Try again.')
        self.assertEqual(self._obj.next_state, PUT_PROMPT)
        
        #test partial prompt
        self._obj.next_state = None
        self._obj._partial = True
        self.post_dialog_responses('02')
        self._obj.runState(PUT_PROMPT)
        self.validate_prompts('Container?')
        self.assertEqual(self._obj.next_state, None)
    
        #test speaking partial
        self._obj._region['containerType'] = '1'
        self._obj.next_state = None
        self._obj._partial = False
        self.post_dialog_responses('partial', 'yes', '3')
        self._obj.runState(PUT_PROMPT)
        self.validate_prompts('Put 7 in 1', 'partial, correct?', 'Quantity?')
        self.assertEqual(self._obj.next_state, PUT_PROMPT)
        self.assertTrue(self._obj._partial)
    
    def test_validate_partial(self):
        self._setup_put_data()
        self._obj._region["qtyVerification"] = "0"
        
        #TESTLINK: 96372 - test not available if not picking to containers
        #TESTLINK: 96376 - test not available if not picking to containers
        self._obj._region['containerType'] = 0
        self.assertFalse(self._obj._validate_partial(7, False))
        self.validate_prompts('Partial not allowed when not picking to containers')
        self._obj._region['containerType'] = 1
    
        #TESTLINK: 96386 - test not available if expected qty is 1
        self.assertFalse(self._obj._validate_partial(1, False))
        self.validate_prompts('Partial not allowed when pick quantity is one.')
        
        #TESTLINK: 96368 - test not available if picking to target containers
        self._obj._picks[0]['targetContainer'] = 1
        self.assertFalse(self._obj._validate_partial(7, False))
        self.validate_prompts('partial not allowed when picks have a target container')
        self._obj._picks[0]['targetContainer'] = 0
    
        #TESTLINK: 96403 - test put prompt flag
        self.assertFalse(self._obj._validate_partial(7, False))
        self.validate_prompts('Partial only allowed at put prompt.')
    
        #test partial, no
        self.post_dialog_responses('no')
        self.assertFalse(self._obj._validate_partial(7, True))
        self.validate_prompts('partial, correct?')
    
        #test partial, yes
        self._obj._short_product = True
        self.post_dialog_responses('yes')
        self.assertTrue(self._obj._validate_partial(7, True))
        self.validate_prompts('partial, correct?')
        self.assertFalse(self._obj._short_product)
        
        #test multiple prompt, current state
        self._obj._short_product = False
        self._obj._region['pickPromptType'] = '2'
        self._obj.current_state = SLOT_VERIFICATION
        self.assertFalse(self._obj._validate_partial(7, True))
        self.validate_prompts('Partial not allowed until pick quantity is spoken.')
      
    def test_validate_shorts(self):
        #test multiple prompt, current state
        self._obj._short_product = False
        self._obj._region['pickPromptType'] = '2'
        self._obj.current_state = SLOT_VERIFICATION
        self.assertFalse(self._obj._validate_short_product())
        self.validate_prompts('Short product not allowed until pick quantity spoken')
        
        #test quantity verification, current state
        self._obj._region['pickPromptType'] = '1'
        self._obj._region['qtyVerification'] = '1'
        self._obj.current_state = SLOT_VERIFICATION
        self.assertFalse(self._obj._validate_short_product())
        self.validate_prompts('Report Shorts when prompted for quantity')
        
    def test_get_partial_quantity(self):
        #This is only called from put prompt
        self._setup_put_data()
        
        
        #TESTLINK: 96418 - test, quantity to high, quantity equal
        self._obj._partial = True
        self._obj._put_quantity = 11
        self.post_dialog_responses('20', '11')
        self._obj._get_partial_quantity()
        self.validate_prompts('Quantity?', 
                              'You said 20 only asked for 11. Try again.',
                              'Quantity?')
        self.assertFalse(self._obj._partial)
        self.assertEqual(self._obj._put_quantity, 11)

        #TESTLINK: 96430 - test, quantity 0 entered
        self._obj._partial = True
        self.post_dialog_responses('0!')
        self._obj._get_partial_quantity()
        self.validate_prompts('Quantity?')
        self.assertFalse(self._obj._partial)
        self.assertEqual(self._obj._put_quantity, 11)
        
        #test, quantity less entered
        self._obj._partial = True
        self.post_dialog_responses('5!')
        self._obj._get_partial_quantity()
        self.validate_prompts('Quantity?')
        self.assertTrue(self._obj._partial)
        self.assertEqual(self._obj._put_quantity, 5)
        
    def test_validate_container(self):
        #TESTLINK 95777 :: Test Case Verify if correct container Id digits are accepted.
        self._setup_put_data()

        #test single assignment, single open container
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._validate_container(None, False)
        self.validate_prompts()
        self.assertEqual(self._obj._curr_container['systemContainerID'], '2')
        self._obj._curr_assignment = self._obj._assignment_lut[1]
        self._obj._validate_container(None, False)
        self.validate_prompts()
        self.assertEqual(self._obj._curr_container['systemContainerID'], '3')
        
        #test spoken container ID, Found
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._validate_container('02', False)
        self.validate_prompts()
        
        #test spoken container ID, Not Found
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._validate_container('03', False)
        self.validate_prompts('Wrong 03. Try again.')
        
        #TESTLINK 95788 :: Test Case Verify if container ID can be scanned during put prompt.
        #test scanned container ID, Found
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._validate_container('0000000002', True)
        self.validate_prompts()
        
        #test scanned container ID, Not Found
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._validate_container('0000000003', True)
        self.validate_prompts('Wrong 0000000003. Try again.')
  
        #test target containers, valid container
        self._obj._curr_assignment['activeTargetContainer'] = 1
        self._obj._container_lut[0]['targetConatiner'] = 1
        for put in self._obj._puts:
            put['targetContainer'] = 1
        self._obj._validate_container('02', False)
        self.validate_prompts()
            
        #test target containers, wrong container
        self._obj._curr_assignment['activeTargetContainer'] = 1
        self._obj._container_lut[0]['targetConatiner'] = 1
        for put in self._obj._puts:
            put['targetContainer'] = 1
        self._obj._validate_container('03', False)
        self.validate_prompts('Wrong 03. Try again.')
            
        #test target containers, unknown container
        self._obj._curr_assignment['activeTargetContainer'] = 1
        self._obj._container_lut[0]['targetConatiner'] = 1
        for put in self._obj._puts:
            put['targetContainer'] = 1
        self._obj._validate_container('99', False)
        self.validate_prompts('Wrong 99. Try again.')
            
        
    def test_check_partial(self):
        self._setup_put_data()
        self._obj.next_state = None
        
        #Check not in partial
        self._obj._partial = False
        self._obj.runState(CHECK_PARTIAL)
        self.validate_prompts()
        self.assertEqual(self._obj.next_state, None)
        self.assertFalse(self._obj._partial)
        
        #check partial, with single open containers, all put
        self._obj._partial = True
        self._obj._put_quantity = 0
        self.assertRaises(Launch, self._obj.runState, CHECK_PARTIAL)
        self.validate_prompts()
        self.assertEqual(self._obj.next_state, None)
        self.assertFalse(self._obj._partial)
        
        #check partial, with single open containers, more puts
        self._obj._partial = True
        self._obj._put_quantity = 5
        self._obj.runState(CHECK_PARTIAL)
        self.validate_prompts()
        self.assertEqual(self._obj.next_state, None)
        self.assertTrue(self._obj._partial)
        
        #check partial, with multiple open containers
        self.pickassign._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                                '\n')
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n'
                                         '3,0000000003,03,12345,Store 123,,O,0,0,\n'
                                         '\n')
        self._obj._partial = True
        self._obj._put_quantity = 0
        self._obj.runState(CHECK_PARTIAL)
        self.validate_prompts()
        self.assertEqual(self._obj.next_state, None)
        self.assertFalse(self._obj._partial)
        
    def test_xmit_picks(self):
        self.start_server()

        #----------------------------------------------------------------
        #test normal
        self._reset_picks()
        self._obj._picked_quantity = 20
        self._obj._lot_quantity = 20
        self._obj._put_quantity = 20
        

        self._transmit_picks(None)
        
        self.validate_prompts()
        self._validate_picks(0, 0, 0, ['P', 'P', 'P', 'P'], [5, 5, 5, 5])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','5','1','','1','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','5','1','','4','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','5','1','','6','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','5','1','','7','','',''])
        
        #----------------------------------------------------------------
        #test short
        self._reset_picks()
        self._obj._expected_quantity = 13
        self._obj._picked_quantity = 13
        self._obj._lot_quantity = 13
        self._obj._put_quantity = 13
        
        self._transmit_picks(None)
        
        self.validate_prompts()
        self._validate_picks(0, 0, 0, ['P', 'P', 'P', 'P'], [5, 5, 3, 0])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','5','1','','1','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','5','1','','4','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','3','1','','6','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','0','1','','7','','',''])
        
    def test_xmit_picks_lots(self):
        self.start_server()

        #----------------------------------------------------------------
        #test lot, no weights, no serial numbers
        self._reset_picks()
        self._obj._picked_quantity = 20
        self._obj._lot_quantity = 13
        self._obj._put_quantity = 13
        self._obj._lot_number = '12345'
        
        self._transmit_picks(SLOT_VERIFICATION)
        
        self.validate_prompts()
        self._validate_picks(7, 0, 0, ['P', 'P', 'N', 'N'], [5, 5, 3, 0])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','5','1','','1','12345','',''],
                                      ['prTaskLUTPicked','1','12345','L1','5','1','','4','12345','',''],
                                      ['prTaskLUTPicked','1','12345','L1','3','0','','6','12345','',''])
        
        #----------------------------------------------------------------
        #finish pick from previous test
        self._obj._lot_quantity = 7
        self._obj._put_quantity = 7
        self._obj._lot_number = '67890'

        self._transmit_picks(None)
            
        self.validate_prompts()
        self._validate_picks(0, 0, 0, ['P', 'P', 'P', 'P'], [5, 5, 5, 5])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','2','1','','6','67890','',''],
                                      ['prTaskLUTPicked','1','12345','L1','5','1','','7','67890','',''])
            
        #----------------------------------------------------------------
        #Test lot short
        self._reset_picks()
        self._obj._picked_quantity = 20
        self._obj._lot_quantity = 9
        self._obj._put_quantity = 9
        self._obj._lot_number = '12345'
        
        self._transmit_picks(SLOT_VERIFICATION)
        
        self.validate_prompts()
        self._validate_picks(11, 0, 0, ['P', 'N', 'N', 'N'], [5, 4, 0, 0])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','5','1','','1','12345','',''],
                                      ['prTaskLUTPicked','1','12345','L1','4','0','','4','12345','',''])
        
        #----------------------------------------------------------------
        #finish pick from previous test
        self._obj._expected_quantity = 0
        self._obj._picked_quantity = 0
        self._obj._lot_quantity = 0
        self._obj._put_quantity = 0
        self._obj._lot_number = None

        self._transmit_picks(None)
            
        self.validate_prompts()
        self._validate_picks(0, 0, 0, ['P', 'P', 'P', 'P'], [5, 4, 0, 0])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','0','1','','4','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','0','1','','6','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','0','1','','7','','',''])
    
    
    def test_skip_slot(self):
        self.start_server()

        #----------------------------------------------------------------
        #Test skip slot not allowed
        self._obj._region['skipSlotAllowed'] = False
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])

        self.post_dialog_responses('skip slot')
        
        self._obj.runState(SLOT_VERIFICATION)
        
        self.validate_prompts('S 1',
                              'skip slot not allowed')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test skip slot confirm/no
        self._obj._region['skipSlotAllowed'] = True
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])

        self.post_dialog_responses('skip slot', 'no')
        
        self._obj.runState(SLOT_VERIFICATION)
        
        self.validate_prompts('S 1',
                              'Skip slot, correct?')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
        
        
        #----------------------------------------------------------------
        #Test skip slot last slot
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])
        self.tempPickLut[0]['status'] = 'P'
        self.tempPickLut[1]['status'] = 'N'
        self.tempPickLut[2]['status'] = 'P'
        self.tempPickLut[3]['status'] = 'P'
        self.tempPickLut[4]['status'] = 'P'
        
        self.post_dialog_responses('skip slot')
        
        self._obj.runState(SLOT_VERIFICATION)
        
        self.validate_prompts('S 1',
                              'Last slot, skip slot not allowed')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test skip slot base items
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])
        self.tempPickLut[0]['status'] = 'B'
        self.tempPickLut[1]['status'] = 'B'
        self.tempPickLut[2]['status'] = 'N'
        self.tempPickLut[3]['status'] = 'N'
        self.tempPickLut[4]['status'] = 'B'
        
        self.post_dialog_responses('skip slot', 'yes')
        
        self._obj.runState(SLOT_VERIFICATION)
        
        self.validate_prompts('S 1',
                              'Skip slot, correct?')
        self.assertEquals(self._obj.next_state, '')
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','L1','0','N'])
        self.assertEqual('N', self.tempPickLut[0]['status'])
        self.assertEqual('N', self.tempPickLut[1]['status'])
        
        #----------------------------------------------------------------
        #Test skip slot normal pass
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])
        self.tempPickLut[0]['status'] = 'P'
        self.tempPickLut[1]['status'] = 'N'
        self.tempPickLut[2]['status'] = 'N'
        self.tempPickLut[3]['status'] = 'N'
        self.tempPickLut[4]['status'] = 'N'
        
        self.post_dialog_responses('skip slot', 'yes')
        
        self._obj.runState(SLOT_VERIFICATION)
        
        self.validate_prompts('S 1',
                              'Skip slot, correct?')
        self.assertEquals(self._obj.next_state, '')
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','L1','0','S'])
        self.assertEqual('P', self.tempPickLut[0]['status'])
        self.assertEqual('S', self.tempPickLut[1]['status'])
        
        
        #----------------------------------------------------------------
        #Test skip slot qty verification prompts returns to correct state
        self._obj._region["qtyVerification"] = "1"
        self._obj._region['skipSlotAllowed'] = False
        
        self.post_dialog_responses('skip slot')
        
        self._obj.runState(ENTER_QTY)
        
        self.validate_prompts('Pick 0    ',
                              'skip slot not allowed')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test skip slot qty verification prompts returns to correct state
        self._obj._region["qtyVerification"] = "0"
        
        self.post_dialog_responses('skip slot')
        
        self._obj.runState(ENTER_QTY)
        
        self.validate_prompts('Pick 0    ',
                              'skip slot not allowed')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()

    def test_xmit_picks_weights_and_serial(self):
        self.start_server()

        #----------------------------------------------------------------
        #test normal weights
        self._reset_picks()
        self._obj._picked_quantity = 20
        self._obj._lot_quantity = 20
        self._obj._put_quantity = 20
        self._obj._lot_number = None
        self._obj._weights = [1.00, 1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09, 
                             1.10, 1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19]
        
        self._transmit_picks(None)
        
        self.assertEquals(len(self._obj._weights), 0)
        self.validate_prompts()
        self._validate_picks(0, 0, 0, ['P', 'P', 'P', 'P'], [5, 5, 5, 5])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.00',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.01',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.02',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.03',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','1','','1.04',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','1.05',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','1.06',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','1.07',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','1.08',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','4','','1.09',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','1.10',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','1.11',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','1.12',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','1.13',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','6','','1.14',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','1.15',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','1.16',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','1.17',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','1.18',''],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','7','','1.19',''])
        

        #----------------------------------------------------------------
        #test normal serial numbers
        self._reset_picks()
        self._obj._picked_quantity = 20
        self._obj._lot_quantity = 20
        self._obj._put_quantity = 20
        self._obj._lot_number = None
        self._obj._serial_numbers = ['SER01', 'SER02', 'SER03', 'SER04', 'SER05', 'SER06', 'SER07', 'SER08', 'SER09', 'SER10', 
                                    'SER11', 'SER12', 'SER13', 'SER14', 'SER15', 'SER16', 'SER17', 'SER18', 'SER19', 'SER20']
        
        self._transmit_picks(None)
        
        self.assertEquals(len(self._obj._weights), 0)
        self.validate_prompts()
        self._validate_picks(0, 0, 0, ['P', 'P', 'P', 'P'], [5, 5, 5, 5])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','1','0','','1','','','SER01'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','','SER02'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','','SER03'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','','SER04'],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','1','','','SER05'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','','SER06'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','','SER07'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','','SER08'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','','SER09'],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','4','','','SER10'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','','SER11'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','','SER12'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','','SER13'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','6','','','SER14'],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','6','','','SER15'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','','SER16'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','','SER17'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','','SER18'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','7','','','SER19'],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','7','','','SER20'])


        #----------------------------------------------------------------
        #test short weights
        self._reset_picks()
        self._obj._expected_quantity = 7
        self._obj._picked_quantity = 7
        self._obj._lot_quantity = 7
        self._obj._put_quantity = 7
        self._obj._lot_number = None
        self._obj._weights = [1.00, 1.01, 1.02, 1.03, 1.04, 1.05, 1.06]
        self._obj._serial_numbers = ['SER01', 'SER02', 'SER03', 'SER04', 'SER05', 'SER06', 'SER07']
        
        self._transmit_picks(None)
        
        self.assertEquals(len(self._obj._weights), 0)
        self.validate_prompts()
        self._validate_picks(0, 0, 0, ['P', 'P', 'P', 'P'], [5, 2, 0, 0])
        self.validate_server_requests(['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.00','SER01'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.01','SER02'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.02','SER03'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','1','','1.03','SER04'],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','1','','1.04','SER05'],
                                      ['prTaskLUTPicked','1','12345','L1','1','0','','4','','1.05','SER06'],
                                      ['prTaskLUTPicked','1','12345','L1','1','1','','4','','1.06','SER07'],
                                      ['prTaskLUTPicked','1','12345','L1','0','1','','6','','',''],
                                      ['prTaskLUTPicked','1','12345','L1','0','1','','7','','',''])
       
#=====================================================
#Single Pick prompt
#=====================================================
class testSinglePickPromptTask(BasePickPromptTests):
    '''Test multiple pick prompt as well as all base pick prompt functionality '''
     
    def setUp(self):
        BasePickPromptTests.setUp(self)
        self._obj = PickPromptSingleTask(self.pickassign._region,
                                                      self.pickassign._assignment_lut, 
                                                      self.pickassign._pickList,
                                                      self.pickassign._container_lut,
                                                      False,
                                                      self.pickassign.taskRunner, 
                                                      self.pickassign)
    
    def test_prompts(self):
         
        # verify the message at pick prompt for single
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[2]) 
        self._obj._picks.append(self.tempPickLut[3])
        self._obj._picks.append(self.tempPickLut[4])

        #TESTLINK 95307 :: Test Case Verify if the pick is summed up.
        #TESTLINK 95312 :: Test Case Pick is summed up and spoken in single prompt when supress qty 1.
        self._obj._set_variables()
        self._obj._region['pickPromptType'] = '1'
        self._obj._expected_quantity = 10
        self.post_dialog_responses('890')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, None)
        
        self.post_dialog_responses('890')
        self._obj._expected_quantity = 1
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        
        #TESTLINK 95223 :: Test Case Verify if slot and quantity are spoken in one prompt and Quantity verified.
        self._obj._region['pickPromptType'] = '0'
        self.post_dialog_responses('890')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 1 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, None)
        
        #TESTLINK 95227 :: Test Case Verify if only the slot information is spoken, and quantity is verified.
        self._obj._region['pickPromptType'] = '1'
        self._obj._expected_quantity=1
        self.post_dialog_responses('890')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, None)
        
        #TESTLINK 95304 :: Test Case Verify if Quantity is spoken in Qty supress 1 and qty req is more than 1.
        self._obj._region['pickPromptType'] = '1'
        self._obj._expected_quantity=10
        self.post_dialog_responses('890')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, None)

        #TESTLINK 95231 :: Test Case Verify if the slot and quantity are spoken in one prompt, no check digit.
        self._obj._picks[0]['checkDigits']=''
        self._obj._region['pickPromptType'] = '0'
        self.post_dialog_responses('ready')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, None)
        
        #TEST: 95233 :: Test Case Verify if only slot is spoken, with no check digit.
        self._obj._picks[0]['checkDigits']=''
        self._obj._region['pickPromptType'] = '1'
        self._obj._expected_quantity=1
        self.post_dialog_responses('ready')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, None)
        
        #TESTLINK 95907 :: Test Case Verify if correct flow is taken when No check digits and PVID.
        self._obj._pvid='55'
        self.post_dialog_responses('ready')
        self.post_dialog_responses('no')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pack, item 891 site,  Pick Message Site 1', 'Is this a short product?', 'You must speak the product ID. Try Again.')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
        
        self.post_dialog_responses('ready')
        self.post_dialog_responses('yes')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pack, item 891 site,  Pick Message Site 1', 'Is this a short product?')
        self.assertEquals(self._obj.next_state, LOT_TRACKING)
        self.validate_server_requests()
        
        self._obj._picks[0]['scannedProdID']=''
        self._obj._verify_product_slot('54', True)
        self.validate_prompts('wrong 54, try again')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
        
        #TESTLINK 95909 :: Test Case Verify if correct flow is taken when Check digits & PVID are same.
        self._obj._picks[0]['checkDigits']='24'
        self._obj._pvid='24'
        self._obj._expected_quantity=10
        self.post_dialog_responses('24')
        self.post_dialog_responses('yes')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1', 'Identical Product verification and check digits. Is this a short product')
        self.assertEquals(self._obj.next_state, LOT_TRACKING)
        self.validate_server_requests()
        
        #TESTLINK 95911 :: Test Case Verify if correct flow is taken when Check digits & PVID are different.
        self._obj._picks[0]['checkDigits']='15'
        self._obj._pvid='24'
        self.post_dialog_responses('24')
        self._obj._expected_quantity=10
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, LOT_TRACKING)
        self.validate_server_requests()
                
        #TESTLINK 95918 :: Test Case Check the correct flow if no check digits and no PVID are mentioned.
        self._obj._picks[0]['checkDigits']=''
        self._obj._pvid=''
        self.post_dialog_responses('ready')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1')
        self.assertEquals(self._obj.next_state, LOT_TRACKING)
        self.validate_server_requests()
        
        #TESTLINK ID 95924 :: Test Case Check the correct flow if Check digits are mentioned and no PVID.
        self._obj._picks[0]['checkDigits']='55'
        self._obj._pvid=''
        self.post_dialog_responses('ready')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1', 'You must speak the check digit.')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
                        
        #tests if short product can be spoken
        self.post_dialog_responses('short product')
        self._obj._picks[0]['checkDigits']='12'
        self._obj._region['qtyVerification'] = "0"
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('slot 891 Pick 10 Pack, item 891 site,  Pick Message Site 1',
                              'check digit?')
        self.assertTrue(self._obj._skip_prompt)
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        
        #Asks for check digit if short product is spoken (prompt skipped)
        self._obj._short_product = True
        self._obj._picks[0]['checkDigits']='12'
        self._obj._region['qtyVerification'] = "0"
        self.post_dialog_responses('12')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        
    def test_verify_quantity(self):
        self.start_server()
        
        #setup for single prompt
        self._obj._picks.append(self.tempPickLut[2]) 
        self._obj._picks.append(self.tempPickLut[3])
        self._obj._picks.append(self.tempPickLut[4])

        #initialize pick prompt single
        self._obj._set_variables()
                
        #verify the prompts for single pick prompts
        #TESTLINK 95223 :: Test Case Verify if slot and quantity are spoken in one prompt and Quantity verified.
        #TESTLINK 95227 :: Test Case Verify if only the slot information is spoken, and quantity is verified.
        self._obj._expected_quantity = 10
        self.post_dialog_responses("20")
        self._obj.runState(ENTER_QTY)
        self.validate_prompts("Quantity?")
        self.assertEquals(self._obj._picked_quantity, 20)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        
        #TESTLINK 95229 :: Test Case Verify if only the slot information is spoken, and no quantity is verified.
        self._obj._region['qtyVerification'] = "0"
        self._obj._expected_quantity = 10
        self._obj.runState(ENTER_QTY)
        self.validate_prompts()
        self.assertEquals(self._obj._picked_quantity, 10)
        self.assertEquals(self._obj.next_state, LOT_TRACKING)
        self.validate_server_requests()
        
        #TESTLINK 95338 :: Test Case Verify if Short product can be spoken,qty verif set to no & Chk digits 
        #TESTLINK 95348 :: Test Case Verify if Short product can be spoken,
        # quantity verification set to no & no Check digits 
        self._obj._short_product= True
        self._obj._region["qtyVerification"] = "0"
        self.post_dialog_responses("20")
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('how many did you pick?')
        self.assertEquals(self._obj._picked_quantity, 20)
        self.assertEquals(self._obj.next_state, LOT_TRACKING)
        self.validate_server_requests()
        
        #verify the prompts for single pick prompts
        self._obj._short_product= False
        self._obj._expected_quantity = 10
        self._obj._region["qtyVerification"] = "0"
        self._obj.runState(ENTER_QTY)
        self.validate_prompts()
        self.assertEquals(self._obj._picked_quantity, 10)
        self.assertEquals(self._obj.next_state, LOT_TRACKING)
        self.validate_server_requests()
        
        #TESTLINK 95310 :: Test Case Verify if error message is heard when qty picked is greater that requested.
        self._obj._expected_quantity = 10
        self._obj._picked_quantity = 20
        self._obj.runState(QUANTITY_VERIFICATION)
        self.validate_prompts("You said 20 only asked for 10. Try again.")
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()

        #verify the prompts for single pick prompts, if qty verification failed
        self._obj._short_product= False
        self._obj._partial = False
        self._obj._expected_quantity = 10
        self.post_dialog_responses("10")
        self._obj._region["qtyVerification"] = "0"
        self._obj.previous_state = QUANTITY_VERIFICATION
        self._obj.next_state = None
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Quantity?')
        self.assertEquals(self._obj._picked_quantity, 10)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        
        #verify the prompts for single pick prompts, if repeating enter qty
        self._obj._short_product= False
        self._obj._partial = False
        self._obj._expected_quantity = 10
        self.post_dialog_responses("10")
        self._obj._region["qtyVerification"] = "0"
        self._obj.previous_state = ENTER_QTY
        self._obj.next_state = None
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Quantity?')
        self.assertEquals(self._obj._picked_quantity, 10)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()

        #test for verify quanity =1 and no check digits
        
    def test_partials(self):
        self._reset_picks()
        self._obj._set_variables()
        self._obj._region['pickPromptType'] = '1'
        self._obj._region['containerType'] = '1'
        self._obj._region["qtyVerification"] = "0"
        
        #-------------------------------------
        # Test Slot Verification
        #-------------------------------------
        #test partial, when in short
        self._obj._short_product = True
        self._obj._partial = False
        self.post_dialog_responses('partial',
                                   'yes')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('S 1 Pick 0 ,   pick message',
                              'partial, correct?',
                              'check digit?')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.assertTrue(self._obj._partial)
        self.assertFalse(self._obj._short_product)
        
        #test short when in partial (main prompt skipped)
        self._obj._short_product = False
        self._obj._partial = True
        self.post_dialog_responses('short product')
        self._obj.runState(SLOT_VERIFICATION)
        self.validate_prompts('check digit?')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.assertFalse(self._obj._partial)
        self.assertTrue(self._obj._short_product)

        #-------------------------------------
        # Test Enter quantity
        #-------------------------------------
        #test normal
        self._obj._region["qtyVerification"] = "1"
        self._obj._short_product = False
        self._obj._partial = False
        self.post_dialog_responses('partial', 'yes')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Quantity?', 'partial, correct?')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.assertTrue(self._obj._partial)
        self.assertFalse(self._obj._short_product)
        
        #test in short product
        self._obj._region["qtyVerification"] = "0"
        self._obj._short_product = True
        self._obj._partial = False
        self.post_dialog_responses('partial', 'yes')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('how many did you pick?', 
                              'partial, correct?')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.assertTrue(self._obj._partial)
        self.assertFalse(self._obj._short_product)
        
        #test in partial
        self._obj._region["qtyVerification"] = "0"
        self._obj._short_product = False
        self._obj._partial = True
        self.post_dialog_responses('partial', 'no')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Quantity?', 
                              'partial, correct?')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.assertFalse(self._obj._partial)
        self.assertFalse(self._obj._short_product)
        
        #test in partial, with partial quantity
        self._obj._region["qtyVerification"] = "0"
        self._obj._short_product = False
        self._obj._partial = True
        self.post_dialog_responses('3')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Quantity?')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.assertTrue(self._obj._partial)
        self.assertFalse(self._obj._short_product)
        self.assertEqual(self._obj._picked_quantity, 3)
        
        #test in partial, but enter full quantity
        self._obj._region["qtyVerification"] = "0"
        self._obj._short_product = False
        self._obj._partial = True
        self._obj._expected_quantity = 20
        self.post_dialog_responses('15')
        self._obj.runState(ENTER_QTY)
        self.validate_prompts('Quantity?')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.assertTrue(self._obj._partial)
        self.assertFalse(self._obj._short_product)
        self.assertEqual(self._obj._picked_quantity, 15)
                
    def test_skip_slot(self):
        ''' Most test covered in multiple pick prompt, these test just cover word can
        be properly used at correct places and returns correctly'''
        
        self._obj._region['skipSlotAllowed'] = False
        
        #----------------------------------------------------------------
        #Test skip slot normal prompt
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])

        self.post_dialog_responses('skip slot')
        
        self._obj.runState(SLOT_VERIFICATION)
        
        self.validate_prompts('S 1 Pick 0    ',
                              'skip slot not allowed')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test skip slot short product
        self._obj._short_product = True
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])

        self.post_dialog_responses('skip slot')
        
        self._obj.runState(SLOT_VERIFICATION)
        
        self.validate_prompts('S 1 Pick 0    ',
                              'skip slot not allowed')
        self.assertEquals(self._obj.next_state, SLOT_VERIFICATION)
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test skip qty verification
        self._obj._short_product = False
        self._obj._region["qtyVerification"] = "1"
        
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])

        self.post_dialog_responses('skip slot')
        
        self._obj.runState(ENTER_QTY)
        
        self.validate_prompts('Quantity?',
                              'skip slot not allowed')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test skip no qty verification short product
        self._obj._short_product = True
        self._obj._region["qtyVerification"] = "0"
        
        self._obj._picks = []
        self._obj._picks.append(self.tempPickLut[1])

        self.post_dialog_responses('skip slot')
        
        self._obj.runState(ENTER_QTY)
        
        self.validate_prompts('how many did you pick?',
                              'skip slot not allowed')
        self.assertEquals(self._obj.next_state, ENTER_QTY)
        self.validate_server_requests()
        
if __name__ == '__main__':
    unittest.main()

