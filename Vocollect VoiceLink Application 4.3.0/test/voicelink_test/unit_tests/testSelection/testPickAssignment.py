from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from common.VoiceLinkLut import VoiceLinkLut
from core.VoiceLink import VoiceLink
from selection.PickPrompt import PickPromptTask
from selection.SelectionTask import SelectionTask
from vocollect_core.task.task_runner import Launch
from selection.PickAssignment import PickAssignmentTask,\
    PICK_ASSIGNMENT_CHECK_NEXT_PICK, PICK_ASSIGNMENT_PREAISLE,\
    PICK_ASSIGNMENT_AISLE, PICK_ASSIGNMENT_POSTAISLE, PICK_ASSIGNMENT_PICKPROMPT,\
    PICK_ASSIGNMENT_END_PICKING, VERIFY_LOCATION_REPLEN
import unittest

class testPickAssignmentTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        self._obj = obj_factory.get(PickAssignmentTask,
                                      sel.taskRunner, sel)
        self._obj.configure(sel._region_config_lut[0],
                            sel._assignment_lut,
                            sel._picks_lut,
                            sel._container_lut,
                            False)
       
        self.tempRegionLut = obj_factory.get(VoiceLinkLut, "prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
      
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'pickAssignment')

        #test states
        self.assertEquals(self._obj.states[0], PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.states[1], VERIFY_LOCATION_REPLEN)
        self.assertEquals(self._obj.states[2], PICK_ASSIGNMENT_PREAISLE)
        self.assertEquals(self._obj.states[3], PICK_ASSIGNMENT_AISLE)
        self.assertEquals(self._obj.states[4], PICK_ASSIGNMENT_POSTAISLE)
        self.assertEquals(self._obj.states[5], PICK_ASSIGNMENT_PICKPROMPT)
        self.assertEquals(self._obj.states[6], PICK_ASSIGNMENT_END_PICKING)
        
    def test_check_next_pick(self):
        #test check next 
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj._region =self.tempRegionLut[0]
       
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
  
        #tests to see the pre populating the status and getting back 2 records 
        self._obj.status='N'
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\nN,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 2)
        self.validate_prompts()
        self.assertEquals(self._obj._pickList, self._obj.dynamic_vocab.current_picks)
       
        #tests to see the pre populating the status and getting back 1 records
        self._obj.status='N'
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\nN,0,1,L2,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 1)
        self.validate_prompts()
        self.assertEquals(self._obj._pickList, self._obj.dynamic_vocab.current_picks)
    
    def test_verify_location(self):
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 1)
        self.validate_prompts()

        #verify off
        self._obj.runState(VERIFY_LOCATION_REPLEN)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        self.validate_prompts()
        
        #verify on - out of range
        self._obj._pickList[0]['verifyLocation'] = 1
        self.post_dialog_responses('ready')
        self._obj.runState(VERIFY_LOCATION_REPLEN)
        self.assertEquals(self._obj.next_state, VERIFY_LOCATION_REPLEN)
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')
        
        #verify on - error returned
        self.start_server()
        self.set_server_response('1,1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(VERIFY_LOCATION_REPLEN)
        self.assertEquals(self._obj.next_state, VERIFY_LOCATION_REPLEN)
        self.validate_server_requests(['prTaskLUTVerifyReplenishment','L1','ITEM12'])
        self.validate_prompts('error message, To continue say ready')
        
        #verify on - replenished = true
        self._obj.next_state = None
        self.set_server_response('1,0,\n\n')
        self._obj.runState(VERIFY_LOCATION_REPLEN)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTVerifyReplenishment','L1','ITEM12'])
        self.validate_prompts()
        self.assertFalse(self._obj._auto_short)
        self.assertEquals(self._obj._pickList[0]['verifyLocation'], 0)
        
        #verify on - replenished = false
        self._obj._pickList[0]['verifyLocation'] = 1
        self.set_server_response('0,0,\n\n')
        self._obj.runState(VERIFY_LOCATION_REPLEN)
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_PICKPROMPT)
        self.validate_server_requests(['prTaskLUTVerifyReplenishment','L1','ITEM12'])
        self.validate_prompts()
        self.assertTrue(self._obj._auto_short)
        
    def test_pre_aisle(self):
        #RALLYTC:1140 - Verify that the correct location prompts are spoken if the Pre-Aisle is the only location field that 
        #is different between the first and second Locations.
        # test to make sure pre aisle is spoken when preaisle is not empty and not equal to previous pre -aisle
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 1)
        
        self.validate_prompts()
        self._obj._pre_aisle_direction = ''
        self.post_dialog_responses('ready')
        self._obj.runState(PICK_ASSIGNMENT_PREAISLE)
        self.assertEquals(self._obj._pre_aisle_direction, 'PRE 1')
        self.validate_prompts("PRE 1")
        self.assertEquals(self._obj.next_state, None)
   
        #RALLYTC:1142:Verify that the correct location prompts are spoken if the Pre Aisle is same for Location 1 and Location 2
        self._obj.runState(PICK_ASSIGNMENT_PREAISLE)
        self.assertEquals(self._obj._pre_aisle_direction, 'PRE 1')
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
    def test_aisle(self):
        # test to make sure  aisle is spoken when  aisle is not empty and not equal to previous aisle
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 1)
        self.validate_prompts()
        
        #RALLYTC:1144 - Verify that the correct location prompts are spoken if the Pre aisle is same and Aisle is different for Location 1 and Location 2
        self._obj._aisle_direction = ''
        self._obj._pre_aisle_direction='PRE-1'
        self.post_dialog_responses('ready')
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        self.assertEquals(self._obj._aisle_direction, 'A 1')
        self.validate_prompts("Aisle A 1")
        self.assertEquals(self._obj.next_state, None)
        
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        self.assertEquals(self._obj._aisle_direction, 'A 1')
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
       
        #rallyTC:1143 - Verify that the correct location prompts are spoken if the Pre Aisle and Aisle is same for Location 1 and Location 2 
        self._obj._aisle_direction = 'A 1'
        self._obj._pre_aisle_direction='PRE-1'
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
    def test_post_aisle(self):
        # test to make sure  post aisle is spoken when  aisle is not empty and not equal to previous post aisle
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POST 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 1)
        self.validate_prompts()
       
        self._obj._post_aisle_direction = ''
        self.post_dialog_responses('ready')
        self._obj.aisle_direction='A-1'
        self._obj.runState(PICK_ASSIGNMENT_POSTAISLE)
        self.assertEquals(self._obj._post_aisle_direction, 'POST 1')
        self.validate_prompts("POST 1")
        self.assertEquals(self._obj.next_state, None)
      
        self._obj.runState(PICK_ASSIGNMENT_POSTAISLE)
        self.assertEquals(self._obj._post_aisle_direction, 'POST 1')
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
       
        #RALLYTC:1147 - Verify that the correct location prompts are spoken if the Pre , Aisle and Post Aisle is same for Location 1 and Location 2
        #RALLYTC:1148 - Verify that the pick is not repeated if the Location ID is same.
        self._obj._aisle_direction = 'A 1'
        self._obj._pre_aisle_direction='PRE-1'
        self._obj._post_aisle_direction = 'POS1'
        self._obj._picks_lut.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
    def test_end_picking(self):
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._region =self.tempRegionLut[0]
        self.start_server()

        # if we don't have any base items set status to Not picked
        self._obj.status='B'
        self._obj.runState(PICK_ASSIGNMENT_END_PICKING)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','','2','N'])
        
        self._obj.next_state = None
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_END_PICKING)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Picking complete')
        self.validate_server_requests()
        
        self._obj._picks_lut.receive("P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "S,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "\n");
        self._obj.next_state = None
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_END_PICKING)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEqual('N', self._obj._picks_lut[1]['status'])
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','','2','N'])

        self._obj._picks_lut.receive("P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "\n");
        self._obj.next_state = None
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_END_PICKING)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEqual('N', self._obj._picks_lut[1]['status'])
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','','2','N'])

        self._obj._picks_lut.receive("P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "P,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                     "\n");
        self._obj.next_state = None
        self._obj.status='N'
        self._obj.runState(PICK_ASSIGNMENT_END_PICKING)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(self._obj.next_state, None)
        self.assertEqual('P', self._obj._picks_lut[1]['status'])
        self.validate_prompts('Picking complete')
        self.validate_server_requests()

        self._obj.next_state = None
        self._obj.status='G'
        self._obj.runState(PICK_ASSIGNMENT_END_PICKING)
        self.assertEquals(self._obj.status, 'G')
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()

        
    def test_pick_prompt(self):
        #Right now just make all picks to picked
        self._obj.status='N'
       
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
    
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 1)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self._obj._region =self.tempRegionLut[0]
        self._obj._region['pickPromptType'] = '1'
       
        self.assertRaises(Launch, self._obj.runState, PICK_ASSIGNMENT_PICKPROMPT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PickPromptTask))
        self.assertEquals(self._obj.current_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'pickPrompt')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
       
        self._obj._region['pickPromptType'] = '2'
        
        self._obj.runState(PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.status, 'N')
        self.assertEquals(len(self._obj._pickList), 1)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self._obj._region =self.tempRegionLut[0]
        
        self.assertRaises(Launch, self._obj.runState, PICK_ASSIGNMENT_PICKPROMPT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PickPromptTask))
        self.assertEquals(self._obj.current_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'pickPrompt')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
     
    def test_skip_aisle(self):
        self.start_server()

        #-------------------------------------------------------------
        #test last aisle
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive('P,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n'
                                     'N,0,1,L2,1,,A 2,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n'
                                     'N,0,1,L3,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,,,0,0,0,0,0,\n'
                                     '\n');
        self._obj._pickList = [self._obj._picks_lut[1]]
        self._obj._region = self.tempRegionLut[0]
        
        self.post_dialog_responses('skip aisle')
        
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_AISLE)
        self.validate_prompts('Aisle A 2',
                              'Last aisle to be picked, you must pick it now')
        self.validate_server_requests()

        #-------------------------------------------------------------
        #test last aisle, but different pre aisles
        self._obj._aisle_direction = ''
        self._obj._picks_lut[0]['preAisle'] = '1'
        self._obj._picks_lut[1]['preAisle'] = '2'
        self._obj._picks_lut[2]['preAisle'] = '3'
        
        self.post_dialog_responses('skip aisle',
                                   'yes')
        
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.validate_prompts('Aisle A 2',
                              'Skip aisle, correct?')
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','L2','1','S'])
        self.assertEqual('P', self._obj._picks_lut[0]['status'])
        self.assertEqual('S', self._obj._picks_lut[1]['status'])
        self.assertEqual('N', self._obj._picks_lut[2]['status'])
        
        
        self._obj._picks_lut[0]['preAisle'] = ''
        self._obj._picks_lut[1]['preAisle'] = ''
        self._obj._picks_lut[2]['preAisle'] = ''
        self._obj._picks_lut[0]['status'] = 'P'
        self._obj._picks_lut[1]['status'] = 'N'
        self._obj._picks_lut[2]['status'] = 'N'
        #-------------------------------------------------------------
        #test skip aisle not allowed
        self._obj._aisle_direction = ''
        self.tempRegionLut[0]['skipAisleAllowed'] = False
        self.post_dialog_responses('skip aisle')
        
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_AISLE)
        self.validate_prompts('Aisle A 2',
                              'skip aisle not allowed')
        self.validate_server_requests()
        
        #-------------------------------------------------------------
        #test skip aisle no to confirm
        self._obj._aisle_direction = ''
        self._obj._picks_lut[0]['status'] = 'N'
        self.tempRegionLut[0]['skipAisleAllowed'] = True
        self.post_dialog_responses('skip aisle',
                                   'no')
        
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_AISLE)
        self.validate_prompts('Aisle A 2',
                              'Skip aisle, correct?')
        self.validate_server_requests()
        
        #-------------------------------------------------------------
        #test skip aisle yes to confirm
        self._obj._aisle_direction = ''
        self._obj._picks_lut[0]['status'] = 'N'
        self.tempRegionLut[0]['skipAisleAllowed'] = True
        self.post_dialog_responses('skip aisle',
                                   'yes')
        
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.validate_prompts('Aisle A 2',
                              'Skip aisle, correct?')
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','L2','1','S'])
        self.assertEqual('S', self._obj._picks_lut[1]['status'])
        self.assertEqual('S', self._obj._picks_lut[2]['status'])
        
        #-------------------------------------------------------------
        #test skip aisle yes to confirm, base items
        self._obj._aisle_direction = ''
        self._obj._picks_lut[0]['status'] = 'N'
        self._obj._picks_lut[1]['status'] = 'B'
        self._obj._picks_lut[2]['status'] = 'N'
        self.tempRegionLut[0]['skipAisleAllowed'] = True
        self.post_dialog_responses('skip aisle',
                                   'yes')
        
        self._obj.runState(PICK_ASSIGNMENT_AISLE)
        
        self.assertEquals(self._obj.next_state, PICK_ASSIGNMENT_CHECK_NEXT_PICK)
        self.validate_prompts('Aisle A 2',
                              'Skip aisle, correct?')
        self.validate_server_requests(['prTaskLUTUpdateStatus','1','L2','1','N'])
        self.assertEqual('N', self._obj._picks_lut[1]['status'])
        self.assertEqual('N', self._obj._picks_lut[2]['status'])
        
if __name__ == '__main__':
    unittest.main()
        
                
