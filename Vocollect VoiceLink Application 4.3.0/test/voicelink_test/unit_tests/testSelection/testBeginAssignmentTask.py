from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.BeginAssignment import BeginAssignment, BEGIN_ASSIGN_SUMMARY, \
    BEGIN_ASSIGN_PRINT, BEGIN_ASSIGN_BASE_INTIALIIZE, BEGIN_ASSIGN_BASE_START,\
    BEGIN_ASSIGN_BASE_PROMPT, BEGIN_ASSIGN_BASE_PICK, BEGIN_ASSIGN_BASE_NEXT
from selection.SelectionTask import SelectionTask
from core.VoiceLink import VoiceLink

import unittest
from vocollect_core.task.task_runner import Launch
from selection.GetContainerPrintLabel import GetContainerPrintLabel

class testBeginAssignment(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        self._obj = obj_factory.get(BeginAssignment,
                                      sel._region_config_lut[0],
                                      sel._assignment_lut,
                                      sel._picks_lut,
                                      sel._container_lut,
                                      sel.taskRunner, sel)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'beginAssignment')

        #test states
        self.assertEquals(self._obj.states[0], BEGIN_ASSIGN_SUMMARY)
        self.assertEquals(self._obj.states[1], BEGIN_ASSIGN_PRINT)
        self.assertEquals(self._obj.states[2], BEGIN_ASSIGN_BASE_INTIALIIZE)
        self.assertEquals(self._obj.states[3], BEGIN_ASSIGN_BASE_START)
        self.assertEquals(self._obj.states[4], BEGIN_ASSIGN_BASE_NEXT)
        self.assertEquals(self._obj.states[5], BEGIN_ASSIGN_BASE_PROMPT)
        self.assertEquals(self._obj.states[6], BEGIN_ASSIGN_BASE_PICK)

        #test LUTS defined correctly
        fields = self._obj._update_status_lut._lut_conn.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

    def test_summary_prompt(self):
        self._obj._picks_lut.receive('N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n\n')
        
        #-----------------------------------------------------------------
        #test default single assignment 0 goal time
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('ID Store 123, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test default single assignment 1 goal time
        self._obj._assignment_lut.receive('1,0,12345,Store 456,1,1,R12,0,0,0,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('ID Store 456, has a goal time of 1 minute, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test default single assignment >1 goal time
        self._obj._assignment_lut.receive('1,0,12345,Store 456,1,15,R12,0,0,0,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('ID Store 456, has a goal time of 15 minutes, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #RALLYTC: 1171 - Position of assignment is spoken in summary prompt for multiple assignments.
        #test default multiple assignments all goal times
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '1,0,12345,Store 456,2,1,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '1,0,12345,Store 789,3,15,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '\n' )
        self.post_dialog_responses('ready', 'ready', 'ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('position 1, ID Store 123, say ready',
                              'position 2, ID Store 456, has a goal time of 1 minute, say ready',
                              'position 3, ID Store 789, has a goal time of 15 minutes, say ready')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #RALLYTC: 1152 - Verify that the summary prompt is spoken if enabled.
        #RALLYTC: 1153 - Verify that the summary prompt is not spoken if disabled.
        #RALLYTC: 1154 - Verify that the summary prompt can be overridden. 
        #test override, no prompt, and normal prompt
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '1,0,12345,Store 456,2,1,R12,0,0,1,Override summary prompt,0,\n' \
                                                   + '1,0,12345,Store 789,3,15,R12,0,0,2,Override summary prompt,0,\n' \
                                                   + '\n')
        self.post_dialog_responses('ready', 'ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('position 1, ID Store 123, say ready',
                              'Override summary prompt')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #RALLYTC: 1155 - Verify that the summary prompt is not overridden if the override prompt is blank.
        #test override set, but blank prompt
        self._obj._assignment_lut.receive('1,0,12345,Store 456,1,15,R12,0,0,2,,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('ID Store 456, has a goal time of 15 minutes, say ready')
        self.validate_server_requests()
        
    def test_summary_prompt_chase(self):
        self._obj._picks_lut.receive('N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n\n')
        
        #-----------------------------------------------------------------
        #test default single assignment 0 goal time
        self._obj._assignment_lut.receive('1,1,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('chase assignment, ID Store 123, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test default single assignment 1 goal time
        self._obj._assignment_lut.receive('1,1,12345,Store 456,1,1,R12,0,0,0,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('chase assignment, ID Store 456, has a goal time of 1 minute, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test default single assignment >1 goal time
        self._obj._assignment_lut.receive('1,1,12345,Store 456,1,15,R12,0,0,0,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('chase assignment, ID Store 456, has a goal time of 15 minutes, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #RALLYTC: 1171 - Position of assignment is spoken in summary prompt for multiple assignments.
        #test default multiple assignments all goal times
        self._obj._assignment_lut.receive('1,1,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '1,1,12345,Store 456,2,1,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '1,1,12345,Store 789,3,15,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '\n' )
        self.post_dialog_responses('ready', 'ready', 'ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('chase assignment, position 1, ID Store 123, say ready',
                              'chase assignment, position 2, ID Store 456, has a goal time of 1 minute, say ready',
                              'chase assignment, position 3, ID Store 789, has a goal time of 15 minutes, say ready')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #RALLYTC: 1152 - Verify that the summary prompt is spoken if enabled.
        #RALLYTC: 1153 - Verify that the summary prompt is not spoken if disabled.
        #RALLYTC: 1154 - Verify that the summary prompt can be overridden. 
        #test override, no prompt, and normal prompt
        self._obj._assignment_lut.receive('1,1,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n' \
                                                   + '1,1,12345,Store 456,2,1,R12,0,0,1,Override summary prompt,0,\n' \
                                                   + '1,1,12345,Store 789,3,15,R12,0,0,2,Override summary prompt,0,\n' \
                                                   + '\n')
        self.post_dialog_responses('ready', 'ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('chase assignment, position 1, ID Store 123, say ready',
                              'Override summary prompt')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #RALLYTC: 1155 - Verify that the summary prompt is not overridden if the override prompt is blank.
        #test override set, but blank prompt
        self._obj._assignment_lut.receive('1,1,12345,Store 456,1,15,R12,0,0,2,,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_SUMMARY)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('chase assignment, ID Store 456, has a goal time of 15 minutes, say ready')
        self.validate_server_requests()
        


    def test_print_labels(self):
        #-----------------------------------------------------------------
        #test getting chase assignment with no region config available and no error
        self.assertRaises(Launch, self._obj.runState, BEGIN_ASSIGN_PRINT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, GetContainerPrintLabel))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'getContainerPrintLabel')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, BEGIN_ASSIGN_BASE_INTIALIIZE)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
    
    def test_base_item_summary_initialize(self):
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n\n')
        #-----------------------------------------------------------------
        #test no base items
        self._obj._picks_lut.receive('N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     'N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     '\n')

        self._obj.runState(BEGIN_ASSIGN_BASE_INTIALIIZE)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(len(self._obj.all_base_items), 0)

        #-----------------------------------------------------------------
        #test base items
        self._obj._picks_lut.receive('B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     'B,0,2,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     'N,0,3,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     '\n')

        self._obj.runState(BEGIN_ASSIGN_BASE_INTIALIIZE)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(len(self._obj.all_base_items), 1)
        self.assertEquals(self._obj.all_base_items[0]['qtyToPick'], 15)
    
    def test_base_item_summary_start(self):
        
        #-----------------------------------------------------------------
        #test no base items
        self._obj.all_base_items = []
        
        self._obj.runState(BEGIN_ASSIGN_BASE_START)

        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts()
        self.validate_server_requests()
        
        
        #-----------------------------------------------------------------
        #test no base items, response no
        self._obj.next_state = None
        self.post_dialog_responses('no')
        self._obj.all_base_items.append('dummy item')

        self._obj.runState(BEGIN_ASSIGN_BASE_START)

        self.assertEquals(self._obj.next_state, BEGIN_ASSIGN_BASE_PICK)
        self.validate_prompts('Base summary?')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test no base items, response yes
        self._obj.next_state = None
        self.post_dialog_responses('yes')
        self._obj.all_base_items.append('dummy item')

        self._obj.runState(BEGIN_ASSIGN_BASE_START)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Base summary?')
        self.validate_server_requests()
        
    def test_base_item_summary_next_prompt(self):
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive('B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     'B,0,2,L2,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     'N,0,3,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     '\n')

        #initialize iterator
        self._obj.runState(BEGIN_ASSIGN_BASE_INTIALIIZE)
        self.post_dialog_responses('yes')
        self._obj.runState(BEGIN_ASSIGN_BASE_START)
        self.validate_prompts('Base summary?')
        
        #-----------------------------------------------------------------
        #test base item prompt first base item
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_BASE_NEXT)
        self.assertEquals(self._obj.next_state, None)
        self._obj.runState(BEGIN_ASSIGN_BASE_PROMPT)

        self.assertEquals(self._obj.next_state, BEGIN_ASSIGN_BASE_NEXT)
        self.validate_prompts('pre 1, aisle A 1, post 1, slot S 1, item description, quantity 10, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test base item prompt second base item
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(BEGIN_ASSIGN_BASE_NEXT)
        self.assertEquals(self._obj.next_state, None)
        self._obj.runState(BEGIN_ASSIGN_BASE_PROMPT)

        self.assertEquals(self._obj.next_state, BEGIN_ASSIGN_BASE_NEXT)
        self.validate_prompts('pre 1, aisle A 1, post 1, slot S 1, item description, quantity 5, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test no more base items
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(BEGIN_ASSIGN_BASE_NEXT)
        self.assertEquals(self._obj.next_state, BEGIN_ASSIGN_BASE_PICK)
        self.validate_prompts()
        self.validate_server_requests()
        
        #initialize iterator
        self._obj.runState(BEGIN_ASSIGN_BASE_INTIALIIZE)
        self.post_dialog_responses('yes')
        self._obj.runState(BEGIN_ASSIGN_BASE_START)
        self.validate_prompts('Base summary?')

        #-----------------------------------------------------------------
        #test cancel base item prompt, no 
        self._obj.next_state = None
        self.post_dialog_responses('cancel', 'no')
        
        self._obj.runState(BEGIN_ASSIGN_BASE_NEXT)
        self.assertEquals(self._obj.next_state, None)
        self._obj.runState(BEGIN_ASSIGN_BASE_PROMPT)

        self.assertEquals(self._obj.next_state, BEGIN_ASSIGN_BASE_PROMPT)
        self.validate_prompts('pre 1, aisle A 1, post 1, slot S 1, item description, quantity 10, say ready', 
                              'cancel base summary?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #test cancel base item prompt, yes 
        self._obj.next_state = None
        self.post_dialog_responses('cancel', 'yes')
        
        self._obj.runState(BEGIN_ASSIGN_BASE_PROMPT)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('pre 1, aisle A 1, post 1, slot S 1, item description, quantity 10, say ready', 
                              'cancel base summary?')
        self.validate_server_requests()
    
    def test_base_item_summary_pick(self):
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n\n')
        self._obj._picks_lut.receive('B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     'B,0,2,L2,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     'N,0,3,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
                                     '\n')

        #-----------------------------------------------------------------
        #test yes to pick base items
        self.post_dialog_responses('yes')
        
        self._obj.runState(BEGIN_ASSIGN_BASE_PICK)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Pick base items?')
        self.validate_server_requests()
        self.assertEquals(self._obj._picks_lut[0]['status'], 'B')
        self.assertEquals(self._obj._picks_lut[1]['status'], 'B')
        self.assertEquals(self._obj._picks_lut[2]['status'], 'N')
         
        #-----------------------------------------------------------------
        #test no to pick base items, with error returned
        self.start_server()
        self.set_server_response('1,Test Error\n\n')
        self.post_dialog_responses('no', 'ready')
        
        self._obj.runState(BEGIN_ASSIGN_BASE_PICK)

        self.assertEquals(self._obj.next_state, BEGIN_ASSIGN_BASE_PICK)
        self.validate_prompts('Pick base items?',
                              'Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTUpdateStatus', '1', '', '2', 'N'])
        self.assertEquals(self._obj._picks_lut[0]['status'], 'B')
        self.assertEquals(self._obj._picks_lut[1]['status'], 'B')
        self.assertEquals(self._obj._picks_lut[2]['status'], 'N')
        
        
        #-----------------------------------------------------------------
        #test no to pick base items, with no error returned
        self.set_server_response('0,\n\n')
        self.post_dialog_responses('no')
        
        self._obj.runState(BEGIN_ASSIGN_BASE_PICK)

        self.assertEquals(self._obj.next_state, BEGIN_ASSIGN_BASE_PICK)
        self.validate_prompts('Pick base items?')
        self.validate_server_requests(['prTaskLUTUpdateStatus', '1', '', '2', 'N'])
        self.assertEquals(self._obj._picks_lut[0]['status'], 'N')
        self.assertEquals(self._obj._picks_lut[1]['status'], 'N')
        self.assertEquals(self._obj._picks_lut[2]['status'], 'N')
        
        
        
#    def test_base_item_summary(self):
#        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n\n')
#        
#        #-----------------------------------------------------------------
#        #test no base items
#        self._obj._picks_lut.receive('N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                              'N,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                              '\n')
#        
#        self._obj.runState(BEGIN_ASSIGN_BASE_SUMM_START)
#
#        self.assertEquals(self._obj.next_state, None)
#        self.validate_prompts()
#        self.validate_server_requests()
#        
#        self.start_server()
#
#        #-----------------------------------------------------------------
#        #RALLYTC: 1163 - Base items are not picked first.
#        #RALLYTC: 1166 - Error message returned in response to the Update Status LUT.
#        #test no to hear summary, no to pick, with error from host 
#        self._obj._picks_lut.receive('B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     '\n')
#
#        self.set_server_response('1,Test Error\n\n')
#        self.set_server_response('0,\n\n')
#        self.post_dialog_responses('no', 'no', 'ready', 'no')
#        
#        self._obj.runState(BEGIN_ASSIGN_BASE_SUMM_START)
#
#        self.assertEquals(self._obj.next_state, None)
#        self.validate_prompts('Base summary?',
#                              'Pick base items?',
#                              'Test Error, To continue say ready',
#                              'Pick base items?')
#        self.validate_server_requests(['prTaskLUTUpdateStatus','1','','2','N'],
#                                      ['prTaskLUTUpdateStatus','1','','2','N'])
#        self.assertEquals(self._obj._picks_lut[0]['status'], 'N')
#        self.assertEquals(self._obj._picks_lut[1]['status'], 'N')
#
#        #-----------------------------------------------------------------
#        #RALLYTC: 1161 - Verify that the base summary is not spoken if selector responds 'no' to Base summary.
#        #RALLYTC: 1165 - Verify that the Update Status LUT contains the correct fields and data.
#        #test no to hear summary, yes to pick 
#        self._obj._picks_lut.receive('B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     '\n')
#
#        self.set_server_response('1,Test Error\n\n')
#        self.post_dialog_responses('no', 'no', 'ready', 'yes')
#        
#        self._obj.runState(BEGIN_ASSIGN_BASE_SUMM_START)
#
#        self.assertEquals(self._obj.next_state, None)
#        self.validate_prompts('Base summary?',
#                              'Pick base items?',
#                              'Test Error, To continue say ready',
#                              'Pick base items?')
#        self.validate_server_requests(['prTaskLUTUpdateStatus','1','','2','N'])
#        self.assertEquals(self._obj._picks_lut[0]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[1]['status'], 'B')
#
#
#        #-----------------------------------------------------------------
#        #RALLYTC: 1160 - Verify that the Base summary is spoken if there are Base Picks in the assignment.
#        #RALLYTC: 1164 - Base items are picked first. 
#        #test yes to base summary and grouping 
#        self._obj._region['containerType'] = '0'
#        self._obj._picks_lut.receive('B,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,2,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,3,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,4,L3,1,,A 3,,S 3,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'N,0,5,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,6,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     '\n')
#
#        self.post_dialog_responses('yes',
#                                   'ready', 
#                                   'ready',
#                                   'ready', 
#                                   'yes')
#        
#        self._obj.runState(BEGIN_ASSIGN_BASE_SUMM_START)
#
#        self.assertEquals(self._obj.next_state, None)
#        self.validate_prompts('Base summary?',
#                              ', aisle A 1, , slot S 1, item description, quantity 15, say ready', #group 3 including status N
#                              'pre 2, aisle A 2, post 2, slot S 2, item description, quantity 10, say ready', #group 2
#                              ', aisle A 3, , slot S 3, item description, quantity 5, say ready', #single item
#                              'Pick base items?') #no more summaries should be spoken 
#        self.validate_server_requests()
#        self.assertEquals(self._obj._picks_lut[0]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[1]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[2]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[3]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[4]['status'], 'N')
#        self.assertEquals(self._obj._picks_lut[5]['status'], 'B')
#
#        #-----------------------------------------------------------------
#        #test yes to base summary and canceling
#        self._obj._region['containerType'] = '1'
#        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,0,R12,0,0,0,Override summary prompt,0,\n' 
#                                          '1,0,12345,Store 456,2,1,R12,0,0,0,Override summary prompt,0,\n' 
#                                          '1,0,12345,Store 789,3,15,R12,0,0,0,Override summary prompt,0,\n' 
#                                          '\n' )
#        self._obj._picks_lut.receive('B,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,2,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,3,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,4,L3,1,,A 3,,S 3,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'N,0,5,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,6,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     '\n')
#
#        self.post_dialog_responses('yes',
#                                   'cancel',
#                                   'no', 
#                                   'ready',
#                                   'cancel',
#                                   'yes',
#                                   'yes')
#        
#        self._obj.runState(BEGIN_ASSIGN_BASE_SUMM_START)
#
#        self.assertEquals(self._obj.next_state, None)
#        self.validate_prompts('Base summary?',
#                              ', aisle A 1, , slot S 1, item description, quantity 15, say ready', #group 3 including status N
#                              'cancel base summary?',
#                              ', aisle A 1, , slot S 1, item description, quantity 15, say ready', #repeat previous prompt
#                              'pre 2, aisle A 2, post 2, slot S 2, item description, quantity 10, say ready', #group 2
#                              'cancel base summary?', 
#                              'Pick base items?') #no more summaries should be spoken 
#        self.validate_server_requests()
#        self.assertEquals(self._obj._picks_lut[0]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[1]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[2]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[3]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[4]['status'], 'N')
#        self.assertEquals(self._obj._picks_lut[5]['status'], 'B')
#
#        #-----------------------------------------------------------------
#        #test ID is spoken 
#        self._obj._region['containerType'] = '0'
#        self._obj._picks_lut.receive('B,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,2,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,3,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,4,L3,1,,A 3,,S 3,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'N,0,5,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     'B,0,6,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,\n' \
#                                     '\n')
#
#        self.post_dialog_responses('yes',
#                                   'cancel',
#                                   'no', 
#                                   'ready',
#                                   'cancel',
#                                   'yes',
#                                   'yes')
#        
#        self._obj.runState(BEGIN_ASSIGN_BASE_SUMM_START)
#
#        self.assertEquals(self._obj.next_state, None)
#        self.validate_prompts('Base summary?', 
#                              ', aisle A 1, , slot S 1, item description, ID Store 123, quantity 15, say ready', 
#                              'cancel base summary?', 
#                              ', aisle A 1, , slot S 1, item description, ID Store 123, quantity 15, say ready', 
#                              'pre 2, aisle A 2, post 2, slot S 2, item description, ID Store 123, quantity 10, say ready', 
#                              'cancel base summary?', 
#                              'Pick base items?') 
#        self.validate_server_requests()
#        self.assertEquals(self._obj._picks_lut[0]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[1]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[2]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[3]['status'], 'B')
#        self.assertEquals(self._obj._picks_lut[4]['status'], 'N')
#        self.assertEquals(self._obj._picks_lut[5]['status'], 'B')


if __name__ == '__main__':
    unittest.main()
