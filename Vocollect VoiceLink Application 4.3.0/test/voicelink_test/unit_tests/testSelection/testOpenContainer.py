from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.OpenContainer import OpenContainer, OPEN_CONTAINER_OPEN
from selection.SelectionPrint import SelectionPrintTask
from selection.SelectionTask import SelectionTask
from selection.BeginAssignment import BeginAssignment
from selection.GetContainerPrintLabel import GetContainerPrintLabel
from core.VoiceLink import VoiceLink
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core.task.task_runner import Launch
import unittest

##########################################
#Test Open Container                     #
##########################################
class TestOpenContainer(BaseVLTestCase):

    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        begin_assign = obj_factory.get(BeginAssignment,
                                         sel._region_config_lut[0],
                                         sel._assignment_lut,
                                         sel._picks_lut,
                                         sel._container_lut,
                                         sel.taskRunner, sel)
        
        containerPrintLabel = obj_factory.get(GetContainerPrintLabel,
                                                begin_assign._region ,
                                                begin_assign._assignment_lut, 
                                                begin_assign._picks_lut, 
                                                begin_assign._container_lut,
                                                begin_assign.taskRunner, begin_assign)

        self._obj  = obj_factory.get(OpenContainer,
                                       containerPrintLabel._region, 
                                       containerPrintLabel._assignment, 
                                       containerPrintLabel._picks, 
                                       containerPrintLabel._container_lut,
                                       False,
                                       containerPrintLabel.taskRunner,
                                       containerPrintLabel)
        self.tempRegionLut = obj_factory.get(VoiceLinkLut, "prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
       
        self.tempAssignmentLut = obj_factory.get(VoiceLinkLut, "prTaskLUTGetAssignment")
        self.tempAssignmentLut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        
   
    def test_initializeStates(self):
        ''' Initialize States'''
              
        #test name
        self.assertEquals(self._obj.name, 'openContainer')

        #test states
        self.assertEquals(self._obj.states[0], OPEN_CONTAINER_OPEN)


    def test_open_container(self):
        ''' tests open container'''
         
        #test to check the prompt operator for container ID set to true
        self._obj._region = self.tempRegionLut[0]
        self.start_server()
        
        self._obj._picks.receive("N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n"
                                 "N,0,1,L1,1,PRE 1,A 1,POS1 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n");
                                     
        self._obj._assignment = self.tempAssignmentLut[0]                       
        self._obj._set_variables()
   
   
        #TESLINK 95642 :: Test Case Verify correct behavior if VoiceLink region profile 104 is used.
        self._obj._region['promptForContainer'] = 1
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,0,\n\n')
        self.post_dialog_responses('123!', 'yes')
        self._obj.runState(OPEN_CONTAINER_OPEN)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTContainer','1','12345','','','123','2',''])
        self.validate_prompts('New Container ID?',
                              '123, correct?')
         
        #test to check the prompt operator for container ID set to false
        #TESLINK 95640 :: Test Case Verify correct behavior if VoiceLink region profile 103 is used.
        #TESTLINK 95684 :: Test Case Verify correct behavior if list of available containers is returned.
        self._obj._multiple_assignments = True
        self._obj._set_variables()
        self._obj._region['promptForContainer'] = 0
        self.set_server_response('1,0000000003,03,12345,Store 123,,O,0,0,\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(OPEN_CONTAINER_OPEN)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTContainer', '1','12345','','','','2',''])
        self.validate_prompts('Open 03 Position 1')
         
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,0,\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(OPEN_CONTAINER_OPEN)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTContainer', '1','12345','','','','2',''])
        self.validate_prompts('Open 03 Position 1')
         
        self._obj._position=''
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,0,\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(OPEN_CONTAINER_OPEN)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTContainer', '1','12345','','','','2',''])
        self.validate_prompts('Open 03')
         
        #test no containers returned from host
        self._obj._set_variables()
        self.set_server_response('1,0000000003,03,12344,Store 123,,C,0,0,\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(OPEN_CONTAINER_OPEN)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTContainer', '1','12345','','','','2',''])
        self.validate_prompts('No containers returned from host')
            
        # verify error message
        self._obj._region['promptForContainer'] = 0
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,199,Error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(OPEN_CONTAINER_OPEN)
        self.assertEquals(self._obj.next_state, OPEN_CONTAINER_OPEN)
        self.validate_server_requests(['prTaskLUTContainer', '1','12345','','','','2',''])
        self.validate_prompts('Error message, To continue say ready')
         
        #verify print
        self._obj._region['promptForContainer'] = 0
        self._obj._region['printLabels'] = '1'
        self.set_server_response('1,0000000003,03,12345,Store 123,,O,0,0,\n\n')
        self.post_dialog_responses('ready')
        self.assertRaises(Launch, self._obj.runState, OPEN_CONTAINER_OPEN)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, '')
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')
        
if __name__ == '__main__':
    unittest.main()
           
