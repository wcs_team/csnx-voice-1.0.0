from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.GetAssignment import GetAssignmentAuto, GET_ASSIGN_START,\
    GET_ASSIGN_XMIT_ASSIGNMENT, GET_ASSIGN_PROMPT_REVERSE, GET_ASSIGN_XMIT_PICK,\
    GET_ASSIGN_ENTER_WORKID, GET_ASSIGN_XMIT_WORKID, GET_ASSIGN_REVIEW_WORKIDS,\
    GetAssignmentManual
from selection.SelectionTask import SelectionTask
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask

import unittest
from vocollect_core.task.task_runner import Launch, TaskRunnerBase

class testGetAssignmentAuto(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1
        sel = obj_factory.get(SelectionTask, 3, temp.taskRunner)
        self._obj = obj_factory.get(GetAssignmentAuto,
                                      sel._region_config_lut,
                                      sel._assignment_lut,
                                      sel._picks_lut,
                                      False,
                                      sel.taskRunner, sel)
        TaskRunnerBase._main_runner = self._obj.taskRunner
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'getAssignmentAuto')

        #test states
        self.assertEquals(self._obj.states[0], GET_ASSIGN_START)
        self.assertEquals(self._obj.states[1], GET_ASSIGN_XMIT_ASSIGNMENT)
        self.assertEquals(self._obj.states[2], GET_ASSIGN_PROMPT_REVERSE)
        self.assertEquals(self._obj.states[3], GET_ASSIGN_XMIT_PICK)


    def test_start(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,0,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]

        #-----------------------------------------------------------------
        # test not in progress
        self._obj._inprogress_work = False
        self._obj.runState(GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        # test in progress
        self._obj._inprogress_work = True
        self._obj.runState(GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_ASSIGNMENT)
        self.validate_prompts()
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #RALLYTC: 1184 - Verify that operator is prompted for number of assignments if more than one allowed.
        #RALLYTC: 1188 - Verify that error message is spoken if more assignments than allowed are requested.
        # test multiple work IDs
        self._obj._inprogress_work = False
        self._obj._region_config_lut.receive('1,dry grocery,1,1,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self.post_dialog_responses('0', '4', '2','ready')

        self._obj.runState(GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_ASSIGNMENT)
        self.validate_prompts('How many assignments?',
                              'The number of work IDs must be between 1 and 3, try again',
                              'How many assignments?',
                              'The number of work IDs must be between 1 and 3, try again',
                              'How many assignments?',
                              'To receive work, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj._number_work_ids, 2)

        #-----------------------------------------------------------------
        # test change function, No
        self.start_server()
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_START)
        self.validate_prompts('How many assignments?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        # test change function, Yes
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('How many assignments?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        # test change region
        self._obj.next_state = None
        self._obj.callingTask._region_selected = True
        self.post_dialog_responses('change region')

        self.assertRaises(Launch, self._obj.runState, GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('How many assignments?')
        self.validate_server_requests()


    def test_start_cont(self):
        self.start_server()
        self._obj._region_config_lut.receive('1,dry grocery,1,1,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,0,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        #-----------------------------------------------------------------
        # test change function at receive work prompt
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj.next_state = None
        self.post_dialog_responses('3!', 'change function', 'no', 'change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('How many assignments?',
                              'To receive work, say ready', 
                              'change function, correct?',
                              'To receive work, say ready', 
                              'change function, correct?'
                              )
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'])


        #-----------------------------------------------------------------
        # test change region
        self._obj.next_state = None
        self._obj.callingTask._region_selected = True
        self.post_dialog_responses('3!', 'change region')

        self.assertRaises(Launch, self._obj.runState, GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('How many assignments?',
                              'To receive work, say ready')
        self.validate_server_requests()
        
    def test_xmit_assignemnts(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]

        #-----------------------------------------------------------------
        #RALLYTC: 1099 - Verify that the Get Assignment LUT is re-sent if a timeout occurs.
        # test contacting server error
        self.post_dialog_responses('ready')

        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)
        
        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_ASSIGNMENT)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        
        self.start_server()
        #-----------------------------------------------------------------
        #RALLYTC: 1098 - Error returned in response to Get Assignment LUT
        # test Lut error
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,1,Test Error\n\n')
        self.post_dialog_responses('ready')
        self._obj._number_work_ids = 1
        
        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_START)
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTGetAssignment', '1', '1'])

        #-----------------------------------------------------------------
        #RALLYTC: 1106 - Verify getting an assignment in a region with automatic issuance.
        #RALLYTC: 1120 - Verify that the correct fields are included in the Get Assignment LUT request.
        # test Lut success
        self._obj.next_state = None
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')

        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetAssignment', '1', '1'])

        #-----------------------------------------------------------------
        # TESTLINK 95315 :: test Lut for manual issuance
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj.next_state = None
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')

        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetAssignment', '', '1'])        

        #-----------------------------------------------------------------
        # test less received singular
        self._obj._region_config_lut.receive('1,dry grocery,1,1,4,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj.next_state = None
        self._obj._number_work_ids = 3
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('only 1 work ID available, say ready')
        self.validate_server_requests(['prTaskLUTGetAssignment', '3', '1'])        

        #-----------------------------------------------------------------
        # test less received singular
        self._obj._region_config_lut.receive('1,dry grocery,1,1,4,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj.next_state = None
        self._obj._number_work_ids = 3
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                       '1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('only 2 work IDs available, say ready')
        self.validate_server_requests(['prTaskLUTGetAssignment', '3', '1'])        

        #-----------------------------------------------------------------
        # test less received in-progress work
        self._obj._region_config_lut.receive('1,dry grocery,1,1,4,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj.next_state = None
        self._obj._number_work_ids = 3
        self._obj._inprogress_work = True
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                       '1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')

        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetAssignment', '3', '1'])        

        #-----------------------------------------------------------------
        # test less received in-progress work
        self._obj._region_config_lut.receive('1,dry grocery,1,1,4,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj.next_state = None
        self._obj._number_work_ids = 2
        self._obj._inprogress_work = False
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n'
                                       '1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')

        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetAssignment', '2', '1'])        

        #-----------------------------------------------------------------
        # test Error code 2 received, no message spoken, state set to ''
        self._obj._region_config_lut.receive('1,dry grocery,1,1,4,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj.next_state = None
        self._obj._inprogress_work = False
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,2,\n'
                                 '1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,2,\n\n')

        self.assertEquals(0, self._obj._assignment_lut.last_error)
        self._obj.runState(GET_ASSIGN_XMIT_ASSIGNMENT)

        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetAssignment', '2', '1'])        
        self.assertEquals(2, self._obj._assignment_lut.last_error)

    def test_prompt_reverse(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        
        #-----------------------------------------------------------------
        # test reverse not allowed
        self._obj.runState(GET_ASSIGN_PROMPT_REVERSE)
        
        self.assertEquals(self._obj._picks_lut._picking_order, 0)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()

        #-----------------------------------------------------------------
        # test reverse allowed, no response
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,1,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self.post_dialog_responses('no')

        self._obj.runState(GET_ASSIGN_PROMPT_REVERSE)
        
        self.assertEquals(self._obj._picks_lut._picking_order, 0)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Pick in reverse order?',
                              'receiving picks in normal order, please wait')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #RALLYTC: 1105 - Verify that operator is asked for reverse picking.
        # test reverse allowed, yes response
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,1,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self.post_dialog_responses('yes')

        self._obj.runState(GET_ASSIGN_PROMPT_REVERSE)

        self.assertEquals(self._obj._picks_lut._picking_order, 1)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Pick in reverse order?',
                              'receiving picks in reverse order, please wait')
        self.validate_server_requests()

    def test_xmit_picks(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj._assignment_lut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')

        #-----------------------------------------------------------------
        #RALLYTC: 1101 - Verify that the Get Picks LUT is re-sent if a timeout occurs.
        # test contacting server error
        self.post_dialog_responses('ready')

        self._obj.runState(GET_ASSIGN_XMIT_PICK)
        
        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_PICK)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        
        self.start_server()
        #-----------------------------------------------------------------
        # test Lut error
        self._obj._assignment_lut[0]['passAssignment'] = '1'
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,1,Test Error\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(GET_ASSIGN_XMIT_PICK)
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTGetPicks', '1', '1', '0', '0'])
        self._obj._assignment_lut[0]['passAssignment'] = '0'

        #-----------------------------------------------------------------
        #RALLYTC: 1121 - Verify that the correct fields are included in the Get Picks LUT request.
        # test Lut success (different group ID, and reverse)
        self._obj._assignment_lut.receive('2,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks_lut._picking_order = 1
        self._obj.next_state = None
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n')

        self._obj.runState(GET_ASSIGN_XMIT_PICK)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetPicks', '2', '0', '0', '1'])
        
        #pick by pick mode
        self._obj._assignment_lut[0]['passAssignment'] = '0'
        self._obj._region_config_rec['pickByPick'] = 1
        self.set_server_response('B,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n')
        self._obj.runState(GET_ASSIGN_XMIT_PICK)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetPicks', '2', '0', '0', '1'])
        self.assertEquals(self._obj._picks_lut[0]['status'], 'N') 
        
        # go back for shorts mode
        self._obj._pick_only = True
        self._obj._region_config_rec['pickByPick'] = 0
        self.set_server_response('S,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\
                                N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\
                                G,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n\n')
        self._obj.runState(GET_ASSIGN_XMIT_PICK)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTGetPicks', '2', '0', '0', '1'])
        self.assertEquals(self._obj._picks_lut[0]['status'], 'N')
        self.assertEquals(self._obj._picks_lut[1]['status'], 'X')
        self.assertEquals(self._obj._picks_lut[2]['status'], 'N')
        
    def test_pick_by_pick(self):
        #test pick by pick mode state transition
        sel = SelectionTask(3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,1,4,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,1,0,\n\n')
        sel._region_config_lut[0]['pickByPick'] = 1
        self._obj = GetAssignmentAuto(sel._region_config_lut,
                                            sel._assignment_lut,
                                            sel._picks_lut,
                                            True,
                                            sel.taskRunner, sel)
      
        
        self.assertEquals(self._obj.current_state, GET_ASSIGN_XMIT_PICK)
        
    def test_pick_only(self):
        #test pick by pick mode state transition
        sel = SelectionTask(3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,1,4,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
      
        self._obj = GetAssignmentAuto(sel._region_config_lut,
                                            sel._assignment_lut,
                                            sel._picks_lut,
                                            True,
                                            sel.taskRunner, sel)
      
        self._obj._region_config_lut[0]['pickByPick'] = 0
        self.assertEquals(self._obj.current_state, GET_ASSIGN_XMIT_PICK)
        
        
        
class testGetAssignmentManual(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1
        sel = obj_factory.get(SelectionTask, 3, temp.taskRunner)
        self._obj = obj_factory.get(GetAssignmentManual,
                                      sel._region_config_lut,
                                      sel._assignment_lut,
                                      sel._picks_lut,
                                      False,
                                      sel.taskRunner, sel)
        TaskRunnerBase._main_runner = self._obj.taskRunner

    def test_initializeStates(self):
        #test states
        self.assertEquals(self._obj.states[0], GET_ASSIGN_START)
        self.assertEquals(self._obj.states[1], GET_ASSIGN_ENTER_WORKID)
        self.assertEquals(self._obj.states[2], GET_ASSIGN_XMIT_WORKID)
        self.assertEquals(self._obj.states[3], GET_ASSIGN_REVIEW_WORKIDS)
        self.assertEquals(self._obj.states[4], GET_ASSIGN_XMIT_ASSIGNMENT)
        self.assertEquals(self._obj.states[5], GET_ASSIGN_PROMPT_REVERSE)
        self.assertEquals(self._obj.states[6], GET_ASSIGN_XMIT_PICK)
        
        #test LUTS defined correctly
        fields = self._obj._work_request_lut.fields
        self.assertEquals(fields['workid'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
    
    def test_start(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]

        #-----------------------------------------------------------------
        # test not in progress
        self._obj._inprogress_work = False
        self._obj.runState(GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        # test in progress
        self._obj._inprogress_work = True
        self._obj.runState(GET_ASSIGN_START)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_ASSIGNMENT)
        self.validate_prompts()
        self.validate_server_requests()

    def test_enter_workid(self):

        #-----------------------------------------------------------------
        #RALLYTC: 1195 - Verify Work ID is requested for manual issuance.
        # test specified length
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self.post_dialog_responses('1234', 'yes')

        self._obj.runState(GET_ASSIGN_ENTER_WORKID)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Work ID?',
                              '1234, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj._workid, '1234')
        self.assertEquals(self._obj._workid_scanned, False)
        
        #-----------------------------------------------------------------
        # TESTLINK 95267 :: Verify if the work ID can be scanned both numeric and alpha numeric. (Cannot test actual scan)
        # test no length
        self._obj._region_config_lut.receive('1,dry grocery,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,-1,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self.post_dialog_responses('1234!', 'yes')

        self._obj.runState(GET_ASSIGN_ENTER_WORKID)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Work ID?', 
                              '1234, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj._workid, '1234')
        # TESTLINK 95281 :: Verify speaking complete workID (Region work id length = -1) sets scanned to true (full workID) 
        self.assertEquals(self._obj._workid_scanned, True)
        
        #-----------------------------------------------------------------
        #TESTLINK 9250 :: Verify if No more can be spoken during work ID prompt.
        # test no more none specified
        self._obj._region_config_lut.receive('1,dry grocery,1,0,3,1,1,0,0,0,1,1,0,0,0,1,1,-1,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self.post_dialog_responses('no more')

        self._obj.runState(GET_ASSIGN_ENTER_WORKID)
        
        self.assertEquals(self._obj.next_state, GET_ASSIGN_ENTER_WORKID)
        self.validate_prompts('Work ID?',
                              'You must enter at least one work ID, try again.')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        # test no more none specified, more region configs
        self._obj._region_config_lut.receive('1,dry grocery,1,0,3,1,1,0,0,0,1,1,0,0,0,1,1,-1,0,0,0,,,,,1,0,0,2,3,\n'
                                             '1,dry grocery,1,0,3,1,1,0,0,0,1,1,0,0,0,1,1,-1,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj._region_config_lut._current_region_rec = 0
        self.post_dialog_responses('no more')

        self._obj.runState(GET_ASSIGN_ENTER_WORKID)
        
        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts('Work ID?')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        # test no more none specified, no more region configs
        self._obj._region_config_lut._current_region_rec = 1
        self.post_dialog_responses('no more')

        self._obj.runState(GET_ASSIGN_ENTER_WORKID)
        
        self.assertEquals(self._obj.next_state, GET_ASSIGN_ENTER_WORKID)
        self.validate_prompts('Work ID?',
                              'You must enter at least one work ID, try again.')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #TESTLINK 9250 :: Verify if No more can be spoken during work ID prompt.
        # test no more 1 specified
        self._obj._region_config_lut.receive('1,dry grocery,1,0,3,1,1,0,0,0,1,1,0,0,0,1,1,-1,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self._obj._number_work_ids = 1
        self.post_dialog_responses('no more')

        self._obj.runState(GET_ASSIGN_ENTER_WORKID)
        
        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_ASSIGNMENT)
        self.validate_prompts('Work ID?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        # test change function, No
        self.start_server()
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(GET_ASSIGN_ENTER_WORKID)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_ENTER_WORKID)
        self.validate_prompts('Work ID?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        # test change function, Yes
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, GET_ASSIGN_ENTER_WORKID)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Work ID?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        # test change region
        self._obj.next_state = None
        self._obj.callingTask._region_selected = True
        self.post_dialog_responses('change region')

        self.assertRaises(Launch, self._obj.runState, GET_ASSIGN_ENTER_WORKID)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Work ID?')
        self.validate_server_requests()
        

    def test_xmit_workId(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        
        #-----------------------------------------------------------------
        #RALLYTC: 1198 - Verify that the Selector is notified if timeout occurs on Request Work LUT.
        # TESTLINK 95325 :: test server error
        self._obj._workid = '1234'
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_ASSIGN_XMIT_WORKID)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_WORKID)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        self.start_server()

        #-----------------------------------------------------------------
        #RALLYTC: 1199 - Verify that the Request work LUT contains the correct fields and data.
        # test no error
        self._obj._region_config_lut.receive('1,dry grocery,2,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]
        self.set_server_response('1,0,\n\n')
        self._obj._workid = '4567'
        self._obj.next_state = None
        
        self._obj.runState(GET_ASSIGN_XMIT_WORKID)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTRequestWork','4567','1','2'])
        
        #-----------------------------------------------------------------
        #RALLYTC: 1202 - Verify error message if host returns error in response to Request Work LUT
        # TESTLINK 95267 :: Verify if the work ID can be scanned both numeric and alpha numeric. (Cannot test actual scan)
        # TESTLINK 95327 :: Test Case Verify the selector hears an error msg if an error or warning is returned
        # test host returned error
        self.set_server_response('1,5,Error message\n\n')
        self._obj._workid = '1234'
        self._obj._workid_scanned = True
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_ASSIGN_XMIT_WORKID)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_ENTER_WORKID)
        self.validate_prompts('Error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTRequestWork','1234','0','2'])
        
        #-----------------------------------------------------------------
        #RALLYTC: 1200 - Verify correct flow is taken if error code 3 is returned.
        # TESTLINK 95322 :: test host returned error code 3 - no message spoken
        self.set_server_response('1,3,Error message\n\n')
        self._obj._workid = '1234'
        self._obj.next_state = None
        
        self._obj.runState(GET_ASSIGN_XMIT_WORKID)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTRequestWork','1234','0','2'])
        
        #-----------------------------------------------------------------
        # TESTLINK 95138 :: test host returned error code 4 - no message spoken
        self.set_server_response('1,4,Error message\n\n')
        self._obj._workid = '1234'
        self._obj.next_state = None
        
        self._obj.runState(GET_ASSIGN_XMIT_WORKID)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTRequestWork','1234','0','2'])
        
    def test_review_workids(self):
        self._obj._region_config_lut.receive('1,dry grocery,1,0,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        self._obj._region_config_rec = self._obj._region_config_lut[0]

        #-----------------------------------------------------------------
        # TESTLINK 95322 :: test error code 3
        self._obj._work_request_lut.receive('1234A,3,Error message\n\n')
        self._obj._number_work_ids = 0
        self._obj.next_state = None
        
        self._obj.runState(GET_ASSIGN_REVIEW_WORKIDS)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._number_work_ids, 1)
        
        #-----------------------------------------------------------------
        # test no error code, max not reached
        self._obj._work_request_lut.receive('1234A,0,\n\n')
        self._obj._number_work_ids = 0
        self._obj.next_state = None
        
        self._obj.runState(GET_ASSIGN_REVIEW_WORKIDS)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_ENTER_WORKID)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._number_work_ids, 1)
        
        #-----------------------------------------------------------------
        #RALLYTC: 1197 - Number of work IDs requested equal to number allowed.
        # test no error code, max reached
        self._obj._work_request_lut.receive('1234A,0,\n\n')
        self._obj._number_work_ids = 2
        self._obj.next_state = None
        
        self._obj.runState(GET_ASSIGN_REVIEW_WORKIDS)

        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._number_work_ids, 3)
        
        #-----------------------------------------------------------------
        #RALLYTC: 1201 - Verify flow is correct if error code 4 is returned.
        # test error code 4, no to review
        self._obj._work_request_lut.receive('1234A,4,\n'
                                                     '1234A,4,\n'
                                                     '1234A,4,\n'
                                                     '1234A,4,\n\n')
        self._obj._number_work_ids = 0
        self._obj.next_state = None
        self.post_dialog_responses('no')
        
        self._obj.runState(GET_ASSIGN_REVIEW_WORKIDS)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_ENTER_WORKID)
        self.validate_prompts("found 4 work IDs, do you want to review the list?")
        self.validate_server_requests()
        self.assertEquals(self._obj._number_work_ids, 0)

        #-----------------------------------------------------------------
        # TESTLINK 95138 :: test error code 4, with review, canceling
        self._obj._work_request_lut.receive('1234A,4,\n'
                                                     '1234B,4,\n'
                                                     '1234C,4,\n'
                                                     '1234D,4,\n\n')
        self._obj._number_work_ids = 0
        self._obj.next_state = None
        self.post_dialog_responses('yes','no','no','no','no',
                                   'cancel','no','cancel','yes')
        
        self._obj.runState(GET_ASSIGN_REVIEW_WORKIDS)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_ENTER_WORKID)
        self.validate_prompts("found 4 work IDs, do you want to review the list?",
                              '<spell>1234A</spell>, select?',
                              '<spell>1234B</spell>, select?',
                              '<spell>1234C</spell>, select?',
                              '<spell>1234D</spell>, select?',
                              "End of matching work IDs, starting over.",
                              '<spell>1234A</spell>, select?',
                              'Cancel this work ID request?',
                              '<spell>1234A</spell>, select?',
                              'Cancel this work ID request?')
        self.validate_server_requests()
        self.assertEquals(self._obj._number_work_ids, 0)
        
        #-----------------------------------------------------------------
        # TESTLINK 95138 :: test error code 4, with review, canceling
        self._obj._work_request_lut.receive('1234A,4,\n'
                                                     '1234B,4,\n'
                                                     '1234C,4,\n'
                                                     '1234D,4,\n\n')
        self._obj._number_work_ids = 0
        self._obj.next_state = None
        self._obj._workid_scanned = False
        self._obj._workid = ''
        self.post_dialog_responses('yes','no','yes')
        
        self._obj.runState(GET_ASSIGN_REVIEW_WORKIDS)

        self.assertEquals(self._obj.next_state, GET_ASSIGN_XMIT_WORKID)
        self.validate_prompts("found 4 work IDs, do you want to review the list?",
                              '<spell>1234A</spell>, select?',
                              '<spell>1234B</spell>, select?')
        self.validate_server_requests()
        self.assertEquals(self._obj._number_work_ids, 0)
        self.assertTrue(self._obj._workid_scanned, True)
        self.assertEquals(self._obj._workid, '1234B')
        
        
        
if __name__ == '__main__':
    unittest.main()
