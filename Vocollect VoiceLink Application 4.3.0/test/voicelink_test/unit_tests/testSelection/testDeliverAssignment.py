from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.SelectionTask import SelectionTask
from core.VoiceLink import VoiceLink
from vocollect_core.task.task_runner import Launch
from common.VoiceLinkLut import VoiceLinkLut
from selection.DeliverAssignment import DeliverAssignmentTask,\
    ASSIGNMENT_DELIVER, ASSIGNMENT_DELIVER_LOAD
from loading.LoadingActions import LoadContainerTask

import unittest
from selection.SharedConstants import ASSIGNMENT_DELIVER_VERIFY, ASSIGNMENT_LOAD

class testDeliverAssignmentTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        self._obj = obj_factory.get(DeliverAssignmentTask,
                                      sel._region_config_lut,
                                      sel._assignment_lut,
                                      True,
                                      sel.taskRunner, sel)
        
        self.tempAssignmentLut = obj_factory.get(VoiceLinkLut, "prTaskLUTGetAssignment")
        self.tempAssignmentLut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n');
        
        self.tempRegionLut = obj_factory.get(VoiceLinkLut, "prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,3,0,\n\n')
        
        self.tempDeliveryLut = obj_factory.get(VoiceLinkLut, "prTaskLUTGetDeliveryLocation")
        self.tempDeliveryLut.receive('Location 1,00,1011,1000,1,1,A00001,\n\n')
        
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'deliverAssignment')

        #test states
        self.assertEquals(self._obj.states[0], ASSIGNMENT_DELIVER)
        self.assertEquals(self._obj.states[1], ASSIGNMENT_DELIVER_LOAD)

    def test_deliver(self):
        self.start_server()
        self._obj.region = self.tempRegionLut[0]
        self._obj._assignment =  self.tempAssignmentLut[0]
          
        #RALLYTC : 1187 Verify that the correct fields are included in the Get Delivery Location LUT and the fields are populated with the correct data.
        self._obj.runState(ASSIGNMENT_DELIVER)
        self.validate_server_requests(['prTaskLUTGetDeliveryLocation', '1', '12345'])
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        
        #test error message
        #RALLYTC : 1186 -Verify that the Get Delivery Location LUT request is re-sent if a timeout occurs.
        self.set_server_response('Location 3,00,1011,1000,0,1,0,1,Error Message\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(ASSIGNMENT_DELIVER)
        
        self.validate_server_requests(['prTaskLUTGetDeliveryLocation', '1', '12345'])
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER)
        self.validate_prompts('Error Message, To continue say ready')
        
    def test_deliver_load(self):
         
        #tests to see load 
        self.start_server()
        self._obj._region = self.tempRegionLut[0]
        self._obj._assignment =  self.tempAssignmentLut[0]
        self._obj._delivery_location_lut=[]
        self._obj._delivery_location_lut.append(self.tempDeliveryLut[0]);
                  
        # the operator is prompted to deliver the assignment.
        self.tempDeliveryLut[0]['allowOverride']=False
        self.post_dialog_responses('ready')
        self._obj.runState(ASSIGNMENT_DELIVER_LOAD)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Deliver position 1 ID Store 123 to Location 1')

        #speak override
        self._obj.next_state = None
        self.tempDeliveryLut[0]['allowOverride']=True
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_LOAD)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, True)
        self.validate_prompts('Deliver position 1 ID Store 123 to Location 1')

        #speak override again
        self._obj.next_state = None
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_LOAD)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Load position 1 ID Store 123 at 1011')

        #Same Tests with only single assignment
        self._obj._multiple_assignments = False
        #speak override
        self._obj.next_state = None
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_LOAD)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, True)
        self.validate_prompts('Deliver to Location 1')

        #speak override again
        self._obj.next_state = None
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_LOAD)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Load at 1011')
        
    def test_deliver_verify_with_check_digits(self):
        #tests to see load 
        self.start_server()
        self._obj._region = self.tempRegionLut[0]
        self._obj._assignment =  self.tempAssignmentLut[0]
        self._obj._delivery_location_lut=[]
        self._obj._delivery_location_lut.append(self.tempDeliveryLut[0]);
        self._obj._override = False
        
        #test speaking location and override when not available    
        self.tempDeliveryLut[0]['allowOverride']=False
        self.post_dialog_responses('override', 'location')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_VERIFY)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Confirm Delivery',
                              'Location 1')
        
        #test speaking check digits normally
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Confirm Delivery')
        
        #test speaking override
        self.tempDeliveryLut[0]['allowOverride']=True
        self._obj.next_state = None
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, True)
        self.validate_prompts('Confirm Delivery')
        
        #test speaking override again (toggles _override flag)
        self.tempDeliveryLut[0]['allowOverride']=True
        self._obj.next_state = None
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Confirm Delivery')
        
    def test_deliver_verify_without_check_digits(self):
        #tests to see load 
        self.start_server()
        self._obj._region = self.tempRegionLut[0]
        self._obj._assignment =  self.tempAssignmentLut[0]
        self._obj._delivery_location_lut=[]
        self._obj._delivery_location_lut.append(self.tempDeliveryLut[0]);
        self._obj._override = False
        self._obj._delivery_location_lut[0]["checkDigit"]=""
        
        #test speaking location and override when not available    
        self.tempDeliveryLut[0]['allowOverride']=False
        self.post_dialog_responses('override', 'location')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_VERIFY)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Confirm Delivery',
                              'Location 1')
        
        #test speaking check digits normally
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Confirm Delivery')
        
        #test speaking override
        self.tempDeliveryLut[0]['allowOverride']=True
        self._obj.next_state = None
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, True)
        self.validate_prompts('Confirm Delivery')
        
        #test speaking override again (toggles _override flag)
        self.tempDeliveryLut[0]['allowOverride']=True
        self._obj.next_state = None
        self.post_dialog_responses('override')
        self._obj.runState(ASSIGNMENT_DELIVER_VERIFY)
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_DELIVER_LOAD)
        self.assertEquals(self._obj._override, False)
        self.validate_prompts('Confirm Delivery')
        
        
    def test_load(self):
        self.start_server()
        self._obj._region = self.tempRegionLut[0]
        self._obj._assignment =  self.tempAssignmentLut[0]
        self._obj._delivery_location_lut=[]
        self._obj._delivery_location_lut.append(self.tempDeliveryLut[0]);
        self._obj._override = False
        self._obj._delivery_location_lut[0]["checkDigit"]=""

        #test not direct load
        self._obj._override = False
        self._obj.runState(ASSIGNMENT_LOAD)
        self.validate_server_requests()
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._override, False)

        self._obj._override = True
        self.assertRaises(Launch, self._obj.runState, ASSIGNMENT_LOAD)
        
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer', 'A00001', False])
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, LoadContainerTask))
        self.validate_server_requests()
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._override, True)
       
if __name__ == '__main__':
    unittest.main()
