from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from selection.PickAssignment import PickAssignmentTask
from selection.CloseContainer import CloseContainer, VERIFY_CLOSE_CONTAINER,\
    CLOSE_PROMPT_FOR_CONTAINER, CLOSE_CONTAINER_CLOSE, CLOSE_CONTAINER_PRINT_LABEL, DELIVER,\
    CLOSE_RETURN_AFTER_DELIVERY
from vocollect_core.task.task_runner import Launch
from selection.SelectionPrint import SelectionPrintTask
from selection.DeliverAssignment import DeliverAssignmentTask
from selection.SelectionTask import SelectionTask
from core.VoiceLink import VoiceLink
from common.VoiceLinkLut import VoiceLinkLut

import unittest

##########################################
# Test Close Container                   #
##########################################
class TestCloseContainer(BaseVLTestCase):

    def setUp(self):
        self.clear()
        sel = obj_factory.get(SelectionTask, 3, VoiceLink())
        sel._region_config_lut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,2,,,,,1,0,0,2,3,0,\n\n')
        
        pickAssign = obj_factory.get(PickAssignmentTask,
                                       sel.taskRunner, sel)
        pickAssign.configure(sel._region_config_lut[0],
                             sel._assignment_lut,
                             sel._picks_lut,
                             sel._container_lut,
                             False)
      
        self._obj  = obj_factory.get(CloseContainer,
                                       pickAssign._region, 
                                       pickAssign._assignment_lut,
                                       pickAssign._picks_lut, 
                                       pickAssign._container_lut,
                                       False,
                                       None,
                                       pickAssign.taskRunner,
                                       pickAssign)
        
        self._obj.dynamic_vocab = pickAssign.dynamic_vocab
        
        self.tempRegionLut = obj_factory.get(VoiceLinkLut, "prTaskLUTPickingRegion")
        self.tempRegionLut.receive('1,dry grocery,1,0,2,1,1,0,0,0,1,1,0,1,0,1,1,4,0,0,0,,,,,1,0,0,2,3,\n\n')
        
        self.tempAssignmentLut = obj_factory.get(VoiceLinkLut, "prTaskLUTGetAssignment")
        self.tempAssignmentLut.receive('1,0,12345,Store 123,1,15,R12,0,0,1,Override summary prompt,0,\n\n')
        self._obj._picks.receive('N,0,1,L1,1,,A 1,,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC,12345,Store 123,0,,0,,,0,0,0,0,0,\n\n')
        self._obj._container_lut.receive('1,0000000003,03,12345,Store 123,,O,0,0,\n2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self._obj._region =self.tempRegionLut[0]
        self._obj._assignment = self.tempAssignmentLut[0]
   
    def test_initializeStates(self):
        ''' Initialize States'''
              
        #test name
        self.assertEquals(self._obj.name, 'closeContainer')

        #test states
        self.assertEquals(self._obj.states[0], VERIFY_CLOSE_CONTAINER)
        self.assertEquals(self._obj.states[1], CLOSE_PROMPT_FOR_CONTAINER)
        self.assertEquals(self._obj.states[2], CLOSE_CONTAINER_CLOSE)
        self.assertEquals(self._obj.states[3], CLOSE_CONTAINER_PRINT_LABEL)
        self.assertEquals(self._obj.states[4], DELIVER)
        self.assertEquals(self._obj.states[5], CLOSE_RETURN_AFTER_DELIVERY)
        
    def test_verify_close_container(self):
        ''' verify if container can be closed'''
        
        self._obj.runState(VERIFY_CLOSE_CONTAINER)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CLOSE_CONTAINER_CLOSE)
        
        #test container cannot be closed for container type not pick to containers
        #TESTLINK 96475 :: Test Case Close Container not allowed when container type is none.
        self._obj._close_container_command = True
        self._obj.runState(VERIFY_CLOSE_CONTAINER)
        self.validate_prompts('close container not allowed')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #test multiple assignments
        #TESTLINK 96477 :: Test Case Close Container not allowed for multiple assignment.
        self._obj._region['containerType'] = 1
        self._obj._multiple_assignments = True
        self._obj.runState(VERIFY_CLOSE_CONTAINER)
        self.validate_prompts('close container not allowed when picking multiple assignments')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #test target containers
        #TESTLINK 96479 :: Test Case Close Container not allowed when picks have target container.
        self._obj._multiple_assignments = False
        self._obj._picks[0]['targetContainer'] = 5
        self._obj.runState(VERIFY_CLOSE_CONTAINER)
        self.validate_prompts('close container not allowed when picks have a target container')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #tests multiple open containers
        self._obj._picks[0]['targetContainer'] = 0
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,C,0,0,\n\n')
        self._obj.runState(VERIFY_CLOSE_CONTAINER)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #only one open container
        #TESTLINK 96481 :: Test Case Close container when only one open container avaliable.
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,O,0,0,\n\n')
        self._obj.runState(VERIFY_CLOSE_CONTAINER)
        self.validate_prompts('Can\'t close the only container')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
    def test_prompt_for_conatiner(self):
        '''prompt for container to be closed'''
        
        #prompt for container
        self.post_dialog_responses('03', 'yes')
        self._obj.runState(CLOSE_PROMPT_FOR_CONTAINER)
        self.validate_prompts('Container?', 
                              '03, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #Already closed container
        #TESTLINK 96485 :: Test Case Operator hears msg when trying to close already closed/incorrect container.
        self._obj._container_lut.receive('2,0000000002,02,12345,Store 123,,C,0,0,\n\n')
        self.post_dialog_responses('02', 'yes')
        self._obj.runState(CLOSE_PROMPT_FOR_CONTAINER)
        self.validate_prompts('Container?', 
                              '02, correct?',
                              '<spell>02</spell> is already closed. Try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CLOSE_PROMPT_FOR_CONTAINER)
        
        #not valid container
        #TESTLINK 96485 :: Test Case Operator hears msg when trying to close already closed/incorrect container.
        self.post_dialog_responses('05', 'yes')
        self._obj.runState(CLOSE_PROMPT_FOR_CONTAINER)
        self.validate_prompts('Container?', 
                              '05, correct?',
                              '<spell>05</spell> not valid try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CLOSE_PROMPT_FOR_CONTAINER)
    
    def test_close_container(self):
        '''close container'''
        self.start_server()
        self._obj._container_id = 11
        self._obj._container = self._obj._container_lut[0]
         
        #verify error response from host    
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,97,error\n\n', 'prTaskLUTContainer')
        self.post_dialog_responses('ready')
        self._obj.runState(CLOSE_CONTAINER_CLOSE)
        self.validate_prompts('error, To continue say ready')
        self.validate_server_requests(['prTaskLUTContainer','1','12345','','1','11','1',''])
        self.assertEquals(self._obj.next_state, CLOSE_CONTAINER_CLOSE)
        
        #verify the container lut 
        self._obj.next_state = None
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,0,\n\n', 'prTaskLUTContainer')
        self._obj.runState(CLOSE_CONTAINER_CLOSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTContainer','1','12345','','1','11','1',''])
        self.assertEquals(self._obj.next_state, None)
        
    def test_print_label(self):
        '''verify print label launch'''
        #test launch of print label
        self._obj._region['printLabels'] = '2'
        self._obj._container = self._obj._container_lut[0]
        self._obj._container['printed']  = 0
        self._obj.taskRunner = VoiceLink()
        self.assertRaises(Launch, self._obj.runState, CLOSE_CONTAINER_PRINT_LABEL)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SelectionPrintTask))
        self.assertEquals(self._obj.current_state, DELIVER)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'selectionPrint')
        
    def test_deliver(self):
        '''verify deliver launch'''
        #test launch of deliver assignment
        self._obj._region['delContWhenClosed']='1'
        self._obj.taskRunner = VoiceLink()
        self.assertRaises(Launch, self._obj.runState, DELIVER)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, DeliverAssignmentTask))
        self.assertEquals(self._obj.current_state, CLOSE_RETURN_AFTER_DELIVERY)
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'deliverAssignment')
        
        #delivery is set to none make sure it is not delivered
        self._obj._region['delivery']='2'
        self._obj.runState(DELIVER)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
    
    def test_return_on_delivery(self):
        '''verify return to deliver launch'''
        #test launch of deliver assignment
        self._obj._region['delContWhenClosed']='1'
        ps = PickAssignmentTask(self._obj.taskRunner, self._obj)
        ps.configure(self._obj._region, 
                     self.tempAssignmentLut, 
                     self._obj._picks, 
                     self._obj._container_lut,
                     False)
        vl = VoiceLink()
        vl._append(ps)
        self._obj.taskRunner = vl
           
        self._obj._close_container_command = True
        self.assertRaises(Launch, self._obj.runState, CLOSE_RETURN_AFTER_DELIVERY)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)    
        
         
if __name__ == '__main__':
    unittest.main()