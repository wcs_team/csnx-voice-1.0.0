from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core.task.task_runner import Launch
from puttostore.PutToStoreNewContainerTask import PutToStoreNewContainer,\
    PTS_NEW_CONT_SPECIFY_CONT, PTS_NEW_CONT_OPEN_CONT, PTS_NEW_CONT_PRINT,\
    PTS_NEW_CONT_RETURN_TO
from puttostore.PutToStorePrint import PutToStorePrintTask
from puttostore.PutToStorePutAssignment import PutAssignmentTask, PTS_ASSIGNMENT_RESET

import unittest

class testPutToStoreNewContainer(BaseVLTestCase):
    
    def setUp(self):
        #clear test object before beginning a test
        self.clear()
        temp = obj_factory.get(PutAssignmentTask, None, None, None, VoiceLink())
        temp.taskRunner._append(temp)
        
        #create test object
        self._obj = obj_factory.get(PutToStoreNewContainer,
                                      obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetRegionConfiguration'),
                                      12345,
                                      67890,
                                      temp.taskRunner,
                                      temp)
        self._obj.taskRunner._append(self._obj)
        
        self._reset_data()

    def _reset_data(self):
        #populate with initial data
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,2,pre,aisle,post,0,0,\n\n')
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putToStoreNewContainerTask')

        #test states
        self.assertEquals(self._obj.states[0], PTS_NEW_CONT_SPECIFY_CONT)
        self.assertEquals(self._obj.states[1], PTS_NEW_CONT_OPEN_CONT)
        self.assertEquals(self._obj.states[2], PTS_NEW_CONT_PRINT)
        self.assertEquals(self._obj.states[3], PTS_NEW_CONT_RETURN_TO)
    
        #test LUTS defined correctly
        fields = self._obj._containers_lut.fields
        self.assertEquals(fields['customerLocationID'].index, 0)
        self.assertEquals(fields['containerNumber'].index, 1)
        self.assertEquals(fields['spokenContainerNumber'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)
        
    def test_specify_container(self):
        #---------------------------------------------------------
        #test system generated containers
        self._obj.next_state = None
        self._obj._region_config_lut[0]['system_gen_container_id'] = True
        
        self._obj.runState(PTS_NEW_CONT_SPECIFY_CONT)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._container_id, None)
        
        #---------------------------------------------------------
        #test system generated containers
        self._obj.next_state = None
        self._obj._region_config_lut[0]['system_gen_container_id'] = False
        self.post_dialog_responses('11111!', 'yes')
        
        self._obj.runState(PTS_NEW_CONT_SPECIFY_CONT)
        
        self.validate_prompts('New container ID?', 
                              '11111, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._container_id, '11111')
    
    def test_open_container(self):
        
        #---------------------------------------------------------
        #test out of range
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(PTS_NEW_CONT_OPEN_CONT)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_NEW_CONT_OPEN_CONT)
        
        #---------------------------------------------------------
        #test error code
        self.start_server()
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response(',,,11,Test Error\n\n')
        
        self._obj.runState(PTS_NEW_CONT_OPEN_CONT)
        
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsContainer','2','67890',''])
        self.assertEquals(self._obj.next_state, '')
        
        #---------------------------------------------------------
        #test warning code
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response(',,,99,Test Warning\n\n')
        
        self._obj.runState(PTS_NEW_CONT_OPEN_CONT)
        
        self.validate_prompts('Test Warning, say ready')
        self.validate_server_requests(['prTaskLUTPtsContainer','2','67890',''])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test normal
        self._obj.next_state = None
        self.post_dialog_responses()
        self.set_server_response(',,,0,\n\n')
        
        self._obj.runState(PTS_NEW_CONT_OPEN_CONT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsContainer','2','67890',''])
        self.assertEquals(self._obj.next_state, None)

    def test_print_labels(self):
        
        #---------------------------------------------------------
        #test normal
        self._obj.next_state = None
        self.post_dialog_responses()
        self._obj._containers_lut.receive('123,456,789,0,\n\n')
        
        self.assertRaises(Launch, self._obj.runState, PTS_NEW_CONT_PRINT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStorePrintTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStorePrint')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PTS_NEW_CONT_RETURN_TO)
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj._group_or_license, 12345)
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj._location_or_item, 123)
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj._contatiner_id, '456')
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_determine_return_to(self):
        #---------------------------------------------------------
        #test operator generated ID
        self._obj._region_config_lut[0]['system_gen_container_id'] = False
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(PTS_NEW_CONT_RETURN_TO)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test system generated ID
        self._obj._region_config_lut[0]['system_gen_container_id'] = True
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self.assertRaises(Launch, self._obj.runState, PTS_NEW_CONT_RETURN_TO)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, 
                                   PutAssignmentTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putAssignment')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PTS_NEW_CONT_RETURN_TO)
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj.current_state, 
                          PTS_ASSIGNMENT_RESET)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        
if __name__ == '__main__':
    unittest.main()


