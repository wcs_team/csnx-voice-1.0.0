from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from vocollect_core.task.task_runner import Launch
from core.VoiceLink import VoiceLink
from puttostore.PutToStoreInductLicense import InductLicenseTask, \
    PROMPT_FOR_LICENSE, RESERVE_LICENSE, SELECT_LICENSE, INDUCT_COMPLETE 
from puttostore.PutToStoreTask import PutToStoreTask
from puttostore.PutToStoreCloseContainerTask import PutToStoreCloseContainer

import unittest

class testInductLicense(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        pts = obj_factory.get(PutToStoreTask, VoiceLink())
        pts._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self._obj = obj_factory.get(InductLicenseTask, pts._region_config_lut, VoiceLink(), pts)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'inductLicense')

        #test states
        self.assertEquals(self._obj.states[0], PROMPT_FOR_LICENSE)
        self.assertEquals(self._obj.states[1], RESERVE_LICENSE)
        self.assertEquals(self._obj.states[2], SELECT_LICENSE)
        self.assertEquals(self._obj.states[3], INDUCT_COMPLETE)
        
        #test LUTS defined correctly
        fields = self._obj._verify_license_lut.fields
        self.assertEquals(fields['licenseNumber'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
    def test_prompt_for_licesnse(self):
        #-----------------------------------------------------------------
        #Test no more when none selected
        self.post_dialog_responses('no more')
        self._obj._license_scanned = False
        
        self._obj.runState(PROMPT_FOR_LICENSE)

        self.validate_prompts('License?',
                              'No more not allowed. You must reserve at least one license.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PROMPT_FOR_LICENSE)
        self.assertEquals(self._obj._license_scanned, False)
        

        #-----------------------------------------------------------------
        #Test no more when at least 1 select
        self._obj.next_state = None
        self.post_dialog_responses('no more')
        self._obj._reserved_licenses = 1
        self._obj._license_scanned = False
        
        self._obj.runState(PROMPT_FOR_LICENSE)
        
        self.validate_prompts('License?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, INDUCT_COMPLETE)
        self.assertEquals(self._obj._reserved_licenses, 
                          self._obj._region_config_lut[0]['max_license_in_group'])
        self.assertEquals(self._obj._license_scanned, False)
        
        #-----------------------------------------------------------------
        #Test Speaking a license
        self._obj.next_state = None
        self.post_dialog_responses('12345!')
        self._obj._reserved_licenses = 0
        self._obj._license_scanned = False
        
        self._obj.runState(PROMPT_FOR_LICENSE)
        
        self.validate_prompts('License?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reserved_licenses, 0)
        self.assertEquals(self._obj._license_scanned, False)

        #-----------------------------------------------------------------
        #Test Speaking a license to short
        self._obj.next_state = None
        self.post_dialog_responses('123!', '12345!')
        self._obj._reserved_licenses = 0
        self._obj._license_scanned = False
        
        self._obj.runState(PROMPT_FOR_LICENSE)
        
        self.validate_prompts('License?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reserved_licenses, 0)
        self.assertEquals(self._obj._license_scanned, False)

        #-----------------------------------------------------------------
        #Test Speaking a license with confirm on
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,1,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self._obj.next_state = None
        self.post_dialog_responses('12345!', 'yes')
        self._obj._reserved_licenses = 0
        self._obj._license_scanned = False
        
        self._obj.runState(PROMPT_FOR_LICENSE)
        
        self.validate_prompts('License?',
                              '12345, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reserved_licenses, 0)
        self.assertEquals(self._obj._license_scanned, False)
        
        #-----------------------------------------------------------------
        #Test Speaking a license with all digits set
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,0,1,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self._obj.next_state = None
        self.post_dialog_responses('1234567890', 'ready', 'yes')
        self._obj._reserved_licenses = 0
        self._obj._license_scanned = False
        
        self._obj.runState(PROMPT_FOR_LICENSE)
        
        self.validate_prompts('License?',
                              '1234567890, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reserved_licenses, 0)
        self.assertEquals(self._obj._license_scanned, True)
        
        #---------------------------------------------------------
        #test close container
        self._obj.next_state = None
        self._obj._container_id = None 
        self.post_dialog_responses('close container', 'no', 'close container', 'yes')

        self.assertRaises(Launch, self._obj.runState, PROMPT_FOR_LICENSE)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreCloseContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreCloseContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PROMPT_FOR_LICENSE)
        self.validate_prompts('License?', 
                              'close container, correct?', 
                              'License?', 
                              'close container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

    def test_reserve_license(self):

        #-----------------------------------------------------------------
        #test server not started
        self.post_dialog_responses('ready')

        self._obj.runState(RESERVE_LICENSE)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, RESERVE_LICENSE)
        
        #-----------------------------------------------------------------
        #test error condition
        self.start_server()
        self._obj._license = '12345'
        self._obj._license_scanned = False
        
        self.post_dialog_responses('ready')
        self.set_server_response(',1,Error Message\n\n')

        self._obj.runState(RESERVE_LICENSE)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsVerifyLicense', '12345', '1'])
        self.assertEquals(self._obj.next_state, PROMPT_FOR_LICENSE)
        
        #-----------------------------------------------------------------
        #test warning condition/single location
        self._obj.next_state = None
        self._obj._license = '67890'
        self._obj._license_scanned = True
        self.post_dialog_responses('ready')
        self.set_server_response(',99,Warning Message\n\n')

        self._obj.runState(RESERVE_LICENSE)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTPtsVerifyLicense', '67890', '0'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test multiple locations, with no
        self._obj.next_state = None
        self.post_dialog_responses('no')
        self.set_server_response('12345,1,\n67890,1,\n\n')

        self._obj.runState(RESERVE_LICENSE)
        
        self.validate_prompts('Found 2 licenses. Do you want to review the list?')
        self.validate_server_requests(['prTaskLUTPtsVerifyLicense', '67890', '0'])
        self.assertEquals(self._obj.next_state, PROMPT_FOR_LICENSE)

        #-----------------------------------------------------------------
        #test multiple locations, with yes
        self._obj.next_state = None
        self.post_dialog_responses('yes')
        self.set_server_response('12345,1,\n67890,1,\n\n')

        self._obj.runState(RESERVE_LICENSE)
        
        self.validate_prompts('Found 2 licenses. Do you want to review the list?')
        self.validate_server_requests(['prTaskLUTPtsVerifyLicense', '67890', '0'])
        self.assertEquals(self._obj.next_state, None)

    def test_select_license(self):
        #-----------------------------------------------------------------
        #test single license in LUT
        self._obj.next_state = None
        self._obj._verify_license_lut.receive('12345,1,\n\n')
        self.post_dialog_responses()

        self._obj.runState(SELECT_LICENSE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test multiple license in LUT, cancel
        self._obj.next_state = None
        self._obj._verify_license_lut.receive('12345,1,\n67890,1,\n\n')
        self.post_dialog_responses('cancel', 'yes')

        self._obj.runState(SELECT_LICENSE)
        
        self.validate_prompts('12345, select?',
                              'cancel this license request?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PROMPT_FOR_LICENSE)
        self.assertEquals(self._obj._license, 'cancel')
        
        #-----------------------------------------------------------------
        #test multiple license in LUT, no selections
        self._obj.next_state = None
        self._obj._verify_license_lut.receive('12345,1,\n67890,1,\n\n')
        self.post_dialog_responses('no', 'no')

        self._obj.runState(SELECT_LICENSE)
        
        self.validate_prompts('12345, select?',
                              '67890, select?',
                              'End of matching licenses. Starting over')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, SELECT_LICENSE)
        self.assertEquals(self._obj._license, None)
        
        #-----------------------------------------------------------------
        #test multiple license in LUT, select
        self._obj.next_state = None
        self._obj._license_scanned = False
        self._obj._verify_license_lut.receive('12345,1,\n67890,1,\n\n')
        self.post_dialog_responses('no', 'yes')

        self._obj.runState(SELECT_LICENSE)
        
        self.validate_prompts('12345, select?',
                              '67890, select?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, RESERVE_LICENSE)
        self.assertEquals(self._obj._license, '67890')
        self.assertEquals(self._obj._license_scanned, True)
        
    def test_induct_complete(self):
        #-----------------------------------------------------------------
        #test not at max
        self._obj.next_state = None
        self._obj._reserved_licenses = 0

        self._obj.runState(INDUCT_COMPLETE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PROMPT_FOR_LICENSE)
        self.assertEquals(self._obj._reserved_licenses, 1)
        
        #-----------------------------------------------------------------
        #test reached max
        self._obj.next_state = None
        max = self._obj._region_config_lut[0]['max_license_in_group']
        self._obj._reserved_licenses = max - 1

        self._obj.runState(INDUCT_COMPLETE)
        
        self.validate_prompts('Getting work.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reserved_licenses, max)
        
        
    
if __name__ == '__main__':
    unittest.main()


