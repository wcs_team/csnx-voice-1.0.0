from BaseVLTestCase import BaseVLTestCase, BOTH_SERVERS #Needs to be first import
from vocollect_core import obj_factory
from vocollect_core.task.task_runner import Launch, TaskRunnerBase

from core.VoiceLink import VoiceLink
from puttostore.PutToStoreTask import PutToStoreTask
from puttostore.PutToStorePutAssignment import PutAssignmentTask
from puttostore.StartAssignment import StartAssignmentTask
from puttostore.PutToStorePutTask import PutToStorePutTask
from core.CoreTask import CoreTask

import time

class testPutToStoreVocabulary(BaseVLTestCase):

    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.function = 1
        self._obj = obj_factory.get(PutToStoreTask, temp.taskRunner)
        self._obj.taskRunner._append(temp)
        self._obj.taskRunner._append(self._obj)
        TaskRunnerBase._main_runner = self._obj.taskRunner

    def _add_put_task(self):
        ''' add a put assignment '''
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,2,pre,aisle,post,0,0,\n\n')
        self._obj._assignment_lut.receive('999,5,25,3,RL002,00,EL001,99,123,98,0,\n\n')
        self._obj._pts_lut.receive('N,1,88,22222,pre 1,11,post 1,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   'N,2,88,22222,pre 1,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   'N,3,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   'N,4,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   '\n')
        
        self._obj.dynamic_vocab.region_config = self._obj._region_config_lut[0]
        temp = obj_factory.get(PutToStorePutTask,
                                 self._obj._region_config_lut, 
                                 self._obj._assignment_lut, 
                                 self._obj._pts_lut, 
                                 self._obj._pts_lut[0], 
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        
    def test_valid(self):

        #running in main task
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('pass assignment'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('review containers'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('license'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)

        #running in start assignment task
        temp = obj_factory.get(StartAssignmentTask,
                                 self._obj._region_config_lut, 
                                 self._obj._assignment_lut, 
                                 self._obj._pts_lut, 
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('pass assignment'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('review containers'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('license'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)

        #running in put assignment task
        temp = obj_factory.get(PutAssignmentTask,
                                 self._obj._region_config_lut, 
                                 self._obj._assignment_lut, 
                                 self._obj._pts_lut, 
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('pass assignment'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('review containers'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('license'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)

        #Test have location ID
        self._obj.dynamic_vocab.location_id = '123'
        self._obj.taskRunner._insert(temp)
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('pass assignment'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('review containers'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('license'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)

        #running in put assignment task
        self._obj.dynamic_vocab.location_id = None
        self._add_put_task()
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('pass assignment'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('review containers'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('license'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), True)

    def test_pass_assignment(self):
        self._add_put_task()
        
        #--------------------------------------------------------------
        #Test Not Allowed
        self._obj._region_config_lut[0]['allow_pass_assignment'] = False
        
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('pass assignment')
        
        self.assertTrue(result);
        self.validate_prompts('Pass assignment not allowed.')
        self.validate_server_requests()
        
        #--------------------------------------------------------------
        #Test Allow, response No
        self._obj._region_config_lut[0]['allow_pass_assignment'] = True
        self.post_dialog_responses('no')
        
        result = self._obj.dynamic_vocab.execute_vocab('pass assignment')
        
        self.assertTrue(result);
        self.validate_prompts('Pass assignment, correct?')
        self.validate_server_requests()

        #--------------------------------------------------------------
        #Test Allowed with skips
        self._obj._pts_lut[3]['status'] = 'S'
        
        self.post_dialog_responses('yes')
        
        result = self._obj.dynamic_vocab.execute_vocab('pass assignment')
        
        self.assertTrue(result);
        self.validate_prompts('Pass assignment, correct?', 
                              'Pass assignment not allowed.  You must repick skips.')
        self.validate_server_requests()


        #--------------------------------------------------------------
        #Test pass assignment
        self._obj._pts_lut[3]['status'] = 'N'
        self.start_server()
        
        self.post_dialog_responses('yes', 'ready', 'ready')
        self.set_server_response('1,error message\n\n')
        self.set_server_response('0,\n\n')
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'pass assignment')
        
        self.validate_prompts('Pass assignment, correct?',
                              'error message, To continue say ready', 
                              'Assignment passed, say ready.')
        self.validate_server_requests(['prTaskLUTPtsPassAssignment', '999'],
                                      ['prTaskLUTPtsPassAssignment', '999'])


    def test_pass_assignment_change_function(self):
        self._add_put_task()

        #--------------------------------------------------------------
        #Test pass assignment, change function
        self.start_server()
        
        self._obj._pts_lut[3]['status'] = 'N'
        
        self.post_dialog_responses('yes', 
                                   'change function', 'no',
                                   'change function', 'yes')
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.set_server_response('0,\n\n')
        self.set_server_response('0,\n\n')
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'pass assignment')
        
        self.validate_prompts('Pass assignment, correct?', 
                              'Assignment passed, say ready.',
                              'change function, correct?', 
                              'Assignment passed, say ready.', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTPtsPassAssignment', '999'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'])
    
    def test_pass_assignment_change_region(self):
        self._add_put_task()
        self.start_server()

        #--------------------------------------------------------------
        #Test pass assignment, change function
        self._obj._pts_lut[3]['status'] = 'N'
        self._obj._region_selected = True
        
        self.post_dialog_responses('yes', 
                                   'change region', 'no',
                                   'change region', 'yes')
        self.set_server_response('0,\n\n')
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'pass assignment')
        
        self.validate_prompts('Pass assignment, correct?', 
                              'Assignment passed, say ready.',
                              'Change region, correct?', 
                              'Assignment passed, say ready.', 
                              'Change region, correct?')
        self.validate_server_requests(['prTaskLUTPtsPassAssignment', '999'])
    
    
    
    def test_repick_skips(self):
        
        temp = obj_factory.get(PutAssignmentTask,
                                 self._obj._region_config_lut, 
                                 self._obj._assignment_lut, 
                                 self._obj._pts_lut, 
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        self._add_put_task()
        
        #--------------------------------------------------------------
        #Test Not Allowed
        self._obj._region_config_lut[0]['allow_repick_skips'] = False
        
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('repick skips')
        
        self.assertTrue(result);
        self.validate_prompts('repick skips not allowed')
        self.validate_server_requests()

        #--------------------------------------------------------------
        #Test Allowed, No Skips
        self._obj._region_config_lut[0]['allow_repick_skips'] = True
        
        self.post_dialog_responses('yes')
        
        result = self._obj.dynamic_vocab.execute_vocab('repick skips')
        
        self.assertTrue(result);
        self.validate_prompts('repick skips, correct?', 'There are no skips.')
        self.validate_server_requests()

        #--------------------------------------------------------------
        #Test Allowed, with Skips
        self._obj._pts_lut[3]['status'] = 'S'
        self.start_server(BOTH_SERVERS)
        self.set_server_response('Y')
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'repick skips')
        time.sleep(1)
        self.assertEquals(self._obj._pts_lut[3]['status'], 'N')
        self.validate_prompts('repick skips, correct?')
        self.validate_server_requests(['prTaskODRPtsUpdateStatus', '999', '', '2', 'N'])
    
    def test_review_containers(self):
        self.start_server()

        #----------------------------------------------------------------
        #Test with location ID set, but not
        self._obj.dynamic_vocab.location_id = '123'
        
        self.set_server_response('LOC1,1110,1110S,0,\n'
                                 'LOC2,1111,1111S,0,\n'
                                 'LOC3,1112,1112S,0,\n'
                                 '\n')
        self.post_dialog_responses('ready', 'ready', 'ready')
        
        result = self._obj.dynamic_vocab.execute_vocab('review containers')
        
        self.assertTrue(result);
        self.validate_prompts('1 1 1 0', 
                              '1 1 1 1', 
                              '1 1 1 2')
        self.validate_server_requests(['prTaskLUTPtsContainer', '0', '123', ''])
        
        #----------------------------------------------------------------
        #Test with put task, cancel review
        temp = obj_factory.get(PutAssignmentTask,
                                 self._obj._region_config_lut, 
                                 self._obj._assignment_lut, 
                                 self._obj._pts_lut, 
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        self._add_put_task()

        self._obj.dynamic_vocab.location_id = '123'
        
        self.set_server_response('LOC1,1110,1110S,0,\n'
                                 'LOC2,1111,1111S,0,\n'
                                 'LOC3,1112,1112S,0,\n'
                                 '\n')
        self.post_dialog_responses('ready', 'cancel')
        
        result = self._obj.dynamic_vocab.execute_vocab('review containers')
        
        self.assertTrue(result);
        self.validate_prompts('1 1 1 0', 
                              '1 1 1 1')
        self.validate_server_requests(['prTaskLUTPtsContainer', '0', '88', ''])

        #----------------------------------------------------------------
        #Test no containers returned
        self._obj.dynamic_vocab.location_id = '123'
        
        self.set_server_response(',,,0,\n\n')
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('review containers')
        
        self.assertTrue(result);
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsContainer', '0', '88', ''])

        #----------------------------------------------------------------
        #Test error in LUT
        self._obj.dynamic_vocab.location_id = '123'
        
        self.set_server_response('LOC1,1111,1111S,1,Error Message\n\n')
        self.post_dialog_responses('ready')
        
        result = self._obj.dynamic_vocab.execute_vocab('review containers')
        
        self.assertTrue(result);
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsContainer', '0', '88', ''])

        #----------------------------------------------------------------
        #Test warning in LUT
        self._obj.dynamic_vocab.location_id = '123'
        
        self.set_server_response('LOC1,1111,1111S,99,Warning Message\n\n')
        self.post_dialog_responses('ready', 'ready')
        
        result = self._obj.dynamic_vocab.execute_vocab('review containers')
        
        self.assertTrue(result);
        self.validate_prompts('Warning Message, say ready', 
                              '1 1 1 1')
        self.validate_server_requests(['prTaskLUTPtsContainer', '0', '88', ''])
    
    def test_license(self):
        self._add_put_task()

        #----------------------------------------------------------------
        #Test license
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('license')
        
        self.assertTrue(result);
        self.validate_prompts('22222')
        self.validate_server_requests()

    def test_item_number(self):
        self._add_put_task()

        #----------------------------------------------------------------
        #Test item number
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('item number')
        
        self.assertTrue(result);
        self.validate_prompts('ITEM123')
        self.validate_server_requests()

    def test_description(self):
        self._add_put_task()

        #----------------------------------------------------------------
        #Test description
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('description')
        
        self.assertTrue(result);
        self.validate_prompts('description')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test no description
        self.post_dialog_responses()
        self._obj._pts_lut[0]['itemDescription'] = ''
        result = self._obj.dynamic_vocab.execute_vocab('description')
        
        self.assertTrue(result);
        self.validate_prompts('description not available')
        self.validate_server_requests()
    
    def test_quantity(self):
        self._add_put_task()

        #----------------------------------------------------------------
        #Test quantity
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('quantity')
        
        self.assertTrue(result);
        self.validate_prompts('5 uom')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test quantity, some put
        self.post_dialog_responses()
        self._obj._pts_lut[0]['quantityPut'] = 2
        self._obj._pts_lut[0]['uom'] = ''
        
        result = self._obj.dynamic_vocab.execute_vocab('quantity')
        
        self.assertTrue(result);
        self.validate_prompts('3 ')
        self.validate_server_requests()

    def test_location(self):
        self._add_put_task()

        #----------------------------------------------------------------
        #Test location
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('location')
        
        self.assertTrue(result);
        self.validate_prompts('pre 1, Aisle <spell>11</spell>, post 1, Slot <spell>slot</spell>')
        self.validate_server_requests()

