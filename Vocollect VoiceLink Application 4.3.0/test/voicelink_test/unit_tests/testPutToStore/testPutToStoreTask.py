from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from common.RegionSelectionTasks import SingleRegionTask
from puttostore.PutToStoreTask import PutToStoreTask, REGIONS, \
    VALIDATE_REGIONS, GET_FT_LOCATION, INDUCT_LICENSE, START_ASSIGNMENT,\
    PUT_ASSIGNMENT, HANDLE_RESIDUALS, COMPLETE_ASSIGNMENT
from puttostore.PutToStoreInductLicense import InductLicenseTask
from puttostore.StartAssignment import StartAssignmentTask
from puttostore.PutToStorePutAssignment import PutAssignmentTask
from puttostore.PutToStoreHandleResiduals import PutToStoreHandleResiduals
from core.CoreTask import CoreTask

import unittest

class testPutToStoreTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1
        self._obj = obj_factory.get(PutToStoreTask, temp.taskRunner)
        TaskRunnerBase._main_runner = self._obj.taskRunner
        
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putToStore')

        #test states
        self.assertEquals(self._obj.states[0], REGIONS)
        self.assertEquals(self._obj.states[1], VALIDATE_REGIONS)
        self.assertEquals(self._obj.states[2], GET_FT_LOCATION)
        self.assertEquals(self._obj.states[3], INDUCT_LICENSE)
        self.assertEquals(self._obj.states[4], START_ASSIGNMENT)
        self.assertEquals(self._obj.states[5], PUT_ASSIGNMENT)
        self.assertEquals(self._obj.states[6], HANDLE_RESIDUALS)
        self.assertEquals(self._obj.states[7], COMPLETE_ASSIGNMENT)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['allow_skip_aisle'].index, 2)
        self.assertEquals(fields['allow_skip_slot'].index, 3)
        self.assertEquals(fields['allow_repick_skips'].index, 4)
        self.assertEquals(fields['allow_signoff'].index, 5)
        self.assertEquals(fields['allow_pass_assignment'].index, 6)
        self.assertEquals(fields['allow_multi_containers'].index, 7)
        self.assertEquals(fields['max_license_in_group'].index, 8)
        self.assertEquals(fields['system_gen_container_id'].index, 9)
        self.assertEquals(fields['spoken_license_length'].index, 10)
        self.assertEquals(fields['confirm_spoken_license'].index, 11)
        self.assertEquals(fields['validate_container_id'].index, 12)
        self.assertEquals(fields['validate_container_length'].index, 13)
        self.assertEquals(fields['confirm_spoken_location'].index, 14)
        self.assertEquals(fields['spoken_location_length'].index, 15)
        self.assertEquals(fields['check_digit_length'].index, 16)
        self.assertEquals(fields['use_lut'].index, 17)
        self.assertEquals(fields['current_pre_aisle'].index, 18)
        self.assertEquals(fields['current_aisle'].index, 19)
        self.assertEquals(fields['current_post_aisle'].index, 20)
        self.assertEquals(fields['current_slot'].index, 21)
        self.assertEquals(fields['print_exception_label'].index, 22)
        self.assertEquals(fields['errorCode'].index, 23)
        self.assertEquals(fields['errorMessage'].index, 24)

        fields = self._obj._ft_location_lut.fields
        self.assertEquals(fields['ftLocation'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        fields = self._obj._assignment_lut.fields
        self.assertEquals(fields['groupId'].index, 0)
        self.assertEquals(fields['totalSlots'].index, 1)
        self.assertEquals(fields['totalItems'].index, 2)
        self.assertEquals(fields['expectedResidual'].index, 3)
        self.assertEquals(fields['returnLocation'].index, 4)
        self.assertEquals(fields['returnCheckdigit'].index, 5)
        self.assertEquals(fields['exceptionLocation'].index, 6)
        self.assertEquals(fields['exceptionCheckdigit'].index, 7)
        self.assertEquals(fields['perfLast'].index, 8)
        self.assertEquals(fields['perfDaily'].index, 9)
        self.assertEquals(fields['errorCode'].index, 10)
        self.assertEquals(fields['errorMessage'].index, 11)
        
        fields = self._obj._pts_lut.fields
        self.assertEquals(fields['status'].index, 0)
        self.assertEquals(fields['putID'].index, 1)
        self.assertEquals(fields['customerLocationID'].index, 2)
        self.assertEquals(fields['preAisle'].index, 4)
        self.assertEquals(fields['aisle'].index, 5)
        self.assertEquals(fields['postAisle'].index, 6)
        self.assertEquals(fields['slot'].index, 7)
        self.assertEquals(fields['checkDigits'].index, 8)
        self.assertEquals(fields['spokenVerification'].index, 9)
        self.assertEquals(fields['scannedVerification'].index, 10)
        self.assertEquals(fields['quantityToPut'].index, 11)
        self.assertEquals(fields['quantityPut'].index, 12)
        self.assertEquals(fields['itemNumber'].index, 13)
        self.assertEquals(fields['itemDescription'].index, 14)
        self.assertEquals(fields['uom'].index, 15)
        self.assertEquals(fields['allowOverPack'].index, 16)
        self.assertEquals(fields['residualQuantity'].index, 17)
        self.assertEquals(fields['errorCode'].index, 18)
        self.assertEquals(fields['errorMessage'].index, 19)
        
        
    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SingleRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskSingleRegion')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, VALIDATE_REGIONS)
    
    def test_validate_regions(self):
        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj.runState(VALIDATE_REGIONS)

        self.validate_prompts('not authorized for any regions, see your supervisor')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #-----------------------------------------------------------------
        #Test authorized regions
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(VALIDATE_REGIONS)
        
        self.validate_prompts('To start Put to store work, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #Test change function No
        self.start_server()
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')
        
        self._obj.runState(VALIDATE_REGIONS)
        
        self.validate_prompts('To start Put to store work, say ready', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, VALIDATE_REGIONS)

        #-----------------------------------------------------------------
        #Test change function Yes
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, VALIDATE_REGIONS)
        
        self.validate_prompts('To start Put to store work, say ready', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #Test change region No
        self._obj.next_state = None
        self._obj._region_selected = True
        self.post_dialog_responses('change region', 'no')
        
        self._obj.runState(VALIDATE_REGIONS)
        
        self.validate_prompts('To start Put to store work, say ready', 
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, VALIDATE_REGIONS)

        #-----------------------------------------------------------------
        #Test change region Yes
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, VALIDATE_REGIONS)
        
        self.validate_prompts('To start Put to store work, say ready', 
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        
    def test_get_ft_location(self):

        #-----------------------------------------------------------------
        #test server not started
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(GET_FT_LOCATION)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, GET_FT_LOCATION)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response(',1,Error Message\n\n')

        self._obj.runState(GET_FT_LOCATION)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsGetFTLocation', '1'])
        self.assertEquals(self._obj.next_state, GET_FT_LOCATION)
        
        #-----------------------------------------------------------------
        #test warning condition/no FT location
        self._obj.next_state = None
        self._obj._region_config_lut.receive('2,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.post_dialog_responses('ready')
        self.set_server_response(',99,Warning Message\n\n')

        self._obj.runState(GET_FT_LOCATION)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTPtsGetFTLocation', '2'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors with FT location
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('1234,0,\n\n')

        self._obj.runState(GET_FT_LOCATION)
        
        self.validate_prompts('Go to 1234')
        self.validate_server_requests(['prTaskLUTPtsGetFTLocation', '2'])
        self.assertEquals(self._obj.next_state, None)

    def test_induct_license(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, INDUCT_LICENSE)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, InductLicenseTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'inductLicense')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, START_ASSIGNMENT)
    
    def test_start_assignment(self):
        #Check that the correct object gets launched and state is set correctly
        self._obj._region_config_lut.receive('2,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.assertRaises(Launch, self._obj.runState, START_ASSIGNMENT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, StartAssignmentTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'startAssignment')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_ASSIGNMENT)
    
    def test_put_assignment(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, PUT_ASSIGNMENT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutAssignmentTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putAssignment')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, HANDLE_RESIDUALS)
    
    def test_handle_residuals(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, HANDLE_RESIDUALS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreHandleResiduals))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreHandleResiduals')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, COMPLETE_ASSIGNMENT)
        
    def test_complete_assignment(self):
        self._obj._assignment_lut.receive('999,5,25,3,RL002,00,EL001,99,123,98,0,\n\n')

        #-----------------------------------------------------------------
        #test server not started
        self.post_dialog_responses('ready')

        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, COMPLETE_ASSIGNMENT)
        
        #-----------------------------------------------------------------
        #test error condition
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response('1,Error Message\n\n')

        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsStopAssignment', '999'])
        self.assertEquals(self._obj.next_state, COMPLETE_ASSIGNMENT)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj._region_config_lut.receive('2,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.post_dialog_responses('ready', 'ready')
        self.set_server_response('99,Warning Message\n\n')

        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.validate_prompts('Warning Message, say ready',
                              'For next assignment, say ready')
        self.validate_server_requests(['prTaskLUTPtsStopAssignment', '999'])
        self.assertEquals(self._obj.next_state, INDUCT_LICENSE)

        #-----------------------------------------------------------------
        #test no errors 
        self.post_dialog_responses('ready')
        self.set_server_response('0,\n\n')

        self._obj.runState(COMPLETE_ASSIGNMENT)
        
        self.validate_prompts('For next assignment, say ready')
        self.validate_server_requests(['prTaskLUTPtsStopAssignment', '999'])
        self.assertEquals(self._obj.next_state, INDUCT_LICENSE)

        #-----------------------------------------------------------------
        #Test change function No
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no',
                                   'change function', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, COMPLETE_ASSIGNMENT)
        
        self.validate_prompts('For next assignment, say ready', 
                              'change function, correct?', 
                              'For next assignment, say ready', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTPtsStopAssignment', '999'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #Test change region
        self._obj.next_state = None
        self._obj._region_selected = True
        self.post_dialog_responses('change region', 'no',
                                   'change region', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, COMPLETE_ASSIGNMENT)
        
        self.validate_prompts('For next assignment, say ready', 
                              'Change region, correct?', 
                              'For next assignment, say ready', 
                              'Change region, correct?')
        self.validate_server_requests(['prTaskLUTPtsStopAssignment', '999'])
        self.assertEquals(self._obj.next_state, None)

    def test_change_region(self):
        #TESTLINK 95630 :: Tests for Change Region
        
        #---------------------------------------------------------------
        #Test no region currently selected
        self._obj._region_selected = False
        
        self._obj.change_region()
        
        self.validate_prompts() 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response no
        self._obj._region_selected = True
        self.post_dialog_responses('no')
        
        self._obj.change_region()
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response yes
        self._obj._region_selected = True
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.change_region)
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
    
if __name__ == '__main__':
    unittest.main()


