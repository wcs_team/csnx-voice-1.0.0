from BaseVLTestCase import BaseVLTestCase, BOTH_SERVERS #Needs to be first import

from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core.task.task_runner import Launch
from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from puttostore.PutToStorePutTask import PutToStorePutTask, VERIFY_SLOT,\
    PUT_PROMPT, VALIDATE_CONTAINER, FINISH_PUT, TRANSMIT_PUT
from puttostore.PutToStoreNewContainerTask import PutToStoreNewContainer
from puttostore.PutToStoreCloseContainerTask import PutToStoreCloseContainer

import unittest
import time
from puttostore.PutToStoreTask import PutToStoreTask

class testPutToStorePut(BaseVLTestCase):
    
    def setUp(self):
        #clear test object before beginning a test
        self.clear()
        #create test object
        pts = PutToStoreTask(VoiceLink(), None)
        self._obj = obj_factory.get(PutToStorePutTask,
                                      obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetRegionConfiguration'),
                                      obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetAssignment'),
                                      obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetPuts'),
                                      None, 
                                      pts.taskRunner,
                                      pts)
        
        self._reset_data()

    def _reset_data(self):
        #populate with initial data
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,2,pre,aisle,post,0,0,\n\n')
        self._obj._assignment_lut.receive('999,5,25,3,RL002,00,EL001,99,123,98,0,\n\n')
        self._obj._pts_lut.receive('N,1,88,22222,pre 1,1,post 1,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   'N,2,88,22222,pre 1,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   'N,3,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   'N,4,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,2,0,\n'
                                   '\n')
        self._obj._current_put = self._obj._pts_lut[0]
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putToStorePutTask')

        #test states
        self.assertEquals(self._obj.states[0], VERIFY_SLOT)
        self.assertEquals(self._obj.states[1], PUT_PROMPT)
        self.assertEquals(self._obj.states[2], VALIDATE_CONTAINER)
        self.assertEquals(self._obj.states[3], TRANSMIT_PUT)
        self.assertEquals(self._obj.states[4], FINISH_PUT)
    
        #test LUTS defined correctly
        fields = self._obj._put_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)
        
    def test_verify_slot(self):
        #---------------------------------------------------------
        #test no check digits specified by host
        self._obj._current_put['checkDigits'] = ''
        self._obj._current_put['slot'] = 'S123'
        self.post_dialog_responses('ready')
        
        self._obj.runState(VERIFY_SLOT)
        
        self.validate_prompts('S123')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test with check digits
        self._obj._current_put['checkDigits'] = '01'
        self.post_dialog_responses('00!',
                                   '11!',
                                   '01!')
        
        self._obj.runState(VERIFY_SLOT)
        
        self.validate_prompts('S123',
                              'wrong 00, try again',
                              'wrong 11, try again',
                              'S123')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)


        #---------------------------------------------------------
        #test skip slot, not allowed
        self._obj._region_config_lut[0]['allow_skip_slot'] = False
        self.post_dialog_responses('skip slot')
        
        self._obj.runState(VERIFY_SLOT)
        
        self.validate_prompts('S123',
                              'skip slot not allowed')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, VERIFY_SLOT)
        
        #---------------------------------------------------------
        #test skip slot, allowed, last slot
        self._obj._region_config_lut[0]['allow_skip_slot'] = True
        self.post_dialog_responses('skip slot')
        
        self._obj.runState(VERIFY_SLOT)
        
        self.validate_prompts('S123',
                              'Last slot, skip slot not allowed')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, VERIFY_SLOT)
        
        #---------------------------------------------------------
        #test skip slot, allowed, no to confirm
        self._obj._pts_lut[0]['customerLocationID'] = '00000'
        self._obj._pts_lut[1]['customerLocationID'] = '00000'
        self.post_dialog_responses('skip slot', 'no')
        
        self._obj.runState(VERIFY_SLOT)
        
        self.validate_prompts('S123',
                              'Skip slot, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, VERIFY_SLOT)
        
        #---------------------------------------------------------
        #test skip slot, allowed, no to confirm, LUT
        self.post_dialog_responses('skip slot', 'yes')
        self.start_server(BOTH_SERVERS)
        self.set_server_response(',0\n\n', 'prTaskLutPtsUpdateStatus')
        self.set_server_response('Y', 'prTaskODRPtsUpdateStatus')
        
        self._obj.runState(VERIFY_SLOT)
        
        self.validate_prompts('S123',
                              'Skip slot, correct?')
        self.validate_server_requests(['prTaskLutPtsUpdateStatus','999','0','0','S'])
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(self._obj._pts_lut[0]['status'], 'S')
        self.assertEquals(self._obj._pts_lut[1]['status'], 'S')
        
        #---------------------------------------------------------
        #test skip slot, allowed, no to confirm, ODR
        self._obj._region_config_lut[0]['use_lut'] = 0
        self._obj._pts_lut[0]['status'] = 'N'
        self._obj._pts_lut[1]['status'] = 'P'
        self.post_dialog_responses('skip slot', 'yes')
        
        self._obj.runState(VERIFY_SLOT)
        time.sleep(1) #added to ensure ODR had time to be received
        
        self.validate_prompts('S123',
                              'Skip slot, correct?')
        self.validate_server_requests(['prTaskODRPtsUpdateStatus','999','0','0','S'])
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(self._obj._pts_lut[0]['status'], 'S')
        self.assertEquals(self._obj._pts_lut[1]['status'], 'P')
        
    def test_put_prompt(self):
        #---------------------------------------------------------
        #test normal put prompt, multiple items, UOM
        self._obj.next_state = None
        self._obj._current_put['quantityToPut'] = 5
        self.post_dialog_responses('5!')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('description, Put 5, uom')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, False)

        #---------------------------------------------------------
        #test less, no to partial, no to short (no UOM)
        self._obj.next_state = None
        self._obj._current_put['uom'] = ''
        self.post_dialog_responses('3!', 'no', 'no')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('description, Put 5, ',
                              'Is this a partial?',
                              'Is this a short?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PUT_PROMPT)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, False)
        
        #---------------------------------------------------------
        #test less, no to partial, yes to short (no UOM, Single Item)
        self._obj.next_state = None
        self._obj._assignment_lut[0]['totalItems'] = 1
        self.post_dialog_responses('3!', 'no', 'yes')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('Put 5, ',
                              'Is this a partial?',
                              'Is this a short?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, True)
        
        #---------------------------------------------------------
        #test less, yes to partial
        self._obj.next_state = None
        self.post_dialog_responses('3!', 'yes')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('Put 5, ',
                              'Is this a partial?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertAlmostEquals(self._obj._is_partial, True)
        self.assertAlmostEquals(self._obj._is_short, False)

        #---------------------------------------------------------
        #test more, no to overpack
        self._obj.next_state = None
        self.post_dialog_responses('7!', 'no')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('Put 5, ',
                              'Is this an overpack?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PUT_PROMPT)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, False)
        
        #---------------------------------------------------------
        #test more, yes to overpack, not allowed
        self._obj.next_state = None
        self._obj._current_put['allowOverPack'] = False
        self.post_dialog_responses('7!', 'yes')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('Put 5, ',
                              'Is this an overpack?',
                              'Overpacking not allowed.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PUT_PROMPT)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, False)
        
        #---------------------------------------------------------
        #test more, yes to overpack, allowed, no residual
        self._obj.next_state = None
        self._obj._current_put['allowOverPack'] = True
        self._obj._current_put['residualQuantity'] = 0
        self.post_dialog_responses('7!', 'yes')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('Put 5, ',
                              'Is this an overpack?',
                              'Cannot overpack. There are no residuals for this item.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PUT_PROMPT)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, False)
        
        #---------------------------------------------------------
        #test more, yes to overpack, allowed, not enough residual
        self._obj.next_state = None
        self._obj._current_put['allowOverPack'] = True
        self._obj._current_put['residualQuantity'] = 2
        self.post_dialog_responses('8!', 'yes')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('Put 5, ',
                              'Is this an overpack?',
                              'Can only overpack 2 items.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PUT_PROMPT)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, False)
        
        #---------------------------------------------------------
        #test more, yes to overpack, allowed, enough residual
        self._obj.next_state = None
        self._obj._current_put['allowOverPack'] = True
        self._obj._current_put['residualQuantity'] = 5
        self.post_dialog_responses('8!', 'yes')
        
        self._obj.runState(PUT_PROMPT)
        
        self.validate_prompts('Put 5, ',
                              'Is this an overpack?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertAlmostEquals(self._obj._is_partial, False)
        self.assertAlmostEquals(self._obj._is_short, False)
        
        #---------------------------------------------------------
        #test new container
        self._obj.next_state = None
        self._obj._container_id = None 
        self.post_dialog_responses('new container', 'no', 'new container', 'yes')

        self.assertRaises(Launch, self._obj.runState, PUT_PROMPT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreNewContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreNewContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('Put 5, ', 
                              'new container, correct?', 
                              'Put 5, ', 
                              'new container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #---------------------------------------------------------
        #test close container
        self._obj._region_config_lut[0]['validate_container_id'] = False
        self._obj._validate_container = True
        self._obj.next_state = None
        self._obj._container_id = None 
        self.post_dialog_responses('close container', 'no', 'close container', 'yes')

        self.assertRaises(Launch, self._obj.runState, PUT_PROMPT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreCloseContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreCloseContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('Put 5, ', 
                              'close container, correct?', 
                              'Put 5, ', 
                              'close container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertFalse(self._obj._validate_container)
        
    def test_validate_container(self):
        #---------------------------------------------------------
        #test region not set to validate container
        self._obj.next_state = None
        self._obj._container_id = None 
        self._obj._region_config_lut[0]['validate_container_id'] = False
        self._obj._validate_container = False
        self._obj._qty_put = 9
        
        self._obj.runState(VALIDATE_CONTAINER)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._container_id, None)
        
        #---------------------------------------------------------
        #test region set to validate container, put qty = 0
        self._obj.next_state = None
        self._obj._container_id = None 
        self._obj._region_config_lut[0]['validate_container_id'] = True
        self._obj._validate_container = True
        self._obj._qty_put = 0
        
        self._obj.runState(VALIDATE_CONTAINER)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._container_id, None)
        
        #---------------------------------------------------------
        #test region set to validate container, put qty > 0
        self._obj.next_state = None
        self._obj._container_id = None 
        self._obj._region_config_lut[0]['validate_container_id'] = True
        self._obj._validate_container = True
        self._obj._qty_put = 5
        self.post_dialog_responses('11111!', 'yes')
        
        self._obj.runState(VALIDATE_CONTAINER)
        
        self.validate_prompts('Container?', '11111, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._container_id, '11111')

        #---------------------------------------------------------
        #test region set to validate container, put qty > 0, new container
        self._obj.next_state = None
        self._obj._container_id = None 
        self._obj._region_config_lut[0]['validate_container_id'] = False
        self._obj._validate_container = True
        self._obj._qty_put = 5
        self.post_dialog_responses('new container', 'no', 'new container', 'yes')

        self.assertRaises(Launch, self._obj.runState, VALIDATE_CONTAINER)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreNewContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreNewContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('Container?', 
                              'new container, correct?', 
                              'Container?', 
                              'new container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._container_id, None)
        
    def test_transmit_put(self):
        #---------------------------------------------------------
        #test out of range
        self._obj.next_state = None
        self._obj._qty_put = 9
        self.post_dialog_responses('ready')
        
        self._obj.runState(TRANSMIT_PUT)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, TRANSMIT_PUT)

        #---------------------------------------------------------
        #test success 
        self.start_server()
        self._obj.next_state = None
        self._obj._qty_put = 9
        self.set_server_response(',0\n\n')
        
        self._obj.runState(TRANSMIT_PUT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsPut','999','88','ITEM123','1','9','','22222','0'])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test success, different values 
        self._obj.next_state = None
        self._obj._qty_put = 7
        self._obj._assignment_lut[0]['groupId'] = 9876
        self._obj._current_put['customerLocationID'] = 5432
        self._obj._current_put['itemNumber'] = 'ItemNum'
        self._obj._current_put['putID'] = 1234
        self._obj._current_put['licenseNumber'] = 'licNum'
        self._obj._container_id = 'containerID'
        self._obj._is_partial = True
        self.set_server_response(',0\n\n')
        
        self._obj.runState(TRANSMIT_PUT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsPut','9876','5432','ItemNum','1234','7','containerID','licNum','1'])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test warning code 
        self._obj.next_state = None
        self.set_server_response('99,Test Warning\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(TRANSMIT_PUT)
        
        self.validate_prompts('Test Warning, say ready')
        self.validate_server_requests(['prTaskLUTPtsPut','9876','5432','ItemNum','1234','7','containerID','licNum','1'])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test general error 
        self._obj.next_state = None
        self.set_server_response('10,Test Error\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(TRANSMIT_PUT)
        
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsPut','9876','5432','ItemNum','1234','7','containerID','licNum','1'])
        self.assertEquals(self._obj.next_state, TRANSMIT_PUT)
        
        #---------------------------------------------------------
        #test error 2 
        self._obj._validate_container = False
        self._obj.next_state = None
        self.set_server_response('2,Test Error\n\n')
        self.post_dialog_responses()
        
        self._obj.runState(TRANSMIT_PUT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsPut','9876','5432','ItemNum','1234','7','containerID','licNum','1'])
        self.assertEquals(self._obj.next_state, VALIDATE_CONTAINER)
        self.assertTrue(self._obj._validate_container)
        
        #---------------------------------------------------------
        #test error 3 
        self._obj._validate_container = False
        self._obj.next_state = None
        self.set_server_response('3,Test Error\n\n')
        self.post_dialog_responses()
        
        self._obj.runState(TRANSMIT_PUT)
        
        self.validate_prompts('Test Error')
        self.validate_server_requests(['prTaskLUTPtsPut','9876','5432','ItemNum','1234','7','containerID','licNum','1'])
        self.assertEquals(self._obj.next_state, VALIDATE_CONTAINER)
        self.assertTrue(self._obj._validate_container)
        
        #---------------------------------------------------------
        #test error 1 - System generated containers
        self._obj._region_config_lut[0]['system_gen_container_id'] = True 
        self._obj.next_state = None
        self.set_server_response('1,Test Error\n\n')
        self.post_dialog_responses()
        
        self.assertRaises(Launch, self._obj.runState, TRANSMIT_PUT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreNewContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreNewContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('Opening new container.')
        self.validate_server_requests(['prTaskLUTPtsPut','9876','5432','ItemNum','1234','7','containerID','licNum','1'])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test error 1 - Operater generated containers
        self._obj._region_config_lut[0]['system_gen_container_id'] = False 
        self._obj.next_state = None
        self.set_server_response('1,Test Error\n\n')
        self.post_dialog_responses()
        
        self.assertRaises(Launch, self._obj.runState, TRANSMIT_PUT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreNewContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreNewContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('Please open a new container.')
        self.validate_server_requests(['prTaskLUTPtsPut','9876','5432','ItemNum','1234','7','containerID','licNum','1'])
        self.assertEquals(self._obj.next_state, None)
        
    def test_finish_put(self):
        #---------------------------------------------------------
        #test normal put
        self._obj.next_state = None
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 5
        
        self._obj.runState(FINISH_PUT)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['P', 'N', 'N', 'N'],
                           [5, 0, 0, 0],
                           [2, 2, 2, 2])

        #---------------------------------------------------------
        #test short, same item
        self._reset_data()
        self._obj.next_state = None
        self._obj._is_short = True
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 5
        
        self._obj.runState(FINISH_PUT)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['P', 'P', 'P', 'P'],
                           [5, 0, 0, 0],
                           [2, 2, 2, 2])

        #---------------------------------------------------------
        #test short, multiple items
        self._reset_data()
        self._obj._pts_lut[1]['itemNumber'] = 'Different'
        self._obj.next_state = None
        self._obj._is_short = True
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 5
        
        self._obj.runState(FINISH_PUT)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['P', 'N', 'P', 'P'],
                           [5, 0, 0, 0],
                           [2, 2, 2, 2])

        #---------------------------------------------------------
        #test Over packed
        self._reset_data()
        self._obj._pts_lut[1]['itemNumber'] = 'Different'
        self._obj.next_state = None
        self._obj._is_short = False
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 6
        
        self._obj.runState(FINISH_PUT)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['P', 'N', 'N', 'N'],
                           [6, 0, 0, 0],
                           [1, 2, 1, 1])


        #---------------------------------------------------------
        #test Partial, full quantity put
        self._reset_data()
        self._obj.next_state = None
        self._obj._is_partial = True
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 5
        
        self._obj.runState(FINISH_PUT)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['P', 'N', 'N', 'N'],
                           [5, 0, 0, 0],
                           [2, 2, 2, 2])

        #---------------------------------------------------------
        #test Partial, allow multiple open containers, close container yes
        self._reset_data()
        self._obj._region_config_lut[0]['allow_multi_containers'] = True
        self._obj.next_state = None
        self._obj._is_partial = True
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 2
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.runState, FINISH_PUT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreCloseContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreCloseContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('close container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['N', 'N', 'N', 'N'],
                           [2, 0, 0, 0],
                           [2, 2, 2, 2])

        #---------------------------------------------------------
        #test Partial, allow multiple open containers, close container no
        self._reset_data()
        self._obj._region_config_lut[0]['allow_multi_containers'] = True
        self._obj.next_state = None
        self._obj._is_partial = True
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 2
        self.post_dialog_responses('no')
        
        self._obj.runState(FINISH_PUT)
        
        self.validate_prompts('close container, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PUT_PROMPT)
        self.validate_puts(['N', 'N', 'N', 'N'],
                           [2, 0, 0, 0],
                           [2, 2, 2, 2])

        #---------------------------------------------------------
        #test Partial, NOT allow multiple open containers, system directed containers
        self._reset_data()
        self._obj._region_config_lut[0]['allow_multi_containers'] = False
        self._obj._region_config_lut[0]['system_gen_container_id'] = True 
        self._obj.next_state = None
        self._obj._is_partial = True
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 2
        
        self.assertRaises(Launch, self._obj.runState, FINISH_PUT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreNewContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreNewContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('Opening new container.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['N', 'N', 'N', 'N'],
                           [2, 0, 0, 0],
                           [2, 2, 2, 2])

        #---------------------------------------------------------
        #test Partial, NOT allow multiple open containers, operator directed containers
        self._reset_data()
        self._obj._region_config_lut[0]['allow_multi_containers'] = False
        self._obj._region_config_lut[0]['system_gen_container_id'] = False 
        self._obj.next_state = None
        self._obj._is_partial = True
        self._obj._current_put['quantityToPut'] = 5
        self._obj._current_put['quantityPut'] = 0
        self._obj._qty_put = 2
        
        self.assertRaises(Launch, self._obj.runState, FINISH_PUT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStoreNewContainer))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStoreNewContainerTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PUT_PROMPT)
        self.validate_prompts('Please open a new container.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.validate_puts(['N', 'N', 'N', 'N'],
                           [2, 0, 0, 0],
                           [2, 2, 2, 2])

    def validate_puts(self, status, quantityPut, residual):
        count = 0
        for put in self._obj._pts_lut:
            self.assertEquals(put['status'], status[count], 'Record %s: Expected Status %s, was %s' % (count, status[count], put['status']))
            self.assertEquals(put['quantityPut'], quantityPut[count], 'Record %s: Expected quantityPut %s, was %s' % (count, quantityPut[count], put['quantityPut']))
            self.assertEquals(put['residualQuantity'], residual[count], 'Record %s: Expected residualQuantity %s, was %s' % (count, residual[count], put['residualQuantity']))
            count += 1
            
if __name__ == '__main__':
    unittest.main()


