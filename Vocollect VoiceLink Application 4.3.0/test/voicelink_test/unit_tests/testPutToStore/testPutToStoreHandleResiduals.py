from BaseVLTestCase import BaseVLTestCase, BOTH_SERVERS #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from puttostore.PutToStoreTask import PutToStoreTask
from puttostore.PutToStoreHandleResiduals import PutToStoreHandleResiduals,\
    PTS_RESIDUALS_GET_RESIDUALS, PTS_RESIDUALS_PROMPT_RESIDUALS,\
    PTS_RESIDUALS_INIALIZE_LICENSE,\
    PTS_RESIDUALS_GET_NEXT_RESID, PTS_RESIDUALS_PROMPT_RESID_QTY,\
    PTS_RESIDUALS_DELIVER_EXCEPT, PTS_RESIDUALS_PRINT_EXCEPT

import time
import unittest
from vocollect_core.task.task_runner import Launch
from puttostore.PutToStorePrint import PutToStorePrintTask

class testPutToStoreHandleResiduals(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        pts = obj_factory.get(PutToStoreTask, VoiceLink())
        pts._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        pts._assignment_lut.receive('999,5,25,3,RL002,00,EL001,99,123,98,0,\n\n')
        pts._pts_lut.receive('N,1,88,22222,pre 1,1,post 1,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,2,88,22222,pre 1,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,3,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,4,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,5,88,22222,pre 2,2,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,6,88,22222,pre 2,3,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,7,88,22222,pre 2,4,,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             '\n')

        self._obj = obj_factory.get(PutToStoreHandleResiduals, 
                                      pts._region_config_lut, 
                                      pts._assignment_lut,
                                      pts._pts_lut,
                                      VoiceLink(), pts)

        self._reset_residual_data()
        
    def _reset_residual_data(self):
        self._obj._residuals_lut.receive('Lic1,Item1,Item 1,9,N,0,\n'
                                         'Lic2,Item2a,Item 2a,0,N,0,\n'
                                         'Lic2,Item2b,Item 2b,0,N,0,\n'
                                         'Lic3,Item3a,Item 3a,3,N,0,\n'
                                         'Lic3,Item3b,Item 3b,1,N,0,\n'
                                         'Lic4,Item1,Item 1,0,N,0,\n'
                                         '\n')
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putToStoreHandleResiduals')

        #test states
        self.assertEquals(self._obj.states[0], PTS_RESIDUALS_GET_RESIDUALS)
        self.assertEquals(self._obj.states[1], PTS_RESIDUALS_PROMPT_RESIDUALS)
        self.assertEquals(self._obj.states[2], PTS_RESIDUALS_INIALIZE_LICENSE)
        self.assertEquals(self._obj.states[3], PTS_RESIDUALS_GET_NEXT_RESID)
        self.assertEquals(self._obj.states[4], PTS_RESIDUALS_PROMPT_RESID_QTY)
        self.assertEquals(self._obj.states[5], PTS_RESIDUALS_DELIVER_EXCEPT)
        self.assertEquals(self._obj.states[6], PTS_RESIDUALS_PRINT_EXCEPT)
    
        #test LUTS defined correctly
        fields = self._obj._residuals_lut.fields
        self.assertEquals(fields['licenseNumber'].index, 0)
        self.assertEquals(fields['itemNumber'].index, 1)
        self.assertEquals(fields['itemDescription'].index, 2)
        self.assertEquals(fields['expectedResidual'].index, 3)
        self.assertEquals(fields['processed'].index, 4)
        self.assertEquals(fields['errorCode'].index, 5)
        self.assertEquals(fields['errorMessage'].index, 6)
        
    def test_get_residuals(self):
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(PTS_RESIDUALS_GET_RESIDUALS)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_RESIDUALS_GET_RESIDUALS)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response(',,,,,1,Error Message\n\n')

        self._obj.runState(PTS_RESIDUALS_GET_RESIDUALS)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsGetExpectedResidual', '999'])
        self.assertEquals(self._obj.next_state, PTS_RESIDUALS_GET_RESIDUALS)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self._obj._region_config_lut.receive('2,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self.post_dialog_responses('ready')
        self.set_server_response(',,,,,99,Warning Message\n\n')

        self._obj.runState(PTS_RESIDUALS_GET_RESIDUALS)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTPtsGetExpectedResidual', '999'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors 
        self._obj.next_state = None
        self.set_server_response('loc1,item1,desc1,0,N,0,\n\n')

        self._obj.runState(PTS_RESIDUALS_GET_RESIDUALS)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsGetExpectedResidual', '999'])
        self.assertEquals(self._obj.next_state, None)
    
    def test_prompt_residuals(self):
        self.start_server(BOTH_SERVERS)
        self.set_server_response('Y', 'prTaskODR')
        
        #--------------------------------------------------------------
        #test no to residuals, with expected
        self._obj.next_state = None
        self.post_dialog_responses('no')
        
        self._obj.runState(PTS_RESIDUALS_PROMPT_RESIDUALS)
        time.sleep(1)
        
        self.validate_prompts('Assignment complete.  Do you have residuals?')
        self.validate_server_requests(['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=Lic1'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=Lic3'])
        self.assertEquals(self._obj.next_state, '')
        
        #--------------------------------------------------------------
        #test no to residuals, with none expected
        self._obj.next_state = None
        self.post_dialog_responses('no')
        self._reset_residual_data()
        self._obj._residuals_lut[0]['expectedResidual'] = 0
        self._obj._residuals_lut[3]['expectedResidual'] = 0

        self._obj.runState(PTS_RESIDUALS_PROMPT_RESIDUALS)
        
        self.validate_prompts('Assignment complete.  Do you have residuals?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #--------------------------------------------------------------
        #test yes to residuals, with none expected, exception location blank
        self._obj.next_state = None
        self._reset_residual_data()
        self._obj._residuals_lut[0]['expectedResidual'] = 0
        self._obj._residuals_lut[3]['expectedResidual'] = 0
        self._obj._residuals_lut[4]['expectedResidual'] = 0
        self._obj._assignment_lut[0]['exceptionLocation'] = ''
        self.post_dialog_responses('yes', 'ready', '00!')
        
        self._obj.runState(PTS_RESIDUALS_PROMPT_RESIDUALS)
        time.sleep(1)
        
        self.validate_prompts('Assignment complete.  Do you have residuals?',
                              'return residual to RL002, say ready', 
                              'Check Digit?')
        self.validate_server_requests(['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsnoresiduals', '0', ''])
        self.assertEquals(self._obj.next_state, '')
        
        #--------------------------------------------------------------
        #test yes to residuals, with expected, exception location same as return
        self._obj.next_state = None
        self._obj._assignment_lut[0]['exceptionLocation'] = self._obj._assignment_lut[0]['returnLocation']
        self._obj._assignment_lut[0]['exceptionCheckdigit'] = self._obj._assignment_lut[0]['returnCheckdigit']
        self._reset_residual_data()
        self.post_dialog_responses('yes', 'ready', '00!')
        
        self._obj.runState(PTS_RESIDUALS_PROMPT_RESIDUALS)
        
        self.validate_prompts('Assignment complete.  Do you have residuals?',
                              'return residual to RL002, say ready', 
                              'Check Digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #--------------------------------------------------------------
        #test yes to residuals, with expected, exception location different from return
        self._obj.next_state = None
        self._obj._assignment_lut[0]['exceptionLocation'] = self._obj._assignment_lut[0]['returnLocation']
        self._obj._assignment_lut[0]['exceptionCheckdigit'] = '99'
        self._reset_residual_data()
        self.post_dialog_responses('yes')
        
        self._obj.runState(PTS_RESIDUALS_PROMPT_RESIDUALS)
        
        self.validate_prompts('Assignment complete.  Do you have residuals?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_initialize_license(self):
        #--------------------------------------------------------------
        #test license 1
        self._obj.next_state = None
        self._reset_residual_data()
        self.post_dialog_responses()
        
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._lic_residuals), 1)
        self.assertEquals(self._obj._lic_residuals[0], self._obj._residuals_lut[0])
        
        #--------------------------------------------------------------
        #test license 2
        for r in self._obj._lic_residuals:
            r['processed'] = 'P'
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        
        self.validate_prompts('Confirm residuals for license Lic2')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._lic_residuals), 2)
        self.assertEquals(self._obj._lic_residuals[0], self._obj._residuals_lut[1])
        self.assertEquals(self._obj._lic_residuals[1], self._obj._residuals_lut[2])

        #--------------------------------------------------------------
        #test license 3
        for r in self._obj._lic_residuals:
            r['processed'] = 'P'
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        
        self.validate_prompts('Confirm residuals for license Lic3')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._lic_residuals), 2)
        self.assertEquals(self._obj._lic_residuals[0], self._obj._residuals_lut[3])
        self.assertEquals(self._obj._lic_residuals[1], self._obj._residuals_lut[4])

        #--------------------------------------------------------------
        #test license 4
        for r in self._obj._lic_residuals:
            r['processed'] = 'P'
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(len(self._obj._lic_residuals), 1)
        self.assertEquals(self._obj._lic_residuals[0], self._obj._residuals_lut[5])

    def test_get_next_resid(self):
        self._reset_residual_data()

        #--------------------------------------------------------------
        #test single item license
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)

        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._curr_residual, self._obj._residuals_lut[0])
        
        self._obj.next_state = None
        self.post_dialog_responses('ready', '00!')
        
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)
        
        self.validate_prompts('return residual to RL002, say ready', 
                              'Check Digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_RESIDUALS_INIALIZE_LICENSE)
        
        #--------------------------------------------------------------
        #test multiple item license
        self._obj._residuals_lut[0]['processed'] = 'P'
        
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        self.validate_prompts('Confirm residuals for license Lic2')

        self._obj.next_state = None
        self.post_dialog_responses()

        for i in range(2):
            self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)
            
            self.validate_prompts()
            self.validate_server_requests()
            self.assertEquals(self._obj.next_state, None)
            self.assertEquals(self._obj._curr_residual, self._obj._residuals_lut[i+1])

        
        self._obj.next_state = None
        self.post_dialog_responses('ready', '00!')
        
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)
        
        self.validate_prompts('return residual to RL002, say ready', 
                              'Check Digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_RESIDUALS_INIALIZE_LICENSE)
        
        
    def test_prompt_resid_qty(self):
        self._reset_residual_data()

        #--------------------------------------------------------------
        #test single item license, correct quantity
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)

        self._obj.next_state = None
        self.post_dialog_responses('9!', 'yes')
        
        self._obj.runState(PTS_RESIDUALS_PROMPT_RESID_QTY)
        
        self.validate_prompts('Confirm residuals for license Lic1', '9, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_RESIDUALS_GET_NEXT_RESID)
        self.assertEquals(self._obj._residuals_lut[0]['processed'], 'P')
        
        #--------------------------------------------------------------
        #test multiple item license, incorrect quantity
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        self.validate_prompts('Confirm residuals for license Lic2')
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)

        self._obj.next_state = None
        self.post_dialog_responses('9!', 'yes')
        
        self._obj.runState(PTS_RESIDUALS_PROMPT_RESID_QTY)
        
        self.validate_prompts('How many for item Item 2a?', 
                              '9, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._residuals_lut[1]['processed'], 'P')
        self.assertEquals(self._obj._residuals_lut[2]['processed'], 'P')
    
        #--------------------------------------------------------------
        #test multiple item license, item number
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        self.validate_prompts('Confirm residuals for license Lic3')
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)

        self._obj.next_state = None
        self.post_dialog_responses('item number')
        
        self._obj.runState(PTS_RESIDUALS_PROMPT_RESID_QTY)
        
        self.validate_prompts('How many for item Item 3a?', 'Item3a')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_RESIDUALS_PROMPT_RESID_QTY)
        self.assertEquals(self._obj._residuals_lut[3]['processed'], 'N')
        self.assertEquals(self._obj._residuals_lut[4]['processed'], 'N')
    
    def test_deliver_exception(self):
        self._reset_residual_data()
        self.start_server(BOTH_SERVERS)
        
        #--------------------------------------------------------------
        #test single item license, correct quantity
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)

        self._obj.next_state = None
        self.post_dialog_responses('ready', '99!')
        
        self._obj.runState(PTS_RESIDUALS_DELIVER_EXCEPT)
        time.sleep(1)
        
        self.validate_prompts('return residual to EL001, say ready', 
                              'Check Digit?')
        self.validate_server_requests(['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.unexpectedresidual', '0', 'task.licenseNumber=Lic1|task.itemNumber=Item1'])
        self.assertEquals(self._obj.next_state, None)
    
    def test_print_exception(self):
        self._reset_residual_data()
        self._obj.runState(PTS_RESIDUALS_INIALIZE_LICENSE)
        self._obj.runState(PTS_RESIDUALS_GET_NEXT_RESID)

        #--------------------------------------------------------------
        #Test region set to not print
        self._obj._region_config_lut[0]['print_exception_label'] = False
        
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(PTS_RESIDUALS_PRINT_EXCEPT)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_RESIDUALS_INIALIZE_LICENSE)
    
        #--------------------------------------------------------------
        #Test region set to print
        self._obj._region_config_lut[0]['print_exception_label'] = True
        
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self.assertRaises(Launch, self._obj.runState, PTS_RESIDUALS_PRINT_EXCEPT)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStorePrintTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStorePrint')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PTS_RESIDUALS_INIALIZE_LICENSE)
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj._group_or_license, 'Lic1')
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj._location_or_item, 'Item1')
        self.assertEquals(self._obj.taskRunner.task_stack[0].obj._contatiner_id, None)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

    def test_deliver_prompts(self):
        #Above tests covered when a location and check digit were provided
        
        #--------------------------------------------------------------
        #Test location blank
        self.post_dialog_responses('ready')
        self._obj._deliver_prompts('', '00')
        self.validate_prompts('return residual')
        
        #--------------------------------------------------------------
        #Test CD blank
        self.post_dialog_responses('ready')
        self._obj._deliver_prompts('test location', '')
        self.validate_prompts('return residual to test location, say ready')
        
        #--------------------------------------------------------------
        #Test speaking location at check digit prompt
        self.post_dialog_responses('ready', 'location', 'ready', '00!')
        self._obj._deliver_prompts('test location', '00')
        self.validate_prompts('return residual to test location, say ready', 
                              'Check Digit?', 
                              'test location, say ready', 
                              'Check Digit?')
        
        
        
if __name__ == '__main__':
    unittest.main()


