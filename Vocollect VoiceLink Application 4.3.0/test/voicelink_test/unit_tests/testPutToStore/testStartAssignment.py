from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from puttostore.StartAssignment import StartAssignmentTask, GET_ASSIGNEMNT, \
    ASSIGNMENT_SUMMARY, GET_PUTS 
from puttostore.PutToStoreTask import PutToStoreTask

import unittest

class testStartAssignment(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        pts = obj_factory.get(PutToStoreTask, VoiceLink())
        pts._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        self._obj = obj_factory.get(StartAssignmentTask,
                                      pts._region_config_lut, 
                                      pts._assignment_lut,
                                      pts._pts_lut,
                                      VoiceLink(), pts)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'startAssignment')

        #test states
        self.assertEquals(self._obj.states[0], GET_ASSIGNEMNT)
        self.assertEquals(self._obj.states[1], ASSIGNMENT_SUMMARY)
        self.assertEquals(self._obj.states[2], GET_PUTS)
        
    def test_get_assignment(self):

        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(GET_ASSIGNEMNT)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, GET_ASSIGNEMNT)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response('999,5,25,3,RL002,00,EL001,99,123,98,1,Error Message\n\n')

        self._obj.runState(GET_ASSIGNEMNT)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsGetAssignment'])
        self.assertEquals(self._obj.next_state, GET_ASSIGNEMNT)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('999,5,25,3,RL002,00,EL001,99,123,98,99,Warning Message\n\n')

        self._obj.runState(GET_ASSIGNEMNT)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTPtsGetAssignment'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors or warnings
        self.post_dialog_responses()
        self.set_server_response('999,5,25,3,RL002,00,EL001,99,123,98,0,\n\n')

        self._obj.runState(GET_ASSIGNEMNT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsGetAssignment'])
        self.assertEquals(self._obj.next_state, None)
        
    def test_assignment_summary(self):
        self._obj._assignment_lut.receive('999,1,25,3,RL002,00,EL001,99,123,98,0,\n\n')
        
        #-----------------------------------------------------------------
        #test single location, single item
        self._obj._assignment_lut[0]['totalSlots'] = 1
        self._obj._assignment_lut[0]['totalItems'] = 1
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(ASSIGNMENT_SUMMARY)
        
        self.validate_prompts('Assignment has 1 slot location 1 item, and a total expected residual of 3, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test single location, multiple item
        self._obj._assignment_lut[0]['totalSlots'] = 1
        self._obj._assignment_lut[0]['totalItems'] = 2
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(ASSIGNMENT_SUMMARY)
        
        self.validate_prompts('Assignment has 1 slot location 2 items, and a total expected residual of 3, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test multiple location, single item
        self._obj._assignment_lut[0]['totalSlots'] = 2
        self._obj._assignment_lut[0]['totalItems'] = 1
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(ASSIGNMENT_SUMMARY)
        
        self.validate_prompts('Assignment has 2 slot locations 1 item, and a total expected residual of 3, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test multiple location, multiple item
        self._obj._assignment_lut[0]['totalSlots'] = 2
        self._obj._assignment_lut[0]['totalItems'] = 2
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(ASSIGNMENT_SUMMARY)
        
        self.validate_prompts('Assignment has 2 slot locations 2 items, and a total expected residual of 3, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no residual
        self._obj._assignment_lut[0]['expectedResidual'] = 0
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(ASSIGNMENT_SUMMARY)
        
        self.validate_prompts('Assignment has 2 slot locations 2 items, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test performance
        self._obj.next_state = None
        self.post_dialog_responses('performance', 'ready')

        self._obj.runState(ASSIGNMENT_SUMMARY)
        
        self.validate_prompts('Assignment has 2 slot locations 2 items, say ready',
                              'performance of last assignment was 123 percent of standard. Your daily performance is 98 percent of standard. To continue, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_SUMMARY)
        
        #-----------------------------------------------------------------
        #test performance not available
        self._obj._assignment_lut[0]['perfLast'] = -1
        self._obj.next_state = None
        self.post_dialog_responses('performance')

        self._obj.runState(ASSIGNMENT_SUMMARY)
        
        self.validate_prompts('Assignment has 2 slot locations 2 items, say ready',
                              'Performance not available.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ASSIGNMENT_SUMMARY)
        
        
        
    def test_get_puts(self):
        self._obj._assignment_lut.receive('999,5,25,3,RL002,00,EL001,99,123,98,0,\n\n')
        
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(GET_PUTS)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, GET_PUTS)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response('N,1,999,22222,pre,aisle,post,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,1,Error Message\n\n')

        self._obj.runState(GET_PUTS)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsGetPuts', '999'])
        self.assertEquals(self._obj.next_state, GET_PUTS)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('N,1,999,22222,pre,aisle,post,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,99,Warning Message\n\n')

        self._obj.runState(GET_PUTS)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTPtsGetPuts', '999'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors or warnings
        self._obj._assignment_lut[0]['groupId'] = '111'
        self.post_dialog_responses()
        self.set_server_response('N,1,999,22222,pre,aisle,post,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n\n')

        self._obj.runState(GET_PUTS)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsGetPuts', '111'])
        self.assertEquals(self._obj.next_state, None)
        
        
if __name__ == '__main__':
    unittest.main()


