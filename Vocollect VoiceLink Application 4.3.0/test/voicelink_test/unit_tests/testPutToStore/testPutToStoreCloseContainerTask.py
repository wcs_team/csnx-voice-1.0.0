from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from common.VoiceLinkLut import VoiceLinkLut
from puttostore.PutToStoreCloseContainerTask import PutToStoreCloseContainer, \
    PTS_CLOSE_CONT_SPECIFY_LOC, PTS_CLOSE_CONT_SPECIFY_CD,\
    PTS_CLOSE_CONT_VERIFY_LOC, PTS_CLOSE_CONT_CLOSE_CONT,\
    PTS_CLOSE_CONT_SPECIFY_CONT, PTS_CLOSE_CONT_SELECT_CONT
    
import unittest

class testPutToStoreCloseContainer(BaseVLTestCase):
    
    def setUp(self):
        #clear test object before beginning a test
        self.clear()
        #create test object
        self._obj = obj_factory.get(PutToStoreCloseContainer,
                                      obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetRegionConfiguration'),
                                      12345,
                                      '67890',
                                      VoiceLink())
        
        self._reset_data()

    def _reset_data(self):
        #populate with initial data
        self._obj._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,2,pre,aisle,post,0,0,\n\n')
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putToStoreCloseContainerTask')

        #test states
        self.assertEquals(self._obj.states[0], PTS_CLOSE_CONT_SPECIFY_LOC)
        self.assertEquals(self._obj.states[1], PTS_CLOSE_CONT_SPECIFY_CD)
        self.assertEquals(self._obj.states[2], PTS_CLOSE_CONT_VERIFY_LOC)
        self.assertEquals(self._obj.states[3], PTS_CLOSE_CONT_CLOSE_CONT)
        self.assertEquals(self._obj.states[4], PTS_CLOSE_CONT_SPECIFY_CONT)
        self.assertEquals(self._obj.states[5], PTS_CLOSE_CONT_SELECT_CONT)
    
        #test LUTS defined correctly
        fields = self._obj._verify_location_lut.fields
        self.assertEquals(fields['customerLocationID'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        fields = self._obj._containers_lut.fields
        self.assertEquals(fields['customerLocationID'].index, 0)
        self.assertEquals(fields['containerNumber'].index, 1)
        self.assertEquals(fields['spokenContainerNumber'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)
        
    def test_specify_location_id(self):
        
        #---------------------------------------------------------
        #test location ID already specified
        self._obj._location_id = 12345
        self._obj.next_state = None
        self.post_dialog_responses()
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_LOC)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_CLOSE_CONT)
        
        #---------------------------------------------------------
        #test location ID not provided, confirm location true
        self._obj._location_id = None
        self._obj.next_state = None
        self._obj._region_config_lut[0]['confirm_spoken_location'] = True
        self.post_dialog_responses('123AB!', 'yes')
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_LOC)
        
        self.validate_prompts('Location?',
                              '<spell>123AB</spell>, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_entry, '123AB')
        self.assertEquals(self._obj._loc_scanned, False)
        
        #---------------------------------------------------------
        #test location ID not provided, confirm location false
        self._obj._location_id = None
        self._obj.next_state = None
        self._obj._region_config_lut[0]['confirm_spoken_location'] = False
        self.post_dialog_responses('12345!')
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_LOC)
        
        self.validate_prompts('Location?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_entry, '12345')
        self.assertEquals(self._obj._loc_scanned, False)
        
        #---------------------------------------------------------
        #test location ID not provided, scanned
        self._obj._location_id = None
        self._obj.next_state = None
        self._obj._region_config_lut[0]['confirm_spoken_location'] = True
        self.post_dialog_responses('#12345')
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_LOC)
        
        self.validate_prompts('Location?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_VERIFY_LOC)
        self.assertEquals(self._obj._location_entry, '12345')
        self.assertEquals(self._obj._loc_scanned, True)
        
    def test_specify_check_digits(self):
        
        #---------------------------------------------------------
        #test entering check digits
        self._obj.next_state = None
        self.post_dialog_responses('22!')
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_CD)
        
        self.validate_prompts('Check digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_CD, '22')
        
        #---------------------------------------------------------
        #test entering check digits, scanned
        self._obj.next_state = None
        self.post_dialog_responses('#33')
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_CD)
        
        self.validate_prompts('Check digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_CD, '33')
        
    def test_verify_location(self):
        #---------------------------------------------------------
        #test out of range
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(PTS_CLOSE_CONT_VERIFY_LOC)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_SPECIFY_LOC)
        
        #---------------------------------------------------------
        #test error code
        self.start_server()
        self._obj.next_state = None
        self._obj._loc_scanned = True
        self._obj._location_entry = '12345'
        self._obj._location_CD = ''
        
        self.post_dialog_responses('ready')
        self.set_server_response(',11,Test Error\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_VERIFY_LOC)
        
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsVerifyCustomerLocation','1','12345',''])
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_SPECIFY_LOC)
        
        #---------------------------------------------------------
        #test warning code
        self._obj.next_state = None
        self._obj._loc_scanned = False
        self._obj._location_entry = '67890'
        self._obj._location_CD = '12'
        self.post_dialog_responses('ready')
        self.set_server_response(',99,Test Warning\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_VERIFY_LOC)
        
        self.validate_prompts('Test Warning, say ready')
        self.validate_server_requests(['prTaskLUTPtsVerifyCustomerLocation','0','67890','12'])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test normal
        self._obj.next_state = None
        self.post_dialog_responses()
        self.set_server_response('1234,0,\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_VERIFY_LOC)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsVerifyCustomerLocation','0','67890','12'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_id, 1234)
        
    def test_close_container(self):
        
        #---------------------------------------------------------
        #test out of range
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(PTS_CLOSE_CONT_CLOSE_CONT)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_CLOSE_CONT)
        
        #---------------------------------------------------------
        #test warning code
        self.start_server()
        self._obj.next_state = None
        self._obj._location_id = 67890
        self._obj._container_id = None
        self.post_dialog_responses('ready')
        self.set_server_response(',,,99,Test Warning\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_CLOSE_CONT)
        
        self.validate_prompts('Test Warning, say ready')
        self.validate_server_requests(['prTaskLUTPtsContainer','1','67890',''])
        self.assertEquals(self._obj.next_state, '')
        
        #---------------------------------------------------------
        #test normal
        self._obj.next_state = None
        self._obj._location_id = 99999
        self._obj._container_id = '123AB'
        self.post_dialog_responses()
        self.set_server_response('123,456,789,0,\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_CLOSE_CONT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsContainer','1','99999','123AB'])
        self.assertEquals(self._obj.next_state, '')
        
        #---------------------------------------------------------
        #test general error code
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response(',,,11,Test Error\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_CLOSE_CONT)
        
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsContainer','1','99999','123AB'])
        self.assertEquals(self._obj.next_state, '')
        
        #---------------------------------------------------------
        #test error code 8
        self._obj.next_state = None
        self.post_dialog_responses()
        self.set_server_response(',,,8,Test Error\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_CLOSE_CONT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsContainer','1','99999','123AB'])
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_SELECT_CONT)
        
        #---------------------------------------------------------
        #test error code 3
        self._obj.next_state = None
        self.post_dialog_responses()
        self.set_server_response(',,,3,Test Error\n\n')
        
        self._obj.runState(PTS_CLOSE_CONT_CLOSE_CONT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsContainer','1','99999','123AB'])
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_SPECIFY_CONT)
    
    def test_specify_container(self):
        
        #---------------------------------------------------------
        #test entering container
        self._obj.next_state = None
        self.post_dialog_responses('12345!', 'yes')
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_CONT)
        
        self.validate_prompts('Container to close?',
                              '12345, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_CLOSE_CONT)
        self.assertEquals(self._obj._container_id, '12345')
        
        #---------------------------------------------------------
        #test entering container, scanned
        self._obj.next_state = None
        self.post_dialog_responses('#12345ABCDE')
        
        self._obj.runState(PTS_CLOSE_CONT_SPECIFY_CONT)
        
        self.validate_prompts('Container to close?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_CLOSE_CONT)
        self.assertEquals(self._obj._container_id, '12345ABCDE')
        
    def test_select_container(self):
        self._obj._container_id = None
        self._obj._containers_lut.receive('111,C1111,S111,0,\n\n')

        #---------------------------------------------------------
        #test selecting from 1
        self._obj.next_state = None
        self.post_dialog_responses('no')
        
        self._obj.runState(PTS_CLOSE_CONT_SELECT_CONT)
        
        self.validate_prompts('Close container C1111?', 
                              'End of matching container. Starting over.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_SELECT_CONT)
        self.assertEquals(self._obj._container_id, None)
        
        #---------------------------------------------------------
        #test selecting from 1
        self._obj.next_state = None
        self.post_dialog_responses('yes')
        
        self._obj.runState(PTS_CLOSE_CONT_SELECT_CONT)
        
        self.validate_prompts('Close container C1111?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_CLOSE_CONT)
        self.assertEquals(self._obj._container_id, 'C1111')
        
        #---------------------------------------------------------
        #test selecting from multiple
        self._obj._container_id = None
        self._obj._containers_lut.receive('111,C1111,S111,0,\n'
                                          '222,C2222,S222,0,\n'
                                          '333,C3333,S333,0,\n'
                                          '\n')
        self._obj.next_state = None
        self.post_dialog_responses('no', 'no', 'no')
        
        self._obj.runState(PTS_CLOSE_CONT_SELECT_CONT)
        
        self.validate_prompts('Close container C1111?', 
                              'Close container C2222?', 
                              'Close container C3333?', 
                              'End of matching containers. Starting over.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_SELECT_CONT)
        self.assertEquals(self._obj._container_id, None)
        
        #---------------------------------------------------------
        #test selecting from multiple
        self._obj.next_state = None
        self.post_dialog_responses('no', 'yes')
        
        self._obj.runState(PTS_CLOSE_CONT_SELECT_CONT)
        
        self.validate_prompts('Close container C1111?', 
                              'Close container C2222?')
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_CLOSE_CONT_CLOSE_CONT)
        self.assertEquals(self._obj._container_id, 'C2222')
        
        #---------------------------------------------------------
        #test selecting cancel
        self._obj._container_id = None
        self._obj.next_state = None
        self.post_dialog_responses('no', 'cancel', 'no', 'cancel', 'yes')
        
        self._obj.runState(PTS_CLOSE_CONT_SELECT_CONT)
        
        self.validate_prompts('Close container C1111?', 
                              'Close container C2222?',
                              'Cancel this close container request?', 
                              'Close container C2222?', 
                              'Cancel this close container request?')
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._container_id, None)
        
        
if __name__ == '__main__':
    unittest.main()


