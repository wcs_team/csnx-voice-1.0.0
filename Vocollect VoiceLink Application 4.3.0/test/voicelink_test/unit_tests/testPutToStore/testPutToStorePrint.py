from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from puttostore.PutToStorePrint import PutToStorePrintTask, PRINT_LABELS,\
    PRINT_RETRIEVE
import unittest
from core.Print import ENTER_PRINTER, CONFIRM_PRINTER
from puttostore.PutToStoreTask import PutToStoreTask

class testPutToStorePrint(BaseVLTestCase):
    
    def setUp(self):
        #clear test object before beginning a test
        self.clear()
        #NOTE: Tests for enter_printer, and confirm_printer are in the core 
        #section. This file only tests extended or changed functionality
        
        temp = obj_factory.get(PutToStoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.printer_number = '19'
        
        #create test object
        self._obj = obj_factory.get(PutToStorePrintTask,
                                      99999,
                                      12345,
                                      '67890',
                                      PutToStorePrintTask.LABEL_TYPE_NEW_CONTAINER,
                                      temp.taskRunner, temp)
        

    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putToStorePrint')

        #test states
        self.assertEquals(self._obj.states[0], ENTER_PRINTER)
        self.assertEquals(self._obj.states[1], CONFIRM_PRINTER)
        self.assertEquals(self._obj.states[2], PRINT_LABELS)
        self.assertEquals(self._obj.states[3], PRINT_RETRIEVE)
    
        #test LUTS defined correctly
        fields = self._obj._print_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)
        
    def test_print_labels_new_containers(self):
        #---------------------------------------------------------
        #test out of range
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        
        #---------------------------------------------------------
        #test error code
        self.start_server()
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('1,Test Error\n\n')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsPrintContainerLabel','99999','19','12345','67890'])
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        
        #---------------------------------------------------------
        #test warning code
        self._obj.next_state = None
        self._obj._loc_scanned = False
        self.post_dialog_responses('ready')
        self.set_server_response('99,Test Warning\n\n')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts('Test Warning, say ready')
        self.validate_server_requests(['prTaskLUTPtsPrintContainerLabel','99999','19','12345','67890'])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test normal
        self._obj.next_state = None
        self.post_dialog_responses()
        self.set_server_response('0,\n\n')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsPrintContainerLabel','99999','19','12345','67890'])
        self.assertEquals(self._obj.next_state, None)
    
    def test_retrieve_labels_new_container(self):
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(PRINT_RETRIEVE)
        
        self.validate_prompts('Retrieve label from printer <spell>19</spell>, then say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_print_labels_exceptions(self):
        self._obj._label_type = self._obj.LABEL_TYPE_EXCEPTION
        
        #---------------------------------------------------------
        #test out of range
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        
        #---------------------------------------------------------
        #test error code
        self.start_server()
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('1,Test Error\n\n')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts('Test Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTPtsPrintContainerLabel', '19', '99999', '12345'])
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        
        #---------------------------------------------------------
        #test warning code
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('99,Test Warning\n\n')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts('Test Warning, say ready')
        self.validate_server_requests(['prTaskLUTPtsPrintContainerLabel', '19', '99999', '12345'])
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------
        #test normal
        self._obj.next_state = None
        self.post_dialog_responses()
        self.set_server_response('0,\n\n')
        
        self._obj.runState(PRINT_LABELS)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTPtsPrintContainerLabel', '19', '99999', '12345'])
        self.assertEquals(self._obj.next_state, None)
    
    def test_retrieve_labels_exceptions(self):
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self._obj._label_type = self._obj.LABEL_TYPE_EXCEPTION
        
        self._obj.runState(PRINT_RETRIEVE)
        
        self.validate_prompts('Retrieve exception label from printer <spell>19</spell> and attach, then say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
if __name__ == '__main__':
    unittest.main()


