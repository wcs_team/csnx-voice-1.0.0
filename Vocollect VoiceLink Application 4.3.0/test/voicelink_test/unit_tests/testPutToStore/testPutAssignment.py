from BaseVLTestCase import BaseVLTestCase, BOTH_SERVERS #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from puttostore.PutToStorePutAssignment import PutAssignmentTask, \
    PTS_ASSIGNMENT_CHECK_NEXT_PUT, \
    PTS_ASSIGNMENT_PREAISLE, PTS_ASSIGNMENT_AISLE, PTS_ASSIGNMENT_POSTAISLE, \
    PTS_ASSIGNMENT_PUTPROMPT, PTS_ASSIGNMENT_END_PUTTING, PTS_ASSIGNMENT_RESET
from puttostore.PutToStoreTask import PutToStoreTask
import time

import unittest
from vocollect_core.task.task_runner import Launch
from puttostore.PutToStorePutTask import PutToStorePutTask

class testPutAssignment(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        pts = obj_factory.get(PutToStoreTask, VoiceLink())
        pts._region_config_lut.receive('1,put to store region 1,1,1,1,1,1,1,3,1,5,0,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n')
        pts._assignment_lut.receive('999,5,25,3,RL002,00,EL001,99,123,98,0,\n\n')
        pts._pts_lut.receive('N,1,88,22222,pre 1,1,post 1,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,2,88,22222,pre 1,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,3,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,4,88,22222,pre 2,1,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,5,88,22222,pre 2,2,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,6,88,22222,pre 2,3,post 2,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             'N,7,88,22222,pre 2,4,,slot,00,99,SCAN99,5,0,ITEM123,Description,uom,1,0,0,\n'
                             '\n')

        self._obj = obj_factory.get(PutAssignmentTask, pts._region_config_lut, 
                                        pts._assignment_lut,
                                        pts._pts_lut,
                                        VoiceLink(), pts)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putAssignment')

        #test states
        self.assertEquals(self._obj.states[0], PTS_ASSIGNMENT_RESET)
        self.assertEquals(self._obj.states[1], PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        self.assertEquals(self._obj.states[2], PTS_ASSIGNMENT_PREAISLE)
        self.assertEquals(self._obj.states[3], PTS_ASSIGNMENT_AISLE)
        self.assertEquals(self._obj.states[4], PTS_ASSIGNMENT_POSTAISLE)
        self.assertEquals(self._obj.states[5], PTS_ASSIGNMENT_PUTPROMPT)
        self.assertEquals(self._obj.states[6], PTS_ASSIGNMENT_END_PUTTING)
    
    def test_reset(self):
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        
        self._obj.runState(PTS_ASSIGNMENT_RESET)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, '')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
    def test_check_next_put(self):
        #----------------------------------------------------------
        #test returns first put
        self._obj.next_state = None
        self._obj.runState(PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._current_put['putID'], 1)
        
        #----------------------------------------------------------
        #test returns first put
        self._obj.next_state = None
        self._obj._pts_lut[0]['status'] = 'P'
        self._obj._pts_lut[1]['status'] = 'P'
        self._obj._pts_lut[2]['status'] = 'P'
        self._obj._pts_lut[4]['status'] = 'P'
        
        self._obj.runState(PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._current_put['putID'], 4)
        
        #----------------------------------------------------------
        #test no more puts
        self._obj.next_state = None
        self._obj._pts_lut[3]['status'] = 'P'
        self._obj._pts_lut[5]['status'] = 'P'
        self._obj._pts_lut[6]['status'] = 'P'
        
        self._obj.runState(PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PTS_ASSIGNMENT_END_PUTTING)
        self.assertEquals(self._obj._current_put, None)
        
    def test_pre_aisle(self):
        self._obj._current_put = self._obj._pts_lut[0]

        #-------------------------------------------------
        #test pre aisle is spoken when new pre-asile
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses('ready')
        
        self._obj.runState(PTS_ASSIGNMENT_PREAISLE)

        self.validate_prompts("pre 1")
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'pre 1')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when matches previous
        self._obj._pre_aisle_direction = 'pre 1'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_PREAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'pre 1')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'pre 1'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['preAisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_PREAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, '')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = ''
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['preAisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_PREAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, '')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)
        
    def test_aisle(self):
        self._obj._current_put = self._obj._pts_lut[0]

        #-------------------------------------------------
        #test aisle is spoken when new asile
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses('ready')
        
        self._obj.runState(PTS_ASSIGNMENT_AISLE)

        self.validate_prompts('Aisle <spell>1</spell>')
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '1')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test aisle is not spoken when matches previous
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = '1'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_AISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '1')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = '1'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_AISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = ''
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_AISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test skip aisle is called
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['aisle'] = '1'
        self.post_dialog_responses('skip aisle',
                                   'no')
        
        self._obj.runState(PTS_ASSIGNMENT_AISLE)

        self.validate_prompts('Aisle <spell>1</spell>',
                              'Skip aisle, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, PTS_ASSIGNMENT_AISLE)
        
    def test_post_aisle(self):
        self._obj._current_put = self._obj._pts_lut[0]

        #-------------------------------------------------
        #test post aisle is spoken when new post-asile
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses('ready')
        
        self._obj.runState(PTS_ASSIGNMENT_POSTAISLE)

        self.validate_prompts("post 1")
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'post 1')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test post aisle is not spoken when matches previous
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'post 1'
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_POSTAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'post 1')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test post aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['postAisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_POSTAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test post aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = ''
        self._obj._current_put['postAisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(PTS_ASSIGNMENT_POSTAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
    def test_put_prompt(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, PTS_ASSIGNMENT_PUTPROMPT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, PutToStorePutTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putToStorePutTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        
        
    def test_end_puts(self):
        self.start_server(BOTH_SERVERS)
        self._obj._current_put = self._obj._pts_lut[0]
        
        self._obj._pts_lut[0]['status'] = 'P'
        self._obj._pts_lut[1]['status'] = 'P'
        self._obj._pts_lut[2]['status'] = 'P'
        self._obj._pts_lut[3]['status'] = 'P'
        self._obj._pts_lut[4]['status'] = 'P'
        self._obj._pts_lut[5]['status'] = 'P'
        self._obj._pts_lut[6]['status'] = 'P'
        
        #------------------------------------------------
        #Test no skips
        self._obj.runState(PTS_ASSIGNMENT_END_PUTTING)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #------------------------------------------------
        #Test skips with LUT
        self._obj._pts_lut[2]['status'] = 'S'
        self._obj._pts_lut[5]['status'] = 'S'
        self._obj._region_config_lut[0]['use_lut'] = 2
        self.post_dialog_responses('ready')
        self.set_server_response(',0\n\n')

        self._obj.runState(PTS_ASSIGNMENT_END_PUTTING)
        
        self.validate_prompts('To put skips, say ready')
        self.validate_server_requests(['prTaskLutPtsUpdateStatus','999','','2','N'])
        self.assertEquals(self._obj.next_state, PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        self.assertEquals(self._obj._pts_lut[2]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[5]['status'], 'N')
    
        #------------------------------------------------
        #Test skips with ODR
        self._obj._pts_lut[2]['status'] = 'S'
        self._obj._pts_lut[5]['status'] = 'S'
        self._obj._region_config_lut[0]['use_lut'] = 0
        self.post_dialog_responses('ready')
        self.set_server_response(',0\n\n')

        self._obj.runState(PTS_ASSIGNMENT_END_PUTTING)
        time.sleep(1) #added to ensure ODR had time to be received
        
        self.validate_prompts('To put skips, say ready')
        self.validate_server_requests(['prTaskODRPtsUpdateStatus','999','','2','N'])
        self.assertEquals(self._obj.next_state, PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        self.assertEquals(self._obj._pts_lut[2]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[5]['status'], 'N')
    
    def test_skip_aisle(self):
        self.start_server(BOTH_SERVERS)
        self._obj._current_put = self._obj._pts_lut[0]
        
        #------------------------------------------------
        #test skip aisle not allowed
        self._obj._region_config_lut[0]['allow_skip_aisle'] = False
        self.post_dialog_responses()
        
        self._obj._skip_aisle()
        self.validate_prompts('skip aisle not allowed')
        self.validate_server_requests()
        
        #------------------------------------------------
        #test skip aisle allowed, but last aisle
        self._obj._region_config_lut[0]['allow_skip_aisle'] = True
        self._obj._pts_lut[2]['status'] = 'P'
        self._obj._pts_lut[3]['status'] = 'P'
        self._obj._pts_lut[4]['status'] = 'P'
        self._obj._pts_lut[5]['status'] = 'P'
        self._obj._pts_lut[6]['status'] = 'P'
        self.post_dialog_responses()
        
        self._obj._skip_aisle()
        
        self.validate_prompts('Last aisle. Skip aisle not allowed.')
        self.validate_server_requests()

        self._obj._pts_lut[2]['status'] = 'N'
        self._obj._pts_lut[3]['status'] = 'N'
        self._obj._pts_lut[4]['status'] = 'N'
        self._obj._pts_lut[5]['status'] = 'N'
        self._obj._pts_lut[6]['status'] = 'N'
        
        #------------------------------------------------
        #test skip aisle allowed, confirm no
        curr_state = self._obj.next_state
        self.post_dialog_responses('no')
        
        self._obj._skip_aisle()
        
        self.validate_prompts('Skip aisle, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, curr_state)
        
        #------------------------------------------------
        #test skip aisle allowed, confirm yes, update with LUT
        self._obj._region_config_lut[0]['use_lut'] = 2
        self.post_dialog_responses('yes')
        self.set_server_response(',0\n\n')
        
        self._obj._skip_aisle()
        
        self.validate_prompts('Skip aisle, correct?')
        self.validate_server_requests(['prTaskLutPtsUpdateStatus','999','88','1','S'])
        self.assertEquals(self._obj._pts_lut[0]['status'], 'S')
        self.assertEquals(self._obj._pts_lut[1]['status'], 'S')
        self.assertEquals(self._obj._pts_lut[2]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[3]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[4]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[5]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[6]['status'], 'N')
        self.assertEquals(self._obj.next_state, PTS_ASSIGNMENT_CHECK_NEXT_PUT)
 
        #------------------------------------------------
        #test skip aisle allowed, confirm yes, update with LUT
        self._obj._region_config_lut[0]['use_lut'] = 0
        self._obj._pts_lut[0]['status'] = 'N'
        self._obj._pts_lut[1]['status'] = 'N'
        self.post_dialog_responses('yes')
        self.set_server_response('Y')
        
        self._obj._skip_aisle()
        time.sleep(1) #added to ensure ODR had time to be received
        
        self.validate_prompts('Skip aisle, correct?')
        self.validate_server_requests(['prTaskODRPtsUpdateStatus','999','88','1','S'])
        self.assertEquals(self._obj._pts_lut[0]['status'], 'S')
        self.assertEquals(self._obj._pts_lut[1]['status'], 'S')
        self.assertEquals(self._obj._pts_lut[2]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[3]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[4]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[5]['status'], 'N')
        self.assertEquals(self._obj._pts_lut[6]['status'], 'N')
        self.assertEquals(self._obj.next_state, PTS_ASSIGNMENT_CHECK_NEXT_PUT)
 
   
if __name__ == '__main__':
    unittest.main()


