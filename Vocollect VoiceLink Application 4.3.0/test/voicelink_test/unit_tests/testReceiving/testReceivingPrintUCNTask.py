from BaseVLTestCase import BaseVLTestCase #Needs to be first import
from core.VoiceLink import VoiceLink
from receiving.ReceivingPrintUCNTask import ReceivingPrintUCNTask, \
    GET_PRINTER_LIST, ENTER_PRINTER, CONFIRM_PRINTER, XMIT_UCN_PRINT, CONFIRM_UCN,\
    SHOW_PRINTERS_HTML
from vocollect_core.utilities import obj_factory
from receiving.ReceivingTask import ReceivingTask


class testReceivingPrintUCNTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        vl = VoiceLink()
        self._obj = obj_factory.get(ReceivingPrintUCNTask, vl, 
                                      obj_factory.get(ReceivingTask, vl))
        self._obj.taskRunner._append(self._obj)
        self._obj.taskRunner._append(self._obj.callingTask)
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'receivingPrintUPC')

        #test states
        self.assertEquals(self._obj.states[0], GET_PRINTER_LIST)
        self.assertEquals(self._obj.states[1], ENTER_PRINTER)
        self.assertEquals(self._obj.states[2], CONFIRM_PRINTER)
        self.assertEquals(self._obj.states[3], XMIT_UCN_PRINT)
        self.assertEquals(self._obj.states[4], CONFIRM_UCN)
        
        #test LUTS defined correctly
        fields = self._obj._print_lut.fields
        self.assertEquals(fields['description'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        fields = self._obj._printer_list_lut.fields
        self.assertEquals(fields['description'].index, 0)
        self.assertEquals(fields['number'].index, 1)
        self.assertEquals(fields['location'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)
        
    def test_get_printer_list(self):
        self.start_server()
        
        self._obj.runState(GET_PRINTER_LIST)
        
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
    def test_enter_printer(self):
        self._obj._printer_list_lut.receive("Color dup Ricoh 2200,1,NW,0,\n\n")
        
        # No printer number has been previously entered
        self.post_dialog_responses('1!')
        
        self._obj.runState(ENTER_PRINTER)
        
        self.validate_prompts('Printer?')
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_PRINTERS_HTML)
        
        
    def test_xmit_ucn_print(self):
        self._obj.callingTask._current_item = {'ucn':'123'}
        self._obj.callingTask.printer_number = '1'
        
        self.start_server()
        
        self._obj.runState(XMIT_UCN_PRINT)
        
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
    def test_confirm_ucn(self):
        # Confirm as being the correct UCN
        self._obj.callingTask._current_item = {'ucn':'123'}
        self.post_dialog_responses('123!', 'yes')
        
        self._obj.runState(CONFIRM_UCN)
        
        self.validate_prompts('SKU?','123, correct?')
        self.assertEquals(self._obj.next_state, None)
        
        # Say a different SKU
        self.post_dialog_responses('12!', 'yes', 'no')
        
        self._obj.runState(CONFIRM_UCN)
        
        self.validate_prompts('SKU?','12, correct?', 'Wrong SKU. Would you like to re enter the SKU?', 'Skipping item.')
        self.assertEquals(self._obj.next_state, None)
        
        # Say a different SKU
        self.post_dialog_responses('12!', 'yes', 'yes')
        
        self._obj.runState(CONFIRM_UCN)
        
        self.validate_prompts('SKU?','12, correct?', 'Wrong SKU. Would you like to re enter the SKU?')
        self.assertEquals(self._obj.next_state, CONFIRM_UCN)
        
    def test_get_print_data_map(self):
        self._obj._printer_list_lut.receive("Color dup Ricoh 2200,1,NW,0,\n\n")
        
        map = self._obj.get_print_data_map(SHOW_PRINTERS_HTML)
        
        self.assertEquals(map['title'], self._obj.callingTask.title)
        self.assertEquals(map['operator'], self._obj.callingTask.operator_name)
        self.assertNotEqual(map['printers'], None)
        