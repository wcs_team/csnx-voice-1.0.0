from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from core.CoreTask import CoreTask
from core.VoiceLink import VoiceLink
from vocollect_core.utilities import obj_factory
from receiving.ReceivingTask import ReceivingTask, REGIONS, VALIDATE_REGIONS,\
    RECEIVING_GET_PO, RECEIVING_VALIDATE_PO, RECEIVING_GET_ITEM, RECEIVING_PRINT_UCN,\
    RECEIVING_GET_PALLET_ID, RECEIVING_GET_QUANTITY, RECEIVING_PALLET_COMPLETE,\
    SHOW_PO_HTML, SHOW_ITEM_DETAILS_HTML, SHOW_UNRECOG_ITEMS_HTML, DEFAULT_HTML
from common.RegionSelectionTasks import SingleRegionTask
from vocollect_core.task.task_runner import Launch

class testReceivingTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        self._obj = obj_factory.get(ReceivingTask, 
                                      temp.taskRunner, 
                                      temp)
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'receiving')

        #test states
        self.assertEquals(self._obj.states[0], REGIONS)
        self.assertEquals(self._obj.states[1], VALIDATE_REGIONS)
        self.assertEquals(self._obj.states[2], RECEIVING_GET_PO)
        self.assertEquals(self._obj.states[3], RECEIVING_VALIDATE_PO)
        self.assertEquals(self._obj.states[4], RECEIVING_GET_ITEM)
        self.assertEquals(self._obj.states[5], RECEIVING_PRINT_UCN)
        self.assertEquals(self._obj.states[6], RECEIVING_GET_PALLET_ID)
        self.assertEquals(self._obj.states[7], RECEIVING_GET_QUANTITY)
        self.assertEquals(self._obj.states[8], RECEIVING_PALLET_COMPLETE)

        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['print_label'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)
        
        fields = self._obj._valid_po_lut.fields
        self.assertEquals(fields['ucn'].index, 0)
        self.assertEquals(fields['upc'].index, 1)
        self.assertEquals(fields['description'].index, 2)
        self.assertEquals(fields['expected_quantity'].index, 3)
        self.assertEquals(fields['amount_received'].index, 4)
        self.assertEquals(fields['expiration_date'].index, 5)
        self.assertEquals(fields['lot_code'].index, 6)
        self.assertEquals(fields['rush_item'].index, 7)
        self.assertEquals(fields['po_description'].index, 8)
        self.assertEquals(fields['errorCode'].index, 9)
        self.assertEquals(fields['errorMessage'].index, 10)
        
        fields = self._obj._receive_item_lut.fields
        self.assertEquals(fields['description'].index, 0)
        self.assertEquals(fields['quantity_remaining'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, SingleRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskSingleRegion')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, VALIDATE_REGIONS)
    
    def test_validate_region(self):
        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj.runState(VALIDATE_REGIONS)

        self.validate_prompts('not authorized for any regions, see your supervisor')

        #-----------------------------------------------------------------
        #Test authorized regions
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self._obj._region_config_lut.receive('1,put to store region 1,1,0,0,\n\n')
        
        self._obj.runState(VALIDATE_REGIONS)
        
        self.validate_server_requests()
        self.validate_prompts()
        
    def test_get_purchase_order(self):
        self.post_dialog_responses('456!', 'yes')

        self._obj.runState(RECEIVING_GET_PO)
        
        self.validate_prompts('Purchase order?', '456, correct?')
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._purchase_orders, '456')
        
    def test_validate_purchase_order(self):
        self.start_server()
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n\n")
        
        self._obj.runState(RECEIVING_VALIDATE_PO)
        
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_PO_HTML)
        
    def test_get_item_number(self):
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n\n")
        
        # The item's SKU/UCN is identified
        self.post_dialog_responses('123!', 'yes')
        
        self._obj.runState(RECEIVING_GET_ITEM)
        
        self.validate_prompts('SKU?', '123, correct?')
        self.assertEquals(self._obj.next_state, RECEIVING_GET_PALLET_ID)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_ITEM_DETAILS_HTML)
    
        # The item's SKU/UCN is not identified. The UPC is identified.
        self.post_dialog_responses('22!', 'yes', '321!', 'yes')
        
        self._obj.runState(RECEIVING_GET_ITEM)
        
        self.validate_prompts('SKU?', '22, correct?', 'SKU not found. Scan or speak UPC.','321, correct?')
        self.assertEquals(self._obj.next_state, RECEIVING_GET_PALLET_ID)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_ITEM_DETAILS_HTML)
        
        # The item's SKU/UCN and UPC are not identified.
        self.post_dialog_responses('22!', 'yes', '11!', 'yes', '1!')
        
        self._obj.runState(RECEIVING_GET_ITEM)
        
        self.validate_prompts('SKU?', '22, correct?','SKU not found. Scan or speak UPC.','11, correct?','Choose the item from the list')
        self.assertEquals(self._obj.next_state, RECEIVING_PRINT_UCN)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_UNRECOG_ITEMS_HTML)
        
    def test_get_quantity(self):
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n" + 
                                        "124,421,Charmin toilet paper,12,0,,,0,Paper products,0,\n" + 
                                        "125,521,Charmin tissues,6,0,,,0,Paper products,0,\n\n")
        
        #Get all of the items
        self._obj._current_item = self._obj._valid_po_lut[0]
        self.post_dialog_responses('10!')
        
        self._obj.runState(RECEIVING_GET_QUANTITY)
        
        self.validate_prompts('Quantity?', 'There are 0 remaining')
        self.assertEquals(self._obj._current_item['amount_received'], 10)
        
        #Get some of the items
        self._obj._current_item = self._obj._valid_po_lut[1]
        self.post_dialog_responses('2!')
        
        self._obj.runState(RECEIVING_GET_QUANTITY)
        
        self.validate_prompts('Quantity?', 'There are 10 remaining')
        self.assertEquals(self._obj._current_item['amount_received'], 2)
        
        #Get more items than are expected.
        self._obj._current_item = self._obj._valid_po_lut[2]
        self.post_dialog_responses('8!','yes','12345!')
        
        self._obj.runState(RECEIVING_GET_QUANTITY)
        
        self.validate_prompts('Quantity?', 'You said 8, only 6 remaining. Authorize?',\
                              'Please have your supervisor authorize this quantity', \
                              'Quantity 8 approved')
        self.assertEquals(self._obj._current_item['amount_received'], 8)
        
    def test_get_pallet_id(self):
        # Any pallet is accepted so there's really only a happy path to test
        self.post_dialog_responses('22!','yes')
        
        self._obj.runState(RECEIVING_GET_PALLET_ID)
        
        self.validate_prompts('LPN?', '22, correct?')
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_ITEM_DETAILS_HTML)   
        
    def test_pallet_complete(self):
        self.start_server()
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n\n")
        self._obj._current_item = self._obj._valid_po_lut[0]
        self._obj._current_item['amount_received'] = 2
        self._obj._current_item['expected_quantity'] = 10
        
        self._obj._pallet_id = '22'
        self._obj._qty_received = '6'
        
        # Complete a pallet with items left to receive
        self._obj.runState(RECEIVING_PALLET_COMPLETE)
        
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, RECEIVING_GET_ITEM)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_PO_HTML)
        
        # Complete a pallet with no items left to receive
        self._obj._current_item['amount_received'] = 2
        self._obj._current_item['expected_quantity'] = 2
        
        self.post_dialog_responses('yes')
        
        self._obj.runState(RECEIVING_PALLET_COMPLETE)
        
        self.validate_prompts('PO is complete. Correct?')
        self.assertEquals(self._obj.next_state, RECEIVING_GET_PO)
        self.assertEquals(self._obj.page, self._obj.name + "/" + DEFAULT_HTML)
        
        # Don't complete the pallet even with no items left to receive
        self._obj._current_item['amount_received'] = 2
        self._obj._current_item['expected_quantity'] = 2
        
        self.post_dialog_responses('no')
        
        self._obj.runState(RECEIVING_PALLET_COMPLETE)
        
        self.validate_prompts('PO is complete. Correct?')
        self.assertEquals(self._obj.next_state, RECEIVING_GET_ITEM)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_PO_HTML)
        
    def test_have_all_items_been_received(self):
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n\n")
        self.assertFalse(self._obj.have_all_items_been_received())

        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,0,0,,,0,Paper products,0,\n\n")
        self.assertTrue(self._obj.have_all_items_been_received())
        
    def test_any_rush_items(self):
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n\n")
        self.assertFalse(self._obj.any_rush_items())

        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,0,0,,,1,Paper products,0,\n\n")
        self.assertTrue(self._obj.any_rush_items())
        
    def test_find_item_by_attribute(self):
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n" + 
                                        "124,421,Charmin toilet paper,12,0,,,0,Paper products,0,\n\n")
        self.assertEquals(self._obj.find_item_by_attribute('ucn','123')['ucn'], '123')
        self.assertEquals(self._obj.find_item_by_attribute('upc','421')['upc'], '421')

        self.assertEquals(self._obj.find_item_by_attribute('ucn','129'), None)
        self.assertEquals(self._obj.find_item_by_attribute('upc','429'), None)     

        
    def test_get_data_map(self):
        self._obj._valid_po_lut.receive("123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n\n")
        self._obj._purchase_orders = '200'
        self._obj._current_item = self._obj._valid_po_lut[0]
        
        # Show PO page
        map = self._obj.get_data_map(SHOW_PO_HTML)
        self.assertEquals(map['title'],self._obj.title)
        self.assertEquals(map['operator'],self._obj.operator_name)
        self.assertNotEqual(map['po'], None)
        self.assertEquals(map['po']['number'], '200')
        self.assertNotEqual(map['po']['items'], None)
        
        
        # Show item details
        map = self._obj.get_data_map(SHOW_ITEM_DETAILS_HTML)
        self.assertEquals(map['title'],self._obj.title)
        self.assertEquals(map['operator'],self._obj.operator_name)
        self.assertNotEqual(map['current_item'], None)
        self.assertEquals(map['current_item']['ucn'], '123')
        
        # Show unrecognized items
        map = self._obj.get_data_map(SHOW_UNRECOG_ITEMS_HTML)
        self.assertEquals(map['title'],self._obj.title)
        self.assertEquals(map['operator'],self._obj.operator_name)
        self.assertNotEqual(map['items'], None)
        
        