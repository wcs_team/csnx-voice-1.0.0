from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from loading.LoadingPrintReportTask import LoadingPrintReportTask
from loading.LoadingTask import LoadingTask
from loading.LoadingPrintReportTask import ENTER_PRINTER, CONFIRM_PRINTER
from loading.LoadingPrintReportTask import XMIT_PRINT_REPORT, CONFIRM_REPORT
import unittest


class testLoadingPrintReportTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        vl = VoiceLink()
        self.start_server()
        lt = obj_factory.get(LoadingTask, VoiceLink())
        lt._region_config_lut.receive('1,loading region 1,0,5,3,3,0,\n\n')
        self._obj = obj_factory.get(LoadingPrintReportTask, lt._region_config_lut, vl, 
                                      obj_factory.get(LoadingTask, vl))
        self._obj.taskRunner._append(self._obj)
        self._obj.taskRunner._append(self._obj.callingTask)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'loadingPrintReport')

        #test states
        self.assertEquals(self._obj.states[0], ENTER_PRINTER)
        self.assertEquals(self._obj.states[1], CONFIRM_PRINTER)
        self.assertEquals(self._obj.states[2], XMIT_PRINT_REPORT)
        self.assertEquals(self._obj.states[3], CONFIRM_REPORT)
        
        #test LUTS defined correctly
        fields = self._obj._print_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)
        
    def test_enter_print(self):
        if hasattr(self._obj.callingTask, 'printer_number'):
            del self._obj.calling.Task.printer_numer
        
        #----------------------------------------------------
        #Test currently no printer
        self.post_dialog_responses('12')
        
        self._obj.runState(ENTER_PRINTER)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Printer?')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '12')
        
        #----------------------------------------------------
        #Test currently printer set to none
        self._obj.callingTask.printer_number = None
        self.post_dialog_responses('23')
        
        self._obj.runState(ENTER_PRINTER)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Printer?')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '23')
         
        #----------------------------------------------------
        #Test currently printer is set 
        self._obj.runState(ENTER_PRINTER)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '23')
         
    def test_confirm_printer(self):

        #----------------------------------------------------
        #confirm printer no
        self._obj.callingTask.printer_number = '11'
        self.post_dialog_responses('no')
        
        self._obj.runState(CONFIRM_PRINTER)
        
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        self.validate_prompts('printer <spell>11</spell>, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, None)
         
        #----------------------------------------------------
        #confirm printer yes
        self._obj.next_state = None
        self._obj.callingTask.printer_number = '11'
        self.post_dialog_responses('yes')
        
        self._obj.runState(CONFIRM_PRINTER)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('printer <spell>11</spell>, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '11')
         
    def test_xmit_print_report(self):
        self._obj.callingTask._request_route_lut.receive('1,123,1,789,5,13,route has 13 containers,0,\n\n')
        
        #----------------------------------------------------
        #TESTLINK 95613 :: check timeout
        self.stop_server()
        self._obj.callingTask.printer_number = '11'
        self.post_dialog_responses('ready')
        
        self._obj.runState(XMIT_PRINT_REPORT)
        
        self.assertEquals(self._obj.next_state, XMIT_PRINT_REPORT)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '11')
        
        #----------------------------------------------------
        #check success
        self.start_server()
        self._obj.next_state = None
        self._obj.callingTask.printer_number = '12'
        self.set_server_response('0,\n\n')
        
        self._obj.runState(XMIT_PRINT_REPORT)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTLoadingPrint', '1', '12'])
        self.assertEquals(self._obj.callingTask.printer_number, '12')
        
        #----------------------------------------------------
        #TESTLINK 95615 :: check error
        self._obj.callingTask.printer_number = '11'
        self.set_server_response('2,Error Message\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(XMIT_PRINT_REPORT)
        
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTLoadingPrint', '1', '11'])
        self.assertEquals(self._obj.callingTask.printer_number, '11')
        
    def test_confirm_report(self):
        self._obj.callingTask._request_route_lut.receive('1,123,1,789,5,13,route has 13 containers,0,\n\n')

        #----------------------------------------------------
        #wrong load, load number
        self._obj.callingTask.printer_number = '11'
        self.post_dialog_responses('456!', 'route number', 'ready')
        
        self._obj.runState(CONFIRM_REPORT)
        
        self.assertEquals(self._obj.next_state, CONFIRM_REPORT)
        self.validate_prompts('Route?',
                              'wrong 456, to try again say ready',
                              'Route 123',
                              'wrong 456, to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '11')
         
        #----------------------------------------------------
        #reprint report
        self._obj.callingTask.printer_number = '11'
        self.post_dialog_responses('reprint report')
        
        self._obj.runState(CONFIRM_REPORT)
        
        self.assertEquals(self._obj.next_state, ENTER_PRINTER)
        self.validate_prompts('Route?')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '11')
         
        #----------------------------------------------------
        #reprint report
        self._obj.next_state = None
        self._obj.callingTask.printer_number = '11'
        self.post_dialog_responses('123ready')
        
        self._obj.runState(CONFIRM_REPORT)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Route?')
        self.validate_server_requests()
        self.assertEquals(self._obj.callingTask.printer_number, '11')
         
if __name__ == '__main__':
    unittest.main()


