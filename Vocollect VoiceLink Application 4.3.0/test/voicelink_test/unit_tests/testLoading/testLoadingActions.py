from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from common import VoiceLinkLut
from core.VoiceLink import VoiceLink
from loading.LoadingTask import LoadingTask
from loading.LoadingActions import LoadContainerTask, UnloadContainerTask, ConsolidateContainerTask
from loading.LoadingActions import CONTAINER_LOAD, UNLOAD_CONTAINER, CONSOLIDATE_MASTER
from loading.LoadingActions import CONSOLIDATE_CONSOLIDATE
import unittest
from loading.SharedConstants import GET_CONTAINER



class testLoadingActionsTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        VoiceLinkLut.transport._timeout = 1
    
    def test_LoadContainerTask_initialize_states(self):
        vl = VoiceLink()
        obj = obj_factory.get(LoadContainerTask, '11111', True, '999', vl, LoadingTask(vl))
        
        #test name
        self.assertEquals(obj.name, 'loadContainer')

        self.assertEquals(obj.states[0], CONTAINER_LOAD)
        
        #test LUTS defined correctly
        fields = obj._route_lut.fields
        self.assertEquals(fields['route_complete'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        
    def test_LoadContainerTask_load_container(self):
        self.start_server()
        vl = VoiceLink()
        obj = obj_factory.get(LoadContainerTask, '11111', True, '999', vl, LoadingTask(vl))
        
        #-----------------------------------------------------------------
        #test capture position, not complete
        self.set_server_response('0,0,\n\n')
        self.post_dialog_responses('11!', 'yes')

        obj.runState(CONTAINER_LOAD)
        
        self.assertEquals(obj.next_state, None)
        self.validate_prompts('Position?',
                              '11, correct?')
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','L','11111','','11','999'])
        self.assertFalse(obj.callingTask.route_complete)
        
        #-----------------------------------------------------------------
        #RALLYTC: 1065 - test complete
        #RALLYTC: 1066 - test do not capture position
        obj.get_position = False
        self.set_server_response('1,0,\n\n')

        obj.runState(CONTAINER_LOAD)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','L','11111','','','999'])
        self.assertFalse(obj.callingTask.route_complete)
        
        
        #-----------------------------------------------------------------
        #test error
        obj.get_position = False
        obj.callingTask.route_complete = False
        self.set_server_response('1,1,Error message\n\n')
        self.post_dialog_responses('ready')

        obj.runState(CONTAINER_LOAD)
        
        self.assertEquals(obj.next_state, GET_CONTAINER)
        self.validate_prompts('Error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','L','11111','','','999'])
        self.assertFalse(obj.callingTask.route_complete)
        
    def test_UnLoadContainerTask_initialize_states(self):
        lt = LoadingTask(VoiceLink())
        obj = obj_factory.get(UnloadContainerTask, lt._containers_lut, lt._region_config_lut, lt.taskRunner, lt)
        
        #test name
        self.assertEquals(obj.name, 'unloadContainer')

        self.assertEquals(obj.states[0], UNLOAD_CONTAINER)
        
        #test LUTS defined correctly
        fields = obj._route_lut.fields
        self.assertEquals(fields['route_complete'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        
    def test_UnLoadContainerTask_load_container(self):
        self.start_server()
        lt = obj_factory.get(LoadingTask, VoiceLink())
        lt._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        
        obj = obj_factory.get(UnloadContainerTask, lt._containers_lut, lt._region_config_lut, lt.taskRunner, lt)
        
        #-----------------------------------------------------------------
        #test cancel
        self.post_dialog_responses('cancel')

        obj.runState(UNLOAD_CONTAINER)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to unload?')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test not loaded
        self.set_server_response('1,A11111,11111,N,1,0,\n\n' +
                                       '1,A22222,22222,L,1,0,\n\n' +
                                       '1,A33333,33333,L,1,0,\n\n')
        
        self.post_dialog_responses('11111')

        obj.runState(UNLOAD_CONTAINER)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to unload?')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','11111','1'], ['prTaskLUTLoadingUpdateContainer', 'N', 'A11111', '', '', ''])
        self.assertFalse(obj.callingTask.route_complete)
        
        #-----------------------------------------------------------------
        #test loaded
        self.set_server_response('0,0,\n\n')
        self.post_dialog_responses('22222')

        obj.runState(UNLOAD_CONTAINER)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to unload?')
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','N','A22222','','',''])
        self.assertFalse(obj.callingTask.route_complete)
        self.assertEquals(lt._containers_lut[1]['status'], 'N')
        
        #-----------------------------------------------------------------
        #test loaded
        self.set_server_response('0,1,Error message\n\n')
        self.post_dialog_responses('33333', 'ready')

        obj.runState(UNLOAD_CONTAINER)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to unload?',
                              'Error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','N','A33333','','',''])
        self.assertFalse(obj.callingTask.route_complete)
        self.assertEquals(lt._containers_lut[2]['status'], 'L')
        
        #-----------------------------------------------------------------
        #test invalid container loaded
        self.set_server_response('1,A11111,11111,N,1,0,\n\n' +
                                       '1,A22222,22222,L,1,0,\n\n' +
                                       '1,A33333,33333,L,1,0,\n\n')
        self.post_dialog_responses('44444', 'ready')

        obj.runState(UNLOAD_CONTAINER)
        
        self.assertEquals(obj.next_state, UNLOAD_CONTAINER)
        self.validate_prompts('Container to unload?',
                              'Container <spell>44444</spell> not found, to try again say ready')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','44444','1'])
        
        #-----------------------------------------------------------------
        #test loaded route complete
        obj.next_state = None
        self.set_server_response('1,0,\n\n')
        self.post_dialog_responses('33333')

        obj.runState(UNLOAD_CONTAINER)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to unload?')
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','N','A33333','','',''])
        self.assertFalse(obj.callingTask.route_complete)
        self.assertEquals(lt._containers_lut[2]['status'], 'N')
        

    def test_consolidate_initialize_states(self):
        lt = obj_factory.get(LoadingTask, VoiceLink())
        obj = obj_factory.get(ConsolidateContainerTask, lt._containers_lut, lt._region_config_lut, lt.taskRunner, lt)
        
        #test name
        self.assertEquals(obj.name, 'consolidateContainer')

        self.assertEquals(obj.states[0], CONSOLIDATE_MASTER)
        self.assertEquals(obj.states[1], CONSOLIDATE_CONSOLIDATE)
        
        #test LUTS defined correctly
        fields = obj._route_lut.fields
        self.assertEquals(fields['route_complete'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        
    def test_consolidate_get_master_container(self):
        self.start_server()
        lt = obj_factory.get(LoadingTask, VoiceLink())
        lt._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        
        obj = obj_factory.get(ConsolidateContainerTask, lt._containers_lut, lt._region_config_lut, lt.taskRunner, lt)
        
        #-----------------------------------------------------------------
        #test no more
        self.post_dialog_responses('no more')

        obj.runState(CONSOLIDATE_MASTER)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Main Container?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #test not allowed to consolidate
        self.set_server_response('1,A11111,11111,L,1,0,\n\n' +
                                       '1,A22222,22222,N,1,0,\n\n' +
                                       '1,A33333,33333,N,1,0,\n\n' +
                                       '1,A44444,44444,N,1,0,\n\n' +
                                       '1,A55555,55555,N,1,0,\n\n' +
                                       '1,A66666,66666,N,1,0,\n\n' +
                                       '1,A77777,77777,N,1,0,\n\n')
        
        self.post_dialog_responses('11111')

        obj.runState(CONSOLIDATE_MASTER)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Main Container?')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','11111','1'])
        self.assertFalse(obj.callingTask.route_complete)
        
        #-----------------------------------------------------------------
        #test main container not found
        obj.next_state = None
        self.set_server_response('1,A88888,88888,L,1,0,\n\n')
        self.post_dialog_responses('99999', 'ready')

        obj.runState(CONSOLIDATE_MASTER)
        
        self.assertEquals(obj.next_state, CONSOLIDATE_MASTER)
        self.validate_prompts('Main Container?',
                              'Container <spell>99999</spell> not found, to try again say ready')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','99999','1'])
        self.assertFalse(obj.callingTask.route_complete)
        self.assertEquals(obj.master_container, 'A11111')

        #-----------------------------------------------------------------
        #test allowed to consolidate
        obj.next_state = None
        obj.consolidate_count = 99
        
        self.post_dialog_responses('22222')

        obj.runState(CONSOLIDATE_MASTER)
        
        self.assertEquals(obj.next_state, None)
        self.validate_prompts('Main Container?')
        self.validate_server_requests()
        self.assertFalse(obj.callingTask.route_complete)
        self.assertEquals(obj.master_container, 'A22222')
        self.assertEquals(obj.consolidate_count, 1)
        
    def test_consolidate_get_consolidate_container(self):
        lt = obj_factory.get(LoadingTask, VoiceLink())
        lt._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self.start_server()
        
        obj = obj_factory.get(ConsolidateContainerTask, lt._containers_lut, lt._region_config_lut, lt.taskRunner, lt)
        obj.master_container = 'A77777'

        #-----------------------------------------------------------------
        #test no more
        self.post_dialog_responses('no more')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to consolidate?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #test container not not loaded
        self.set_server_response('1,A11111,11111,L,1,0,\n\n' +
                                       '1,A22222,22222,N,1,0,\n\n' +
                                       '1,A33333,33333,N,1,0,\n\n' +
                                       '1,A44444,44444,N,1,0,\n\n' +
                                       '1,A55555,55555,N,1,0,\n\n' +
                                       '1,A66666,66666,N,1,0,\n\n' +
                                       '1,A77777,77777,N,1,0,\n\n')
        
        self.post_dialog_responses('11111', 'yes')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, CONSOLIDATE_CONSOLIDATE)
        self.validate_prompts('Container to consolidate?',
                              'Consolidate 11111 into 77777')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','11111','1'], ['prTaskLUTLoadingUpdateContainer', 'C', 'A11111', 'A77777', '', ''])
        self.assertEquals(obj.consolidate_count, 2)

        #-----------------------------------------------------------------
        #test container is master
        self.post_dialog_responses('77777')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, CONSOLIDATE_CONSOLIDATE)
        self.validate_prompts('Container to consolidate?',
                              'Container cannot be consolidated')
        self.assertEquals(obj.consolidate_count, 2)
        
        #-----------------------------------------------------------------
        #test container not found
        self.set_server_response('1,A88888,88888,L,1,0,\n\n')
        self.post_dialog_responses('99999', 'ready')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, CONSOLIDATE_CONSOLIDATE)
        self.validate_prompts('Container to consolidate?',
                              'Container <spell>99999</spell> not found, to try again say ready')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','99999','1'])
        self.assertEquals(obj.consolidate_count, 2)
        
        #-----------------------------------------------------------------
        #test respond No
        self.post_dialog_responses('22222', 'no')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, CONSOLIDATE_CONSOLIDATE)
        self.validate_prompts('Container to consolidate?',
                              'Consolidate 22222 into 77777')
        self.validate_server_requests()
        self.assertEquals(obj.consolidate_count, 2)
        self.assertEquals(lt._containers_lut.containers['A22222']['status'], 'N')
        
        #-----------------------------------------------------------------
        #test respond Yes
        self.set_server_response('0,0,\n\n')
        self.post_dialog_responses('22222', 'yes')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to consolidate?',
                              'Consolidate 22222 into 77777')
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','C','A22222','A77777','',''])
        self.assertEquals(obj.consolidate_count, 3)
        self.assertEquals(lt._containers_lut.containers['A22222']['status'], 'C')

        #-----------------------------------------------------------------
        #test respond Yes, last allowed
        self.set_server_response('0,0,\n\n')
        self.post_dialog_responses('33333', 'yes')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to consolidate?',
                              'Consolidate 33333 into 77777')
        self.validate_server_requests(['prTaskLUTLoadingUpdateContainer','C','A33333','A77777','',''])
        self.assertEquals(obj.consolidate_count, 4)
        self.assertEquals(lt._containers_lut.containers['A33333']['status'], 'C')

    def test_consolidate_route_conplete(self):
        lt = LoadingTask(VoiceLink())
        lt._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self.start_server()
        
        obj = ConsolidateContainerTask(lt._containers_lut, lt._region_config_lut, lt.taskRunner, lt)
        obj.master_container = 'A77777'

        #-----------------------------------------------------------------
        #test respond Yes
        self.set_server_response('1,A11111,11111,L,1,0,\n\n' +
                                       '1,A22222,22222,N,1,0,\n\n' +
                                       '1,A33333,33333,N,1,0,\n\n' +
                                       '1,A44444,44444,N,1,0,\n\n' +
                                       '1,A55555,55555,N,1,0,\n\n' +
                                       '1,A66666,66666,N,1,0,\n\n' +
                                       '1,A77777,77777,N,1,0,\n\n')
        
        self.set_server_response('1,0,\n\n')
        self.post_dialog_responses('22222', 'yes')

        obj.runState(CONSOLIDATE_CONSOLIDATE)
        
        self.assertEquals(obj.next_state, '')
        self.validate_prompts('Container to consolidate?',
                              'Consolidate 22222 into 77777')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','22222','1'],
                                      ['prTaskLUTLoadingUpdateContainer','C','A22222','A77777','',''])
        self.assertEquals(obj.consolidate_count, 2)
        self.assertEquals(lt._containers_lut.containers['A22222']['status'], 'C')
        self.assertFalse(obj.callingTask.route_complete)

if __name__ == '__main__':
    unittest.main()


