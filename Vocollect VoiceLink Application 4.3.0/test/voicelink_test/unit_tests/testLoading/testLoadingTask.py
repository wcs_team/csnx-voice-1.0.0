from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from common import VoiceLinkLut
from common.RegionSelectionTasks import MultipleRegionTask
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
from loading.LoadingActions import ConsolidateContainerTask, UnloadContainerTask
from loading.LoadingActions import LoadContainerTask
from loading.LoadingTask import LoadingTask
from loading.LoadingTask import REGIONS, CHECK_AUTHORIZED_REGIONS, REQUEST_ROUTE, PRINT_ROUTE
from loading.LoadingTask import INITIALIZE_ROUTE, TRAILER_ID, GET_CONTAINER, LOAD
from loading.LoadingTask import ROUTE_COMPLETE_PRINT, CHECK_COMPLETE
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
import unittest


class testLoadingTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1

        VoiceLinkLut.transport._timeout = 1
        self._obj = obj_factory.get(LoadingTask, temp.taskRunner)
        self._obj.taskRunner._append(self._obj)
        TaskRunnerBase._main_runner = self._obj.taskRunner
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'loading')

        #test states
        self.assertEquals(self._obj.states[0], REGIONS)
        self.assertEquals(self._obj.states[1], CHECK_AUTHORIZED_REGIONS)
        self.assertEquals(self._obj.states[2], REQUEST_ROUTE)
        self.assertEquals(self._obj.states[3], PRINT_ROUTE)
        self.assertEquals(self._obj.states[4], INITIALIZE_ROUTE)
        self.assertEquals(self._obj.states[5], TRAILER_ID)
        self.assertEquals(self._obj.states[6], GET_CONTAINER)
        self.assertEquals(self._obj.states[7], LOAD)
        self.assertEquals(self._obj.states[8], ROUTE_COMPLETE_PRINT)
        self.assertEquals(self._obj.states[9], CHECK_COMPLETE)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._request_reqion_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['route_issuance'].index, 2)
        self.assertEquals(fields['container_digits_oper_speaks'].index, 3)
        self.assertEquals(fields['trailer_id_digits'].index, 4)
        self.assertEquals(fields['max_consolidation'].index, 5)
        self.assertEquals(fields['errorCode'].index, 6)
        self.assertEquals(fields['errorMessage'].index, 7)
        
        fields = self._obj._request_route_lut.fields
        self.assertEquals(fields['route_id'].index, 0)
        self.assertEquals(fields['route_number'].index, 1)
        self.assertEquals(fields['capture_trailer_id'].index, 2)
        self.assertEquals(fields['trailer_id'].index, 3)
        self.assertEquals(fields['door_number'].index, 4)
        self.assertEquals(fields['door_check_digit'].index, 5)
        self.assertEquals(fields['summary_prompt'].index, 6)

        fields = self._obj._complete_route_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, MultipleRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskMultiRegions')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, CHECK_AUTHORIZED_REGIONS)
        
    def test_check_authorized(self):

        #-----------------------------------------------------------------
        #RALLYTC: 980-Verify that selector is notified if no valid regions are returned 
        #in response to the Valid Loading Regions LUT
        #RALLYTC: 985-Verify that the operator is notified if an un-authorized 
        #region number is entered at the region prompt.
        #Test no authorized regions
        self._obj.runState(CHECK_AUTHORIZED_REGIONS)
        
        self.validate_prompts('not authorized for any regions, see your supervisor')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '') 
        self.assertEquals(self._obj.region_selected, False) 
        
        #-----------------------------------------------------------------
        #RALLYTC: 1005-Verify that if only one valid region is returned in response
        #to the Valid Loading Regions LUT, the selector is not prompted to enter a region.
        #Test authorized regions
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(CHECK_AUTHORIZED_REGIONS)
        
        self.assertEquals(self._obj.next_state, '')
        self.validate_prompts('To start loading, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.region_selected, True) 

        self.start_server()
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        #-----------------------------------------------------------------
        #Test change function, no
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(CHECK_AUTHORIZED_REGIONS)
        
        self.assertEquals(self._obj.next_state, CHECK_AUTHORIZED_REGIONS)
        self.validate_prompts('To start loading, say ready', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        #Test change function, yes
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, CHECK_AUTHORIZED_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('To start loading, say ready', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        #Test change region, no
        self._obj.next_state = None
        self._obj._region_selected = True
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(CHECK_AUTHORIZED_REGIONS)
        
        self.assertEquals(self._obj.next_state, CHECK_AUTHORIZED_REGIONS)
        self.validate_prompts('To start loading, say ready', 
                              'Change region, correct?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test change region, yes
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, CHECK_AUTHORIZED_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('To start loading, say ready', 
                              'Change region, correct?')
        self.validate_server_requests()

    def test_request_route(self):
        self.start_server()
         
        #-----------------------------------------------------------------
        #RALLYTC: 1018 - Verify that if a loading region specifies manual issuance, 
        #the operator is prompted to enter a route number.
        #Test region with no auto issuance
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self.post_dialog_responses('10ready','yes')
        self.post_dialog_responses()
        self.set_server_response('1,10,1,789,5,13,route 10 has 5 stops 5 remaining,0,\n\n', 'prTaskLUTLoadingRequestRoute')
        
        self._obj.runState(REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Route?','10, correct?')
        self.validate_server_requests(['prTaskLUTLoadingRequestRoute', '10'])
         
        #-----------------------------------------------------------------
        #Test region with auto issuance
        #RALLYTC: 1019 - Verify that if a loading region specifies automatic issuance, 
        #the operator is not prompted to enter a route number.
        self._obj._region_config_lut.receive('1,loading region 1,0,5,3,3,0,\n\n')
        self.set_server_response('0,,0,\n\n', 'prTaskLUTLoadingRequestRoute')
        
        self._obj.runState(REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTLoadingRequestRoute', ''])
        
        #------------------------------------------------------------------
        #Test connection error      
        self._obj._region_config_lut.receive('1,loading region 1,0,5,3,3,0,\n\n')
        self.set_server_response('0,,0,\n')
        self.set_server_response('0,,0,\n\n')
        
        self._obj.runState(REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTLoadingRequestRoute', ''])
        
        #------------------------------------------------------------------
        #Validate error code
        #RALLYTC: 1021 - Verify that the Request Route LUT is re-sent if a timeout occurs.
        self._obj._region_config_lut.receive('1,loading region 1,0,5,3,3,0,\n\n')
        self.set_server_response('0,1,1,789,2,2,0,2,Error Message\n\n', 'prTaskLUTLoadingRequestRoute')
        self.post_dialog_responses('ready')
        
        self._obj.runState(REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, REQUEST_ROUTE)
        self.validate_server_requests(['prTaskLUTLoadingRequestRoute', ''])     
        self.validate_prompts('Error Message, To continue say ready')

        #-----------------------------------------------------------------
        #Test change function, no
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, REQUEST_ROUTE)
        self.validate_prompts('Route?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        #Test change function, yes
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Route?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

         
        #-----------------------------------------------------------------
        #Test change region, no
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj.region_selected = True
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, REQUEST_ROUTE)
        self.validate_prompts('Route?', 
                              'Change region, correct?')
        self.validate_server_requests()

        #-----------------------------------------------------------------
        #Test change region, yes
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, REQUEST_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Route?', 
                              'Change region, correct?')
        self.validate_server_requests()

    def test_initialize_route(self):
        #-------------------------------------------------------------------------------
        #RALLYTC: 1046 Verify that the summary prompt is spoken if it exists.
        #RALLYTC: 1049 Verify that the operator is prompted for the trailer ID.
        #Test Summary Prompt exists, check digits required and capture trailer id is on.
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj._request_route_lut.receive('1,123,1,789,5,13,route 123 has 5 stops 5 remaining,0,\n\n')

        self.post_dialog_responses('ready', '13!')
        
        self._obj.runState(INITIALIZE_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('route 123 has 5 stops 5 remaining, To continue say ready',
                              'Door 5')
                
        self.validate_server_requests()
        
        #self.assertEquals(self._obj.trailer_id, '789')
        
        #------------------------------------------------------------------
        #RALLYTC: 1047 Verify that summary prompt is not spoken if not provided.
        #Test No Summary Prompt exists, check digits required and capture trailer id is on.        
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj._request_route_lut.receive('1,123,1,789,5,13,,0,\n\n')

        self.post_dialog_responses('13!')
        
        self._obj.runState(INITIALIZE_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Door 5')
                
        self.validate_server_requests()
        
        #self.assertEquals(self._obj.trailer_id, '789')
        
        #-------------------------------------------------------------------------------
        #RALLYTC: 1048 Verify that operator must speak ready if no check digits are provided.        
        #Test Summary Prompt exists,  no check digits required and capture trailer id is on.        
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj._request_route_lut.receive('1,123,1,789,5,,route 123 has 5 stops 5 remaining,0,\n\n')

        self.post_dialog_responses('ready', 'ready')
        
        self._obj.runState(INITIALIZE_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('route 123 has 5 stops 5 remaining, To continue say ready',
                              'Door 5')
                
        self.validate_server_requests()
        
        #self.assertEquals(self._obj.trailer_id, '789')  
              
        #------------------------------------------------------------------
        #RALLYTC: 1050 Operator not prompted for the trailer ID if flag is 0.
        #Test No Summary Prompt exists, check digits required and capture trailer id is off.        
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj._request_route_lut.receive('1,123,0,,5,13,,0,\n\n')

        self.post_dialog_responses('13')
        
        self._obj.runState(INITIALIZE_ROUTE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Door 5')
                
        self.validate_server_requests()
    
        self.assertEquals(self._obj.trailer_id, '')
        
    def test_container_lut(self):
        self.start_server()
        
        #--------------------------------------------------------------------
        # Queue up lut data
        self.set_server_response('1,A12345,12345,N,1,0,\n\n' +
                                       '1,B12345,12345,N,1,0,\n\n' + 
                                       '1,A11111,11111,N,1,0,\n\n' +
                                       '1,A22222,22222,N,1,0,\n\n')
            
        self.set_server_response('1,C12345,12345,N,1,0,\n\n' +
                                       '1,A11111,11111,L,1,0,\n\n' +
                                       '1,A33333,33333,N,1,0,\n\n' +
                                       '1,A44444,44444,N,1,0,\n\n')

        self.set_server_response('1,A55555,55555,N,1,0,\n\n')

        
        #--------------------------------------------------------------------
        #RALLYTC: 1056 - verify lut sent if container does not exist
        #verify getting first container does 1 transmit
        #
        record = self._obj._containers_lut.get_container_record('11111', False)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer', '11111','1'])
        self.assertEquals('A11111', record['number'])
        self.assertEquals('N', record['status'])
        self.assertEquals(4, len(list(self._obj._containers_lut.containers.keys())))
        
        #--------------------------------------------------------------------
        #RALLYTC: 1057 - verify not sent if already exists
        #verify getting second container does not transmit
        record = self._obj._containers_lut.get_container_record('22222', False)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals('A22222', record['number'])
        self.assertEquals(4, len(list(self._obj._containers_lut.containers.keys())))
        
        #--------------------------------------------------------------------
        # verify getting by scanned value
        record = self._obj._containers_lut.get_container_record('A12345', True)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals('A12345', record['number'])
        self.assertEquals(4, len(list(self._obj._containers_lut.containers.keys())))
        
        #--------------------------------------------------------------------
        #RALLYTC: 1061 - verify pick from list on duplicates
        #verify getting duplicate
        self.post_dialog_responses('yes')

        record = self._obj._containers_lut.get_container_record('12345', False)
        
        self.validate_prompts('Container <spell>A12345</spell>?')
        self.validate_server_requests()
        self.assertEquals('A12345', record['number'])
        self.assertEquals(4, len(list(self._obj._containers_lut.containers.keys())))
        
        self.post_dialog_responses('no')
        self.post_dialog_responses('yes')

        record = self._obj._containers_lut.get_container_record('12345', False)
        
        self.validate_prompts('Container <spell>A12345</spell>?',
                              'Container <spell>B12345</spell>?')
        self.validate_server_requests()
        self.assertEquals('B12345', record['number'])
        self.assertEquals(4, len(list(self._obj._containers_lut.containers.keys())))
        
        #--------------------------------------------------------------------
        #RALLYTC: 1058 - verify lut sent if container does not exist
        #verify not found
        self.post_dialog_responses('no')
        self.post_dialog_responses('no')
        self.post_dialog_responses('no')
        self.post_dialog_responses('ready')

        record = self._obj._containers_lut.get_container_record('12345', False)
        
        self.validate_prompts('Container <spell>A12345</spell>?',
                              'Container <spell>B12345</spell>?',
                              'Container <spell>C12345</spell>?',
                              'Container <spell>12345</spell> not found, to try again say ready')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','12345','1'])
        self.assertEquals(None, record)
        self.assertEquals(7, len(list(self._obj._containers_lut.containers.keys())))
        
        #--------------------------------------------------------------------
        # verify updates
        record = self._obj._containers_lut.get_container_record('11111', False)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals('A11111', record['number'])
        self.assertEquals('L', record['status'])
        self.assertEquals(7, len(list(self._obj._containers_lut.containers.keys())))
        
        #--------------------------------------------------------------------
        # verify not found scanned
        self.post_dialog_responses('ready')
        record = self._obj._containers_lut.get_container_record('A66666', True)
        self.validate_prompts('Container <spell>A66666</spell> not found, to try again say ready')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','A66666','0'])
        self.assertEquals(None, record)
        #RALLYTC: 1059 - verify containers have been appended
        self.assertEquals(8, len(list(self._obj._containers_lut.containers.keys())))

    def test_get_container(self):
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')

        self.set_server_response('1,A12345,12345,N,1,0,\n\n' +
                                       '1,B12345,12345,N,1,0,\n\n' + 
                                       '1,A11111,11111,N,1,0,\n\n' +
                                       '1,A22222,22222,N,1,0,\n\n')
        
        self.start_server()
        #-----------------------------------------------------------------
        #RALLYTC: 1060 - Confirm that the operator is notified if the container entered does not exist.
        #Test container not found
        self.post_dialog_responses('66666', 'ready')
        
        self._obj.runState(GET_CONTAINER)
        
        self.assertEquals(self._obj.next_state, GET_CONTAINER)
        self.validate_prompts('Container?',
                              'Container <spell>66666</spell> not found, to try again say ready')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer', '66666', '1'])
         
        #-----------------------------------------------------------------
        #Test container found
        self._obj.next_state = None
        self.post_dialog_responses('11111')
        
        self._obj.runState(GET_CONTAINER)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Container?') 
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #Test consolidate
        self._obj.next_state = None
        self.post_dialog_responses('consolidate')
        
        self.assertRaises(Launch, self._obj.runState, GET_CONTAINER)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, ConsolidateContainerTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'consolidateContainer')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, CHECK_COMPLETE)
        self.validate_prompts('Container?')

        #-----------------------------------------------------------------
        #Test Consolidate not allowed
        self._obj._region_config_lut[0]['max_consolidation'] = 1

        self.post_dialog_responses('consolidate','ready')
        
        self._obj.runState(GET_CONTAINER)
        
        self.assertEquals(self._obj.next_state, GET_CONTAINER)
        self.validate_prompts('Container?',
                              'Consolidation not allowed, To continue say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #Test Unload
        self._obj.next_state = None
        self.post_dialog_responses('unload container')
        
        self.assertRaises(Launch, self._obj.runState, GET_CONTAINER)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, UnloadContainerTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'unloadContainer')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, CHECK_COMPLETE)
        self.validate_prompts('Container?')
        
        #-----------------------------------------------------------------
        #Test route Complete Fail
        self._obj._request_route_lut.receive('1,123,1,789,5,13,,0,\n\n')

        self._obj.next_state = None
        self.post_dialog_responses('complete route', 'ready')
        self.set_server_response('1,Error message\n\n')

        self._obj.runState(GET_CONTAINER)

        self.assertEquals(self._obj.next_state, GET_CONTAINER)
        self.validate_prompts('Container?',
                              'Error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTLoadingCompleteRoute','1'])
        self.assertFalse(self._obj.route_complete)
        
        #-----------------------------------------------------------------
        #Test Route Complete Success
        self._obj._request_route_lut.receive('2,123,1,789,5,13,,0,\n\n')

        self._obj.next_state = None
        self.post_dialog_responses('complete route')
        self.set_server_response('0,\n\n')

        self._obj.runState(GET_CONTAINER)

        self.assertEquals(self._obj.next_state, ROUTE_COMPLETE_PRINT)
        self.validate_prompts('Container?')
        self.validate_server_requests(['prTaskLUTLoadingCompleteRoute','2'])
        self.assertTrue(self._obj.route_complete)
        
    def test_load_container(self):
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj._containers_lut.receive('1,A12345,12345,N,1,0,\n\n' +
                                                 '1,B12345,12345,N,1,0,\n\n' + 
                                                 '1,A11111,11111,N,1,0,\n\n' +
                                                 '1,A22222,22222,N,1,0,\n\n')

        #-----------------------------------------------------------------
        #Test ok to load
        self._obj.container_rec = self._obj._containers_lut[2]
        self._obj.trailer_id = '789'
        
        self.assertRaises(Launch, self._obj.runState, LOAD)
        
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, LoadContainerTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'loadContainer')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, ROUTE_COMPLETE_PRINT)
        temp = self._obj.taskRunner.task_stack[0].obj
        self.assertEquals(temp.master_container, '')
        self.assertEquals(temp.status, 'L')
        self.assertEquals(temp.container, 'A11111')
        self.assertEquals(temp.trailer_id, '789')
        
    def test_check_complete(self):
        self._obj._containers_lut.receive('1,A12345,12345,L,1,0,\n\n' +
                                                 '1,B12345,12345,C,1,0,\n\n' + 
                                                 '1,A11111,11111,N,1,0,\n\n' +
                                                 '1,A22222,22222,N,1,0,\n\n')
        
        #-----------------------------------------------------------------
        #Test not complete
        self._obj.container_rec = self._obj._containers_lut[2]
        self._obj.trailer_id = '789'
        self._obj.route_complete = False
        
        self._obj.runState(CHECK_COMPLETE)
        
        self.assertEquals(self._obj.next_state, GET_CONTAINER)
        self.validate_prompts() 
        self.validate_server_requests()
        self.assertEquals(self._obj._containers_lut[0]['status'], 'L')
        
        #-----------------------------------------------------------------
        #RALLYTC: 1065 - Test complete
        self._obj.container_rec = self._obj._containers_lut[3]
        self._obj.trailer_id = '789'
        self._obj.route_complete = True
        self.post_dialog_responses('ready')
        
        self._obj.runState(CHECK_COMPLETE)
        
        self.assertEquals(self._obj.next_state, REQUEST_ROUTE)
        self.validate_prompts('Route complete, for next route say ready') 
        self.validate_server_requests()
        self.assertEquals(self._obj._containers_lut[3]['status'], 'N')
        self.assertFalse(self._obj.route_complete)

        self.start_server()
        #-----------------------------------------------------------------
        #Test change function, no
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj.next_state = None
        self._obj.route_complete = True
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(CHECK_COMPLETE)
        
        self.assertEquals(self._obj.next_state, CHECK_COMPLETE)
        self.validate_prompts('Route complete, for next route say ready', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.route_complete, True)

        #-----------------------------------------------------------------
        #Test change function, yes
        self._obj.next_state = None
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, CHECK_COMPLETE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Route complete, for next route say ready', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])

        #-----------------------------------------------------------------
        #Test change region, no
        self._obj._region_config_lut.receive('1,loading region 1,1,5,3,3,0,\n\n')
        self._obj.next_state = None
        self._obj.route_complete = True
        self._obj.region_selected = True
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(CHECK_COMPLETE)
        
        self.assertEquals(self._obj.next_state, CHECK_COMPLETE)
        self.validate_prompts('Route complete, for next route say ready', 
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.route_complete, True)

        #-----------------------------------------------------------------
        #Test change function, yes
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, CHECK_COMPLETE)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts('Route complete, for next route say ready', 
                              'Change region, correct?')
        self.validate_server_requests()
        
    def test_change_region(self):
        #TESTLINK 95630 :: Tests for Change Region
        
        #---------------------------------------------------------------
        #Test no region currently selected
        self._obj.region_selected = False
        
        self._obj.change_region()
        
        self.validate_prompts() 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response no
        self._obj.region_selected = True
        self.post_dialog_responses('no')
        
        self._obj.change_region()
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response yes
        self._obj.region_selected = True
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.change_region)
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
        
    def test_change_route(self):
        #---------------------------------------------------------------
        #Test region selected, response no
        self.post_dialog_responses('no')
        
        self._obj.change_route()
        
        self.validate_prompts('change route, correct?') 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response yes
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.change_route)
        
        self.validate_prompts('change route, correct?') 
        self.validate_server_requests()
        
if __name__ == '__main__':
    unittest.main()


