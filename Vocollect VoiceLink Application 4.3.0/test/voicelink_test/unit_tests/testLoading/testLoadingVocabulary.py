from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from loading.LoadingTask import LoadingTask, INITIALIZE_ROUTE, GET_CONTAINER
from vocollect_core.task.task_runner import Launch
from core.VoiceLink import VoiceLink
from common import VoiceLinkLut
import unittest


class testLoadingVocabulary(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        VoiceLinkLut.transport._timeout = 1
        self._obj = obj_factory.get(LoadingTask, VoiceLink())
        self._obj.taskRunner._append(self._obj)
    
    def test_valid(self):
        
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('change route'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('route number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('reprint report'), False)

        self._obj.dynamic_vocab.route_number = '123'
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('change route'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('route number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('reprint report'), True)

        self._obj.dynamic_vocab.container_lut = self._obj._containers_lut
        self._obj.dynamic_vocab.route_number = None
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('change route'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('route number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('reprint report'), False)

    def test_how_much_more(self):
        self.start_server()
        
        #TESTLINK 95620 :: Test how much more
        self._obj._containers_lut.receive('1,A12345,12345,L,1,0,\n\n' +
                                        '1,B12345,12345,C,1,0,\n\n' + 
                                        '1,A11111,11111,N,1,0,\n\n' +
                                        '1,A22222,22222,N,1,0,\n\n')
        for con in self._obj._containers_lut:
            self._obj._containers_lut.containers[con['number']] = con
            
        self._obj.dynamic_vocab.container_lut = self._obj._containers_lut
        self._obj.dynamic_vocab.route_number = '123'
        
        self._obj.dynamic_vocab._how_much_more()
        
        self.validate_prompts('2 containers.')
        self.validate_server_requests(['prTaskLUTLoadingRequestContainer','','1'])
        
    def test_route_number(self):
        #TESTLINK 95620 :: Test route number
        self._obj.dynamic_vocab.route_number = '123'
        
        self._obj.dynamic_vocab._route_number()
        
        self.validate_prompts('Route 123')
        self.validate_server_requests()
    
    def test_reprint_report(self):
        #TESTLINK 95609 :: Test reprint report manual
        #TESTLINK 95611 :: Test reprint report auto
        self._obj.current_state = INITIALIZE_ROUTE

        self.assertRaises(Launch, self._obj.dynamic_vocab._reprint_report)
        
        self.assertEquals(self._obj.current_state, INITIALIZE_ROUTE)
        
    def test_change_route(self):
        self._obj.current_state = GET_CONTAINER

        #----------------------------------------------
        # Test No
        self.post_dialog_responses('no')

        self._obj.dynamic_vocab._change_route()
        
        self.validate_prompts('change route, correct?')
        self.validate_server_requests()
        
        #----------------------------------------------
        # Test Yes
        self.post_dialog_responses('yes')

        self.assertRaises(Launch, self._obj.dynamic_vocab._change_route)
        
        self.validate_prompts('change route, correct?')
        self.validate_server_requests()
        
        
if __name__ == '__main__':
    unittest.main()


