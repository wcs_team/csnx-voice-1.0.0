from BaseVLTestCase import BaseVLTestCase, ODR_SERVER #Needs to be first import

from vocollect_core import obj_factory
from core.CoreTask import CoreTask
from core.VoiceLink import VoiceLink
from core.TakeBreakTask import TakeBreakTask
from core.TakeBreakTask import SELECT_BREAK, TRANSMIT_START_BREAK 
from core.TakeBreakTask import ENTER_PASSWORD, TRANSMIT_STOP_BREAK
import time

############################################
# Test for take a break class              #
############################################
class testTakeABreak(BaseVLTestCase):
 
    def setUp(self):
        self.clear()
        coretask = obj_factory.get(CoreTask, VoiceLink())
        self._obj = obj_factory.get(TakeBreakTask, 
                                      coretask._break_lut, 
                                      '123', 
                                      coretask.taskRunner, coretask)
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'takeABreak')

        #test states
        self.assertEquals(self._obj.states[0], SELECT_BREAK)
        self.assertEquals(self._obj.states[1], TRANSMIT_START_BREAK)
        self.assertEquals(self._obj.states[2], ENTER_PASSWORD)
        self.assertEquals(self._obj.states[3], TRANSMIT_STOP_BREAK)
        
    def testSelectBreak(self):
        self.post_dialog_responses('1', 'yes')
        self._obj._breaks_lut.receive('1,lunch,0,\n2,battery,0,\n\n')
        self._obj.runState(SELECT_BREAK)
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj.breakType, '1')
        self.validate_server_requests()
        self.validate_prompts('Break type?',
                              'lunch, correct?')
        
    def testEnterPassword(self):
        self._obj.password = '123'
        self.post_dialog_responses('ready', '123!')
        self._obj.runState(ENTER_PASSWORD)
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        self.validate_prompts('To continue work, say ready', 'Password?')
        
     
    def testStartBreak(self):
        self.start_server(ODR_SERVER)
        self._obj._breaks_lut.receive('1,lunch,0,\n2,battery,0,\n\n') 
         
        #Test valid response -- confirmation byte
        self.set_server_response('Y')
        self._obj.breakType = '1'    

        self._obj.runState(TRANSMIT_START_BREAK)
        time.sleep(0.5)
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskODRCoreSendBreakInfo', '1','0','lunch'])
        self.assertEquals(0, self._obj._break_odr.has_pending_odr())
        self.validate_prompts()
        
    def testStopBreak(self):
        self.start_server(ODR_SERVER)
        self._obj._breaks_lut.receive('1,lunch,0,\n2,battery,0,\n\n') 
         
        #Test valid response -- confirmation byte
        self.set_server_response('Y')
        self._obj.breakType = '1'    

        self._obj.runState(TRANSMIT_STOP_BREAK)
        time.sleep(0.5)
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskODRCoreSendBreakInfo', '1','1','lunch'])
        self.assertEquals(0, self._obj._break_odr.has_pending_odr())
        self.validate_prompts()
        

