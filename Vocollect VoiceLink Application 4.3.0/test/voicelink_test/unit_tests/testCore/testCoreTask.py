from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from backstocking.BackStockingTask import BackstockingTask
from core.CoreTask import CoreTask
from core.CoreTask import REQUEST_CONFIG, REQUEST_BREAKS, GET_WELCOME
from core.CoreTask import REQUEST_SIGNON, REQUEST_VEHICLES, REQUEST_FUNCTIONS 
from core.CoreTask import EXECUTE_FUNCTIONS
from core.VehicleTask import VehicleTask
from core.VoiceLink import VoiceLink
from lineloading.LineLoadingTask import LineLoadingTask
from loading.LoadingTask import LoadingTask
from forkapps.PutawayTask import PutawayTask
from puttostore.PutToStoreTask import PutToStoreTask
from forkapps.ReplenishmentTask import ReplenishmentTask
from selection.SelectionTask import SelectionTask
from vocollect_core.task.task_runner import Launch
import unittest
import mock_catalyst

class testCoreTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(CoreTask, VoiceLink())
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'taskCore')

        #test states
        self.assertEquals(self._obj.states[0], REQUEST_CONFIG)
        self.assertEquals(self._obj.states[1], REQUEST_BREAKS)
        self.assertEquals(self._obj.states[2], GET_WELCOME)
        self.assertEquals(self._obj.states[3], REQUEST_SIGNON)
        self.assertEquals(self._obj.states[4], REQUEST_VEHICLES)
        self.assertEquals(self._obj.states[5], REQUEST_FUNCTIONS)
        self.assertEquals(self._obj.states[6], EXECUTE_FUNCTIONS)
        
        #test LUTS defined correctly
        fields = self._obj._config_lut.fields
        self.assertEquals(fields['customerName'].index, 0)
        self.assertEquals(fields['operatorId'].index, 1)
        self.assertEquals(fields['confirmPassword'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)
        
        fields = self._obj._break_lut.fields
        self.assertEquals(fields['type'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)

        fields = self._obj._signon_lut.fields
        self.assertEquals(fields['interleave'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)

        fields = self._obj._functions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
         
    def test_request_configurations(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response
        self.set_server_response('Vocollect,123,0,0,\n\n', 'prTaskLUTCoreConfiguration')

        self._obj.runState(REQUEST_CONFIG)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #RALLYTC: 1083 - Verify that selector hears an error message if error is returned in LUT response.
        #Test Error response
        self.set_server_response('Vocollect,123,0,1,Error Message\n\n', 'prTaskLUTCoreConfiguration')
        self.post_dialog_responses('ready')
        
        self._obj.runState(REQUEST_CONFIG)
        
        self.assertEquals(self._obj.next_state, REQUEST_CONFIG) #current state should not have changed
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'])
        self.validate_prompts('Error Message')
        
        #---------------------------------------------------------------------------
        #RALLYTC: 1114 - Verify that Core LUT requests are re-sent if a timeout occurs.
        #Test communication error
        self.stop_server()
        self.post_dialog_responses('ready')
        
        self._obj.runState(REQUEST_CONFIG)
        
        self.assertEquals(self._obj.next_state, REQUEST_CONFIG) #current state should not have changed
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')
        
        
    def test_request_breaks(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response
        self.set_server_response('1,lunch,0,\n\n', 'prTaskLUTCoreBreakTypes')
        
        self._obj.runState(REQUEST_BREAKS)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #RALLYTC: 1111 - Verify that selector hears an error message if error is returned in LUT response.
        #Test Error response
        self.set_server_response('1,lunch,1,Error Message\n\n', 'prTaskLUTCoreBreakTypes')
        self.post_dialog_responses('ready')

        self._obj.runState(REQUEST_BREAKS)
        
        self.assertEquals(self._obj.next_state, REQUEST_BREAKS)
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'])
        self.validate_prompts('Error Message, To continue say ready')

        #---------------------------------------------------------------------------
        #RALLYTC: 1114 - Verify that Core LUT requests are re-sent if a timeout occurs.
        #Test Communication error
        self.stop_server()
        self.post_dialog_responses('ready')

        self._obj.runState(REQUEST_BREAKS)
        
        self.assertEquals(self._obj.next_state, REQUEST_BREAKS)
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

    def test_welcome(self):
        #---------------------------------------------------------------------------
        #Test Welcome prompt built correctly
        self._obj._config_lut.receive('Vocollect,123,0,0,\n\n')
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_WELCOME)
        
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready')
        self.validate_server_requests()
        
        #---------------------------------------------------------------------------
        #Test Welcome prompt built correctly
        self._obj._config_lut.receive('Unit Test,123,0,0,\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(GET_WELCOME)
        
        self.validate_prompts('Welcome to the Unit Test system. Current operator is Operator.Name. Say ready')
        self.validate_server_requests()

    def test_request_signon(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response
        mock_catalyst.application_properties['CollectVehicleInfo'] = 'false'
        self._obj._config_lut.receive('Vocollect,Operator.Id,0,0,\n\n')
        self.post_dialog_responses('123!')
        self.set_server_response('0,0,\n\n', 'prTaskLUTCoreSignOn')

        self._obj.runState(REQUEST_SIGNON)
        
        self.assertEquals(self._obj.next_state, REQUEST_FUNCTIONS) #Next state should be None
        self.validate_server_requests(['prTaskLUTCoreSignOn', '123'])
        self.validate_prompts('Password?')
        mock_catalyst.application_properties['CollectVehicleInfo'] = 'true'

        #---------------------------------------------------------------------------
        #Test operator ID different
        self._obj.next_state = None
        self._obj._config_lut.receive('Vocollect,Operator,0,0,\n\n')
        self.post_dialog_responses('123!')
        self.set_server_response('Vocollect,Operator.Id,0,0,\n\n', 'prTaskLUTCoreConfiguration')
        self.set_server_response('0,0,\n\n', 'prTaskLUTCoreSignOn')

        self._obj.runState(REQUEST_SIGNON)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'])
        self.validate_prompts('Password?')
        
        #---------------------------------------------------------------------------
        #RALLYTC: 1112 - Verify that selector hears an error message if error is returned in LUT response.
        #Test Error response
        self._obj._config_lut.receive('Vocollect,Operator.Id,0,0,\n\n')
        self.post_dialog_responses('456!', 'yes', 'ready')
        self.set_server_response('0,1,Error Message\n\n', 'prTaskLUTCoreSignOn')
        
        self._obj.runState(REQUEST_SIGNON)
        
        self.assertEquals(self._obj.next_state, REQUEST_SIGNON)
        self.validate_server_requests(['prTaskLUTCoreSignOn', '456'])
        self.validate_prompts('Password?',
                              'Error Message, To continue say ready')
        
        #---------------------------------------------------------------------------
        #RALLYTC: 1114 - Verify that Core LUT requests are re-sent if a timeout occurs.
        #Test error contacting host 
        #self._obj._config_lut.receive('Vocollect,Operator.Id,0,0,\n\n')
        #self.post_dialog_responses('456!', 'yes', 'ready')
        #self.stop_server()
        
        #self._obj.runState(REQUEST_SIGNON)
        
        #self.assertEquals(self._obj.next_state, REQUEST_SIGNON)
        #self.validate_server_requests()
        #self.validate_prompts('Password?',
        #                      'Error contacting host,  to try again say ready')
        

    
    def test_vehicles(self):
        #EXAMPLE: Sample test of launching a task
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, REQUEST_VEHICLES)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, VehicleTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskVehicle')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, REQUEST_FUNCTIONS)
        
    def test_request_functions(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #RALLYTC: 1141 - Verify that operator is notified if no functions are returned.
        #Test valid response, 0 functions
        self.set_server_response('0,,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('sign off')
        self.assertRaises(Launch, self._obj.request_functions)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreSignOff'])
        self.validate_prompts('Not authorized for any supported functions. Notify your supervisor')

        #---------------------------------------------------------------------------
        #RALLYTC: 1139 - Confirm that operator is prompted if any functions are returned.
        #Test valid response, >0 functions
        self.set_server_response('1,Only Function,0,\n\n', 'prTaskLUTCoreValidFunctions')
        
        self._obj.runState(REQUEST_FUNCTIONS)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #RALLYTC: 1138 - Verify that selector hears an error message if error is returned in LUT response.
        #Test error response
        self.set_server_response('1,Only Function,1,Error Message\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('ready')
        
        self._obj.runState(REQUEST_FUNCTIONS)
        
        self.assertEquals(self._obj.next_state, REQUEST_FUNCTIONS) 
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts('Error Message, To continue say ready')

        #---------------------------------------------------------------------------
        #RALLYTC: 1114 - Verify that Core LUT requests are re-sent if a timeout occurs.
        #Test error contacting host
        self.stop_server()
        self.post_dialog_responses('ready')

        self._obj.runState(REQUEST_FUNCTIONS)
        
        self.assertEquals(self._obj.next_state, REQUEST_FUNCTIONS) 
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

    def test_execute_function(self):

        #---------------------------------------------------------------------------
        #RALLYTC: 1150 - Verify that selector is notified if function is not implemented.
        #Test single functions
        self.post_dialog_responses('ready')
        self._obj._functions_lut.receive('99,Only Function,0,\n\n')

        self._obj.runState(EXECUTE_FUNCTIONS)
        
        self.assertEquals(self._obj.next_state, REQUEST_FUNCTIONS) 
        self.validate_server_requests()
        self.validate_prompts('Only Function',
                              'function 99 not implemented, say ready to try again')
        
        #---------------------------------------------------------------------------
        #RALLYTC: 1150 - Verify that selector is notified if function is not implemented.
        #Test multiple functions
        self.post_dialog_responses('98', '1', 'yes')
        self._obj._functions_lut.receive('1,Function 1,0,\n2,Function 2,0,\n\n')

        self.assertRaises(Launch, self._obj.runState, EXECUTE_FUNCTIONS)
        
        self.assertEquals(self._obj.next_state, REQUEST_FUNCTIONS) 
        self.validate_server_requests()
        self.validate_prompts('Function?',
                              'Not authorized for <spell>9</spell>, Try again.',
                              'Function?',
                              'Not authorized for <spell>8</spell>, Try again.',
                              'Function?',
                              'Function 1, correct?')

    def test_launch_putaway(self):
        self._launch(1, PutawayTask, 'putAway')

    def test_launch_replenishment(self):
        self._launch(2, ReplenishmentTask, 'replenishment')

    def test_launch_selection(self):

        self._launch(3, SelectionTask, 'selection')
        self.assertTrue(self._obj.taskRunner.task_stack[0].obj.function, '3')

        self._launch(4, SelectionTask, 'selection')
        self.assertTrue(self._obj.taskRunner.task_stack[0].obj.function, '4')
        
        self._launch(6, SelectionTask, 'selection')
        self.assertTrue(self._obj.taskRunner.task_stack[0].obj.function, '6')
        
    def test_launch_lineloading(self):
        self._launch(7, LineLoadingTask, 'lineLoading')

    def test_launch_puttostore(self):
        self._launch(8, PutToStoreTask, 'putToStore')

    def test_launch_loading(self):
        self._launch(10, LoadingTask, 'loading')

    def test_launch_backstocking(self):
        self._launch(11, BackstockingTask, 'backStocking')

    def _launch(self, function, type, name):
        #---------------------------------------------------------------------------
        #Test launch Application
        self._obj._functions_lut.receive(str(function) + ',Only Function,0,\n\n')
        self.assertRaises(Launch, self._obj.runState, EXECUTE_FUNCTIONS)
        self.validate_prompts('Only Function')
        self.validate_server_requests()
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, type))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, name)
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, EXECUTE_FUNCTIONS)
        
    def test_sign_off_confirm(self):
        #TESLINK 95552 :: Test Case Verify Sign Off Global Word
        self.start_server()
        self._obj.password='123'
        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.sign_off_confirm)
        self.assertEquals(self._obj.password, None)
        self.validate_prompts('Sign off, correct?')
        
        self._obj.password=None
        self._obj.sign_off_confirm()
        self.assertEquals(self._obj.password, None)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCoreSignOff'])
        
        self._obj.password='123'
        self.post_dialog_responses('no')
        self._obj.sign_off_confirm()
        self.assertEquals(self._obj.password, '123')
        self.validate_prompts('Sign off, correct?')
        self.validate_server_requests()
       
        self._obj.password='123'
        self.post_dialog_responses('yes', 'ready')
        self.set_server_response('99,error message\n\n')
        self.assertRaises(Launch, self._obj.sign_off_confirm)
        self.assertEquals(self._obj.password, None)
        self.validate_prompts('Sign off, correct?',
                              'error message, say ready')
        self.validate_server_requests(['prTaskLUTCoreSignOff'])
       
        self._obj.password='123'
        self.post_dialog_responses('yes', 'ready')
        self.set_server_response('199,test error\n\n')
        self.set_server_response('0,\n\n')
        self.assertRaises(Launch, self._obj.sign_off_confirm)
        self.assertEquals(self._obj.password, None)
        self.validate_prompts('Sign off, correct?',
                              'test error, To continue say ready')
        self.validate_server_requests(['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreSignOff'])


        #---------------------------------------------------------------        
        #sign off not allowed
        self._obj.password='123'
        self._obj.sign_off_allowed = False
        self.post_dialog_responses()
        self.set_server_response('0,\n\n')
        self._obj.sign_off_confirm()
        
        self.assertEquals(self._obj.password, '123')
        self.validate_prompts('Sign off not allowed.')
        self.validate_server_requests()
        
        
        #---------------------------------------------------------------        
        #sign off not allowed, override
        self._obj.password='123'
        self._obj.sign_off_allowed = False
        self.post_dialog_responses('no')
        self.set_server_response('0,\n\n')
        self._obj.sign_off_confirm(True)
        
        self.assertEquals(self._obj.password, '123')
        self.validate_prompts('Sign off, correct?')
        self.validate_server_requests()
        
        
    def test_change_function(self):
        #TESLINK 95547 :: Test Case Verify Change Function Global Word
        self.start_server()
         
        self._obj._functions_lut.receive('1,Function 1,0,\n2,Function 2,0,\n\n')
        self.set_server_response('1,Only Function,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        
        self._obj.password = None
        self._obj.function =None
        self._obj.change_function()
        
        self.assertEquals(self._obj.function, None)
        self.validate_prompts()
        
        self._obj.password = 123
        self._obj.change_function()
        self.assertEquals(self._obj.function, None)
        self.validate_prompts()
        
        self._obj.function =1
        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.change_function)
        self.assertEquals(self._obj.function, 1)
        self.validate_prompts('change function, correct?')
        
        self._obj._functions_lut.receive('1,Function 1,0,\n\n')
        self.set_server_response('1,Only Function,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj.change_function()
        self.assertEquals(self._obj.function, 1)
        self.validate_prompts('Change function not allowed')
        
        self._obj.function =None
        self._obj._functions_lut.receive('1,Function 1,0,\n2,Function 2,0,\n\n')
        self.set_server_response('1,Only Function,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj.change_function()
        self.assertEquals(self._obj.function, None)
        self.validate_prompts()
        
        self.clear()
        self._obj.function =1
        self.set_server_response('1,Only Function,0,\n2,Function 2,0,') #time out
        self.set_server_response('1,Only Function,0,\n2,Function 2,0,\n\n')
        self.post_dialog_responses('ready')
        self.post_dialog_responses('no')
        self._obj.change_function()
        
#Commented out this code after consulting with QA because of inconsistent number of beeps.

#        self.validate_prompts('beep',
#                              'beep',
#                              'beep',
#                              'beep',
#                              'Error contacting host,  to try again say ready',
#                              'change function, correct?')
        
        
    def test_take_a_break(self):
        #TESLINK 95554 :: Test Case Verify Take a Break Global Word Partially automated
        self.start_server()
        self._obj.password=123
        self._obj._break_lut.receive(',lunch,0,\n\n')
        self.post_dialog_responses('ready')
        self._obj.take_a_break()
        self.validate_prompts('Breaks not allowed, to continue say ready')
        self.validate_server_requests()
        
        self._obj.password=None
        self._obj._break_lut.receive('1,lunch,0,\n\n')
        self._obj.take_a_break()
        self.validate_prompts()
        self.validate_server_requests()
       
        self._obj.password=123
        self._obj._break_lut.receive('1,lunch,0,\n\n')
        self.post_dialog_responses('no')
        self._obj.take_a_break()
        self.validate_prompts('take a break, correct?')
        self.validate_server_requests()
       
        self._obj.password=123
        self._obj.current_state=REQUEST_FUNCTIONS
        self._obj._break_lut.receive('1,lunch,0,\n\n')
        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.take_a_break)
        self.validate_prompts('take a break, correct?')
        self.validate_server_requests()
 
if __name__ == '__main__':
    unittest.main()
