from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from core.VehicleTask import VehicleTask, NEXT_SAFETY_CHECK,\
    COMPLETE_SAFETYCHECK
from core.VehicleTask import REQUEST_VEHTYPE, REQUEST_VEHICLEID, REQUEST_XMIT_VEHID, EXECUTE_SAFETYCHECK
import unittest

class testVehicleTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(VehicleTask, VoiceLink())
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'taskVehicle')

        #test states
        self.assertEquals(self._obj.states[0], REQUEST_VEHTYPE)
        self.assertEquals(self._obj.states[1], REQUEST_VEHICLEID)
        self.assertEquals(self._obj.states[2], REQUEST_XMIT_VEHID)
        self.assertEquals(self._obj.states[3], NEXT_SAFETY_CHECK)
        self.assertEquals(self._obj.states[4], EXECUTE_SAFETYCHECK)
        self.assertEquals(self._obj.states[5], COMPLETE_SAFETYCHECK)
        
        #test LUTS defined correctly
        fields = self._obj._veh_types_lut.fields
        self.assertEquals(fields['type'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['captureId'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)
        
        fields = self._obj._veh_id_lut.fields
        self.assertEquals(fields['safetyCheck'].index, 0)
        self.assertEquals(fields['responseType'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)

        fields = self._obj._veh_safety_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

    def test_request_veh_types(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response with vehicles
        self.set_server_response('1,vehicle 1,0,0,\n\n', 'prTaskLUTCoreValidVehicleTypes')
        self._obj.runState(REQUEST_VEHTYPE)
        
        self.assertEquals(self._obj.next_state, REQUEST_VEHICLEID) #Next state should be None
        
        self.validate_server_requests(['prTaskLUTCoreValidVehicleTypes', '0'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #RALLYTC: 1116 - Go directly to function prompt if no vehicles are returned.
        #Test valid response with no vehicles
        self.set_server_response('0,,0,0,\n\n', 'prTaskLUTCoreValidVehicleTypes')
        self._obj.runState(REQUEST_VEHTYPE)
        
        self.assertEquals(self._obj.next_state, '') #Next state should be None
        
        self.validate_server_requests(['prTaskLUTCoreValidVehicleTypes', '0'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #RALLYTC: 1117 - Error message is returned in response to the Valid Vehicle Types LUT.
        #Test Error response
        self.set_server_response('1,vehicle 1,0,1,Error Message\n\n', 'prTaskLUTCoreValidVehicleTypes')
        self.post_dialog_responses('ready')

        self._obj.runState(REQUEST_VEHTYPE)
        
        self.assertEquals(self._obj.next_state, REQUEST_VEHTYPE) #current state should not have changed
        self.validate_server_requests(['prTaskLUTCoreValidVehicleTypes', '0'])
        self.validate_prompts('Error Message, To continue say ready')

        #---------------------------------------------------------------------------
        #RALLYTC: 1114 - Verify that Core LUT requests are re-sent if a timeout occurs.
        #Test Error contacting host
        self.stop_server()
        self.post_dialog_responses('ready')

        self._obj.runState(REQUEST_VEHTYPE)
        
        self.assertEquals(self._obj.next_state, REQUEST_VEHTYPE) #current state should not have changed
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

    def test_request_vehicle_id_confirm_vehType_prompt(self):
        #---------------------------------------------------------------------------
        #RALLYTC: 1122 - Confirm operator is prompted for vehicle type if any vehicles returned.
        #Test valid response, no ID, no safety check
        self.set_server_response(',0,\n\n', 'prTaskLUTCoreSendVehicleIDs')
        self._obj._veh_types_lut.receive('1,vehicle 1,0,0,\n\n')

        self._obj.runState(REQUEST_VEHICLEID)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_server_requests()
        self.assertEquals('1', self._obj.vehicle_type)
        self.assertEquals('', self._obj.vehicle_id)
        
    def test_request_vehicle_id_confirm_vehId_prompt(self):        
        #---------------------------------------------------------------------------
        #RALLYTC: 1125 - Confirm operator is prompted for vehicle ID.
        #Test valid response, ID
        self.set_server_response(',0,\n\n', 'prTaskLUTCoreSendVehicleIDs')
        self.post_dialog_responses('35!', 'yes')
        self._obj._veh_types_lut.receive('2,vehicle 1,1,0,\n\n')

        self._obj.runState(REQUEST_VEHICLEID)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_server_requests()
        self.validate_prompts('Vehicle ID?',
                              '35, correct?')
        self.assertEquals('2', self._obj.vehicle_type)
        self.assertEquals('35', self._obj.vehicle_id)
        
    def test_request_vehicle_id_invalidResponse(self):        
        #---------------------------------------------------------------------------
        #Test invalid response for vehicle        
        self.set_server_response('brakes,0,\n\n', 'prTaskLUTCoreSendVehicleIDs')
        self.post_dialog_responses('2', '1', 'yes')
        self._obj._veh_types_lut.receive('1,vehicle 1,0,0,\n\n')
        
        self._obj.runState(REQUEST_VEHICLEID)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests() 
        self.assertEquals('1', self._obj.vehicle_type)
        self.assertEquals('', self._obj.vehicle_id)
        
    def test_xmit_veh_id(self):
        #---------------------------------------------------------------------------
        #RALLYTC: 1114 - Verify that Core LUT requests are re-sent if a timeout occurs.
        #test time out
        self.post_dialog_responses('ready')

        self._obj.runState(REQUEST_XMIT_VEHID)
        
        self.assertEquals(self._obj.next_state, REQUEST_XMIT_VEHID) 
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

        self.start_server()

        #---------------------------------------------------------------------------
        #RALLYTC: 1118 - Error message is returned in response to the Send Vehicle ID LUT.
        #test error
        self._obj.vehicle_type = '1'
        self._obj.vehicle_id = ''
        self.set_server_response(',0,1,Error message\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(REQUEST_XMIT_VEHID)
        
        self.assertEquals(self._obj.next_state, REQUEST_VEHICLEID) 
        self.validate_server_requests(['prTaskLUTCoreSendVehicleIDs', '1', ''])
        self.validate_prompts('Error message, To continue say ready')

        #---------------------------------------------------------------------------
        #RALLYTC: 1126 - Verify that if no value returned for safety check, next prompt is for function.
        #test no error no check list
        self._obj.vehicle_type = '2'
        self._obj.vehicle_id = '35'
        self.set_server_response(',0,\nbrakes,0,\n\n')

        self._obj.runState(REQUEST_XMIT_VEHID)
        
        self.assertEquals(self._obj.next_state, '') 
        self.validate_server_requests(['prTaskLUTCoreSendVehicleIDs', '2', '35'])
        self.validate_prompts()
                
        #---------------------------------------------------------------------------
        #test no error with check list
        self._obj.vehicle_type = '2'
        self._obj.vehicle_id = '35'
        self.set_server_response('brakes,0,\n\n')
        
        self._obj.runState(REQUEST_XMIT_VEHID)
        
        self.assertEquals(self._obj.next_state, NEXT_SAFETY_CHECK) 
        self.validate_server_requests(['prTaskLUTCoreSendVehicleIDs', '2', '35'])
        self.validate_prompts('Safety checklist. say yes, no or numeric value to verify each check')
    
    def test_next_safety_check(self):
        #initialize data
        self.start_server()
        self.set_server_response('brakes,0,\n'
                                 ',0,\n'
                                 'tires,0,\n'
                                 '\n')
        self._obj.runState(REQUEST_XMIT_VEHID)
        self.validate_prompts('Safety checklist. say yes, no or numeric value to verify each check')
        self.validate_server_requests(['prTaskLUTCoreSendVehicleIDs', '', ''])
        
        #-------------------------------------------------------------------
        #Test getting first record
        self._obj.next_state = None
        self._obj.runState(NEXT_SAFETY_CHECK)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._curr_saftey_check, self._obj._veh_id_lut[0])
        
        #-------------------------------------------------------------------
        #Test getting next record when safety check is blank
        self._obj.next_state = None
        self._obj.runState(NEXT_SAFETY_CHECK)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, NEXT_SAFETY_CHECK)
        self.assertEquals(self._obj._curr_saftey_check, self._obj._veh_id_lut[1])

        #-------------------------------------------------------------------
        #test getting next record, normal
        self._obj.next_state = None
        self._obj.runState(NEXT_SAFETY_CHECK)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._curr_saftey_check, self._obj._veh_id_lut[2])
                    
        #-------------------------------------------------------------------
        #test no more records
        self._obj.next_state = None
        self._obj.runState(NEXT_SAFETY_CHECK)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, COMPLETE_SAFETYCHECK)
        self.assertEquals(self._obj._curr_saftey_check, self._obj._veh_id_lut[2])
                    
    def test_execute_safety_check(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test pass safety check 
        self._obj._veh_id_lut.receive('brakes,B,0,\ntires,B,0,\nmirrors,B,0,\n\n')
        self._obj._curr_saftey_check = self._obj._veh_id_lut[0] 
        self.post_dialog_responses('yes')
        
        self._obj.runState(EXECUTE_SAFETYCHECK)
        
        self.validate_prompts('brakes')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, NEXT_SAFETY_CHECK) 

        #---------------------------------------------------------------------------
        #Test failed, but no to confirm, should repeat state
        self._obj._veh_id_lut.receive('brakes,B,0,\ntires,B,0,\nmirrors,B,0,\n\n')
        self._obj._curr_saftey_check = self._obj._veh_id_lut[1] 
        self.post_dialog_responses('no', 'no')
        
        self._obj.runState(EXECUTE_SAFETYCHECK)
        
        self.validate_prompts('tires',
                              'tires, failed, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, EXECUTE_SAFETYCHECK) 
        
        #---------------------------------------------------------------------------
        #Test safety check numeric response
        self._obj._veh_id_lut.receive('brakes,B,0,\ntires,B,0,\nmiles,N,0,\nmirrors,B,0,\n\n')
        self._obj._curr_saftey_check = self._obj._veh_id_lut[2] 
        self.post_dialog_responses('12345','yes')
        
        self._obj.runState(EXECUTE_SAFETYCHECK)
        
        self.validate_prompts('miles',
                              '12345, correct?')
        self.validate_server_requests(['prTaskLUTCoreSafetyCheck', '12345', 'miles', '0'])
        self.assertEquals(self._obj.next_state, NEXT_SAFETY_CHECK)
        
        #---------------------------------------------------------------------------
        #Test failed, but yes to confirm, yes to quick fix, should repeat state
        self._obj._veh_id_lut.receive('brakes,B,0,\ntires,B,0,\nmirrors,B,0,\n\n')
        self._obj._curr_saftey_check = self._obj._veh_id_lut[1] 
        self.set_server_response('0,\n\n')
        self.post_dialog_responses('no', 'yes', 'yes', 'ready')
        
        self._obj.runState(EXECUTE_SAFETYCHECK)
        
        self.validate_prompts('tires',
                              'tires, failed, correct?',
                              'Your vehicle has failed, can you do a quick repair?', 
                              'Perform quick repair, then say ready')
        self.validate_server_requests(['prTaskLUTCoreSafetyCheck', '-2', 'tires', '2'])
        self.assertEquals(self._obj.next_state, EXECUTE_SAFETYCHECK) 

        #---------------------------------------------------------------------------
        #Test failed, but yes to confirm, yes to quick fix, should repeat state
        self._obj._veh_id_lut.receive('brakes,B,0,\ntires,B,0,\nmirrors,B,0,\n\n')
        self._obj._curr_saftey_check = self._obj._veh_id_lut[2] 
        self.set_server_response('0,\n\n')
        self.post_dialog_responses('no', 'yes', 'no')
        
        self._obj.runState(EXECUTE_SAFETYCHECK)
        
        self.validate_prompts('mirrors',
                              'mirrors, failed, correct?',
                              'Your vehicle has failed, can you do a quick repair?',
                              'Vehicle failed, get new vehicle')
        self.validate_server_requests(['prTaskLUTCoreSafetyCheck', '-2', 'mirrors', '1'])
        self.assertEquals(self._obj.next_state, REQUEST_VEHTYPE) 

    def test_complete_safety_check(self):
        #---------------------------------------------------------------------------
        #test error contacting host
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(COMPLETE_SAFETYCHECK)
        
        self.assertEquals(self._obj.next_state, COMPLETE_SAFETYCHECK) 
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

        self.start_server()

        #---------------------------------------------------------------------------
        #test error
        self._obj.next_state = None
        self.set_server_response('1,Error message\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(COMPLETE_SAFETYCHECK)
        
        self.assertEquals(self._obj.next_state, COMPLETE_SAFETYCHECK) 
        self.validate_server_requests(['prTaskLUTCoreSafetyCheck', '-1', '', '0'])
        self.validate_prompts('Error message, To continue say ready')

        #---------------------------------------------------------------------------
        #test warning
        self._obj.next_state = None
        self.set_server_response('99,Warning message\n\n')
        self.post_dialog_responses('ready')

        self._obj.runState(COMPLETE_SAFETYCHECK)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_server_requests(['prTaskLUTCoreSafetyCheck', '-1', '', '0'])
        self.validate_prompts('Warning message, say ready')

        #---------------------------------------------------------------------------
        #test no error
        self._obj.next_state = None
        self.set_server_response('0,\n\n')
        
        self._obj.runState(COMPLETE_SAFETYCHECK)
        
        self.assertEquals(self._obj.next_state, None) 
        self.validate_server_requests(['prTaskLUTCoreSafetyCheck', '-1', '', '0'])
        self.validate_prompts()
    
        
if __name__ == '__main__':
    unittest.main()
        