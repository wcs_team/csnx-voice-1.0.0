from vocollect_core_test.base_test_case import BaseTestCaseCore #Should always be first import for tests to run from ant
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
from vocollect_core.task.task_runner import Launch
import unittest

class testVoiceLink(BaseTestCaseCore):

    def setUp(self):
        self.clear()
        self._obj = VoiceLink()

    def test_startUp(self):
        self.assertRaises(Launch, self._obj.startUp)
        
        self.assertTrue(isinstance(self._obj.task_stack[0].obj, CoreTask))
        self.assertEquals(self._obj.task_stack[0].name, 'taskCore')
        self.assertEquals(self._obj.task_stack[0].id, None)
        
    def test_initialize(self):
        #initialize method should be called when obj created and should set appname
        self.assertEquals(self._obj.app_name, 'VoiceLink')
        
if __name__ == '__main__':
    unittest.main()

