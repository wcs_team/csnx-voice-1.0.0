from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from common.RegionSelectionTasks import MultipleRegionTask
from forkapps.PutawayTask import PA_VALIDATE_REGION, PA_SPECIFY_LICENSES, PA_GET_LICENSES,\
    PA_START_LOCATION, PA_PUT_LICENSES, PA_REGIONS, PutawayTask
from common import VoiceLinkLut
import unittest

class testPutawayTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        VoiceLinkLut.transport._timeout = 1
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1
        self._obj = obj_factory.get(PutawayTask, temp.taskRunner)
        TaskRunnerBase._main_runner = temp.taskRunner
        
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putAway')

        #test states
        self.assertEquals(self._obj.states[0], PA_REGIONS)
        self.assertEquals(self._obj.states[1], PA_VALIDATE_REGION)
        self.assertEquals(self._obj.states[2], PA_START_LOCATION)
        self.assertEquals(self._obj.states[3], PA_SPECIFY_LICENSES)
        self.assertEquals(self._obj.states[4], PA_GET_LICENSES)
        self.assertEquals(self._obj.states[5], PA_PUT_LICENSES)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._request_reqion_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['capture_start_location'].index, 2)
        self.assertEquals(fields['allow_release_license'].index, 3)
        self.assertEquals(fields['allow_cancel_license'].index, 4)
        self.assertEquals(fields['allow_override_location'].index, 5)
        self.assertEquals(fields['allow_override_quantity'].index, 6)
        self.assertEquals(fields['allow_partial_put'].index, 7)
        self.assertEquals(fields['capture_pickup_quantity'].index, 8)
        self.assertEquals(fields['capture_put_quantity'].index, 9)
        self.assertEquals(fields['lic_digits_task_speak'].index, 10)
        self.assertEquals(fields['lic_digits_oper_speak'].index, 11)
        self.assertEquals(fields['loc_digits_oper_speak'].index, 12)
        self.assertEquals(fields['cd_digits_oper_speak'].index, 13)
        self.assertEquals(fields['max_lic_plate'].index, 14)
        self.assertEquals(fields['exception_location'].index, 15)
        self.assertEquals(fields['verify_license_location'].index, 16)
        self.assertEquals(fields['errorCode'].index, 17)
        self.assertEquals(fields['errorMessage'].index, 18)

        fields = self._obj._verify_license.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)
        
        fields = self._obj._licenses.fields
        self.assertEquals(fields['licenseId'].index, 0)
        self.assertEquals(fields['region'].index, 1)
        self.assertEquals(fields['locationId'].index, 2)
        self.assertEquals(fields['locationScan'].index, 3)
        self.assertEquals(fields['preAisle'].index, 4)
        self.assertEquals(fields['Aisle'].index, 5)
        self.assertEquals(fields['postAisle'].index, 6)
        self.assertEquals(fields['slot'].index, 7)
        self.assertEquals(fields['locationCD'].index, 8)
        self.assertEquals(fields['itemNumber'].index, 9)
        self.assertEquals(fields['description'].index, 10)
        self.assertEquals(fields['quantity'].index, 11)
        self.assertEquals(fields['goalTime'].index, 12)
        self.assertEquals(fields['errorCode'].index, 13)
        self.assertEquals(fields['errorMessage'].index, 14)

    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, PA_REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, MultipleRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskMultiRegions')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, PA_VALIDATE_REGION)
    
    def test_validate_region(self):
        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj.runState(PA_VALIDATE_REGION)

        self.validate_prompts('not authorized for any regions, see your supervisor')
        self.validate_server_requests()
        #-----------------------------------------------------------------
        #Test authorized regions
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self._obj.runState(PA_VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()

    def test_start_location(self):
        self._obj._region_config_lut.receive('1,put away region 1,0,1,1,1,1,1,0,0,5,5,5,2,1,exception location,0,0,\n\n')
        
        #Not support to capture start location
        self._obj._start_location.curr_location_id = None
        self._obj._region_config_lut[0]['capture_start_location'] = 0 #never
        self._obj.runState(PA_START_LOCATION)
        self.assertEquals(0, self._obj._license_count)
        self.assertEquals('', self._obj._start_location.curr_location_id)

        #One time before licenses specified (No license specified yet)
        self._obj._start_location.curr_location_id = None
        self._obj._region_config_lut[0]['capture_start_location'] = 1 #once before license, not specified
        self.assertRaises(Launch, self._obj.runState, PA_START_LOCATION)
        self.assertEquals(0, self._obj._license_count)
        self.assertEquals(None, self._obj._start_location.curr_location_id)

        #One time before licenses specified (license has been specified)
        self._obj._start_location.curr_location_id = '123'
        self._obj._region_config_lut[0]['capture_start_location'] = 1 #once before license, not specified
        self._obj.runState(PA_START_LOCATION)
        self.assertEquals(0, self._obj._license_count)
        self.assertEquals('123', self._obj._start_location.curr_location_id)

        #Every time before license is specified (no location yet)
        self._obj._start_location.curr_location_id = None
        self._obj._region_config_lut[0]['capture_start_location'] = 2 #everytime before a license
        self.assertRaises(Launch, self._obj.runState, PA_START_LOCATION)
        self.assertEquals(0, self._obj._license_count)
        self.assertEquals(None, self._obj._start_location.curr_location_id)

        #Every time before license is specified (location previously specified)
        self._obj._start_location.curr_location_id = '123'
        self._obj._region_config_lut[0]['capture_start_location'] = 2 #everytime before a license
        self.assertRaises(Launch, self._obj.runState, PA_START_LOCATION)
        self.assertEquals(0, self._obj._license_count)
        self.assertEquals(None, self._obj._start_location.curr_location_id)

        #Every time before putting a license
        self._obj._start_location.curr_location_id = None
        self._obj._region_config_lut[0]['capture_start_location'] = 3 #everytime before each license
        self._obj.runState(PA_START_LOCATION)
        self.assertEquals(0, self._obj._license_count)
        self.assertEquals('', self._obj._start_location.curr_location_id)
        
    def test_specify_licenses(self):
        self._obj._region_config_lut.receive('1,put away region 1,0,1,1,1,1,1,0,0,5,5,5,2,1,exception location,0,0,\n\n')
        
        #test no more when no licenses spoken
        self._obj._license_count = 0
        self.post_dialog_responses('no more')
        self._obj.runState(PA_SPECIFY_LICENSES)
        self.validate_prompts('License?',
                              'at least 1 license must be entered')
        self.assertEquals(self._obj.next_state, PA_SPECIFY_LICENSES)
        
        #test no more when licenses were specified
        self._obj.next_state = None
        self._obj._license_count = 1
        self.post_dialog_responses('no more')

        self._obj.runState(PA_SPECIFY_LICENSES)
        
        self.validate_prompts('License?',
                              'getting work')
        self.assertEquals(self._obj.next_state, None)
        
        
        self.start_server()
        #test error from verify license
        self.set_server_response('1,Error Message\n\n')
        self._obj.next_state = None
        self._obj._license_count = 0
        self.post_dialog_responses('12345',
                                   'ready')
        self._obj.runState(PA_SPECIFY_LICENSES)
        self.validate_prompts('License?',
                              'Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTForkVerifyLicense', '12345','0'])
        self.assertEquals(self._obj.next_state, PA_SPECIFY_LICENSES)
        self.assertEquals(self._obj._license_count, 0)
        
        #test success verify license, more licenses
        self._obj._region_config_lut[0]['max_lic_plate'] = 3
        self.set_server_response('0,\n\n')
        self._obj.next_state = None
        self._obj._license_count = 0
        self.post_dialog_responses('11111')
        self._obj.runState(PA_SPECIFY_LICENSES)
        self.validate_prompts('License?')
        self.validate_server_requests(['prTaskLUTForkVerifyLicense', '11111','0'])
        self.assertEquals(self._obj.next_state, PA_SPECIFY_LICENSES)
        self.assertEquals(self._obj._license_count, 1)
        
        #test success verify license, max licenses specified
        self._obj._region_config_lut[0]['max_lic_plate'] = 3
        self.set_server_response('0,\n\n')
        self._obj.next_state = None
        self._obj._license_count = 2
        self.post_dialog_responses('33333')
        self._obj.runState(PA_SPECIFY_LICENSES)
        self.validate_prompts('License?',
                              'getting work')
        self.validate_server_requests(['prTaskLUTForkVerifyLicense', '33333','0'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._license_count, 3)

        #test change function no
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj.next_state = None
        self._obj._license_count = 0
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(PA_SPECIFY_LICENSES)
        
        self.validate_prompts('License?',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, PA_SPECIFY_LICENSES)
        self.assertEquals(self._obj._license_count, 0)
        
        #test change function yes
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj.next_state = None
        self._obj._license_count = 0
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, PA_SPECIFY_LICENSES)
        
        self.validate_prompts('License?',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._license_count, 0)
        
        #test change region no
        self._obj.next_state = None
        self._obj._region_selected = True
        self._obj._license_count = 0
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(PA_SPECIFY_LICENSES)
        
        self.validate_prompts('License?',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PA_SPECIFY_LICENSES)
        self.assertEquals(self._obj._license_count, 0)
        
        #test change region yes
        self._obj.next_state = None
        self._obj._license_count = 0
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, PA_SPECIFY_LICENSES)
        
        self.validate_prompts('License?',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._license_count, 0)
        
    def test_get_licenses(self):
        self.start_server()

        #test error contacting host
        self.set_server_response('')
        self.post_dialog_responses('ready')
        self._obj.runState(PA_GET_LICENSES)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests(['prTaskLUTForkGetPutAway'])
        self.assertEquals(self._obj.next_state, PA_GET_LICENSES)
        
        #test error from host
        self._obj.next_state = None
        self.set_server_response('AB123,1,123,12345,PRE 1,A 1,POST 1,S 1,00,item number,description,100,15.5,1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(PA_GET_LICENSES)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTForkGetPutAway'])
        self.assertEquals(self._obj.next_state, PA_GET_LICENSES)
        
        #test warning from host
        self._obj.next_state = None
        self.set_server_response('AB123,1,123,12345,PRE 1,A 1,POST 1,S 1,00,item number,description,100,15.5,99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(PA_GET_LICENSES)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTForkGetPutAway'])
        self.assertEquals(self._obj.next_state, None)
        
        #test success from host
        self._obj.next_state = None
        self._obj._license_count = 2
        self.set_server_response('AB123,1,123,12345,PRE 1,A 1,POST 1,S 1,00,item number,description,100,15.5,0,\n\n')
        self._obj.runState(PA_GET_LICENSES)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkGetPutAway'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._license_count, 0)
        
    def test_put_licenses(self):
        self.assertRaises(Launch, self._obj.runState, PA_PUT_LICENSES)
        
if __name__ == '__main__':
    unittest.main()


