from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from forkapps.ReplenishLicensesTask import ReplenishLicensesTask, RP_GET_REPLENISHMENT_LICENSE, RP_VERIFY_RESERVE_LOCATION, RP_VERIFY_PICKUP_QUANTITY,\
                                           RP_VERIFY_LOCATION,RP_VERIFY_PUT_QUANTITY, RP_REPLENISH_LICENSE, RP_COMPLETE_LICENSE, RP_NEXT_STEP                 

from common import VoiceLinkLut
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from forkapps.VerifyLocationTask import VerifyLocationTask
from forkapps.AdditionalTasks import CancelLicense
from forkapps.ReplenishmentTask import ReplenishmentTask
import unittest

class testReplenishLicenseTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1

        temp2 = obj_factory.get(ReplenishmentTask, temp.taskRunner)
        temp.taskRunner._append(temp2)
        temp2._region_selected = True

        VoiceLinkLut.transport._timeout = 1
        self._obj = obj_factory.get(ReplenishLicensesTask,
                                      obj_factory.get(VoiceLinkLut.VoiceLinkLut, 'prTaskLUTForkReplenishmentRegionConfiguration'),
                                      temp.taskRunner, temp2)

        self._obj._region_config_lut.receive('1,replenishment region 1,0,0,0,0,0,0,5,5,2,exception location,0,0,\n'
                                             '2,replenishment region 2,0,0,0,0,0,0,5,5,2,exception location,0,0,\n'
                                             '\n')

        self._obj._get_replenishment_lut.receive('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,76,58425148,Building 2,43,Bay 76,22345,92,51528610,12,25,0,\n'
                                           '\n')
        TaskRunnerBase._main_runner = temp.taskRunner
         
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'replenishLicense')

        #test states
        self.assertEquals(self._obj.states[0], RP_GET_REPLENISHMENT_LICENSE)
        self.assertEquals(self._obj.states[1], RP_VERIFY_RESERVE_LOCATION)
        self.assertEquals(self._obj.states[2], RP_VERIFY_PICKUP_QUANTITY)
        self.assertEquals(self._obj.states[3], RP_VERIFY_LOCATION)
        self.assertEquals(self._obj.states[4], RP_VERIFY_PUT_QUANTITY)
        self.assertEquals(self._obj.states[5], RP_REPLENISH_LICENSE)
        self.assertEquals(self._obj.states[6], RP_COMPLETE_LICENSE)
        self.assertEquals(self._obj.states[7], RP_NEXT_STEP)
        
        #test LUTS defined correctly
        fields = self._obj._get_replenishment_lut.fields
        self.assertEquals(fields['errorCode'].index, 21)
        self.assertEquals(fields['errorMessage'].index, 22)
        
    def test_get_replenishment_license(self):
        self.start_server()
        
        self._obj._region = self._obj._region_config_lut[0]
        
        #test error from host
        self._obj.next_state = None
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,76,58425148,Building 2,43,Bay 76,22345,92,51528610,12,25, 1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(RP_GET_REPLENISHMENT_LICENSE)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTForkGetReplenishment'])
        self.assertEquals(self._obj.next_state, RP_GET_REPLENISHMENT_LICENSE)
        
        #test warning from host
        self._obj.next_state = None
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,76,58425148,Building 2,43,Bay 76,22345,92,51528610,12,25,99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(RP_GET_REPLENISHMENT_LICENSE)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTForkGetReplenishment'])
        self.assertEquals(self._obj.next_state, None)

        #test success from host, and location information
        self._obj.next_state = None
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,76,58425148,Building 2,43,Bay 76,22345,92,51528610,12,25,0,\n\n')
        self._obj.runState(RP_GET_REPLENISHMENT_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkGetReplenishment'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._loc_info.curr_aisle, '24')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'Bay 23')
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_post_aisle, '')

        self.assertEquals(self._obj._dest_loc_info.curr_pre_aisle, 'Building 2')
        self.assertEquals(self._obj._dest_loc_info.curr_aisle, '43')
        self.assertEquals(self._obj._dest_loc_info.curr_post_aisle, 'Bay 76')
        self.assertEquals(self._obj._dest_loc_info.prev_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._dest_loc_info.prev_aisle, '')
        self.assertEquals(self._obj._dest_loc_info.prev_post_aisle, '')

        #test success from host, and location information
        self._obj.next_state = None
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,76,58425148,Building 1,43,Bay 76,22345,92,51528610,12,25,0,\n\n')
        self._obj.runState(RP_GET_REPLENISHMENT_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkGetReplenishment'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._loc_info.curr_aisle, '24')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'Bay 23')
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_post_aisle, '')

        self.assertEquals(self._obj._dest_loc_info.curr_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._dest_loc_info.curr_aisle, '43')
        self.assertEquals(self._obj._dest_loc_info.curr_post_aisle, 'Bay 76')
        self.assertEquals(self._obj._dest_loc_info.prev_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._dest_loc_info.prev_aisle, '24')
        self.assertEquals(self._obj._dest_loc_info.prev_post_aisle, '')
        
        #test success from host, and location information
        self._obj.next_state = None
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,76,58425148,Building 1,24,Bay 76,22345,92,51528610,12,25,0,\n\n')
        self._obj.runState(RP_GET_REPLENISHMENT_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkGetReplenishment'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._loc_info.curr_aisle, '24')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'Bay 23')
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_post_aisle, '')

        self.assertEquals(self._obj._dest_loc_info.curr_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._dest_loc_info.curr_aisle, '24')
        self.assertEquals(self._obj._dest_loc_info.curr_post_aisle, 'Bay 76')
        self.assertEquals(self._obj._dest_loc_info.prev_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._dest_loc_info.prev_aisle, '24')
        self.assertEquals(self._obj._dest_loc_info.prev_post_aisle, 'Bay 23')
        
        #test success from host, and location information
        self._obj.next_state = None
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,76,58425148,Building 2,24,Bay 76,22345,92,51528610,12,25,0,\n\n')
        self._obj.runState(RP_GET_REPLENISHMENT_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkGetReplenishment'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._loc_info.curr_aisle, '24')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'Bay 23')
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_aisle, '')
        self.assertEquals(self._obj._loc_info.prev_post_aisle, '')

        self.assertEquals(self._obj._dest_loc_info.curr_pre_aisle, 'Building 2')
        self.assertEquals(self._obj._dest_loc_info.curr_aisle, '24')
        self.assertEquals(self._obj._dest_loc_info.curr_post_aisle, 'Bay 76')
        self.assertEquals(self._obj._dest_loc_info.prev_pre_aisle, 'Building 1')
        self.assertEquals(self._obj._dest_loc_info.prev_aisle, '')
        self.assertEquals(self._obj._dest_loc_info.prev_post_aisle, '')
        
        
    def test_verify_reserve_location(self):
        self._obj._region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._get_replenishment_lut[0]
        self.assertRaises(Launch, self._obj.runState, RP_VERIFY_RESERVE_LOCATION)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, VerifyLocationTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putAwayVerifyLocation')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, RP_VERIFY_PICKUP_QUANTITY)    
    
    def test_verify_pickup_quantity(self):
        self._obj._region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._get_replenishment_lut[0]

        #test capture quantity off
        self._obj._region['capture_pickup_quantity'] = 0
        self._obj._current_license['quantity'] = 99
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self._obj.runState(RP_VERIFY_PICKUP_QUANTITY)
        self.validate_prompts()
        self.assertEquals(self._obj._pick_up_qty, 99)
        self.assertEquals(self._obj._put_qty, 99)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 99)
        self.assertEquals(self._obj.next_state, None)

        #---------------------------------------------------------------------
        #test capture quantity on, known quantity 0
        self._obj._region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 0
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('11!', 'yes')
        self._obj.runState(RP_VERIFY_PICKUP_QUANTITY)
        self.validate_prompts('quantity',
                              '11, correct?')
        self.assertEquals(self._obj._pick_up_qty, 11)
        self.assertEquals(self._obj._put_qty, 11)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 11)
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter exact
        self._obj._region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('11!')
        self._obj.runState(RP_VERIFY_PICKUP_QUANTITY)
        self.validate_prompts('quantity')
        self.assertEquals(self._obj._pick_up_qty, 11)
        self.assertEquals(self._obj._put_qty, 11)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 11)
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter more
        self._obj._region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('12!', 'yes')
        self.assertRaises(Launch, self._obj.runState, RP_VERIFY_PICKUP_QUANTITY)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, CancelLicense))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'cancelLicense')
        self.assertEquals(self._obj.current_state, RP_NEXT_STEP)    
        self.validate_prompts('quantity','You said 12, expected 11, correct?', 
                              'Canceling replenishment')
    
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter more
        self._obj._region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('12!', 'no')
        self._obj.runState(RP_VERIFY_PICKUP_QUANTITY)
        self.validate_prompts('quantity',
                              'You said 12, expected 11, correct?')
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PICKUP_QUANTITY)
        
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter more, override not allowed
        self._obj.next_state = None
        self._obj._region['capture_pickup_quantity'] = 1
        self._obj._region['allow_override_quantity'] = 0
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('12!', 'yes')
        self.assertRaises(Launch, self._obj.runState, RP_VERIFY_PICKUP_QUANTITY)
        self.validate_prompts('quantity',
                              'You said 12, expected 11, correct?',
                              'Canceling replenishment')
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 0)
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------------------
        #test capture quantity on, qty 0
        self._obj.next_state = None
        self._obj._region['capture_pickup_quantity'] = 1
        self._obj._region['allow_override_quantity'] = 0
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('0!')
        self._obj.runState(RP_VERIFY_PICKUP_QUANTITY)
        self.validate_prompts('quantity',
                              'You must enter a value greater than zero')
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PICKUP_QUANTITY)
    
    def test_verify_location(self):
        self._obj._region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._get_replenishment_lut[0]
        self.assertRaises(Launch, self._obj.runState, RP_VERIFY_LOCATION)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, VerifyLocationTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'putAwayVerifyLocation')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, RP_VERIFY_PUT_QUANTITY)  
    
    def test_verify_put_quantity(self):
        self._obj._region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._get_replenishment_lut[0]

        #test capture put quantity off, partial not spoken
        self._obj._region['capture_replenished_quantity'] = 0
        self._obj._current_license['quantity'] = 99
        self._obj._pick_up_qty = 99
        self._obj._put_qty = 0
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts()
        self.assertEquals(self._obj._put_qty, 99)
        self.assertEquals(self._obj.next_state, None)
        
        #test partial spoken, no to verify
        self._obj._put_qty = 0
        self._obj._region['allow_partial_put'] = 1
        self._obj._dest_loc_info.partial_spoken = True
        self.post_dialog_responses('10!', 'no')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              '10, correct?')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PUT_QUANTITY)
        
        #test partial spoken, too much
        self._obj.next_state = None
        self.post_dialog_responses('150!')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              'You said 150, expected 99')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PUT_QUANTITY)
        
        #test partial spoken, yes to verify
        self._obj.next_state = None
        self.post_dialog_responses('15!', 'yes')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              '15, correct?')
        self.assertEquals(self._obj._put_qty, 15)
        self.assertEquals(self._obj.next_state, None)
        
        #test capture qty on, exact qty spoken
        self._obj._put_qty = 0
        self._obj._region['capture_replenished_quantity'] = 1
        self._obj._dest_loc_info.partial_spoken = False
        self.post_dialog_responses('99!')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity')
        self.assertEquals(self._obj._put_qty, 99)
        self.assertEquals(self._obj.next_state, None)
        
        #test capture qty on, more qty spoken
        self._obj._put_qty = 0
        self._obj._region['capture_replenished_quantity'] = 1
        self._obj._dest_loc_info.partial_spoken = False
        self.post_dialog_responses('199!')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              'You said 199, expected 99')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PUT_QUANTITY)
        
        #test capture qty on, less qty spoken, no to verify
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._region['capture_replenished_quantity'] = 1
        self._obj._dest_loc_info.partial_spoken = False
        self.post_dialog_responses('19!', 'no')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              'You said 19, expected 99, is this a partial?')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PUT_QUANTITY)
        
        #test capture qty on, less qty spoken, yes to verify
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._region['capture_replenished_quantity'] = 1
        self._obj._dest_loc_info.partial_spoken = False
        self.post_dialog_responses('19!', 'yes')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              'You said 19, expected 99, is this a partial?')
        self.assertEquals(self._obj._put_qty, 19)
        self.assertEquals(self._obj.next_state, None)
        
        #test capture qty on, less qty spoken, partial not allowed
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._region['capture_replenished_quantity'] = 1
        self._obj._region['allow_partial_put'] = 0
        self._obj._dest_loc_info.partial_spoken = False
        self.post_dialog_responses('19!')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              'You said 19, expected 99')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PUT_QUANTITY)
        
        #test capture qty on, 0 qty spoken
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._dest_loc_info.partial_spoken = False
        self.post_dialog_responses('0!')
        self._obj.runState(RP_VERIFY_PUT_QUANTITY)
        self.validate_prompts('quantity',
                              'You must enter a value greater than zero')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, RP_VERIFY_PUT_QUANTITY)
        
    def test_replenish_license(self):
        self._obj._region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._get_replenishment_lut[0]
        self._obj._dest_loc_info.set_destination_from_replenish_license(self._obj._current_license)
        self._obj._dest_loc_info.set_replenishment_region_settings(self._obj._region)
        self._obj._start_time = 'start time'
        
        self._obj._pick_up_qty = 10
        self._obj._put_qty = 10
        
        #test out of range
        self.post_dialog_responses('ready')
        self._obj.runState(RP_REPLENISH_LICENSE)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, RP_REPLENISH_LICENSE)
  
        #test error
        self.start_server()
        self._obj.next_state = None
        self.set_server_response('1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(RP_REPLENISH_LICENSE)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTForkReplenishmentLicense', '115647','10','12','1','','start time'])
        self.assertEquals(self._obj.next_state, RP_REPLENISH_LICENSE)
        
        #test warning
        self._obj.next_state = None
        self.set_server_response('99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(RP_REPLENISH_LICENSE)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTForkReplenishmentLicense', '115647','10','12','1','','start time'])
        self.assertEquals(self._obj.next_state, None)
        
        #test partial
        self._obj._put_qty = 5
        self.set_server_response('0,\n\n')
        self._obj.runState(RP_REPLENISH_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkReplenishmentLicense', '115647','5','12','0','','start time'])
        self.assertEquals(self._obj.next_state, None)
        
        #test unknown pickup partial spoken
        self._obj._pick_up_qty = 0
        self._obj._dest_loc_info.partial_spoken = True
        self.set_server_response('0,\n\n')
        self._obj.runState(RP_REPLENISH_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkReplenishmentLicense', '115647','5','12','0','','start time'])
        self.assertEquals(self._obj.next_state, None)
        
        #test unknown pickup partial not spoken
        self._obj._pick_up_qty = 0
        self._obj._dest_loc_info.partial_spoken = False
        self.set_server_response('0,\n\n')
        self._obj.runState(RP_REPLENISH_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkReplenishmentLicense', '115647','5','12','1','','start time'])
        self.assertEquals(self._obj.next_state, None)
        
    
    def test_complete(self):
        #Test not in partial
        self._obj._partial = False
        self.post_dialog_responses('ready')
        self._obj.runState(RP_COMPLETE_LICENSE)
        self.validate_prompts('Complete. Say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #Test not all put away
        self._obj._partial = True
        self._obj.runState(RP_COMPLETE_LICENSE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        self.start_server()
        #Test change function, no
        self._obj._partial = False
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(RP_COMPLETE_LICENSE)
        
        self.validate_prompts('Complete. Say ready.',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, RP_COMPLETE_LICENSE)

        #Test change function, yes
        self._obj._partial = False
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, RP_COMPLETE_LICENSE)
        
        self.validate_prompts('Complete. Say ready.',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #Test change region, no
        self._obj._partial = False
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(RP_COMPLETE_LICENSE)
        
        self.validate_prompts('Complete. Say ready.',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, RP_COMPLETE_LICENSE)

        #Test change function, yes
        self._obj._partial = False
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, RP_COMPLETE_LICENSE)
        
        self.validate_prompts('Complete. Say ready.',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_next_step(self):
        # test complete
        self._obj._region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._get_replenishment_lut[0]

        self._obj._pick_up_qty = 5
        self._obj._put_qty = 3
        
        self.start_server()
        self.set_server_response('ALT PRE 1,ALT A 1,ALT POST 1,ALT S 1,99,ALTSCAN,Alt Location,0,\n\n')
        self._obj.next_state = None
        self._obj.runState(RP_NEXT_STEP)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkGetAlternatePutLocation','2','','','2'])
        self.assertEquals(self._obj._pick_up_qty, 2)
        self.assertEquals(self._obj._put_qty, 2)
        self.assertEquals(self._obj._loc_info.partial_spoken, False)
        self.assertEquals(self._obj.next_state, RP_VERIFY_LOCATION)
        self.assertEquals(self._obj._dest_loc_info.curr_pre_aisle, 'ALT PRE 1')
        self.assertEquals(self._obj._dest_loc_info.curr_aisle, 'ALT A 1')
        self.assertEquals(self._obj._dest_loc_info.curr_post_aisle, 'ALT POST 1')
        self.assertEquals(self._obj._dest_loc_info.curr_slot, 'ALT S 1')
        self.assertEquals(self._obj._dest_loc_info.curr_cd, '99')
        self.assertEquals(self._obj._dest_loc_info.curr_scan_value, 'ALTSCAN')
        self.assertEquals(self._obj._dest_loc_info.curr_location_id, 'Alt Location')
        
        self._obj._pick_up_qty = 3
        self._obj._put_qty = 3
        self._obj.next_state = None
        self._obj.runState(RP_NEXT_STEP)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
      
      
if __name__ == '__main__':
    unittest.main()
