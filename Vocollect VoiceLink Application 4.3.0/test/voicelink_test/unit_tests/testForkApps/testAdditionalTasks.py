from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from forkapps.AdditionalTasks import CancelLicense, TRANSMIT_REASON_CODES,\
    SELECT_REASON, DELIVER_LICENSE, TRANSMIT_PUT_LICENSE, COMMAND_COMPLETE,\
    ReleaseLicense
from core.VoiceLink import VoiceLink
from common import VoiceLinkLut
from vocollect_core.task.task_runner import Launch
import unittest

class testCancelLicense(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(CancelLicense,
                                      1, '12345','12345','10', 'start_time', 
                                      'exception location', 
                                      obj_factory.get(VoiceLinkLut.VoiceLinkLut, 'prTaskLUTForkPutAwayLicense'), 
                                      'returnto_task', 
                                      'returnto_state', 
                                      VoiceLink())
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'cancelLicense')

        #test states
        self.assertEquals(self._obj.states[0], TRANSMIT_REASON_CODES)
        self.assertEquals(self._obj.states[1], SELECT_REASON)
        self.assertEquals(self._obj.states[2], DELIVER_LICENSE)
        self.assertEquals(self._obj.states[3], TRANSMIT_PUT_LICENSE)
        self.assertEquals(self._obj.states[4], COMMAND_COMPLETE)
        
        #test LUTS defined correctly
        fields = self._obj._reason_codes_lut.fields
        self.assertEquals(fields['code'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)

    def test_transmit_reason_codes(self):
        #test out of range
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_REASON_CODES)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, TRANSMIT_REASON_CODES)

        self.start_server()
        #test error message
        self._obj.next_state = None
        self.set_server_response(',,1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_REASON_CODES)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes','1','1'])
        self.assertEquals(self._obj.next_state, TRANSMIT_REASON_CODES)
        
        #test warning message
        self._obj.next_state = None
        self.set_server_response(',,99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_REASON_CODES)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes','1','1'])
        self.assertEquals(self._obj.next_state, None)

        #test warning message
        self._obj.next_state = None
        self.set_server_response('1,reason 1,0,\n\n')
        self._obj.runState(TRANSMIT_REASON_CODES)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes','1','1'])
        self.assertEquals(self._obj.next_state, None)

    def test_select_reason(self):
        
        #test no reason returned
        self._obj._reason_codes_lut.receive(',,0,\n\n')
        self._obj.runState(SELECT_REASON)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason, None)
        
        #test reason codes returned
        self._obj._reason_codes_lut.receive('1,reason 1,0,\n'
                                            '2,reason 2,0,\n'
                                            '\n')
        self.post_dialog_responses('description',
                                   'ready',
                                   'ready',
                                   '1',
                                   'yes')
        self._obj.runState(SELECT_REASON)
        self.validate_prompts('Reason?',
                              '1, reason 1',
                              '2, reason 2',
                              'Reason?',
                              '<spell>1</spell>, reason 1, correct?')
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason, '1')
    
    def test_deliver_license(self):
        #test with delivery location
        self.post_dialog_responses('ready')
        self._obj.runState(DELIVER_LICENSE)
        self.validate_prompts('Deliver to exception location')
        self.assertEquals(self._obj.next_state, None)
        
        #test blank location
        self._obj._deliver_location = ''
        self._obj.runState(DELIVER_LICENSE)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
        #test None location
        self._obj._deliver_location = None
        self._obj.runState(DELIVER_LICENSE)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
    def test_transmit_put_license(self):
        #test out of range
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, TRANSMIT_PUT_LICENSE)
        
        self.start_server()
        #test error returned
        self._obj.next_state = None
        self.set_server_response('1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense','12345','10','','2','','start_time'])
        self.assertEquals(self._obj.next_state, TRANSMIT_PUT_LICENSE)
        
        #test warning returned
        self._obj.next_state = None
        self.set_server_response('99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense','12345','10','','2','','start_time'])
        self.assertEquals(self._obj.next_state, None)
        
        #test normal response
        self._obj.next_state = None
        self._obj._reason = '99'
        self.set_server_response('0,\n\n')
        self.post_dialog_responses()
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense','12345','10','','2','99','start_time'])
        self.assertEquals(self._obj.next_state, None)

    def test_command_complete(self):
        self.post_dialog_responses('ready')
        self.assertRaises(Launch, self._obj.runState, COMMAND_COMPLETE)
        self.validate_prompts('1 2 3 4 5 canceled, say ready')
        self.validate_server_requests()
        
        
class testReleaseLicense(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(ReleaseLicense,
                                      '12345', 
                                      'start_time', 
                                      obj_factory.get(VoiceLinkLut.VoiceLinkLut, 'prTaskLUTForkPutAwayLicense'), 
                                      'returnto_task', 
                                      'returnto_state', 
                                      VoiceLink())
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'releaseLicense')

        #test states
        self.assertEquals(self._obj.states[0], TRANSMIT_PUT_LICENSE)
        self.assertEquals(self._obj.states[1], COMMAND_COMPLETE)

    def test_transmit_put_license(self):
        #test out of range
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, TRANSMIT_PUT_LICENSE)
        
        self.start_server()
        #test error returned
        self._obj.next_state = None
        self.set_server_response('1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense','12345','0','','3','','start_time'])
        self.assertEquals(self._obj.next_state, TRANSMIT_PUT_LICENSE)
        
        #test warning returned
        self._obj.next_state = None
        self.set_server_response('99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense','12345','0','','3','','start_time'])
        self.assertEquals(self._obj.next_state, None)
        
        #test normal response
        self._obj.next_state = None
        self.set_server_response('0,\n\n')
        self.post_dialog_responses()
        self._obj.runState(TRANSMIT_PUT_LICENSE)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense','12345','0','','3','','start_time'])
        self.assertEquals(self._obj.next_state, None)

    def test_command_complete(self):
        
        self.post_dialog_responses('ready')
        self.assertRaises(Launch, self._obj.runState, COMMAND_COMPLETE)
        self.validate_prompts('1 2 3 4 5 released, say ready')
        self.validate_server_requests()
        
if __name__ == '__main__':
    unittest.main()


