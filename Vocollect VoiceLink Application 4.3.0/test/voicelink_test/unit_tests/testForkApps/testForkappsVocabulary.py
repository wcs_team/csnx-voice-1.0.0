from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from forkapps.PutawayLicensesTask import PutawayLicensesTask, PA_INITIALIZE_LICENSE
from common import VoiceLinkLut
from forkapps.AdditionalTasks import CancelLicense
from forkapps.VerifyLocationTask import VerifyLocationTask, CONFIRM_LOCATION,\
    Location, PROMPT_PUT_LOCATION, PROMPT_START_LOCATION, GET_LOCATION
from vocollect_core.task.task_runner import Launch
import unittest


class testForkAppsVocabulary(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(PutawayLicensesTask,
                                      obj_factory.get(VoiceLinkLut.VoiceLinkLut, 'prTaskLUTForkPutAwayRegionConfiguration'),
                                      obj_factory.get(VoiceLinkLut.VoiceLinkLut, 'prTaskLUTForkGetPutAway'),
                                      VoiceLink())
        self._obj.taskRunner._append(self._obj)

        self._obj._region_config_lut.receive('1,put away region 1,0,1,1,1,1,1,0,0,5,5,5,2,1,exception location,0,0,\n'
                                             '\n')

        self._obj._licenses_lut.receive('AB101,1,123,12345,PRE 1,A 1,POST 1,S 1,00,item number,description,100,15.5,99,warning message\n'
                                        '\n')
        

    def test_valid(self):
        
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('license'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('goal time'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('release license'), False)

        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('license'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('goal time'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('release license'), True)
        
        #check release license
        self._obj.dynamic_vocab.release_return_state = None
        self._obj.dynamic_vocab.release_return_task = None
        self.assertEqual(self._obj.dynamic_vocab._valid('release license'), False)
        
        #check cancel and description
        cancel_task = CancelLicense(1, 'license_id', 'quantity', 'start_time', 
                                    'deliver_location', 'put_lut', 'returnto_task', 
                                    'returnto_state', self._obj.taskRunner)
        self._obj.taskRunner._insert(cancel_task)
        
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self._obj.taskRunner.task_stack.pop(0)

        self._obj.dynamic_vocab.cancel_return_state = None
        self._obj.dynamic_vocab.cancel_return_task = None
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), True)

        self._obj.dynamic_vocab.cancel_return_state = ''
        self._obj.dynamic_vocab.cancel_return_task = ''
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), True)
        self._obj.dynamic_vocab.license = None
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), False)
        self._obj.dynamic_vocab.license = ''
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), True)
        temp = self._obj.dynamic_vocab.location
        self._obj.dynamic_vocab.location = None
        self.assertEqual(self._obj.dynamic_vocab._valid('cancel'), False)
        self._obj.dynamic_vocab.location = temp

        #check partial
        loc_task = VerifyLocationTask(Location(1, PROMPT_PUT_LOCATION), 
                                      self._obj.taskRunner)
        loc_task.current_state = CONFIRM_LOCATION
        self._obj.taskRunner._insert(loc_task)
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), True)
        self._obj.dynamic_vocab.location.curr_slot = ''
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), False)
        loc_task.current_state = GET_LOCATION
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), True)

        self._obj.dynamic_vocab.location.curr_slot = '1'
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), False)
        loc_task.current_state = CONFIRM_LOCATION
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), True)

        self._obj.dynamic_vocab.location.prompt_type = PROMPT_START_LOCATION
        self.assertEqual(self._obj.dynamic_vocab._valid('partial'), False)
    
    def test_item_number(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('item number'))
        self.validate_prompts('item number')
        
        self._obj.dynamic_vocab.item_number = ''
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('item number'))
        self.validate_prompts('item number not available')
    
    def test_description(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('description'))
        self.validate_prompts('description')
        
        self._obj.dynamic_vocab.description = ''
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('description'))
        self.validate_prompts('description not available')
    
    def test_license(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('license'))
        self.validate_prompts('A B 1 0 1')
        
        self._obj.dynamic_vocab.license = ''
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('license'))
        self.validate_prompts('license not available')
    
    def test_location(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('location'))
        self.validate_prompts('PRE 1, Aisle <spell>A 1</spell>, POST 1, Slot <spell>S 1</spell>')
        
        self._obj.dynamic_vocab.location.curr_aisle = ''
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('location'))
        self.validate_prompts('PRE 1, POST 1, Slot <spell>S 1</spell>')
        
        self._obj.dynamic_vocab.location.curr_aisle = 'A 1'
        self._obj.dynamic_vocab.location.curr_slot = ''
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('location'))
        self.validate_prompts('PRE 1, Aisle <spell>A 1</spell>, POST 1, Location not available')

        self._obj.dynamic_vocab.location.curr_aisle = ''
        self._obj.dynamic_vocab.location.curr_slot = ''
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('location'))
        self.validate_prompts('PRE 1, POST 1, Location not available')

        self._obj.dynamic_vocab.location.curr_post_aisle = ''
        self._obj.dynamic_vocab.location.curr_pre_aisle = ''
        self._obj.dynamic_vocab.location.curr_aisle = ''
        self._obj.dynamic_vocab.location.curr_slot = ''
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('location'))
        self.validate_prompts(', , Location not available')

    
    def test_quantity(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('quantity'))
        self.validate_prompts('100')
        
        self._obj.dynamic_vocab.quantity = 0
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('quantity'))
        self.validate_prompts('quantity not available')

    def test_goal_time(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('goal time'))
        self.validate_prompts('15.5')
        
        self._obj.dynamic_vocab.goal_time = 0
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('goal time'))
        self.validate_prompts('goal time not available')
        
    def test_partial(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)
        
        self._obj.dynamic_vocab.partial_allowed = False
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('partial'))
        self.validate_prompts('partial not allowed in this region')
        self.assertFalse(self._obj.dynamic_vocab.vocabs['partial'].skip_prompt)
        self.assertFalse(self._obj.dynamic_vocab.location.partial_spoken)
        
        self._obj.dynamic_vocab.partial_allowed = True
        self.post_dialog_responses('no')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('partial'))
        self.validate_prompts('partial, correct?')
        self.assertFalse(self._obj.dynamic_vocab.vocabs['partial'].skip_prompt)
        self.assertFalse(self._obj.dynamic_vocab.location.partial_spoken)
        
        loc_task = VerifyLocationTask(Location(1, PROMPT_PUT_LOCATION), 
                                      self._obj.taskRunner)
        loc_task.current_state = CONFIRM_LOCATION
        self._obj.taskRunner._insert(loc_task)

        self.post_dialog_responses('yes')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('partial'))
        self.validate_prompts('partial, correct?',
                              'confirm location')
        self.assertTrue(self._obj.dynamic_vocab.vocabs['partial'].skip_prompt)
        self.assertTrue(self._obj.dynamic_vocab.location.partial_spoken)
        
        loc_task.current_state = GET_LOCATION
        
        self.post_dialog_responses('yes')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('partial'))
        self.validate_prompts('partial, correct?',
                              'Location?')
        self.assertTrue(self._obj.dynamic_vocab.vocabs['partial'].skip_prompt)
        self.assertTrue(self._obj.dynamic_vocab.location.partial_spoken)
        
        
    def test_cancel(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)

        self._obj.dynamic_vocab.cancel_allowed = False
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('cancel'))
        self.validate_prompts('Cancel not allowed in this region')
        
        self._obj.dynamic_vocab.cancel_allowed = True
        self.post_dialog_responses('no')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('cancel'))
        self.validate_prompts('Cancel, correct?')
        
        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'cancel')
        self.validate_prompts('Cancel, correct?')
    
    def test_release_license(self):
        self._obj.runState(PA_INITIALIZE_LICENSE)

        self._obj.dynamic_vocab.release_allowed = False
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('release license'))
        self.validate_prompts('Release License not allowed in this region')

        self._obj.dynamic_vocab.release_allowed = True
        self.post_dialog_responses('no')
        self.assertTrue(self._obj.dynamic_vocab.execute_vocab('release license'))
        self.validate_prompts('Release License, correct?')

        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'release license')
        self.validate_prompts('Release License, correct?')
        
        
        
        
        
if __name__ == '__main__':
    unittest.main()

