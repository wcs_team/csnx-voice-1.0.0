from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from common import VoiceLinkLut
from forkapps.VerifyLocationTask import VerifyLocationTask, Location, \
    PROMPT_PICKUP_LOCATION, PROMPT_PUT_LOCATION, PROMPT_START_LOCATION,\
    DIRECT_PREAISLE, DIRECT_AISLE, DIRECT_POSTAISLE, GET_LOCATION,\
    CONFIRM_LOCATION, TRANSMIT_LOCATION
import unittest

class testVerifyLocation(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        VoiceLinkLut.transport._timeout = 1
        location_info = obj_factory.get(Location, 1, PROMPT_PICKUP_LOCATION)
        
        location_info.check_digit_length = 2
        location_info.location_length = 5
        location_info.allow_override = False
        location_info.partial_spoken = False
        location_info.quantity = 0
        location_info.verify_location = False
        location_info.curr_pre_aisle = 'Pre Aisle'
        location_info.curr_aisle = 'Aisle'
        location_info.curr_post_aisle = 'Post Aisle'
        location_info.curr_slot = 'Slot'
        location_info.curr_cd = '00'
        location_info.curr_scan_value = '12345'
        location_info.curr_location_id = 'location id'
        location_info.license_id = '12345'
        
        self._obj = VerifyLocationTask(location_info, VoiceLink())
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putAwayVerifyLocation')

        #test states
        self.assertEquals(self._obj.states[0], DIRECT_PREAISLE)
        self.assertEquals(self._obj.states[1], DIRECT_AISLE)
        self.assertEquals(self._obj.states[2], DIRECT_POSTAISLE)
        self.assertEquals(self._obj.states[3], GET_LOCATION)
        self.assertEquals(self._obj.states[4], CONFIRM_LOCATION)
        self.assertEquals(self._obj.states[5], TRANSMIT_LOCATION)
        
        #test LUTS defined correctly
        fields = self._obj._verify_location_lut.fields
        self.assertEquals(fields['locationID'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
    def test_direct_preaisle(self):
        #test no pre_aisle for current location
        self._obj._loc_info.prev_pre_aisle = 'Prev Pre Aisle'
        self._obj._loc_info.curr_pre_aisle = ''
        self._obj._loc_info.curr_slot = 'Slot'
        
        self._obj.runState(DIRECT_PREAISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, 'Prev Pre Aisle')
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, '')
        self.assertEquals(self._obj.next_state, None)
        
        #test same as prev pre aisle
        self._obj._loc_info.prev_pre_aisle = 'Pre Aisle'
        self._obj._loc_info.curr_pre_aisle = 'Pre Aisle'
        self._obj._loc_info.curr_slot = 'Slot'
        
        self._obj.runState(DIRECT_PREAISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, 'Pre Aisle')
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'Pre Aisle')
        self.assertEquals(self._obj.next_state, None)
        
        #test no slot specified
        self._obj._loc_info.prev_pre_aisle = ''
        self._obj._loc_info.curr_pre_aisle = 'Pre Aisle'
        self._obj._loc_info.curr_slot = ''
        
        self._obj.runState(DIRECT_PREAISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, '')
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'Pre Aisle')
        self.assertEquals(self._obj.next_state, None)
        
        #test different pre aisle
        self._obj._loc_info.prev_pre_aisle = 'Prev Pre Aisle'
        self._obj._loc_info.curr_pre_aisle = 'Pre Aisle'
        self._obj._loc_info.prev_aisle = 'Aisle'
        self._obj._loc_info.prev_post_aisle = 'Post Aisle'
        self._obj._loc_info.curr_slot = 'Slot'
        
        self.post_dialog_responses('ready')
        self._obj.runState(DIRECT_PREAISLE)
        self.validate_prompts('Pre Aisle')
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_pre_aisle, 'Prev Pre Aisle')
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'Pre Aisle')
        self.assertEquals(self._obj._loc_info.prev_aisle, 'Aisle')
        self.assertEquals(self._obj._loc_info.prev_post_aisle, 'Post Aisle')
        self.assertEquals(self._obj.next_state, None)
        
    def test_direct_aisle(self):
        #test no pre_aisle for current location
        self._obj._loc_info.prev_aisle = 'Prev Aisle'
        self._obj._loc_info.curr_aisle = ''
        self._obj._loc_info.curr_slot = 'Slot'
        
        self._obj.runState(DIRECT_AISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_aisle, 'Prev Aisle')
        self.assertEquals(self._obj._loc_info.curr_aisle, '')
        self.assertEquals(self._obj.next_state, None)
        
        #test same as prev pre aisle
        self._obj._loc_info.prev_aisle = 'Aisle'
        self._obj._loc_info.curr_aisle = 'Aisle'
        self._obj._loc_info.curr_slot = 'Slot'
        
        self._obj.runState(DIRECT_AISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_aisle, 'Aisle')
        self.assertEquals(self._obj._loc_info.curr_aisle, 'Aisle')
        self.assertEquals(self._obj.next_state, None)
        
        #test no slot specified
        self._obj._loc_info.prev_aisle = ''
        self._obj._loc_info.curr_aisle = 'Aisle'
        self._obj._loc_info.curr_slot = ''
        
        self._obj.runState(DIRECT_AISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_aisle, '')
        self.assertEquals(self._obj._loc_info.curr_aisle, 'Aisle')
        self.assertEquals(self._obj.next_state, None)
        
        #test different aisle
        self._obj._loc_info.prev_aisle = 'Prev Aisle'
        self._obj._loc_info.curr_aisle = 'Aisle'
        self._obj._loc_info.prev_post_aisle = 'Post Aisle'
        self._obj._loc_info.curr_slot = 'Slot'
        
        self.post_dialog_responses('ready')
        self._obj.runState(DIRECT_AISLE)
        self.validate_prompts('Aisle Aisle')
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_aisle, 'Prev Aisle')
        self.assertEquals(self._obj._loc_info.curr_aisle, 'Aisle')
        self.assertEquals(self._obj._loc_info.prev_post_aisle, 'Post Aisle')
        self.assertEquals(self._obj.next_state, None)
        
    def test_direct_post_aisle(self):
        #test no pre_aisle for current location
        self._obj._loc_info.prev_post_aisle = 'Post Aisle'
        self._obj._loc_info.curr_post_aisle = ''
        self._obj._loc_info.curr_slot = 'Slot'
        
        self._obj.runState(DIRECT_POSTAISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_post_aisle, 'Post Aisle')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, '')
        self.assertEquals(self._obj.next_state, None)
        
        #test same as prev pre aisle
        self._obj._loc_info.prev_post_aisle = 'Post Aisle'
        self._obj._loc_info.curr_post_aisle = 'Post Aisle'
        self._obj._loc_info.curr_slot = 'Slot'
        
        self._obj.runState(DIRECT_POSTAISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_post_aisle, 'Post Aisle')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'Post Aisle')
        self.assertEquals(self._obj.next_state, None)
        
        #test no slot specified
        self._obj._loc_info.prev_post_aisle = ''
        self._obj._loc_info.curr_post_aisle = 'Post Aisle'
        self._obj._loc_info.curr_slot = ''
        
        self._obj.runState(DIRECT_POSTAISLE)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_post_aisle, '')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'Post Aisle')
        self.assertEquals(self._obj.next_state, None)
        
        #test different aisle
        self._obj._loc_info.prev_post_aisle = 'Prev Post Aisle'
        self._obj._loc_info.curr_post_aisle = 'Post Aisle'
        self._obj._loc_info.curr_slot = 'Slot'
        
        self.post_dialog_responses('ready')
        self._obj.runState(DIRECT_POSTAISLE)
        self.validate_prompts('Post Aisle')
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.prev_post_aisle, 'Prev Post Aisle')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'Post Aisle')
        self.assertEquals(self._obj.next_state, None)
        
    def test_get_location(self):
        #test slot/location given 
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj.runState(GET_LOCATION)
        self.validate_prompts()
        self.assertEquals(self._obj.next_state, None)
        
        #test location, no verify
        self._obj._loc_info.curr_slot = ''
        self._obj._loc_info.prompt_type = PROMPT_START_LOCATION
        self._obj._loc_info.verify_location = False
        self.post_dialog_responses('12345!')
        self._obj.runState(GET_LOCATION)
        self.validate_prompts('Start location?')
        self.assertEquals(self._obj._location, '12345')
        self.assertEquals(self._obj._scanned, False)
        self.assertEquals(self._obj.next_state, None)
        
        #test location, verify
        self._obj._loc_info.curr_slot = ''
        self._obj._loc_info.prompt_type = PROMPT_START_LOCATION
        self._obj._loc_info.verify_location = True
        self.post_dialog_responses('12345!', 'yes')
        self._obj.runState(GET_LOCATION)
        self.validate_prompts('Start location?',
                              '12345, correct?')
        self.assertEquals(self._obj._location, '12345')
        self.assertEquals(self._obj._scanned, False)
        self.assertEquals(self._obj.next_state, None)
        
        #test location, verify, scanned
       
        self._obj._loc_info.curr_slot = ''
        self._obj._loc_info.prompt_type = PROMPT_START_LOCATION
        self._obj._loc_info.verify_location = True
        self.post_dialog_responses('#1234567890')
        self._obj.runState(GET_LOCATION)
        self.validate_prompts('Start location?')
        self.assertEquals(self._obj._location, '1234567890')
        self.assertEquals(self._obj._scanned, True)
        self.assertEquals(self._obj.next_state, TRANSMIT_LOCATION)
        
        #test not start location
        self._obj._loc_info.curr_slot = ''
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PICKUP_LOCATION
        self._obj._loc_info.verify_location = False
        self.post_dialog_responses('12345!')
        self._obj.runState(GET_LOCATION)
        self.validate_prompts('Location?')
        self.assertEquals(self._obj._location, '12345')
        self.assertEquals(self._obj._scanned, False)
        self.assertEquals(self._obj.next_state, None)
        
    def test_get_check_digits(self):
        #test getting check digits when not slot (operator directed)
        self._obj._loc_info.curr_slot = ''
        self.post_dialog_responses('00')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('check digits?')
        self.assertEquals(self._obj._check_digits, '00')
        
        #test when no scan or check digits specified
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = ''
        self._obj._loc_info.curr_scan_value = ''
        self._obj._check_digits = None
        self.post_dialog_responses('ready')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        #test when scan value with no check digits specified, speak ready
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = ''
        self._obj._loc_info.curr_scan_value = 'scan value'
        self._obj._loc_info.quantity = 10
        self._obj._check_digits = None
        self.post_dialog_responses('ready')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        #test when scan value with no check digits specified, scanned invalid
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = ''
        self._obj._loc_info.curr_scan_value = 'scan value'
        self._obj._loc_info.quantity = 10
        self._obj._check_digits = None
        self.post_dialog_responses('#scanned')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t',
                              'wrong scanned, try again')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, CONFIRM_LOCATION)
        
        #test when scan value with no check digits specified, scanned invalid
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = ''
        self._obj._loc_info.curr_scan_value = 'scan value'
        self._obj._loc_info.quantity = 10
        self._obj._check_digits = None
        self.post_dialog_responses('#scan value')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        #test with check digits
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = '00'
        self._obj._loc_info.curr_scan_value = ''
        self._obj._check_digits = None
        self.post_dialog_responses('01', '11', '00')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t',
                              'wrong 01, try again',
                              'wrong 11, try again',
                              'S l o t')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        #test with check digits, and scan
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = '00'
        self._obj._loc_info.curr_scan_value = 'scan value'
        self._obj._check_digits = None
        self.post_dialog_responses('#scan value')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        #test with check digits, and scan
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = '00'
        self._obj._loc_info.curr_scan_value = 'scan value'
        self._obj._check_digits = None
        self.post_dialog_responses('#scanned', '00!')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t',
                              'wrong <spell>scanned</spell>, try again')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        
        #test override not allowed for pick up location
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PICKUP_LOCATION
        self._obj._loc_info.curr_slot = 'Slot'
        self._obj._loc_info.curr_cd = '00'
        self._obj._loc_info.curr_scan_value = ''
        self._obj._check_digits = None
        self.post_dialog_responses('override', '00')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        #test override not allowed for start location
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_START_LOCATION
        self.post_dialog_responses('override', '00')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, None)
        
        #test override put location, override not allowed
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self.post_dialog_responses('override')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t', 
                              'Override not allowed in this region')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, CONFIRM_LOCATION)
        
        #test override put location, override allowed
        self._obj.next_state = None
        self._obj._loc_info.allow_override = True
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self.post_dialog_responses('override', 'no')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t', 
                              'Override, correct?')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, CONFIRM_LOCATION)
        
        #test override put location, override allowed
        self.start_server()
        self.set_server_response('ALT PRE 1,ALT A 1,ALT POST 1,ALT S 1,99,ALTSCAN,Alt Location,0,\n\n')
        
        self._obj.next_state = None
        self._obj._loc_info.allow_override = True
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self.post_dialog_responses('override', 'yes')
        self._obj.runState(CONFIRM_LOCATION)
        self.validate_prompts('S l o t', 
                              'Override, correct?')
        self.assertEquals(self._obj._check_digits, None)
        self.assertEquals(self._obj.next_state, DIRECT_PREAISLE)
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'ALT PRE 1')
        self.assertEquals(self._obj._loc_info.curr_aisle, 'ALT A 1')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'ALT POST 1')
        self.assertEquals(self._obj._loc_info.curr_slot, 'ALT S 1')
        self.assertEquals(self._obj._loc_info.curr_cd, '99')
        self.assertEquals(self._obj._loc_info.curr_scan_value, 'ALTSCAN')
        self.assertEquals(self._obj._loc_info.curr_location_id, 'Alt Location')

    def test_transmit_valid_location(self):

        #test with slot specified
        self._obj.runState(TRANSMIT_LOCATION)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        self._obj._location = '22222'
        self._obj._check_digits = '99'
        self._obj._scanned = False
        
        #test out of range
        self._obj._loc_info.curr_slot = ''
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_LOCATION)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj._loc_info.curr_location_id, 'location id')
        self.assertEquals(self._obj.next_state, TRANSMIT_LOCATION)
        
        self.start_server()
        #test error returned
        self._obj.next_state = None
        self.set_server_response('11111,1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_LOCATION)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTCoreVerifyLocation','0','22222','99','0'])
        self.assertEquals(self._obj._loc_info.curr_location_id, 'location id')
        self.assertEquals(self._obj.next_state, DIRECT_PREAISLE)
        
        #test warning returned
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_PUT_LOCATION
        self._obj._scanned = True
        self.set_server_response('11111,99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(TRANSMIT_LOCATION)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTCoreVerifyLocation','1','22222','99','0'])
        self.assertEquals(self._obj._loc_info.curr_location_id, '11111')
        self.assertEquals(self._obj.next_state, None)
        
        #test success
        self._obj.next_state = None
        self._obj._loc_info.prompt_type = PROMPT_START_LOCATION
        self._obj._location = '33333'
        self._obj._check_digits = ''
        self._obj._scanned = True
        self.set_server_response('55555,,\n\n')
        self._obj.runState(TRANSMIT_LOCATION)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCoreVerifyLocation','1','33333','','1'])
        self.assertEquals(self._obj._loc_info.curr_location_id, '55555')
        self.assertEquals(self._obj.next_state, None)
        
if __name__ == '__main__':
    unittest.main()


