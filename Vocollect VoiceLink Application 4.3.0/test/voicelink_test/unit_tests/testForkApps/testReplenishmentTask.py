from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from forkapps.ReplenishmentTask import ReplenishmentTask, RP_REGIONS, RP_VALIDATE_REGION, RP_LICENSE
from forkapps.ReplenishLicensesTask import ReplenishLicensesTask
from common.RegionSelectionTasks import MultipleRegionTask
import unittest

class testReplenishmentTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1

        self._obj = obj_factory.get(ReplenishmentTask, temp.taskRunner)
        TaskRunnerBase._main_runner = self._obj.taskRunner
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'replenishment')

        #test states
        self.assertEquals(self._obj.states[0], RP_REGIONS)
        self.assertEquals(self._obj.states[1], RP_VALIDATE_REGION)
        self.assertEquals(self._obj.states[2], RP_LICENSE)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._request_reqion_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['allow_cancel_license'].index, 2)
        self.assertEquals(fields['allow_override_location'].index, 3)
        self.assertEquals(fields['allow_override_quantity'].index, 4)
        self.assertEquals(fields['allow_partial_put'].index, 5)
        self.assertEquals(fields['capture_pickup_quantity'].index, 6)
        self.assertEquals(fields['capture_replenished_quantity'].index, 7)
        self.assertEquals(fields['lic_digits_task_speak'].index, 8)
        self.assertEquals(fields['loc_digits_oper_speak'].index, 9)
        self.assertEquals(fields['cd_digits_oper_speak'].index, 10)
        self.assertEquals(fields['exception_location'].index, 11)
        self.assertEquals(fields['verify_license_location'].index, 12)
        self.assertEquals(fields['errorCode'].index, 13)
        self.assertEquals(fields['errorMessage'].index, 14)

    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, RP_REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, MultipleRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskMultiRegions')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, RP_VALIDATE_REGION)
    
    def test_validate_regions(self):
        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj.runState(RP_VALIDATE_REGION)

        self.validate_prompts('not authorized for any regions, see your supervisor')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #Test authorized regions
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self.post_dialog_responses('ready')
        self._obj.runState(RP_VALIDATE_REGION)
        
        self.validate_prompts('To start replenishment, say ready')
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #Test change function, no
        self.start_server()
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self.post_dialog_responses('change function', 'no')
        self._obj.runState(RP_VALIDATE_REGION)
        
        self.validate_prompts('To start replenishment, say ready',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, RP_VALIDATE_REGION)
        
        #-----------------------------------------------------------------
        #Test change function, yes
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self.post_dialog_responses('change function', 'yes')
        self.assertRaises(Launch, self._obj.runState, RP_VALIDATE_REGION)
        
        self.validate_prompts('To start replenishment, say ready',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #Test change region, no
        self._obj.next_state = None
        self._obj._region_selected = True
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self.post_dialog_responses('change region', 'no')
        self._obj.runState(RP_VALIDATE_REGION)
        
        self.validate_prompts('To start replenishment, say ready',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, RP_VALIDATE_REGION)
        
        #-----------------------------------------------------------------
        #Test change function, yes
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self.post_dialog_responses('change region', 'yes')
        self.assertRaises(Launch, self._obj.runState, RP_VALIDATE_REGION)
        
        self.validate_prompts('To start replenishment, say ready',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_replenish_license(self):
        self.assertRaises(Launch, self._obj.runState, RP_LICENSE)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, ReplenishLicensesTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'replenishLicense')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, RP_LICENSE)

        
if __name__ == '__main__':
    unittest.main()
