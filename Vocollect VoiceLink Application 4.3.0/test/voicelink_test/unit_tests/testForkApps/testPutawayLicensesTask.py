from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from forkapps.PutawayLicensesTask import PutawayLicensesTask, PA_INITIALIZE_LICENSE,\
    PA_VERIFY_PICKUP_QTY, PA_VERIFY_START_LOCATION, PA_VERIFY_LOCATION, PA_VERIFY_PUT_QTY,\
    PA_TRANSMIT_PUT, PA_COMPLETE_PROMPT, PA_NEXT_STEP
from common import VoiceLinkLut
from core.VoiceLink import VoiceLink
from vocollect_core.task.task_runner import Launch
import unittest

class testPutawayTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        VoiceLinkLut.transport._timeout = 1
        self._obj = obj_factory.get(PutawayLicensesTask,
                                      obj_factory.get(VoiceLinkLut.VoiceLinkLut, 'prTaskLUTForkPutAwayRegionConfiguration'),
                                      obj_factory.get(VoiceLinkLut.VoiceLinkLut, 'prTaskLUTForkGetPutAway'),
                                      VoiceLink())

        self._obj._region_config_lut.receive('1,put away region 1,0,1,1,1,1,1,0,0,5,5,5,2,1,exception location,0,0,\n'
                                             '2,put away region 2,0,1,1,1,1,1,0,0,5,5,5,2,1,exception location,0,0,\n'
                                             '\n')

        self._obj._licenses_lut.receive('99AB101,1,123,12345,PRE 1,A 1,POST 1,S 1,00,item number,description,100,15.5,99,warning message\n'
                                        '99AB102,1,123,12345,PRE 1,A 1,POST 1,S 2,00,item number,description,100,15.5,99,warning message\n'
                                        '99AB201,2,123,12345,PRE 2,A 2,POST 2,S 3,00,item number,description,100,15.5,99,warning message\n'
                                        '99AB202,2,123,12345,PRE 2,A 2,POST 2,S 4,00,item number,description,100,15.5,99,warning message\n'
                                        '\n')
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'putLicenseAway')

        #test states
        self.assertEquals(self._obj.states[0], PA_INITIALIZE_LICENSE)
        self.assertEquals(self._obj.states[1], PA_VERIFY_PICKUP_QTY)
        self.assertEquals(self._obj.states[2], PA_VERIFY_START_LOCATION)
        self.assertEquals(self._obj.states[3], PA_VERIFY_LOCATION)
        self.assertEquals(self._obj.states[4], PA_VERIFY_PUT_QTY)
        self.assertEquals(self._obj.states[5], PA_TRANSMIT_PUT)
        self.assertEquals(self._obj.states[6], PA_COMPLETE_PROMPT)
        self.assertEquals(self._obj.states[7], PA_NEXT_STEP)
        
        #test LUTS defined correctly
        fields = self._obj._put_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)
        
    def test_initialize_license(self):

        #Test First license
        self._obj._current_license_rec = 0
        self.post_dialog_responses('ready')
        self._obj.runState(PA_INITIALIZE_LICENSE)
        self.validate_prompts('Put away license <spell>AB101</spell>')
        self.assertEquals(self._obj._current_region, self._obj._region_config_lut[0])
        self._validate_settings(0, 0)
        
        #Test Third license, different region
        self._obj._current_license_rec = 2
        self.post_dialog_responses('ready')
        self._obj.runState(PA_INITIALIZE_LICENSE)
        self.validate_prompts('Put away license <spell>AB201</spell>')
        self.assertEquals(self._obj._current_region, self._obj._region_config_lut[1])
        self._validate_settings(2, 1)
        
        #Test no prompt if only one license
        self._obj._current_license_rec = 0
        self._obj._licenses_lut.receive('AB101,1,123,12345,PRE 1,A 1,POST 1,S 1,00,item number,description,100,15.5,99,warning message\n'
                                        '\n')
        self._obj.runState(PA_INITIALIZE_LICENSE)
        self.validate_prompts()
        self.assertEquals(self._obj._current_region, self._obj._region_config_lut[0])
        self._validate_settings(0, 0)
    
    def test_verify_pickup_qty(self):
        self._obj._current_region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._licenses_lut[0]

        #test capture quantity off
        self._obj._current_region['capture_pickup_quantity'] = 0
        self._obj._current_license['quantity'] = 99
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self._obj.runState(PA_VERIFY_PICKUP_QTY)
        self.validate_prompts()
        self.assertEquals(self._obj._pick_up_qty, 99)
        self.assertEquals(self._obj._put_qty, 99)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 99)
        self.assertEquals(self._obj.next_state, None)

        #---------------------------------------------------------------------
        #test capture quantity on, known quantity 0
        self._obj._current_region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 0
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('11!', 'yes')
        self._obj.runState(PA_VERIFY_PICKUP_QTY)
        self.validate_prompts('quantity',
                              '11, correct?')
        self.assertEquals(self._obj._pick_up_qty, 11)
        self.assertEquals(self._obj._put_qty, 11)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 11)
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter exact
        self._obj._current_region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('11!')
        self._obj.runState(PA_VERIFY_PICKUP_QTY)
        self.validate_prompts('quantity')
        self.assertEquals(self._obj._pick_up_qty, 11)
        self.assertEquals(self._obj._put_qty, 11)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 11)
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter more
        self._obj._current_region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('12!', 'yes')
        self._obj.runState(PA_VERIFY_PICKUP_QTY)
        self.validate_prompts('quantity',
                              'You said 12, expected 11, correct?')
        self.assertEquals(self._obj._pick_up_qty, 12)
        self.assertEquals(self._obj._put_qty, 12)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 12)
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter more
        self._obj._current_region['capture_pickup_quantity'] = 1
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('12!', 'no')
        self._obj.runState(PA_VERIFY_PICKUP_QTY)
        self.validate_prompts('quantity',
                              'You said 12, expected 11, correct?')
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PICKUP_QTY)
        
        #---------------------------------------------------------------------
        #test capture quantity on, known quantity > 0, enter more, override not allowed
        self._obj.next_state = None
        self._obj._current_region['capture_pickup_quantity'] = 1
        self._obj._current_region['allow_override_quantity'] = 0
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('12!', 'yes')
        self.assertRaises(Launch, self._obj.runState, PA_VERIFY_PICKUP_QTY)
        self.validate_prompts('quantity',
                              'You said 12, expected 11, correct?',
                              'Canceling put away')
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 0)
        self.assertEquals(self._obj.next_state, None)
        
        #---------------------------------------------------------------------
        #test capture quantity on, qty 0
        self._obj.next_state = None
        self._obj._current_region['capture_pickup_quantity'] = 1
        self._obj._current_region['allow_override_quantity'] = 0
        self._obj._current_license['quantity'] = 11
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 0
        
        self.post_dialog_responses('0!')
        self._obj.runState(PA_VERIFY_PICKUP_QTY)
        self.validate_prompts('quantity',
                              'You must enter a value greater than zero')
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.dynamic_vocab.quantity, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PICKUP_QTY)

    def test_capture_start_location(self):
        self._obj._current_region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._licenses_lut[0]

        #capture != 3
        self._obj._current_region['capture_start_location'] = 0
        self._obj.runState(PA_VERIFY_START_LOCATION)
        self.validate_prompts()
        
        #capture != 3
        self._obj._current_region['capture_start_location'] = 3
        self.assertRaises(Launch, self._obj.runState, PA_VERIFY_START_LOCATION)
        self.validate_prompts()
        self.assertEquals(self._obj.current_state, PA_VERIFY_LOCATION)
        
    def test_verify_location(self):
        self.assertRaises(Launch, self._obj.runState, PA_VERIFY_LOCATION)
        self.validate_prompts()
        self.assertEquals(self._obj.current_state, PA_VERIFY_PUT_QTY)
        
    def test_verify_put_qty(self):
        self._obj._current_region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._licenses_lut[0]

        #test capture put quantity off, partial not spoken
        self._obj._current_region['capture_put_quantity'] = 0
        self._obj._current_license['quantity'] = 99
        self._obj._pick_up_qty = 99
        self._obj._put_qty = 0
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts()
        self.assertEquals(self._obj._put_qty, 99)
        self.assertEquals(self._obj.next_state, None)
        
        #test partial spoken, no to verify
        self._obj._put_qty = 0
        self._obj._loc_info.partial_spoken = True
        self.post_dialog_responses('10!', 'no')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              '10, correct?')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PUT_QTY)
        
        #test partial spoken, too much
        self._obj.next_state = None
        self.post_dialog_responses('150!')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              'You said 150, expected 99')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PUT_QTY)
        
        #test partial spoken, yes to verify
        self._obj.next_state = None
        self.post_dialog_responses('15!', 'yes')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              '15, correct?')
        self.assertEquals(self._obj._put_qty, 15)
        self.assertEquals(self._obj.next_state, None)
        
        #test capture qty on, exact qty spoken
        self._obj._put_qty = 0
        self._obj._current_region['capture_put_quantity'] = 1
        self._obj._loc_info.partial_spoken = False
        self.post_dialog_responses('99!')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity')
        self.assertEquals(self._obj._put_qty, 99)
        self.assertEquals(self._obj.next_state, None)
        
        #test capture qty on, more qty spoken
        self._obj._put_qty = 0
        self._obj._current_region['capture_put_quantity'] = 1
        self._obj._loc_info.partial_spoken = False
        self.post_dialog_responses('199!')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              'You said 199, expected 99')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PUT_QTY)
        
        #test capture qty on, less qty spoken, no to verify
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._current_region['capture_put_quantity'] = 1
        self._obj._loc_info.partial_spoken = False
        self.post_dialog_responses('19!', 'no')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              'You said 19, expected 99, is this a partial?')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PUT_QTY)
        
        #test capture qty on, less qty spoken, yes to verify
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._current_region['capture_put_quantity'] = 1
        self._obj._loc_info.partial_spoken = False
        self.post_dialog_responses('19!', 'yes')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              'You said 19, expected 99, is this a partial?')
        self.assertEquals(self._obj._put_qty, 19)
        self.assertEquals(self._obj.next_state, None)
        
        #test capture qty on, less qty spoken, partial not allowed
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._current_region['capture_put_quantity'] = 1
        self._obj._current_region['allow_partial_put'] = 0
        self._obj._loc_info.partial_spoken = False
        self.post_dialog_responses('19!')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              'You said 19, expected 99')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PUT_QTY)
        
        #test capture qty on, 0 qty spoken
        self._obj.next_state = None
        self._obj._put_qty = 0
        self._obj._loc_info.partial_spoken = False
        self.post_dialog_responses('0!')
        self._obj.runState(PA_VERIFY_PUT_QTY)
        self.validate_prompts('quantity',
                              'You must enter a value greater than zero')
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj.next_state, PA_VERIFY_PUT_QTY)
        
    def test_transmit_put(self):
        self._obj._current_region = self._obj._region_config_lut[0]
        self._obj._current_license = self._obj._licenses_lut[0]
        self._obj._loc_info.set_from_put_license(self._obj._current_license)
        self._obj._loc_info.set_putaway_region_settings(self._obj._current_region)
        self._obj._start_time = 'start time'
        
        self._obj._pick_up_qty = 10
        self._obj._put_qty = 10
        #test out of range
        self.post_dialog_responses('ready')
        self._obj.runState(PA_TRANSMIT_PUT)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, PA_TRANSMIT_PUT)
        
        #test error
        self.start_server()
        self._obj.next_state = None
        self.set_server_response('1,error message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(PA_TRANSMIT_PUT)
        self.validate_prompts('error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense', '99AB101','10','123','1','','start time'])
        self.assertEquals(self._obj.next_state, PA_TRANSMIT_PUT)
        
        #test warning
        self._obj.next_state = None
        self.set_server_response('99,warning message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(PA_TRANSMIT_PUT)
        self.validate_prompts('warning message, say ready')
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense', '99AB101','10','123','1','','start time'])
        self.assertEquals(self._obj.next_state, None)
        
        #test partial
        self._obj._put_qty = 5
        self.set_server_response('0,\n\n')
        self._obj.runState(PA_TRANSMIT_PUT)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense', '99AB101','5','123','0','','start time'])
        self.assertEquals(self._obj.next_state, None)
        
        #test partial
        self._obj._pick_up_qty = 0
        self.set_server_response('0,\n\n')
        self._obj.runState(PA_TRANSMIT_PUT)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkPutAwayLicense', '99AB101','5','123','1','','start time'])
        self.assertEquals(self._obj.next_state, None)
        
    def test_complete(self):
        #Test full quantity put away
        self._obj._pick_up_qty = 5
        self._obj._put_qty = 5
        self.post_dialog_responses('ready')
        self._obj.runState(PA_COMPLETE_PROMPT)
        self.validate_prompts('Complete. Say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #Test full quantity put away (when unkown)
        self._obj._pick_up_qty = 0
        self._obj._put_qty = 5
        self.post_dialog_responses('ready')
        self._obj.runState(PA_COMPLETE_PROMPT)
        self.validate_prompts('Complete. Say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #Test not all put away
        self._obj._pick_up_qty = 10
        self._obj._put_qty = 5
        self._obj.runState(PA_COMPLETE_PROMPT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_next_step(self):
        #Test Put away, more licenses
        self._obj._pick_up_qty = 5
        self._obj._put_qty = 10
        self._obj._current_license_rec = 0
        self._obj._loc_info.partial_spoken = True
        self._obj.runState(PA_NEXT_STEP)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj._current_license_rec, 1)
        self.assertEquals(self._obj._loc_info.partial_spoken, False)
        self.assertEquals(self._obj.next_state, PA_INITIALIZE_LICENSE)
        
        #Test Put away, NO more licenses
        self._obj.next_state = None
        self._obj._pick_up_qty = 5
        self._obj._put_qty = 10
        self._obj._current_license_rec = 3
        self._obj._loc_info.partial_spoken = True
        self._obj.runState(PA_NEXT_STEP)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pick_up_qty, 0)
        self.assertEquals(self._obj._put_qty, 0)
        self.assertEquals(self._obj._current_license_rec, 4)
        self.assertEquals(self._obj._loc_info.partial_spoken, False)
        self.assertEquals(self._obj.next_state, None)
        
        #Test not completely put away
        self.start_server()
        self.set_server_response('ALT PRE 1,ALT A 1,ALT POST 1,ALT S 1,99,ALTSCAN,Alt Location,0,\n\n')
        self._obj.next_state = None
        self._obj._pick_up_qty = 5
        self._obj._put_qty = 3
        self._obj._current_license_rec = 1
        self._obj._loc_info.partial_spoken = True
        self._obj.runState(PA_NEXT_STEP)
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTForkGetAlternatePutLocation','1','','','2'])
        self.assertEquals(self._obj._pick_up_qty, 2)
        self.assertEquals(self._obj._put_qty, 2)
        self.assertEquals(self._obj._current_license_rec, 1)
        self.assertEquals(self._obj._loc_info.partial_spoken, False)
        self.assertEquals(self._obj.next_state, PA_VERIFY_LOCATION)
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, 'ALT PRE 1')
        self.assertEquals(self._obj._loc_info.curr_aisle, 'ALT A 1')
        self.assertEquals(self._obj._loc_info.curr_post_aisle, 'ALT POST 1')
        self.assertEquals(self._obj._loc_info.curr_slot, 'ALT S 1')
        self.assertEquals(self._obj._loc_info.curr_cd, '99')
        self.assertEquals(self._obj._loc_info.curr_scan_value, 'ALTSCAN')
        self.assertEquals(self._obj._loc_info.curr_location_id, 'Alt Location')
        
        
    def _validate_settings(self, license_rec, region_rec):
        self.assertEquals(self._obj.dynamic_vocab.item_number, self._obj._licenses_lut[license_rec]['itemNumber'])
        self.assertEquals(self._obj.dynamic_vocab.description, self._obj._licenses_lut[license_rec]['description'])
        self.assertEquals(self._obj.dynamic_vocab.license, self._obj._licenses_lut[license_rec]['licenseId'])
        self.assertEquals(self._obj.dynamic_vocab.quantity, self._obj._licenses_lut[license_rec]['quantity'])
        self.assertEquals(self._obj.dynamic_vocab.goal_time, self._obj._licenses_lut[license_rec]['goalTime'])
        self.assertEquals(self._obj.dynamic_vocab.partial_allowed, self._obj._region_config_lut[region_rec]['allow_partial_put'])
        self.assertEquals(self._obj.dynamic_vocab.cancel_allowed, self._obj._region_config_lut[region_rec]['allow_cancel_license'])
        self.assertEquals(self._obj.dynamic_vocab.exception_location, self._obj._region_config_lut[region_rec]['exception_location'])
        self.assertEquals(self._obj.dynamic_vocab.release_allowed, self._obj._region_config_lut[region_rec]['allow_release_license'])

        self.assertEquals(self._obj.dynamic_vocab.location, self._obj._loc_info)
        self.assertEquals(self._obj._loc_info.curr_pre_aisle, self._obj._licenses_lut[license_rec]['preAisle'])
        self.assertEquals(self._obj._loc_info.curr_aisle, self._obj._licenses_lut[license_rec]['Aisle'])
        self.assertEquals(self._obj._loc_info.curr_post_aisle, self._obj._licenses_lut[license_rec]['postAisle'])
        self.assertEquals(self._obj._loc_info.curr_slot, self._obj._licenses_lut[license_rec]['slot'])
        self.assertEquals(self._obj._loc_info.curr_cd, self._obj._licenses_lut[license_rec]['locationCD'])
        self.assertEquals(self._obj._loc_info.curr_scan_value, self._obj._licenses_lut[license_rec]['locationScan'])
        self.assertEquals(self._obj._loc_info.curr_location_id, self._obj._licenses_lut[license_rec]['locationId'])
        self.assertEquals(self._obj._loc_info.license_id, self._obj._licenses_lut[license_rec]['licenseId'])
        self.assertEquals(self._obj._loc_info.allow_override, self._obj._region_config_lut[region_rec]['allow_override_location'])
        self.assertEquals(self._obj._loc_info.check_digit_length, self._obj._region_config_lut[region_rec]['cd_digits_oper_speak'])
        self.assertEquals(self._obj._loc_info.verify_location, self._obj._region_config_lut[region_rec]['verify_license_location'])
        self.assertEquals(self._obj._loc_info.location_length, self._obj._region_config_lut[region_rec]['loc_digits_oper_speak'])
        
if __name__ == '__main__':
    unittest.main()


