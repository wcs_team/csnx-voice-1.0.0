from BaseVLTestCase import BaseVLTestCase #Needs to be first import
from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from backstocking.BackStockingTask import BackstockingTask
from backstocking.BackStockingSystemBuildLPNTask import BackstockingSystemBuildLPNTask
from vocollect_core.task.task_runner import Launch
import unittest


class testCycleCountingVocabulary(BaseVLTestCase):

    def setUp(self):
        self.clear()
        temp = obj_factory.get(BackstockingTask, VoiceLink())
        temp._valid_lpn_lut.receive('N,11111SCAN,11111,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,22222SCAN,22222,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,33333SCAN,33333,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')
        temp._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')

        self._obj = obj_factory.get(BackstockingSystemBuildLPNTask,
                                      '12345',
                                      temp._region_config_lut[0],
                                      temp._valid_lpn_lut,
                                      temp.taskRunner, temp)


    def test_valid(self):
        self.assertEqual(self._obj.dynamic_vocab._valid('invalid word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('carton ID'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('case code'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('license plate'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), False)
        
        self._obj.dynamic_vocab.current_carton = self._obj._valid_lpn_lut[0]
        
        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('carton ID'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('case code'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('license plate'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), True)
        
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut

        self.assertEqual(self._obj.dynamic_vocab._valid('repick skips'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('how much more'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('carton ID'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('case code'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('license plate'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), True)
    
    def test_repick_skips(self):
        #----------------------------------------------------------------
        #Test repick skips not allowed
        self._obj._region_rec['allow_repick_skips'] = False
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut

        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('repick skips')
        
        self.assertTrue(result);
        self.validate_prompts()
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test repick skips allowed, no skips
        self._obj._region_rec['allow_repick_skips'] = True
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut

        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('repick skips')
        
        self.assertTrue(result);
        self.validate_prompts('No skips to repick.')
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test repick skips allowed, with skips, no
        self._obj._region_rec['allow_repick_skips'] = True
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut
        self._obj._valid_lpn_lut[0]['status'] = 'S'
        self._obj._valid_lpn_lut[1]['status'] = 'S'
        
        self.post_dialog_responses('no')
        
        result = self._obj.dynamic_vocab.execute_vocab('repick skips')
        
        self.assertTrue(result);
        self.validate_prompts('Repick skips, correct?')
        self.validate_server_requests()        
        self.assertEquals(self._obj._valid_lpn_lut[0]['status'], 'S')
        self.assertEquals(self._obj._valid_lpn_lut[1]['status'], 'S')
        
        #----------------------------------------------------------------
        #Test repick skips allowed, with skips, yes
        self._obj._region_rec['allow_repick_skips'] = True
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut
        self._obj._valid_lpn_lut[0]['status'] = 'S'
        self._obj._valid_lpn_lut[1]['status'] = 'S'
        
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.dynamic_vocab.execute_vocab, 'repick skips')
        
        self.assertTrue(result);
        self.validate_prompts('Repick skips, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj._valid_lpn_lut[0]['status'], 'N')
        self.assertEquals(self._obj._valid_lpn_lut[1]['status'], 'N')

    def test_how_much_more(self):
        #----------------------------------------------------------------
        #Test how much more nothing picked
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut

        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('how much more')
        
        self.assertTrue(result);
        self.validate_prompts('39 remaining at 3 line items')
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test how much more partial picked
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 10
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('how much more')
        
        self.assertTrue(result);
        self.validate_prompts('29 remaining at 3 line items')
        self.validate_server_requests()
        
        #----------------------------------------------------------------
        #Test how much more skips
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 10
        self._obj._valid_lpn_lut[1]['status'] = 'S'
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('how much more')
        
        self.assertTrue(result);
        self.validate_prompts('29 remaining at 3 line items')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test how much more with picks
        self._obj.dynamic_vocab.valid_lpn_lut = self._obj._valid_lpn_lut
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 12
        self._obj._valid_lpn_lut[1]['status'] = 'P'
        self._obj._valid_lpn_lut[2]['status'] = 'P'
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('how much more')
        
        self.assertTrue(result);
        self.validate_prompts('1 remaining at 1 line item')
        self.validate_server_requests()
        
    def test_carton_id(self):
        self._obj.dynamic_vocab.current_carton = self._obj._valid_lpn_lut[0]

        #----------------------------------------------------------------
        #Test carton ID
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('carton ID')
        
        self.assertTrue(result);
        self.validate_prompts('11111')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test carton ID not available
        self._obj._valid_lpn_lut[0]['system_directed_carton_id'] = ''
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('carton ID')
        
        self.assertTrue(result);
        self.validate_prompts('carton ID not available')
        self.validate_server_requests()
                
    def test_case_code(self):
        self._obj.dynamic_vocab.current_carton = self._obj._valid_lpn_lut[0]

        #----------------------------------------------------------------
        #Test case code
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('case code')
        
        self.assertTrue(result);
        self.validate_prompts('UCN')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test case code not available
        self._obj._valid_lpn_lut[0]['UCN'] = ''
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('case code')
        
        self.assertTrue(result);
        self.validate_prompts('case code not available')
        self.validate_server_requests()

    def test_item_number(self):
        self._obj.dynamic_vocab.current_carton = self._obj._valid_lpn_lut[0]

        #----------------------------------------------------------------
        #Test item number
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('item number')
        
        self.assertTrue(result);
        self.validate_prompts('ITEM1')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test item number not available
        self._obj._valid_lpn_lut[0]['item_number'] = ''
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('item number')
        
        self.assertTrue(result);
        self.validate_prompts('item number not available')
        self.validate_server_requests()

    def test_description(self):
        self._obj.dynamic_vocab.current_carton = self._obj._valid_lpn_lut[0]

        #----------------------------------------------------------------
        #Test description
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('description')
        
        self.assertTrue(result);
        self.validate_prompts('description')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test description not available
        self._obj._valid_lpn_lut[0]['description'] = ''
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('description')
        
        self.assertTrue(result);
        self.validate_prompts('description not available')
        self.validate_server_requests()

    def test_quantity(self):
        self._obj.dynamic_vocab.current_carton = self._obj._valid_lpn_lut[0]

        #----------------------------------------------------------------
        #Test quantity
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('quantity')
        
        self.assertTrue(result);
        self.validate_prompts('13')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test carton ID not available
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 5
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('quantity')
        
        self.assertTrue(result);
        self.validate_prompts('8')
        self.validate_server_requests()

    def test_license_plate(self):
        self._obj.dynamic_vocab.lpn = '12345'

        #----------------------------------------------------------------
        #Test license plate
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('license plate')
        
        self.assertTrue(result);
        self.validate_prompts('LPN 12345')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test license plate not available
        self._obj.dynamic_vocab.lpn = ''
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('license plate')
        
        self.assertTrue(result);
        self.validate_prompts('license plate not available')
        self.validate_server_requests()

    def test_upc(self):
        self._obj.dynamic_vocab.current_carton = self._obj._valid_lpn_lut[0]

        #----------------------------------------------------------------
        #Test UPC
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('UPC')
        
        self.assertTrue(result);
        self.validate_prompts('UPC')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test UPC not available
        self._obj._valid_lpn_lut[0]['UPC'] = ''
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('UPC')
        
        self.assertTrue(result);
        self.validate_prompts('UPC not available')
        self.validate_server_requests()

if __name__ == '__main__':
    unittest.main()
