from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from common.RegionSelectionTasks import GET_REGION_CONFIG
from backstocking.BackStockingTask import BackstockingTask, BackStockingRegionTask,\
    VALIDATE_REGION, STOCKING_GET_LICENSE, STOCKING_INDUCT_LICENSE,\
    STOCKING_BUILD_LICENSE, STOCKING_GET_TRIP, STOCKING_START_TRIP,\
    STOCKING_PUT_TRIP, STOCKING_CHECK_RESIDUALS, STOCKING_STOP_TRIP
from backstocking.BackStockingTask import REGIONS
from backstocking.BackStockingSystemBuildLPNTask import BackstockingSystemBuildLPNTask
from backstocking.BackStockingOperBuildLPNTask import BackstockingOperBuildLPNTask
from backstocking.BackStockingPutTrip import BackStockingPutTripTask
from core.CoreTask import CoreTask
import unittest

class testBackStockingTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1
        self._obj = obj_factory.get(BackstockingTask, temp.taskRunner)
        TaskRunnerBase._main_runner = self._obj.taskRunner
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'backStocking')
        
        #test states
        self.assertEquals(self._obj.states[0], REGIONS)
        self.assertEquals(self._obj.states[1], VALIDATE_REGION)
        self.assertEquals(self._obj.states[2], STOCKING_GET_LICENSE)
        self.assertEquals(self._obj.states[3], STOCKING_INDUCT_LICENSE)
        self.assertEquals(self._obj.states[4], STOCKING_BUILD_LICENSE)
        self.assertEquals(self._obj.states[5], STOCKING_GET_TRIP)
        self.assertEquals(self._obj.states[6], STOCKING_START_TRIP)
        self.assertEquals(self._obj.states[7], STOCKING_PUT_TRIP)
        self.assertEquals(self._obj.states[8], STOCKING_CHECK_RESIDUALS)
        self.assertEquals(self._obj.states[9], STOCKING_STOP_TRIP)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['current_pre_aisle'].index, 2)
        self.assertEquals(fields['current_aisle'].index, 3)
        self.assertEquals(fields['current_post_aisle'].index, 4)
        self.assertEquals(fields['allow_reverse_order'].index, 5)
        self.assertEquals(fields['allow_skip_aisle'].index, 6)
        self.assertEquals(fields['allow_skip_slot'].index, 7)
        self.assertEquals(fields['require_qty_verification'].index, 8)
        self.assertEquals(fields['allow_repick_skips'].index, 9)
        self.assertEquals(fields['goback_shorts_skips'].index, 10)
        self.assertEquals(fields['errorCode'].index, 11)
        self.assertEquals(fields['errorMessage'].index, 12)

        fields = self._obj._lpn_lut.fields
        self.assertEquals(fields['lpn_staging_area'].index, 0)
        self.assertEquals(fields['lpn'].index, 1)
        self.assertEquals(fields['scan_lpn'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)

        fields = self._obj._valid_lpn_lut.fields
        self.assertEquals(fields['status'].index, 0)
        self.assertEquals(fields['scanned_carton_id'].index, 1)
        self.assertEquals(fields['spoken_carton_id'].index, 2)
        self.assertEquals(fields['quantity_to_pick'].index, 3)
        self.assertEquals(fields['quantity_picked'].index, 4)
        self.assertEquals(fields['item_number'].index, 5)
        self.assertEquals(fields['UCN'].index, 6)
        self.assertEquals(fields['UPC'].index, 7)
        self.assertEquals(fields['description'].index, 8)
        self.assertEquals(fields['system_directed'].index, 9)
        self.assertEquals(fields['all_cartons'].index, 10)
        self.assertEquals(fields['system_directed_carton_id'].index, 11)
        self.assertEquals(fields['pick_quantity'].index, 12)
        self.assertEquals(fields['unique_id'].index, 13)
        self.assertEquals(fields['prep_message'].index, 14)
        self.assertEquals(fields['errorCode'].index, 15)
        self.assertEquals(fields['errorMessage'].index, 16)

        fields = self._obj._trip_lut.fields
        self.assertEquals(fields['id'].index, 0)
        self.assertEquals(fields['sequence'].index, 1)
        self.assertEquals(fields['trip_summary'].index, 2)
        self.assertEquals(fields['performance_summary'].index, 3)
        self.assertEquals(fields['location_id'].index, 4)
        self.assertEquals(fields['scanned_validation'].index, 5)
        self.assertEquals(fields['status'].index, 6)
        self.assertEquals(fields['pre_aisle'].index, 7)
        self.assertEquals(fields['aisle'].index, 8)
        self.assertEquals(fields['post_aisle'].index, 9)
        self.assertEquals(fields['slot'].index, 10)
        self.assertEquals(fields['check_digit'].index, 11)
        self.assertEquals(fields['scanned_carton_id'].index, 12)
        self.assertEquals(fields['spoken_carton_id'].index, 13)
        self.assertEquals(fields['carton_id'].index, 14)
        self.assertEquals(fields['quantity'].index, 15)
        self.assertEquals(fields['item_number'].index, 16)
        self.assertEquals(fields['description'].index, 17)
        self.assertEquals(fields['pack_size'].index, 18)
        self.assertEquals(fields['UPC'].index, 19)
        self.assertEquals(fields['UCN'].index, 20)
        self.assertEquals(fields['stocker_prep'].index, 21)
        self.assertEquals(fields['drop_zone'].index, 22)
        self.assertEquals(fields['drop_zone_cd'].index, 23)
        self.assertEquals(fields['errorCode'].index, 24)
        self.assertEquals(fields['errorMessage'].index, 25)

        fields = self._obj._stop_license_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)


    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, BackStockingRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskSingleRegion')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, VALIDATE_REGION)
    
    def test_validate_regions(self):
        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts('not authorized for any regions, see your supervisor')
        
        #-----------------------------------------------------------------
        #Test authorized regions
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_server_requests()
        self.validate_prompts()

    def test_get_region_config(self):
        obj = BackStockingRegionTask(VoiceLink(), BackstockingTask())

        self.start_server()

        #---------------------------------------------------------------------------
        #Test function number part of request 
        self.set_server_response('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n', 'prTaskLUTStockingGetRegionConfiguration')
        obj.selected_region = 1
        
        obj.runState(GET_REGION_CONFIG)
        
        self.assertEquals(obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTStockingGetRegionConfiguration', '1', '11'])
        self.validate_prompts()
    
    def test_get_license(self):
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')

        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(STOCKING_GET_LICENSE)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_GET_LICENSE)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response(',,,1,Error Message\n\n')

        self._obj.runState(STOCKING_GET_LICENSE)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingGetLPN', '1'])
        self.assertEquals(self._obj.next_state, STOCKING_GET_LICENSE)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self._obj._region_config_lut[0]['number'] = 2
        self.post_dialog_responses('ready')
        self.set_server_response('Area,lpn,scan_lpn,99,Warning Message\n\n')

        self._obj.runState(STOCKING_GET_LICENSE)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTStockingGetLPN', '2'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors 
        self._obj.next_state = None
        self.set_server_response('Area,lpn,scan_lpn,0,\n\n')

        self._obj.runState(STOCKING_GET_LICENSE)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingGetLPN', '2'])
        self.assertEquals(self._obj.next_state, None)
    
    def test_induct_license(self):
        self.start_server()
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        self._obj._lpn_lut.receive('Area,12345,1234567890,0,\n\n')
        
        #-----------------------------------------------------------------
        #test different staging area, location spoken
        self._obj.next_state = None
        self._obj._staging_area = ''

        self.post_dialog_responses('ready', 'location')
        
        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('go to Area', 
                              'License Plate 12345', 
                              'go to Area')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        self.assertEquals(self._obj._staging_area, 'Area')
        
        #-----------------------------------------------------------------
        #test same staging area, system specified, invalid spoken lpn
        self._obj.next_state = None
        self._obj._staging_area = 'Area'

        self.post_dialog_responses('11111!')
        
        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate 12345',
                              '<spell>11111</spell> invalid, try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        
        #-----------------------------------------------------------------
        #test same staging area, system specified, invalid scanned LPN
        self._obj.next_state = None
        self._obj._staging_area = 'Area'

        self.post_dialog_responses('#12345')
        
        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate 12345',
                              '<spell>12345</spell> invalid, try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        
        #-----------------------------------------------------------------
        #test same staging area, system specified, valid spoken, error code
        self._obj.next_state = None
        self._obj._staging_area = 'Area'

        self.post_dialog_responses('12345!',
                                   'ready')
        self.set_server_response('N,12345SCAN,12345,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,1,Error Message\n\n')
        
        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate 12345',
                              'Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingValidateLPN', '12345'])
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        
        #-----------------------------------------------------------------
        #test same staging area, system specified, valid scanned, warning code
        self._obj.next_state = None
        self._obj._staging_area = 'Area'

        self.post_dialog_responses('#1234567890',
                                   'ready')
        self.set_server_response('N,12345SCAN,12345,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,99,Warning Message\n\n')
        
        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate 12345',
                              'Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTStockingValidateLPN', '1234567890'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test same staging area, operator specified, spoken, no errors
        self._obj.next_state = None
        self._obj._staging_area = 'Area'
        self._obj._lpn_lut[0]['lpn'] = ''

        self.post_dialog_responses('1111!',
                                   'yes')
        self.set_server_response('N,12345SCAN,12345,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')
        
        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate?',
                              '1111, correct?')
        self.validate_server_requests(['prTaskLUTStockingValidateLPN', '1111'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test same staging area, operator specified, scanned, no errors
        self._obj.next_state = None
        self._obj._staging_area = 'Area'
        self._obj._lpn_lut[0]['lpn'] = ''

        self.post_dialog_responses('#1111111111')
        self.set_server_response('N,12345SCAN,12345,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')
        
        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate?')
        self.validate_server_requests(['prTaskLUTStockingValidateLPN', '1111111111'])
        self.assertEquals(self._obj.next_state, None)

    def test_change_function(self):
        self.start_server()
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        self._obj._lpn_lut.receive('Area,12345,1234567890,0,\n\n')
        self._obj._lpn_lut[0]['lpn'] = ''
        
        #-----------------------------------------------------------------
        #test change function no - staging area prompt
        self._obj.next_state = None
        self._obj._staging_area = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('go to Area', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        
        #-----------------------------------------------------------------
        #test change function yes - staging area prompt
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('go to Area', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test change function no - License prompt
        self._obj.next_state = None
        self._obj._staging_area = self._obj._lpn_lut[0]['lpn_staging_area']
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        
        #-----------------------------------------------------------------
        #test change function yes - License Prompt
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate?', 
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        

    def test_change_region(self):
        self.start_server()
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        self._obj._lpn_lut.receive('Area,12345,1234567890,0,\n\n')
        self._obj._lpn_lut[0]['lpn'] = ''
        self._obj._region_selected = True
        
        #-----------------------------------------------------------------
        #test change region no - staging area prompt
        self._obj.next_state = None
        self._obj._staging_area = None
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('go to Area', 
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        
        #-----------------------------------------------------------------
        #test change region yes - staging area prompt
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('go to Area', 
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test change region no - License prompt
        self._obj.next_state = None
        self._obj._staging_area = self._obj._lpn_lut[0]['lpn_staging_area']
        self._obj._region_selected = True
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate?', 
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_INDUCT_LICENSE)
        
        #-----------------------------------------------------------------
        #test change region yes - License Prompt
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, STOCKING_INDUCT_LICENSE)
        
        self.validate_prompts('License Plate?', 
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        

    def test_build_license(self):
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        self._obj._valid_lpn_lut.receive('N,12345SCAN,12345,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')

        #-----------------------------------------------------------------
        #test system directed task is launched 
        self._obj._valid_lpn_lut[0]['system_directed'] = True
        self.assertRaises(Launch, self._obj.runState, STOCKING_BUILD_LICENSE)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, BackstockingSystemBuildLPNTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'backstockingSystemBuildLPNTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, STOCKING_GET_TRIP)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
        #-----------------------------------------------------------------
        #test operator directed task is launched 
        self._obj._valid_lpn_lut[0]['system_directed'] = False
        self.assertRaises(Launch, self._obj.runState, STOCKING_BUILD_LICENSE)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, BackstockingOperBuildLPNTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'backstockingOperBuildLPNTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, STOCKING_GET_TRIP)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
    def test_get_trip(self):
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        self.start_server()
        self._obj._license = '12345'
        
        #-----------------------------------------------------------------
        #test reverse allowed, error condition
        self._obj._region_config_lut[0]['allow_reverse_order'] = True
        self._obj.next_state = None

        self.post_dialog_responses('yes', 'ready')
        self.set_server_response('1,1,trip summary,preformance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,1,Error message\n\n')

        self._obj.runState(STOCKING_GET_TRIP)
        
        self.validate_prompts('Put away in reverse order?', 
                              'Receiving work in reverse order.',
                              'Error message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingGetTrip', '12345', '1'])
        self.assertEquals(self._obj.next_state, STOCKING_GET_TRIP)
        
        #-----------------------------------------------------------------
        #test reverse allowed, warning condition
        self._obj.next_state = None
        self.post_dialog_responses('no', 'ready')
        self.set_server_response('1,1,trip summary,preformance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,99,Warning message\n\n')

        self._obj.runState(STOCKING_GET_TRIP)
        
        self.validate_prompts('Put away in reverse order?', 'Warning message, say ready')
        self.validate_server_requests(['prTaskLUTStockingGetTrip', '12345', '0'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors, reverse not allowed 
        self._obj._region_config_lut[0]['allow_reverse_order'] = False
        self._obj.next_state = None
        self.post_dialog_responses()
        self.set_server_response('1,1,trip summary,preformance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n\n')

        self._obj.runState(STOCKING_GET_TRIP)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingGetTrip', '12345', '0'])
        self.assertEquals(self._obj.next_state, None)
    
    def test_start_trip(self):
        self._obj._trip_lut.receive('1,1,trip summary,performance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n\n')

        #-----------------------------------------------------------------
        #test speaking performance 
        self._obj.next_state = None
        self.post_dialog_responses('performance')
        
        self._obj.runState(STOCKING_START_TRIP)
        
        self.validate_prompts('trip summary, say ready', 'performance summary')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_START_TRIP)
        
        #-----------------------------------------------------------------
        #test ready 
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_START_TRIP)
        
        self.validate_prompts('trip summary, say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
    
    def test_put_trip(self):
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        self._obj._valid_lpn_lut.receive('N,12345SCAN,12345,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')

        #-----------------------------------------------------------------
        #test system directed task is launched 
        self.assertRaises(Launch, self._obj.runState, STOCKING_PUT_TRIP)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, BackStockingPutTripTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'backStockingPutTrip')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, STOCKING_CHECK_RESIDUALS)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
    
    def test_check_residuals(self):
        self._obj._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        self._obj._trip_lut.receive('1,1,trip summary,performance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                    '1,1,trip summary,performance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n\n')

        #-----------------------------------------------------------------
        #test no residuals 
        self._obj.next_state = None
        self.post_dialog_responses('no')
        
        self._obj.runState(STOCKING_CHECK_RESIDUALS)
        
        self.validate_prompts('Do you have residuals?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test yes residuals, drop zone with check digits 
        self._obj.next_state = None
        self.post_dialog_responses('yes', '00!', '01', '11!')
        
        self._obj.runState(STOCKING_CHECK_RESIDUALS)
        
        self.validate_prompts('Do you have residuals?',
                              'Deliver residuals to drop zone.', 
                              'wrong 00, try again', 
                              'wrong 01, try again', 
                              'Deliver residuals to drop zone.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test LPN canceled, not check digits
        self._obj._valid_lpn_lut.canceled_cartons = True
        self._obj._trip_lut[0]['drop_zone_cd'] = '' 
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_CHECK_RESIDUALS)
        
        self.validate_prompts('Deliver residuals to drop zone.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test put canceled, not check digits
        self._obj._valid_lpn_lut.canceled_cartons = False
        self._obj._trip_lut[0]['drop_zone_cd'] = '' 
        self._obj._trip_lut[0]['status'] = 'C' 
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_CHECK_RESIDUALS)
        
        self.validate_prompts('Deliver residuals to drop zone.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test put skipped
        self._obj._valid_lpn_lut.canceled_cartons = False
        self._obj._trip_lut[0]['drop_zone_cd'] = '' 
        self._obj._trip_lut[0]['status'] = 'S' 
        self._obj._trip_lut[1]['status'] = 'S' 
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_CHECK_RESIDUALS)
        
        self.validate_prompts('Trip complete.  To go back for skips, say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_TRIP)
        self.assertEquals(self._obj._trip_lut[0]['status'], 'N')
        self.assertEquals(self._obj._trip_lut[1]['status'], 'N')

    def test_stop_trip(self):
        self._obj._license = '11111'
        
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(STOCKING_STOP_TRIP)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_STOP_TRIP)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response('1,Error Message\n\n')

        self._obj.runState(STOCKING_STOP_TRIP)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingStopLicense', '11111'])
        self.assertEquals(self._obj.next_state, STOCKING_STOP_TRIP)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('99,Warning Message\n\n')

        self._obj.runState(STOCKING_STOP_TRIP)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTStockingStopLicense', '11111'])
        self.assertEquals(self._obj.next_state, STOCKING_GET_LICENSE)

        #-----------------------------------------------------------------
        #test no errors 
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(STOCKING_STOP_TRIP)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingStopLicense', '11111'])
        self.assertEquals(self._obj.next_state, STOCKING_GET_LICENSE)

    def test_change_region_function(self):
        #---------------------------------------------------------------
        #Test no region currently selected
        self._obj._region_selected = False
        
        self._obj.change_region()
        
        self.validate_prompts() 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response no
        self._obj._region_selected = True
        self.post_dialog_responses('no')
        
        self._obj.change_region()
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response yes
        self._obj._region_selected = True
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.change_region)
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
    
if __name__ == '__main__':
    unittest.main()
