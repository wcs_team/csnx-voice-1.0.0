from BaseVLTestCase import BaseVLTestCase #Needs to be first import
from vocollect_core import obj_factory
from vocollect_core.task.task_runner import Launch
from core.VoiceLink import VoiceLink
from backstocking.BackStockingTask import BackstockingTask
from backstocking.BackStockingPutTrip import BackStockingPutTripTask,\
    STOCKING_TRIP_RESET, STOCKING_TRIP_CHECK_NEXT_PUT, STOCKING_TRIP_PREAISLE,\
    STOCKING_TRIP_AISLE, STOCKING_TRIP_POSTAISLE, STOCKING_TRIP_PUTPROMPT
from backstocking.BackStockingPutPrompt import BackStockingPutPromptTask

import unittest

class testBackStockingSystemBuildLPNTask(BaseVLTestCase):

    def setUp(self):
        self.clear()
        temp = obj_factory.get(BackstockingTask, VoiceLink())
        temp._valid_lpn_lut.receive('N,11111SCAN,11111,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,22222SCAN,22222,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,33333SCAN,33333,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')
        temp._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        temp._trip_lut.receive('1,1,trip summary,performance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                               '1,1,trip summary,performance summary,loc1,11111,N,pre 2,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n\n')

        self._obj = obj_factory.get(BackStockingPutTripTask,
                                      temp._region_config_lut[0],
                                      temp._trip_lut,
                                      temp.taskRunner, temp)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'backStockingPutTrip')

        #test states
        self.assertEquals(self._obj.states[0], STOCKING_TRIP_RESET)
        self.assertEquals(self._obj.states[1], STOCKING_TRIP_CHECK_NEXT_PUT)
        self.assertEquals(self._obj.states[2], STOCKING_TRIP_PREAISLE)
        self.assertEquals(self._obj.states[3], STOCKING_TRIP_AISLE)
        self.assertEquals(self._obj.states[4], STOCKING_TRIP_POSTAISLE)
        self.assertEquals(self._obj.states[5], STOCKING_TRIP_PUTPROMPT)
    
    def test_reset(self):
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        
        self._obj.runState(STOCKING_TRIP_RESET)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, '')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
    def test_check_next_put(self):
        #initialize iterator
        self._obj.runState(STOCKING_TRIP_RESET)

        #----------------------------------------------------------
        #test returns first put
        self._obj.next_state = None
        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._current_put, self._obj._trip_lut[0])
        
        #----------------------------------------------------------
        #test returns gets second put, but returns to same state
        self._obj.next_state = None
        self._obj._trip_lut[1]['status'] = 'P'
        
        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_TRIP_CHECK_NEXT_PUT)
        self.assertEquals(self._obj._current_put, self._obj._trip_lut[1])
        
        #----------------------------------------------------------
        #test no more puts
        self._obj.next_state = None

        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
    def test_pre_aisle(self):
        #initialize iterator
        self._obj.runState(STOCKING_TRIP_RESET)
        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)

        #-------------------------------------------------
        #test pre aisle is spoken when new pre-asile
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_TRIP_PREAISLE)

        self.validate_prompts("pre 1")
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'pre 1')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when matches previous
        self._obj._pre_aisle_direction = 'pre 1'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_PREAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'pre 1')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'pre 1'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['pre_aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_PREAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, '')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = ''
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['pre_aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_PREAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, '')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)

    def test_aisle(self):
        #initialize iterator
        self._obj.runState(STOCKING_TRIP_RESET)
        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)

        #-------------------------------------------------
        #test aisle is spoken when new asile
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_TRIP_AISLE)

        self.validate_prompts('Aisle <spell>1</spell>')
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '1')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test aisle is not spoken when matches previous
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = '1'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_AISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '1')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = '1'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_AISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test pre aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = ''
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_AISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, '')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test skip aisle is called
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['aisle'] = '1'
        self.post_dialog_responses('skip aisle',
                                   'no')
        
        self._obj.runState(STOCKING_TRIP_AISLE)

        self.validate_prompts('Aisle <spell>1</spell>',
                              'Skip aisle, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'dummy')
        self.assertEquals(self._obj.next_state, STOCKING_TRIP_AISLE)

    def test_post_aisle(self):
        #initialize iterator
        self._obj.runState(STOCKING_TRIP_RESET)
        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)

        #-------------------------------------------------
        #test post aisle is spoken when new post-asile
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_TRIP_POSTAISLE)

        self.validate_prompts("post 1")
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'post 1')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test post aisle is not spoken when matches previous
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'post 1'
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_POSTAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, 'post 1')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test post aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = 'dummy'
        self._obj._current_put['post_aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_POSTAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)
        
        #-------------------------------------------------
        #test post aisle is not spoken when blank
        self._obj._pre_aisle_direction = 'dummy'
        self._obj._aisle_direction = 'dummy'
        self._obj._post_aisle_direction = ''
        self._obj._current_put['post_aisle'] = ''
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_TRIP_POSTAISLE)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj._pre_aisle_direction, 'dummy')
        self.assertEquals(self._obj._aisle_direction, 'dummy')
        self.assertEquals(self._obj._post_aisle_direction, '')
        self.assertEquals(self._obj.next_state, None)

    def test_put_prompt(self):
        #initialize iterator
        self._obj.runState(STOCKING_TRIP_RESET)
        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)

        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, STOCKING_TRIP_PUTPROMPT)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, BackStockingPutPromptTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'backStockingPutTask')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, STOCKING_TRIP_CHECK_NEXT_PUT)

    def test_skip_aisle(self):
        #initialize iterator
        self._obj.runState(STOCKING_TRIP_RESET)
        self._obj.runState(STOCKING_TRIP_CHECK_NEXT_PUT)
        
        #------------------------------------------------
        #test skip aisle not allowed
        self._obj._region_config_rec['allow_skip_aisle'] = False
        self.post_dialog_responses()
        
        self._obj._skip_aisle()
        self.validate_prompts('skip aisle not allowed')
        self.validate_server_requests()
        
        #------------------------------------------------
        #test skip aisle allowed, confirm no
        self._obj._region_config_rec['allow_skip_aisle'] = True
        curr_state = self._obj.next_state
        self.post_dialog_responses('no')
        
        self._obj._skip_aisle()
        
        self.validate_prompts('Skip aisle, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, curr_state)
        
        #------------------------------------------------
        #test skip aisle allowed, confirm yes
        self.post_dialog_responses('yes')
        self.set_server_response(',0\n\n')
        
        self._obj._skip_aisle()
        
        self.validate_prompts('Skip aisle, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_TRIP_CHECK_NEXT_PUT)
        self.assertEquals(self._obj._trip_lut[0]['status'], 'S')
        self.assertEquals(self._obj._trip_lut[1]['status'], 'N')
 
if __name__ == '__main__':
    unittest.main()

