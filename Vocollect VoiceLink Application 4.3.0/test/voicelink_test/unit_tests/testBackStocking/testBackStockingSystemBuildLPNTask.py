from BaseVLTestCase import BaseVLTestCase #Needs to be first import
from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from backstocking.BackStockingTask import BackstockingTask
from backstocking.BackStockingSystemBuildLPNTask import BackstockingSystemBuildLPNTask,\
    BACKSTOCKING_SYSTEM_BUILD_ALL, BACKSTOCKING_SYSTEM_NEXT_CARTON,\
    BACKSTOCKING_SYSTEM_CONFIRM_CARTON, BACKSTOCKING_SYSTEM_QUANITY,\
    BACKSTOCKING_SYSTEM_REASON, BACKSTOCKING_SYSTEM_XMIT_CARTON,\
    BACKSTOCKING_SYSTEM_NEXT_STEP
import unittest


class testBackStockingSystemBuildLPNTask(BaseVLTestCase):

    def setUp(self):
        self.clear()
        temp = obj_factory.get(BackstockingTask, VoiceLink())
        temp._valid_lpn_lut.receive('N,11111SCAN,11111,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,22222SCAN,22222,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,33333SCAN,33333,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')
        temp._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')

        self._obj = obj_factory.get(BackstockingSystemBuildLPNTask,
                                      '12345',
                                      temp._region_config_lut[0],
                                      temp._valid_lpn_lut,
                                      temp.taskRunner, temp)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'backstockingSystemBuildLPNTask')
        
        #test states
        self.assertEquals(self._obj.states[0], BACKSTOCKING_SYSTEM_BUILD_ALL)
        self.assertEquals(self._obj.states[1], BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj.states[2], BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        self.assertEquals(self._obj.states[3], BACKSTOCKING_SYSTEM_QUANITY)
        self.assertEquals(self._obj.states[4], BACKSTOCKING_SYSTEM_REASON)
        self.assertEquals(self._obj.states[5], BACKSTOCKING_SYSTEM_XMIT_CARTON)
        self.assertEquals(self._obj.states[6], BACKSTOCKING_SYSTEM_NEXT_STEP)
        
        #test LUTS defined correctly
        fields = self._obj._carton_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)
    
        fields = self._obj._reason_code_lut.fields
        self.assertEquals(fields['code'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
    
    def test_build_all(self):
        
        #-----------------------------------------------------------------
        #Test all carton True
        self._obj.next_state = None
        self._obj._valid_lpn_lut[0]['all_cartons'] = True
        self.post_dialog_responses('ready')
        
        self._obj.runState(BACKSTOCKING_SYSTEM_BUILD_ALL)
        
        self.validate_prompts('This is a full pallet assignment.  To continue say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_REASON)

        #-----------------------------------------------------------------
        #Test all carton False
        self._obj.next_state = None
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        
        self._obj.runState(BACKSTOCKING_SYSTEM_BUILD_ALL)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertTrue(self._obj.dynamic_vocab != None)
        self.assertTrue(self._obj._valid_lpn_iter != None)

    def test_next_carton(self):
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj.runState(BACKSTOCKING_SYSTEM_BUILD_ALL)
        self._obj._valid_lpn_lut[1]['status'] = 'P'
        
        #-----------------------------------------------------------------
        #Test has next
        self._obj.next_state = None
        
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._valid_lpn_curr, self._obj._valid_lpn_lut[0])

        #-----------------------------------------------------------------
        #Test has next, but status not N
        self._obj.next_state = None
        
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj._valid_lpn_curr, self._obj._valid_lpn_lut[1])
        
        #-----------------------------------------------------------------
        #Test has next 
        self._obj.next_state = None
        
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._valid_lpn_curr, self._obj._valid_lpn_lut[2])
    
        #-----------------------------------------------------------------
        #Test no more 
        self._obj.next_state = None
        
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_STEP)
        self.assertEquals(self._obj._valid_lpn_curr, self._obj._valid_lpn_lut[2])
    
    def test_confirm_carton(self):
        #initialize data
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj.runState(BACKSTOCKING_SYSTEM_BUILD_ALL)
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_CARTON)
        
        #-----------------------------------------------------------------
        #Test cancel spoken, No 
        self._obj.next_state = None
        self.post_dialog_responses('cancel', 'no')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message', 'Cancel carton, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
        #-----------------------------------------------------------------
        #Test cancel spoken, Yes 
        self._obj.next_state = None
        self.post_dialog_responses('cancel', 'yes')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message', 'Cancel carton, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_REASON)
        self.assertEquals(self._obj._canceled, True)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)

        #-----------------------------------------------------------------
        #Test stop spoken, No 
        self._obj.next_state = None
        self.post_dialog_responses('stop', 'no')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message', 'Stop build, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
        #-----------------------------------------------------------------
        #Test stop spoken, Yes 
        self._obj.next_state = None
        self.post_dialog_responses('stop', 'yes')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message', 'Stop build, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_REASON)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, True)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)

        #-----------------------------------------------------------------
        #Test stop spoken, No 
        self._obj.next_state = None
        self.post_dialog_responses('skip carton', 'no', 'skip carton', 'yes')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message', 
                              'skip carton, correct?', 
                              '<spell>11111</spell>, pick 13, prep message', 
                              'skip carton, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'S')
        
        #-----------------------------------------------------------------
        #Test ready with spoken carton available
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message',
                              'you must speak the carton ID, try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
        #-----------------------------------------------------------------
        #Test ready with no spoken carton available
        self._obj.next_state = None
        self._obj._valid_lpn_curr['spoken_carton_id'] = ''
        self.post_dialog_responses('ready')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
        #-----------------------------------------------------------------
        #Test invalid carton spoken
        self._obj.next_state = None
        self._obj._valid_lpn_curr['spoken_carton_id'] = '11111'
        self.post_dialog_responses('12345!')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message',
                              'Wrong 12345')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
        #-----------------------------------------------------------------
        #Test invalid carton scanned
        self._obj.next_state = None
        self._obj._valid_lpn_curr['spoken_carton_id'] = '11111'
        self.post_dialog_responses('#12345')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message',
                              'Wrong 12345')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
        #-----------------------------------------------------------------
        #Test valid carton spoken
        self._obj.next_state = None
        self._obj._valid_lpn_curr['spoken_carton_id'] = '11111'
        self.post_dialog_responses('11111!')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
        #-----------------------------------------------------------------
        #Test valid carton spoken
        self._obj.next_state = None
        self._obj._valid_lpn_curr['spoken_carton_id'] = '11111'
        self.post_dialog_responses('#11111SCAN')

        self._obj.runState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON)
        
        self.validate_prompts('<spell>11111</spell>, pick 13, prep message')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._canceled, False)
        self.assertEquals(self._obj._stopped, False)
        self.assertEquals(self._obj._shorted, False)
        self.assertEquals(self._obj._quantity, 0)
        
    def test_quantity(self):
        #initialize data
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj.runState(BACKSTOCKING_SYSTEM_BUILD_ALL)
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_CARTON)

        #-----------------------------------------------------------------
        #Test too much entered, nothing picked
        self._obj.next_state = None
        self.post_dialog_responses('20!')
        
        self._obj.runState(BACKSTOCKING_SYSTEM_QUANITY)
        
        self.validate_prompts('Quantity?', 
                              'You said 20, only asked for 13, try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_QUANITY)
        
        #-----------------------------------------------------------------
        #Test too much entered, nothing picked
        self._obj.next_state = None
        self._obj._valid_lpn_curr['quantity_picked'] = 3
        self.post_dialog_responses('13!')
        
        self._obj.runState(BACKSTOCKING_SYSTEM_QUANITY)
        
        self.validate_prompts('Quantity?', 
                              'You said 13, only asked for 10, try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_QUANITY)
        
        #-----------------------------------------------------------------
        #Test too little entered, no to confirm
        self._obj.next_state = None
        self.post_dialog_responses('1!', 'no')
        
        self._obj.runState(BACKSTOCKING_SYSTEM_QUANITY)
        
        self.validate_prompts('Quantity?',
                              'You said 1. Is this a short product?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_QUANITY)
        self.assertEquals(self._obj._shorted, False)
        
        #-----------------------------------------------------------------
        #Test too little entered, yes to confirm
        self._obj.next_state = None
        self.post_dialog_responses('1!', 'yes')
        
        self._obj.runState(BACKSTOCKING_SYSTEM_QUANITY)
        
        self.validate_prompts('Quantity?',
                              'You said 1. Is this a short product?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._shorted, True)
        
        
        #-----------------------------------------------------------------
        #Test correct quantity entered
        self._obj._shorted = False
        self._obj.next_state = None
        self.post_dialog_responses('10!')
        
        self._obj.runState(BACKSTOCKING_SYSTEM_QUANITY)
        
        self.validate_prompts('Quantity?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._shorted, False)
        
    def test_get_reason_code(self):
        #-----------------------------------------------------------------
        #Test no reason code required
        self._obj._shorted = False
        self._obj._canceled = False
        self._obj._stopped = False
        
        self.post_dialog_responses()
        
        self._obj.runState(BACKSTOCKING_SYSTEM_REASON)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason_code, '')

        #-----------------------------------------------------------------
        #test server not started, shorted
        self._obj._shorted = True
        self._obj._canceled = False
        self._obj._stopped = False
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(BACKSTOCKING_SYSTEM_REASON)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_REASON)
        self.assertEquals(self._obj._reason_code, '')
        
        #-----------------------------------------------------------------
        #test error condition, canceled
        self._obj._shorted = False
        self._obj._canceled = True
        self._obj._stopped = False
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response(',,1,Error Message\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_REASON)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '11', '1'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_REASON)
        self.assertEquals(self._obj._reason_code, '')
        
        #-----------------------------------------------------------------
        #test warning condition, Stopped
        self._obj._shorted = False
        self._obj._canceled = False
        self._obj._stopped = True
        self._obj.next_state = None
        self.set_server_response('1,reason 1,99,Warning Message\n\n')
        self.post_dialog_responses('ready', 
                                   '1!', 
                                   'yes')

        self._obj.runState(BACKSTOCKING_SYSTEM_REASON)
        
        self.validate_prompts('Warning Message, say ready',
                              'Reason?', 
                              'reason 1, correct?')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '11', '1'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason_code, '1')

        #-----------------------------------------------------------------
        #test no errors, all cartons
        self._obj._quantity = 13
        self._obj._all_cartons = True
        self._obj.next_state = None
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n\n')
        self.post_dialog_responses('description', 
                                   'ready', 
                                   'ready', 
                                   '2!', 
                                   'yes')
        
        self._obj.runState(BACKSTOCKING_SYSTEM_REASON)
        
        self.validate_prompts('Reason?', 
                              '1, reason 1', 
                              '2, reason 2', 
                              'Reason?', 
                              'reason 2, correct?')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '11', '1'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason_code, '2')

    def test_xmit_carton(self):
        #initialize data
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj.runState(BACKSTOCKING_SYSTEM_BUILD_ALL)
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_CARTON)
        
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response('1,Error Message\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '0', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        #-----------------------------------------------------------------
        #test code 97 condition
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('97,Special Message\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts('Special Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '0', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_CONFIRM_CARTON)

        #-----------------------------------------------------------------
        #test warning condition
        self._obj._valid_lpn_lut[0]['status'] = 'N'
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 0
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj._shorted = False
        self._obj._canceled = False
        self._obj._stopped = False
        self._obj._quantity = 13
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('99,Warning Message\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '13', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'P')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 13)

        #-----------------------------------------------------------------
        #test no errors, all cartons
        self._obj._valid_lpn_lut[0]['status'] = 'N'
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 0
        self._obj._valid_lpn_lut[0]['all_cartons'] = True
        self._obj._shorted = False
        self._obj._canceled = False
        self._obj._stopped = False
        self._obj._quantity = 13
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '1', '', '', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'N')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 0)
        
        #-----------------------------------------------------------------
        #test no errors, stopped
        self._obj._valid_lpn_lut[0]['status'] = 'N'
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 0
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj._shorted = False
        self._obj._canceled = False
        self._obj._stopped = True
        self._obj._quantity = 13
        
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '', '', '1'])
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'N')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 0)
        
        #-----------------------------------------------------------------
        #test no errors, canceled
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj._valid_lpn_lut[0]['status'] = 'N'
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 0
        self._obj._shorted = False
        self._obj._canceled = True
        self._obj._stopped = False
        self._obj._quantity = 13
        self.assertFalse(self._obj._valid_lpn_lut.canceled_cartons)
        
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '', 'U1', '0', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'P')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 13)
        self.assertTrue(self._obj._valid_lpn_lut.canceled_cartons)
        
        #-----------------------------------------------------------------
        #test no errors, shorted, reason code
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj._valid_lpn_lut[0]['status'] = 'N'
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 0
        self._obj._shorted = True
        self._obj._canceled = False
        self._obj._stopped = False
        self._obj._quantity = 10
        self._obj._reason_code = '1'
        
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '10', '1', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'R')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 10)
        
        #-----------------------------------------------------------------
        #test no errors, full qty
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self._obj._valid_lpn_lut[0]['status'] = 'N'
        self._obj._valid_lpn_lut[0]['quantity_picked'] = 0
        self._obj._shorted = False
        self._obj._canceled = False
        self._obj._stopped = False
        self._obj._quantity = 13
        self._obj._reason_code = ''
        
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_SYSTEM_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '13', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_NEXT_CARTON)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'P')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 13)
    
    def test_next_step(self):
        self._obj._region_rec['goback_shorts_skips'] = False
        self._obj._valid_lpn_lut[0]['status'] = 'S'
        self._obj._valid_lpn_lut[1]['status'] = 'R'

        #-----------------------------------------------------------------
        #test region not allowed to go back
        self._obj.next_state = None
        
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_STEP)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test region allowed to go back with short and skips
        self._obj._region_rec['goback_shorts_skips'] = True
        self._obj.next_state = None
        
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_STEP)
        
        self.validate_prompts('Going back for shorts and skips.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_SYSTEM_BUILD_ALL)
        self.assertEquals(self._obj._valid_lpn_lut[0]['status'], 'N')
        self.assertEquals(self._obj._valid_lpn_lut[1]['status'], 'N')
        
        #-----------------------------------------------------------------
        #test region allowed to go back, not shorts or skips
        self._obj._region_rec['goback_shorts_skips'] = True
        self._obj.next_state = None
        
        self._obj.runState(BACKSTOCKING_SYSTEM_NEXT_STEP)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
if __name__ == '__main__':
    unittest.main()
        