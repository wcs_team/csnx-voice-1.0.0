from BaseVLTestCase import BaseVLTestCase #Needs to be first import
from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from backstocking.BackStockingTask import BackstockingTask
from backstocking.BackStockingOperBuildLPNTask import BackstockingOperBuildLPNTask,\
    BACKSTOCKING_OPER_CONFIRM_CARTON, BACKSTOCKING_OPER_QUANITY,\
    BACKSTOCKING_OPER_XMIT_CARTON, BACKSTOCKING_OPER_NEXT_STEP
import unittest

class testBackStockingOperBuildLPNTask(BaseVLTestCase):
    
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(BackstockingTask, VoiceLink())
        temp._valid_lpn_lut.receive('N,11111SCAN,11111,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,22222SCAN,22222,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,33333SCAN,33333,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')

        self._obj = obj_factory.get(BackstockingOperBuildLPNTask,
                                      '12345',
                                      temp._valid_lpn_lut,
                                      temp.taskRunner, temp)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'backstockingOperBuildLPNTask')
        
        #test states
        self.assertEquals(self._obj.states[0], BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertEquals(self._obj.states[1], BACKSTOCKING_OPER_QUANITY)
        self.assertEquals(self._obj.states[2], BACKSTOCKING_OPER_XMIT_CARTON)
        self.assertEquals(self._obj.states[3], BACKSTOCKING_OPER_NEXT_STEP)
        
        #test LUTS defined correctly
        fields = self._obj._carton_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)
    
    def test_confirm_carton(self):
        #-----------------------------------------------------------------
        #Test stop spoken, no to confirm
        self._obj.next_state = None
        self.post_dialog_responses('stop', 'no')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'Stop build, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test stop spoken, yes to confirm
        self._obj.next_state = None
        self.post_dialog_responses('stop', 'yes')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'Stop build, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_XMIT_CARTON)
        self.assertTrue(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test all cartons spoken, not allowed
        self._obj.next_state = None
        self._obj._valid_lpn_lut[0]['all_cartons'] = False
        self.post_dialog_responses('all cartons', 'ready')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'All cartons not allowed, to continue, say ready.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test all cartons spoken, allowed, no to confirm
        self._obj.next_state = None
        self._obj._valid_lpn_lut[0]['all_cartons'] = True
        self.post_dialog_responses('all cartons', 'no')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'All cartons, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test all cartons spoken, allowed, yes to confirm
        self._obj.next_state = None
        self.post_dialog_responses('all cartons', 'yes')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'All cartons, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_XMIT_CARTON)
        self.assertFalse(self._obj._stopped)
        self.assertTrue(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test license plate spoken
        self._obj.next_state = None
        self.post_dialog_responses('license plate')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'LPN 12345')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test invalid carton spoken
        self._obj.next_state = None
        self.post_dialog_responses('44444!')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'Wrong 44444')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test invalid carton scanned
        self._obj.next_state = None
        self.post_dialog_responses('#44444SCAN')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?', 'Wrong 44444SCAN')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, None)
        
        #-----------------------------------------------------------------
        #Test valid carton spoken
        self._obj.next_state = None
        self.post_dialog_responses('22222!')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, self._obj._valid_lpn_lut[1])
        
        #-----------------------------------------------------------------
        #Test valid carton scanned
        self._obj.next_state = None
        self.post_dialog_responses('#33333SCAN')
        
        self._obj.runState(BACKSTOCKING_OPER_CONFIRM_CARTON)

        self.validate_prompts('Carton?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertFalse(self._obj._stopped)
        self.assertFalse(self._obj._all_cartons)
        self.assertEquals(self._obj._quantity, 0)
        self.assertEquals(self._obj._valid_lpn_curr, self._obj._valid_lpn_lut[2])
        
    def test_quantity(self):
        self._obj._valid_lpn_curr = self._obj._valid_lpn_lut[0]
        self._obj._carton = 'spoken carton'
        
        #-----------------------------------------------------------------
        #Test cancel spoken, no, then yes
        self._obj.next_state = None
        self.post_dialog_responses('cancel', 'no', 'cancel', 'yes')
        
        self._obj.runState(BACKSTOCKING_OPER_QUANITY)

        self.validate_prompts('Quantity?', 
                              'cancel, correct?', 
                              'Quantity?', 
                              'cancel, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertEquals(self._obj._quantity, 0)

        #-----------------------------------------------------------------
        #Test quantity to high
        self._obj.next_state = None
        self.post_dialog_responses('20!', 'ready')
        
        self._obj.runState(BACKSTOCKING_OPER_QUANITY)

        self.validate_prompts('Quantity?',
                              'You said 20, only asked for 13, try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertEquals(self._obj._quantity, 20)
        
        #-----------------------------------------------------------------
        #Test quantity to high (set quantity picked)
        self._obj._valid_lpn_curr['quantity_picked'] = 2
        self._obj.next_state = None
        self.post_dialog_responses('13!', 'ready')
        
        self._obj.runState(BACKSTOCKING_OPER_QUANITY)

        self.validate_prompts('Quantity?',
                              'You said 13, only asked for 11, try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertEquals(self._obj._quantity, 13)
        
        #-----------------------------------------------------------------
        #Test quantity to low, no to short
        self._obj._valid_lpn_curr['quantity_picked'] = 2
        self._obj.next_state = None
        self.post_dialog_responses('10!', 'no')
        
        self._obj.runState(BACKSTOCKING_OPER_QUANITY)

        self.validate_prompts('Quantity?', '10, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        self.assertEquals(self._obj._quantity, 10)
        
        #-----------------------------------------------------------------
        #Test quantity to low, yes to short
        self._obj._valid_lpn_curr['quantity_picked'] = 2
        self._obj.next_state = None
        self.post_dialog_responses('10!', 'yes')
        
        self._obj.runState(BACKSTOCKING_OPER_QUANITY)

        self.validate_prompts('Quantity?', '10, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._quantity, 10)
        
    def test_xmit_carton(self):
        self._obj._carton = 'spoken carton'
        
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_XMIT_CARTON)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response('1,Error Message\n\n')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', '', '0', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_XMIT_CARTON)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('99,Warning Message\n\n')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', '', '0', '', '0'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test code 97 condition
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('97,Special Message\n\n')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts('Special Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', '', '0', '', '0'])
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)

        #-----------------------------------------------------------------
        #test no errors, all cartons
        self._obj._quantity = 13
        self._obj._all_cartons = True
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '2', '', '', '', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test no errors, stopped
        self._obj._quantity = 13
        self._obj._all_cartons = False
        self._obj._stopped = True
        
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', '', '', '', '1'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test no errors, partial qty
        self._obj._valid_lpn_curr = self._obj._valid_lpn_lut[0]
        self._obj._quantity = 10
        self._obj._stopped = False
        
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '10', '', '0'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'N')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 10)

        #-----------------------------------------------------------------
        #test no errors, full qty
        self._obj._valid_lpn_curr = self._obj._valid_lpn_lut[0]
        self._obj._quantity = 3
        self._obj._stopped = False
        
        self._obj.next_state = None
        self.set_server_response('0,\n\n')

        self._obj.runState(BACKSTOCKING_OPER_XMIT_CARTON)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTStockingCarton', '12345', '0', 'U1', '3', '', '0'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._valid_lpn_curr['status'], 'P')
        self.assertEquals(self._obj._valid_lpn_curr['quantity_picked'], 13)
    
    def test_next_step(self):
        #-----------------------------------------------------------------
        #test stopped
        self._obj._stopped = True
        self._obj._all_cartons = False
        self._obj.next_state = None

        self._obj.runState(BACKSTOCKING_OPER_NEXT_STEP)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test All Cartons
        self._obj._stopped = False
        self._obj._all_cartons = True
        self._obj.next_state = None

        self._obj.runState(BACKSTOCKING_OPER_NEXT_STEP)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test Not all picked
        self._obj._stopped = False
        self._obj._all_cartons = False
        self._obj.next_state = None

        self._obj.runState(BACKSTOCKING_OPER_NEXT_STEP)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, BACKSTOCKING_OPER_CONFIRM_CARTON)
        
        #-----------------------------------------------------------------
        #test all picked
        for carton in self._obj._valid_lpn_lut:
            carton['status'] = 'P'
            
        self._obj._stopped = False
        self._obj._all_cartons = False
        self._obj.next_state = None

        self._obj.runState(BACKSTOCKING_OPER_NEXT_STEP)
        
        self.validate_prompts('Build complete.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
if __name__ == '__main__':
    unittest.main()
