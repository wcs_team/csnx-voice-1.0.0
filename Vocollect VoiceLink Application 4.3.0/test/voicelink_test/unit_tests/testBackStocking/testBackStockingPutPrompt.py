from BaseVLTestCase import BaseVLTestCase, BOTH_SERVERS #Needs to be first import
from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from backstocking.BackStockingVocabularyPut import BackStockingVocabularyPut
from backstocking.BackStockingTask import BackstockingTask
from backstocking.BackStockingPutPrompt import BackStockingPutPromptTask,\
    STOCKING_PUT_VERIFY_SLOT, STOCKING_PUT_VERIFY_CARTON,\
    STOCKING_PUT_GET_QUANTITY, STOCKING_PUT_REASON_CODE, STOCKING_PUT_XMIT_PUT
import unittest
import time

class testBackStockingPutPromptTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(BackstockingTask, VoiceLink())
        temp._valid_lpn_lut.receive('N,11111SCAN,11111,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,22222SCAN,22222,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n'
                                    'N,33333SCAN,33333,13,0,ITEM1,UCN,UPC,Description,1,0,11111,0,U1,prep message,0,\n\n')
        temp._region_config_lut.receive('1,back stocking region 1,pre,aisle,post,1,1,1,1,1,1,0,\n\n')
        temp._trip_lut.receive('1,1,trip summary,performance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                               '1,1,trip summary,performance summary,loc1,11111,N,pre 2,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n\n')

        self._obj = obj_factory.get(BackStockingPutPromptTask,
                                      temp._region_config_lut[0],
                                      temp._trip_lut,
                                      temp._trip_lut[0],
                                      temp.taskRunner, temp)
        self._obj.dynamic_vocab = obj_factory.get(BackStockingVocabularyPut,
                                                    temp._region_config_lut[0],
                                                    temp.taskRunner)
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'backStockingPutTask')
        
        #test states
        self.assertEquals(self._obj.states[0], STOCKING_PUT_VERIFY_SLOT)
        self.assertEquals(self._obj.states[1], STOCKING_PUT_VERIFY_CARTON)
        self.assertEquals(self._obj.states[2], STOCKING_PUT_GET_QUANTITY)
        self.assertEquals(self._obj.states[3], STOCKING_PUT_REASON_CODE)
        self.assertEquals(self._obj.states[4], STOCKING_PUT_XMIT_PUT)
        
        #test LUTS defined correctly
        fields = self._obj._reason_code_lut.fields
        self.assertEquals(fields['code'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)

    def test_verify_slot(self):

        #-----------------------------------------------------------------
        #test no check digits
        self._obj.next_state = None
        self._obj._current_put['check_digit'] = ''
        self.post_dialog_responses('ready')
        
        self._obj.runState(STOCKING_PUT_VERIFY_SLOT)
        
        self.validate_prompts('1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test check digits spoken
        self._obj.next_state = None
        self._obj._current_put['check_digit'] = '00'
        self.post_dialog_responses('11!', '01!', '00!')
        
        self._obj.runState(STOCKING_PUT_VERIFY_SLOT)
        
        self.validate_prompts('1', 
                              'wrong 11, try again', 
                              'wrong 01, try again', 
                              '1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test check digits scanned
        self._obj.next_state = None
        self.post_dialog_responses('#11', '#00')
        
        self._obj.runState(STOCKING_PUT_VERIFY_SLOT)
        
        self.validate_prompts('1', 'wrong 11, try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test skip slot, no
        self._obj.next_state = None
        self.post_dialog_responses('skip slot', 'no')
        
        self._obj.runState(STOCKING_PUT_VERIFY_SLOT)
        
        self.validate_prompts('1', 'Skip slot, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_VERIFY_SLOT)
        
        #-----------------------------------------------------------------
        #test skip slot, yes
        self._obj.next_state = None
        self.post_dialog_responses('skip slot', 'yes')
        
        self._obj.runState(STOCKING_PUT_VERIFY_SLOT)
        
        self.validate_prompts('1', 'Skip slot, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(self._obj._current_put['status'], 'S')
        
        #-----------------------------------------------------------------
        #test skip slot not allowed
        self._obj.next_state = None
        self._obj._region_config_rec['allow_skip_slot'] = False
        self.post_dialog_responses('skip slot')
        
        self._obj.runState(STOCKING_PUT_VERIFY_SLOT)
        
        self.validate_prompts('1', 'skip slot not allowed')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_VERIFY_SLOT)
        self.assertEquals(self._obj._current_put['status'], 'S')
        
    def test_verify_carton(self):
        self._obj._region_config_rec['require_qty_verification'] = False
        self._obj._cancel = False
        
        #-----------------------------------------------------------------
        #test cancel, no
        self._obj.next_state = None
        self.post_dialog_responses('cancel', 'no')
        
        self._obj.runState(STOCKING_PUT_VERIFY_CARTON)
        
        self.validate_prompts('<spell>1234</spell> put 13, stocker prep', 
                              'Cancel carton, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_VERIFY_CARTON)
        self.assertEquals(self._obj._cancel, False)
        
        #-----------------------------------------------------------------
        #test cancel, yes
        self._obj.next_state = None
        self.post_dialog_responses('cancel', 'yes')
        
        self._obj.runState(STOCKING_PUT_VERIFY_CARTON)
        
        self.validate_prompts('<spell>1234</spell> put 13, stocker prep', 
                              'Cancel carton, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_REASON_CODE)
        self.assertEquals(self._obj._cancel, True)
        
        #-----------------------------------------------------------------
        #test short product
        self._obj._cancel = False
        self._obj.next_state = None
        self.post_dialog_responses('short product')
        
        self._obj.runState(STOCKING_PUT_VERIFY_CARTON)
        
        self.validate_prompts('<spell>1234</spell> put 13, stocker prep',
                              'Verify carton.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_VERIFY_CARTON)
        self.assertEquals(self._obj._cancel, False)
        self.assertEquals(self._obj._short_spoken, True)
        self.assertEquals(self._obj._skip_prompt, True)
        
        #-----------------------------------------------------------------
        #test prompt skipped, wrong value entered, then correct value entered
        self._obj.next_state = None
        self.post_dialog_responses('999!', '111!', '222!')
        
        self._obj.runState(STOCKING_PUT_VERIFY_CARTON)
        
        self.validate_prompts('wrong 999, try again',
                              'wrong 111, try again', 
                              '<spell>1234</spell> put 13, stocker prep')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._cancel, False)
        self.assertEquals(self._obj._short_spoken, True)
        self.assertEquals(self._obj._skip_prompt, False)
        
        #-----------------------------------------------------------------
        #test prompt skipped, wrong value entered, then correct value entered
        self._obj._region_config_rec['require_qty_verification'] = True
        self._obj._current_put['spoken_carton_id'] = ''
        
        self._obj.next_state = None
        self.post_dialog_responses('short product', 'ready')
        
        self._obj.runState(STOCKING_PUT_VERIFY_CARTON)
        
        self.validate_prompts('<spell>1234</spell> put 13, stocker prep')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._cancel, False)
        self.assertEquals(self._obj._short_spoken, True)
        self.assertEquals(self._obj._skip_prompt, False)
    
    def test_get_quantity(self):
        
        #-----------------------------------------------------------------
        #test not required
        self._obj.next_state = None
        self._obj._region_config_rec['require_qty_verification'] = False
        self._obj._short_spoken = False
        
        self._obj.runState(STOCKING_PUT_GET_QUANTITY)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._qty_put, self._obj._current_put['quantity'])

        #-----------------------------------------------------------------
        #test short spoken, less entered
        self._obj.next_state = None
        self._obj._region_config_rec['require_qty_verification'] = False
        self._obj._short_spoken = True
        
        self.post_dialog_responses('9!', 'no', '10!', 'yes')
        
        self._obj.runState(STOCKING_PUT_GET_QUANTITY)
        
        self.validate_prompts('How many did you put?',
                              '9, correct?', 
                              'How many did you put?', 
                              '10, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._qty_put, 10)

        #-----------------------------------------------------------------
        #test verify quantity, less entered
        self._obj.next_state = None
        self._obj._region_config_rec['require_qty_verification'] = True
        self._obj._short_spoken = False
        
        self.post_dialog_responses('10!', 'no')
        
        self._obj.runState(STOCKING_PUT_GET_QUANTITY)
        
        self.validate_prompts('Quantity?', 'you said 10, is this a short?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_GET_QUANTITY)
        self.assertEquals(self._obj._qty_put, 10)

        #-----------------------------------------------------------------
        #test verify quantity, less entered
        self._obj.next_state = None
        self._obj._region_config_rec['require_qty_verification'] = True
        self._obj._short_spoken = False
        
        self.post_dialog_responses('10!', 'yes')
        
        self._obj.runState(STOCKING_PUT_GET_QUANTITY)
        
        self.validate_prompts('Quantity?', 'you said 10, is this a short?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._qty_put, 10)

        #-----------------------------------------------------------------
        #test verify quantity, more entered
        self._obj.next_state = None
        self._obj._region_config_rec['require_qty_verification'] = True
        self._obj._short_spoken = False
        
        self.post_dialog_responses('15!')
        
        self._obj.runState(STOCKING_PUT_GET_QUANTITY)
        
        self.validate_prompts('Quantity?', 
                              'You said 15, only asked for 13, try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_GET_QUANTITY)
        self.assertEquals(self._obj._qty_put, 15)


        #-----------------------------------------------------------------
        #test verify quantity, expected entered
        self._obj.next_state = None
        self._obj._region_config_rec['require_qty_verification'] = True
        self._obj._short_spoken = False
        
        self.post_dialog_responses('13!')
        
        self._obj.runState(STOCKING_PUT_GET_QUANTITY)
        
        self.validate_prompts('Quantity?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._qty_put, 13)

    def test_get_reason_code(self):
        #-----------------------------------------------------------------
        #Test no reason code required
        self._obj._cancel = False
        self._obj._qty_put = 13
        
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_PUT_REASON_CODE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason_code, '')

        #-----------------------------------------------------------------
        #test server not started, canceled
        self._obj._cancel = True
        self._obj._qty_put = 13
        self.post_dialog_responses('ready')

        self._obj.runState(STOCKING_PUT_REASON_CODE)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, STOCKING_PUT_REASON_CODE)
        self.assertEquals(self._obj._reason_code, '')
        
        #-----------------------------------------------------------------
        #test error condition, shorted
        self._obj._cancel = False
        self._obj._qty_put = 10
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response(',,1,Error Message\n\n')

        self._obj.runState(STOCKING_PUT_REASON_CODE)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '11', '1'])
        self.assertEquals(self._obj.next_state, STOCKING_PUT_REASON_CODE)
        self.assertEquals(self._obj._reason_code, '')
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self.set_server_response('1,reason 1,99,Warning Message\n'
                                 '2,reason 2,0,\n\n')
        self.post_dialog_responses('ready', 
                                   '1!', 
                                   'yes')

        self._obj.runState(STOCKING_PUT_REASON_CODE)
        
        self.validate_prompts('Warning Message, say ready',
                              'Reason?', 
                              'reason 1, correct?')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '11', '1'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason_code, '1')

        #-----------------------------------------------------------------
        #test no errors
        self._obj.next_state = None
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n\n')
        self.post_dialog_responses('description', 
                                   'ready', 
                                   'ready', 
                                   '2!', 
                                   'yes')
        
        self._obj.runState(STOCKING_PUT_REASON_CODE)
        
        self.validate_prompts('Reason?', 
                              '1, reason 1', 
                              '2, reason 2', 
                              'Reason?', 
                              'reason 2, correct?')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '11', '1'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason_code, '2')

        #-----------------------------------------------------------------
        #test 1 reason code
        self._obj.next_state = None
        self._obj._reason_code = ''
        self.set_server_response('1,reason 1,0,\n\n')
        self.post_dialog_responses()
        
        self._obj.runState(STOCKING_PUT_REASON_CODE)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '11', '1'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._reason_code, '1')

    def test_xmit_put(self):
        self.start_server(BOTH_SERVERS)
    
        #-----------------------------------------------------------------
        #test transmit canceled
        self._obj.next_state = None
        self._obj._qty_put = 0
        self._obj._cancel = True
        self._obj._reason_code = '1'
        
        self._obj.runState(STOCKING_PUT_XMIT_PUT)
        time.sleep(1)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '0', '13', '1'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._current_put['status'], 'C')
        
        #-----------------------------------------------------------------
        #test transmit shorted
        self._obj.next_state = None
        self._obj._qty_put = 10
        self._obj._cancel = False
        self._obj._reason_code = '2'
        
        self._obj.runState(STOCKING_PUT_XMIT_PUT)
        time.sleep(1)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '10', '3', '2'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._current_put['status'], 'R')
        
        #-----------------------------------------------------------------
        #test transmit normal
        self._obj.next_state = None
        self._obj._qty_put = 13
        self._obj._cancel = False
        self._obj._reason_code = ''
        
        self._obj.runState(STOCKING_PUT_XMIT_PUT)
        time.sleep(1)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '13', '0', ''])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._current_put['status'], 'P')
        
if __name__ == '__main__':
    unittest.main()
    