from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from puttostore.PutToStoreTask import PutToStoreTask
from core.VoiceLink import VoiceLink
from common.RegionSelectionTasks import SingleRegionTask
from common.RegionSelectionTasks import GET_VALID_REGIONS, REQUEST_REGIONS, GET_REGION_CONFIG
import unittest

class testMultipleRegionTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(SingleRegionTask,
                                      VoiceLink(), 
                                      obj_factory.get(PutToStoreTask))
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'taskSingleRegion')

        #test states
        self.assertEquals(self._obj.states[0], GET_VALID_REGIONS)
        self.assertEquals(self._obj.states[1], REQUEST_REGIONS)
        self.assertEquals(self._obj.states[2], GET_REGION_CONFIG)
        
    def test_get_valid_regions(self):
        #---------------------------------------------------------------------------
        #RALLYTC: 969 Test Error contacting host Valid Regions LUT
        self.set_server_response('1,region 1,1,Error Message\n\n', 'prTaskLUTPtsValidRegions')
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(self._obj.next_state, GET_VALID_REGIONS) #current state should not have changed
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')
        self._obj.next_state = None
        
        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response 1 or more regions
        self.set_server_response('1,region 1,0,\n\n', 'prTaskLUTPtsValidRegions')

        self._obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTPtsValidRegions'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #Test valid response 0 regions
        self.set_server_response('-1,region 1,0,\n\n', 'prTaskLUTPtsValidRegions')
        
        self._obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(self._obj.next_state, '') 
        self.validate_server_requests(['prTaskLUTPtsValidRegions'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #RALLYTC: 968 Test Error response Valid Regions LUT
        self.set_server_response('1,region 1,1,Error Message\n\n', 'prTaskLUTPtsValidRegions')
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(self._obj.next_state, GET_VALID_REGIONS) #current state should not have changed
        self.validate_server_requests(['prTaskLUTPtsValidRegions'])
        self.validate_prompts('Error Message, To continue say ready')
        
    def test_request_regions(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #RALLYTC: 966 - Test 1 region
        self._obj._valid_regions_lut.receive('9,region 9,0,\n\n')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        self.validate_prompts()
        self.assertEquals(self._obj.selected_region, 9)
        
        #---------------------------------------------------------------------------
        #RALLYTC: 967 - Test multiple regions
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.post_dialog_responses('2', 'yes')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests()
        self.validate_prompts('Region?', 
                              'region 2, correct?')
        self.assertEquals(self._obj.selected_region, '2')
        
    def test_get_region_config(self):
        #---------------------------------------------------------------------------
        #RALLYTC: 970 Test Error contacting host Region config LUT
        self.post_dialog_responses('ready')

        self._obj.runState(GET_REGION_CONFIG)

        self.assertEquals(self._obj.next_state, GET_REGION_CONFIG) #current state should not have changed
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')
        self._obj.next_state = None
        
        self.start_server()
        #---------------------------------------------------------------------------
        #Test valid response 
        self.set_server_response('1,put to store region 1,1,1,1,1,1,1,3,1,5,1,1,5,1,5,2,1,pre,aisle,post,0,0,\n\n', 'prTaskLUTPtsGetRegionConfiguration')
        self._obj.selected_region = 1
        
        self._obj.runState(GET_REGION_CONFIG)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTPtsGetRegionConfiguration', '1'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #RALLYTC: 971 Test Error response region configuration
        self.set_server_response('1,put to store region 1,1,1,1,1,1,1,3,1,5,1,1,5,1,5,2,1,pre,aisle,post,slot,0,1,Error Message\n\n', 'prTaskLUTPtsGetRegionConfiguration')
        self._obj.selected_region = 2
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_REGION_CONFIG)
        
        self.assertEquals(self._obj.next_state, GET_VALID_REGIONS) 
        self.validate_server_requests(['prTaskLUTPtsGetRegionConfiguration', '2'])
        self.validate_prompts('Error Message, To continue say ready')
        
if __name__ == '__main__':
    unittest.main()
