from BaseVLTestCase import BaseVLTestCase, ODR_SERVER #Needs to be first import

from common.VoiceLinkLut import VoiceLinkLut, VoiceLinkOdr, VoiceLinkLutOdr, transport
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
import time
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from vocollect_core import obj_factory



class testVoiceLinkLuts(BaseVLTestCase):

    def setUp(self):
        self.clear()
        transport._timeout = 9

        some_var = CoreTask(VoiceLink())
        some_var.taskRunner._append(some_var)
        TaskRunnerBase._main_runner = some_var.taskRunner
        
        self._obj = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreBreakTypes')
        self._odr = obj_factory.get(VoiceLinkOdr, 'prTaskODRCoreSendBreakInfo')
        self._lut_odr_conn = obj_factory.get(
            VoiceLinkLutOdr, 'prTaskLUTCoreBreakTypes', 'prTaskODRCoreSendBreakInfo', 0)
        
    def test_no_server(self):
        self.post_dialog_responses('ready')
        result = self._obj.do_transmit()
        self.assertEquals(result, -1)
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        
    def test_99_response(self):
        self.start_server()
        self.set_server_response(',,99,Warning\n\n')
        self.post_dialog_responses('ready')

        result = self._obj.do_transmit()
        
        self.validate_prompts('Warning, say ready')
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'])
        self.assertEquals(result, 0)
        
    def test_98_response(self):
        self.start_server()
        self.set_server_response(',,98,Critical Error\n\n')
        self.set_server_response('0,\n\n')
        self.post_dialog_responses('sign off')

        self.assertRaises(Launch, self._obj.do_transmit)
                
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'],['prTaskLUTCoreSignOff'])
        self.validate_prompts('Critical Error, say sign off')
        
    def test_general_error_response(self):
        self.start_server()
        self.set_server_response(',,10,Error\n\n')
        self.post_dialog_responses('ready')

        result = self._obj.do_transmit()
        
        self.validate_prompts('Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'])
        self.assertEquals(result, 10)
        
        #---------------------------------------------------------
        #test sign off, no
        self.set_server_response(',,10,Error\n\n')
        self.post_dialog_responses('sign off', 'no', 'ready')

        result = self._obj.do_transmit()
        
        self.validate_prompts('Error, To continue say ready',
                              'sign off, correct?', 
                              'Error, To continue say ready')
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'])
        self.assertEquals(result, 10)
        
        #---------------------------------------------------------
        #test sign off, yes
        self.set_server_response(',,10,Error\n\n')
        self.post_dialog_responses('sign off', 'yes')

        self.assertRaises(Launch, self._obj.do_transmit)
        
        self.validate_prompts('Error, To continue say ready',
                              'sign off, correct?')
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOff'])
        
        
        
    def test_time_out(self):
        self.start_server()
        self.set_server_response(',,10,Error')
        self.post_dialog_responses('ready')

        result = self._obj.do_transmit()
        
#Commented out this code after consulting with QA because of inconsistent number of beeps.
        
#        self.validate_prompts('beep', 'beep', 'beep', 'beep',
#                              'Error contacting host,  to try again say ready')
        
        self.validate_server_requests(['prTaskLUTCoreBreakTypes'])
        self.assertEquals(result, -1)
        
    def test_has_pending_odr(self):
        self._odr.do_transmit(1,0,'lunch')
        #Make sure there is 1 pending ODR
        self.assertEquals(1, self._odr.has_pending_odr())       
        #Start ODR server
        self.start_server(ODR_SERVER)           
        self.set_server_response('Y')
        time.sleep(1)
        self.validate_server_requests(['prTaskODRCoreSendBreakInfo', '1','0','lunch'])
        #Verify no more pending ODRs
        self.assertEquals(0, self._odr.has_pending_odr())
        
    def test_voicelinklutodr(self):

        #Flag equal to 0 -- Send ODR
        self._lut_odr_conn.do_transmit(1,0,'lunch')
        self.validate_server_requests()       
        #Turn on ODR server
        self.assertEquals(1, self._lut_odr_conn._odr_conn.has_pending_odr())

        self.start_server(ODR_SERVER)
        self.set_server_response('Y')
        count = 0
        while self._lut_odr_conn._odr_conn.has_pending_odr() > 0 and count < 200:
            time.sleep(0.2)
            count += 1
        
        self.validate_server_requests(['prTaskODRCoreSendBreakInfo', '1','0','lunch'])
        self.stop_server(ODR_SERVER)
        self.assertEquals(0, self._lut_odr_conn._odr_conn.has_pending_odr())
               
        #Flag set to 2 -- Send LUT
        self._lut_odr_conn = VoiceLinkLutOdr('prTaskLUTCoreBreakTypes', 'prTaskODRCoreSendBreakInfo', 2)
        #Turn on LUT server
        self.start_server()
        self._lut_odr_conn.do_transmit(1,0,'lunch')
        self.set_server_response('1,lunch,0,\n\n', 'prTaskLUTCoreBreakTypes')
        self.validate_server_requests(['prTaskLUTCoreBreakTypes', '1','0','lunch'])
        self.stop_server()     
        self.assertEquals(0, self._lut_odr_conn._odr_conn.has_pending_odr())
        
        #Flag set to 1 and no pending ODRs -- Send LUT
        self._lut_odr_conn = VoiceLinkLutOdr('prTaskLUTCoreBreakTypes', 'prTaskODRCoreSendBreakInfo', 1)
        #Turn on LUT server
        self.start_server()
        self._lut_odr_conn.do_transmit(1,0,'lunch')
        self.assertEquals(0, self._lut_odr_conn._odr_conn.has_pending_odr())
        self.set_server_response('1,lunch,0,\n\n', 'prTaskLUTCoreBreakTypes')
        self.validate_server_requests(['prTaskLUTCoreBreakTypes', '1','0','lunch'])
        self.stop_server()
        
        #Flag set to 1 and pending ODRs -- Send ODR
        self._lut_odr_conn = VoiceLinkLutOdr('prTaskLUTCoreBreakTypes', 'prTaskODRCoreSendBreakInfo', 1)
        self._lut_odr_conn.do_transmit(1,0,'lunch')
        self.validate_server_requests()
        self.assertEquals(1, self._lut_odr_conn._odr_conn.has_pending_odr())
        #Turn on ODR server
        self.start_server(ODR_SERVER)
        self.set_server_response('Y')
        
        count = 0
        while self._lut_odr_conn._odr_conn.has_pending_odr() > 0 and count < 200:
            time.sleep(0.2)
            count += 1
        
        self.validate_server_requests(['prTaskODRCoreSendBreakInfo', '1','0','lunch'])
        self.stop_server(ODR_SERVER)
        