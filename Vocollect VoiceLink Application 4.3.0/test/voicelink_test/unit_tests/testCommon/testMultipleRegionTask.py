from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from forkapps.PutawayTask import PutawayTask
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
from common.RegionSelectionTasks import MultipleRegionTask
from common.RegionSelectionTasks import GET_VALID_REGIONS, REQUEST_REGIONS, GET_REGION_CONFIG

import unittest
from vocollect_core.task.task_runner import Launch, TaskRunnerBase

class testMultipleRegionTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(MultipleRegionTask, VoiceLink(), 
                                      obj_factory.get(PutawayTask))
        temp = obj_factory.get(CoreTask, self._obj.taskRunner)
        self._obj.taskRunner._append(temp)
        temp.function = 1
        TaskRunnerBase._main_runner = self._obj.taskRunner
        
        
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'taskMultiRegions')

        #test states
        self.assertEquals(self._obj.states[0], GET_VALID_REGIONS)
        self.assertEquals(self._obj.states[1], REQUEST_REGIONS)
        self.assertEquals(self._obj.states[2], GET_REGION_CONFIG)
        
    def test_get_valid_regions(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response 1 or more regions
        self.set_server_response('1,region 1,0,\n\n', 'prTaskLUTForkValidPutAwayRegions')

        self._obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTForkValidPutAwayRegions'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #Test valid response 0 regions
        self.set_server_response('-1,region 1,0,\n\n', 'prTaskLUTForkValidPutAwayRegions')
        
        self._obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(self._obj.next_state, '') 
        self.validate_server_requests(['prTaskLUTForkValidPutAwayRegions'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #Test Error response
        self.set_server_response('1,region 1,1,Error Message\n\n', 'prTaskLUTForkValidPutAwayRegions')
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_VALID_REGIONS)
        
        self.assertEquals(self._obj.next_state, GET_VALID_REGIONS) #current state should not have changed
        self.validate_server_requests(['prTaskLUTForkValidPutAwayRegions'])
        self.validate_prompts('Error Message, To continue say ready')
        
    def test_request_regions(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test 1 region
        
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self.set_server_response('0,\n\n', 'prTaskLUTForkRequestPutAwayRegion')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTForkRequestPutAwayRegion', '1', '0'])
        self.validate_prompts()
        
        #---------------------------------------------------------------------------
        #Test multiple regions speak no more, select 1 region, no to another
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('0,\n\n', 'prTaskLUTForkRequestPutAwayRegion')
        self.post_dialog_responses('no more', '1', 'yes', 'no')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTForkRequestPutAwayRegion', '1', '0'])
        self.validate_prompts('Region?',
                              'No more not valid until a region is specified.',
                              'Region?',
                              'region 1, correct?',
                              'Another Region?')
        
        #---------------------------------------------------------------------------
        #Test multiple regions speak no more, select 1 region, yes to another, then no more
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('0,\n\n', 'prTaskLUTForkRequestPutAwayRegion')
        self.post_dialog_responses('no more', 
                                   '1', 
                                   'yes', 
                                   'yes', 
                                   'no more', 
                                   'no', 
                                   'no more', 
                                   'yes')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTForkRequestPutAwayRegion', '1', '0'])
        self.validate_prompts('Region?',
                              'No more not valid until a region is specified.',
                              'Region?',
                              'region 1, correct?',
                              'Another Region?',
                              'Region?',
                              'No more, correct?',
                              'Region?',
                              'No more, correct?')
        
        #---------------------------------------------------------------------------
        #Test multiple regions select all individually
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('0,\n\n', 'prTaskLUTForkRequestPutAwayRegion')
        self.post_dialog_responses('1', 'yes', 'yes', '2', 'yes')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTForkRequestPutAwayRegion', '1', '0'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '2', '0'])
        self.validate_prompts('Region?',
                              'region 1, correct?',
                              'Another Region?',
                              'Region?',
                              'region 2, correct?')
        
        #---------------------------------------------------------------------------
        #Test multiple speaking all
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('0,\n\n', 'prTaskLUTForkRequestPutAwayRegion')
        self.post_dialog_responses('all', 'no', 'all', 'yes')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTForkRequestPutAwayRegion', '', '1'])
        self.validate_prompts('Region?',
                              'all, correct?',
                              'Region?',
                              'all, correct?')
        
        
        #---------------------------------------------------------------------------
        #Test multiple select 1 then speaking all
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('0,\n\n', 'prTaskLUTForkRequestPutAwayRegion')
        self.post_dialog_responses('1', 'yes', 'yes', 'all', 'yes')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTForkRequestPutAwayRegion', '1', '0'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '', '1'])
        self.validate_prompts('Region?',
                              'region 1, correct?',
                              'Another Region?',
                              'Region?',
                              'all, correct?')

        #---------------------------------------------------------------------------
        #Test change function no
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')
        
        self._obj.runState(REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, REQUEST_REGIONS)
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts('Region?',
                              'change function, correct?')
        
        #---------------------------------------------------------------------------
        #Test change function yes
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, REQUEST_REGIONS)
        
        self.assertEquals(self._obj.next_state, REQUEST_REGIONS)
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts('Region?',
                              'change function, correct?')
        
    def test_request_regions_only_one_allowed(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test multiple regions speak select 1 region
        self._obj._valid_regions_lut.receive('1,region 1,0,\n2,region 2,0,\n\n')
        self.set_server_response('0,\n\n', 'prTaskLUTForkRequestPutAwayRegion')
        self.post_dialog_responses('1', 'yes')

        self._obj.allow_only_one = True
        self._obj.runState(REQUEST_REGIONS)
        self._obj.allow_only_one = False
        
        self.assertEquals(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTForkRequestPutAwayRegion', '1'])
        self.validate_prompts('Region?',
                              'region 1, correct?')
        
    def test_get_region_config(self):
        self.start_server()

        #---------------------------------------------------------------------------
        #Test valid response 
        self.set_server_response('1,region 1,0,0,0,0,0,0,0,0,5,5,5,2,3,exception location,0,0,\n\n', 'prTaskLUTForkPutAwayRegionConfiguration')
        
        self._obj.runState(GET_REGION_CONFIG)
        
        self.assertEquals(self._obj.next_state, None) #Next state should be None
        self.validate_server_requests(['prTaskLUTForkPutAwayRegionConfiguration'])
        self.validate_prompts()

        #---------------------------------------------------------------------------
        #Test Error response
        self.set_server_response('1,region 1,0,0,0,0,0,0,0,0,5,5,5,2,3,exception location,0,1,Error Message\n\n', 'prTaskLUTForkPutAwayRegionConfiguration')
        self.post_dialog_responses('ready')
        
        self._obj.runState(GET_REGION_CONFIG)
        
        self.assertEquals(self._obj.next_state, GET_VALID_REGIONS) 
        self.validate_server_requests(['prTaskLUTForkPutAwayRegionConfiguration'])
        self.validate_prompts('Error Message, To continue say ready')
        
if __name__ == '__main__':
    unittest.main()
