from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from lineloading.LineLoadingTask import LineLoadingTask
from lineloading.CloseStop import CloseStop, PALLET, PRINTER, XMIT_CLOSE, END


class testLineLoadingTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(LineLoadingTask, VoiceLink())
        temp._region_config_lut.receive('1,region,1,1,0,\n\n')
        self._obj = obj_factory.get(CloseStop,
                                      temp._region_config_lut[0], 
                                      temp.taskRunner, temp)
        
    
    def test_initialize_states(self):
        self.assertEquals(self._obj.name, 'closeStop')

        #test states
        self.assertEquals(self._obj.states[0], PALLET)
        self.assertEquals(self._obj.states[1], PRINTER)
        self.assertEquals(self._obj.states[2], XMIT_CLOSE)
        self.assertEquals(self._obj.states[3], END)
        
        #test LUTS defined correctly
        fields = self._obj._close_stop_lut.fields
        self.assertEquals(fields['route'].index, 0)
        self.assertEquals(fields['stop'].index, 1)
        self.assertEquals(fields['palletCount'].index, 2)
        self.assertEquals(fields['errorCode'].index, 3)
        self.assertEquals(fields['errorMessage'].index, 4)
        
    def test_pallet(self):
        
        #test normal flow
        self.post_dialog_responses('A99ready', 'yes')
        self._obj.runState(PALLET)
        self.assertEqual(self._obj._pallet, 'A99')
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('Enter pallet for stop to close',
                              '<spell>A99</spell>, correct?')
        
        #test cancel
        self.post_dialog_responses('cancel')
        self._obj.runState(PALLET)
        self.assertEqual(self._obj.next_state, '')
        self.validate_prompts('Enter pallet for stop to close')
        
    def test_printer(self):
        
        #test normal flow
        self.post_dialog_responses('1!', 'yes')
        self._obj.runState(PRINTER)
        self.assertEqual(self._obj._printer, '1')
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('Printer?',
                              '1, correct?')
        
        #test cancel
        self.post_dialog_responses('cancel')
        self._obj.runState(PRINTER)
        self.assertEqual(self._obj.next_state, '')
        self.validate_prompts('Printer?')
        
        #test no auto print
        self._obj.next_state = None
        self._obj._printer = None
        self._obj._region['auto_print_manifest'] = False
        self._obj.runState(PRINTER)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts()
        self.assertEqual(self._obj._printer, None)

    def test_xmit_close(self):
        self._obj._pallet = '99'
        self._obj._printer = None
        
        #test error contacting host
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_CLOSE)
        self.assertEqual(self._obj.next_state, XMIT_CLOSE)
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

        self.start_server()

        #Test error message from host
        self.set_server_response(',,,1,Error Message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_CLOSE)
        self.assertEqual(self._obj.next_state, PALLET)
        self.validate_server_requests(['prTaskLUTLineLoadingCloseStop', '99', ''])
        self.validate_prompts('Error Message, To continue say ready')
        
        #Test success
        self._obj._printer = '1'
        self._obj.next_state = None
        self.set_server_response('R1,S1,33,0,\n\n')
        self._obj.runState(XMIT_CLOSE)
        self.assertEqual(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTLineLoadingCloseStop', '99', '1'])
        self.validate_prompts()
    
    def test_end(self):
        self._obj._close_stop_lut.receive('R1,S1,33,0,\n\n')
        #test normal flow
        self.post_dialog_responses('ready')
        self._obj.runState(END)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('Route <spell>R1</spell>, stop <spell>S1</spell> is closed. Take away 33. To continue, say ready.')
