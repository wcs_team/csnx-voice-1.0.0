from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from lineloading.LineLoadingTask import LineLoadingTask


class testLineLoadVocabulary(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(LineLoadingTask, VoiceLink())
    
    def test_valid(self):
        self._obj._spur = None
        self._obj._carton = None
        
        self.assertFalse(self._obj.dynamic_vocab._valid('spur'))
        self.assertFalse(self._obj.dynamic_vocab._valid('route'))
        
        self._obj._spur = '99'
        self._obj._carton = None
        
        self.assertTrue(self._obj.dynamic_vocab._valid('spur'))
        self.assertFalse(self._obj.dynamic_vocab._valid('route'))
        
        self._obj._spur = None
        self._obj._carton = '88'
        
        self.assertFalse(self._obj.dynamic_vocab._valid('spur'))
        self.assertTrue(self._obj.dynamic_vocab._valid('route'))
        
        self._obj._spur = '99'
        self._obj._carton = '88'
        
        self.assertTrue(self._obj.dynamic_vocab._valid('spur'))
        self.assertTrue(self._obj.dynamic_vocab._valid('route'))

        self.assertFalse(self._obj.dynamic_vocab._valid('dummy word'))
        
    def test_spur(self):
        self._obj._spur = '99'
        
        self.assertTrue(self._obj.dynamic_vocab._spur())
        self.validate_prompts('9 9')
        
    def test_route(self):
        self._obj._carton = '88'
        self._obj._carton_lut.receive('R1,S1,0,,\n\n')
        
        self.assertTrue(self._obj.dynamic_vocab._route())
        self.validate_prompts('Route <spell>R1</spell>, Stop <spell>S1</spell>')
        