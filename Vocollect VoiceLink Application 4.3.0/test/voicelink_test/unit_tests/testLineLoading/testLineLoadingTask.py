from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from common.RegionSelectionTasks import MultipleRegionTask
from core.VoiceLink import VoiceLink
from core.CoreTask import CoreTask
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from lineloading.LineLoadingTask import LineLoadingTask
from lineloading.LineLoadingTask import REGIONS, VALIDATE_REGION, ENTER_SPUR
from lineloading.LineLoadingTask import XMIT_SPUR, ENTER_CARTON, XMIT_CARTON
from lineloading.LineLoadingTask import ENTER_PALLET, XMIT_PALLET
import unittest

class testLineLoadingTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(LineLoadingTask, VoiceLink())
        self._obj.taskRunner._append(self._obj)
        
        
        core = obj_factory.get(CoreTask, self._obj)
        core.function = 7
        self._obj.taskRunner._append(core)
        TaskRunnerBase._main_runner = self._obj.taskRunner

    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'lineLoading')

        #test states
        self.assertEquals(self._obj.states[0], REGIONS)
        self.assertEquals(self._obj.states[1], VALIDATE_REGION)
        self.assertEquals(self._obj.states[2], ENTER_SPUR)
        self.assertEquals(self._obj.states[3], XMIT_SPUR)
        self.assertEquals(self._obj.states[4], ENTER_CARTON)
        self.assertEquals(self._obj.states[5], XMIT_CARTON)
        self.assertEquals(self._obj.states[6], ENTER_PALLET)
        self.assertEquals(self._obj.states[7], XMIT_PALLET)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._request_reqion_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['allow_close_stop'].index, 2)
        self.assertEquals(fields['auto_print_manifest'].index, 3)
        self.assertEquals(fields['errorCode'].index, 4)
        self.assertEquals(fields['errorMessage'].index, 5)

        fields = self._obj._confirm_spur_lut.fields
        self.assertEquals(fields['spur'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        fields = self._obj._carton_lut.fields
        self.assertEquals(fields['route'].index, 0)
        self.assertEquals(fields['stop'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._pallet_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, MultipleRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskMultiRegions')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, VALIDATE_REGION)
    
    def test_validate_region(self):
        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj.runState(VALIDATE_REGION)

        self.validate_prompts('not authorized for any regions, see your supervisor')
        self.validate_server_requests()
        self.assertEqual(self._obj.next_state, '')
        self.assertFalse(self._obj._region_selected)

        #-----------------------------------------------------------------
        #Test authorized regions
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')

        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertTrue(self._obj._region_selected)

    def test_enter_spur(self):
        self.post_dialog_responses('99ready', 'yes')
        self._obj.runState(ENTER_SPUR)
        self.assertEqual(self._obj._spur, '99')
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('spur?',
                              '99, correct?')
        
    def test_xmit_spur(self):
        self._obj._spur = '99'

        #test error contacting host
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_SPUR)
        self.assertEqual(self._obj.next_state, XMIT_SPUR)
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

        self.start_server()

        #Test error message from host
        self.set_server_response(',1,Error Message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_SPUR)
        self.assertEqual(self._obj.next_state, ENTER_SPUR)
        self.validate_server_requests(['prTaskLUTLineLoadingConfirmSpur', '99'])
        self.validate_prompts('Error Message, To continue say ready')
        
        #Test success
        self.set_server_response('98,0,\n\n')
        self._obj.runState(XMIT_SPUR)
        self.assertEqual(self._obj.next_state, ENTER_SPUR)
        self.validate_server_requests(['prTaskLUTLineLoadingConfirmSpur', '99'])
        self.validate_prompts()
        self.assertEqual(self._obj._spur, '98')
        
    def test_enter_carton(self):
        
        #Test normal flow
        self.post_dialog_responses('11ready', 'yes')
        self._obj.runState(ENTER_CARTON)
        self.assertEqual(self._obj._carton, '11')
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('carton?',
                              '11, correct?')
        
        #Test change spur
        self.post_dialog_responses('change spur')
        self._obj.runState(ENTER_CARTON)
        self.assertEqual(self._obj._carton, None)
        self.assertEqual(self._obj.next_state, ENTER_SPUR)
        self.validate_prompts('carton?')
        
        #test close stop allowed
        self._obj.next_state = None
        self._obj._region_config_lut.receive('1,region,1,1,0,\n\n')
        self.post_dialog_responses('close stop')
        self.assertRaises(Launch, self._obj.runState, ENTER_CARTON)
        self.assertEqual(self._obj._carton, None)
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('carton?')
        
        #test close stop allowed
        self._obj.next_state = None
        self._obj._region_config_lut.receive('1,region,0,1,0,\n\n')
        self.post_dialog_responses('close stop')
        self._obj.runState(ENTER_CARTON)
        self.assertEqual(self._obj._carton, None)
        self.assertEqual(self._obj.next_state, ENTER_CARTON)
        self.validate_prompts('carton?')
        
        #test change function allowed
        self.start_server()
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n')
        self.post_dialog_responses('change function', 'no')
        self._obj.runState(ENTER_CARTON)
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts('carton?','change function, correct?')
        self.assertEqual(self._obj.next_state, ENTER_CARTON)

        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n')
        self.post_dialog_responses('change function', 'yes')
        
        self.assertRaises(Launch, self._obj.runState, ENTER_CARTON)
        
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.validate_prompts('carton?','change function, correct?')
        
        self.stop_server()
        
    def test_xmit_carton(self):
        self._obj._spur = '99'
        self._obj._carton = '11'
        
        #test error contacting host
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_CARTON)
        self.assertEqual(self._obj.next_state, XMIT_CARTON)
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

        self.start_server()

        #Test error message from host
        self.set_server_response(',,1,Error Message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_CARTON)
        self.assertEqual(self._obj.next_state, ENTER_CARTON)
        self.validate_server_requests(['prTaskLUTLineLoadingCarton', '11', '0', '99'])
        self.validate_prompts('Error Message, To continue say ready')
        
        #Test success
        self._obj.next_state = None
        self.set_server_response('R1,S1,0,\n\n')
        self._obj.runState(XMIT_CARTON)
        self.assertEqual(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTLineLoadingCarton', '11', '0', '99'])
        self.validate_prompts()
        
        #Test success, canceled cartons
        self._obj.next_state = None
        self._obj._carton_canceled = True
        
        self.post_dialog_responses('ready')
        self.set_server_response('R1,S1,0,\n\n')
        self._obj.runState(XMIT_CARTON)
        self.assertEqual(self._obj.next_state, ENTER_CARTON)
        self.assertEqual(self._obj._carton_canceled, False)
        self.validate_server_requests(['prTaskLUTLineLoadingCarton', '11', '1', '99'])
        self.validate_prompts('Set carton aside. To continue, say ready.')

        #Test error code 2, no
        self._obj.next_state = None
        self.post_dialog_responses('no', 'ready')
        self.set_server_response('R1,S1,2,move carton?\n\n')
        self._obj.runState(XMIT_CARTON)
        self.assertEqual(self._obj.next_state, ENTER_CARTON)
        self.validate_server_requests(['prTaskLUTLineLoadingCarton', '11', '0', '99'])
        self.validate_prompts('Move carton?', 
                              'Leave carton on pallet.')
        
        #Test error code 2, yes
        self._obj.next_state = None
        self.post_dialog_responses('yes')
        self.set_server_response('R1,S1,2,move carton?\n\n')
        self._obj.runState(XMIT_CARTON)
        self.assertEqual(self._obj.next_state, None)
        self.validate_server_requests(['prTaskLUTLineLoadingCarton', '11', '0', '99'])
        self.validate_prompts('Move carton?')
        
        
        
    def test_enter_pallet(self):
        self._obj._carton_lut.receive('R1,S1,0,\n\n')
        self._obj._carton = '11'
        
        #Test normal flow
        self._obj._pallet_new = False
        self.post_dialog_responses('22ready', 'yes')
        self._obj.runState(ENTER_PALLET)
        self.assertEqual(self._obj._pallet, '22')
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('<spell>R 1</spell>, <spell>S 1</spell>, pallet?',
                              '22, correct?')
        
        #Test normal flow, after new pallet
        self._obj._pallet_new = True
        self.post_dialog_responses('22ready', 'yes')
        self._obj.runState(ENTER_PALLET)
        self.assertEqual(self._obj._pallet, '22')
        self.assertEqual(self._obj.next_state, None)
        self.validate_prompts('<spell>R 1</spell>, <spell>S 1</spell>, new pallet?',
                              '22, correct?')
        
        #Test speaking new pallet
        self._obj._pallet_new = False
        self.post_dialog_responses('new pallet')
        self._obj.runState(ENTER_PALLET)
        self.assertEqual(self._obj._pallet, None)
        self.assertEqual(self._obj.next_state, ENTER_PALLET)
        self.validate_prompts('<spell>R 1</spell>, <spell>S 1</spell>, pallet?')
        self.assertTrue(self._obj._pallet_new)
        
        #Test speaking, cancel, no
        self._obj._pallet_new = False
        self.post_dialog_responses('cancel', 'no')
        self._obj.runState(ENTER_PALLET)
        self.assertEqual(self._obj._pallet, None)
        self.assertEqual(self._obj.next_state, ENTER_PALLET)
        self.validate_prompts('<spell>R 1</spell>, <spell>S 1</spell>, pallet?',
                              'Abort loading carton <spell>1 1</spell>?')
        self.assertFalse(self._obj._pallet_new)
        self.assertFalse(self._obj._carton_canceled)
        
        #Test speaking, cancel, yes
        self._obj._pallet_new = True
        self.post_dialog_responses('cancel', 'yes')
        self._obj.runState(ENTER_PALLET)
        self.assertEqual(self._obj._pallet, None)
        self.assertEqual(self._obj.next_state, XMIT_CARTON)
        self.validate_prompts('<spell>R 1</spell>, <spell>S 1</spell>, new pallet?',
                              'Abort loading carton <spell>1 1</spell>?')
        self.assertFalse(self._obj._pallet_new)
        self.assertTrue(self._obj._carton_canceled)
        
    def test_xmit_pallet(self):
        self._obj._spur = '99'
        self._obj._carton = '11'
        self._obj._pallet = '22'
        self._obj._pallet_new = False
        
        #test error contacting host
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_PALLET)
        self.assertEqual(self._obj.next_state, XMIT_PALLET)
        self.validate_server_requests()
        self.validate_prompts('Error contacting host,  to try again say ready')

        self.start_server()

        #Test error message from host
        self.set_server_response('1,Error Message\n\n')
        self.post_dialog_responses('ready')
        self._obj.runState(XMIT_PALLET)
        self.assertEqual(self._obj.next_state, ENTER_PALLET)
        self.validate_server_requests(['prTaskLUTLineLoadingPallet', '22', '11', '0'])
        self.validate_prompts('Error Message, To continue say ready')
        
        #Test success
        self._obj._pallet_new = True
        self.set_server_response('0,\n\n')
        self._obj.runState(XMIT_PALLET)
        self.assertEqual(self._obj.next_state, ENTER_CARTON)
        self.validate_server_requests(['prTaskLUTLineLoadingPallet', '22', '11', '1'])
        self.validate_prompts()
        self.assertFalse(self._obj._pallet_new)

    def test_change_region(self):
        #test no region selected
        self._obj._region_selected = False
        
        self._obj.change_region()
        self.validate_prompts()

        #test region selected, no
        self._obj._region_selected = True
        self.post_dialog_responses('no')
        self._obj.change_region()
        self.validate_prompts('Change region, correct?')

        #test region selected, yes
        self._obj._region_selected = True
        self.post_dialog_responses('yes')
        self.assertRaises(Launch, self._obj.change_region)
        self.validate_prompts('Change region, correct?')
        
        
if __name__ == '__main__':
    unittest.main()


