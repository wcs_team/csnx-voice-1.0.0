from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from cyclecounting.CycleCountingDirectToLocation import CycleCountDirectToLocationTask,\
    CC_LOCATION_PREAISLE, CC_LOCATION_AISLE, CC_LOCATION_POSTAISLE,\
    CC_LOCATION_VALIDATE_SLOT
from cyclecounting.CycleCountingLuts import CCLocation
import unittest

class testCycleCountingDirectToLocationTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(CycleCountDirectToLocationTask,
                                      obj_factory.get(CCLocation, 'prTaskLUTCycleCountingLocation'), 
                                      VoiceLink())
        self._obj._location_lut.receive('pre 1,A1,post 1,S1,00,LOCID,11,0,\n\n')
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'cycleCountDirectToLocation')

        #test states
        self.assertEquals(self._obj.states[0], CC_LOCATION_PREAISLE)
        self.assertEquals(self._obj.states[1], CC_LOCATION_AISLE)
        self.assertEquals(self._obj.states[2], CC_LOCATION_POSTAISLE)
        self.assertEquals(self._obj.states[3], CC_LOCATION_VALIDATE_SLOT)
    
    def test_pre_aisle(self):
        #-----------------------------------------------------------
        #Test no pre aisle
        self._obj._location_lut[0]['pre_aisle'] = ''

        self._obj.runState(CC_LOCATION_PREAISLE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------
        #Test pre aisle, different than previous
        self._obj._location_lut[0]['pre_aisle'] = 'pre 1'
        self._obj._location_lut.previous_pre_aisle = ''
        self._obj._location_lut.previous_aisle = '1'
        self._obj._location_lut.previous_post_aisle = '1'
        self.post_dialog_responses('ready')
        
        self._obj.runState(CC_LOCATION_PREAISLE)
        
        self.validate_prompts('pre 1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.previous_pre_aisle, 'pre 1')
        self.assertEquals(self._obj._location_lut.previous_aisle, '')
        self.assertEquals(self._obj._location_lut.previous_post_aisle, '')

        #-----------------------------------------------------------
        #Test pre aisle, same as previous
        self._obj._location_lut[0]['pre_aisle'] = 'pre 1'
        self._obj._location_lut.previous_pre_aisle = 'pre 1'
        self._obj._location_lut.previous_aisle = '1'
        self._obj._location_lut.previous_post_aisle = '1'
        self.post_dialog_responses()
        
        self._obj.runState(CC_LOCATION_PREAISLE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.previous_pre_aisle, 'pre 1')
        self.assertEquals(self._obj._location_lut.previous_aisle, '1')
        self.assertEquals(self._obj._location_lut.previous_post_aisle, '1')
        
    def test_aisle(self):
        #-----------------------------------------------------------
        #Test no aisle
        self._obj._location_lut[0]['aisle'] = ''

        self._obj.runState(CC_LOCATION_AISLE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------
        #Test pre aisle, different than previous
        self._obj._location_lut[0]['aisle'] = '1'
        self._obj._location_lut.previous_pre_aisle = 'pre 1'
        self._obj._location_lut.previous_aisle = '2'
        self._obj._location_lut.previous_post_aisle = 'post 1'
        self.post_dialog_responses('ready')
        
        self._obj.runState(CC_LOCATION_AISLE)
        
        self.validate_prompts('Aisle 1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.previous_pre_aisle, 'pre 1')
        self.assertEquals(self._obj._location_lut.previous_aisle, '1')
        self.assertEquals(self._obj._location_lut.previous_post_aisle, '')

        #-----------------------------------------------------------
        #Test pre aisle, same as previous
        self._obj._location_lut[0]['aisle'] = '1'
        self._obj._location_lut.previous_pre_aisle = 'pre 1'
        self._obj._location_lut.previous_aisle = '1'
        self._obj._location_lut.previous_post_aisle = 'post 1'
        self.post_dialog_responses()
        
        self._obj.runState(CC_LOCATION_AISLE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.previous_pre_aisle, 'pre 1')
        self.assertEquals(self._obj._location_lut.previous_aisle, '1')
        self.assertEquals(self._obj._location_lut.previous_post_aisle, 'post 1')
        
    def test_post_aisle(self):
        #-----------------------------------------------------------
        #Test no pre aisle
        self._obj._location_lut[0]['post_aisle'] = ''

        self._obj.runState(CC_LOCATION_POSTAISLE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------
        #Test pre aisle, different than previous
        self._obj._location_lut[0]['post_aisle'] = 'post 1'
        self._obj._location_lut.previous_pre_aisle = 'pre 1'
        self._obj._location_lut.previous_aisle = '1'
        self._obj._location_lut.previous_post_aisle = 'post 2'
        self.post_dialog_responses('ready')
        
        self._obj.runState(CC_LOCATION_POSTAISLE)
        
        self.validate_prompts('post 1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.previous_pre_aisle, 'pre 1')
        self.assertEquals(self._obj._location_lut.previous_aisle, '1')
        self.assertEquals(self._obj._location_lut.previous_post_aisle, 'post 1')

        #-----------------------------------------------------------
        #Test pre aisle, same as previous
        self._obj._location_lut[0]['post_aisle'] = 'post 1'
        self._obj._location_lut.previous_pre_aisle = 'pre 1'
        self._obj._location_lut.previous_aisle = '1'
        self._obj._location_lut.previous_post_aisle = 'post 1'
        self.post_dialog_responses()
        
        self._obj.runState(CC_LOCATION_POSTAISLE)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.previous_pre_aisle, 'pre 1')
        self.assertEquals(self._obj._location_lut.previous_aisle, '1')
        self.assertEquals(self._obj._location_lut.previous_post_aisle, 'post 1')
    
    def test_validate_slot_no_cd_no_pvid(self):
        #-----------------------------------------------------------
        #Test ready
        self._obj._location_lut[0]['check_digits'] = ''
        self._obj._location_lut[0]['pvid'] = ''
        self.post_dialog_responses('ready')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)

        #-----------------------------------------------------------
        #Test skip slot
        self._run_skip_slot()
        
    def test_validate_slot_with_cd_no_pvid(self):
        #-----------------------------------------------------------
        #Test ready not accepted, wrong check digits
        self._obj._location_lut[0]['check_digits'] = '00'
        self._obj._location_lut[0]['pvid'] = ''
        self.post_dialog_responses('ready', '11!', '00!')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1',
                              'wrong 11, try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)
        
        #-----------------------------------------------------------
        #Test correct check digits
        self._obj.next_state = None
        self._obj._location_lut[0]['check_digits'] = '00'
        self._obj._location_lut[0]['pvid'] = ''
        self.post_dialog_responses('ready', '00!')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)
        
        #-----------------------------------------------------------
        #Test skip slot
        self._run_skip_slot()

    def test_validate_slot_no_cd_with_pvid(self):
        #-----------------------------------------------------------
        #Test ready for check digits, no
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = ''
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('ready', 'no')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1', 
                              'Is location empty?', 
                              'You must speak the product ID. Try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_LOCATION_VALIDATE_SLOT)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, True)
        
        #-----------------------------------------------------------
        #Test ready for check digits, yes, suppressed prompt from previous test
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = ''
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('ready', 'yes')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('Is location empty?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)

        #-----------------------------------------------------------
        #Test wrong PVID
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = ''
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('11!', '22!')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1', 'wrong 11, try again')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)

        #-----------------------------------------------------------
        #Test correct PVID
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = ''
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('22!')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)

        #-----------------------------------------------------------
        #Test skip slot
        self._run_skip_slot()

    def test_validate_slot_with_cd_with_pvid(self):
        #-----------------------------------------------------------
        #Test ready ignored, correct check digits, no
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = '00'
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('ready', '00!', 'no')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1', 
                              'Is location empty?', 
                              'You must speak the product ID. Try again.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_LOCATION_VALIDATE_SLOT)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, True)
        
        #-----------------------------------------------------------
        #Test correct check digits, yes, suppressed prompt from previous test
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = '00'
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('00!', 'yes')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('Is location empty?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)

        #-----------------------------------------------------------
        #Test wrong PVID and Check Digit
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = '00'
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('11!', '00', 'yes')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1', 'wrong 11, try again',
                              'Is location empty?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)

        #-----------------------------------------------------------
        #Test correct PVID
        self._obj.next_state = None 
        self._obj._location_lut[0]['check_digits'] = '00'
        self._obj._location_lut[0]['pvid'] = '22'
        self.post_dialog_responses('22!')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, False)
        self.assertEquals(self._obj._supress_slot_prompt, False)

        #-----------------------------------------------------------
        #Test skip slot
        self._run_skip_slot()


    def _run_skip_slot(self):
        #-----------------------------------------------------------
        #Test skip slot
        self._obj.next_state = None
        self.post_dialog_responses('skip slot', 'no', 'skip slot', 'yes')
        
        self._obj.runState(CC_LOCATION_VALIDATE_SLOT)
        
        self.validate_prompts('S1', 
                              'skip slot, correct?', 
                              'S1', 
                              'skip slot, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location_lut.skipped, True)
        
if __name__ == '__main__':
    unittest.main()


