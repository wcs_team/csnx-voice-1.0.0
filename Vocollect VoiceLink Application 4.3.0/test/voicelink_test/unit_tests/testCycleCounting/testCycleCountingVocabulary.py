from BaseVLTestCase import BaseVLTestCase #Needs to be first import
from vocollect_core import obj_factory

from core.VoiceLink import VoiceLink
from cyclecounting.CycleCountingTask import CycleCountingTask
from cyclecounting.CycleCountingDirectToLocation import CycleCountDirectToLocationTask
from cyclecounting.CycleCountingCountItem import CycleCountCountItemTask

import unittest

class testCycleCountingVocabulary(BaseVLTestCase):

    def setUp(self):
        self.clear()
        self._obj = obj_factory.get(CycleCountingTask, VoiceLink())
        self._obj.taskRunner._append(self._obj)


    def _add_count_task(self):
        temp = obj_factory.get(CycleCountCountItemTask,
                                 '', False, '', False,
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        temp._assignment_lut.receive('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,K,99,Warning Message\n\n')
        self._obj.dynamic_vocab.curr_assignment = temp._assignment_lut[0]
        
    def test_valid(self):

        #running in main task
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), False)

        #running in Direct to location task
        temp = obj_factory.get(CycleCountDirectToLocationTask,
                                 self._obj._location_lut, 
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        self._obj.dynamic_vocab.location_lut = self._obj._location_lut
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), False)

        #running in count item task, no current assignment
        temp = obj_factory.get(CycleCountCountItemTask,
                                 '', False, '', False,
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        temp._assignment_lut.receive('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,K,99,Warning Message\n\n')
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), False)
        
        #running in count item task, with current assignment
        self._obj.dynamic_vocab.curr_assignment = temp._assignment_lut[0]
        self.assertEqual(self._obj.dynamic_vocab._valid('not a word'), False)
        self.assertEqual(self._obj.dynamic_vocab._valid('item number'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('description'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('location'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('quantity'), True)
        self.assertEqual(self._obj.dynamic_vocab._valid('UPC'), True)

    def test_item_number(self):
        self._add_count_task()

        #----------------------------------------------------------------
        #Test item number
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('item number')
        
        self.assertTrue(result);
        self.validate_prompts('item 1')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test no item number
        self.post_dialog_responses()
        self._obj.dynamic_vocab.curr_assignment['item_number'] = ''
        result = self._obj.dynamic_vocab.execute_vocab('item number')
        
        self.assertTrue(result);
        self.validate_prompts('item number not available')
        self.validate_server_requests()

    def test_description(self):
        self._add_count_task()

        #----------------------------------------------------------------
        #Test description
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('description')
        
        self.assertTrue(result);
        self.validate_prompts('descript 1')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test no description
        self.post_dialog_responses()
        self._obj.dynamic_vocab.curr_assignment['item_description'] = ''
        result = self._obj.dynamic_vocab.execute_vocab('description')
        
        self.assertTrue(result);
        self.validate_prompts('description not available')
        self.validate_server_requests()

    def test_quantity(self):
        self._add_count_task()

        #----------------------------------------------------------------
        #Test quantity
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('quantity')
        
        self.assertTrue(result);
        self.validate_prompts('3 packs')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test quantity not available
        self.post_dialog_responses()
        self._obj.dynamic_vocab.curr_assignment['count_mode'] = 'B'
        result = self._obj.dynamic_vocab.execute_vocab('quantity')
        
        self.assertTrue(result);
        self.validate_prompts('quantity not available')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test quantity not available
        self.post_dialog_responses()
        self._obj.dynamic_vocab.curr_assignment['count_mode'] = 'N'
        self._obj.dynamic_vocab.curr_assignment['expected_qty'] = -1
        result = self._obj.dynamic_vocab.execute_vocab('quantity')
        
        self.assertTrue(result);
        self.validate_prompts('quantity not available')
        self.validate_server_requests()

    def test_upc(self):
        self._add_count_task()

        #----------------------------------------------------------------
        #Test UPC
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('UPC')
        
        self.assertTrue(result);
        self.validate_prompts('U P C   1')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test no UPC
        self.post_dialog_responses()
        self._obj.dynamic_vocab.curr_assignment['upc'] = ''
        result = self._obj.dynamic_vocab.execute_vocab('UPC')
        
        self.assertTrue(result);
        self.validate_prompts('UPC not available')
        self.validate_server_requests()

    def test_location(self):
        self._add_count_task()

        #----------------------------------------------------------------
        #Test location from count task
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('location')
        
        self.assertTrue(result);
        self.validate_prompts('pre 1, Aisle 1, post 1, Slot S1')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test location from count task, No aisle
        self.post_dialog_responses()
        self._obj.dynamic_vocab.curr_assignment['aisle'] = ''
        
        result = self._obj.dynamic_vocab.execute_vocab('location')
        
        self.assertTrue(result);
        self.validate_prompts('pre 1, post 1, Slot S1')
        self.validate_server_requests()

        
        #----------------------------------------------------------------
        #Test location from location task
        temp = obj_factory.get(CycleCountDirectToLocationTask,
                                 self._obj._location_lut, 
                                 self._obj.taskRunner, self._obj)
        self._obj.taskRunner._insert(temp)
        self._obj.dynamic_vocab.location_lut = temp._location_lut
        temp._location_lut.receive('pre,aisle,post,slot,,,,0,\n\n')
        
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('location')
        
        self.assertTrue(result);
        self.validate_prompts('pre, Aisle aisle, post, Slot slot')
        self.validate_server_requests()

        #----------------------------------------------------------------
        #Test location from location task, no aisle
        temp._location_lut[0]['aisle'] = ''
        
        self.post_dialog_responses()
        
        result = self._obj.dynamic_vocab.execute_vocab('location')
        
        self.assertTrue(result);
        self.validate_prompts('pre, post, Slot slot')
        self.validate_server_requests()
        
if __name__ == '__main__':
    unittest.main()
