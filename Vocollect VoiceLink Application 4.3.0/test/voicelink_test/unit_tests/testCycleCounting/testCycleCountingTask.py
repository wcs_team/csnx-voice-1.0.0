from BaseVLTestCase import BaseVLTestCase #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from vocollect_core.task.task_runner import Launch, TaskRunnerBase
from cyclecounting.CycleCountingTask import CycleCountingTask, REGIONS,\
    VALIDATE_REGION, CYCLE_COUNT_MODE, CYCLE_COUNT_NEXT_LOC, CYCLE_COUNT_GET_LOC,\
    CYCLE_COUNT_COUNT_ITEM, CYCLE_COUNT_NO_MORE, CycleCountRegionTask,\
    DEFAULT_HTML
from cyclecounting.CycleCountingDirectToLocation import CycleCountDirectToLocationTask
from cyclecounting.CycleCountingCountItem import CycleCountCountItemTask
from core.CoreTask import CoreTask

import unittest

class testCycleCountRegionTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CycleCountingTask, VoiceLink())
        self._obj = obj_factory.get(CycleCountRegionTask, 
                                      temp.taskRunner, 
                                      temp)
        

    
    def test_request_region_configs(self):
        
        self._obj._request_region_configs()
        
        self.assertEquals(len(self._obj._region_config_lut), 1)
        self.assertEquals(self._obj._region_config_lut[0]['number'], 1)
        self.assertEquals(self._obj._region_config_lut[0]['description'], 'region')
        
        
class testCycleCountingTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        temp = obj_factory.get(CoreTask, VoiceLink())
        temp.taskRunner._append(temp)
        temp.function = 1

        self._obj = obj_factory.get(CycleCountingTask, temp.taskRunner)
        TaskRunnerBase._main_runner = self._obj.taskRunner
    
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'cycleCounting')

        #test states
        self.assertEquals(self._obj.states[0], REGIONS)
        self.assertEquals(self._obj.states[1], VALIDATE_REGION)
        self.assertEquals(self._obj.states[2], CYCLE_COUNT_MODE)
        self.assertEquals(self._obj.states[3], CYCLE_COUNT_NEXT_LOC)
        self.assertEquals(self._obj.states[4], CYCLE_COUNT_GET_LOC)
        self.assertEquals(self._obj.states[5], CYCLE_COUNT_COUNT_ITEM)
        self.assertEquals(self._obj.states[6], CYCLE_COUNT_NO_MORE)
        
        #test LUTS defined correctly
        fields = self._obj._valid_regions_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._request_reqion_lut.fields
        self.assertEquals(fields['errorCode'].index, 0)
        self.assertEquals(fields['errorMessage'].index, 1)

        fields = self._obj._region_config_lut.fields
        self.assertEquals(fields['number'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)

        fields = self._obj._cycle_count_mode.fields
        self.assertEquals(fields['mode_indicator'].index, 0)
        self.assertEquals(fields['errorCode'].index, 1)
        self.assertEquals(fields['errorMessage'].index, 2)
        
        fields = self._obj._location_lut.fields
        self.assertEquals(fields['pre_aisle'].index, 0)
        self.assertEquals(fields['aisle'].index, 1)
        self.assertEquals(fields['post_aisle'].index, 2)
        self.assertEquals(fields['slot'].index, 3)
        self.assertEquals(fields['check_digits'].index, 4)
        self.assertEquals(fields['location_id'].index, 5)
        self.assertEquals(fields['pvid'].index, 6)
        self.assertEquals(fields['errorCode'].index, 7)
        self.assertEquals(fields['errorMessage'].index, 8)
        
    def test_regions(self):
        #Check that the correct object gets launched and state is set correctly
        self.assertRaises(Launch, self._obj.runState, REGIONS)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, CycleCountRegionTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'taskMultiRegions')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, VALIDATE_REGION)
    
    def test_validate_regions(self):
        #-----------------------------------------------------------------
        #Test no authorized regions
        self._obj.runState(VALIDATE_REGION)

        self.validate_prompts('not authorized for any regions, see your supervisor')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')
        
        #-----------------------------------------------------------------
        #Test authorized regions
        self._obj.next_state = None
        self._obj._valid_regions_lut.receive('1,region 1,0,\n\n')
        self._obj._region_config_lut.receive('1,cycle count region 1,0,\n\n')
        
        self._obj.runState(VALIDATE_REGION)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_change_region(self):
        #TESTLINK 95630 :: Tests for Change Region
        
        #---------------------------------------------------------------
        #Test no region currently selected
        self._obj._region_selected = False
        
        self._obj.change_region()
        
        self.validate_prompts() 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response no
        self._obj._region_selected = True
        self.post_dialog_responses('no')
        
        self._obj.change_region()
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
        
        #---------------------------------------------------------------
        #Test region selected, response yes
        self._obj._region_selected = True
        self.post_dialog_responses('yes')
        
        self.assertRaises(Launch, self._obj.change_region)
        
        self.validate_prompts('Change region, correct?') 
        self.validate_server_requests()
    
    def test_cycle_counting_mode(self):
        
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(CYCLE_COUNT_MODE)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CYCLE_COUNT_MODE)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response(',1,Error Message\n\n')

        self._obj.runState(CYCLE_COUNT_MODE)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTCycleCountingMode'])
        self.assertEquals(self._obj.next_state, CYCLE_COUNT_MODE)
        
        #-----------------------------------------------------------------
        #test warning condition/no FT location
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response(',99,Warning Message\n\n')

        self._obj.runState(CYCLE_COUNT_MODE)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTCycleCountingMode'])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors with FT location
        self._obj.next_state = None
        self.set_server_response('S,0,\n\n')

        self._obj.runState(CYCLE_COUNT_MODE)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCycleCountingMode'])
        self.assertEquals(self._obj.next_state, None)
        
    def test_next_location(self):
        self._obj._cycle_count_mode.receive('P,0,\n\n')
        
        
        #-----------------------------------------------------------------
        #test operator directed only
        self._obj.next_state = None
        self.post_dialog_responses('ready', #should be ignored
                                   '11111!')

        self._obj.runState(CYCLE_COUNT_NEXT_LOC)
        
        self.validate_prompts('next location?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location, '11111')
        self.assertEquals(self._obj._scanned, False)
    
        #-----------------------------------------------------------------
        #test scanned
        self._obj.next_state = None
        self.post_dialog_responses('ready', #should be ignored
                                   '#11111')

        self._obj.runState(CYCLE_COUNT_NEXT_LOC)
        
        self.validate_prompts('next location?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location, '11111')
        self.assertEquals(self._obj._scanned, True)
        
        #-----------------------------------------------------------------
        #test system directed
        self._obj.next_state = None
        self._obj._cycle_count_mode[0]['mode_indicator'] = 'S'
        self.post_dialog_responses('ready')

        self._obj.runState(CYCLE_COUNT_NEXT_LOC)
        
        self.validate_prompts('next location?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._location, 'ready')
        self.assertEquals(self._obj._scanned, False)
        
        #-----------------------------------------------------------------
        #test change function no
        self._obj.next_state = None
        self.start_server()
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(CYCLE_COUNT_NEXT_LOC)
        
        self.validate_prompts('next location?',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, CYCLE_COUNT_NEXT_LOC)
        
        #-----------------------------------------------------------------
        #test change function yes
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, CYCLE_COUNT_NEXT_LOC)
        
        self.validate_prompts('next location?',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test change region no
        self._obj.next_state = None
        self._obj._region_selected = True
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(CYCLE_COUNT_NEXT_LOC)
        
        self.validate_prompts('next location?',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CYCLE_COUNT_NEXT_LOC)
        
        #-----------------------------------------------------------------
        #test change function yes
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, CYCLE_COUNT_NEXT_LOC)
        
        self.validate_prompts('next location?',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_get_location_operator_directed(self):
        #-----------------------------------------------------------------
        #test operator directed, spoken, no check digits
        self._obj._location = '11111'
        self._obj._scanned = False
        
        self.post_dialog_responses('ready')

        self._obj.runState(CYCLE_COUNT_GET_LOC)
        
        self.validate_prompts('Check digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._check_digit, '')
        
        #-----------------------------------------------------------------
        #test operator directed, spoken, check digits
        self._obj._location = '11111'
        self._obj._scanned = False
        
        self.post_dialog_responses('00!')

        self._obj.runState(CYCLE_COUNT_GET_LOC)
        
        self.validate_prompts('Check digit?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._check_digit, '00')
        
        #-----------------------------------------------------------------
        #test operator directed, scanned
        self._obj._location = '11111'
        self._obj._scanned = True
        self._obj._check_digit = '11'
        
        self.post_dialog_responses()

        self._obj.runState(CYCLE_COUNT_GET_LOC)
        
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._check_digit, '')
        
    def test_get_location_system_directed(self):
        self._obj._location = 'ready'

        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = 'getLocation'
        self.post_dialog_responses('ready')

        self._obj.runState(CYCLE_COUNT_GET_LOC)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CYCLE_COUNT_GET_LOC)
       
       
        
    def test_count_item(self):
        #-----------------------------------------------------------------
        #test tasked is launched 

        self.assertRaises(Launch, self._obj.runState, CYCLE_COUNT_COUNT_ITEM)
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, CycleCountCountItemTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, 'cycleCountCountItem')
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        self.assertEquals(self._obj.current_state, CYCLE_COUNT_NEXT_LOC)
        self.assertEquals(self._obj.next_state, None)
        self.validate_prompts()
        self.validate_server_requests()
        
    def test_no_more(self):
        self.post_dialog_responses('ready')

        self._obj.runState(CYCLE_COUNT_NO_MORE)
        
        self.validate_prompts('No more assignments.')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, REGIONS)
        
        #-----------------------------------------------------------------
        #test change function no
        self._obj.next_state = None
        self.start_server()
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'no')

        self._obj.runState(CYCLE_COUNT_NO_MORE)
        
        self.validate_prompts('No more assignments.',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, CYCLE_COUNT_NO_MORE)
        
        #-----------------------------------------------------------------
        #test change function yes
        self._obj.next_state = None
        self.set_server_response('1,Function 1,0,\n2,Function 2,0,\n\n', 'prTaskLUTCoreValidFunctions')
        self.post_dialog_responses('change function', 'yes')

        self.assertRaises(Launch, self._obj.runState, CYCLE_COUNT_NO_MORE)
        
        self.validate_prompts('No more assignments.',
                              'change function, correct?')
        self.validate_server_requests(['prTaskLUTCoreValidFunctions', '0'])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test change region no
        self._obj.next_state = None
        self._obj._region_selected = True
        self.post_dialog_responses('change region', 'no')

        self._obj.runState(CYCLE_COUNT_NO_MORE)
        
        self.validate_prompts('No more assignments.',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CYCLE_COUNT_NO_MORE)
        
        #-----------------------------------------------------------------
        #test change function yes
        self._obj.next_state = None
        self.post_dialog_responses('change region', 'yes')

        self.assertRaises(Launch, self._obj.runState, CYCLE_COUNT_NO_MORE)
        
        self.validate_prompts('No more assignments.',
                              'Change region, correct?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
    def test_get_data_map(self):
        map = self._obj.get_data_map(DEFAULT_HTML)
        
        self.assertEquals(map['title'], self._obj.title)
        self.assertEquals(map['operator'], self._obj.operator_name)

        
        
if __name__ == '__main__':
    unittest.main()


