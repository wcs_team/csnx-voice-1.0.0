from BaseVLTestCase import BaseVLTestCase, BOTH_SERVERS #Needs to be first import

from vocollect_core import obj_factory
from core.VoiceLink import VoiceLink
from cyclecounting.CycleCountingTask import CycleCountingTask
from cyclecounting.CycleCountingCountItem import CycleCountCountItemTask,\
    CC_COUNT_GET_ASSIGNMENT, CC_COUNT_NEXT_ASSIGNMENT, CC_COUNT_PROMPT_KNOWN,\
    CC_COUNT_PROMPT_BLIND, CC_COUNT_SEND_ITEM_COUNT, CC_COUNT_REASON_CODE,\
    SHOW_TIE_BY_HIGH_HTML, SHOW_REASON_CODES_HTML
from cyclecounting.SharedConstants import CC_TASK_NAME
from vocollect_core.task.task_runner import TaskRunnerBase, Launch

import unittest
import time

class testCycleCountCountItemTask(BaseVLTestCase):
    
    def setUp(self):
        self.clear()
        # put CycleCountingTask on the stack first.
        vl = VoiceLink()
        temp = obj_factory.get(CycleCountingTask, vl)
        temp.function = 1
        vl._append(temp)
        
        self._obj = obj_factory.get(CycleCountCountItemTask, 
                                      '', False, '', False, 
                                      vl,
                                      temp)
        vl._append(self._obj)
        TaskRunnerBase._main_runner = vl
   
    def test_initializeStates(self):
        #test name
        self.assertEquals(self._obj.name, 'cycleCountCountItem')

        #test states
        self.assertEquals(self._obj.states[0], CC_COUNT_GET_ASSIGNMENT)
        self.assertEquals(self._obj.states[1], CC_COUNT_NEXT_ASSIGNMENT)
        self.assertEquals(self._obj.states[2], CC_COUNT_PROMPT_KNOWN)
        self.assertEquals(self._obj.states[3], CC_COUNT_PROMPT_BLIND)
        self.assertEquals(self._obj.states[4], CC_COUNT_REASON_CODE)
        self.assertEquals(self._obj.states[5], CC_COUNT_SEND_ITEM_COUNT)
        
        #test LUTS defined correctly
        fields = self._obj._assignment_lut.fields
        self.assertEquals(fields['location_id'].index, 0)
        self.assertEquals(fields['pre_aisle'].index, 1)
        self.assertEquals(fields['aisle'].index, 2)
        self.assertEquals(fields['post_aisle'].index, 3)
        self.assertEquals(fields['slot'].index, 4)
        self.assertEquals(fields['check_digits'].index, 5)
        self.assertEquals(fields['pvid'].index, 6)
        self.assertEquals(fields['item_number'].index, 7)
        self.assertEquals(fields['item_description'].index, 8)
        self.assertEquals(fields['upc'].index, 9)
        self.assertEquals(fields['expected_qty'].index, 10)
        self.assertEquals(fields['uom'].index, 11)
        self.assertEquals(fields['count_mode'].index, 12)
        self.assertEquals(fields['tie'].index, 13)
        self.assertEquals(fields['high'].index, 14)
        self.assertEquals(fields['image'].index, 15)        
        self.assertEquals(fields['errorCode'].index, 16)
        self.assertEquals(fields['errorMessage'].index, 17)
        
        fields = self._obj._reason_code_lut.fields
        self.assertEquals(fields['code'].index, 0)
        self.assertEquals(fields['description'].index, 1)
        self.assertEquals(fields['errorCode'].index, 2)
        self.assertEquals(fields['errorMessage'].index, 3)
        
        fields = self._obj._reserve_locations.fields
        self.assertEquals(fields['location_id'].index, 0)
        self.assertEquals(fields['palletID'].index, 1)
        self.assertEquals(fields['item'].index, 2)
        self.assertEquals(fields['quantity'].index, 3)
        self.assertEquals(fields['errorCode'].index, 4)
        self.assertEquals(fields['errorMessage'].index, 5)
    
    def test_get_assignment(self):
        
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(CC_COUNT_GET_ASSIGNMENT)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_COUNT_GET_ASSIGNMENT)
        
        #-----------------------------------------------------------------
        #test error condition
        self._obj.next_state = None
        self.start_server()
        self.post_dialog_responses('ready')
        self.set_server_response(',,,,,,,,,,,,,,,,1,Error Message\n\n')

        self.assertRaises(Launch, self._obj.runState, CC_COUNT_GET_ASSIGNMENT)
        
        self.validate_prompts('Error Message, To continue say ready')
        self.validate_server_requests(['prTaskLUTCycleCountingAssignment', '', '', ''])
        self.assertTrue(isinstance(self._obj.taskRunner.task_stack[0].obj, CycleCountingTask))
        self.assertEquals(self._obj.taskRunner.task_stack[0].name, CC_TASK_NAME)
        self.assertEquals(self._obj.taskRunner.task_stack[0].id, None)
        
        #-----------------------------------------------------------------
        #test warning condition
        self._obj.next_state = None
        self.post_dialog_responses('ready')
        self.set_server_response('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,K,,,,99,Warning Message\n\n')

        self._obj.runState(CC_COUNT_GET_ASSIGNMENT)
        
        self.validate_prompts('Warning Message, say ready')
        self.validate_server_requests(['prTaskLUTCycleCountingAssignment', '', '', ''])
        self.assertEquals(self._obj.next_state, None)

        #-----------------------------------------------------------------
        #test no errors, no location 
        self._obj.next_state = None
        self.set_server_response('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,K,,,,0,\n\n')

        self._obj.runState(CC_COUNT_GET_ASSIGNMENT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCycleCountingAssignment', '', '', ''])
        self.assertEquals(self._obj.next_state, None)
        
        #-----------------------------------------------------------------
        #test no errors, location, not scanned
        self._obj.next_state = None
        self._obj._location = '1234'
        self._obj._check_digits = '00'
        self._obj._scanned = False
        self._obj._multi_items = False
        self._obj._assignment_iterator = None
        self.set_server_response('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,K,,,,0,\n\n')

        self._obj.runState(CC_COUNT_GET_ASSIGNMENT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCycleCountingAssignment', '0', '1234', '00'])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._multi_items, False)
        self.assertTrue(self._obj._assignment_iterator != None)
        
        #-----------------------------------------------------------------
        #test no errors, location, not scanned
        self._obj.next_state = None
        self._obj._location = '1234'
        self._obj._check_digits = ''
        self._obj._scanned = True
        self._obj._assignment_iterator = None
        self.set_server_response('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,K,,,,0,\n'
                                 'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,,,,0,\n'
                                 '\n')

        self._obj.runState(CC_COUNT_GET_ASSIGNMENT)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskLUTCycleCountingAssignment', '1', '1234', ''])
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._multi_items, True)
        self.assertTrue(self._obj._assignment_iterator != None)
        
        #-----------------------------------------------------------------
        #test invalid slot
        self._obj.next_state = None
        self._obj._location = '1234'
        self._obj._check_digits = ''
        self._obj._scanned = True
        self._obj._multi_items = False
        self._obj._assignment_iterator = None
        self.set_server_response(',,,,,,,,,,,,,0,\n'
                                 '\n')

        self._obj.runState(CC_COUNT_GET_ASSIGNMENT)
        
        self.validate_prompts('Incorrect location.')
        self.validate_server_requests(['prTaskLUTCycleCountingAssignment', '1', '1234', ''])
        self.assertEquals(self._obj.next_state, '')
        self.assertEquals(self._obj._multi_items, False)
        self.assertTrue(self._obj._assignment_iterator == None)
    
    def test_next_assignment(self):
        self._obj._assignment_lut.receive('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,K,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          '\n')
        self._obj._assignment_iterator = iter(self._obj._assignment_lut)
        
        #-----------------------------------------------------------------
        #test error code not 0
        self._obj.next_state = None

        self._obj._assignment_lut[0]['errorCode'] = 1
        
        self._obj.runState(CC_COUNT_NEXT_ASSIGNMENT)
        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_COUNT_NEXT_ASSIGNMENT)
        self.assertEquals(self._obj._curr_assignment['errorCode'], 1)
        
        #-----------------------------------------------------------------
        #test error code 0, not skipped
        self._obj.next_state = None

        self._obj.runState(CC_COUNT_NEXT_ASSIGNMENT)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_TIE_BY_HIGH_HTML)
        self.assertEquals(self._obj._curr_assignment['errorCode'], 0)
        self.assertEquals(self._obj._curr_assignment, self._obj.dynamic_vocab.curr_assignment)
        self.assertEquals(self._obj._actual_quantity, -1)

        #-----------------------------------------------------------------
        #test error code 0, not skipped
        self._obj.next_state = None
        self._obj._skipped = True
        
        self._obj.runState(CC_COUNT_NEXT_ASSIGNMENT)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_COUNT_SEND_ITEM_COUNT)
        self.assertEquals(self._obj._curr_assignment['errorCode'], 0)
        self.assertEquals(self._obj._curr_assignment, self._obj.dynamic_vocab.curr_assignment)
        self.assertEquals(self._obj._actual_quantity, 3)
        
        #-----------------------------------------------------------------
        #test No more assignments
        self._obj.next_state = None
        self._obj._skipped = True
        
        self._obj.runState(CC_COUNT_NEXT_ASSIGNMENT)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, '')

    def test_prompt_known(self):
        self._obj._assignment_lut.receive('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,B,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          '\n')

        #----------------------------------------------------------
        #Test prompt mode is Blind
        self._obj.next_state = None
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        
        self._obj.runState(CC_COUNT_PROMPT_KNOWN)

        self.validate_prompts()
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        
        #----------------------------------------------------------
        #Test prompt mode is Known, yes
        self._obj.next_state = None
        self._obj._curr_assignment = self._obj._assignment_lut[1]
        self._obj._actual_quantity = -1
        self.post_dialog_responses('yes')
        
        self._obj.runState(CC_COUNT_PROMPT_KNOWN)

        self.validate_prompts('Are there 3 packs present?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_COUNT_SEND_ITEM_COUNT)
        self.assertEquals(self._obj._actual_quantity, 3)
        
        
        #----------------------------------------------------------
        #Test prompt mode is Known, no
        self._obj.next_state = None
        self._obj._multi_items = True
        self._obj._actual_quantity = -1
        self._obj._curr_assignment = self._obj._assignment_lut[1]
        self.post_dialog_responses('no')
        
        self._obj.runState(CC_COUNT_PROMPT_KNOWN)

        self.validate_prompts('Are there 3 packs of item 2 present?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._actual_quantity, -1)
        
    def test_prompt_blind(self):
        self._obj._assignment_lut.receive('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,B,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          '\n')

        #----------------------------------------------------------
        #Test prompt mode is Blind, correct quantity
        self._obj.next_state = None
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._actual_quantity = -1
        self.post_dialog_responses('3!')

        
        self._obj.runState(CC_COUNT_PROMPT_BLIND)

        self.validate_prompts('How many packs are present?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._actual_quantity, 3)
        
        #----------------------------------------------------------
        #Test prompt mode is Blind, incorrect quantity
        self.start_server()
        self._obj.next_state = None
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._actual_quantity = -1
        self.post_dialog_responses('1!')

        
        self._obj.runState(CC_COUNT_PROMPT_BLIND)

        self.validate_prompts('How many packs are present?')
        self.validate_server_requests(['prTaskLUTCycleCountingReserveChain', 'LocID', 'item 1'])
        self.assertEquals(self._obj.next_state, CC_COUNT_PROMPT_BLIND)
        self.assertEquals(self._obj._actual_quantity, 1)
        self.stop_server()
        #----------------------------------------------------------
        #Test prompt mode is Blind, incorrect quantity and different than previous
        self._obj.next_state = None
        self._obj._multi_items = True
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self.post_dialog_responses('2!')

        
        self._obj.runState(CC_COUNT_PROMPT_BLIND)

        self.validate_prompts('recount pre 1,, aisle 1,, post 1,, Slot S1', 
                              'How many packs of item 1 are present?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_COUNT_PROMPT_BLIND)
        self.assertEquals(self._obj._actual_quantity, 2)
        
        #----------------------------------------------------------
        #Test prompt mode is Blind, incorrect quantity and same as previous
        self._obj.next_state = None
        self._obj._multi_items = True
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self.post_dialog_responses('2!')

        
        self._obj.runState(CC_COUNT_PROMPT_BLIND)

        self.validate_prompts('recount pre 1,, aisle 1,, post 1,, Slot S1', 
                              'How many packs of item 1 are present?')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, None)
        self.assertEquals(self._obj._actual_quantity, 2)
        
        #----------------------------------------------------------
        #Test receiving a reserve chain

        
        #----------------------------------------------------------
        #Test not receiving a reserve chain
        self.stop_server()
        
    def test_send_item_count(self):
        self._obj._assignment_lut.receive('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,B,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          '\n')
        self.start_server(BOTH_SERVERS)
        self.set_server_response('Y', 'prTaskODRItemCountUpdate')

        #----------------------------------------------------------
        #Test not skipped
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj.next_state = None
        self._obj._skipped = False
        self._obj._actual_quantity = 3
        self._obj._reason_code = 2

        self._obj.runState(CC_COUNT_SEND_ITEM_COUNT)
        time.sleep(1)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskODRItemCountUpdate', 'LocID', 'item 1', '3', 'packs', 'N', '2'])
        self.assertEquals(self._obj.next_state, CC_COUNT_NEXT_ASSIGNMENT)
        
        #----------------------------------------------------------
        #Test skipped
        self._obj._curr_assignment = self._obj._assignment_lut[1]
        self._obj.next_state = None
        self._obj._skipped = True
        self._obj._actual_quantity = 0
        self._obj._reason_code = 2

        self._obj.runState(CC_COUNT_SEND_ITEM_COUNT)
        time.sleep(1)
        
        self.validate_prompts()
        self.validate_server_requests(['prTaskODRItemCountUpdate', 'LocID', 'item 2', '0', 'packs', 'Y', '2'])
        self.assertEquals(self._obj.next_state, CC_COUNT_NEXT_ASSIGNMENT)

    def test_get_reason_code(self):
        self._obj._assignment_lut.receive('LocID,pre 1,1,post 1,S1,00,11,item 1,descript 1,UPC 1,3,packs,B,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          'LocID,pre 1,1,post 1,S1,00,11,item 2,descript 2,UPC 1,3,packs,K,2,4,,0,\n'
                                          '\n')
        self._obj._curr_assignment = self._obj._assignment_lut[0]
        self._obj._actual_quantity = 20
        
        #-----------------------------------------------------------------
        #test server not started
        self._obj.next_state = None
        self.post_dialog_responses('ready')

        self._obj.runState(CC_COUNT_REASON_CODE)
        
        self.validate_prompts('Error contacting host,  to try again say ready')
        self.validate_server_requests()
        self.assertEquals(self._obj.next_state, CC_COUNT_REASON_CODE)        
        
        #-----------------------------------------------------------------
        #test with server now
        self.start_server()
        self.set_server_response('1,Short,0,\n'
                                           '2,Overage,0,\n'
                                           '3,Damage,0,\n'
                                           '\n')
        
        #Trigger the need to get the reason codes and show the screen
        self.post_dialog_responses('2', 'yes')
        
        self._obj.runState(CC_COUNT_REASON_CODE)
        
        self.validate_prompts('Expected 3 you said 20, reason code?', 'Overage, correct?')
        self.validate_server_requests(['prTaskLUTCoreGetReasonCodes', '9', '1'])
        self.assertEquals(self._obj.page, self._obj.name + "/" + SHOW_REASON_CODES_HTML)
        self.assertEquals(self._obj.next_state, CC_COUNT_REASON_CODE)

if __name__ == '__main__':
    unittest.main()


