#Region 110 - Verify shorting, new container and serial number capture.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion110_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion110_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '10,selection region 110,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('10,Region 110,1,0,1,1,1,0,0,0,2,1,1,0,0,2,1,4,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,3,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,2,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,,O,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,,C,0,0,\n'
                                       '\n')
        self.set_server_response('2,0000000004,04,12345,Store 123,,O,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   '10!',            # Region?
                                   'yes',            # selection region 110, correct?
                                   '1234',           # Work ID?
                                   'yes',            # 1234, correct?
                                   'ready',          # ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',          # Open 03
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 1
                                   '4',              # Pick 4    pick message
                                   'new container',  # S 1
                                   'yes',            # new container, correct?
                                   'ready',          # Open 04
                                   '00',             # S 1
                                   '0!',             # Pick 10    pick message
                                   'no',             # You said 0 asked for 10. Is this a short product?
                                   '10',             # Pick 10    pick message
                                   '00',             # S 1
                                   '15',             # Pick 15    pick message
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 1
                                   '4',              # Pick 5    pick message
                                   'yes',            # You said 4 asked for 5. Is this a short product?
                                   '11111ready',     # serial number 1 of 4
                                   '222ready',       # serial number 2 of 4
                                   '3333ready',   # serial number 3 of 4
                                   '44444ready',    # serial number 4 of 4
                                   'ready',          # Entries complete
                                   '-')              # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 110, correct?',
                              'Work ID?',
                              '1234, correct?',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Open 03',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 4 ,   pick message',
                              'S 1',
                              'new container, correct?',
                              'Open 04',
                              'S 1',
                              'Pick 10 ,   pick message',
                              'You said 0 asked for 10. Is this a short product?',
                              'Pick 10 ,   pick message',
                              'S 1',
                              'Pick 15 ,   pick message',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1',
                              'Pick 5 ,   pick message',
                              'You said 4 asked for 5. Is this a short product?',
                              'serial number 1 of 4',
                              'serial number 2 of 4',
                              'serial number 3 of 4',
                              'serial number 4 of 4',
                              'Entries complete',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '10', '3'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '4', '1', '1', '3', '', '', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '1', '', '1', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '9', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '0', '2', '5', '', '', '11111'],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '0', '2', '5', '', '', '222'],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '2', '5', '', '', '3333'],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '2', '8', '', '', '44444'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
