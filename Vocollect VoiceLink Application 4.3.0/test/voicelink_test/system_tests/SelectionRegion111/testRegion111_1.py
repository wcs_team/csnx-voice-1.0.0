# Verify Pass assignment works correctly in Region 111, manual issuance with no containers.
# Rename the file testGenerator_111.txt to testGenerator_111.py and copy the file to the
# root level of the VoiceLink project and run to regenerate the python unit test if necessary.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testRegion111_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()
        
    def testRegion111_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '5,selection region 105,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '108,Region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '10,selection region 110,0,\n'
                                       '111,Region 111,0,\n'
                                       '12,selection region 112,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('111,Region 111,1,0,1,1,1,1,0,0,2,1,0,0,1,2,1,4,1,1,0,X                                           ,X                                  ,X                         ,X                ,0,0,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('1111001,0,\n'
                                       '\n')
        self.set_server_response('203,0,783,1111001,1,18.00,180,       00,0,0,,0,\n'
                                       '\n')
        self.set_server_response('B,1,8113,1302,111,Building 3 Site 1        ,4 E                                               ,Bay 5                                             ,5 0                                         ,4,By Weight,1349,1,52.3418,54.478199999999994,        0,50,,,HEN TURKEY,53.41,7262306325,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'N,0,8112,1284,111,Building 2 Site 1                ,3       ,,3 2                                               ,1,,1330,0,0.0,0.0,        0,32,,,KOOL-AID BLUE MOON BERRY,5.4,4300095389,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8111,1282,111,Building 2 Site 1                           ,2                  ,,3 0          ,3,,1304,0,0.0,0.0,        0,30,,,PENCIL MECHANICAL,0.1,3977918025,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8110,1266,111,Building 1 Site 1         ,3,,1 4                                               ,2,,1334,0,0.0,0.0,        0,14,,,PF VANILLA 3 LAYER CAKE,8.9,9999953903,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8109,1264,111,Building 1 Site 1                   ,3          ,,1 2  ,2,Packs,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'B,1,8108,1264,111,Building 1 Site 1                       ,3              ,,1 2      ,6,Boxes,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8107,1259,111,Building 1 Site 1                           ,2                  ,,0 7          ,3,,1350,1,58.8,61.2,        0,07,,,C & H CANE SUGAR,60,1580003062,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'B,1,8106,1257,111,Building 1 Site 1        ,1                                                 ,,0 5                                               ,1,,1305,1,0.098,0.10200000000000001,        0,05,,,TUMS PEPPERMINT,0.1,7385408100,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'N,0,8105,1255,111,Building 1 Site 1                   ,1          ,,0 3  ,13,,1303,0,0.0,0.0,        0,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'B,1,8104,1253,111,Building 1 Site 1                  ,1         ,,0 1 ,26,Eaches,1302,0,0.0,0.0,        0,01,,,TUMS 4 FLAVOR JAR,0.1,7161030002,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('G,1,8113,1302,111,Building 3 Site 1        ,4 E                                               ,Bay 5                                             ,5 0                                         ,4,By Weight,1349,1,52.3418,54.478199999999994,        2,50,,,HEN TURKEY,53.41,7262306325,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'G,0,8107,1259,111,Building 1 Site 1                ,2       ,,0 7                                               ,3,,1350,1,58.8,61.2,        0,07,,,C & H CANE SUGAR,60,1580003062,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1111001,0,\n'
                                       '\n')
        self.set_server_response('210,0,783,1111001,1,18.00,180,       00,0,0,,0,\n'
                                       '\n')
        self.set_server_response('N,0,8105,1255,111,Building 1 Site 1        ,1                                                 ,,0 3                                               ,13,,1303,0,0.0,0.0,        0,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('Y')
        self.set_server_response('G,0,8105,1255,111,Building 1 Site 1        ,1                                                 ,,0 3                                               ,13,,1303,0,0.0,0.0,       12,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '111',              # Region?
                                   'yes',              # Region 111, correct?
                                   '1001',             # Work ID?
                                   'yes',              # 1001, correct?
                                   'yes',              # Pick in reverse order?
                                   'ready',            # ID 1111001, has a goal time of 18.0 minutes, say ready
                                   'no',               # Base summary?
                                   'yes',              # Pick base items?
                                   'ready',            # Building 3 Site 1
                                   'ready',            # Aisle 4 E
                                   'ready',            # Bay 5
                                   '50',               # 5 0
                                   '2',                # Pick 4 By Weight,   Item has PVID
                                   'yes',              # You said 2 asked for 4. Is this a short product?
                                   '53.30',            # weight 1 of 2
                                   '53.31',            # weight 2 of 2
                                   'ready',            # Entries complete
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 3
                                   '12',               # 1 2
                                   '6',                # Pick 6 Boxes,   Location has CD Site 1
                                   'ready',            # Aisle 1
                                   'skip slot',        # 0 5
                                   'yes',              # Skip slot, correct?
                                   '01',               # 0 1
                                   '26',               # Pick 26 Eaches,   Item has PVID
                                   'ready',            # Building 2 Site 1
                                   'ready',            # Aisle 3
                                   'skip slot',        # 3 2
                                   'yes',              # Skip slot, correct?
                                   'ready',            # Aisle 2
                                   '30',               # 3 0
                                   '3',                # Pick 3 ,   Location has CD Site 1
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 3
                                   '14',               # 1 4
                                   'repick skips',     # Pick 2 ,   Location has CD Site 1
                                   'yes',              # repick skips, correct?
                                   'ready',            # Building 2 Site 1
                                   'ready',            # Aisle 3
                                   '32',               # 3 2
                                   '1',                # Pick 1 ,   Location has CD Site 1
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 3
                                   '14',               # 1 4
                                   '2',                # Pick 2 ,   Location has CD Site 1
                                   '12',               # 1 2
                                   '2',                # Pick 2 Packs,   Location has CD Site 1
                                   'ready',            # Aisle 2
                                   '07',               # 0 7
                                   '0',                # Pick 3 ,   Location has CD Site 1
                                   'yes',              # You said 0 asked for 3. Is this a short product?
                                   'ready',            # Aisle 1
                                   'skip slot',        # 0 5
                                   'yes',              # Skip slot, correct?
                                   '03',               # 0 3
                                   '0!',               # Pick 13 ,   Item has PVID
                                   'no',               # You said 0 asked for 13. Is this a short product?
                                   'skip slot',        # Pick 13 ,   Item has PVID
                                   'yes',              # Skip slot, correct?
                                   'repick skips',     # 0 5
                                   'yes',              # repick skips, correct?
                                   'skip slot',        # 0 5
                                   'yes',              # Skip slot, correct?
                                   'pass assignment',  # 0 3
                                   'yes',              # Pass assignment, correct?
                                   '05',               # 0 5
                                   '1',                # Pick 1 ,   Item has PVID
                                   '0.10',             # weight 1 of 1
                                   'review last',      # Entries complete
                                   'ready',            # Unit 1,weight was 0.1,say ready
                                   'undo entry',       # Entries complete
                                   'all entries',      # last entry, all entries, or continue?
                                   'yes',              # re-enter all entries, correct?
                                   '0.10',             # weight 1 of 1
                                   'ready',            # Entries complete
                                   'ready',            # Building 3 Site 1
                                   'ready',            # Aisle 4 E
                                   'ready',            # Bay 5
                                   '50',               # 5 0
                                   '2',                # Pick 2 By Weight,   Item has PVID
                                   '54.01',            # weight 1 of 2
                                   '54.02',            # weight 2 of 2
                                   'ready',            # Entries complete
                                   'pass assignment',  # Building 1 Site 1
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 2
                                   '07',               # 0 7
                                   '0',                # Pick 3 ,   Location has CD Site 1
                                   'yes',              # You said 0 asked for 3. Is this a short product?
                                   'ready',            # Assignment passed.  For next assignment, say ready
                                   '1001',             # Work ID?
                                   'yes',              # 1001, correct?
                                   'no',               # Pick in reverse order?
                                   'ready',            # ID 1111001, has a goal time of 18.0 minutes, say ready
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 1
                                   '03',               # 0 3
                                   '12',               # Pick 13 ,   Item has PVID
                                   'yes',              # You said 12 asked for 13. Is this a short product?
                                   'ready',            # Building 1 Site 1
                                   'skip aisle',       # Aisle 1
                                   'ready',            # Aisle 1
                                   '03',               # 0 3
                                   '0',                # Pick 1 ,   Item has PVID
                                   'yes',              # You said 0 asked for 1. Is this a short product?
                                   '-')                # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'Region 111, correct?',
                              'Work ID?',
                              '1001, correct?',
                              'Pick in reverse order?',
                              'receiving picks in reverse order, please wait',
                              'ID 1111001, has a goal time of 18.0 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'Building 3 Site 1',
                              'Aisle 4 E',
                              'Bay 5',
                              '5 0',
                              'Pick 4 By Weight,   Item has PVID',
                              'You said 2 asked for 4. Is this a short product?',
                              'weight 1 of 2',
                              'weight 2 of 2',
                              'Entries complete',
                              'Building 1 Site 1',
                              'Aisle 3',
                              '1 2',
                              'Pick 6 Boxes,   Location has CD Site 1',
                              'Aisle 1',
                              '0 5',
                              'Skip slot, correct?',
                              '0 1',
                              'Pick 26 Eaches,   Item has PVID',
                              'Building 2 Site 1',
                              'Aisle 3',
                              '3 2',
                              'Skip slot, correct?',
                              'Aisle 2',
                              '3 0',
                              'Pick 3 ,   Location has CD Site 1',
                              'Building 1 Site 1',
                              'Aisle 3',
                              '1 4',
                              'Pick 2 ,   Location has CD Site 1',
                              'repick skips, correct?',
                              'Building 2 Site 1',
                              'Aisle 3',
                              '3 2',
                              'Pick 1 ,   Location has CD Site 1',
                              'Building 1 Site 1',
                              'Aisle 3',
                              '1 4',
                              'Pick 2 ,   Location has CD Site 1',
                              '1 2',
                              'Pick 2 Packs,   Location has CD Site 1',
                              'Aisle 2',
                              '0 7',
                              'Pick 3 ,   Location has CD Site 1',
                              'You said 0 asked for 3. Is this a short product?',
                              'Aisle 1',
                              '0 5',
                              'Skip slot, correct?',
                              '0 3',
                              'Pick 13 ,   Item has PVID',
                              'You said 0 asked for 13. Is this a short product?',
                              'Pick 13 ,   Item has PVID',
                              'Skip slot, correct?',
                              '0 5',
                              'repick skips, correct?',
                              'no skips to repick, returning to picking',
                              '0 5',
                              'Skip slot, correct?',
                              '0 3',
                              'Pass assignment, correct?',
                              '0 5',
                              'Pick 1 ,   Item has PVID',
                              'weight 1 of 1',
                              'Entries complete',
                              'Unit 1, weight was 0.1, say ready',
                              'Entries complete',
                              'last entry, all entries, or continue?',
                              're enter all items, correct?',
                              'weight 1 of 1',
                              'Entries complete',
                              'Picking complete',
                              'shorts are reported',
                              'Going back for shorts',
                              'Picking shorts',
                              'Building 3 Site 1',
                              'Aisle 4 E',
                              'Bay 5',
                              '5 0',
                              'Pick 2 By Weight,   Item has PVID',
                              'weight 1 of 2',
                              'weight 2 of 2',
                              'Entries complete',
                              'Building 1 Site 1',
                              'Pass assignment not allowed.',
                              'Building 1 Site 1',
                              'Aisle 2',
                              '0 7',
                              'Pick 3 ,   Location has CD Site 1',
                              'You said 0 asked for 3. Is this a short product?',
                              'Picking complete',
                              'Assignment passed.  For next assignment, say ready',
                              'Work ID?',
                              '1001, correct?',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'ID 1111001, has a goal time of 18.0 minutes, say ready',
                              'Building 1 Site 1',
                              'Aisle 1',
                              '0 3',
                              'Pick 13 ,   Item has PVID',
                              'You said 12 asked for 13. Is this a short product?',
                              'Picking complete',
                              'shorts are reported',
                              'Going back for shorts',
                              'Picking shorts',
                              'Building 1 Site 1',
                              'Aisle 1',
                              'Last aisle to be picked, you must pick it now',
                              'Aisle 1',
                              '0 3',
                              'Pick 1 ,   Item has PVID',
                              'You said 0 asked for 1. Is this a short product?',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '111', '3'],
                                      ['prTaskLUTRequestWork', '1001', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '203', '0', '1', '1'],
                                      ['prTaskODRPicked', '203', '783', '1302', '1', '0', '', '8113', '', '53.30', ''],
                                      ['prTaskODRPicked', '203', '783', '1302', '1', '1', '', '8113', '', '53.31', ''],
                                      ['prTaskODRPicked', '203', '783', '1264', '6', '1', '', '8108', '', '', ''],
                                      ['prTaskODRUpdateStatus', '203', '1257', '0', 'N'],
                                      ['prTaskODRPicked', '203', '783', '1253', '26', '1', '', '8104', '', '', ''],
                                      ['prTaskODRUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskODRUpdateStatus', '203', '1284', '0', 'S'],
                                      ['prTaskODRPicked', '203', '783', '1282', '3', '1', '', '8111', '', '', ''],
                                      ['prTaskODRUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskODRPicked', '203', '783', '1284', '1', '1', '', '8112', '', '', ''],
                                      ['prTaskODRPicked', '203', '783', '1266', '2', '1', '', '8110', '', '', ''],
                                      ['prTaskODRPicked', '203', '783', '1264', '2', '1', '', '8109', '', '', ''],
                                      ['prTaskODRPicked', '203', '783', '1259', '0', '1', '', '8107', '', '', ''],
                                      ['prTaskODRUpdateStatus', '203', '1257', '0', 'S'],
                                      ['prTaskODRUpdateStatus', '203', '1255', '0', 'S'],
                                      ['prTaskODRUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskODRUpdateStatus', '203', '1257', '0', 'S'],
                                      ['prTaskODRUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskODRPicked', '203', '783', '1257', '1', '1', '', '8106', '', '0.10', ''],
                                      ['prTaskLUTGetPicks', '203', '1', '1', '1'],
                                      ['prTaskODRPicked', '203', '783', '1302', '1', '0', '', '8113', '', '54.01', ''],
                                      ['prTaskODRPicked', '203', '783', '1302', '1', '1', '', '8113', '', '54.02', ''],
                                      ['prTaskODRPicked', '203', '783', '1259', '0', '1', '', '8107', '', '', ''],
                                      ['prTaskLUTPassAssignment', '203'],
                                      ['prTaskLUTRequestWork', '1001', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '210', '0', '1', '0'],
                                      ['prTaskODRPicked', '210', '783', '1255', '12', '1', '', '8105', '', '', ''],
                                      ['prTaskLUTGetPicks', '210', '0', '1', '0'],
                                      ['prTaskODRPicked', '210', '783', '1255', '0', '1', '', '8105', '', '', ''],
                                      ['prTaskLUTStopAssignment', '210'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
