# Pick 3 batched assignments in Region 115.
# Verify that the 'short product', 'new container' and 'partial' commands function correctly.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion115_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion115_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                       '2,selection region 2,0,\n'
                                       '3,selection region 3,0,\n'
                                       '4,selection region 4,0,\n'
                                       '8,selection region 8,0,\n'
                                       '9,selection region 9,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('15,Region 115,1,1,4,1,1,0,0,0,0,1,1,0,0,2,0,-1,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,1,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 345,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 567,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,3,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12347,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12347,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 123,,C,0,0,\n'
                                       '5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('2,0000000002,02,12345,Store 123,,O,0,0,\n'
                                       '5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,C,0,0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12346,Store 123,,C,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,C,0,0,\n'
                                       '5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('4,0000000004,04,12346,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,O,0,0,\n'
                                       '5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,C,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,C,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   '15!',            # Region?
                                   'yes',            # selection region 115, correct?
                                   '3',              # How many assignments?
                                   'ready',          # To receive work, say ready                               
                                   'ready',          # position 1, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',          # position 2, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',          # position 3, ID Store 567, has a goal time of 15 minutes, say ready
                                   '01!',            # New Container ID? Position 1
                                   'yes',            # 01, correct?
                                   '03!',            # New Container ID? Position 2
                                   'yes',            # 03, correct?
                                   '05!',            # New Container ID? Position 3
                                   'yes',            # 05, correct?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   'short product',  # S 1 Pick 4    pick message
                                   '00',             # check digit?
                                   '3',              # how many did you pick?
                                   'yes',            # 3, correct?
                                   '1234ready',      # lot text
                                   'yes',            # 1234, correct?
                                   '2!',             # Quantity for this lot text
                                   'yes',            # 1234A
                                   '01!',            # Put 2 in 1
                                   '00',             # S 1 Pick 1    pick message
                                   '1234ready',      # lot text
                                   '1!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   '01!',            # Put 1 in 1
                                   '00',             # S 1 Pick 8    pick message
                                   '1234ready',      # lot text
                                   '3!',             # Quantity for this lot text
                                   'yes',            # 1234A
                                   '01!',            # Put 3 in 1
                                   'short product',  # S 1 Pick 5    pick message
                                   '00',             # check digit?
                                   '3',              # how many did you pick?
                                   'yes',            # 3, correct?
                                   '1234ready',      # lot text
                                   '1!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   '01!',            # Put 1 in 1
                                   'short product',  # S 1 Pick 2    pick message
                                   '00',             # check digit?
                                   '1',              # how many did you pick?
                                   'yes',            # 1, correct?
                                   '1234ready',      # lot text
                                   '1!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   '01!',            # Put 1 in 1
                                   'short product',  # S 1 Pick 12    pick message
                                   '00',             # check digit?
                                   '10',             # how many did you pick?
                                   'yes',            # 10, correct?
                                   '1234ready',      # lot text
                                   '6!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   'partial',        # Put 5 in 1
                                   'yes',            # partial, correct?
                                   '2',              # Quantity?
                                   '01!',            # Container?
                                   'yes',            # new container, correct?
                                   '02!',            # New Container ID? Position 1
                                   'yes',            # 02, correct?
                                   '00',             # S 1 Pick 8    pick message
                                   '1234ready',      # lot text
                                   '4!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   '02!',            # Put 3 in 1
                                   'new container',  # Put 1 in 2
                                   'yes',            # new container, correct?
                                   '04!',            # New Container ID? Position 2
                                   'yes',            # 04, correct?
                                   '04!',            # Put 1 in 2
                                   'short product',  # S 1 Pick 4    pick message
                                   '00',             # check digit?
                                   '3',              # how many did you pick?
                                   'yes',            # 3, correct?
                                   '1234ready',      # lot text
                                   '1!',             # Quantity for this lot text
                                   'yes',            # 1234A
                                   '04!',            # Put 1 in 2
                                   '00',             # S 1 Pick 2    pick message
                                   '1234ready',      # lot text
                                   '2!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   '04!',            # Put 1 in 2
                                   '05!',            # Put 1 in 3
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 1 Pick 8    pick message
                                   '1234ready',      # lot text
                                   '4!',             # Quantity for this lot text
                                   'yes',            # 1234A
                                   '04!',            # Put 3 in 2
                                   '05!',            # Put 1 in 3
                                   '00',             # S 1 Pick 4    pick message
                                   '1234ready',      # lot text
                                   '4!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   '05!',            # Put 4 in 3
                                   '-')              # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 115, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'position 1, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 567, has a goal time of 15 minutes, say ready',
                              'New Container ID? Position 1',
                              '01, correct?',
                              'New Container ID? Position 2',
                              '03, correct?',
                              'New Container ID? Position 3',
                              '05, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 4 ,   pick message',
                              'check digit?',
                              'how many did you pick?',
                              '3, correct?',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 2 in 1',
                              'S 1 Pick 1 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 1 in 1',
                              'S 1 Pick 8 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 3 in 1',
                              'S 1 Pick 5 ,   pick message',
                              'check digit?',
                              'how many did you pick?',
                              '3, correct?',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 1 in 1',
                              'S 1 Pick 2 ,   pick message',
                              'check digit?',
                              'how many did you pick?',
                              '1, correct?',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 1 in 1',
                              'S 1 Pick 12 ,   pick message',
                              'check digit?',
                              'how many did you pick?',
                              '10, correct?',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 5 in 1',
                              'partial, correct?',
                              'Quantity?',
                              'Container?',
                              'new container, correct?',
                              'New Container ID? Position 1',
                              '02, correct?',
                              'S 1 Pick 8 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 3 in 1',
                              'Put 1 in 2',
                              'new container, correct?',
                              'New Container ID? Position 2',
                              '04, correct?',
                              'Put 1 in 2',
                              'S 1 Pick 4 ,   pick message',
                              'check digit?',
                              'how many did you pick?',
                              '3, correct?',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 1 in 2',
                              'S 1 Pick 2 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 1 in 2',
                              'Put 1 in 3',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1 Pick 8 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 3 in 2',
                              'Put 1 in 3',
                              'S 1 Pick 4 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 4 in 3',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '15', '3'],
                                      ['prTaskLUTGetAssignment', '3', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '01', '2', ''],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '03', '2', ''],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '05', '2', ''],
                                      ['prTaskLUTSendLot', '1234', '2', '12345', '3'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '0', '1', '3', '1234A', '', ''],
                                      ['prTaskLUTSendLot', '1234', '1', '12345', '3'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '3', '1234B', '', ''],
                                      ['prTaskLUTSendLot', '1234', '3', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '0', '1', '4', '1234A', '', ''],
                                      ['prTaskLUTSendLot', '1234', '1', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234B', '', ''],
                                      ['prTaskLUTSendLot', '1234', '1', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '4', '1234B', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '0', '1', '', '9', '', '', ''],
                                      ['prTaskLUTSendLot', '1234', '6', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '0', '1', '4', '1234B', '', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '1', '', '1', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '02', '2', ''],
                                      ['prTaskLUTSendLot', '1234', '4', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '2', '4', '1234B', '', ''],
                                      ['prTaskLUTContainer', '1', '12346', '', '3', '', '1', ''],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '04', '2', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '1', '0', '4', '6', '1234B', '', ''],
                                      ['prTaskLUTSendLot', '1234', '1', '12346', '6'],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '1', '0', '4', '6', '1234A', '', ''],
                                      ['prTaskLUTSendLot', '1234', '2', '12346', '6'],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '1', '1', '4', '6', '1234B', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '1', '1', '5', '7', '1234B', '', ''],
                                      ['prTaskLUTSendLot', '1234', '4', '12346', '5'],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '3', '1', '4', '5', '1234A', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L2', '1', '0', '5', '8', '1234A', '', ''],
                                      ['prTaskLUTSendLot', '1234', '4', '12347', '8'],
                                      ['prTaskLUTPicked', '1', '12347', 'L2', '4', '1', '5', '8', '1234B', '', ''],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
