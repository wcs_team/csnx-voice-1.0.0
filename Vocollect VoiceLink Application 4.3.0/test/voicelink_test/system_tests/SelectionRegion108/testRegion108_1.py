# Verify Pass assignment works correctly in Region 108, manual issuance with containers.
# Rename the file testGenerator_108.txt to testGenerator_108.py and copy the file to the
# root level of the VoiceLink project and run to regenerate the python unit test if necessary.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testRegion108_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion108_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '5,selection region 105,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '108,Region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '10,selection region 110,0,\n'
                                       '11,selection region 111,0,\n'
                                       '12,selection region 112,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('108,Region 108,1,0,1,1,1,1,1,0,0,1,1,0,1,2,0,4,1,1,2,X                                           ,X                                  ,X                         ,X                ,1,0,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('1081000,0,\n'
                                       '\n')
        self.set_server_response('239,0,784,1081000,1,18.00,180,       00,0,0,,0,\n'
                                       '\n')
        self.set_server_response('N,0,8114,1253,108,Building 1 Site 1        ,1                                                 ,,0 1                                               ,26,Eaches,1302,0,0.0,0.0,        0,01,,,TUMS 4 FLAVOR JAR,0.1,7161030002,784,1081000,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'N,0,8115,1255,108,Building 1 Site 1        ,1                                                 ,,0 3                                               ,13,,1303,0,0.0,0.0,        0,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,784,1081000,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'N,0,8116,1257,108,Building 1 Site 1       ,1                                                 ,,0 5                                               ,1,,1305,1,0.098,0.10200000000000001,        0,05,,,TUMS PEPPERMINT,0.1,7385408100,784,1081000,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'N,0,8117,1259,108,Building 1 Site 1                  ,2         ,,0 7                                         ,3,,1350,1,58.8,61.2,        0,07,,,C & H CANE SUGAR,60,1580003062,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8118,1264,108,Building 1 Site 1  ,3                                                 ,,1 2                                             ,6,Boxes,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8119,1264,108,Building 1 Site 1      ,3                                                 ,,1 2                                               ,2,Packs,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8120,1266,108,Building 1 Site 1        ,3                                                 ,,1 4                                               ,2,,1334,0,0.0,0.0,        0,14,,,PF VANILLA 3 LAYER CAKE,8.9,9999953903,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8121,1282,108,Building 2 Site 1                                 ,2                                                 ,,3 0                                         ,3,,1304,0,0.0,0.0,        0,30,,,PENCIL MECHANICAL,0.1,3977918025,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8122,1284,108,Building 2 Site 1  ,3                                                 ,,3 2                                             ,1,,1330,0,0.0,0.0,        0,32,,,KOOL-AID BLUE MOON BERRY,5.4,4300095389,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8123,1302,108,Building 3 Site 1            ,4 E   ,Bay 5                                             ,5 0                                             ,4,By Weight,1349,1,52.3418,54.478199999999994,        0,50,,,HEN TURKEY,53.41,7262306325,784,1081000,18,0,18,,,0,,Item hasPVID,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('296,0000000296,96,784,1081000,,A,0,0,\n'
                                       '295,0000000295,95,784,1081000,,A,0,0,\n'
                                       '294,0000000294,94,784,1081000,,A,0,0,\n'
                                       '293,0000000293,93,784,1081000,,A,0,0,\n'
                                       '292,0000000292,92,784,1081000,,A,0,0,\n'
                                       '291,0000000291,91,784,1081000,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('291,0000000291,91,784,1081000,,O,1,0,\n'
                                       '296,0000000296,96,784,1081000,,A,1,0,\n'
                                       '295,0000000295,95,784,1081000,,A,1,0,\n'
                                       '294,0000000294,94,784,1081000,,A,1,0,\n'
                                       '293,0000000293,93,784,1081000,,A,1,0,\n'
                                       '292,0000000292,92,784,1081000,,A,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('291,0000000291,91,784,1081000,,C,0,0,\n'
                                       '296,0000000296,96,784,1081000,,A,0,0,\n'
                                       '295,0000000295,95,784,1081000,,A,0,0,\n'
                                       '294,0000000294,94,784,1081000,,A,0,0,\n'
                                       '293,0000000293,93,784,1081000,,A,0,0,\n'
                                       '292,0000000292,92,784,1081000,,A,0,0,\n'
                                       '\n')
        self.set_server_response('292,0000000292,92,784,1081000,,O,1,0,\n'
                                       '291,0000000291,91,784,1081000,,C,1,0,\n'
                                       '296,0000000296,96,784,1081000,,A,1,0,\n'
                                       '295,0000000295,95,784,1081000,,A,1,0,\n'
                                       '294,0000000294,94,784,1081000,,A,1,0,\n'
                                       '293,0000000293,93,784,1081000,,A,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('292,0000000292,92,784,1081000,,C,0,0,\n'
                                       '291,0000000291,91,784,1081000,,C,0,0,\n'
                                       '296,0000000296,96,784,1081000,,A,0,0,\n'
                                       '295,0000000295,95,784,1081000,,A,0,0,\n'
                                       '294,0000000294,94,784,1081000,,A,0,0,\n'
                                       '293,0000000293,93,784,1081000,,A,0,0,\n'
                                       '\n')
        self.set_server_response('293,0000000293,93,784,1081000,,O,1,0,\n'
                                       '292,0000000292,92,784,1081000,,C,1,0,\n'
                                       '291,0000000291,91,784,1081000,,C,1,0,\n'
                                       '296,0000000296,96,784,1081000,,A,1,0,\n'
                                       '295,0000000295,95,784,1081000,,A,1,0,\n'
                                       '294,0000000294,94,784,1081000,,A,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('293,0000000293,93,784,1081000,,C,0,0,\n'
                                       '292,0000000292,92,784,1081000,,C,0,0,\n'
                                       '291,0000000291,91,784,1081000,,C,0,0,\n'
                                       '296,0000000296,96,784,1081000,,A,0,0,\n'
                                       '295,0000000295,95,784,1081000,,A,0,0,\n'
                                       '294,0000000294,94,784,1081000,,A,0,0,\n'
                                       '\n')
        self.set_server_response('294,0000000294,94,784,1081000,,O,1,0,\n'
                                       '293,0000000293,93,784,1081000,,C,1,0,\n'
                                       '292,0000000292,92,784,1081000,,C,1,0,\n'
                                       '291,0000000291,91,784,1081000,,C,1,0,\n'
                                       '296,0000000296,96,784,1081000,,A,1,0,\n'
                                       '295,0000000295,95,784,1081000,,A,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('G,0,8117,1259,108,Building 1 Site 1        ,2                                                 ,,0 7                                               ,3,,1350,1,58.8,61.2,        1,07,,,C & H CANE SUGAR,60,1580003062,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'G,0,8119,1264,108,Building 1 Site 1            ,3   ,,1 2                                               ,2,Packs,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,784,1081000,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'G,0,8123,1302,108,Building 3 Site 1                ,4 E       ,Bay 5                                             ,5 0                                               ,4,By Weight,1349,1,52.3418,54.478199999999994,        2,50,,,HEN TURKEY,53.41,7262306325,784,1081000,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('294,0000000294,94,784,1081000,,C,0,0,\n'
                                       '293,0000000293,93,784,1081000,,C,0,0,\n'
                                       '292,0000000292,92,784,1081000,,C,0,0,\n'
                                       '291,0000000291,91,784,1081000,,C,0,0,\n'
                                       '296,0000000296,96,784,1081000,,A,0,0,\n'
                                       '295,0000000295,95,784,1081000,,A,0,0,\n'
                                       '\n')
        self.set_server_response('295,0000000295,95,784,1081000,,O,1,0,\n'
                                       '294,0000000294,94,784,1081000,,C,1,0,\n'
                                       '293,0000000293,93,784,1081000,,C,1,0,\n'
                                       '292,0000000292,92,784,1081000,,C,1,0,\n'
                                       '291,0000000291,91,784,1081000,,C,1,0,\n'
                                       '296,0000000296,96,784,1081000,,A,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1081000,0,\n'
                                       '\n')
        self.set_server_response('240,0,784,1081000,1,18.00,180,       00,0,0,,0,\n'
                                       '\n')
        self.set_server_response('N,0,8115,1255,108,Building 1 Site 1        ,1                                                 ,,0 3                                               ,13,,1303,0,0.0,0.0,        0,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,784,1081000,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('295,0000000295,95,784,1081000,,O,0,0,\n'
                                       '294,0000000294,94,784,1081000,,C,0,0,\n'
                                       '293,0000000293,93,784,1081000,,C,0,0,\n'
                                       '292,0000000292,92,784,1081000,,C,0,0,\n'
                                       '291,0000000291,91,784,1081000,,C,0,0,\n'
                                       '296,0000000296,96,784,1081000,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '108',              # Region?
                                   'yes',              # Region 108, correct?
                                   '1000',             # Work ID?
                                   'yes',              # 1000, correct?
                                   'no',               # Pick in reverse order?
                                   'ready',            # ID 1081000, has a goal time of 18.0 minutes, say ready
                                   '6!',               # Number of Labels for ID 1081000
                                   'yes',              # 6, correct?
                                   '1!',               # Printer?
                                   'yes',              # printer 1, correct?
                                   'ready',            # Open 91
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 1
                                   '01',               # 0 1 Pick 26 Eaches,   Item has PVID
                                   'new container',    # 0 3 Pick 13 ,   Item has PVID
                                   'yes',              # new container, correct?
                                   'ready',            # Open 92
                                   'skip slot',        # 0 3 Pick 13 ,   Item has PVID
                                   'yes',              # Skip slot, correct?
                                   '05',               # 0 5 Pick 1 ,   Item has PVID
                                   '0.10',             # weight 1 of 1
                                   'ready',            # Entries complete
                                   'ready',            # Aisle 2
                                   'short product',    # 0 7 Pick 3 ,   Location has CD Site 1
                                   '07',               # check digit?
                                   '1',                # how many did you pick?
                                   'yes',              # 1, correct?
                                   '60.00',            # weight 1 of 1
                                   'ready',            # Entries complete
                                   'ready',            # Aisle 3
                                   'new container',    # 1 2 Pick 6 Boxes,   Location has CD Site 1
                                   'yes',              # new container, correct?
                                   'ready',            # Open 93
                                   '12',               # 1 2 Pick 6 Boxes,   Location has CD Site 1
                                   'short product',    # 1 2 Pick 2 Packs,   Location has CD Site 1
                                   '12',               # check digit?
                                   '0',                # how many did you pick?
                                   'yes',              # 0, correct?
                                   '14',               # 1 4 Pick 2 ,   Location has CD Site 1
                                   'ready',            # Building 2 Site 1
                                   'ready',            # Aisle 2
                                   'partial',          # 3 0 Pick 3 ,   Location has CD Site 1
                                   'yes',              # partial, correct?
                                   '30',               # check digit?
                                   'pass assignment',  # Quantity?
                                   '2',                # Quantity?
                                   'yes',              # new container, correct?
                                   'ready',            # Open 94
                                   '30',               # 3 0 Pick 1 ,   Location has CD Site 1
                                   'ready',            # Aisle 3
                                   '32',               # 3 2 Pick 1 ,   Location has CD Site 1
                                   'ready',            # Building 3 Site 1
                                   'ready',            # Aisle 4 E
                                   'ready',            # Bay 5
                                   'short product',    # 5 0 Pick 4 By Weight,   Item hasPVID
                                   '50',               # check digit?
                                   '2',                # how many did you pick?
                                   'yes',              # 2, correct?
                                   '53.30',            # weight 1 of 2
                                   '53.31',            # weight 2 of 2
                                   'undo entry',       # Entries complete
                                   'last entry',       # last entry, all entries, or continue?
                                   'yes',              # re-enter item 2, correct?
                                   '53.32',            # weight 2 of 2
                                   'ready',            # Entries complete
                                   'ready',            # Building 1 Site 1
                                   'how much more',    # Aisle 1
                                   'ready',            # Aisle 1
                                   'pass assignment',  # 0 3 Pick 13 ,   Item has PVID
                                   'yes',              # Pass assignment, correct?
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 2
                                   'short product',    # 0 7 Pick 2 ,   Location has CD Site 1
                                   '07',               # check digit?
                                   '1',                # how many did you pick?
                                   'yes',              # 1, correct?
                                   '60.00',            # weight 1 of 1
                                   'ready',            # Entries complete
                                   'ready',            # Aisle 3
                                   'short product',    # 1 2 Pick 2 Packs,   Location has CD Site 1
                                   '12',               # check digit?
                                   '0',                # how many did you pick?
                                   'yes',              # 0, correct?
                                   'ready',            # Building 3 Site 1
                                   'ready',            # Aisle 4 E
                                   'ready',            # Bay 5
                                   'new container',    # 5 0 Pick 2 By Weight,   Item has PVID
                                   'yes',              # new container, correct?
                                   'ready',            # Open 95
                                   '50',               # 5 0 Pick 2 By Weight,   Item has PVID
                                   '53.32',            # weight 1 of 2
                                   '53.33',            # weight 2 of 2
                                   'ready',            # Entries complete
                                   'ready',            # Assignment passed.  For next assignment, say ready
                                   '1000',             # Work ID?
                                   'yes',              # 1000, correct?
                                   'yes',              # Pick in reverse order?
                                   'ready',            # ID 1081000, has a goal time of 18.0 minutes, say ready
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 1
                                   'how much more',    # 0 3 Pick 13 ,   Item has PVID
                                   '03',               # 0 3 Pick 13 ,   Item has PVID
                                   '-')                # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'Region 108, correct?',
                              'Work ID?',
                              '1000, correct?',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'ID 1081000, has a goal time of 18.0 minutes, say ready',
                              'Number of Labels for ID 1081000',
                              '6, correct?',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Open 91',
                              'Building 1 Site 1',
                              'Aisle 1',
                              '0 1 Pick 26 Eaches,   Item has PVID',
                              '0 3 Pick 13 ,   Item has PVID',
                              'new container, correct?',
                              'Open 92',
                              '0 3 Pick 13 ,   Item has PVID',
                              'Skip slot, correct?',
                              '0 5 Pick 1 ,   Item has PVID',
                              'weight 1 of 1',
                              'Entries complete',
                              'Aisle 2',
                              '0 7 Pick 3 ,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '1, correct?',
                              'weight 1 of 1',
                              'Entries complete',
                              'Aisle 3',
                              '1 2 Pick 6 Boxes,   Location has CD Site 1',
                              'new container, correct?',
                              'Open 93',
                              '1 2 Pick 6 Boxes,   Location has CD Site 1',
                              '1 2 Pick 2 Packs,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '0, correct?',
                              '1 4 Pick 2 ,   Location has CD Site 1',
                              'Building 2 Site 1',
                              'Aisle 2',
                              '3 0 Pick 3 ,   Location has CD Site 1',
                              'partial, correct?',
                              'check digit?',
                              'Quantity?',
                              'Pass assignment not allowed.',
                              'Quantity?',
                              'new container, correct?',
                              'Open 94',
                              '3 0 Pick 1 ,   Location has CD Site 1',
                              'Aisle 3',
                              '3 2 Pick 1 ,   Location has CD Site 1',
                              'Building 3 Site 1',
                              'Aisle 4 E',
                              'Bay 5',
                              '5 0 Pick 4 By Weight,   Item hasPVID',
                              'check digit?',
                              'how many did you pick?',
                              '2, correct?',
                              'weight 1 of 2',
                              'weight 2 of 2',
                              'Entries complete',
                              'last entry, all entries, or continue?',
                              're enter item 2, correct?',
                              'weight 2 of 2',
                              'Entries complete',
                              'Building 1 Site 1',
                              'Aisle 1',
                              '13 remaining at 1 line item',
                              'Aisle 1',
                              '0 3 Pick 13 ,   Item has PVID',
                              'Pass assignment, correct?',
                              'Picking complete',
                              'shorts are reported',
                              'Going back for shorts',
                              'Picking shorts',
                              'Building 1 Site 1',
                              'Aisle 2',
                              '0 7 Pick 2 ,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '1, correct?',
                              'weight 1 of 1',
                              'Entries complete',
                              'Aisle 3',
                              '1 2 Pick 2 Packs,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '0, correct?',
                              'Building 3 Site 1',
                              'Aisle 4 E',
                              'Bay 5',
                              '5 0 Pick 2 By Weight,   Item has PVID',
                              'new container, correct?',
                              'Open 95',
                              '5 0 Pick 2 By Weight,   Item has PVID',
                              'weight 1 of 2',
                              'weight 2 of 2',
                              'Entries complete',
                              'Picking complete',
                              'Assignment passed.  For next assignment, say ready',
                              'Work ID?',
                              '1000, correct?',
                              'Pick in reverse order?',
                              'receiving picks in reverse order, please wait',
                              'ID 1081000, has a goal time of 18.0 minutes, say ready',
                              'Building 1 Site 1',
                              'Aisle 1',
                              '0 3 Pick 13 ,   Item has PVID',
                              '13 remaining at 1 line item',
                              '0 3 Pick 13 ,   Item has PVID',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '108', '3'],
                                      ['prTaskLUTRequestWork', '1000', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '239', '0', '1', '0'],
                                      ['prTaskLUTContainer', '239', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '', '', '3', '6'],
                                      ['prTaskLUTPrint', '239', '784', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '239', '784', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '239', '784', '1253', '26', '1', '291', '8114', '', '', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '291', '', '1', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '', '', '2', ''],
                                      ['prTaskLUTUpdateStatus', '239', '1255', '0', 'S'],
                                      ['prTaskLUTPicked', '239', '784', '1257', '1', '1', '292', '8116', '', '0.10', ''],
                                      ['prTaskLUTPicked', '239', '784', '1259', '1', '1', '292', '8117', '', '60.00', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '292', '', '1', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '239', '784', '1264', '6', '1', '293', '8118', '', '', ''],
                                      ['prTaskLUTPicked', '239', '784', '1264', '0', '1', '', '8119', '', '', ''],
                                      ['prTaskLUTPicked', '239', '784', '1266', '2', '1', '293', '8120', '', '', ''],
                                      ['prTaskLUTPicked', '239', '784', '1282', '2', '0', '293', '8121', '', '', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '293', '', '1', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '239', '784', '1282', '1', '1', '294', '8121', '', '', ''],
                                      ['prTaskLUTPicked', '239', '784', '1284', '1', '1', '294', '8122', '', '', ''],
                                      ['prTaskLUTPicked', '239', '784', '1302', '1', '0', '294', '8123', '', '53.30', ''],
                                      ['prTaskLUTPicked', '239', '784', '1302', '1', '1', '294', '8123', '', '53.32', ''],
                                      ['prTaskLUTUpdateStatus', '239', '', '2', 'N'],
                                      ['prTaskLUTGetPicks', '239', '1', '1', '0'],
                                      ['prTaskLUTPicked', '239', '784', '1259', '1', '1', '294', '8117', '', '60.00', ''],
                                      ['prTaskLUTPicked', '239', '784', '1264', '0', '1', '', '8119', '', '', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '294', '', '1', ''],
                                      ['prTaskLUTContainer', '239', '784', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '239', '784', '1302', '1', '0', '295', '8123', '', '53.32', ''],
                                      ['prTaskLUTPicked', '239', '784', '1302', '1', '1', '295', '8123', '', '53.33', ''],
                                      ['prTaskLUTPassAssignment', '239'],
                                      ['prTaskLUTRequestWork', '1000', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '240', '0', '1', '1'],
                                      ['prTaskLUTContainer', '240', '', '', '', '', '0', ''],
                                      ['prTaskLUTPicked', '240', '784', '1255', '13', '1', '295', '8115', '', '', ''],
                                      ['prTaskLUTStopAssignment', '240'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
