# Selection Region 113 - Verify multiple picks for the same item are summed correctly when not picking to containers.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion113_2(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion113_2(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('13,Region 113,1,0,4,1,1,0,0,0,0,1,0,0,0,2,0,4,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 346,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 347,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,3,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '3!',       # Function?
                                   'yes',      # normal assignments, correct?
                                   '13!',      # Region?
                                   'yes',      # selection region 113, correct?
                                   '2345',     # Work ID?
                                   'yes',      # 2345, correct?
                                   '2346',     # Work ID?
                                   'yes',      # 2346, correct?
                                   '2347',     # Work ID?
                                   'yes',      # 2347, correct?
                                   'no more',  # Work ID?
                                   'ready',    # position 1, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',    # position 2, ID Store 346, has a goal time of 15 minutes, say ready
                                   'ready',    # position 3, ID Store 347, has a goal time of 15 minutes, say ready
                                   'ready',    # pre 1
                                   'ready',    # Aisle A 1
                                   'ready',    # post 1
                                   '00',       # S 1 Pick 11  ID Store 345  pick message
                                   '00',       # S 1 Pick 5  ID Store 345  pick message
                                   'ready',    # pre 2
                                   'ready',    # Aisle A 1
                                   'ready',    # post 2
                                   '00',       # S 1 Pick 3  ID Store 346  pick message
                                   'ready',    # pre 1
                                   'ready',    # Aisle A 1
                                   'ready',    # post 1
                                   '00',       # S 1 Pick 3  ID Store 346  pick message
                                   '00',       # S 1 Pick 4  ID Store 347  pick message
                                   'ready',    # pre 2
                                   'ready',    # Aisle A 1
                                   'ready',    # post 2
                                   '00',       # S 1 Pick 5  ID Store 347  pick message
                                   'ready',    # pre 1
                                   'ready',    # Aisle A 1
                                   'ready',    # post 1
                                   '00',       # S 1 Pick 3  ID Store 347  pick message
                                   '-')        # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 113, correct?',
                              'Work ID?',
                              '2345, correct?',
                              'Work ID?',
                              '2346, correct?',
                              'Work ID?',
                              '2347, correct?',
                              'Work ID?',
                              'position 1, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 346, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 347, has a goal time of 15 minutes, say ready',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 11 ,  ID Store 345, pick message',
                              'S 1 Pick 5 ,  ID Store 345, pick message',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1 Pick 3 ,  ID Store 346, pick message',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 3 ,  ID Store 346, pick message',
                              'S 1 Pick 4 ,  ID Store 347, pick message',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1 Pick 5 ,  ID Store 347, pick message',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 3 ,  ID Store 347, pick message',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '13', '3'],
                                      ['prTaskLUTRequestWork', '2345', '1', '1'],
                                      ['prTaskLUTRequestWork', '2346', '1', '1'],
                                      ['prTaskLUTRequestWork', '2347', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '4', '1', '', '3', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '', '9', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '3', '1', '', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '3', '1', '', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '4', '1', '', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L2', '5', '1', '', '8', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '3', '1', '', '10', '', '', ''],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
