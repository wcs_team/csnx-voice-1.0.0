# Core - Test vehicle safety check list.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testCore_1(BaseVLTestCase):

    def setUp(self):
        self.clear()
        self.start_server(BOTH_SERVERS)

    def testCore_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                       '2,pallet jack,1,0,\n'
                                       '\n')
        self.set_server_response('brakes,B,0,\n'
                                       'tires,B,0,\n'
                                       'mirrors,B,0,\n'
                                       'lights,B,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                       '2,pallet jack,1,0,\n'
                                       '\n')
        self.set_server_response('brakes,B,0,\n'
                                       'tires,B,0,\n'
                                       'mirrors,B,0,\n'
                                       'lights,B,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',    # Password?
                                   '2!',      # Vehicle Type?
                                   'yes',     # pallet jack, correct?
                                   '425!',    # Vehicle ID?
                                   'yes',    # 425, correct?
                                   'yes',    # brakes
                                   'no',     # tires
                                   'no',     # tires, failed, correct?
                                   'yes',    # tires
                                   'no',     # mirrors
                                   'yes',    # mirrors, failed, correct?
                                   'no',     # Your vehicle has failed, can you do a quick repair?
                                   '1!',      # Vehicle Type?
                                   'yes',    # fork lift, correct?
                                   'yes',    # brakes
                                   'yes',    # tires
                                   'no',     # mirrors
                                   'no',     # mirrors, failed, correct?
                                   'yes',    # mirrors
                                   'no',     # lights
                                   'yes',    # lights, failed, correct?
                                   'yes',    # Your vehicle has failed, can you do a quick repair?
                                   'ready',  # Perform quick repair and then say ready
                                   'yes',    # lights
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '-')      # Region?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'pallet jack, correct?', 
                              'Vehicle ID?',
                              '425, correct?',
                              'Safety checklist. say yes, no or numeric value to verify each check',
                              'brakes',
                              'tires',
                              'tires, failed, correct?',
                              'tires',
                              'mirrors',
                              'mirrors, failed, correct?',
                              'Your vehicle has failed, can you do a quick repair?',
                              'Vehicle failed, get new vehicle',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Safety checklist. say yes, no or numeric value to verify each check',
                              'brakes',
                              'tires',
                              'mirrors',
                              'mirrors, failed, correct?',
                              'mirrors',
                              'lights',
                              'lights, failed, correct?',
                              'Your vehicle has failed, can you do a quick repair?',
                              'Perform quick repair, then say ready',
                              'lights',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '2', '425'],
                                      ['prTaskLUTCoreSafetyCheck', '-2', 'mirrors', '1'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskLUTCoreSafetyCheck', '-2', 'lights', '2'],
                                      ['prTaskLUTCoreSafetyCheck', '-1', '', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
