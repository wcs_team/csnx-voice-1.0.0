#Selection region 103.  Test shorting and 'partial command with single assignment and multiple open containers.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion103_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion103_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 4,0,\n'
                                       '8,selection region 8,0,\n'
                                       '9,selection region 9,0,\n'
                                       '15,selection region 15,0,\n'
                                       '\n')
        self.set_server_response('3,Region 103,1,1,1,1,1,0,1,0,2,1,1,1,0,0,1,-1,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,1,2,0,0\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,1,O,0,0,\n'
                                       '2,0000000004,04,12345,Store 123,2,O,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
#        self.set_server_response('0,\n'
#                                       '\n')
#        self.set_server_response('0,\n'
#                                       '\n')
        self.set_server_response('Location 1,00,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '3!',       # Function?
                                   'yes',      # normal assignments, correct   
                                   '3!',       # Region?
                                   'yes',      # selection region 3, correct?
                                   'ready',    # To receive work, say ready
                                   'ready',    # ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',    # Open 03
                                   '1!',       # Printer?
                                   'yes',      # printer 1, correct?
                                   'ready',    # pre 1
                                   'ready',    # Aisle A 1
                                   'ready',    # post 1
                                   '00',       # S 1
                                   '3',        # Pick 4    pick message
                                   'yes',      # You said 3 asked for 4. Is this a short product?
#                                   'partial',  # Container?
#                                   'yes',      # partial, correct?
#                                   '2',        # Quantity?
#                                   '04!',      # Container?
                                   '03!',      # Container?
                                   'ready',    # S 1
                                   '00',       # S 1
                                   '10',       # Pick 10    pick message
                                   '03!',      # Container?
                                   '00',       # S 1
                                   '00',       # Pick 15    pick message
                                   'no',       # You said 0 asked for 15. Is this a short product?
                                   '00',       # Pick 15    pick message
                                   'no',       # You said 0 asked for 15. Is this a short product?
                                   '15',       # Pick 15    pick message
                                   '04!',      # Container?
                                   'ready',    # pre 2
                                   'ready',    # Aisle A 1
                                   'ready',    # post 2
                                   '00',       # S 1
                                   '9!',        # Pick 10    pick message
                                   'yes',      # You said 9 asked for 10. Is this a short product?
#                                   'partial',  # Container?
#                                   'yes',      # partial, correct?
#                                   '8',        # Quantity?
#                                   '03!',      # Container?
                                   '04!',      # Container?
                                   'ready',    # Deliver to Location 1
                                   '-')        # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 103, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Open 03',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 4 ,   pick message',
                              'You said 3 asked for 4. Is this a short product?',
                              'Container?',
#                              'partial, correct?',
#                              'Quantity?',
#                              'Container?',
#                              'Container?',
                              'S 1',
                              'You must speak the check digit.',
                              'S 1',
                              'Pick 10 ,   pick message',
                              'Container?',
                              'S 1',
                              'Pick 15 ,   pick message',
                              'You said 0 asked for 15. Is this a short product?',
                              'Pick 15 ,   pick message',
                              'You said 0 asked for 15. Is this a short product?',
                              'Pick 15 ,   pick message',
                              'Container?',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1',
                              'Pick 10 ,   pick message',
                              'You said 9 asked for 10. Is this a short product?',
                              'Container?',
#                              'partial, correct?',
#                              'Quantity?',
#                              'Container?',
#                              'Container?',
                              'Picking complete',
                              'Deliver to Location 1',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '1', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '1', '3', '', '', ''],
#                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '3', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '1', '9', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '2', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '5', '1', '2', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '4', '1', '2', '8', '', '', ''],
#                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '2', '8', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
