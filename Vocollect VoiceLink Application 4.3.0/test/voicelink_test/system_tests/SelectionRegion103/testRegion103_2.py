#Selection Region 103. Single assignment, multiple open containers, short product and partial test
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion103_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion103_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                       '2,selection region 2,0,\n'
                                       '3,selection region 3,0,\n'
                                       '\n')
        self.set_server_response('3,dry grocery,1,1,1,1,1,1,1,0,2,1,1,1,0,1,1,4,0,0,2,,,,,0,0,1,2,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,4,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L2,1,pre 1,A 1,post 1,S 2,2,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,5,L3,1,pre 2,A 1,post 2,S 3,7,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L4,1,pre 1,A 1,post 1,S 4,11,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L2,1,pre 1,A 1,post 1,S 2,13,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,8,L6,1,pre 2,A 1,post 2,S 6,17,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,19,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,3,L3,1,pre 2,A 1,post 2,S 3,2,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,C,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12346,Store 123,,C,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct   
                                   '3!',             # Region?
                                   'yes',            # 'selection region 3, correct?'
                                   'ready',          # To receive work, say ready
                                   'ready',          # ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',          # Open 01
                                   '05!',            # Printer?
                                   'yes',            # printer 05, correct?
                                   'no',             # Base summary?
                                   'yes',            # Pick base items?
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 3
                                   '9',              # Pick 9    pick message
                                   '00',             # S 6
                                   'skip slot',      # Pick 17    pick message
                                   'yes',            # Skip slot, correct?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 1
                                   '20',             # Pick 22    pick message
                                   'yes',            # 'You said 20 asked for 22. Is this a short product?'
                                   '11ready',        # lot text
                                   '15!',            # Quantity for this lot text
                                   '5',              # Pick 5    pick message
                                   '22ready',        # lot text
                                   '5!',             # Quantity for this lot text
                                   '00',             # S 2
                                   '15',             # Pick 15    pick message
                                   '00',             # S 4
                                   'partial',        # Pick 11    pick message
                                   'yes',            # partial, correct?
                                   '10',             # Quantity?
                                   'yes',            # new container, correct?
                                   'yes',            # close current container?
                                   'ready',          # Load at Location 1
                                   '1000',             # Confirm Delivery
                                   'left',           # Position?
                                   'yes',            # left, correct?
                                   'ready',          # Open 03
                                   'yes',            # printer 05, correct?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 4
                                   '1',              # Pick 1    pick message
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 6
                                   '17',             # Pick 17    pick message
                                   'ready',          # Load at Location 1
                                   '1000',             # Confirm Delivery
                                   'left',           # Position?
                                   'yes',            # left, correct?
                                   '-')              # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 3, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Open 01',
                              'Printer?',
                              'printer <spell>05</spell>, correct?',
                              'Base summary?',
                              'Pick base items?',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 3',
                              'Pick 9 ,   pick message',
                              'S 6',
                              'Pick 17 ,   pick message',
                              'Skip slot, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 22 ,   pick message',
                              'You said 20 asked for 22. Is this a short product?',
                              'lot text',
                              'Quantity for this lot text',
                              'Pick 5 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              'S 2',
                              'Pick 15 ,   pick message',
                              'S 4',
                              'Pick 11 ,   pick message',
                              'partial, correct?',
                              'Quantity?',
                              'new container, correct?',
                              'close current container?',
                              'Load at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Open 03',
                              'printer <spell>05</spell>, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 4',
                              'Pick 1 ,   pick message',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 6',
                              'Pick 17 ,   pick message',
                              'Picking complete',
                              'Load at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '05', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L3', '7', '1', '1', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L3', '2', '1', '1', '3', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L6', '0', 'N'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTSendLot', '11', '15', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '1', '4', '1234A', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '12', '0', '1', '9', '1234A', '', ''],
                                      ['prTaskLUTSendLot', '22', '5', '12345', '9'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '1', '9', '1234A', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '2', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '13', '1', '1', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L4', '10', '0', '1', '6', '', '', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '1', '', '1', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '05', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L4', '1', '1', '3', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L6', '17', '1', '3', '8', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
