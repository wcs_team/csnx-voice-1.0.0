# Selection region 112.  Test with case label check digits
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testRegion112_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion112_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                       '2,pallet jack,1,0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '5,selection region 105,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '10,selection region 110,0,\n'
                                       '11,selection region 111,0,\n'
                                       '12,selection region 112,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('12,Region 112,1,0,1,1,1,0,0,0,2,1,1,0,0,2,1,5,0,0,2,X                                           ,X                                  ,X                         ,X                ,0,0,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('1120001,0,\n'
                                       '\n')
        self.set_server_response('117,0,546,1120001,1,1.00,1,       00,0,0,,0,\n'
                                       '\n')
        self.set_server_response('B,1,5451,940,112,Building 939 Site 1,9 3 9,Bay 939,9 3 9,1,,939,0,0.0,0.0,0,939,,,Item 939 Site 1,939,939,546,1120001,1,0,1,10,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'N,0,5452,949,112,Building 948 Site 1,9 4 8,Bay 948,9 4 8,1,,948,0,0.0,0.0,0,948,,,Item 948 Site 1,948,948,546,1120001,1,0,1,11,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'B,1,5453,762,112,Building 761 Site 1,7 6 1,Bay 761,7 6 1,1,,761,0,0.0,0.0,0,761,,,Item 761 Site 1,761,761,546,1120001,1,0,1,12,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'N,0,5454,976,112,Building 975 Site 1,9 7 5,Bay 975,9 7 5,1,,975,0,0.0,0.0,0,975,,,Item 975 Site 1,975,975,546,1120001,1,0,1,13,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'B,1,5455,861,112,Building 860 Site 1,8 6 0,Bay 860,8 6 0,1,,860,0,0.0,0.0,0,860,,,Item 860 Site 1,860,860,546,1120001,1,0,1,14,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'N,0,5456,918,112,Building 917 Site 1,9 1 7,Bay 917,9 1 7,1,,917,0,0.0,0.0,0,917,,,Item 917 Site 1,917,917,546,1120001,1,0,1,15,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'B,1,5457,975,112,Building 974 Site 1,9 7 4,Bay 974,9 7 4,1,,974,0,0.0,0.0,0,974,,,Item 974 Site 1,974,974,546,1120001,1,0,1,16,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'N,0,5458,790,112,Building 789 Site 1,7 8 9,Bay 789,7 8 9,1,,789,0,0.0,0.0,0,789,,,Item 789 Site 1,789,789,546,1120001,1,0,1,17,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'B,1,5459,871,112,Building 870 Site 1,8 7 0,Bay 870,8 7 0,1,,870,0,0.0,0.0,0,870,,,Item 870 Site 1,870,870,546,1120001,1,0,1,18,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       'N,0,5460,849,112,Building 848 Site 1,8 4 8,Bay 848,8 4 8,1,,848,0,0.0,0.0,0,848,,,Item 848 Site 1,848,848,546,1120001,1,0,1,19,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('126,0000000126,26,546,1120001,,O,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('126,0000000126,26,546,1120001,,C,0,0,\n'
                                       '\n')
        self.set_server_response('127,0000000127,27,546,1120001,,O,0,0,\n'
                                       '126,0000000126,26,546,1120001,,C,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('127,0000000127,27,546,1120001,,C,0,0,\n'
                                       '126,0000000126,26,546,1120001,,C,0,0,\n'
                                       '\n')
        self.set_server_response('128,0000000128,28,546,1120001,,O,0,0,\n'
                                       '127,0000000127,27,546,1120001,,C,0,0,\n'
                                       '126,0000000126,26,546,1120001,,C,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   'yes',            # 123, correct?
                                   '1',              # Vehicle Type?
                                   'yes',            # fork lift, correct?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   '12',             # Region?
                                   'yes',            # selection region 112, correct?
                                   '20001',          # Work ID?
                                   'yes',            # 20001, correct?
                                   'ready',          # ID 1120001, has a goal time of 1.0 minute, say ready
                                   'ready',          # Open 26
                                   'no',             # Base summary?
                                   'no',             # Pick base items?
                                   'ready',          # Building 939 Site 1
                                   'ready',          # Aisle 9 3 9
                                   'ready',          # Bay 939
                                   '939',            # 9 3 9
                                   '10',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 948 Site 1
                                   'ready',          # Aisle 9 4 8
                                   'ready',          # Bay 948
                                   '948',            # 9 4 8
                                   '11',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 761 Site 1
                                   'new container',  # Aisle 7 6 1
                                   'yes',            # new container, correct?
                                   'ready',          # Open 27
                                   'ready',          # Aisle 7 6 1
                                   'ready',          # Bay 761
                                   '761',            # 7 6 1
                                   '12',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 975 Site 1
                                   'ready',          # Aisle 9 7 5
                                   'ready',          # Bay 975
                                   '975',            # 9 7 5
                                   '13',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 860 Site 1
                                   'ready',          # Aisle 8 6 0
                                   'ready',          # Bay 860
                                   '860',            # 8 6 0
                                   '25',             # Case label?
                                   '14',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 917 Site 1
                                   'ready',          # Aisle 9 1 7
                                   'ready',          # Bay 917
                                   '917',            # 9 1 7
                                   '15',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 974 Site 1
                                   'ready',          # Aisle 9 7 4
                                   'ready',          # Bay 974
                                   '974',            # 9 7 4
                                   '16',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 789 Site 1
                                   'ready',          # Aisle 7 8 9
                                   'ready',          # Bay 789
                                   '789',            # 7 8 9
                                   '17',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 870 Site 1
                                   'ready',          # Aisle 8 7 0
                                   'ready',          # Bay 870
                                   'new container',  # 8 7 0
                                   'yes',            # new container, correct?
                                   'ready',          # Open 28
                                   '870',            # 8 7 0
                                   '18',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Building 848 Site 1
                                   'ready',          # Aisle 8 4 8
                                   'ready',          # Bay 848
                                   '848',            # 8 4 8
                                   '19',             # Case label?
                                   '1',              # Pick 1    Pick Message Site 1
                                   'ready',          # Assignment complete.  For next assignment, say ready
                                   '-')              # Work ID?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              '123, correct?',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 112, correct?',
                              'Work ID?',
                              '20001, correct?',
                              'ID 1120001, has a goal time of 1.0 minute, say ready',
                              'Open 26',
                              'Base summary?',
                              'Pick base items?',
                              'Building 939 Site 1',
                              'Aisle 9 3 9',
                              'Bay 939',
                              '9 3 9',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 948 Site 1',
                              'Aisle 9 4 8',
                              'Bay 948',
                              '9 4 8',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 761 Site 1',
                              'Aisle 7 6 1',
                              'new container, correct?',
                              'Open 27',
                              'Aisle 7 6 1',
                              'Bay 761',
                              '7 6 1',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 975 Site 1',
                              'Aisle 9 7 5',
                              'Bay 975',
                              '9 7 5',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 860 Site 1',
                              'Aisle 8 6 0',
                              'Bay 860',
                              '8 6 0',
                              'Case label?',
                              'wrong 25, try again',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 917 Site 1',
                              'Aisle 9 1 7',
                              'Bay 917',
                              '9 1 7',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 974 Site 1',
                              'Aisle 9 7 4',
                              'Bay 974',
                              '9 7 4',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 789 Site 1',
                              'Aisle 7 8 9',
                              'Bay 789',
                              '7 8 9',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 870 Site 1',
                              'Aisle 8 7 0',
                              'Bay 870',
                              '8 7 0',
                              'new container, correct?',
                              'Open 28',
                              '8 7 0',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 848 Site 1',
                              'Aisle 8 4 8',
                              'Bay 848',
                              '8 4 8',
                              'Case label?',
                              'Pick 1 ,   Pick Message Site 1',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready',
                              'Work ID?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '12', '3'],
                                      ['prTaskLUTRequestWork', '20001', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '117', '0', '0', '0'],
                                      ['prTaskLUTContainer', '117', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '117', '546', '', '', '', '2', ''],
                                      ['prTaskLUTUpdateStatus', '117', '', '2', 'N'],
                                      ['prTaskLUTPicked', '117', '546', '940', '1', '1', '126', '5451', '', '', ''],
                                      ['prTaskLUTPicked', '117', '546', '949', '1', '1', '126', '5452', '', '', ''],
                                      ['prTaskLUTContainer', '117', '546', '', '126', '', '1', ''],
                                      ['prTaskLUTContainer', '117', '546', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '117', '546', '762', '1', '1', '127', '5453', '', '', ''],
                                      ['prTaskLUTPicked', '117', '546', '976', '1', '1', '127', '5454', '', '', ''],
                                      ['prTaskLUTPicked', '117', '546', '861', '1', '1', '127', '5455', '', '', ''],
                                      ['prTaskLUTPicked', '117', '546', '918', '1', '1', '127', '5456', '', '', ''],
                                      ['prTaskLUTPicked', '117', '546', '975', '1', '1', '127', '5457', '', '', ''],
                                      ['prTaskLUTPicked', '117', '546', '790', '1', '1', '127', '5458', '', '', ''],
                                      ['prTaskLUTContainer', '117', '546', '', '127', '', '1', ''],
                                      ['prTaskLUTContainer', '117', '546', '', '', '', '2', ''],
                                      ['prTaskLUTPicked', '117', '546', '871', '1', '1', '128', '5459', '', '', ''],
                                      ['prTaskLUTPicked', '117', '546', '849', '1', '1', '128', '5460', '', '', ''],
                                      ['prTaskLUTStopAssignment', '117'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
