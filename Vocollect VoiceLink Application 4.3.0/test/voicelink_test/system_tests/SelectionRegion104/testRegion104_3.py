# Selection Region 104 - Verify new container and close container commands.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion104_3(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion104_3(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                       '2,pallet jack,1,0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '5,selection region 105,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('4,Region 104,1,1,1,1,1,0,0,0,2,1,1,0,0,1,1,0,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,1,1,2,0,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM9,0,0.0,0.0,00,00,,,item description 9,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,item description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM8,0,0.0,0.0,00,00,,,item description 8,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,0.0,0.0,00,00,,,item description 10,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,item description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,11,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,3412,12,12345,Store 345,,O,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('2,5623,23,12345,Store 345,,O,0,0,\n'
                                       '1,3412,12,12345,Store 345,,O,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,1022,Operator cannot have duplicate container numbers open\n'
                                       '\n')
        self.set_server_response('3,7845,45,12345,Store 345,,O,0,0,\n'
                                       '2,5623,23,12345,Store 345,,O,0,0,\n'
                                       '1,3412,12,12345,Store 345,,O,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,7845,45,12345,Store 345,,O,0,0,\n'
                                       '1,3412,12,12345,Store 345,,O,0,0,\n'
                                       '2,5623,23,12345,Store 345,,C,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Vehicle Type?
                                   'yes',              # fork lift, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct
                                   '4!',               # Region?
                                   'yes',              # selection region 4, correct?
                                   'ready',            # To receive work, say ready
                                   'ready',            # ID Store 345, has a goal time of 15 minutes, say ready
                                   '3412!',            # New Container ID?
                                   'yes',              # 3412, correct?
                                   'ready',            # pre 1
                                   'ready',            # Aisle A 1
                                   'ready',            # post 1
                                   '00',               # S 1
                                   '1',                # Pick 1   item description 9 pick message
                                   'new container',    # S 1
                                   'yes',              # new container, correct?
                                   'no',               # close current container?
                                   '5623!',            # New Container ID?
                                   'yes',              # 5623, correct?
                                   '00',               # S 1
                                   '8',                # Pick 8   item description 14 pick message
                                   '45!',              # Container?
                                   '12!',              # Container?
                                   '00',               # S 1
                                   'new container',    # Pick 1   item description 8 pick message
                                   'yes',              # new container, correct?
                                   '4512!',            # New Container ID?
                                   'yes',              # 4512, correct?
                                   'ready',            # Operator cannot have duplicate container numbers open, To continue say ready
                                   '7845!',            # New Container ID?
                                   'yes',              # 7845, correct?
                                   '8',                # Pick 1   item description 8 pick message
                                   '1',                # Pick 1   item description 8 pick message
                                   '23!',              # Container?
                                   'yes',              # 23, correct?
                                   'ready',            # pre 2
                                   'ready',            # Aisle A 1
                                   'ready',            # post 2
                                   '00',               # S 1
                                   '3',                # Pick 3   item description 13 pick message
                                   '45!',              # Container?
                                   'ready',            # pre 1
                                   'close container',  # Aisle A 1
                                   '23!',              # Container?
                                   'yes',              # 23, correct?
                                   'ready',            # Aisle A 1
                                   'ready',            # post 1
                                   '00',               # S 1
                                   '7',                # Pick 7   item description 12 pick message
                                   '23',               # Container?
                                   '12',               # Container?
                                   '00',               # S 1
                                   '2',                # Pick 2   item description 10 pick message
                                   '45',               # Container?
                                   'ready',            # Deliver to Location 11
                                   '01',               # Confirm Delivery
                                   '-')                # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 104, correct?',
                              'To receive work, say ready',
                              'ID Store 345, has a goal time of 15 minutes, say ready',
                              'New Container ID?',
                              '3412, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 1 ,  item description 9, pick message',
                              'S 1',
                              'new container, correct?',
                              'close current container?',
                              'New Container ID?',
                              '5623, correct?',
                              'S 1',
                              'Pick 8 ,  item description 14, pick message',
                              'Container?',
                              'Wrong 45. Try again.',
                              'Container?',
                              'S 1',
                              'Pick 1 ,  item description 8, pick message',
                              'new container, correct?',
                              'New Container ID?',
                              '4512, correct?',
                              'Operator cannot have duplicate container numbers open, To continue say ready',
                              'New Container ID?',
                              '7845, correct?',
                              'Pick 1 ,  item description 8, pick message',
                              'You said 8 only asked for 1. Try again.',
                              'Pick 1 ,  item description 8, pick message',
                              'Container?',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1',
                              'Pick 3 ,  item description 13, pick message',
                              'Container?',
                              'pre 1',
                              'Aisle A 1',
                              'Container?',
                              '23, correct?',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 7 ,  item description 12, pick message',
                              'Container?',
                              'Wrong 23. Try again.',
                              'Container?',
                              'S 1',
                              'Pick 2 ,  item description 10, pick message',
                              'Container?',
                              'Picking complete',
                              'Deliver to Location 11',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '4', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '3412', '2', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '3', '', '', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '5623', '2', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '1', '10', '', '', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '4512', '2', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '7845', '2', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '2', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '3', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '3', '8', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '3', '11', '', '', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '2', '23', '1', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '1', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '4', '1', '1', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '3', '9', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
