from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion105_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion105_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('5,selection region 5,0,\n'
                                       '\n')
        self.set_server_response('5,dry grocery,1,1,1,1,1,1,0,0,2,1,0,0,0,1,1,4,0,0,2,,,,,0,0,0,2,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,30,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,4,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L2,1,pre 1,A 1,post 1,S 2,2,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,5,L3,1,pre 2,A 1,post 2,S 3,7,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L4,1,pre 1,A 1,post 1,S 4,11,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L2,1,pre 1,A 1,post 1,S 2,13,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,8,L6,1,pre 2,A 1,post 2,S 6,17,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,19,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,3,L3,1,pre 2,A 1,post 2,S 3,2,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   'ready',          # To receive work, say ready
                                   'ready',          # ID Store 123, has a goal time of 30 minutes, say ready
                                   'yes',            # Base summary?
                                   'ready',          # pre 2, aisle A 1, post 2, slot S 3, Item Description, ID Store 123, quantity 9, say ready
                                   'ready',          # pre 2, aisle A 1, post 2, slot S 6, Item Description, ID Store 123, quantity 17, say ready
                                   'yes',            # Pick base items?
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 3
                                   'partial',        # Pick 9    pick message
                                   '9',              # Pick 9    pick message
                                   '00',             # S 6
                                   '10',             # Pick 17    pick message
                                   'yes',            # You said 10 asked for 17. Is this a short product?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 1
                                   'skip slot',      # Pick 22    pick message
                                   'yes',            # Skip slot, correct?
                                   '00',             # S 2
                                   '15',             # Pick 15    pick message
                                   '00',             # S 4
                                   'repick skips',   # Pick 11    pick message
                                   'yes',            # repick skips, correct?
#DISCREPANCY: Legacy task always prompts entire location when repicking skips.  Prompt is pre 1                                                                      
#                                   'ready',          # post 1
                                   '00',             # S 1
                                   '22',             # Pick 22    pick message
                                   '1234ready',      # lot text
                                   '20!',            # Quantity for this lot text
                                   '2',              # Pick 2    pick message
                                   '1111ready',      # lot text
                                   '2!',             # Quantity for this lot text
                                   '00',             # S 4
                                   'how much more',  # Pick 11    pick message
                                   'new container',  # Pick 11    pick message
                                   '11',             # Pick 11    pick message
                                   'ready',          # Load at Location 1
                                   '1000',             # Confirm Delivery
                                   'left',           # Position?
                                   'yes',            # left, correct?
                                   '-')              # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 30 minutes, say ready',
                              'Base summary?',
                              'pre 2, aisle A 1, post 2, slot S 3, item description, quantity 9, say ready',
                              'pre 2, aisle A 1, post 2, slot S 6, item description, quantity 17, say ready',
                              'Pick base items?',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 3',
                              'Pick 9 ,   pick message',
                              'Partial not allowed when not picking to containers',
                              'Pick 9 ,   pick message',
                              'S 6',
                              'Pick 17 ,   pick message',
                              'You said 10 asked for 17. Is this a short product?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 22 ,   pick message',
                              'Skip slot, correct?',
                              'S 2',
                              'Pick 15 ,   pick message',
                              'S 4',
                              'Pick 11 ,   pick message',
                              'repick skips, correct?',
                              'S 1',
                              'Pick 22 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              'Pick 2 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              'S 4',
                              'Pick 11 ,   pick message',
                              '11 remaining at 1 line item',
                              'Pick 11 ,   pick message',
                              'New container not allowed',
                              'Pick 11 ,   pick message',
                              'Picking complete',
                              'Load at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '5', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L3', '7', '1', '', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L3', '2', '1', '', '3', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L6', '10', '1', '', '8', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTUpdateStatus', '1', 'L1', '0', 'S'],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '2', '1', '', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '13', '1', '', '7', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTSendLot', '1234', '20', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '', '4', '1234A', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '17', '0', '', '9', '1234A', '', ''],
                                      ['prTaskLUTSendLot', '1111', '2', '12345', '9'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '', '9', '1234A', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L4', '11', '1', '', '6', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
