# Selection Region 105 - Verify re-pick skips not available for skipped base items
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion105_2(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion105_2(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                       '2,pallet jack,1,0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '5,selection region 105,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('5,Region 105,1,1,1,1,1,1,0,0,2,1,0,0,0,0,1,0,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,0,0,0\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,2,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM9,0,0.0,0.0,00,00,,,item description 9,Size,UPC 9,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,3,L20,1,pre 1,A 1,post 20,S 1,5,,ITEM20,0,0.0,0.0,00,00,,,item description 20,Size,UPC 20,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'B,0,4,L3,1,pre 3,A 1,post 3,S 1,1,,ITEM8,0,0.0,0.0,00,00,,,item description 8,Size,UPC 8,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,2,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'B,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'B,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,0.0,0.0,00,00,,,item description 10,Size,UPC 10,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'B,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,item description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,11,L4,1,pre 1,A 1,post 4,S 1,3,,ITEM15,0,0.0,0.0,00,00,,,item description 15,Size,UPC 15,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,12,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'B,0,13,L5,1,pre 1,A 1,post 4,S 5,4,,ITEM16,0,0.0,0.0,00,00,,,item description 16,Size,UPC 16,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,14,L6,1,pre 2,A 1,post 6,S 1,1,,ITEM17,0,0.0,0.0,00,00,,,item description 17,Size,UPC 17,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,15,L7,1,pre 2,A 1,post 6,S 7,8,,ITEM18,0,0.0,0.0,00,00,,,item description 18,Size,UPC 18,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '1!',             # Vehicle Type?
                                   'yes',           # fork lift, correct?
                                   '3!',             # Function?
                                   'yes',           # normal assignments, correct?
                                   '5!',             # Region?
                                   'yes',           # selection region 105, correct?
                                   'ready',         # To receive work, say ready
                                   'ready',         # ID Store 345, has a goal time of 15 minutes, say ready
                                   'yes',           # Base summary?
                                   'ready',         # pre 3, aisle A 1, post 3, slot S 1, item description 8, ID Store 345, quantity 1, say ready
                                   'ready',         # pre 1, aisle A 1, post 1, slot S 1, item description 12, ID Store 345, quantity 7, say ready
                                   'ready',         # pre 1, aisle A 1, post 1, slot S 1, item description 14, ID Store 345, quantity 3, say ready
                                   'ready',         # pre 1, aisle A 1, post 4, slot S 5, item description 16, ID Store 345, quantity 4, say ready
                                   'yes',           # Pick base items?
                                   'ready',         # pre 3
                                   'ready',         # Aisle A 1
                                   'ready',         # post 3
                                   '00',            # S 1
                                   '1',             # Pick 1   item description 8 pick message
                                   'ready',         # pre 1
                                   'ready',         # Aisle A 1
                                   'ready',         # post 1
                                   'skip slot',     # S 1
                                   'yes',           # Skip slot, correct?
                                   'ready',         # post 4
                                   'skip slot',     # S 5
                                   'yes',           # Skip slot, correct?
                                   'ready',         # post 1
                                   'repick skips',  # S 1
                                   'yes',           # repick skips, correct?
                                   '00',            # S 1
                                   '2',             # Pick 2   item description 9 pick message
                                   'ready',         # post 20
                                   '00',            # S 1
                                   '5',             # Pick 5   item description 20 pick message
                                   'ready',         # pre 2
                                   'ready',         # Aisle A 1
                                   'ready',         # post 2
                                   '00',            # S 1
                                   '4',             # Pick 4   item description 13 pick message
                                   'ready',         # pre 1
                                   'ready',         # Aisle A 1
                                   'ready',         # post 1
                                   '00',            # S 1
                                   '7',             # Pick 7   item description 12 pick message
                                   '00',            # S 1
                                   '2',             # Pick 2   item description 10 pick message
                                   'skip slot',     # S 1
                                   'yes',           # Skip slot, correct?
                                   'ready',         # post 4
                                   '00',            # S 1
                                   '3',             # Pick 3   item description 15 pick message
                                   'skip slot',     # S 5
                                   'yes',           # Skip slot, correct?
                                   'ready',         # pre 2
                                   'ready',         # Aisle A 1
                                   'ready',         # post 6
                                   '00',            # S 1
                                   '1',             # Pick 1   item description 17 pick message
                                   'repick skips',  # S 7
                                   'yes',           # repick skips, correct?
                                   'ready',         # pre 1
                                   'ready',         # Aisle A 1
                                   'ready',         # post 1
                                   '00',            # S 1
                                   '3',             # Pick 3   item description 14 pick message
                                   'ready',         # post 4
                                   '00',            # S 5
                                   '4',             # Pick 4   item description 16 pick message
                                   'ready',         # pre 2
                                   'ready',         # Aisle A 1
                                   'ready',         # post 6
                                   '00',            # S 7
                                   '8',             # Pick 8   item description 18 pick message
                                   'ready',         # Deliver to Location 11
                                   '-')             # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 105, correct?',
                              'To receive work, say ready',
                              'ID Store 345, has a goal time of 15 minutes, say ready',
                              'Base summary?',
                              'pre 3, aisle A 1, post 3, slot S 1, item description 8, quantity 1, say ready',
                              'pre 1, aisle A 1, post 1, slot S 1, item description 12, quantity 7, say ready',
                              'pre 1, aisle A 1, post 1, slot S 1, item description 14, quantity 3, say ready',
                              'pre 1, aisle A 1, post 4, slot S 5, item description 16, quantity 4, say ready',
                              'Pick base items?',
                              'pre 3',
                              'Aisle A 1',
                              'post 3',
                              'S 1',
                              'Pick 1 ,  item description 8, pick message',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Skip slot, correct?',
                              'post 4',
                              'S 5',
                              'Skip slot, correct?',
                              'post 1',
                              'S 1',
                              'repick skips, correct?',
                              'no skips to repick, returning to picking',
                              'S 1',
                              'Pick 2 ,  item description 9, pick message',
                              'post 20',
                              'S 1',
                              'Pick 5 ,  item description 20, pick message',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1',
                              'Pick 4 ,  item description 13, pick message',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 7 ,  item description 12, pick message',
                              'S 1',
                              'Pick 2 ,  item description 10, pick message',
                              'S 1',
                              'Skip slot, correct?',
                              'post 4',
                              'S 1',
                              'Pick 3 ,  item description 15, pick message',
                              'S 5',
                              'Skip slot, correct?',
                              'pre 2',
                              'Aisle A 1',
                              'post 6',
                              'S 1',
                              'Pick 1 ,  item description 17, pick message',
                              'S 7',
                              'repick skips, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 3 ,  item description 14, pick message',
                              'post 4',
                              'S 5',
                              'Pick 4 ,  item description 16, pick message',
                              'pre 2',
                              'Aisle A 1',
                              'post 6',
                              'S 7',
                              'Pick 8 ,  item description 18, pick message',
                              'Picking complete',
                              'Deliver to Location 11',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '5', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L3', '1', '1', '', '4', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L1', '0', 'N'],
                                      ['prTaskLUTUpdateStatus', '1', 'L5', '0', 'N'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '', '2', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L20', '5', '1', '', '3', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '2', '1', '', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '', '8', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '1', '1', '', '12', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '4', '1', '', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '', '9', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L1', '0', 'S'],
                                      ['prTaskLUTPicked', '1', '12345', 'L4', '3', '1', '', '11', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L5', '0', 'S'],
                                      ['prTaskLUTPicked', '1', '12345', 'L6', '1', '1', '', '14', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '', '10', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L5', '4', '1', '', '13', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L7', '8', '1', '', '15', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
