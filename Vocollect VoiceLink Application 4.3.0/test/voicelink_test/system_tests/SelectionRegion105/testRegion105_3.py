# Verify skipping, re-picking skips, shorting and going back for shorts works correctly in 
# Region 105, automatic issuance with no containers.
# Rename the file testGenerator_105_3.txt to testGenerator_105_3.py and copy the file to the
# root level of the VoiceLink project and run to regenerate the python unit test if necessary.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testRegion105_3(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)

    def testRegion105_3(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '105,Region 105,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '108,Region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '10,selection region 110,0,\n'
                                       '111,Region 111,0,\n'
                                       '12,selection region 112,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('5,Region 105,1,1,1,1,1,1,0,0,2,1,0,0,0,0,1,0,1,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,0,0,0\n'
                                       '\n')
        self.set_server_response('203,0,783,1111001,1,18.00,180,       00,0,0,,0,\n'
                                       '\n')
        self.set_server_response('B,1,8113,1302,111,Building 3 Site 1,4 E,Bay 5,5 0,4,By Weight,1349,1,52.3418,54.478199999999994,        0,50,,,HEN TURKEY,53.41,7262306325,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'N,0,8112,1284,111,Building 2 Site 1,3,,3 2,1,,1330,0,0.0,0.0,        0,32,,,KOOL-AID BLUE MOON BERRY,5.4,4300095389,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8111,1282,111,Building 2 Site 1,2,,3 0,3,,1304,0,0.0,0.0,        0,30,,,PENCIL MECHANICAL,0.1,3977918025,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8110,1266,111,Building 1 Site 1,3,,1 4,2,,1334,0,0.0,0.0,        0,14,,,PF VANILLA 3 LAYER CAKE,8.9,9999953903,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8109,1264,111,Building 1 Site 1,3,,1 2 ,2,Packs,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'B,1,8108,1264,111,Building 1 Site 1,3,,1 2,6,Boxes,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'N,0,8107,1259,111,Building 1 Site 1,2,,0 7,3,,1350,1,58.8,61.2,        0,07,,,C &amp; H CANE SUGAR,60,1580003062,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       'B,1,8106,1257,111,Building 1 Site 1,1,,0 5,1,,1305,1,0.098,0.10200000000000001,        0,05,,,TUMS PEPPERMINT,0.1,7385408100,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'N,0,8105,1255,111,Building 1 Site 1,1,,0 3,13,,1303,0,0.0,0.0,        0,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'B,1,8104,1253,111,Building 1 Site 1,1,,0 1,26,Eaches,1302,0,0.0,0.0,        0,01,,,TUMS 4 FLAVOR JAR,0.1,7161030002,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('G,1,8113,1302,111,Building 3 Site 1,4 E,Bay 5,5 0,4,By Weight,1349,1,52.3418,54.478199999999994,        2,50,,,HEN TURKEY,53.41,7262306325,783,1111001,18,0,18,,,0,,Item has PVID,0,0,0,0,0,\n'
                                       'G,0,8107,1259,111,Building 1 Site 1,2,,0 7,3,,1350,1,58.8,61.2,0,07,,,C &amp; H CANE SUGAR,60,1580003062,783,1111001,18,0,18,,,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',          # Password?
                                   '3!',            # Function?
                                   'yes',           # normal assignments, correct?
                                   '105',           # Region?
                                   'yes',           # Region 105, correct?
                                   'ready',         # To receive work, say ready
                                   'ready',         # ID 1111001, has a goal time of 18.0 minutes, say ready
                                   'yes',           # Base summary?
                                   'ready',         # Building 3 Site 1, aisle 4 E, Bay 5, slot 5 0, hen turkey, ID 1111001, quantity 4, say ready
                                   'ready',         # Building 1 Site 1, aisle 3, , slot 1 2, rice krispie, ID 1111001, quantity 6, say ready
                                   'ready',         # Building 1 Site 1, aisle 1, , slot 0 5, tums peppermint, ID 1111001, quantity 1, say ready
                                   'ready',         # Building 1 Site 1, aisle 1, , slot 0 1, tums 4 flavor jar, ID 1111001, quantity 26, say ready
                                   'yes',           # Pick base items?
                                   'ready',         # Building 3 Site 1
                                   'ready',         # Aisle 4 E
                                   'ready',         # Bay 5
                                   '50',            # 5 0
                                   '2',             # Pick 4 By Weight,   Item has PVID
                                   'yes',           # You said 2 asked for 4. Is this a short product?
                                   '53.30',         # weight 1 of 2
                                   '53.31',         # weight 2 of 2
                                   'ready',         # Entries complete
                                   'ready',         # Building 1 Site 1
                                   'ready',         # Aisle 3
                                   '12',            # 1 2
                                   '6',             # Pick 6 Boxes,   Location has CD Site 1
                                   'ready',         # Aisle 1
                                   'skip slot',     # 0 5
                                   'yes',           # Skip slot, correct?
                                   '01',            # 0 1
                                   '26',            # Pick 26 Eaches,   Item has PVID
                                   'ready',         # Building 2 Site 1
                                   'ready',         # Aisle 3
                                   'skip slot',     # 3 2
                                   'yes',           # Skip slot, correct?
                                   'ready',         # Aisle 2
                                   '30',            # 3 0
                                   '3',             # Pick 3 ,   Location has CD Site 1
                                   'ready',         # Building 1 Site 1
                                   'ready',         # Aisle 3
                                   '14',            # 1 4
                                   'repick skips',  # Pick 2 ,   Location has CD Site 1
                                   'yes',           # repick skips, correct?
                                   'ready',         # Building 2 Site 1
                                   'ready',         # Aisle 3
                                   '32',            # 3 2
                                   '1',             # Pick 1 ,   Location has CD Site 1
                                   'ready',         # Building 1 Site 1
                                   'ready',         # Aisle 3
                                   '14',            # 1 4
                                   '2',             # Pick 2 ,   Location has CD Site 1
                                   '12',            # 1 2
                                   '2',             # Pick 2 Packs,   Location has CD Site 1
                                   'ready',         # Aisle 2
                                   '07',            # 0 7
                                   '0',             # Pick 3 ,   Location has CD Site 1
                                   'yes',           # You said 0 asked for 3. Is this a short product?
                                   'ready',         # Aisle 1
                                   'skip slot',     # 0 5
                                   'yes',           # Skip slot, correct?
                                   '03',            # 0 3
                                   '0!',            # Pick 13 ,   Item has PVID
                                   'no',            # You said 0 asked for 13. Is this a short product?
                                   'skip slot',     # Pick 13 ,   Item has PVID
                                   'yes',           # Skip slot, correct?
                                   'repick skips',  # 0 5
                                   'yes',           # repick skips, correct?
                                   'skip slot',     # 0 5
                                   'yes',           # Skip slot, correct?
                                   'repick skips',  # 0 3
                                   'yes',           # repick skips, correct?
                                   '05',            # 0 5
                                   '1',             # Pick 1 ,   Item has PVID
                                   '0.10',          # weight 1 of 1
                                   'ready',         # Entries complete
                                   '03',            # 0 3
                                   '13',            # Pick 13 ,   Item has PVID
                                   'ready',         # Building 3 Site 1
                                   'ready',         # Aisle 4 E
                                   'ready',         # Bay 5
                                   '50',            # 5 0
                                   '1',             # Pick 2 By Weight,   Item has PVID
                                   'yes',           # You said 1 asked for 2. Is this a short product?
                                   '59.45',         # weight 1 of 1
                                   'ready',         # 59.45 out of range
                                   '54.02',         # weight 1 of 1
                                   'ready',         # Entries complete
                                   'ready',         # Building 1 Site 1
                                   'ready',         # Aisle 2
                                   '07',            # 0 7
                                   '3',             # Pick 3 ,   Location has CD Site 1
                                   '58.90',         # weight 1 of 3
                                   '58.91',         # weight 2 of 3
                                   '58.92',         # weight 3 of 3
                                   'undo entry',    # Entries complete
                                   'last entry',    # last entry, all entries, or continue?
                                   'yes',           # re-enter item 3, correct?
                                   '58.94',         # weight 3 of 3
                                   'ready',         # Entries complete
                                   'ready',         # Deliver to Location 11
                                   '-')             # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'Region 105, correct?',
                              'To receive work, say ready',
                              'ID 1111001, has a goal time of 18.0 minutes, say ready',
                              'Base summary?',
                              'Building 3 Site 1, aisle 4 E, Bay 5, slot 5 0, hen turkey, quantity 4, say ready',
                              'Building 1 Site 1, aisle 3, slot 1 2, rice krispie, quantity 6, say ready',
                              'Building 1 Site 1, aisle 1, slot 0 5, tums peppermint, quantity 1, say ready',
                              'Building 1 Site 1, aisle 1, slot 0 1, tums 4 flavor jar, quantity 26, say ready',
                              'Pick base items?',
                              'Building 3 Site 1',
                              'Aisle 4 E',
                              'Bay 5',
                              '5 0',
                              'Pick 4 By Weight,   Item has PVID',
                              'You said 2 asked for 4. Is this a short product?',
                              'weight 1 of 2',
                              'weight 2 of 2',
                              'Entries complete',
                              'Building 1 Site 1',
                              'Aisle 3',
                              '1 2',
                              'Pick 6 Boxes,   Location has CD Site 1',
                              'Aisle 1',
                              '0 5',
                              'Skip slot, correct?',
                              '0 1',
                              'Pick 26 Eaches,   Item has PVID',
                              'Building 2 Site 1',
                              'Aisle 3',
                              '3 2',
                              'Skip slot, correct?',
                              'Aisle 2',
                              '3 0',
                              'Pick 3 ,   Location has CD Site 1',
                              'Building 1 Site 1',
                              'Aisle 3',
                              '1 4',
                              'Pick 2 ,   Location has CD Site 1',
                              'repick skips, correct?',
                              'Building 2 Site 1',
                              'Aisle 3',
                              '3 2',
                              'Pick 1 ,   Location has CD Site 1',
                              'Building 1 Site 1',
                              'Aisle 3',
                              '1 4',
                              'Pick 2 ,   Location has CD Site 1',
                              '1 2',
                              'Pick 2 Packs,   Location has CD Site 1',
                              'Aisle 2',
                              '0 7',
                              'Pick 3 ,   Location has CD Site 1',
                              'You said 0 asked for 3. Is this a short product?',
                              'Aisle 1',
                              '0 5',
                              'Skip slot, correct?',
                              '0 3',
                              'Pick 13 ,   Item has PVID',
                              'You said 0 asked for 13. Is this a short product?',
                              'Pick 13 ,   Item has PVID',
                              'Skip slot, correct?',
                              '0 5',
                              'repick skips, correct?',
                              'no skips to repick, returning to picking',
                              '0 5',
                              'Skip slot, correct?',
                              '0 3',
                              'repick skips, correct?',
                              '0 5',
                              'Pick 1 ,   Item has PVID',
                              'weight 1 of 1',
                              'Entries complete',
                              '0 3',
                              'Pick 13 ,   Item has PVID',
                              'Picking complete',
                              'shorts are reported',
                              'Going back for shorts',
                              'Picking shorts',
                              'Building 3 Site 1',
                              'Aisle 4 E',
                              'Bay 5',
                              '5 0',
                              'Pick 2 By Weight,   Item has PVID',
                              'You said 1 asked for 2. Is this a short product?',
                              'weight 1 of 1',
                              '59.45 out of range',
                              'weight 1 of 1',
                              'Entries complete',
                              'Building 1 Site 1',
                              'Aisle 2',
                              '0 7',
                              'Pick 3 ,   Location has CD Site 1',
                              'weight 1 of 3',
                              'weight 2 of 3',
                              'weight 3 of 3',
                              'Entries complete',
                              'last entry, all entries, or continue?',
                              're enter item 3, correct?',
                              'weight 3 of 3',
                              'Entries complete',
                              'Picking complete',
                              'Deliver to Location 11',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '105', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '203', '0', '1', '0'],
                                      ['prTaskLUTPicked', '203', '783', '1302', '1', '0', '', '8113', '', '53.30', ''],
                                      ['prTaskLUTPicked', '203', '783', '1302', '1', '1', '', '8113', '', '53.31', ''],
                                      ['prTaskLUTPicked', '203', '783', '1264', '6', '1', '', '8108', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '203', '1257', '0', 'N'],
                                      ['prTaskLUTPicked', '203', '783', '1253', '26', '1', '', '8104', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskLUTUpdateStatus', '203', '1284', '0', 'S'],
                                      ['prTaskLUTPicked', '203', '783', '1282', '3', '1', '', '8111', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskLUTPicked', '203', '783', '1284', '1', '1', '', '8112', '', '', ''],
                                      ['prTaskLUTPicked', '203', '783', '1266', '2', '1', '', '8110', '', '', ''],
                                      ['prTaskLUTPicked', '203', '783', '1264', '2', '1', '', '8109', '', '', ''],
                                      ['prTaskLUTPicked', '203', '783', '1259', '0', '1', '', '8107', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '203', '1257', '0', 'S'],
                                      ['prTaskLUTUpdateStatus', '203', '1255', '0', 'S'],
                                      ['prTaskLUTUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskLUTUpdateStatus', '203', '1257', '0', 'S'],
                                      ['prTaskLUTUpdateStatus', '203', '', '2', 'N'],
                                      ['prTaskLUTPicked', '203', '783', '1257', '1', '1', '', '8106', '', '0.10', ''],
                                      ['prTaskLUTPicked', '203', '783', '1255', '13', '1', '', '8105', '', '', ''],
                                      ['prTaskLUTGetPicks', '203', '0', '1', '0'],
                                      ['prTaskLUTPicked', '203', '783', '1302', '1', '1', '', '8113', '', '54.02', ''],
                                      ['prTaskLUTPicked', '203', '783', '1259', '1', '0', '', '8107', '', '58.90', ''],
                                      ['prTaskLUTPicked', '203', '783', '1259', '1', '0', '', '8107', '', '58.91', ''],
                                      ['prTaskLUTPicked', '203', '783', '1259', '1', '1', '', '8107', '', '58.94', ''],
                                      ['prTaskLUTGetDeliveryLocation', '203', '783'],
                                      ['prTaskLUTStopAssignment', '203'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
