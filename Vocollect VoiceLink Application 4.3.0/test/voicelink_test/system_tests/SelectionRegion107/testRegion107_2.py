#Region 107 - Verify  variable weights and PVID/slot verification.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion107_2(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion107_2(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('7,Region 107,1,0,3,1,1,1,1,0,0,1,1,0,0,1,0,3,0,1,2,XXXXX,XXX,XXXXXX,XXX,0,1,0,5,0,0,\n'
                                       '\n')
        self.set_server_response('123,0,\n'
                                       '234,0,\n'
                                       '345,0,\n'
                                       '456,0,\n'
                                       '\n')
        self.set_server_response('123,0,\n'
                                       '234,0,\n'
                                       '345,0,\n'
                                       '456,0,\n'
                                       '\n')
        self.set_server_response('123,0,\n'
                                       '234,0,\n'
                                       '345,0,\n'
                                       '456,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 345,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 567,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,1,1.0,10.0,00,00,,123,Item Description,Size,UPC 11,12345,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,4,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM11,1,1.0,10.0,00,,,123,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,5,L4,1,pre 3,A 3,post 3,S 3,3,,ITEM13,1,1.0,10.0,00,,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L5,1,pre 4,A 4,post 4,S 4,3,,ITEM14,1,1.0,10.0,00,123,,123,Item Description,Size,UPC 14,12346,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L6,1,pre 5,A 5,post 5,S 5,4,,ITEM15,1,1.0,10.0,00,123,,123,Item Description,Size,UPC 15,12347,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,8,L7,1,pre 6,A 6,post 6,S 6,5,,ITEM16,1,1.0,10.0,00,00,,123,Item Description,Size,UPC 16,12347,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,9,L8,1,pre 7,A 7,post 7,S 7,3,,ITEM17,1,1.0,10.0,00,00,,,Item Description,Size,UPC 17,12347,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,56565,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,67676,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 21,02,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 31,03,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   '7!',             # Region?
                                   'yes',            # selection region 107, correct?
                                   '123!',           # Work ID?
                                   'yes',            # 123, correct?
                                   '234!',           # Work ID?
                                   'yes',            # 234, correct?
                                   '345!',           # Work ID?
                                   'yes',            # 345, correct?
                                   'yes',            # Pick in reverse order?
                                   'ready',          # position 1, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',          # position 2, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',          # position 3, ID Store 567, has a goal time of 15 minutes, say ready
                                   '12121!',         # New Container ID? Position 1
                                   'yes',            # 12121, correct?
                                   '56',             # Printer?
                                   'yes',            # printer 56, correct?
                                   '34343!',         # New Container ID? Position 2
                                   'yes',            # 34343, correct?
                                   'yes',            # printer 56, correct?
                                   '56565!',         # New Container ID? Position 3
                                   'yes',            # 56565, correct?
                                   'yes',            # printer 56, correct?
                                   'yes',            # Base summary?
                                   'ready',          # pre 2, aisle A 2, post 2, slot S 2, Item Description, ID Store 123, quantity 5, say ready
                                   'ready',          # pre 7, aisle A 7, post 7, slot S 7, Item Description, ID Store 123, quantity 3, say ready
                                   'no',             # Pick base items?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00!',            # S 1 Pick 4    pick message
                                   'no',             # Is this a short product?
                                   'ready',          # S 1 Pick 4    pick message
                                   '123',            # S 1 Pick 4    pick message
                                   '12121',          # Put 4 in 1
                                   '1.23',           # weight 1 of 4
                                   '2.12',           # weight 2 of 4
                                   '2.34',           # weight 3 of 4
                                   '2.56',           # weight 4 of 4
                                   'ready',          # Entries complete
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 2
                                   'ready',          # post 2
                                   'ready',          # S 2 Pick 5    pick message
                                   'no',             # Is this a short product?
                                   '123',            # S 2 Pick 5    pick message
                                   '12121',          # Put 5 in 1
                                   'short product',  # weight 1 of 5
                                   'yes',            # you picked 0 of 5 items, is this a short product?
                                   'ready',          # pre 3
                                   'ready',          # Aisle A 3
                                   'ready',          # post 3
                                   'ready',          # S 3 Pick 3    pick message
                                   '34343',          # Put 3 in 2
                                   '2.45',           # weight 1 of 3
                                   '2.21',           # weight 2 of 3
                                   '1.23',           # weight 3 of 3
                                   'ready',          # Entries complete
                                   'ready',          # pre 4
                                   'ready',          # Aisle A 4
                                   'ready',          # post 4
                                   '123',            # S 4 Pick 3    pick message
                                   'yes',            # Identical Product verification and check digits. Is this a short product
                                   'ready',          # pre 5
                                   'ready',          # Aisle A 5
                                   'ready',          # post 5
                                   'ready',          # S 5 Pick 4    pick message
                                   '123',            # S 5 Pick 4    pick message
                                   'no',             # Identical Product verification and check digits. Is this a short product
                                   '56565',          # Put 4 in 3
                                   '2.34',           # weight 1 of 4
                                   '6.45',           # weight 2 of 4
                                   '3.45',           # weight 3 of 4
                                   '7.56',           # weight 4 of 4
                                   'ready',          # Entries complete
                                   'ready',          # pre 6
                                   'ready',          # Aisle A 6
                                   'ready',          # post 6
                                   'ready',          # S 6 Pick 5    pick message
                                   '123',            # S 6 Pick 5    pick message
                                   '56565',          # Put 5 in 3
                                   'short product',  # weight 1 of 5
                                   'no',             # you picked 0 of 5 items, is this a short product?
                                   'short product',  # weight 1 of 5
                                   'yes',            # you picked 0 of 5 items, is this a short product?
                                   'ready',          # pre 7
                                   'ready',          # Aisle A 7
                                   'ready',          # post 7
                                   '00',             # S 7 Pick 3    pick message
                                   '56565',          # Put 3 in 3
                                   '2.21',           # weight 1 of 3
                                   '1.23',           # weight 2 of 3
                                   '1.34',           # weight 3 of 3
                                   'ready',          # Entries complete
                                   'ready',          # Deliver position 1 ID Store 123 to Location 11
                                   '01',             # Confirm Delivery
                                   'ready',          # Deliver position 2 ID Store 345 to Location 21
                                   '02',             # Confirm Delivery
                                   'ready',          # Deliver position 3 ID Store 567 to Location 31
                                   '03',             # Confirm Delivery
                                   '-')              # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 107, correct?',
                              'Work ID?',
                              '123, correct?',
                              'Work ID?',
                              '234, correct?',
                              'Work ID?',
                              '345, correct?',
                              'Pick in reverse order?',
                              'receiving picks in reverse order, please wait',
                              'position 1, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 567, has a goal time of 15 minutes, say ready',
                              'New Container ID? Position 1',
                              '12121, correct?',
                              'Printer?',
                              'printer <spell>56</spell>, correct?',
                              'New Container ID? Position 2',
                              '34343, correct?',
                              'printer <spell>56</spell>, correct?',
                              'New Container ID? Position 3',
                              '56565, correct?',
                              'printer <spell>56</spell>, correct?',
                              'Base summary?',
                              'pre 2, aisle A 2, post 2, slot S 2, item description, quantity 5, say ready',
                              'pre 7, aisle A 7, post 7, slot S 7, item description, quantity 3, say ready',
                              'Pick base items?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 4 ,   pick message',
                              'Is this a short product?',
                              'You must speak the product ID. Try Again.',
                              'S 1 Pick 4 ,   pick message',
                              'You must speak the check digit.',
                              'S 1 Pick 4 ,   pick message',
                              'Put 4 in 1',
                              'weight 1 of 4',
                              'weight 2 of 4',
                              'weight 3 of 4',
                              'weight 4 of 4',
                              'Entries complete',
                              'pre 2',
                              'Aisle A 2',
                              'post 2',
                              'S 2 Pick 5 ,   pick message',
                              'Is this a short product?',
                              'You must speak the product ID. Try Again.',
                              'S 2 Pick 5 ,   pick message',
                              'Put 5 in 1',
                              'weight 1 of 5',
                              'you picked 0 of 5 items, is this a short product?',
                              'pre 3',
                              'Aisle A 3',
                              'post 3',
                              'S 3 Pick 3 ,   pick message',
                              'Put 3 in 2',
                              'weight 1 of 3',
                              'weight 2 of 3',
                              'weight 3 of 3',
                              'Entries complete',
                              'pre 4',
                              'Aisle A 4',
                              'post 4',
                              'S 4 Pick 3 ,   pick message',
                              'Identical Product verification and check digits. Is this a short product',
                              'pre 5',
                              'Aisle A 5',
                              'post 5',
                              'S 5 Pick 4 ,   pick message',
                              'You must speak the check digit.',
                              'S 5 Pick 4 ,   pick message',
                              'Identical Product verification and check digits. Is this a short product',
                              'Put 4 in 3',
                              'weight 1 of 4',
                              'weight 2 of 4',
                              'weight 3 of 4',
                              'weight 4 of 4',
                              'Entries complete',
                              'pre 6',
                              'Aisle A 6',
                              'post 6',
                              'S 6 Pick 5 ,   pick message',
                              'You must speak the check digit.',
                              'S 6 Pick 5 ,   pick message',
                              'Put 5 in 3',
                              'weight 1 of 5',
                              'you picked 0 of 5 items, is this a short product?',
                              'weight 1 of 5',
                              'you picked 0 of 5 items, is this a short product?',
                              'pre 7',
                              'Aisle A 7',
                              'post 7',
                              'S 7 Pick 3 ,   pick message',
                              'Put 3 in 3',
                              'weight 1 of 3',
                              'weight 2 of 3',
                              'weight 3 of 3',
                              'Entries complete',
                              'Picking complete',
                              'Deliver position 1 ID Store 123 to Location 11',
                              'Confirm Delivery',
                              'Deliver position 2 ID Store 345 to Location 21',
                              'Confirm Delivery',
                              'Deliver position 3 ID Store 567 to Location 31',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '7', '3'],
                                      ['prTaskLUTRequestWork', '123', '1', '1'],
                                      ['prTaskLUTRequestWork', '234', '1', '1'],
                                      ['prTaskLUTRequestWork', '345', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '1'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '12121', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '56', '0'],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '34343', '2', ''],
                                      ['prTaskLUTPrint', '1', '12346', '1', '', '56', '0'],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '56565', '2', ''],
                                      ['prTaskLUTPrint', '1', '12347', '1', '', '56', '0'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '3', '', '1.23', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '3', '', '2.12', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '3', '', '2.34', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '3', '', '2.56', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '0', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L4', '1', '0', '3', '5', '', '2.45', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L4', '1', '0', '3', '5', '', '2.21', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L4', '1', '1', '3', '5', '', '1.23', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L5', '0', '1', '', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L6', '1', '0', '5', '7', '', '2.34', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L6', '1', '0', '5', '7', '', '6.45', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L6', '1', '0', '5', '7', '', '3.45', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L6', '1', '1', '5', '7', '', '7.56', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L7', '0', '1', '5', '8', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L8', '1', '0', '5', '9', '', '2.21', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L8', '1', '0', '5', '9', '', '1.23', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L8', '1', '1', '5', '9', '', '1.34', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12346'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12347'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
