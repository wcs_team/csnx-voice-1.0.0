#Region 107 - Verify new container, variable weights and serial number capture.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion107_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion107_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('7,Region 107,1,0,3,1,1,1,1,0,0,1,1,0,0,1,0,3,0,1,2,XXXXX,XXX,XXXXXX,XXX,0,1,0,5,0,0,\n'
                                       '\n')
        self.set_server_response('123,0,\n'
                                       '234,0,\n'
                                       '345,0,\n'
                                       '456,0,\n'
                                       '\n')
        self.set_server_response('123,0,\n'
                                       '234,0,\n'
                                       '345,0,\n'
                                       '456,0,\n'
                                       '\n')
        self.set_server_response('123,0,\n'
                                       '234,0,\n'
                                       '345,0,\n'
                                       '456,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 345,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 567,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,1,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       'B,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,1,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,1,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,3,,ITEM13,1,1.0,10.0,00,00,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,1,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,1,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,1,1.0,10.0,00,00,,,Item Description,Size,UPC 13,12347,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       'B,0,9,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,1,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12347,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,56565,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,67676,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 123,,C,0,0,\n'
                                       '5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,56565,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,67676,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 21,02,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 31,03,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   '7!',             # Region?
                                   'yes',            # selection region 107, correct?
                                   '123!',           # Work ID?
                                   'yes',            # 123, correct?
                                   '234!',           # Work ID?
                                   'yes',            # 234, correct?
                                   '345!',           # Work ID?
                                   'yes',            # 345, correct?
                                   'no',             # Pick in reverse order?
                                   'ready',          # position 1, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',          # position 2, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',          # position 3, ID Store 567, has a goal time of 15 minutes, say ready
                                   '12121!',         # New Container ID? Position 1
                                   'yes',            # 12121, correct?
                                   '7!',             # Printer?
                                   'yes',            # printer 7, correct?
                                   '34343!',         # New Container ID? Position 2
                                   'yes',            # 34343, correct?
                                   'yes',            # printer 7, correct?
                                   '56565!',         # New Container ID? Position 3
                                   'yes',            # 56565, correct?
                                   'yes',            # printer 7, correct?
                                   'no',             # Base summary?
                                   'no',             # Pick base items?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 1 Pick 4    pick message
                                   '1234ready',      # lot text
                                   '4!',             # Quantity for this lot text
                                   'yes',            # 1234A
                                   '12121',          # Put 4 in 1
                                   '1.23',           # weight 1 of 4
                                   '34ready',        # serial number 1 of 4
                                   'review last',    # weight 2 of 4
                                   'ready',          # Unit 1,weight was 1.23,serial number was 34,say ready
                                   '10.23',          # weight 2 of 4
                                   'override',       # 10.23 out of range
                                   'yes',            # override, correct?
                                   'review last',    # serial number 2 of 4
                                   'ready',          # Unit 1,weight was 1.23,serial number was 34,say ready
                                   '35ready',        # serial number 2 of 4
                                   '2.45',           # weight 3 of 4
                                   '78ready',        # serial number 3 of 4
                                   '2.33',           # weight 4 of 4
                                   '9ready',         # serial number 4 of 4
                                   'undo entry',     # Entries complete
                                   'last entry',     # last entry, all entries, or continue?
                                   'yes',            # re-enter item 4, correct?
                                   '2.78',           # weight 4 of 4
                                   '67ready',        # serial number 4 of 4
                                   'review last',    # Entries complete
                                   'ready',          # Unit 4,weight was 2.78,serial number was 67,say ready
                                   'ready',          # Entries complete
                                   '00',             # S 1 Pick 8    pick message
                                   '1234ready',      # lot text
                                   '5!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   'new container',  # Put 5 in 1
                                   'yes',            # new container, correct?
                                   '23232!',         # New Container ID? Position 1
                                   'yes',            # 23232, correct?
                                   'yes',            # printer 7, correct?
                                   '00',             # S 1 Pick 8    pick message
                                   '1234ready',      # lot text
                                   '5!',             # Quantity for this lot text
                                   'no',             # 1234A
                                   'yes',            # 1234B
                                   '12121!',         # Put 5 in 1
                                   '3.45',           # weight 1 of 5
                                   '56ready',        # serial number 1 of 5
                                   'short product',  # weight 2 of 5
                                   '2.34',           # weight 2 of 5
                                   '45ready',        # serial number 2 of 5
                                   'undo entry',     # weight 3 of 5
                                   'all entries',    # last entry, all entries, or continue?
                                   'no',             # re-enter all entries, correct?
                                   'continue',       # last entry, all entries, or continue?
                                   '5.43',           # weight 3 of 5
                                   '56ready',        # serial number 3 of 5
                                   '7.89',           # weight 4 of 5
                                   'review last',    # serial number 4 of 5
                                   'ready',          # Unit 3,weight was 5.43,serial number was 56,say ready
                                   '45ready',        # serial number 4 of 5
                                   'review last',    # weight 5 of 5
                                   'ready',          # Unit 4,weight was 7.89,serial number was 45,say ready
                                   '6.32',           # weight 5 of 5
                                   '23ready',        # serial number 5 of 5
                                   'review last',    # Entries complete
                                   'ready',          # Unit 5,weight was 6.32,serial number was 23,say ready
                                   'ready',          # Entries complete
                                   '00',             # S 1 Pick 3    pick message
                                   'short product',  # lot text
                                   'yes',            # You picked 0 of 3 items.  Is this a short product?
                                   '00',             # S 1 Pick 12    pick message
                                   '1234ready',      # lot text
                                   '12!',            # Quantity for this lot text
                                   'yes',            # 1234A
                                   '12121!',         # Put 5 in 1
                                   '1.23',           # weight 1 of 5
                                   '23ready',        # serial number 1 of 5
                                   '0.05',           # weight 2 of 5
                                   'override',       # 0.05 out of range
                                   'yes',            # override, correct?
                                   '45ready',        # serial number 2 of 5
                                   '4.56',           # weight 3 of 5
                                   '66ready',        # serial number 3 of 5
                                   '7.78',           # weight 4 of 5
                                   '78ready',        # serial number 4 of 5
                                   'undo entry',     # weight 5 of 5
                                   'all entries',    # last entry, all entries, or continue?
                                   'yes',            # re-enter all entries, correct?
                                   '1.23',           # weight 1 of 5
                                   '34ready',        # serial number 1 of 5
                                   '3.56',           # weight 2 of 5
                                   '12ready',        # serial number 2 of 5
                                   '3.45',           # weight 3 of 5
                                   '23ready',        # serial number 3 of 5
                                   '3.45',           # weight 4 of 5
                                   '56ready',        # serial number 4 of 5
                                   '1.56',           # weight 5 of 5
                                   '67ready',        # serial number 5 of 5
                                   'ready',          # Entries complete
                                   '34343!',         # Put 3 in 2
                                   '1.23',           # weight 1 of 3
                                   '34ready',        # serial number 1 of 3
                                   '5.67',           # weight 2 of 3
                                   '34ready',        # serial number 2 of 3
                                   '8.90',           # weight 3 of 3
                                   '98ready',        # serial number 3 of 3
                                   'review last',    # Entries complete
                                   'ready',          # Unit 3,weight was 8.9,serial number was 98,say ready
                                   'ready',          # Entries complete
                                   '56565!',         # Put 4 in 3
                                   '1.11',           # weight 1 of 4
                                   '23ready',        # serial number 1 of 4
                                   '5.78',           # weight 2 of 4
                                   '66ready',        # serial number 2 of 4
                                   '4.67',           # weight 3 of 4
                                   '89ready',        # serial number 3 of 4
                                   '9.00',           # weight 4 of 4
                                   '56ready',        # serial number 4 of 4
                                   'ready',          # Entries complete
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 1 Pick 8    pick message
                                   '123ready',       # lot text
                                   '4!',             # Quantity for this lot text
                                   'yes',            # 1234A
                                   '34343!',         # Put 3 in 2
                                   '7.89',           # weight 1 of 3
                                   '90ready',        # serial number 1 of 3
                                   '7.34',           # weight 2 of 3
                                   'undo entry',     # serial number 2 of 3
                                   'last entry',     # last entry, all entries, or continue?
                                   'yes',            # re-enter item 1, correct?
                                   '2.12',           # weight 1 of 3
                                   '34ready',        # serial number 1 of 3
                                   '2.34',           # weight 2 of 3
                                   '67ready',        # serial number 2 of 3
                                   '4.66',           # weight 3 of 3
                                   '67ready',        # serial number 3 of 3
                                   'ready',          # Entries complete
                                   '56565',          # Put 1 in 3
                                   '3.21',           # weight 1 of 1
                                   '12ready',        # serial number 1 of 1
                                   'ready',          # Entries complete
                                   '00',             # S 1 Pick 4    pick message
                                   'short product',  # lot text
                                   'yes',            # You picked 0 of 4 items.  Is this a short product?
                                   'ready',          # Deliver position 1 ID Store 123 to Location 11
                                   '01',             # Confirm Delivery
                                   'ready',          # Deliver position 2 ID Store 345 to Location 21
                                   '02',             # Confirm Delivery
                                   'ready',          # Deliver position 3 ID Store 567 to Location 31
                                   '03',             # Confirm Delivery
                                   '-')              # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 107, correct?',
                              'Work ID?',
                              '123, correct?',
                              'Work ID?',
                              '234, correct?',
                              'Work ID?',
                              '345, correct?',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'position 1, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 567, has a goal time of 15 minutes, say ready',
                              'New Container ID? Position 1',
                              '12121, correct?',
                              'Printer?',
                              'printer <spell>7</spell>, correct?',
                              'New Container ID? Position 2',
                              '34343, correct?',
                              'printer <spell>7</spell>, correct?',
                              'New Container ID? Position 3',
                              '56565, correct?',
                              'printer <spell>7</spell>, correct?',
                              'Base summary?',
                              'Pick base items?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 4 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 4 in 1',
                              'weight 1 of 4',
                              'serial number 1 of 4',
                              'weight 2 of 4',
                              'Unit 1, weight was 1.23, serial number was <spell>34</spell>, say ready',
                              'weight 2 of 4',
                              '10.23 out of range',
                              'override, correct?',
                              'serial number 2 of 4',
                              'Unit 1, weight was 1.23, serial number was <spell>34</spell>, say ready',
                              'serial number 2 of 4',
                              'weight 3 of 4',
                              'serial number 3 of 4',
                              'weight 4 of 4',
                              'serial number 4 of 4',
                              'Entries complete',
                              'last entry, all entries, or continue?',
                              're enter item 4, correct?',
                              'weight 4 of 4',
                              'serial number 4 of 4',
                              'Entries complete',
                              'Unit 4, weight was 2.78, serial number was <spell>67</spell>, say ready',
                              'Entries complete',
                              'S 1 Pick 8 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 5 in 1',
                              'new container, correct?',
                              'New Container ID? Position 1',
                              '23232, correct?',
                              'printer <spell>7</spell>, correct?',
                              'S 1 Pick 8 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 5 in 1',
                              'weight 1 of 5',
                              'serial number 1 of 5',
                              'weight 2 of 5',
                              'short product not allowed',
                              'weight 2 of 5',
                              'serial number 2 of 5',
                              'weight 3 of 5',
                              'last entry, all entries, or continue?',
                              're enter all items, correct?',
                              'last entry, all entries, or continue?',
                              'weight 3 of 5',
                              'serial number 3 of 5',
                              'weight 4 of 5',
                              'serial number 4 of 5',
                              'Unit 3, weight was 5.43, serial number was <spell>56</spell>, say ready',
                              'serial number 4 of 5',
                              'weight 5 of 5',
                              'Unit 4, weight was 7.89, serial number was <spell>45</spell>, say ready',
                              'weight 5 of 5',
                              'serial number 5 of 5',
                              'Entries complete',
                              'Unit 5, weight was 6.32, serial number was <spell>23</spell>, say ready',
                              'Entries complete',
                              'S 1 Pick 3 ,   pick message',
                              'lot text',
                              'You picked 0 of 3 items.  Is this a short product?',
                              'S 1 Pick 12 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 5 in 1',
                              'weight 1 of 5',
                              'serial number 1 of 5',
                              'weight 2 of 5',
                              '0.05 out of range',
                              'override, correct?',
                              'serial number 2 of 5',
                              'weight 3 of 5',
                              'serial number 3 of 5',
                              'weight 4 of 5',
                              'serial number 4 of 5',
                              'weight 5 of 5',
                              'last entry, all entries, or continue?',
                              're enter all items, correct?',
                              'weight 1 of 5',
                              'serial number 1 of 5',
                              'weight 2 of 5',
                              'serial number 2 of 5',
                              'weight 3 of 5',
                              'serial number 3 of 5',
                              'weight 4 of 5',
                              'serial number 4 of 5',
                              'weight 5 of 5',
                              'serial number 5 of 5',
                              'Entries complete',
                              'Put 3 in 2',
                              'weight 1 of 3',
                              'serial number 1 of 3',
                              'weight 2 of 3',
                              'serial number 2 of 3',
                              'weight 3 of 3',
                              'serial number 3 of 3',
                              'Entries complete',
                              'Unit 3, weight was 8.9, serial number was <spell>98</spell>, say ready',
                              'Entries complete',
                              'Put 4 in 3',
                              'weight 1 of 4',
                              'serial number 1 of 4',
                              'weight 2 of 4',
                              'serial number 2 of 4',
                              'weight 3 of 4',
                              'serial number 3 of 4',
                              'weight 4 of 4',
                              'serial number 4 of 4',
                              'Entries complete',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1 Pick 8 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 3 in 2',
                              'weight 1 of 3',
                              'serial number 1 of 3',
                              'weight 2 of 3',
                              'serial number 2 of 3',
                              'last entry, all entries, or continue?',
                              're enter item 1, correct?',
                              'weight 1 of 3',
                              'serial number 1 of 3',
                              'weight 2 of 3',
                              'serial number 2 of 3',
                              'weight 3 of 3',
                              'serial number 3 of 3',
                              'Entries complete',
                              'Put 1 in 3',
                              'weight 1 of 1',
                              'serial number 1 of 1',
                              'Entries complete',
                              'S 1 Pick 4 ,   pick message',
                              'lot text',
                              'You picked 0 of 4 items.  Is this a short product?',
                              'Picking complete',
                              'Deliver position 1 ID Store 123 to Location 11',
                              'Confirm Delivery',
                              'Deliver position 2 ID Store 345 to Location 21',
                              'Confirm Delivery',
                              'Deliver position 3 ID Store 567 to Location 31',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '7', '3'],
                                      ['prTaskLUTRequestWork', '123', '1', '1'],
                                      ['prTaskLUTRequestWork', '234', '1', '1'],
                                      ['prTaskLUTRequestWork', '345', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '12121', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '7', '0'],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '34343', '2', ''],
                                      ['prTaskLUTPrint', '1', '12346', '1', '', '7', '0'],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '56565', '2', ''],
                                      ['prTaskLUTPrint', '1', '12347', '1', '', '7', '0'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTSendLot', '1234', '4', '12345', '3'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '3', '1234A', '1.23', '34'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '3', '1234A', '10.23', '35'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '3', '1234A', '2.45', '78'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '3', '1234A', '2.78', '67'],
                                      ['prTaskLUTSendLot', '1234', '5', '12345', '4'],
                                      ['prTaskLUTContainer', '1', '12345', '', '1', '', '1', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '23232', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '7', '0'],
                                      ['prTaskLUTSendLot', '1234', '5', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234B', '3.45', '56'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234B', '2.34', '45'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234B', '5.43', '56'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234B', '7.89', '45'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '4', '1234B', '6.32', '23'],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '0', '1', '', '9', '', '', ''],
                                      ['prTaskLUTSendLot', '1234', '12', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234A', '1.23', '34'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234A', '3.56', '12'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234A', '3.45', '23'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '4', '1234A', '3.45', '56'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '4', '1234A', '1.56', '67'],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '1', '0', '3', '6', '1234A', '1.23', '34'],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '1', '0', '3', '6', '1234A', '5.67', '34'],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '1', '1', '3', '6', '1234A', '8.90', '98'],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '1', '0', '5', '7', '1234A', '1.11', '23'],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '1', '0', '5', '7', '1234A', '5.78', '66'],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '1', '0', '5', '7', '1234A', '4.67', '89'],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '1', '1', '5', '7', '1234A', '9.00', '56'],
                                      ['prTaskLUTSendLot', '123', '4', '12346', '5'],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '1', '0', '3', '5', '1234A', '2.12', '34'],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '1', '0', '3', '5', '1234A', '2.34', '67'],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '1', '1', '3', '5', '1234A', '4.66', '67'],
                                      ['prTaskLUTPicked', '1', '12347', 'L2', '1', '0', '5', '8', '1234A', '3.21', '12'],
                                      ['prTaskLUTPicked', '1', '12347', 'L2', '0', '1', '', '8', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12346'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12347'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
