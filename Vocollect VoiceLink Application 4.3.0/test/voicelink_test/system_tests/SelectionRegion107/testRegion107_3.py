# Selection Region 107 - Single pick prompt suppress quantity of one with no delivery confirmation.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion107_3(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion107_3(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                       '2,pallet jack,1,0,\n'
                                       '\n')
        self.set_server_response('brakes,B,\n'
                                       'tires,B,\n'
                                       'mirrors,B,\n'
                                       'lights,B,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('7,Region 107,1,0,3,1,1,1,1,0,1,1,1,0,0,0,0,3,0,1,2,XXXXX,XXX,XXXXXX,XXX,0,1,0,2,0,0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 346,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 347,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM9,0,0.0,0.0,00,00,,,item description 9,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,item description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM8,0,0.0,0.0,00,00,,,item description 8,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,0.0,0.0,00,00,,,item description 10,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,item description 14,Size,UPC 14,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,11,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 345,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 345,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12346,Store 346,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 346,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 345,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 345,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,05,12347,Store 347,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 347,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 346,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 346,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 345,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 345,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 21,02,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 31,03,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '2',      # Vehicle Type?
                                   'yes',    # pallet jack, correct?
                                   '345!',   # Vehicle ID?
                                   'yes',    # 345, correct?
                                   'yes',    # brakes
                                   'yes',    # tires
                                   'yes',    # mirrors
                                   'yes',    # lights
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '7!',     # Region?
                                   'yes',    # selection region 107, correct?
                                   '234',    # Work ID?
                                   'yes',    # 234, correct?
                                   '235',    # Work ID?
                                   'yes',    # 235, correct?
                                   '236',    # Work ID?
                                   'yes',    # 236, correct?
                                   'no',     # Pick in reverse order?
                                   'ready',  # position 1, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',  # position 2, ID Store 346, has a goal time of 15 minutes, say ready
                                   'ready',  # position 3, ID Store 347, has a goal time of 15 minutes, say ready
                                   '01!',     # New Container ID? Position 1
                                   'yes',     # 01, correct?
                                   '1!',      # Printer?
                                   'yes',    # printer 1, correct?
                                   '03!',     # New Container ID? Position 2
                                   'yes',     # 03, correct?
                                   'yes',    # printer 1, correct?
                                   '05!',     # New Container ID? Position 3
                                   'yes',     # 05, correct?
                                   'yes',    # printer 1, correct?
                                   'ready',  # pre 1
                                   'ready',  # Aisle A 1
                                   'ready',  # post 1
                                   '00',     # S 1   item description 9 pick message
                                   '01',     # Put 1 in 1
                                   '00',     # S 1 Pick 8   item description 14 pick message
                                   '01',     # Put 5 in 1
                                   '05',     # Put 3 in 3
                                   '00',     # S 1   item description 8 pick message
                                   '01',     # Put 1 in 1
                                   'ready',  # pre 2
                                   'ready',  # Aisle A 1
                                   'ready',  # post 2
                                   '00',     # S 1 Pick 3   item description 13 pick message
                                   '03',     # Put 2 in 2
                                   '05',     # Put 1 in 3
                                   'ready',  # pre 1
                                   'ready',  # Aisle A 1
                                   'ready',  # post 1
                                   '00',     # S 1 Pick 7   item description 12 pick message
                                   '03',     # Put 3 in 2
                                   '05',     # Put 4 in 3
                                   '00',     # S 1 Pick 2   item description 10 pick message
                                   '01',     # Put 2 in 1
                                   'ready',  # Deliver position 1 ID Store 345 to Location 11
                                   'ready',  # Deliver position 2 ID Store 346 to Location 21
                                   'ready',  # Deliver position 3 ID Store 347 to Location 31
                                   '-')      # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'pallet jack, correct?',
                              'Vehicle ID?',
                              '345, correct?',
                              'Safety checklist. say yes, no or numeric value to verify each check',
                              'brakes',
                              'tires',
                              'mirrors',
                              'lights',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 107, correct?',
                              'Work ID?',
                              '234, correct?',
                              'Work ID?',
                              '235, correct?',
                              'Work ID?',
                              '236, correct?',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'position 1, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 346, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 347, has a goal time of 15 minutes, say ready',
                              'New Container ID? Position 1',
                              '01, correct?',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'New Container ID? Position 2',
                              '03, correct?',
                              'printer <spell>1</spell>, correct?',
                              'New Container ID? Position 3',
                              '05, correct?',
                              'printer <spell>1</spell>, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 , item description 9,  pick message',
                              'Put 1 in 1',
                              'S 1 Pick 8 , item description 14,  pick message',
                              'Put 5 in 1',
                              'Put 3 in 3',
                              'S 1 , item description 8,  pick message',
                              'Put 1 in 1',
                              'pre 2',
                              'Aisle A 1',
                              'post 2',
                              'S 1 Pick 3 , item description 13,  pick message',
                              'Put 2 in 2',
                              'Put 1 in 3',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 7 , item description 12,  pick message',
                              'Put 3 in 2',
                              'Put 4 in 3',
                              'S 1 Pick 2 , item description 10,  pick message',
                              'Put 2 in 1',
                              'Picking complete',
                              'Deliver position 1 ID Store 345 to Location 11',
                              'Deliver position 2 ID Store 346 to Location 21',
                              'Deliver position 3 ID Store 347 to Location 31',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '2', '345'],
                                      ['prTaskLUTCoreSafetyCheck', '-1', '', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '7', '3'],
                                      ['prTaskLUTRequestWork', '234', '1', '1'],
                                      ['prTaskLUTRequestWork', '235', '1', '1'],
                                      ['prTaskLUTRequestWork', '236', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '01', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '03', '2', ''],
                                      ['prTaskLUTPrint', '1', '12346', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '05', '2', ''],
                                      ['prTaskLUTPrint', '1', '12347', '1', '', '1', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '3', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '3', '1', '5', '10', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '1', '1', '3', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '1', '1', '3', '11', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L2', '1', '1', '5', '8', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '3', '1', '3', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '4', '1', '5', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '1', '9', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12346'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12347'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
