# Verify shorting and skipping works correctly in Region 101.
# Rename the file testGenerator_101_2.txt to testGenerator_101_2.py and copy the file to the
# root level of the VoiceLink project and run to regenerate the python unit test if necessary.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testRegion101_2(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()
        
    def testRegion101_2(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                       '2,selection region 2,0,\n'
                                       '3,selection region 3,0,\n'
                                       '\n')
        self.set_server_response('1,dry grocery,1,0,3,1,1,1,1,0,1,1,0,1,0,0,0,5,1,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response(',0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,10,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 123,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 123,3,5,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,4,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L2,1,pre 1,A 1,post 1,S 2,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,5,L3,1,pre 2,A 1,post 2,S 3,7,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L4,1,pre 1,A 1,post 1,S 4,11,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L5,1,pre 1,A 1,post 1,S 5,13,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,8,L6,1,pre 2,A 1,post 2,S 6,17,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L7,1,pre 1,A 1,post 1,S 7,19,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,3,L8,1,pre 1,A 1,post 1,S 8,2,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('G,0,7,L5,1,pre 1,A 1,post 1,S 5,13,,ITEM12,0,0.0,0.0,10,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   '1',              # Region?
                                   'yes',            # selection region 1, correct?
                                   '12345',          # Work ID?
                                   'yes',            # 12345, correct?
                                   '12346',          # Work ID?
                                   'yes',            # 12346, correct?
                                   '12347',          # Work ID?
                                   'yes',            # 12347, correct?
                                   'ready',          # position 1, ID Store 123, has a goal time of 10 minutes, say ready
                                   'ready',          # position 2, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',          # position 3, ID Store 123, has a goal time of 5 minutes, say ready
                                   '5!',             # Printer?
                                   'yes',            # printer 5, correct?
                                   'yes',            # Base summary?
                                   'ready',          # pre 2, aisle A 1, post 2, slot S 3, item description, ID Store 123, quantity 7, say ready
                                   'ready',          # pre 2, aisle A 1, post 2, slot S 6, item description, ID Store 123, quantity 17, say ready
                                   'ready',          # pre 1, aisle A 1, post 1, slot S 8, item description, ID Store 123, quantity 2, say ready
                                   'no',             # Pick base items?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 1 ,  ID Store 123, pick message
                                   'skip slot',      # S 2 Pick 5 ,  ID Store 123, pick message
                                   'yes',            # Skip slot, correct?
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   'skip slot',      # S 3 Pick 7 ,  ID Store 123, pick message
                                   'yes',            # Skip slot, correct?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 4 Pick 11 ,  ID Store 123, pick message
                                   'repick skips',   # S 5 Pick 13 ,  ID Store 123, pick message
                                   'yes',            # repick skips, correct?
                                   '00',             # S 2 Pick 5 ,  ID Store 123, pick message
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 3 Pick 7 ,  ID Store 123, pick message
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   'short product',  # S 5 Pick 13 ,  ID Store 123, pick message
                                   '00',             # check digit?
                                   '10',             # how many did you pick?
                                   'yes',            # 10, correct?
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 6 Pick 17 ,  ID Store 123, pick message
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   'skip slot',      # S 7 Pick 19 ,  ID Store 123, pick message
                                   'yes',            # Skip slot, correct?
                                   '00',             # S 8 Pick 2 ,  ID Store 123, pick message
                                   'partial',        # S 7 Pick 19 ,  ID Store 123, pick message
                                   'how much more',  # S 7 Pick 19 ,  ID Store 123, pick message
                                   '00',             # S 7 Pick 19 ,  ID Store 123, pick message
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 5 Pick 3 ,  ID Store 123, pick message
                                   'ready',          # Load position 1 ID Store 123 at Location 1
                                   'left',           # Position?
                                   'yes',            # left, correct?
                                   'ready',          # Load position 2 ID Store 123 at Location 1
                                   'right',          # Position?
                                   'yes',            # right, correct?
                                   'ready',          # Load position 3 ID Store 123 at Location 1
                                   '02',             # Position?
                                   'yes',            # 02, correct?
                                   '-')              # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'Work ID?',
                              '12345, correct?',
                              'Work ID?',
                              '12346, correct?',
                              'Work ID?',
                              '12347, correct?',
                              'position 1, ID Store 123, has a goal time of 10 minutes, say ready',
                              'position 2, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 123, has a goal time of 5 minutes, say ready',
                              'Printer?',
                              'printer <spell>5</spell>, correct?', 
                              'Base summary?', 
                              'pre 2, aisle A 1, post 2, slot S 3, item description, ID Store 123, quantity 7, say ready', 
                              'pre 2, aisle A 1, post 2, slot S 6, item description, ID Store 123, quantity 17, say ready', 
                              'pre 1, aisle A 1, post 1, slot S 8, item description, ID Store 123, quantity 2, say ready', 'Pick base items?', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 'S 1 ,  ID Store 123, pick message', 
                              'S 2 Pick 5 ,  ID Store 123, pick message', 
                              'Skip slot, correct?', 
                              'pre 2', 
                              'Aisle A 1', 
                              'post 2', 'S 3 Pick 7 ,  ID Store 123, pick message', 
                              'Skip slot, correct?', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 
                              'S 4 Pick 11 ,  ID Store 123, pick message', 
                              'S 5 Pick 13 ,  ID Store 123, pick message', 
                              'repick skips, correct?', 
                              'S 2 Pick 5 ,  ID Store 123, pick message', 
                              'pre 2', 
                              'Aisle A 1', 
                              'post 2', 
                              'S 3 Pick 7 ,  ID Store 123, pick message', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 
                              'S 5 Pick 13 ,  ID Store 123, pick message', 
                              'check digit?', 
                              'how many did you pick?', 
                              '10, correct?', 
                              'pre 2', 
                              'Aisle A 1', 
                              'post 2', 
                              'S 6 Pick 17 ,  ID Store 123, pick message', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 
                              'S 7 Pick 19 ,  ID Store 123, pick message', 
                              'Skip slot, correct?', 
                              'S 8 Pick 2 ,  ID Store 123, pick message', 
                              'S 7 Pick 19 ,  ID Store 123, pick message', 
                              'Partial not allowed when not picking to containers', 
                              'check digit?', 
                              '19 remaining at 1 line item', 
                              'S 7 Pick 19 ,  ID Store 123, pick message', 
                              'Picking complete', 
                              'shorts are reported', 
                              'Going back for shorts', 
                              'Picking shorts', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 'S 5 Pick 3 ,  ID Store 123, pick message', 
                              'Picking complete', 
                              'Load position 1 ID Store 123 at 1011', 
                              'Position?', 
                              'left, correct?', 
                              'Load position 2 ID Store 123 at 1011', 
                              'Position?', 
                              'right, correct?', 
                              'Load position 3 ID Store 123 at 1011', 
                              'Position?', 
                              '02, correct?', 
                              'Assignment complete.  For next assignment, say ready')


        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTRequestWork', '12345', '1', '1'],
                                      ['prTaskLUTRequestWork', '12346', '1', '1'],
                                      ['prTaskLUTRequestWork', '12347', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '1', '0'],
                                      ['prTaskLUTPrint', '1', '', '1', '', '5', '0'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '', '4', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L2', '0', 'S'],
                                      ['prTaskLUTUpdateStatus', '1', 'L3', '0', 'S'],
                                      ['prTaskLUTPicked', '1', '12346', 'L4', '11', '1', '', '6', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '5', '1', '', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L3', '7', '1', '', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L5', '10', '1', '', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L6', '17', '1', '', '8', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L7', '0', 'S'],
                                      ['prTaskLUTPicked', '1', '12345', 'L8', '2', '1', '', '3', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12347', 'L7', '19', '1', '', '9', '', '', ''],
                                      ['prTaskLUTGetPicks', '1', '0', '1', '0'],
                                      ['prTaskLUTPicked', '1', '12347', 'L5', '3', '1', '', '7', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12346'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12347'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', '02', ''],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
