# Selection Region 101 - Test taking a break.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion101_3(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion101_3(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '6,selection region 106,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('1,Region 101,1,0,4,1,1,0,1,0,0,1,1,1,0,0,0,4,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 346,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 347,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,0,0.0,0.0,00,00,,,item description 10,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,item description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM10,0,0.0,0.0,00,00,,,item description 10,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,4,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,0.0,0.0,00,00,,,item description 12,Size,UPC 12,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,0.0,0.0,00,00,,,item description 10,Size,UPC 12,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,item description 14,Size,UPC 14,12347,Store 347,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       'N,0,11,L2,1,pre 2,A 1,post 2,S 1,2,,ITEM13,0,0.0,0.0,00,00,,,item description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 21,02,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('Location 31,03,0,1,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct
                                   '1!',             # Region?
                                   'yes',            # selection region 1, correct?
                                   '2345',           # Work ID?
                                   'yes',            # 2345, correct?',
                                   '2346',           # Work ID?
                                   'yes',            # 2346, correct?',
                                   '2347',           # Work ID?
                                   'yes',            # 2347, correct?',
                                   'no more',        # Work ID?
                                   'ready',          # position 1, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',          # position 2, ID Store 346, has a goal time of 15 minutes, say ready
                                   'ready',          # position 3, ID Store 347, has a goal time of 15 minutes, say ready
                                   'ready',          # Open 01 Position 1
                                   '1!',             # Printer?
                                   'yes',            # printer 1, correct?
                                   'ready',          # Open 03 Position 2
                                   'yes',            # printer 1, correct?
                                   'ready',          # Open 05 Position 3
                                   'yes',            # printer 1, correct?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   'take a break',   # S 1 Pick 11   item description 10 pick message
                                   'yes',            # Take a break, correct?
                                   '3',              # Break type?
                                   'yes',            #coffee, correct?
                                   'ready',          # To continue work, say ready
                                   '123',           # Password?
                                   '00',            # S 1 Pick 11   item description 10 pick message
                                   '01',            # Put 11 in 1
                                   '00',            # S 1 Pick 8   item description 14 pick message
                                   '01',            # Put 5 in 1
                                   '05',            # Put 3 in 3
                                   'ready',         # pre 2
                                   'ready',         # Aisle A 1
                                   'ready',         # post 2
                                   '00',            # S 1 Pick 11   item description 13 pick message
                                   '03',            # Put 6 in 2
                                   '05',            # Put 5 in 3
                                   'ready',         # pre 1
                                   'ready',         # Aisle A 1
                                   'ready',         # post 1
                                   '00',            # S 1 Pick 7   item description 12 pick message
                                   '03',            # Put 3 in 2
                                   '05',            # Put 4 in 3
                                   'ready',         # Deliver position 1 ID Store 345 to Location 11
                                   'ready',         # Deliver position 2 ID Store 346 to Location 21
                                   'ready',         # Deliver position 3 ID Store 347 to Location 31
                                   '-')             # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 101, correct?',
                              'Work ID?',
                              '2345, correct?',
                              'Work ID?',
                              '2346, correct?',
                              'Work ID?',
                              '2347, correct?',
                              'Work ID?',
                              'position 1, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 346, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 347, has a goal time of 15 minutes, say ready',
                              'Open 01 Position 1',
                              'Printer?',
                              'printer <spell>1</spell>, correct?', 
                              'Open 03 Position 2', 
                              'printer <spell>1</spell>, correct?', 
                              'Open 05 Position 3', 
                              'printer <spell>1</spell>, correct?', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 
                              'S 1 Pick 11 , item description 10,  pick message', 
                              'take a break, correct?', 
                              'Break type?', 
                              'coffee, correct?', 
                              'To continue work, say ready', 
                              'Password?', 
                              'S 1 Pick 11 , item description 10,  pick message', 
                              'Put 11 in 1', 
                              'S 1 Pick 8 , item description 14,  pick message', 
                              'Put 5 in 1', 
                              'Put 3 in 3', 
                              'pre 2', 
                              'Aisle A 1', 
                              'post 2', 'S 1 Pick 11 , item description 13,  pick message', 
                              'Put 6 in 2', 
                              'Put 5 in 3', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 
                              'S 1 Pick 7 , item description 12,  pick message', 
                              'Put 3 in 2', 
                              'Put 4 in 3', 
                              'Picking complete', 
                              'Deliver position 1 ID Store 345 to Location 11', 
                              'Deliver position 2 ID Store 346 to Location 21', 
                              'Deliver position 3 ID Store 347 to Location 31', 
                              'Assignment complete.  For next assignment, say ready')


        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTRequestWork', '2345', '1', '1'],
                                      ['prTaskLUTRequestWork', '2346', '1', '1'],
                                      ['prTaskLUTRequestWork', '2347', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12346', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12347', '1', '', '1', '0'],
                                      ['prTaskODRCoreSendBreakInfo', '3', '0', 'coffee'],
                                      ['prTaskODRCoreSendBreakInfo', '3', '1', 'coffee'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '4', '1', '1', '3', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '1', '9', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '3', '1', '5', '10', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '4', '1', '3', '5', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '2', '1', '3', '11', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L2', '5', '1', '5', '8', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '3', '1', '3', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '4', '1', '5', '7', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12346'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12347'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
