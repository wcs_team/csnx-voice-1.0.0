from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main

import unittest

class testRegion101_1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testRegion101_1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')

        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                       '2,selection region 2,0,\n'
                                       '3,selection region 3,0,\n'
                                       '\n')
        self.set_server_response('2,dry grocery,1,0,3,1,1,1,1,0,1,1,1,1,0,0,0,5,0,0,2,,,,,0,0,0,2,0,\n'
                                       '\n')
        self.set_server_response('1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('1234A,0,\n'
                                       '1234B,0,\n'
                                       '1234C,0,\n'
                                       '1234D,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,10,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 123,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 123,3,5,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,4,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L2,1,pre 1,A 1,post 1,S 2,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,5,L3,1,pre 2,A 1,post 2,S 3,7,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L4,1,pre 1,A 1,post 1,S 4,11,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L5,1,pre 1,A 1,post 1,S 5,13,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,8,L6,1,pre 2,A 1,post 2,S 6,17,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L7,1,pre 1,A 1,post 1,S 7,19,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'B,0,3,L8,1,pre 1,A 1,post 1,S 8,2,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 123,0,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,0,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12346,Store 123,0,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,0,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,0,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,0,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,05,12347,Store 123,0,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,0,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,0,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,0,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,0,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,0,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                       '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct           
                                   '2!',             # Region?
                                   'yes',            # 'selection region 2, correct?'
                                   '12345'           # Work ID?
                                   'yes',            # confirming work id
                                   '12346',          # Work ID?
                                   'yes',            # confirming work id
                                   '12347',          # Work ID?
                                   'yes',            # confirming work id
                                   'ready',          # position 1, ID Store 123, has a goal time of 10 minutes, say ready
                                   'ready',          # position 2, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',          # position 3, ID Store 123, has a goal time of 5 minutes, say ready
                                   'ready',          # Open 01 Position 1
                                   '5!',             # Printer?
                                   'yes',            # printer 5, correct?
                                   'ready',          # Open 03 Position 2
                                   'yes',            # printer 5, correct?
                                   'ready',          # Open 05 Position 3
                                   'yes',            # printer 5, correct?
                                   'yes',            # Base summary?
                                   'ready',          # pre 2, aisle A 1, post 2, slot S 3, Item Description, ID Store 123, quantity 7, say ready
                                   'ready',          # pre 2, aisle A 1, post 2, slot S 6, Item Description, ID Store 123, quantity 17, say ready
                                   'ready',          # pre 1, aisle A 1, post 1, slot S 8, Item Description, ID Store 123, quantity 2, say ready
                                   'yes',            # Pick base items?
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 3 Pick 7    pick message
                                   '03!',            # Put 7 in 2
                                   'skip slot',      # S 6 Pick 17    pick message
                                   'yes',            # Skip slot, correct?
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   '00',             # S 8 Pick 2    pick message
                                   '01!',            # Put 2 in 1
                                   '00',             # S 1 Pick 3    pick message
                                   '03!',            # Put 3 in 1
                                   '01!',            # Put 3 in 1
                                   '00',             # S 2 Pick 5    pick message
                                   '03!',            # Put 5 in 2
                                   'skip slot',      # S 4 Pick 11    pick message
                                   'yes',            # Skip slot, correct?
                                   '00',             # S 5 Pick 13    pick message
                                   '03!',            # Put 13 in 3
                                   '05!',            # Put 13 in 3
                                   'ready',          # pre 2
                                   'ready',          # Aisle A 1
                                   'ready',          # post 2
                                   '00',             # S 6 Pick 17    pick message
                                   '05',             # Put 17 in 3
                                   'ready',          # pre 1
                                   'ready',          # Aisle A 1
                                   'ready',          # post 1
                                   'how much more',  # S 7 Pick 19    pick message
                                   'skip slot',      # S 7 Pick 19    pick message
                                   'yes',            # Skip slot, correct?
                                   '00',             # S 4 Pick 11    pick message
                                   '03',             # Put 11 in 2
                                   'short product',  # S 7 Pick 19    pick message
                                   '00',             # check digit?
                                   '10',             # how many did you pick?
                                   'yes',            # 10, correct?
                                   '05',             # Put 10 in 3
                                   'ready',          # Load position 1 ID Store 123 at Location 1
                                   'left',           # Position?
                                   'yes',            # left, correct?
                                   'ready',          # Load position 2 ID Store 123 at Location 1
                                   'left',           # Position?
                                   'yes',            # left, correct? 
                                   'ready',          # Load position 3 ID Store 123 at Location 1
                                   'left',           # Position?
                                   'yes',            # left, correct? 
                                   '-')              # Assignment complete.  For next assignment, say ready
                                   
        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'Work ID?',
                              '12345, correct?',
                              'Work ID?',
                              '12346, correct?',
                              'Work ID?',
                              '12347, correct?',
                              'position 1, ID Store 123, has a goal time of 10 minutes, say ready',
                              'position 2, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 123, has a goal time of 5 minutes, say ready',
                              'Open 01 Position 1',
                              'Printer?',
                              'printer <spell>5</spell>, correct?',
                              'Open 03 Position 2',
                              'printer <spell>5</spell>, correct?',
                              'Open 05 Position 3',
                              'printer <spell>5</spell>, correct?',
                              'Base summary?',
                              'pre 2, aisle A 1, post 2, slot S 3, item description, quantity 7, say ready',
                              'pre 2, aisle A 1, post 2, slot S 6, item description, quantity 17, say ready',
                              'pre 1, aisle A 1, post 1, slot S 8, item description, quantity 2, say ready',
                              'Pick base items?',
                              'pre 2',
                              'Aisle A 1', 
                              'post 2', 'S 3 Pick 7 ,   pick message', 
                              'Put 7 in 2', 
                              'S 6 Pick 17 ,   pick message', 
                              'Skip slot, correct?', 'pre 1', 
                              'Aisle A 1', 
                              'post 1', 'S 8 Pick 2 ,   pick message', 
                              'Put 2 in 1', 
                              'S 1 Pick 3 ,   pick message', 
                              'Put 3 in 1', 'Wrong 03. Try again.', 
                              'Put 3 in 1', 'S 2 Pick 5 ,   pick message', 
                              'Put 5 in 2', 'S 4 Pick 11 ,   pick message', 
                              'Skip slot, correct?', 
                              'S 5 Pick 13 ,   pick message', 
                              'Put 13 in 3', 
                              'Wrong 03. Try again.', 
                              'Put 13 in 3', 
                              'pre 2', 
                              'Aisle A 1', 
                              'post 2', 
                              'S 6 Pick 17 ,   pick message', 
                              'Put 17 in 3', 
                              'pre 1', 
                              'Aisle A 1', 
                              'post 1', 
                              'S 7 Pick 19 ,   pick message', '30 remaining at 2 line items', 
                              'S 7 Pick 19 ,   pick message', 
                              'Skip slot, correct?', 
                              'S 4 Pick 11 ,   pick message', 
                              'Put 11 in 2', 
                              'S 7 Pick 19 ,   pick message', 
                              'check digit?', 
                              'how many did you pick?', 
                              '10, correct?', 
                              'Put 10 in 3', 
                              'Picking complete', 
                              'Load position 1 ID Store 123 at 1011', 
                              'Position?', 
                              'left, correct?', 
                              'Load position 2 ID Store 123 at 1011', 
                              'Position?', 
                              'left, correct?', 
                              'Load position 3 ID Store 123 at 1011', 
                              'Position?', 
                              'left, correct?', 
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTRequestWork', '12345', '1', '1'],
                                      ['prTaskLUTRequestWork', '12346', '1', '1'],
                                      ['prTaskLUTRequestWork', '12347', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '5', '0'],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12346', '1', '', '5', '0'],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12347', '1', '', '5', '0'],
                                      ['prTaskLUTPicked', '1', '12346', 'L3', '7', '1', '3', '5', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L6', '0', 'N'],
                                      ['prTaskLUTPicked', '1', '12345', 'L8', '2', '1', '1', '3', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '1', '1', '4', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12346', 'L2', '5', '1', '3', '4', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L4', '0', 'S'],
                                      ['prTaskLUTPicked', '1', '12347', 'L5', '13', '1', '5', '7', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L6', '17', '1', '5', '8', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L7', '0', 'S'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12346', 'L4', '11', '1', '3', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L7', '10', '1', '5', '9', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12346'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12347'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
