# Verify operator is notified if an error or warning is returned from the host in response to the
# Line Loading Region Configuration LUT.
# Acceptance test case Line Loading | Begin Line Loading/Specify Region | ID 101822
# Generated using LineLoading_6.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testLineLoading_101822(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testLineLoading_101822(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,line loading region 1,0,\n'
                                       '2,line loading region 2,0,\n'
                                       '3,line loading region 3,0,\n'
                                       '4,line loading region 4,0,\n'
                                       '5,line loading region 5,0,\n'
                                       '6,line loading region 6,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,199,Error 199\n'
                                       '\n')
        self.set_server_response('1,line loading region 1,0,\n'
                                       '2,line loading region 2,0,\n'
                                       '3,line loading region 3,0,\n'
                                       '4,line loading region 4,0,\n'
                                       '5,line loading region 5,0,\n'
                                       '6,line loading region 6,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,line loading region 1,1,1,99,Warning 99\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '7!',     # Function?
                                   'yes',    # line loading, correct?
                                   '1',      # Region?
                                   'yes',    # line loading region 1, correct?
                                   'ready',  # Error 199, To continue say ready
                                   '1',      # Region?
                                   'yes',    # line loading region 1, correct?
                                   'ready',  # Warning 99, say ready
                                   '-')      # spur?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'line loading, correct?',
                              'Region?',
                              'line loading region 1, correct?',
                              'Error 199, To continue say ready',
                              'Region?',
                              'line loading region 1, correct?',
                              'Warning 99, say ready',
                              'spur?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLineLoadingValidRegions'],
                                      ['prTaskLUTLineLoadingRequestRegion', '1'],
                                      ['prTaskLUTLineLoadingRegionConfiguration'],
                                      ['prTaskLUTLineLoadingValidRegions'],
                                      ['prTaskLUTLineLoadingRequestRegion', '1'],
                                      ['prTaskLUTLineLoadingRegionConfiguration'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
