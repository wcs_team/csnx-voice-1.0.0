# Verify correct flow when multiple valid line loading regions are returned by the host
# Acceptance test case Line Loading | Begin Line Loading/Specify Region | ID 101801
# Generated using LineLoading_3.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testLineLoading_101801(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testLineLoading_101801(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,line loading region 1,0,\n'
                                       '2,line loading region 2,0,\n'
                                       '3,line loading region 3,0,\n'
                                       '4,line loading region 4,0,\n'
                                       '5,line loading region 5,0,\n'
                                       '6,line loading region 6,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,line loading region 1,1,1,0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '7!',           # Function?
                                   'yes',          # line loading, correct?
                                   '9',            # Region?
                                   'description',  # Region?
                                   'ready',        # 1,line loading region 1
                                   'ready',        # 2,line loading region 2
                                   'ready',        # 3,line loading region 3
                                   'ready',        # 4,line loading region 4
                                   'ready',        # 5,line loading region 5
                                   'ready',        # 6,line loading region 6
                                   'description',  # Region?
                                   'ready',        # 1,line loading region 1
                                   'stop',         # 2,line loading region 2
                                   '2',            # Region?
                                   'yes',          # line loading region 2, correct?
                                   '-')            # spur?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'line loading, correct?',
                              'Region?',
                              'Not authorized for <spell>9</spell>, Try again.',
                              'Region?',
                              '1, line loading region 1',
                              '2, line loading region 2',
                              '3, line loading region 3',
                              '4, line loading region 4',
                              '5, line loading region 5',
                              '6, line loading region 6',
                              'Region?',
                              '1, line loading region 1',
                              '2, line loading region 2',
                              'Region?',
                              'line loading region 2, correct?',
                              'spur?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLineLoadingValidRegions'],
                                      ['prTaskLUTLineLoadingRequestRegion', '2'],
                                      ['prTaskLUTLineLoadingRegionConfiguration'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
