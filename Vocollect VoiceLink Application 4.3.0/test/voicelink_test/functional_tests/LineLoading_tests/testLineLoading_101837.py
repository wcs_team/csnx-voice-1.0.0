##ID 101837 :: Test Case Spur can be spoken by the operator at carton prompt.
##ID 101839 :: Test Case Change Spur can be spoken by the operator at carton prompt.

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testLL(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testLL(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,line loading region 1,0,\n'
                                       '2,line loading region 2,0,\n'
                                       '3,line loading region 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,line loading region 1,1,1,0,\n'
                                       '\n')
        self.set_server_response('S10,0,\n'
                                       '\n')
        self.set_server_response('S10,0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '7!',           # Function?
                                   'yes',          # line loading, correct?
                                   '1',            # Region?
                                   'yes',          # line loading region 1, correct?
                                   '222',          # spur?
                                   'ready',        # spur?
                                   'yes',          # 222, correct?
                                   'spur',         # carton?
                                   'change spur',  # carton?
                                   '456',          # spur?
                                   'ready',        # spur?
                                   'no',           # 456, correct?
                                   '457',          # spur?
                                   'ready',        # spur?
                                   'yes',          # 457, correct?
                                   '-')            # carton?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'line loading, correct?',
                              'Region?',
                              'line loading region 1, correct?',
                              'spur?',
                              '222, correct?',
                              'carton?',
                              'S 1 0',
                              'carton?',
                              'spur?',
                              '456, correct?',
                              'spur?',
                              '457, correct?',
                              'carton?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLineLoadingValidRegions'],
                                      ['prTaskLUTLineLoadingRequestRegion', '1'],
                                      ['prTaskLUTLineLoadingRegionConfiguration'],
                                      ['prTaskLUTLineLoadingConfirmSpur', '222'],
                                      ['prTaskLUTLineLoadingConfirmSpur', '457'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
