# Verify partial command cannot be used if picking to target containers.
# Regression test case Selection | Target Containers | ID 121022
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_121022(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_121022(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,2,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '3,coffee,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '5,selection region 105,0,\n'
                                 '6,selection region 106,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '9,selection region 109,0,\n'
                                 '10,selection region 110,0,\n'
                                 '11,selection region 111,0,\n'
                                 '12,selection region 112,0,\n'
                                 '13,selection region 113,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('3,Region 103,1,1,1,1,1,0,1,0,2,1,1,1,0,0,1,-1,0,0,2,XXXXX,XXX,XXXXXX,XXX,1,0,1,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,1,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,1,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,1,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,5,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,9,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,1,A,0,0,\n'
                                 '2,0000000004,04,12345,Store 123,2,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,1,O,1,0,\n'
                                 '2,0000000004,04,12345,Store 123,2,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '3,coffee,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '5,selection region 105,0,\n'
                                 '6,selection region 106,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '9,selection region 109,0,\n'
                                 '10,selection region 110,0,\n'
                                 '11,selection region 111,0,\n'
                                 '12,selection region 112,0,\n'
                                 '13,selection region 113,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('9,Region 109,1,1,1,1,1,0,1,0,0,1,1,0,0,2,0,-1,0,0,2,X                                                 ,X                                                 ,X                                                 ,X                                                 ,1,0,1,2,0,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,1,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,1,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,1,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,5,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,9,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,2,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,1,A,0,0,\n'
                                 '2,0000000004,04,12345,Store 123,2,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,1,O,1,0,\n'
                                 '2,0000000004,04,12345,Store 123,2,A,1,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   'yes',              # 123, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '3!',               # Region?
                                   'yes',              # selection region 103, correct?
                                   'ready',            # To receive work, say ready
                                   'ready',            # ID Store 123, has a goal time of 15 minutes, say ready
                                   'yes',              # Preprint all labels for ID Store 123
                                   '1!',               # Printer?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Open 03
                                   'ready',            # pre 1
                                   'ready',            # Aisle A 1
                                   'ready',            # post 1
                                   '00',               # S 1
                                   'partial',          # Pick 7 ,   pick message
                                   'new container',    # Pick 7 ,   pick message
                                   'close container',  # Pick 7 ,   pick message
                                   'sign off',         # Pick 7 ,   pick message
                                   'yes',              # Sign off, correct?
                                   'ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   'yes',              # 123, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '9!',               # Region?
                                   'yes',              # selection region 109, correct?
                                   'ready',            # To receive work, say ready
                                   'ready',            # ID Store 123, has a goal time of 15 minutes, say ready
                                   'yes',              # Preprint all labels for ID Store 123
                                   '1!',               # Printer?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Open 03
                                   'ready',            # pre 1
                                   'ready',            # Aisle A 1
                                   'ready',            # post 1
                                   'partial',          # S 1 Pick 7 ,   pick message
                                   'new container',    # 
                                   'ready',            # 
                                   'close container',  # S 1 Pick 7 ,   pick message
                                   'partial',          # S 1 Pick 7 ,   pick message
                                   'ready',            # 
                                   'new container',    # S 1 Pick 7 ,   pick message
                                   '-')                # S 1 Pick 7 ,   pick message

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              '123, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 103, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Preprint all labels for ID Store 123',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Open 03',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 7 ,   pick message',
                              'partial not allowed when picks have a target container',
                              'Pick 7 ,   pick message',
                              'New container not allowed when picks have a target container',
                              'Pick 7 ,   pick message',
                              'close container not allowed when picks have a target container',
                              'Pick 7 ,   pick message',
                              'Sign off, correct?',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              '123, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 109, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Preprint all labels for ID Store 123',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Open 03',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 7 ,   pick message',
                              'partial not allowed when picks have a target container',
                              'check digit?',
                              'New container not allowed when picks have a target container',
                              'You must speak the check digit.',
                              'S 1 Pick 7 ,   pick message',
                              'close container not allowed when picks have a target container',
                              'S 1 Pick 7 ,   pick message',
                              'partial not allowed when picks have a target container',
                              'check digit?',
                              'You must speak the check digit.',
                              'S 1 Pick 7 ,   pick message',
                              'New container not allowed when picks have a target container',
                              'S 1 Pick 7 ,   pick message')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '3', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '12345', '1', '', '', '2', ''],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '9', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '3', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '12345', '1', '', '', '2', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
