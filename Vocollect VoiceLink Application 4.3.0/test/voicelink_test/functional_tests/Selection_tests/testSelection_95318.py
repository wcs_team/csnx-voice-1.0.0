##TestLink ID 95318 :: Test Case Verify if correct message is heard for error code 4.

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_95318(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_95318(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '3,coffee,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '6,selection region 106,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '9,selection region 109,0,\n'
                                 '13,selection region 113,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('1,Region 101,1,0,4,1,1,0,1,0,0,1,1,1,0,0,0,4,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,0,\n'
                                 '\n')
        self.set_server_response('1234,4,\n'
                                 '2345,4,\n'
                                 '3456,4,\n'
                                 '\n')
        self.set_server_response('1234,4,\n'
                                 '2345,4,\n'
                                 '3456,4,\n'
                                 '\n')
        self.set_server_response('1234,4,\n'
                                 '2345,4,\n'
                                 '3456,4,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',   # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',    # Password?
                                   '3!',      # Function?
                                   'yes',     # normal assignments, correct?
                                   '1!',      # Region?
                                   'yes',     # selection region 101, correct?
                                   '1234',    # Work ID?
                                   'yes',     # 1234, correct?
                                   'yes',     # found 3 work IDs, do you want to review the list?
                                   'no',      # <spell>1234</spell>, select?
                                   'cancel',  # <spell>2345</spell>, select?
                                   'yes',     # Cancel this work ID request?
                                   '1234',    # Work ID?
                                   'yes',     # 1234, correct?
                                   'yes',     # found 3 work IDs, do you want to review the list?
                                   'yes',     # <spell>1234</spell>, select?
                                   'yes',     # found 3 work IDs, do you want to review the list?
                                   '-')       # <spell>1234</spell>, select?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 101, correct?',
                              'Work ID?',
                              '1234, correct?',
                              'found 3 work IDs, do you want to review the list?',
                              '<spell>1234</spell>, select?',
                              '<spell>2345</spell>, select?',
                              'Cancel this work ID request?',
                              'Work ID?',
                              '1234, correct?',
                              'found 3 work IDs, do you want to review the list?',
                              '<spell>1234</spell>, select?',
                              'found 3 work IDs, do you want to review the list?',
                              '<spell>1234</spell>, select?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTRequestWork', '1234', '0', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
