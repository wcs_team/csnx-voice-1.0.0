# Verify selector re-prompted with put prompt if partial quantity is 0.
# Regression test case Selection | Partial | ID 97737
# Generated using Selection_2.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97737(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97737(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                       '2,selection region 2,0,\n'
                                       '3,selection region 3,0,\n'
                                       '4,selection region 4,0,\n'
                                       '8,selection region 8,0,\n'
                                       '9,selection region 9,0,\n'
                                       '15,selection region 15,0,\n'
                                       '\n')
        self.set_server_response('15,Region 115,1,1,4,1,1,0,0,0,0,1,1,0,0,1,0,-1,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,1,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12346,Store 123,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '1,0,12347,Store 123,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC 13,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12347,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('5,0000000005,05,12347,Store 123,,O,0,0,\n'
                                       '6,0000000006,06,12347,Store 123,,A,0,0,\n'
                                       '3,0000000003,03,12346,Store 123,,O,0,0,\n'
                                       '4,0000000004,04,12346,Store 123,,A,0,0,\n'
                                       '1,0000000001,01,12345,Store 123,,O,0,0,\n'
                                       '2,0000000002,02,12345,Store 123,,A,0,0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '3!',       # Function?
                                   'yes',      # normal assignments, correct?
                                   '15',       # Region?
                                   'yes',      # selection region 15, correct?
                                   '3',        # How many assignments?
                                   'ready',    # To receive work, say ready
                                   'ready',    # position 1, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',    # position 2, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',    # position 3, ID Store 123, has a goal time of 15 minutes, say ready
                                   '01!',      # New Container ID? Position 1
                                   'yes',      # 01, correct?
                                   '03!',      # New Container ID? Position 2
                                   'yes',      # 03, correct?
                                   '05!',      # New Container ID? Position 3
                                   'yes',      # 05, correct?
                                   'ready',    # pre 1
                                   'ready',    # Aisle A 1
                                   'ready',    # post 1
                                   '00',       # S 1 Pick 4    pick message
                                   'partial',  # Put 4 in 1
                                   'yes',      # partial, correct?
                                   '0',        # Quantity?
                                   '-')        # Put 4 in 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 15, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'position 1, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 123, has a goal time of 15 minutes, say ready',
                              'New Container ID? Position 1',
                              '01, correct?',
                              'New Container ID? Position 2',
                              '03, correct?',
                              'New Container ID? Position 3',
                              '05, correct?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 4 ,   pick message',
                              'Put 4 in 1',
                              'partial, correct?',
                              'Quantity?',
                              'Put 4 in 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '15', '3'],
                                      ['prTaskLUTGetAssignment', '3', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '01', '2', ''],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '03', '2', ''],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '05', '2', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
