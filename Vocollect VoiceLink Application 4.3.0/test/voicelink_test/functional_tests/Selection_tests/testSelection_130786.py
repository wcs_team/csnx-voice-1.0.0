# Test that when operator picks function = 6 (normal and chase), two regions are returned by host, 
# one for normal profile and one for chase profile and the chase profile is returned before the normal profile
# Regression test case Selection | Normal and Chase Assignments | ID 130786
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_130786(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_130786(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('101,Region 101,0,\r\n'
                                 '107,Region 107,0,\r\n'
                                 '115,Region 115,0,\r\n'
                                 '111,Region 111,0,\r\n'
                                 '112,Region 112,0,\r\n'
                                 '113,Region 113,0,\r\n'
                                 '102,Region 102,0,\r\n'
                                 '103,Region 103,0,\r\n'
                                 '104,Region 104,0,\r\n'
                                 '105,Region 105,0,\r\n'
                                 '106,Region 106,0,\r\n'
                                 '110,Region 110,0,\r\n'
                                 '109,Region 109,0,\r\n'
                                 '108,Region 108,0,\r\n'
                                 '114,Region 114,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('102,Region 102,2,1,1,1,1,0,0,1,0,1,0,0,0,2,0,-1,0,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,0,2,0,0,\r\n'
                                 '102,Region 102,1,1,1,1,1,0,2,0,2,1,0,0,0,0,1,-1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,0,2,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('17,1,34945,800000007,1,0.00,9,       00,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,0,34946,2053,102,Building 751 Site 1                               ,7 5 1                                             ,Bay 751                                           ,7 5 1                                             ,6,,751,0,0.0,0.0,        0,751,,,Item 751 Site 1,751,751,34945,800000007,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,34947,2258,102,Building 956 Site 1                               ,9 5 6                                             ,Bay 956                                           ,9 5 6                                             ,10,,956,0,0.0,0.0,        0,956,,,Item 956 Site 1,956,956,34945,800000007,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,,,,,2,There are no assignments available.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('18,0,5389,1020010,1,10.00,10,       00,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,0,5390,2187,102,Building 885 Site 1                               ,8 8 5                                             ,Bay 885                                           ,8 8 5                                             ,1,,885,0,0.0,0.0,        0,885,,,Item 885 Site 1,885,885,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5391,2130,102,Building 828 Site 1                               ,8 2 8                                             ,Bay 828                                           ,8 2 8                                             ,5,,828,0,0.0,0.0,        0,828,,,Item 828 Site 1,828,828,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5392,2065,102,Building 763 Site 1                               ,7 6 3                                             ,Bay 763                                           ,7 6 3                                             ,13,,763,0,0.0,0.0,        0,763,,,Item 763 Site 1,763,763,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5393,2095,102,Building 793 Site 1                               ,7 9 3                                             ,Bay 793                                           ,7 9 3                                             ,14,,793,0,0.0,0.0,        0,793,,,Item 793 Site 1,793,793,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5394,2055,102,Building 753 Site 1                               ,7 5 3                                             ,Bay 753                                           ,7 5 3                                             ,5,,753,0,0.0,0.0,        0,753,,,Item 753 Site 1,753,753,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5395,2249,102,Building 947 Site 1                               ,9 4 7                                             ,Bay 947                                           ,9 4 7                                             ,9,,947,0,0.0,0.0,        0,947,,,Item 947 Site 1,947,947,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5396,2123,102,Building 821 Site 1                               ,8 2 1                                             ,Bay 821                                           ,8 2 1                                             ,13,,821,0,0.0,0.0,        0,821,,,Item 821 Site 1,821,821,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5397,2066,102,Building 764 Site 1                               ,7 6 4                                             ,Bay 764                                           ,7 6 4                                             ,1,,764,0,0.0,0.0,        0,764,,,Item 764 Site 1,764,764,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5398,2089,102,Building 787 Site 1                               ,7 8 7                                             ,Bay 787                                           ,7 8 7                                             ,11,,787,0,0.0,0.0,        0,787,,,Item 787 Site 1,787,787,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5399,2209,102,Building 907 Site 1                               ,9 0 7                                             ,Bay 907                                           ,9 0 7                                             ,10,,907,0,0.0,0.0,        0,907,,,Item 907 Site 1,907,907,5389,1020010,10,0,10,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '6',      # Function?
                                   'yes',    # Normal And Chase Assignments, correct?
                                   '102',    # Region?
                                   'yes',    # Region 102, correct?
                                   'ready',  # To receive work, say ready
                                   'ready',  # chase assignment, ID 800000007, say ready
                                   '1!',     # Printer?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Building 751 Site 1
                                   'ready',  # Aisle 7 5 1
                                   'ready',  # Bay 751
                                   '751',    # 7 5 1 Pick 6 ,   Pick Message Site 1
                                   'ready',  # Building 956 Site 1
                                   'ready',  # Aisle 9 5 6
                                   'ready',  # Bay 956
                                   '956!',   # 9 5 6 Pick 10 ,   Pick Message Site 1
                                   'ready',  # Assignment complete.  For next assignment, say ready
                                   'ready',  # ID 1020010, has a goal time of 10.0 minutes, say ready
                                   'ready',  # Building 885 Site 1
                                   'ready',  # Aisle 8 8 5
                                   'ready',  # Bay 885
                                   '885',    # 8 8 5
                                   '1',      # Pick 1 ,   Pick Message Site 1
                                   '-')      # Building 828 Site 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Normal And Chase Assignments, correct?',
                              'Region?',
                              'Region 102, correct?',
                              'To receive work, say ready',
                              'chase assignment, ID 800000007, say ready',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Building 751 Site 1',
                              'Aisle 7 5 1',
                              'Bay 751',
                              '7 5 1 Pick 6 ,   Pick Message Site 1',
                              'Building 956 Site 1',
                              'Aisle 9 5 6',
                              'Bay 956',
                              '9 5 6 Pick 10 ,   Pick Message Site 1',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready',
                              'ID 1020010, has a goal time of 10.0 minutes, say ready',
                              'Building 885 Site 1',
                              'Aisle 8 8 5',
                              'Bay 885',
                              '8 8 5',
                              'Pick 1 ,   Pick Message Site 1',
                              'Building 828 Site 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '6'],
                                      ['prTaskLUTPickingRegion', '102', '6'],
                                      ['prTaskLUTGetAssignment', '1', '2'],
                                      ['prTaskLUTGetPicks', '17', '0', '0', '0'],
                                      ['prTaskLUTPrint', '17', '34945', '0', '', '1', '0'],
                                      ['prTaskODRPicked', '17', '34945', '2053', '6', '1', '', '34946', '', '', ''],
                                      ['prTaskODRPicked', '17', '34945', '2258', '10', '1', '', '34947', '', '', ''],
                                      ['prTaskLUTStopAssignment', '17'],
                                      ['prTaskLUTGetAssignment', '1', '2'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '18', '0', '1', '0'],
                                      ['prTaskODRPicked', '18', '5389', '2187', '1', '1', '', '5390', '', '', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
