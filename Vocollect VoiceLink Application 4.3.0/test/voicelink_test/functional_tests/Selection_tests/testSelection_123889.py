# Verify that containers are opened and re-opened as appropriate when picking to target containers.
# Regression test case Selection | Activate Container | ID 123889
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_123889(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_123889(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,2,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '3,coffee,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,2,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '5,selection region 105,0,\n'
                                 '6,selection region 106,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '109,Region 109,0,\n'
                                 '10,selection region 110,0,\n'
                                 '11,selection region 111,0,\n'
                                 '12,selection region 112,0,\n'
                                 '13,selection region 113,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('109,Region 109,1,1,1,1,1,0,1,0,0,1,1,0,0,2,0,-1,1,0,2,X                                                 ,X                                                 ,X                                                 ,X                                                 ,1,0,1,2,0,0,\n'
                                 '\n')
        self.set_server_response('151,0,640,1091000,1,18.00,180,       00,0,0,,0,\n'
                                 '\n')
        self.set_server_response('N,0,6702,1253,109,Building 1 Site 1                                 ,1                                                 ,,0 1                                               ,20,Eaches,1302,0,0.0,0.0,        0,01,,,TUMS 4 FLAVOR JAR,0.1,7161030002,640,1091000,18,0,18,,1,0,,Item has PVID,0,0,0,0,0,\n'
                                 'N,0,6703,1253,109,Building 1 Site 1                                 ,1                                                 ,,0 1                                               ,6,Eaches,1302,0,0.0,0.0,        0,01,,,TUMS 4 FLAVOR JAR,0.1,7161030002,640,1091000,18,0,18,,2,0,,Item has PVID,0,0,0,0,0,\n'
                                 'N,0,6704,1255,109,Building 1 Site 1                                 ,1                                                 ,,0 3                                               ,8,,1303,0,0.0,0.0,        0,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,640,1091000,18,0,18,,3,0,,Item has PVID,0,0,0,0,0,\n'
                                 'N,0,6705,1255,109,Building 1 Site 1                                 ,1                                                 ,,0 3                                               ,5,,1303,0,0.0,0.0,        0,03,,,WIPES ANTISEPTIC MMI,0.1,9999918358,640,1091000,18,0,18,,6,0,,Item has PVID,0,0,0,0,0,\n'
                                 'N,0,6706,1257,109,Building 1 Site 1                                 ,1                                                 ,,0 5                                               ,1,,1305,1,0.098,0.10200000000000001,        0,05,,,TUMS PEPPERMINT,0.1,7385408100,640,1091000,18,0,18,,3,0,,Item has PVID,0,0,0,0,0,\n'
                                 'N,0,6707,1259,109,Building 1 Site 1                                 ,2                                                 ,,0 7                                               ,3,,1350,1,58.8,61.2,        0,07,,,C &amp; H CANE SUGAR,60,1580003062,640,1091000,18,0,18,,3,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 'N,0,6708,1264,109,Building 1 Site 1                                 ,3                                                 ,,1 2                                               ,6,Boxes,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,640,1091000,18,0,18,,4,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 'N,0,6709,1264,109,Building 1 Site 1                                 ,3                                                 ,,1 2                                               ,2,Packs,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,640,1091000,18,0,18,,5,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 'N,0,6710,1266,109,Building 1 Site 1                                 ,3                                                 ,,1 4                                               ,2,,1334,0,0.0,0.0,        0,14,,,PF VANILLA 3 LAYER CAKE,8.9,9999953903,640,1091000,18,0,18,,4,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 'N,0,6711,1282,109,Building 2 Site 1                                 ,2                                                 ,,30                                                ,3,,1304,0,0.0,0.0,        0,30,,,PENCIL MECHANICAL,0.1,3977918025,640,1091000,18,0,18,,4,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 'N,0,6712,1284,109,Building 2 Site 1                                 ,3                                                 ,,3 2                                               ,1,,1330,0,0.0,0.0,        0,32,,,KOOL-AID BLUE MOON BERRY,5.4,4300095389,640,1091000,18,0,18,,5,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 'N,0,6713,1302,109,Building 3 Site 1                                 ,4 E                                               ,Bay 5                                             ,5 0                                               ,2,By Weight,1349,1,52.3418,54.478199999999994,0,50,,,HEN TURKEY,53.41,7262306325,640,1091000,18,0,18,,5,0,,Item has PVID,0,0,0,0,0,\n'
                                 'N,0,6714,1302,109,Building 3 Site 1                                 ,4 E                                               ,Bay 5                                             ,5 0                                               ,2,By Weight,1349,1,52.3418,54.478199999999994,0,50,,,HEN TURKEY,53.41,7262306325,640,1091000,18,0,18,,1,0,,Item has PVID,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('215,0000000215,15,640,1091000,6,A,0,0,\n'
                                 '214,0000000214,14,640,1091000,5,A,0,0,\n'
                                 '213,0000000213,13,640,1091000,4,A,0,0,\n'
                                 '212,0000000212,12,640,1091000,3,A,0,0,\n'
                                 '211,0000000211,11,640,1091000,2,A,0,0,\n'
                                 '210,0000000210,10,640,1091000,1,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('210,0000000210,10,640,1091000,1,O,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '214,0000000214,14,640,1091000,5,A,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,A,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,A,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('211,0000000211,11,640,1091000,2,O,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,O,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '214,0000000214,14,640,1091000,5,A,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,A,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('210,0000000210,10,640,1091000,1,O,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '214,0000000214,14,640,1091000,5,A,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,A,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,A,1,0,\n'
                                 '\n')
        self.set_server_response('212,0000000212,12,640,1091000,3,O,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,O,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '214,0000000214,14,640,1091000,5,A,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('213,0000000213,13,640,1091000,4,O,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,O,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,O,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '214,0000000214,14,640,1091000,5,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('214,0000000214,14,640,1091000,5,O,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,O,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,O,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,O,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('214,0000000214,14,640,1091000,5,O,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,O,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,O,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,C,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('214,0000000214,14,640,1091000,5,O,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,O,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,C,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,C,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,A,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('215,0000000215,15,640,1091000,6,O,1,0,\n'
                                 '214,0000000214,14,640,1091000,5,O,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,O,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,C,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,C,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('214,0000000214,14,640,1091000,5,O,1,0,\n'
                                 '212,0000000212,12,640,1091000,3,O,1,0,\n'
                                 '215,0000000215,15,640,1091000,6,C,1,0,\n'
                                 '213,0000000213,13,640,1091000,4,C,1,0,\n'
                                 '211,0000000211,11,640,1091000,2,C,1,0,\n'
                                 '210,0000000210,10,640,1091000,1,C,1,0,\n'
                                 '\n')
        self.set_server_response('G,0,6707,1259,109,Building 1 Site 1                                 ,2                                                 ,,0 7                                               ,3,,1350,1,58.8,61.2,        1,07,,,C &amp; H CANE SUGAR,60,1580003062,640,1091000,18,0,18,,3,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 'G,0,6709,1264,109,Building 1 Site 1                                 ,3                                                 ,,1 2                                               ,2,Packs,1332,0,0.0,0.0,        0,12,,,RICE KRISPIE,6.5,3800093931,640,1091000,18,0,18,,5,0,,Location has CD Site 1,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   'yes',              # 123, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '109',              # Region?
                                   'yes',              # Region 109, correct?
                                   'ready',            # To receive work, say ready
                                   'ready',            # ID 1091000, has a goal time of 18.0 minutes, say ready
                                   'yes',              # Preprint all labels for ID 1091000
                                   '1!',               # Printer?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Open 10
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 1
                                   '01',               # 0 1 Pick 20 Eaches,   Item has PVID
                                   'ready',            # Open 11
                                   '01',               # 0 1 Pick 6 Eaches,   Item has PVID
                                   '11',               # Container?
                                   'ready',            # Open 12
                                   'skip slot',        # 0 3 Pick 8 ,   Item has PVID
                                   'yes',              # Skip slot, correct?
                                   'new container',    # 0 5 Pick 1 ,   Item has PVID
                                   'close container',  # 0 5 Pick 1 ,   Item has PVID
                                   'partial',          # 0 5 Pick 1 ,   Item has PVID
                                   '05',               # 
                                   '10',               # Container?
                                   '12',               # Container?
                                   '0.10',             # weight 1 of 1
                                   'ready',            # Entries complete
                                   'ready',            # Aisle 2
                                   'short product',    # 0 7 Pick 3 ,   Location has CD Site 1
                                   '07',               # 
                                   '1',                # how many did you pick?
                                   'yes',              # 1, correct?
                                   '12',               # Container?
                                   '60.00',            # weight 1 of 1
                                   'ready',            # Entries complete
                                   'ready',            # Aisle 3
                                   'ready',            # Open 13
                                   '12',               # 1 2 Pick 6 Boxes,   Location has CD Site 1
                                   '13',               # Container?
                                   'ready',            # Open 14
                                   'short product',    # 1 2 Pick 2 Packs,   Location has CD Site 1
                                   '12',               # 
                                   '0',                # how many did you pick?
                                   'yes',              # 0, correct?
                                   '14',               # 1 4 Pick 2 ,   Location has CD Site 1
                                   '13',               # Container?
                                   'ready',            # Building 2 Site 1
                                   'ready',            # Aisle 2
                                   '30',               # 30 Pick 3 ,   Location has CD Site 1
                                   '13',               # Container?
                                   'ready',            # Aisle 3
                                   '32',               # 3 2 Pick 1 ,   Location has CD Site 1
                                   '14',               # Container?
                                   'ready',            # Building 3 Site 1
                                   'ready',            # Aisle 4 E
                                   'ready',            # Bay 5
                                   'short product',    # 5 0 Pick 2 By Weight,   Item has PVID
                                   '50',               # 
                                   '2',                # how many did you pick?
                                   'yes',              # 2, correct?
                                   '14',               # Container?
                                   '53.30',            # weight 1 of 2
                                   '53.31',            # weight 2 of 2
                                   'ready',            # Entries complete
                                   '50',               # 5 0 Pick 2 By Weight,   Item has PVID
                                   '10',               # Container?
                                   '53.33',            # weight 1 of 2
                                   '53.34',            # weight 2 of 2
                                   'ready',            # Entries complete
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 1
                                   '03',               # 0 3 Pick 8 ,   Item has PVID
                                   '12',               # Container?
                                   'ready',            # Open 15
                                   '03',               # 0 3 Pick 5 ,   Item has PVID
                                   '15',               # Container?
                                   'ready',            # Building 1 Site 1
                                   'ready',            # Aisle 2
                                   'short product',    # 0 7 Pick 2 ,   Location has CD Site 1
                                   '07',               # 
                                   '1',                # how many did you pick?
                                   'yes',              # 1, correct?
                                   '12',               # Container?
                                   '60.00',            # weight 1 of 1
                                   'ready',            # Entries complete
                                   'ready',            # Aisle 3
                                   'short product',    # 1 2 Pick 2 Packs,   Location has CD Site 1
                                   '12',               # 
                                   '0',                # how many did you pick?
                                   'yes',              # 0, correct?
                                   '-')                # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              '123, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'Region 109, correct?',
                              'To receive work, say ready',
                              'ID 1091000, has a goal time of 18.0 minutes, say ready',
                              'Preprint all labels for ID 1091000',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Open 10',
                              'Building 1 Site 1',
                              'Aisle 1',
                              '0 1 Pick 20 Eaches,   Item has PVID',
                              'Open 11',
                              '0 1 Pick 6 Eaches,   Item has PVID',
                              'Container?',
                              'Open 12',
                              '0 3 Pick 8 ,   Item has PVID',
                              'Skip slot, correct?',
                              '0 5 Pick 1 ,   Item has PVID',
                              'New container not allowed when picks have a target container',
                              '0 5 Pick 1 ,   Item has PVID',
                              'close container not allowed when picks have a target container',
                              '0 5 Pick 1 ,   Item has PVID',
                              'partial not allowed when picks have a target container',
                              'check digit?',
                              'Container?',
                              'Wrong 10. Try again.',
                              'Container?',
                              'weight 1 of 1',
                              'Entries complete',
                              'Aisle 2',
                              '0 7 Pick 3 ,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '1, correct?',
                              'Container?',
                              'weight 1 of 1',
                              'Entries complete',
                              'Aisle 3',
                              'Open 13',
                              '1 2 Pick 6 Boxes,   Location has CD Site 1',
                              'Container?',
                              'Open 14',
                              '1 2 Pick 2 Packs,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '0, correct?',
                              'reopen 13',
                              '1 4 Pick 2 ,   Location has CD Site 1',
                              'Container?',
                              'Building 2 Site 1',
                              'Aisle 2',
                              '30 Pick 3 ,   Location has CD Site 1',
                              'Container?',
                              'Aisle 3',
                              'reopen 14',
                              '3 2 Pick 1 ,   Location has CD Site 1',
                              'Container?',
                              'Building 3 Site 1',
                              'Aisle 4 E',
                              'Bay 5',
                              '5 0 Pick 2 By Weight,   Item has PVID',
                              'check digit?',
                              'how many did you pick?',
                              '2, correct?',
                              'Container?',
                              'weight 1 of 2',
                              'weight 2 of 2',
                              'Entries complete',
                              'reopen 10',
                              '5 0 Pick 2 By Weight,   Item has PVID',
                              'Container?',
                              'weight 1 of 2',
                              'weight 2 of 2',
                              'Entries complete',
                              'Building 1 Site 1',
                              'Aisle 1',
                              'reopen 12',
                              '0 3 Pick 8 ,   Item has PVID',
                              'Container?',
                              'Open 15',
                              '0 3 Pick 5 ,   Item has PVID',
                              'Container?',
                              'Picking complete',
                              'shorts are reported',
                              'Going back for shorts',
                              'Picking shorts',
                              'Building 1 Site 1',
                              'Aisle 2',
                              'reopen 12',
                              '0 7 Pick 2 ,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '1, correct?',
                              'Container?',
                              'weight 1 of 1',
                              'Entries complete',
                              'Aisle 3',
                              'reopen 14',
                              '1 2 Pick 2 Packs,   Location has CD Site 1',
                              'check digit?',
                              'how many did you pick?',
                              '0, correct?',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '109', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '151', '0', '1', '0'],
                                      ['prTaskLUTContainer', '151', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '151', '640', '', '', '', '3', ''],
                                      ['prTaskLUTPrint', '151', '640', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '151', '640', '1', '', '', '2', ''],
                                      ['prTaskLUTPicked', '151', '640', '1253', '20', '1', '210', '6702', '', '', ''],
                                      ['prTaskLUTContainer', '151', '640', '2', '', '', '2', ''],
                                      ['prTaskLUTPicked', '151', '640', '1253', '6', '1', '211', '6703', '', '', ''],
                                      ['prTaskLUTContainer', '151', '640', '', '211', '', '1', ''],
                                      ['prTaskLUTContainer', '151', '640', '3', '', '', '2', ''],
                                      ['prTaskLUTUpdateStatus', '151', '1255', '0', 'S'],
                                      ['prTaskLUTPicked', '151', '640', '1257', '1', '1', '212', '6706', '', '0.10', ''],
                                      ['prTaskLUTPicked', '151', '640', '1259', '1', '1', '212', '6707', '', '60.00', ''],
                                      ['prTaskLUTContainer', '151', '640', '4', '', '', '2', ''],
                                      ['prTaskLUTPicked', '151', '640', '1264', '6', '1', '213', '6708', '', '', ''],
                                      ['prTaskLUTContainer', '151', '640', '5', '', '', '2', ''],
                                      ['prTaskLUTPicked', '151', '640', '1264', '0', '1', '', '6709', '', '', ''],
                                      ['prTaskLUTPicked', '151', '640', '1266', '2', '1', '213', '6710', '', '', ''],
                                      ['prTaskLUTPicked', '151', '640', '1282', '3', '1', '213', '6711', '', '', ''],
                                      ['prTaskLUTContainer', '151', '640', '', '213', '', '1', ''],
                                      ['prTaskLUTPicked', '151', '640', '1284', '1', '1', '214', '6712', '', '', ''],
                                      ['prTaskLUTPicked', '151', '640', '1302', '1', '0', '214', '6713', '', '53.30', ''],
                                      ['prTaskLUTPicked', '151', '640', '1302', '1', '1', '214', '6713', '', '53.31', ''],
                                      ['prTaskLUTPicked', '151', '640', '1302', '1', '0', '210', '6714', '', '53.33', ''],
                                      ['prTaskLUTPicked', '151', '640', '1302', '1', '1', '210', '6714', '', '53.34', ''],
                                      ['prTaskLUTContainer', '151', '640', '', '210', '', '1', ''],
                                      ['prTaskLUTUpdateStatus', '151', '', '2', 'N'],
                                      ['prTaskLUTPicked', '151', '640', '1255', '8', '1', '212', '6704', '', '', ''],
                                      ['prTaskLUTContainer', '151', '640', '6', '', '', '2', ''],
                                      ['prTaskLUTPicked', '151', '640', '1255', '5', '1', '215', '6705', '', '', ''],
                                      ['prTaskLUTContainer', '151', '640', '', '215', '', '1', ''],
                                      ['prTaskLUTGetPicks', '151', '0', '1', '0'],
                                      ['prTaskLUTPicked', '151', '640', '1259', '1', '1', '212', '6707', '', '60.00', ''],
                                      ['prTaskLUTPicked', '151', '640', '1264', '0', '1', '', '6709', '', '', ''],
                                      ['prTaskLUTStopAssignment', '151'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
