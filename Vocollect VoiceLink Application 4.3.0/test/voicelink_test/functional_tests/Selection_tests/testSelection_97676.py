# Verify that when the user is prompted with pre-aisle location 
# information at second pick item and user says 'How much more' 
# or 'repeat last pick', correct response about picks is given to the user
# Regression test case Selection | Information Vocabs | ID 97676
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97676(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97676(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '\n')
        self.set_server_response('1,LUT Failure,1,1,1,1,1,0,0,0,2,1,1,0,0,0,0,4,0,0,1,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'B,0,2,L2,1,pre 2,A 1,post 2,S 1,15,,ITEM13,0,0.0,0.0,00,00,,,Item Description,Size,UPC,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,3,L1,1,pre 1,A 1,post 1,S 1,25,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,12345,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,12345,Store 3,0,O,0,0,\n'
                                 '45647,0000004444,36,12345,Store 7,0,O,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',             # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',              # Password?
                                   '3!',                # Function?
                                   'yes',               # normal assignments, correct?
                                   '1',                 # Region?
                                   'yes',               # selection region 1, correct?
                                   'ready',             # To receive work, say ready
                                   'ready',             # ID Store 123, has a goal time of 15 minutes, say ready
                                   'no',                # Base summary?
                                   'yes',               # Pick base items?
                                   'ready',             # pre 1
                                   'ready',             # Aisle A 1
                                   'ready',             # post 1
                                   '00',                # S 1
                                   'ready',             # Pick 5 ,   pick message
                                   '34',                # Container?
                                   'how much more',     # pre 2
                                   'repeat last pick',  # pre 2
                                   '-')                 # pre 2

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 5 ,   pick message',
                              'Container?',
                              'pre 2',
                              '45 remaining at 3 line items',
                              'pre 2',
                              'last pick was aisle A 1,, slot S 1,, picked 5 of 5,',
                              'pre 2')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '5', '1', '23452', '1', '', '', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
