# Test that speaking 'pass assignment'  during a chase assignment pick will result in the terminal giving the message 
# 'Pass assignment not allowed.'
# Regression test case Selection | Normal and Chase Assignments | ID 130784
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_130784(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_130784(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('101,Region 101,0,\r\n'
                                 '107,Region 107,0,\r\n'
                                 '115,Region 115,0,\r\n'
                                 '111,Region 111,0,\r\n'
                                 '112,Region 112,0,\r\n'
                                 '113,Region 113,0,\r\n'
                                 '102,Region 102,0,\r\n'
                                 '103,Region 103,0,\r\n'
                                 '104,Region 104,0,\r\n'
                                 '105,Region 105,0,\r\n'
                                 '106,Region 106,0,\r\n'
                                 '110,Region 110,0,\r\n'
                                 '109,Region 109,0,\r\n'
                                 '108,Region 108,0,\r\n'
                                 '114,Region 114,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('102,Region 102,2,1,1,1,1,0,0,1,0,1,0,0,0,2,0,-1,0,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,0,2,0,0,\r\n'
                                 '102,Region 102,1,1,1,1,1,0,2,0,2,1,0,0,0,0,1,-1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,0,2,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('16,1,34805,800000005,1,0.00,8,       00,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,0,34806,2273,102,Building 971 Site 1                               ,9 7 1                                             ,Bay 971                                           ,9 7 1                                             ,2,,971,0,0.0,0.0,        0,971,,,Item 971 Site 1,971,971,34805,800000005,8,0,8,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '6',                # Function?
                                   'yes',              # Normal And Chase Assignments, correct?
                                   '102',              # Region?
                                   'yes',              # Region 102, correct?
                                   'ready',            # To receive work, say ready
                                   'ready',            # chase assignment, ID 800000005, say ready
                                   '1!',               # Printer?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Building 971 Site 1
                                   'ready',            # Aisle 9 7 1
                                   'ready',            # Bay 971
                                   'pass assignment',  # 9 7 1 Pick 2 ,   Pick Message Site 1
                                   '971!',             # 9 7 1 Pick 2 ,   Pick Message Site 1
                                   '-')                # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Normal And Chase Assignments, correct?',
                              'Region?',
                              'Region 102, correct?',
                              'To receive work, say ready',
                              'chase assignment, ID 800000005, say ready',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Building 971 Site 1',
                              'Aisle 9 7 1',
                              'Bay 971',
                              '9 7 1 Pick 2 ,   Pick Message Site 1',
                              'Pass assignment not allowed.',
                              '9 7 1 Pick 2 ,   Pick Message Site 1',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '6'],
                                      ['prTaskLUTPickingRegion', '102', '6'],
                                      ['prTaskLUTGetAssignment', '1', '2'],
                                      ['prTaskLUTGetPicks', '16', '0', '0', '0'],
                                      ['prTaskLUTPrint', '16', '34805', '0', '', '1', '0'],
                                      ['prTaskODRPicked', '16', '34805', '2273', '2', '1', '', '34806', '', '', ''],
                                      ['prTaskLUTStopAssignment', '16'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
