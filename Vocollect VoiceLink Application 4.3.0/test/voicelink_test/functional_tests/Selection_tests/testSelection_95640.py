##TestLink ID 95640 :: Test Case Verify correct behavior if VoiceLink region profile 103 is used.

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_95640(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_95640(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('3,Region 103,1,1,1,1,1,0,1,0,2,1,1,1,0,1,1,4,0,0,0,,,,,0,0,1,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 1,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L3,1,,A 2,,S 3,5,,ITEM13,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L4,1,,A 2,,S 4,5,,ITEM14,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,,O,0,0,\n'
                                 '2,0000000004,04,12345,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '3',      # Region?
                                   'yes',    # selection region 3, correct?
                                   'ready',  # To receive work, say ready
                                   'ready',  # ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',  # Open 03
                                   '3!',     # Printer?
                                   'yes',    # printer <spell>3</spell>, correct?
                                   '-')      # Aisle A 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 3, correct?',
                              'To receive work, say ready',
                              'ID Store 111, has a goal time of 15 minutes, say ready',
                              'Open 03',
                              'Printer?',
                              'printer <spell>3</spell>, correct?',
                              'Aisle A 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '111', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '111', '1', '', '3', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
