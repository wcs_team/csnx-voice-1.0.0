# Verify change function command
# Regression test case Selection | Global Words | ID 97641
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97641(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97641(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,222,Store 222,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,Pre Aisle,A 1,Post Aisle,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'B,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('2,frozen food,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '3!',             # Function?
                                   'yes',            # normal assignments, correct?
                                   '1',              # Region?
                                   'yes',            # selection region 1, correct?
                                   'change region',  # How many assignments?
                                   '1',              # Region?
                                   'yes',            # selection region 1, correct?
                                   '1',              # How many assignments?
                                   'ready',          # To receive work, say ready
                                   'change region',  # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',          # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',          # position 2, ID Store 222, has a goal time of 15 minutes, say ready
                                   'change region',  # Base summary?
                                   'ready',          # Base summary?
                                   'no',             # Base summary?
                                   'change region',  # Pick base items?
                                   'no',             # Pick base items?
                                   'change region',  # Pre Aisle
                                   'ready',          # Pre Aisle
                                   'change region',  # Aisle A 1
                                   'ready',          # Aisle A 1
                                   'change region',  # Post Aisle
                                   'ready',          # Post Aisle
                                   'change region',  # S 1 Pick 5 ,  ID Store 111, 
                                   '00',             # S 1 Pick 5 ,  ID Store 111, 
                                   'change region',  # Quantity?
                                   '5',              # Quantity?
                                   'ready',          # Aisle A 1
                                   '00',             # S 1 Pick 5 ,  ID Store 222, 
                                   '5',              # Quantity?
                                   'ready',          # Aisle A 2
                                   '00',             # S 2 Pick 5 ,  ID Store 222, 
                                   '5',              # Quantity?
                                   'change region',  # Load position 1 ID Store 111 at Location 1
                                   'ready',          # Load position 1 ID Store 111 at Location 1
                                   'change region',  # Confirm Delivery
                                   '1000',             # Confirm Delivery
                                   'change region',  # Position?
                                   'right',          # Position?
                                   'yes',            # right, correct?
                                   'ready',          # Load position 2 ID Store 222 at Location 1
                                   '1000',             # Confirm Delivery
                                   'left',           # Position?
                                   'yes',            # left, correct?
                                   'change region',  # Assignment complete.  For next assignment, say ready
                                   '2',              # Region?
                                   'yes',            # selection region 2, correct?
                                   'change region',  # Work ID?
                                   '-')              # Region?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'How many assignments?',
                              'Region?',
                              'selection region 1, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'position 1, ID Store 111, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 222, has a goal time of 15 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'Pre Aisle',
                              'Aisle A 1',
                              'Post Aisle',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'Quantity?',
                              'Aisle A 1',
                              'S 1 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Aisle A 2',
                              'S 2 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Picking complete',
                              'Load position 1 ID Store 111 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'right, correct?',
                              'Load position 2 ID Store 222 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'Region?',
                              'selection region 2, correct?',
                              'Work ID?',
                              'Region?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '111'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '222'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTStopAssignment', '1'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
