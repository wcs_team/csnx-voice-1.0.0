# Verify reprint labels can be spoken anywhere in the pick prompt
# Regression test case Selection | Reprint Labels | ID 97745
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97745(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97745(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '5,selection region 5,0,\n'
                                 '6,selection region 6,0,\n'
                                 '7,selection region 7,0,\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,5,1,1,1,1,0,1,1,1,1,0,1,1,4,0,1,1,xxxxxxx,xxxxxxxxx,xxxxxxxx,xxxxxxxx,0,1,1,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,0,\n'
                                 '1,0,43,Wet way,2,7.00,233,00,0,0,This is not new,0,\n'
                                 '1,0,44,Red way,3,9.00,244,00,0,0,Few new,0,\n'
                                 '\n')
        self.set_server_response('N,0,402,893,1,pre 302,right 402,post 502,slot 891,150,Pack,893,1,2.56,8.71,0,,,,Item 891 Site,890,890,42,1030,0,0,0,,0,0,,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,401,897,1,pre 311,right 411,post 511,slot 890,15,Pack,992,1,7.5,11.0,0,890,,,Item 890 Site,890,890,43,1040,0,0,0,,0,0,Lot Lot,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,406,891,1,pre 303,right 403,post 503,slot 897,19,Pack,894,1,10.0,6.0,0,890,,,Item 890 Site,890,890,44,1030000,0,0,0,,0,0,Lot very,Pick Message Site 1,0,0,1,1,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,43,Store 3,0,O,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,43,Store 3,0,O,0,0,\n'
                                 '45647,0000004444,36,44,Store 7,0,O,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',           # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',            # Password?
                                   '3!',              # Function?
                                   'yes',             # normal assignments, correct?
                                   '3',               # Region?
                                   'yes',             # selection region 3, correct?
                                   '3',               # How many assignments?
                                   'ready',           # To receive work, say ready
                                   'no',              # Pick in reverse order?
                                   'ready',           # position 1, ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'ready',           # position 2, ID Wet way, has a goal time of 7.0 minutes, say ready
                                   'ready',           # position 3, ID Red way, has a goal time of 9.0 minutes, say ready
                                   '34!',             # New Container ID? Position 1
                                   'yes',             # 34, correct?
                                   '1!',              # Printer?
                                   'yes',             # printer <spell>1</spell>, correct?
                                   '35!',             # New Container ID? Position 2
                                   'yes',             # 35, correct?
                                   'yes',             # printer <spell>1</spell>, correct?
                                   '36!',             # New Container ID? Position 3
                                   'yes',             # 36, correct?
                                   'yes',             # printer <spell>1</spell>, correct?
                                   'reprint labels',  # pre 302
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>1</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   'reprint labels',  # pre 302
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>1</spell>, correct?
                                   '35',              # Container?
                                   'yes',             # 35, correct?
                                   'reprint labels',  # pre 302
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>1</spell>, correct?
                                   'all',             # Container?
                                   'yes',             # All correct?
                                   'ready',           # pre 302
                                   'reprint labels',  # Aisle right 402
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>1</spell>, correct?
                                   '36',              # Container?
                                   'yes',             # 36, correct?
                                   'reprint labels',  # Aisle right 402
                                   'yes',             # reprint labels, correct? 
                                   'no',              # printer <spell>1</spell>, correct?
                                   '2!',              # Printer?
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   'ready',           # Aisle right 402
                                   'reprint labels',  # post 502
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '33',              # Container?
                                   'yes',             # 33, correct?
                                   '36',              # Container?
                                   'ready',           # 36, correct?
                                   'yes',             # 36, correct?
                                   'ready',           # post 502
                                   'reprint labels',  # slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '35',              # Container?
                                   'yes',             # 35, correct?
                                   'ready',           # slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1
                                   'reprint labels',  # Quantity?
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   'reprint labels',  # Quantity?
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '35',              # Container?
                                   'yes',             # 35, correct?
                                   'reprint labels',  # Quantity?
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   '2!',              # Quantity?
                                   'yes',             # You said 2 asked for 150. Is this a short product?
                                   'reprint labels',  # Put 2 in 1
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   '34',              # Put 2 in 1
                                   'reprint labels',  # weight 1 of 2
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '36',              # Container?
                                   'yes',             # 36, correct?
                                   '3.67',            # weight 1 of 2
                                   'reprint labels',  # serial number 1 of 2
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '36',              # Container?
                                   'yes',             # 36, correct?
                                   '1234ready',       # serial number 1 of 2
                                   '3.68',            # weight 2 of 2
                                   '1256ready',       # serial number 2 of 2
                                   'reprint labels',  # Entries complete
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   'ready',           # Entries complete
                                   'ready',           # pre 311
                                   'ready',           # Aisle right 411
                                   'ready',           # post 511
                                   '890',             # slot 890 Pick 15 Pack, item 890 site,  Pick Message Site 1
                                   '0!',              # Quantity?
                                   'yes',             # You said 0 asked for 15. Is this a short product?
                                   'ready',           # pre 303
                                   'ready',           # Aisle right 403
                                   'ready',           # post 503
                                   '890',             # slot 897 Pick 19 Pack, item 890 site,  Pick Message Site 1
                                   '0!',              # Quantity?
                                   'yes',             # You said 0 asked for 19. Is this a short product?
                                   'reprint labels',  # Deliver position 1 ID Dry way to Location 1231
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   'ready',           # Deliver position 1 ID Dry way to Location 1231
                                   'reprint labels',  # Confirm Delivery
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>2</spell>, correct?
                                   '36',              # Container?
                                   'yes',             # 36, correct?
                                   '678',             # Confirm Delivery
                                   'ready',           # Deliver position 2 ID Wet way to Location 1231
                                   '678',             # Confirm Delivery
                                   'ready',           # Deliver position 3 ID Red way to Location 1231
                                   '678',             # Confirm Delivery
                                   'ready',           # Assignment complete.  For next assignment, say ready
                                   '-')               # How many assignments?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 3, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'position 1, ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'position 2, ID Wet way, has a goal time of 7.0 minutes, say ready',
                              'position 3, ID Red way, has a goal time of 9.0 minutes, say ready',
                              'New Container ID? Position 1',
                              '34, correct?',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'New Container ID? Position 2',
                              '35, correct?',
                              'printer <spell>1</spell>, correct?',
                              'New Container ID? Position 3',
                              '36, correct?',
                              'printer <spell>1</spell>, correct?',
                              'pre 302',
                              'reprint labels, correct?',
                              'printer <spell>1</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'pre 302',
                              'reprint labels, correct?',
                              'printer <spell>1</spell>, correct?',
                              'Container?',
                              '35, correct?',
                              'pre 302',
                              'reprint labels, correct?',
                              'printer <spell>1</spell>, correct?',
                              'Container?',
                              'All correct?',
                              'pre 302',
                              'Aisle right 402',
                              'reprint labels, correct?',
                              'printer <spell>1</spell>, correct?',
                              'Container?',
                              '36, correct?',
                              'Aisle right 402',
                              'reprint labels, correct?',
                              'printer <spell>1</spell>, correct?',
                              'Printer?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'Aisle right 402',
                              'post 502',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '33, correct?',
                              'Invalid <spell>33</spell>, Try again',
                              'Container?',
                              '36, correct?',
                              'post 502',
                              'slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '35, correct?',
                              'slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1',
                              'Quantity?',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'Quantity?',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '35, correct?',
                              'Quantity?',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'Quantity?',
                              'You said 2 asked for 150. Is this a short product?',
                              'Put 2 in 1',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'Put 2 in 1',
                              'weight 1 of 2',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '36, correct?',
                              'weight 1 of 2',
                              'serial number 1 of 2',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '36, correct?',
                              'serial number 1 of 2',
                              'weight 2 of 2',
                              'serial number 2 of 2',
                              'Entries complete',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'Entries complete',
                              'pre 311',
                              'Aisle right 411',
                              'post 511',
                              'slot 890 Pick 15 Pack, item 890 site,  Pick Message Site 1',
                              'Quantity?',
                              'You said 0 asked for 15. Is this a short product?',
                              'pre 303',
                              'Aisle right 403',
                              'post 503',
                              'slot 897 Pick 19 Pack, item 890 site,  Pick Message Site 1',
                              'Quantity?',
                              'You said 0 asked for 19. Is this a short product?',
                              'Picking complete',
                              'Deliver position 1 ID Dry way to Location 1231',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'Deliver position 1 ID Dry way to Location 1231',
                              'Confirm Delivery',
                              'reprint labels, correct?',
                              'printer <spell>2</spell>, correct?',
                              'Container?',
                              '36, correct?',
                              'Confirm Delivery',
                              'Deliver position 2 ID Wet way to Location 1231',
                              'Confirm Delivery',
                              'Deliver position 3 ID Red way to Location 1231',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready',
                              'How many assignments?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '3', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '42', '', '', '34', '2', ''],
                                      ['prTaskLUTPrint', '1', '42', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '43', '', '', '35', '2', ''],
                                      ['prTaskLUTPrint', '1', '43', '1', '', '1', '0'],
                                      ['prTaskLUTContainer', '1', '44', '', '', '36', '2', ''],
                                      ['prTaskLUTPrint', '1', '44', '1', '', '1', '0'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '1', '1'],
                                      ['prTaskLUTPrint', '1', '43', '1', '34563', '1', '1'],
                                      ['prTaskLUTPrint', '1', '', '1', '', '1', '1'],
                                      ['prTaskLUTPrint', '1', '44', '1', '45647', '1', '1'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '2', '1'],
                                      ['prTaskLUTPrint', '1', '44', '1', '45647', '2', '1'],
                                      ['prTaskLUTPrint', '1', '43', '1', '34563', '2', '1'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '2', '1'],
                                      ['prTaskLUTPrint', '1', '43', '1', '34563', '2', '1'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '2', '1'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '2', '1'],
                                      ['prTaskLUTPrint', '1', '44', '1', '45647', '2', '1'],
                                      ['prTaskLUTPrint', '1', '44', '1', '45647', '2', '1'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '2', '1'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '23452', '402', '', '3.67', '1234'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '1', '23452', '402', '', '3.68', '1256'],
                                      ['prTaskLUTPicked', '1', '43', '897', '0', '1', '', '401', '', '', ''],
                                      ['prTaskLUTPicked', '1', '44', '891', '0', '1', '', '406', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '42'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '2', '1'],
                                      ['prTaskLUTPrint', '1', '44', '1', '45647', '2', '1'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '43'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '44'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
