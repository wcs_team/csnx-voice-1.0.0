# Verify that the correct location prompts are 1spoken if the Pre , Aisle and Post Aisle
# is same for Location 1 and Location 2
# Regression test case Selection | Direct to a Location | ID 97764
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97764(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97764(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,1,1,1,0,0,0,2,1,0,0,0,1,1,4,1,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,400,892,0, 891, 891, 891,slot 891,13,,890,0,0.0,0.0,0,890,,,Item 890 Site,890,890,12345,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                 'N,0,401,891,0, 891, 891, 891,slot 892,13,,890,0,0.0,0.0,0,890,,,Item 890 Site,890,890,12345,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '1',      # Region?
                                   'yes',    # selection region 1, correct?
                                   'ready',  # To receive work, say ready
                                   'ready',  # ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',  # 891
                                   'ready',  # Aisle 891
                                   'ready',  # 891
                                   '890',    # slot 891
                                   '13',     # Pick 13 ,   Pick Message Site 1
                                   '890',    # slot 892
                                   '13',     # Pick 13 ,   Pick Message Site 1
                                   '-')      # Deliver to Location 1231

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              '891',
                              'Aisle 891',
                              '891',
                              'slot 891',
                              'Pick 13 ,   Pick Message Site 1',
                              'slot 892',
                              'Pick 13 ,   Pick Message Site 1',
                              'Picking complete',
                              'Deliver to Location 1231')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '1', '0'],
                                      ['prTaskODRPicked', '1', '12345', '892', '13', '1', '', '400', '', '', ''],
                                      ['prTaskODRPicked', '1', '12345', '891', '13', '1', '', '401', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
