# Verify that the selector hears an error message if an error or warning is returned 
# in response to the Get Assignment LUT
# Regression test case Selection | Get Assignment Auto | ID 97770
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97770(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97770(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '5,selection region 5,0,\n'
                                 '6,selection region 6,0,\n'
                                 '7,selection region 7,0,\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,1,5,1,1,0,1,0,1,1,1,0,0,1,1,4,0,1,1,,,,,0,1,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,199,Error 199\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,99,Error 99\n'
                                 '\n')
        self.set_server_response('N,0,402,893,1,pre 302,right 402,post 502,slot 891,150,Pack,893,0,2.56,8.71,0,,,,Item 891 Site,890,890,42,1030,0,0,0,,0,0,,Pick Message Site 1,0,0,0,1,0,\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,43,Store 3,0,O,0,0,\n'
                                 '45647,0000004444,36,44,Store 7,0,O,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,98,Error 98\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',     # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',      # Password?
                                   '3!',        # Function?
                                   'yes',       # normal assignments, correct?
                                   '2',         # Region?
                                   'yes',       # selection region 2, correct?
                                   '1',         # How many assignments?
                                   'ready',     # To receive work, say ready
                                   'ready',     # Error 199, To continue say ready
                                   '1',         # How many assignments?
                                   'ready',     # To receive work, say ready
                                   'ready',     # Error 99, say ready
                                   'no',        # Pick in reverse order?
                                   'ready',     # ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'ready',     # pre 302
                                   'ready',     # Aisle right 402
                                   'ready',     # post 502
                                   'ready',     # slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1
                                   '150',       # Quantity?
                                   'ready',     # Deliver to Location 1231
                                   '678',       # Confirm Delivery
                                   'ready',     # Assignment complete.  For next assignment, say ready
                                   '1',         # How many assignments?
                                   'ready',     # To receive work, say ready
                                   'sign off',  # Error 98, say sign off
                                   'ready',     # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '-')         # Password?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'Error 199, To continue say ready',
                              'How many assignments?',
                              'To receive work, say ready',
                              'Error 99, say ready',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'pre 302',
                              'Aisle right 402',
                              'post 502',
                              'slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1',
                              'Quantity?',
                              'Picking complete',
                              'Deliver to Location 1231',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready',
                              'How many assignments?',
                              'To receive work, say ready',
                              'Error 98, say sign off',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTPicked', '1', '42', '893', '150', '1', '23452', '402', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '42'],
                                      ['prTaskLUTStopAssignment', '1'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
