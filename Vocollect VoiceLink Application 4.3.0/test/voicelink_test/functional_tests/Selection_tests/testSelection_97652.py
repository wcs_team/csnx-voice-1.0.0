# Verify container and print LUTs are correct for Region profile 108
# Regression test case Selection | Get Containers and Print Labels | ID 97652
# Generated using Selection_16.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97652(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97652(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('8,Region 108,1,0,1,1,1,0,1,0,0,1,1,0,1,2,0,4,0,0,0,XXXXX,XXX,XXXXXX,XXX,1,0,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('12345,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                       '\n')
        self.set_server_response('B,0,3,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM10,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,0,1,lot text,pick message,0,0,0,0,0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                       '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,,A,0,0,\n'
                                       '2,0000000004,04,12345,Store 123,,A,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,,O,1,0,\n'
                                       '2,0000000004,04,12345,Store 123,,A,0,0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '8!',     # Region?
                                   'yes',    # selection region 108, correct?
                                   '1234',   # Work ID?
                                   'yes',    # 1234, correct?
                                   'ready',  # ID Store 123, has a goal time of 15 minutes, say ready
                                   '4!',     # Number of Labels for ID Store 123
                                   'yes',    # 4, correct?
                                   '2!',     # Printer?
                                   'yes',    # printer 2, correct?
                                   'ready',  # Open 03
                                   '-')      # Base summary?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready', 'Password?', 'Function?', 'normal assignments, correct?', 'Region?', 'selection region 108, correct?', 'Work ID?', '1234, correct?', 'ID Store 123, has a goal time of 15 minutes, say ready', 'Number of Labels for ID Store 123', '4, correct?', 'Printer?', 'printer <spell>2</spell>, correct?', 'Open 03', 'Base summary?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '8', '3'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '3', '4'],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '2', '0'],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
