# Verify that if the Delivery field is set to 1 in the region configuration LUT response, the selector
# is prompted to deliver the assignment and confirm the delivery.
# Regression test case Selection | Deliver | ID 97755
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97755(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97755(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '\n')
        self.set_server_response('1,LUT Failure,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,1,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM14,1,2.0,10.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,1,0,0,\n'
                                 'B,0,4,L2,1,pre 2,A 2,post 2,S 1,5,,ITEM15,0,0.0,0.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '3!',           # Function?
                                   'yes',          # normal assignments, correct?
                                   '1',            # Region?
                                   'yes',          # selection region 1, correct?
                                   'ready',        # To receive work, say ready
                                   'ready',        # ID Store 123, has a goal time of 15 minutes, say ready
                                   'no',           # Base summary?
                                   'no',           # Pick base items?
                                   'ready',        # pre 1
                                   'ready',        # Aisle A 1
                                   'ready',        # post 1
                                   '00',           # S 1 Pick 2 ,   pick message
                                   '2',            # Quantity?
                                   '10.00',        # weight 1 of 2
                                   '123456ready',  # serial number 1 of 2
                                   '2.13',         # weight 2 of 2
                                   '654321ready',  # serial number 2 of 2
                                   'ready',        # Entries complete
                                   'ready',        # pre 2
                                   'ready',        # Aisle A 2
                                   'ready',        # post 2
                                   '00',           # S 1 Pick 5 ,   pick message
                                   '5',            # Quantity?
                                   'ready',        # Deliver to Location 1231
                                   '123',          # Confirm Delivery
                                   '234',          # wrong 123, try again
                                   '456',          # Confirm Delivery
                                   '567',          # wrong 456, try again
                                   '789',          # Confirm Delivery
                                   '678',          # wrong 789, try again
                                   '-')            # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 2 ,   pick message',
                              'Quantity?',
                              'weight 1 of 2',
                              'serial number 1 of 2',
                              'weight 2 of 2',
                              'serial number 2 of 2',
                              'Entries complete',
                              'pre 2',
                              'Aisle A 2',
                              'post 2',
                              'S 1 Pick 5 ,   pick message',
                              'Quantity?',
                              'Picking complete',
                              'Deliver to Location 1231',
                              'Confirm Delivery',
                              'wrong 123, try again',
                              'wrong 234, try again',
                              'Confirm Delivery',
                              'wrong 456, try again',
                              'wrong 567, try again',
                              'Confirm Delivery',
                              'wrong 789, try again',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '', '3', '', '10.00', '123456'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '', '3', '', '2.13', '654321'],
                                      ['prTaskLUTPicked', '1', '12345', 'L2', '5', '1', '', '4', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
