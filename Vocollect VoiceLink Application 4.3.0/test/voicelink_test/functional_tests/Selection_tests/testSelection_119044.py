from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_119044(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_119044(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '9,selection region 109,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('7,Region 107,1,0,3,1,1,1,1,0,0,1,1,0,0,1,0,3,0,1,2,XXXXX,XXX,XXXXXX,XXX,0,1,0,5,1,0,\n'
                                 '\n')
        self.set_server_response('123,0,\n'
                                 '234,0,\n'
                                 '345,0,\n'
                                 '456,0,\n'
                                 '\n')
        self.set_server_response('123,0,\n'
                                 '234,0,\n'
                                 '345,0,\n'
                                 '456,0,\n'
                                 '\n')
        self.set_server_response('123,0,\n'
                                 '234,0,\n'
                                 '345,0,\n'
                                 '456,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,12346,Store 345,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,12347,Store 567,3,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,1,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                 'B,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,,1,lot text,pick message,0,0,0,0,0,\n'
                                 ',0,5,L2,1,pre 2,A 1,post 2,S 1,3,,ITEM13,1,1.0,10.0,00,00,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                 ',0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,1,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,,1,lot text,pick message,0,0,1,0,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                 '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                 '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                 '1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                 '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('5,0000000005,56565,12347,Store 123,,O,0,0,\n'
                                 '6,0000000006,67676,12347,Store 123,,A,0,0,\n'
                                 '3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                 '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                 '1,0000000001,12121,12345,Store 123,,O,0,0,\n'
                                 '2,0000000002,23232,12345,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1234A, 0,\n'
                                 '1234B, 0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1234A, 0,\n'
                                 '1234B, 0,\n'
                                 '\n')
        self.set_server_response('1,0000000001,12121,12345,Store 123,,C,0,0,\n'
                                 '2,0000000002,23232,12345,Store 123,,O,0,0,\n'
                                 '5,0000000005,56565,12347,Store 123,,O,0,0,\n'
                                 '6,0000000006,67676,12347,Store 123,,A,0,0,\n'
                                 '3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                 '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('2,0000000002,23232,12345,Store 123,,O,0,0,\n'
                                 '5,0000000005,56565,12347,Store 123,,O,0,0,\n'
                                 '6,0000000006,67676,12347,Store 123,,A,0,0,\n'
                                 '3,0000000003,34343,12346,Store 123,,O,0,0,\n'
                                 '4,0000000004,45454,12346,Store 123,,A,0,0,\n'
                                 '1,0000000001,12121,12345,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1234A, 0,\n'
                                 '1234B, 0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,,A 1,,S 1,2,,ITEM10,0,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,4,L2,1,,A 2,,S 1,5,,ITEM12,0,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12345,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,5,L2,1,,A 2,,S 1,3,,ITEM13,0,1.0,10.0,00,00,,,Item Description,Size,UPC 13,12346,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,6,L1,1,,A 1,,S 1,3,,ITEM12,0,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,7,L1,1,,A 1,,S 1,4,,ITEM12,0,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12347,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L4,1,,A 1,post 3,S 1,2,,ITEM10,0,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12345,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L5,1,,A 3,,S 1,2,,ITEM10,0,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12347,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L5,1,,A 3,,S 1,2,,ITEM10,0,1.0,10.0,00,00,,,Item Description,Size,UPC 14,12347,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response(',0,6,L1,1,,A 1,,S 1,3,,ITEM12,0,1.0,10.0,00,00,,,Item Description,Size,UPC 12,12346,Store 123,0,0,store,,,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                 '\n')
        self.set_server_response('Location 21,02,0,1,0,0,\n'
                                 '\n')
        self.set_server_response('Location 31,03,0,1,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',             # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',              # Password?
                                   '3!',                # Function?
                                   'yes',               # normal assignments, correct?
                                   '7!',                # Region?
                                   'yes',               # selection region 107, correct?
                                   '123',               # Work ID?
                                   'yes',               # 123, correct?
                                   '234',               # Work ID?
                                   'yes',               # 234, correct?
                                   '345',               # Work ID?
                                   'yes',               # 345, correct?
                                   'no',                # Pick in reverse order?
                                   'ready',             # position 1, ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',             # position 2, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',             # position 3, ID Store 567, has a goal time of 15 minutes, say ready
                                   '12121!',            # New Container ID? Position 1
                                   'yes',               # 12121, correct?
                                   '7!',                # Printer?
                                   'yes',               # printer <spell>7</spell>, correct?
                                   '34343!',            # New Container ID? Position 2
                                   'yes',               # 34343, correct?
                                   'yes',               # printer <spell>7</spell>, correct?
                                   '56565!',            # New Container ID? Position 3
                                   'yes',               # 56565, correct?
                                   'yes',               # printer <spell>7</spell>, correct?
                                   'ready',             # pre 1
                                   'how much more',     # Aisle A 1
                                   'ready',             # Aisle A 1
                                   'ready',             # post 1
                                   '00',                # S 1 Pick 2 ,   pick message
                                   'quantity',          # lot text
                                   '1234!',             # lot text
                                   '2!',                # Quantity for this lot text
                                   'yes',               # 1 2 3 4 A
                                   '12121',             # Put 2 in 1
                                   '1.23',              # weight 1 of 2
                                   '34ready',           # serial number 1 of 2
                                   'review last',       # weight 2 of 2
                                   'ready',             # Unit 1, weight was 1.23, serial number was <spell>34</spell>, say ready
                                   '10.23',             # weight 2 of 2
                                   'override',          # 10.23 out of range
                                   'yes',               # override, correct?
                                   'review last',       # serial number 2 of 2
                                   'ready',             # Unit 1, weight was 1.23, serial number was <spell>34</spell>, say ready
                                   '35ready',           # serial number 2 of 2
                                   'ready',             # Entries complete
                                   'repeat last pick',  # S 1 Pick 5 ,   pick message
                                   '00',                # S 1 Pick 5 ,   pick message
                                   '1234!',             # lot text
                                   '3!',                # Quantity for this lot text
                                   'no',                # 1 2 3 4 A
                                   'yes',               # 1 2 3 4 B
                                   'new container',     # Put 3 in 1
                                   'yes',               # new container, correct?
                                   '23232!',            # New Container ID? Position 1
                                   'yes',               # 23232, correct?
                                   'yes',               # printer <spell>7</spell>, correct?
                                   '00',                # S 1 Pick 5 ,   pick message
                                   '123!',              # lot text
                                   '3!',                # Quantity for this lot text
                                   'no',                # 1 2 3 4 A
                                   'yes',               # 1 2 3 4 B
                                   '23232',             # Put 3 in 1
                                   'how much more',     # S 1 Pick 2 ,   pick message
                                   '00',                # S 1 Pick 2 ,   pick message
                                   'short product',     # lot text
                                   'yes',               # You picked 0 of 2 items.  Is this a short product?
                                   'how much more',     # Aisle A 1
                                   'ready',             # Aisle A 1
                                   'repeat last pick',  # S 1 Pick 2 ,   pick message
                                   '00',                # S 1 Pick 2 ,   pick message
                                   '23232',             # Put 2 in 1
                                   'skip aisle',        # Aisle A 2
                                   'yes',               # Skip aisle, correct?
                                   'ready',             # Aisle A 1
                                   '00',                # S 1 Pick 7 ,   pick message
                                   '34343',             # Put 3 in 2
                                   '56565',             # Put 4 in 3
                                   'repeat last pick',  # post 3
                                   'ready',             # post 3
                                   'ready',             # S 1 Pick 2 ,   pick message
                                   'skip slot',         # S 1 Pick 2 ,   pick message
                                   'yes',               # Skip slot, correct?
                                   'ready',             # Aisle A 3
                                   '00',                # S 1 Pick 2 ,   pick message
                                   '56565',             # Put 2 in 3
                                   'location',          # S 1 Pick 2 ,   pick message
                                   '00',                # S 1 Pick 2 ,   pick message
                                   '56565',             # Put 2 in 3
                                   'ready',             # Deliver position 1 ID Store 123 to Location 11
                                   '01',                # Confirm Delivery
                                   'ready',             # Deliver position 2 ID Store 345 to Location 21
                                   '02',                # Confirm Delivery
                                   'ready',             # Deliver position 3 ID Store 567 to Location 31
                                   '03',                # Confirm Delivery
                                   '-')                 # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 107, correct?',
                              'Work ID?',
                              '123, correct?',
                              'Work ID?',
                              '234, correct?',
                              'Work ID?',
                              '345, correct?',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'position 1, ID Store 123, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 567, has a goal time of 15 minutes, say ready',
                              'New Container ID? Position 1',
                              '12121, correct?',
                              'Printer?',
                              'printer <spell>7</spell>, correct?',
                              'New Container ID? Position 2',
                              '34343, correct?',
                              'printer <spell>7</spell>, correct?',
                              'New Container ID? Position 3',
                              '56565, correct?',
                              'printer <spell>7</spell>, correct?',
                              'pre 1',
                              'Aisle A 1',
                              '14 remaining at 4 line items',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 2 ,   pick message',
                              'lot text',
                              'total quantity is <spell>2</spell> ',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              'Put 2 in 1',
                              'weight 1 of 2',
                              'serial number 1 of 2',
                              'weight 2 of 2',
                              'Unit 1, weight was 1.23, serial number was <spell>34</spell>, say ready',
                              'weight 2 of 2',
                              '10.23 out of range',
                              'override, correct?',
                              'serial number 2 of 2',
                              'Unit 1, weight was 1.23, serial number was <spell>34</spell>, say ready',
                              'serial number 2 of 2',
                              'Entries complete',
                              'S 1 Pick 5 ,   pick message',
                              'last pick was aisle A 1,, slot S 1,, picked 2 of 2,',
                              'S 1 Pick 5 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 3 in 1',
                              'new container, correct?',
                              'New Container ID? Position 1',
                              '23232, correct?',
                              'printer <spell>7</spell>, correct?',
                              'S 1 Pick 5 ,   pick message',
                              'lot text',
                              'Quantity for this lot text',
                              '1 2 3 4 A',
                              '1 2 3 4 B',
                              'Put 3 in 1',
                              'S 1 Pick 2 ,   pick message',
                              '9 remaining at 3 line items',
                              'S 1 Pick 2 ,   pick message',
                              'lot text',
                              'You picked 0 of 2 items.  Is this a short product?',
                              'Aisle A 1',
                              '17 remaining at 5 line items',
                              'Aisle A 1',
                              'S 1 Pick 2 ,   pick message',
                              'last pick was aisle A 1,, slot S 1,, picked 3 of 5,',
                              'S 1 Pick 2 ,   pick message',
                              'Put 2 in 1',
                              'Aisle A 2',
                              'Skip aisle, correct?',
                              'Aisle A 1',
                              'S 1 Pick 7 ,   pick message',
                              'Put 3 in 2',
                              'Put 4 in 3',
                              'post 3',
                              'last pick was aisle A 1,, slot S 1,, picked 7 of 7,',
                              'post 3',
                              'S 1 Pick 2 ,   pick message',
                              'You must speak the check digit.',
                              'S 1 Pick 2 ,   pick message',
                              'Skip slot, correct?',
                              'Aisle A 3',
                              'S 1 Pick 2 ,   pick message',
                              'Put 2 in 3',
                              'S 1 Pick 2 ,   pick message',
                              'aisle A 3, slot S 1',
                              'S 1 Pick 2 ,   pick message',
                              'Put 2 in 3',
                              'Picking complete',
                              'Deliver position 1 ID Store 123 to Location 11',
                              'Confirm Delivery',
                              'Deliver position 2 ID Store 345 to Location 21',
                              'Confirm Delivery',
                              'Deliver position 3 ID Store 567 to Location 31',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '7', '3'],
                                      ['prTaskLUTRequestWork', '123', '1', '1'],
                                      ['prTaskLUTRequestWork', '234', '1', '1'],
                                      ['prTaskLUTRequestWork', '345', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '12121', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '7', '0'],
                                      ['prTaskLUTContainer', '1', '12346', '', '', '34343', '2', ''],
                                      ['prTaskLUTPrint', '1', '12346', '1', '', '7', '0'],
                                      ['prTaskLUTContainer', '1', '12347', '', '', '56565', '2', ''],
                                      ['prTaskLUTPrint', '1', '12347', '1', '', '7', '0'],
                                      ['prTaskLUTSendLot', '1234', '2', '12345', '3'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '0', '1', '3', '1234A', '1.23', '34'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '1', '3', '1234A', '10.23', '35'],
                                      ['prTaskLUTSendLot', '1234', '3', '12345', '4'],
                                      ['prTaskLUTContainer', '1', '12345', '', '1', '', '1', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '23232', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '7', '0'],
                                      ['prTaskLUTSendLot', '123', '3', '12345', '4'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '3', '0', '2', '4', '1234B', '', ''],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '0', '1', '', '4', '', '', ''],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '2', '1', '2', '3', '', '', ''],
                                      ['prTaskLUTUpdateStatus', '1', 'L2', '1', 'S'],
                                      ['prTaskLUTPicked', '1', '12346', 'L1', '3', '1', '3', '6', '', '', ''],
                                      ['prTaskLUTPicked', '1', '12347', 'L1', '4', '1', '5', '7', '', '', ''],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTUpdateStatus', '1', 'L4', '0', 'S'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTPicked', '1', '12347', 'L5', '2', '1', '5', '3', '', '', ''],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTPicked', '1', '12347', 'L5', '2', '1', '5', '3', '', '', ''],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12346'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12347'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
