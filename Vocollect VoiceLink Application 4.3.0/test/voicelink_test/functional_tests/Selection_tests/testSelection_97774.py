# Verify that the correct message is spoken if the number of assignments spoken by the operator
# is more than allowed.
# Regression test case Selection | Get Assignment Auto | ID 97774
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97774(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97774(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '5,selection region 5,0,\n'
                                 '6,selection region 6,0,\n'
                                 '7,selection region 7,0,\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,1,5,1,1,0,1,0,1,1,1,0,0,1,1,4,0,1,1,,,,,0,1,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,0,\n'
                                 '1,0,43,Wet way,2,7.00,233,00,0,0,This is not new,0,\n'
                                 '1,0,44,Red way,3,9.00,244,00,0,0,Few new,0,\n'
                                 '\n')
        self.set_server_response('N,0,402,893,1,pre 302,right 402,post 502,slot 891,150,Pack,893,1,2.56,8.71,0,,,,Item 891 Site,890,890,42,1030,0,0,0,,0,0,,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,401,897,1,pre 311,right 411,post 511,slot 890,15,Pack,992,1,7.5,11.0,0,890,,,Item 890 Site,890,890,43,1040,0,0,0,,0,0,Lot Lot,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,406,891,1,pre 303,right 403,post 503,slot 897,19,Pack,894,1,10.0,6.0,0,890,,,Item 890 Site,890,890,44,1030000,0,0,0,,0,0,Lot very,Pick Message Site 1,0,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,43,Store 3,0,O,0,0,\n'
                                 '45647,0000004444,36,44,Store 7,0,O,1,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '2',      # Region?
                                   'yes',    # selection region 2, correct?
                                   '7',      # How many assignments?
                                   '5',      # How many assignments?
                                   'ready',  # To receive work, say ready
                                   'ready',  # only 3 work IDs available, say ready
                                   'no',     # Pick in reverse order?
                                   'ready',  # position 1, ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'ready',  # position 2, ID Wet way, has a goal time of 7.0 minutes, say ready
                                   'ready',  # position 3, ID Red way, has a goal time of 9.0 minutes, say ready
                                   '-')      # pre 302

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'How many assignments?',
                              'The number of work IDs must be between 1 and 5, try again',
                              'How many assignments?',
                              'To receive work, say ready',
                              'only 3 work IDs available, say ready',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'position 1, ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'position 2, ID Wet way, has a goal time of 7.0 minutes, say ready',
                              'position 3, ID Red way, has a goal time of 9.0 minutes, say ready',
                              'pre 302')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTGetAssignment', '5', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
