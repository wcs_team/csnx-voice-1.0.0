# Verify description can be used at lot code prompt to get a list of valid lot codes.
# Regression test case Selection | Lot Tracking | ID 97625
# Generated using Selection_9.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97625(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97625(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '3,coffee,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                       '2,pallet jack,1,0,\n'
                                       '\n')
        self.set_server_response('brakes,B,0,\n'
                                       'tires,B,0,\n'
                                       'mirrors,B,0,\n'
                                       'lights,B,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                       '2,selection region 102,0,\n'
                                       '3,selection region 103,0,\n'
                                       '4,selection region 104,0,\n'
                                       '6,selection region 106,0,\n'
                                       '7,selection region 107,0,\n'
                                       '8,selection region 108,0,\n'
                                       '9,selection region 109,0,\n'
                                       '13,selection region 113,0,\n'
                                       '14,selection region 114,0,\n'
                                       '15,selection region 115,0,\n'
                                       '\n')
        self.set_server_response('2,Region 102,1,1,3,1,1,0,2,0,0,1,0,0,0,1,0,0,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,0,\n'
                                       '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,,0,\n'
                                       '1,0,12346,Store 346,2,15,R12,0,0,0,,0,\n'
                                       '1,0,12347,Store 347,3,15,R12,0,0,0,,0,\n'
                                       '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,15,,ITEM9,0,2.0,8.7,00,,,,Item Description 9,Size,UPC 14,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'B,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,2.0,8.7,00,,,,Item Description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'N,0,4,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM8,0,2.0,8.7,00,,,,Item Description 8,Size,UPC 12,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,1,0,0,\n'
                                       'N,0,5,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'B,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,2.0,8.7,00,,,,Item Description 12,Size,UPC 12,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,2.0,8.7,00,,,,Item Description 12,Size,UPC 12,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'N,0,8,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'N,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,2.0,8.7,00,,,,Item Description 10,Size,UPC 12,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'B,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,2.0,8.7,00,,,,Item Description 14,Size,UPC 14,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       'N,0,11,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '1234C, 0,\n'
                                       '\n')
        self.set_server_response('1234A, 0,\n'
                                       '1234B, 0,\n'
                                       '1234C, 0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '1!',           # Vehicle Type?
                                   'yes',          # fork lift, correct?
                                   'yes',          # brakes
                                   'yes',          # tires
                                   'yes',          # mirrors
                                   'yes',          # lights
                                   '3!',           # Function?
                                   'yes',          # normal assignments, correct?
                                   '2!',           # Region?
                                   'yes',          # selection region 102, correct?
                                   '3!',           # How many assignments?
                                   'ready',        # To receive work, say ready
                                   'ready',        # position 1, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',        # position 2, ID Store 346, has a goal time of 15 minutes, say ready
                                   'ready',        # position 3, ID Store 347, has a goal time of 15 minutes, say ready
                                   'no',           # Base summary?
                                   'no',           # Pick base items?
                                   'ready',        # pre 1
                                   'ready',        # Aisle A 1
                                   'ready',        # post 1
                                   'ready',        # S 1 Pick 15  ID Store 345 Item Description 9 pick message
                                   'description',  # lot code
                                   'ready',        # 1234A
                                   'ready',        # 1234B
                                   'ready',        # 1234C
                                   'description',  # lot code
                                   'ready',        # 1234A
                                   'stop',         # 1234B
                                   '-')            # lot code

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Safety checklist. say yes, no or numeric value to verify each check',
                              'brakes',
                              'tires',
                              'mirrors',
                              'lights',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 102, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'position 1, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 346, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 347, has a goal time of 15 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1 Pick 15 , item description 9, ID Store 345, pick message',
                              'lot code',
                              '1234A',
                              '1234B',
                              '1234C',
                              'lot code',
                              '1234A',
                              '1234B',
                              'lot code')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskLUTCoreSafetyCheck', '-1', '', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTGetAssignment', '3', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTValidLots', '1', '12345', 'L1', 'ITEM9', '3'],
                                      ['prTaskLUTValidLots', '1', '12345', 'L1', 'ITEM9', '3'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
