# Verify Review Contents command
# Regression test case Selection | Information Vocabs | ID 120391
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_120391(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_120391(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('101,Region 101,0,\r\n'
                                 '102,Region 102,0,\r\n'
                                 '103,Region 103,0,\r\n'
                                 '104,Region 104,0,\r\n'
                                 '105,Region 105,0,\r\n'
                                 '106,Region 106,0,\r\n'
                                 '107,Region 107,0,\r\n'
                                 '108,Region 108,0,\r\n'
                                 '109,Region 109,0,\r\n'
                                 '110,Region 110,0,\r\n'
                                 '111,Region 111,0,\r\n'
                                 '112,Region 112,0,\r\n'
                                 '113,Region 113,0,\r\n'
                                 '114,Region 114,0,\r\n'
                                 '115,Region 115,0,\r\n'
                                 '116,Region 115Satish,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('103,Region 103,1,1,1,1,1,0,1,0,2,1,1,1,0,0,1,-1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,1,2,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('553,0,875,1030015,1,15.00,15,       00,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('B,1,8733,965,103,,9 6 4                                             ,,9 6 4                                             ,9,,964,0,0.0,0.0,        0,964,,,Item 964 Site 1,964,964,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,8734,999,103,Building 998 Site 1                               ,9 9 8                                             ,Bay 998                                           ,9 9 8                                             ,4,,998,0,0.0,0.0,        0,998,,,Item 998 Site 1,998,998,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'B,1,8735,989,103,Building 988 Site 1                               ,9 8 8                                             ,Bay 988                                           ,9 8 8                                             ,3,,988,0,0.0,0.0,        0,988,,,Item 988 Site 1,988,988,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,8736,914,103,,9 1 3                                             ,,9 1 3                                             ,6,,913,0,0.0,0.0,        0,913,,,Item 913 Site 1,913,913,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'B,1,8737,995,103,,9 9 4                                             ,,9 9 4                                             ,6,,994,0,0.0,0.0,        0,994,,,Item 994 Site 1,994,994,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,8738,917,103,Building 916 Site 1                               ,9 1 6                                             ,Bay 916                                           ,9 1 6                                             ,9,,916,0,0.0,0.0,        0,916,,,Item 916 Site 1,916,916,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'B,1,8739,808,103,,8 0 7                                             ,,8 0 7                                             ,1,,807,0,0.0,0.0,        0,807,,,Item 807 Site 1,807,807,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,8740,843,103,,8 4 2                                             ,,8 4 2                                             ,7,,842,0,0.0,0.0,        0,842,,,Item 842 Site 1,842,842,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'B,1,8741,863,103,Building 862 Site 1                               ,8 6 2                                             ,Bay 862                                           ,8 6 2                                             ,3,,862,0,0.0,0.0,        0,862,,,Item 862 Site 1,862,862,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,8742,759,103,Building 758 Site 1                               ,7 5 8                                             ,Bay 758                                           ,7 5 8                                             ,6,,758,0,0.0,0.0,        0,758,,,Item 758 Site 1,758,758,875,1030015,15,0,15,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('396,0000000396,96,875,1030015,,O,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('96,Item 964 Site 1,9,964,2010-06-09 13:54:27.0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('96,Item 964 Site 1,9,964,2010-06-09 13:54:27.0,0,\r\n'
                                 '96,Item 998 Site 1,4,998,2010-06-09 13:56:55.0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('396,0000000396,96,875,1030015,,C,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('15,15,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('397,0000000397,97,875,1030015,,O,0,0,\r\n'
                                 '396,0000000396,96,875,1030015,,C,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('96,Item 964 Site 1,9,964,2010-06-09 13:54:27.0,0,\r\n'
                                 '96,Item 998 Site 1,4,998,2010-06-09 13:56:55.0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('97,Item 988 Site 1,3,988,2010-06-09 13:58:11.0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('398,0000000398,98,875,1030015,,O,0,0,\r\n'
                                 '397,0000000397,97,875,1030015,,O,1,0,\r\n'
                                 '396,0000000396,96,875,1030015,,C,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('97,Item 988 Site 1,3,988,2010-06-09 13:58:11.0,0,\r\n'
                                 '97,Item 913 Site 1,6,913,2010-06-09 13:58:59.0,0,\r\n'
                                 '97,Item 916 Site 1,9,916,2010-06-09 13:59:35.0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('98,Item 994 Site 1,6,994,2010-06-09 13:59:23.0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('98,Item 994 Site 1,6,994,2010-06-09 13:59:23.0,0,\r\n'
                                 '98,Item 807 Site 1,1,807,2010-06-09 14:00:09.0,0,\r\n'
                                 '98,Item 842 Site 1,7,842,2010-06-09 14:00:18.0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('15,15,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '3',                # Function?
                                   'yes',              # Normal Assignments, correct?
                                   '103',              # Region?
                                   'yes',              # Region 103, correct?
                                   'ready',            # To receive work, say ready
                                   'ready',            # ID 1030015, has a goal time of 15.0 minutes, say ready
                                   'ready',            # Open 96
                                   '1!',               # Printer?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'no',               # Base summary?
                                   'no',               # Pick base items?
                                   'ready',            # Aisle 9 6 4
                                   '964',              # 9 6 4
                                   '9',                # Pick 9 ,   Pick Message Site 1
                                   'review contents',  # Building 998 Site 1
                                   'no',               # Review contents, correct?
                                   'review contents',  # Building 998 Site 1
                                   'yes',              # Review contents, correct?
                                   'yes',              # Review contents for Container 96?
                                   'location',         # item 964 site 1 picked 9
                                   'ready',            # item 964 site 1 picked 9
                                   'ready',            # Building 998 Site 1
                                   'ready',            # Aisle 9 9 8
                                   'ready',            # Bay 998
                                   '998',              # 9 9 8
                                   '4',                # Pick 4 ,   Pick Message Site 1
                                   'review contents',  # Building 988 Site 1
                                   'yes',              # Review contents, correct?
                                   'yes',              # Review contents for Container 96?
                                   'ready',            # item 964 site 1 picked 9
                                   'location',         # item 998 site 1 picked 4
                                   'ready',            # item 998 site 1 picked 4
                                   'ready',            # Building 988 Site 1
                                   'ready',            # Aisle 9 8 8
                                   'ready',            # Bay 988
                                   'new container',    # 9 8 8
                                   'yes',              # new container, correct?
                                   'yes',              # close current container?
                                   'ready',            # Deliver to 15
                                   'ready',            # Open 97
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Building 988 Site 1
                                   'ready',            # Aisle 9 8 8
                                   'ready',            # Bay 988
                                   '988',              # 9 8 8
                                   '3',                # Pick 3 ,   Pick Message Site 1
                                   'review contents',  # Aisle 9 1 3
                                   'yes',              # Review contents, correct?
                                   '95',               # Container?
                                   '96',               # Container?
                                   'yes',              # Review contents for Container 96?
                                   'ready',            # item 964 site 1 picked 9
                                   'ready',            # item 998 site 1 picked 4
                                   'review contents',  # Aisle 9 1 3
                                   'yes',              # Review contents, correct?
                                   '97',               # Container?
                                   'yes',              # Review contents for Container 97?
                                   'ready',            # item 988 site 1 picked 3
                                   'ready',            # Aisle 9 1 3
                                   'ready',            # 9 1 3
                                   '913',              # 9 1 3
                                   '6',                # Pick 6 ,   Pick Message Site 1
                                   'ready',            # Aisle 9 9 4
                                   'new container',    # 9 9 4
                                   'yes',              # new container, correct?
                                   'no',               # close current container?
                                   'ready',            # Open 98
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Aisle 9 9 4
                                   '994',              # 9 9 4
                                   '6',                # Pick 6 ,   Pick Message Site 1
                                   '98',               # Container?
                                   'ready',            # Building 916 Site 1
                                   'ready',            # Aisle 9 1 6
                                   'ready',            # Bay 916
                                   '916',              # 9 1 6
                                   '9',                # Pick 9 ,   Pick Message Site 1
                                   '97',               # Container?
                                   'ready',            # Aisle 8 0 7
                                   'review contents',  # 8 0 7
                                   'yes',              # Review contents, correct?
                                   '97',               # Container?
                                   'yes',              # Review contents for Container 97?
                                   'ready',            # item 988 site 1 picked 3
                                   'stop',            # item 913 site 1 picked 6
                                   'review contents',  # 8 0 7
                                   'yes',              # Review contents, correct?
                                   '98',               # Container?
                                   'yes',              # Review contents for Container 98?
                                   'ready',            # item 994 site 1 picked 6
                                   '807',              # 8 0 7
                                   '1',                # Pick 1 ,   Pick Message Site 1
                                   '98',               # Container?
                                   'ready',            # Aisle 8 4 2
                                   '842',              # 8 4 2
                                   '7',                # Pick 7 ,   Pick Message Site 1
                                   '98',               # Container?
                                   'ready',            # Building 862 Site 1
                                   'ready',            # Aisle 8 6 2
                                   'ready',            # Bay 862
                                   '862',              # 8 6 2
                                   '3',                # Pick 3 ,   Pick Message Site 1
                                   '97',               # Container?
                                   'review contents',  # Building 758 Site 1
                                   'yes',              # Review contents, correct?
                                   '98',               # Container?
                                   'yes',              # Review contents for Container 98?
                                   'ready',            # item 994 site 1 picked 6
                                   'ready',            # item 807 site 1 picked 1
                                   'ready',            # item 842 site 1 picked 7
                                   'ready',            # Building 758 Site 1
                                   'ready',            # Aisle 7 5 8
                                   'ready',            # Bay 758
                                   '758',              # 7 5 8
                                   '6',                # Pick 6 ,   Pick Message Site 1
                                   '98',               # Container?
                                   'ready',            # Deliver to 15
                                   '-')                # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Normal Assignments, correct?',
                              'Region?',
                              'Region 103, correct?',
                              'To receive work, say ready',
                              'ID 1030015, has a goal time of 15.0 minutes, say ready',
                              'Open 96',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Base summary?',
                              'Pick base items?',
                              'Aisle 9 6 4',
                              '9 6 4',
                              'Pick 9 ,   Pick Message Site 1',
                              'Building 998 Site 1',
                              'Review contents, correct?',
                              'Building 998 Site 1',
                              'Review contents, correct?',
                              'Review contents for Container 96?',
                              'item 964 site 1 picked 9',
                              '964',
                              'item 964 site 1 picked 9',
                              'Review complete',
                              'Building 998 Site 1',
                              'Aisle 9 9 8',
                              'Bay 998',
                              '9 9 8',
                              'Pick 4 ,   Pick Message Site 1',
                              'Building 988 Site 1',
                              'Review contents, correct?',
                              'Review contents for Container 96?',
                              'item 964 site 1 picked 9',
                              'item 998 site 1 picked 4',
                              '998',
                              'item 998 site 1 picked 4',
                              'Review complete',
                              'Building 988 Site 1',
                              'Aisle 9 8 8',
                              'Bay 988',
                              '9 8 8',
                              'new container, correct?',
                              'close current container?',
                              'Deliver to 15',
                              'Open 97',
                              'printer <spell>1</spell>, correct?',
                              'Building 988 Site 1',
                              'Aisle 9 8 8',
                              'Bay 988',
                              '9 8 8',
                              'Pick 3 ,   Pick Message Site 1',
                              'Aisle 9 1 3',
                              'Review contents, correct?',
                              'Container?',
                              '<spell>95</spell> not valid try again',
                              'Container?',
                              'Review contents for Container 96?',
                              'item 964 site 1 picked 9',
                              'item 998 site 1 picked 4',
                              'Review complete',
                              'Aisle 9 1 3',
                              'Review contents, correct?',
                              'Container?',
                              'Review contents for Container 97?',
                              'item 988 site 1 picked 3',
                              'Review complete',
                              'Aisle 9 1 3',
                              '9 1 3',
                              'You must speak the check digit.',
                              '9 1 3',
                              'Pick 6 ,   Pick Message Site 1',
                              'Aisle 9 9 4',
                              '9 9 4',
                              'new container, correct?',
                              'close current container?',
                              'Open 98',
                              'printer <spell>1</spell>, correct?',
                              'Aisle 9 9 4',
                              '9 9 4',
                              'Pick 6 ,   Pick Message Site 1',
                              'Container?',
                              'Building 916 Site 1',
                              'Aisle 9 1 6',
                              'Bay 916',
                              '9 1 6',
                              'Pick 9 ,   Pick Message Site 1',
                              'Container?',
                              'Aisle 8 0 7',
                              '8 0 7',
                              'Review contents, correct?',
                              'Container?',
                              'Review contents for Container 97?',
                              'item 988 site 1 picked 3',
                              'item 913 site 1 picked 6',
                              '8 0 7',
                              'Review contents, correct?',
                              'Container?',
                              'Review contents for Container 98?',
                              'item 994 site 1 picked 6',
                              'Review complete',
                              '8 0 7',
                              'Pick 1 ,   Pick Message Site 1',
                              'Container?',
                              'Aisle 8 4 2',
                              '8 4 2',
                              'Pick 7 ,   Pick Message Site 1',
                              'Container?',
                              'Building 862 Site 1',
                              'Aisle 8 6 2',
                              'Bay 862',
                              '8 6 2',
                              'Pick 3 ,   Pick Message Site 1',
                              'Container?',
                              'Building 758 Site 1',
                              'Review contents, correct?',
                              'Container?',
                              'Review contents for Container 98?',
                              'item 994 site 1 picked 6',
                              'item 807 site 1 picked 1',
                              'item 842 site 1 picked 7',
                              'Review complete',
                              'Building 758 Site 1',
                              'Aisle 7 5 8',
                              'Bay 758',
                              '7 5 8',
                              'Pick 6 ,   Pick Message Site 1',
                              'Container?',
                              'Picking complete',
                              'Deliver to 15',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '103', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '553', '0', '1', '0'],
                                      ['prTaskLUTContainer', '553', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '553', '875', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '553', '875', '1', '', '1', '0'],
                                      ['prTaskODRUpdateStatus', '553', '', '2', 'N'],
                                      ['prTaskODRPicked', '553', '875', '965', '9', '1', '396', '8733', '', '', ''],
                                      ['prTaskLUTContainerReview', '396', '553', '875'],
                                      ['prTaskODRPicked', '553', '875', '999', '4', '1', '396', '8734', '', '', ''],
                                      ['prTaskLUTContainerReview', '396', '553', '875'],
                                      ['prTaskLUTContainer', '553', '875', '', '396', '', '1', ''],
                                      ['prTaskLUTGetDeliveryLocation', '553', '875'],
                                      ['prTaskLUTContainer', '553', '875', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '553', '875', '1', '', '1', '0'],
                                      ['prTaskODRPicked', '553', '875', '989', '3', '1', '397', '8735', '', '', ''],
                                      ['prTaskLUTContainerReview', '396', '553', '875'],
                                      ['prTaskLUTContainerReview', '397', '553', '875'],
                                      ['prTaskODRPicked', '553', '875', '914', '6', '1', '397', '8736', '', '', ''],
                                      ['prTaskLUTContainer', '553', '875', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '553', '875', '1', '', '1', '0'],
                                      ['prTaskODRPicked', '553', '875', '995', '6', '1', '398', '8737', '', '', ''],
                                      ['prTaskODRPicked', '553', '875', '917', '9', '1', '397', '8738', '', '', ''],
                                      ['prTaskLUTContainerReview', '397', '553', '875'],
                                      ['prTaskLUTContainerReview', '398', '553', '875'],
                                      ['prTaskODRPicked', '553', '875', '808', '1', '1', '398', '8739', '', '', ''],
                                      ['prTaskODRPicked', '553', '875', '843', '7', '1', '398', '8740', '', '', ''],
                                      ['prTaskODRPicked', '553', '875', '863', '3', '1', '397', '8741', '', '', ''],
                                      ['prTaskLUTContainerReview', '398', '553', '875'],
                                      ['prTaskODRPicked', '553', '875', '759', '6', '1', '398', '8742', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '553', '875'],
                                      ['prTaskLUTStopAssignment', '553'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
