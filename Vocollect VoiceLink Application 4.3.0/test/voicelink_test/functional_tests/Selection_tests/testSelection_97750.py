# Verify that the operator is allowed to reprint labels for open containers for multiple assignments.
# Regression test case Selection | Reprint Labels | ID 97750
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97750(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97750(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '5,selection region 5,0,\n'
                                 '6,selection region 6,0,\n'
                                 '7,selection region 7,0,\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,5,1,1,1,1,0,1,1,1,1,0,1,1,4,0,1,1,xxxxxxx,xxxxxxxxx,xxxxxxxx,xxxxxxxx,0,1,1,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,0,\n'
                                 '1,0,43,Wet way,2,7.00,233,00,0,0,This is not new,0,\n'
                                 '1,0,44,Red way,3,9.00,244,00,0,0,Few new,0,\n'
                                 '\n')
        self.set_server_response('N,0,402,893,1,pre 302,right 402,post 502,slot 891,150,Pack,893,1,2.56,8.71,0,,,,Item 891 Site,890,890,42,1030,0,0,0,,0,0,,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,401,897,1,pre 311,right 411,post 511,slot 890,15,Pack,992,1,7.5,11.0,0,890,,,Item 890 Site,890,890,43,1040,0,0,0,,0,0,Lot Lot,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,406,891,1,pre 303,right 403,post 503,slot 897,19,Pack,894,1,10.0,6.0,0,890,,,Item 890 Site,890,890,44,1030000,0,0,0,,0,0,Lot very,Pick Message Site 1,0,0,1,1,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,43,Store 3,0,O,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,43,Store 3,0,O,0,0,\n'
                                 '45647,0000004444,36,44,Store 7,0,O,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '99,Error 99\n'
                                 '98,Error 98\n'
                                 '199,Error 199\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',           # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',            # Password?
                                   '3!',              # Function?
                                   'yes',             # normal assignments, correct?
                                   '3',               # Region?
                                   'yes',             # selection region 3, correct?
                                   '3',               # How many assignments?
                                   'ready',           # To receive work, say ready
                                   'no',              # Pick in reverse order?
                                   'ready',           # position 1, ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'ready',           # position 2, ID Wet way, has a goal time of 7.0 minutes, say ready
                                   'ready',           # position 3, ID Red way, has a goal time of 9.0 minutes, say ready
                                   '34!',             # New Container ID? Position 1
                                   'yes',             # 34, correct?
                                   '5!',              # Printer?
                                   'yes',             # printer <spell>5</spell>, correct?
                                   '35!',             # New Container ID? Position 2
                                   'yes',             # 35, correct?
                                   'yes',             # printer <spell>5</spell>, correct?
                                   '36!',             # New Container ID? Position 3
                                   'yes',             # 36, correct?
                                   'yes',             # printer <spell>5</spell>, correct?
                                   'reprint labels',  # pre 302
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>5</spell>, correct?
                                   '34',              # Container?
                                   'yes',             # 34, correct?
                                   'reprint labels',  # pre 302
                                   'yes',             # reprint labels, correct? 
                                   'no',              # printer <spell>5</spell>, correct?
                                   '12',              # Printer?
                                   'yes',             # printer <spell>12</spell>, correct?
                                   '35',              # Container?
                                   'yes',             # 35, correct?
                                   'reprint labels',  # pre 302
                                   'yes',             # reprint labels, correct? 
                                   'yes',             # printer <spell>12</spell>, correct?
                                   '36',              # Container?
                                   'yes',             # 36, correct?
                                   '-')               # pre 302

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 3, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'position 1, ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'position 2, ID Wet way, has a goal time of 7.0 minutes, say ready',
                              'position 3, ID Red way, has a goal time of 9.0 minutes, say ready',
                              'New Container ID? Position 1',
                              '34, correct?',
                              'Printer?',
                              'printer <spell>5</spell>, correct?',
                              'New Container ID? Position 2',
                              '35, correct?',
                              'printer <spell>5</spell>, correct?',
                              'New Container ID? Position 3',
                              '36, correct?',
                              'printer <spell>5</spell>, correct?',
                              'pre 302',
                              'reprint labels, correct?',
                              'printer <spell>5</spell>, correct?',
                              'Container?',
                              '34, correct?',
                              'pre 302',
                              'reprint labels, correct?',
                              'printer <spell>5</spell>, correct?',
                              'Printer?',
                              'printer <spell>12</spell>, correct?',
                              'Container?',
                              '35, correct?',
                              'pre 302',
                              'reprint labels, correct?',
                              'printer <spell>12</spell>, correct?',
                              'Container?',
                              '36, correct?',
                              'pre 302')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '3', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '42', '', '', '34', '2', ''],
                                      ['prTaskLUTPrint', '1', '42', '1', '', '5', '0'],
                                      ['prTaskLUTContainer', '1', '43', '', '', '35', '2', ''],
                                      ['prTaskLUTPrint', '1', '43', '1', '', '5', '0'],
                                      ['prTaskLUTContainer', '1', '44', '', '', '36', '2', ''],
                                      ['prTaskLUTPrint', '1', '44', '1', '', '5', '0'],
                                      ['prTaskLUTPrint', '1', '42', '1', '23452', '5', '1'],
                                      ['prTaskLUTPrint', '1', '43', '1', '34563', '12', '1'],
                                      ['prTaskLUTPrint', '1', '44', '1', '45647', '12', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
