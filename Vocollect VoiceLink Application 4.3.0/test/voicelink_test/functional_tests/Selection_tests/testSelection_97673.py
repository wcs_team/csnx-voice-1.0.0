# Verify that 'How much more', 'store number' , 'repeat last pick, 'route number' are available to 
# operator when aisle location is spoken
# Regression test case Selection | Information Vocabs | ID 97673
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97673(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97673(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 1,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L3,1,,A 2,,S 3,5,,ITEM13,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L4,1,,A 2,,S 4,5,,ITEM14,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',             # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',              # Password?
                                   '3!',                # Function?
                                   'yes',               # normal assignments, correct?
                                   '1',                 # Region?
                                   'yes',               # selection region 1, correct?
                                   'ready',             # To receive work, say ready
                                   'ready',             # ID Store 111, has a goal time of 15 minutes, say ready
                                   'how much more',     # Aisle A 1
                                   'store number',      # Aisle A 1
                                   'repeat last pick',  # Aisle A 1
                                   'route number',      # Aisle A 1
                                   'ready',             # Aisle A 1
                                   '00',                # S 1 Pick 10 ,   
                                   '10',                # Quantity?
                                   'how much more',     # S 2 Pick 10 ,   
                                   'store number',      # S 2 Pick 10 ,   
                                   'repeat last pick',  # S 2 Pick 10 ,   
                                   'route number',      # S 2 Pick 10 ,   
                                   '00',                # S 2 Pick 10 ,   
                                   '5!',                # Quantity?
                                   'yes',               # You said 5 asked for 10. Is this a short product?
                                   'how much more',     # Aisle A 2
                                   '-')                 # Aisle A 2

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'To receive work, say ready',
                              'ID Store 111, has a goal time of 15 minutes, say ready',
                              'Aisle A 1',
                              '30 remaining at 6 line items',
                              'Aisle A 1',
                              's t o r e',
                              'Aisle A 1',
                              'Repeat last pick not valid.',
                              'Aisle A 1',
                              'R 1 2',
                              'Aisle A 1',
                              'S 1 Pick 10 ,   ',
                              'Quantity?',
                              'S 2 Pick 10 ,   ',
                              '20 remaining at 4 line items',
                              'S 2 Pick 10 ,   ',
                              's t o r e',
                              'S 2 Pick 10 ,   ',
                              'last pick was aisle A 1,, slot S 1,, picked 10 of 10,',
                              'S 2 Pick 10 ,   ',
                              'R 1 2',
                              'S 2 Pick 10 ,   ',
                              'Quantity?',
                              'You said 5 asked for 10. Is this a short product?',
                              'Aisle A 2',
                              '10 remaining at 2 line items',
                              'Aisle A 2')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '111', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '111', 'L2', '0', '1', '', '1', '', '', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
