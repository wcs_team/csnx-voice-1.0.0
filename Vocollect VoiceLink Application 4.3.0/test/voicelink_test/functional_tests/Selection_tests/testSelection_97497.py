# Verify single pick prompt no check digit
# Regression test case Selection | Single Pick Prompt | ID 97497
# Generated using Selection_5.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97497(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97497(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                       '2,selection region 2,0,\n'
                                       '3,selection region 3,0,\n'
                                       '4,selection region 4,0,\n'
                                       '\n')
        self.set_server_response('1,dry grocery,1,0,3,1,1,0,0,0,0,1,0,0,0,1,1,4,0,1,2,XXXXX,XXX,XXXXXX,XXX,1,0,0,2,0,0\n'
                                       '\n')
        self.set_server_response('1234,0,\n'
                                       '6789,3,\n'
                                       '7890,4,\n'
                                       '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,0,\n'
                                       '\n')
        self.set_server_response('B,0,402,893,1,pre 302,right 402,post 502,slot 891,15,Pack,893,0,0.0,0.0,0,,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0,\n'
                                       'B,0,403,894,1,pre 303,right 403,post 503,slot 893,16,Pack,894,0,0.0,0.0,0,,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0,\n'
                                       'N,1,405,895,1,pre 304,right 404,Post 504,Slot 890,1,Pack,895,0,0.0,0.0,0,,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,1,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '99,Error 99\n'
                                       '98,Error 98\n'
                                       '199,Error 199\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '3!',       # Function?
                                   'yes',      # normal assignments, correct?
                                   '1!',       # Region?
                                   'yes',      # selection region 1, correct?
                                   '1234',     # Work ID?
                                   'yes',      # 1234, correct?
                                   'no more',  # Work ID?
                                   'no',       # Pick in reverse order?
                                   'ready',    # ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'no',       # Base summary?
                                   'no',       # Pick base items?
                                   'ready',    # pre 302
                                   'ready',    # Aisle right 402
                                   'ready',    # post 502
                                   'ready',    # slot 891 Pick 15 Pack  Item 891 Site Pick Message Site 1
                                   '15',       # Quantity?
                                   '-')        # pre 303

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'Work ID?',
                              '1234, correct?',
                              'Work ID?',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'pre 302',
                              'Aisle right 402',
                              'post 502',
                              'slot 891 Pick 15 Pack, item 891 site,  Pick Message Site 1',
                              'Quantity?',
                              'pre 303')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskLUTPicked', '1', '42', '893', '15', '1', '', '402', '', '', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
