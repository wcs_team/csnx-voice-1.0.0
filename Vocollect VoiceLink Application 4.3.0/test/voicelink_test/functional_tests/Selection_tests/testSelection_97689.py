# Verify if put prompt is asked for all the Lots if enabled. 
# Single assignment with multiple open containers
# Regression test case Selection | Put Prompt | ID 97689
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97689(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97689(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,1,5,1,1,0,1,0,1,1,1,0,0,1,1,4,0,1,1,,,,,0,1,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,0,\n'
                                 '\n')
        self.set_server_response('N,0,402,893,1,pre 302,right 402,post 502,slot 891,150,Pack,893,1,2.56,8.71,0,,,,Item 891 Site,890,890,42,1030,0,0,0,,0,1,Lot Code,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,401,897,1,pre 311,right 411,post 511,slot 890,15,Pack,992,1,7.5,11.0,0,890,,,Item 890 Site,890,890,42,1040,0,0,0,,0,1,Lot Lot,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,406,891,1,pre 303,right 403,post 503,slot 897,19,Pack,894,1,10.0,6.0,0,890,,,Item 890 Site,890,890,42,1030000,0,0,0,,0,1,Lot very,Pick Message Site 1,0,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,42,Store 3,0,O,0,0,\n'
                                 '45647,0000004444,36,42,Store 7,0,O,1,0,\n'
                                 '\n')
        self.set_server_response('1234A, 0,\n'
                                 '1234B, 0,\n'
                                 '1234C, 0,\n'
                                 '1234D, 0,\n'
                                 '1234E, 0,\n'
                                 '\n')
        self.set_server_response('1234A, 0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1234A, 0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '3!',           # Function?
                                   'yes',          # normal assignments, correct?
                                   '2',            # Region?
                                   'yes',          # selection region 2, correct?
                                   '1',            # How many assignments?
                                   'ready',        # To receive work, say ready
                                   'no',           # Pick in reverse order?
                                   'ready',        # ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'ready',        # pre 302
                                   'ready',        # Aisle right 402
                                   'ready',        # post 502
                                   'ready',        # slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1
                                   '5!',           # Quantity?
                                   'yes',          # You said 5 asked for 150. Is this a short product?
                                   'description',  # Lot Code
                                   'ready',        # 1234A
                                   'ready',        # 1234B
                                   'ready',        # 1234C
                                   'ready',        # 1234D
                                   'ready',        # 1234E
                                   '#1234D',       # Lot Code
                                   '2!',           # Quantity for this Lot Code
                                   '35',           # Container?
                                   '8.71',         # weight 1 of 2
                                   '#458U',        # serial number 1 of 2
                                   '8.70',         # weight 2 of 2
                                   '#ABC123',      # serial number 2 of 2
                                   'ready',        # Entries complete
                                   'ready',        # slot 891 Pick 3 Pack, item 891 site,  Pick Message Site 1
                                   '3',            # Quantity?
                                   '1234A!',       # Lot Code
                                   '3!',           # Quantity for this Lot Code
                                   '36',           # Container?
                                   '7.75',         # weight 1 of 3
                                   '#456YYY',      # serial number 1 of 3
                                   '7.88',         # weight 2 of 3
                                   '456ready',     # serial number 2 of 3
                                   '7.99',         # weight 3 of 3
                                   '#45FG65',      # serial number 3 of 3
                                   'ready',        # Entries complete
                                   '-')            # pre 311

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'pre 302',
                              'Aisle right 402',
                              'post 502',
                              'slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1',
                              'Quantity?',
                              'You said 5 asked for 150. Is this a short product?',
                              'Lot Code',
                              '1234A',
                              '1234B',
                              '1234C',
                              '1234D',
                              '1234E',
                              'Lot Code',
                              'Quantity for this Lot Code',
                              'Container?',
                              'weight 1 of 2',
                              'serial number 1 of 2',
                              'weight 2 of 2',
                              'serial number 2 of 2',
                              'Entries complete',
                              'slot 891 Pick 3 Pack, item 891 site,  Pick Message Site 1',
                              'Quantity?',
                              'Lot Code',
                              'Quantity for this Lot Code',
                              'Container?',
                              'weight 1 of 3',
                              'serial number 1 of 3',
                              'weight 2 of 3',
                              'serial number 2 of 3',
                              'weight 3 of 3',
                              'serial number 3 of 3',
                              'Entries complete',
                              'pre 311')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTValidLots', '1', '42', '893', '893', '402'],
                                      ['prTaskLUTSendLot', '1234D', '2', '42', '402'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '34563', '402', '1234A', '8.71', '458U'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '34563', '402', '1234A', '8.70', 'ABC123'],
                                      ['prTaskLUTSendLot', '1234A', '3', '42', '402'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '45647', '402', '1234A', '7.75', '456YYY'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '45647', '402', '1234A', '7.88', '456'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '1', '45647', '402', '1234A', '7.99', '45FG65'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
