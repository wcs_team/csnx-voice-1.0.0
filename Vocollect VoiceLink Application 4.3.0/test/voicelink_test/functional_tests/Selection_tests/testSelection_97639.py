# Verify change function command
# Regression test case Selection | Global Words | ID 97639
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97639(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97639(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                 '2,pallet jack,1,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('brakes,B,0,\n'
                                 'tires,B,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,dry grocery,1,1,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,222,Store 222,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('3,new grocery,1,1,1,1,1,0,0,0,2,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,222,Store 222,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('2,frozen food,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1234A,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,222,Store 222,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Location 1,00,1011,1000,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,1,5,3,3,0,\n'
                                 '\n')
        self.set_server_response('1,123,1,123,5,13,route 123 has 5 stops 5 remaining,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   'take a break',     # Vehicle Type?
                                   'no',               # Take a break, correct?
                                   'take a break',     # Vehicle Type?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '1',                # Vehicle Type?
                                   'yes',              # fork lift, correct?
                                   'no',               # brakes
                                   'yes',              # brakes, failed, correct?
                                   'take a break',     # Your vehicle has failed, can you do a quick repair?
                                   'no',               # Take a break, correct?
                                   'take a break',     # Your vehicle has failed, can you do a quick repair?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   'yes',              # brakes
                                   'yes',              # tires
                                   'take a break',     # Function?
                                   'no',               # Take a break, correct?
                                   'take a break',     # Function?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   'take a break',     # Region?
                                   'no',               # Take a break, correct?
                                   'take a break',     # Region?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '1',                # Region?
                                   'yes',              # selection region 1, correct?
                                   'take a break',     # How many assignments?
                                   'no',               # Take a break, correct?
                                   'take a break',     # How many assignments?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '1',                # How many assignments?
                                   'take a break',     # To receive work, say ready
                                   'no',               # Take a break, correct?
                                   'take a break',     # To receive work, say ready
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '1',                # How many assignments?
                                   'ready',            # To receive work, say ready
                                   'ready',            # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',            # position 2, ID Store 222, has a goal time of 15 minutes, say ready
                                   'take a break',     # Aisle A 1
                                   'no',               # Take a break, correct?
                                   'take a break',     # Aisle A 1
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   'ready',            # Aisle A 1
                                   'take a break',     # S 1 Pick 5 ,  ID Store 111, 
                                   'no',               # Take a break, correct?
                                   'take a break',     # S 1 Pick 5 ,  ID Store 111, 
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '00',               # S 1 Pick 5 ,  ID Store 111, 
                                   'take a break',     # Quantity?
                                   'no',               # Take a break, correct?
                                   'take a break',     # Quantity?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes',              # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '5',                # Quantity?
                                   '00',               # S 1 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Aisle A 2
                                   '00',               # S 2 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Load position 1 ID Store 111 at Location 1
                                   '1000',               # Confirm Delivery
                                   'left',             # Position?
                                   'yes',              # left, correct?
                                   'ready',            # Load position 2 ID Store 222 at Location 1
                                   'ready',            # Confirm Delivery
                                   '1000',               # Confirm Delivery
                                   'right',            # Position?
                                   'yes',              # right, correct?
                                   'take a break',     # Assignment complete.  For next assignment, say ready
                                   'no',               # Take a break, correct?
                                   'take a break',     # Assignment complete.  For next assignment, say ready
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   'ready',            # Assignment complete.  For next assignment, say ready
                                   'change region',    # How many assignments?
                                   '3',                # Region?
                                   'yes',              # selection region 3, correct?
                                   'ready',            # To receive work, say ready
                                   'ready',            # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',            # position 2, ID Store 222, has a goal time of 15 minutes, say ready
                                   'ready',            # Aisle A 1
                                   'take a break',     # S 1
                                   'no',               # Take a break, correct?
                                   'take a break',     # S 1
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '00',               # S 1
                                   'take a break',     # Pick 5 , ID Store 111,  
                                   'no',               # Take a break, correct?
                                   'take a break',     # Pick 5 , ID Store 111,  
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '5',                # Pick 5 , ID Store 111,  
                                   '00',               # S 1
                                   '5',                # Pick 5 , ID Store 222,  
                                   'ready',            # Aisle A 2
                                   '00',               # S 2
                                   '5',                # Pick 5 , ID Store 222,  
                                   'ready',            # Load position 1 ID Store 111 at Location 1
                                   '1000',               # Confirm Delivery
                                   'right',            # Position?
                                   'yes',              # right, correct?
                                   'ready',            # Load position 2 ID Store 222 at Location 1
                                   '1000',               # Confirm Delivery
                                   'left',             # Position?
                                   'yes',              # left, correct?
                                   'take a break',     # Assignment complete.  For next assignment, say ready
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   '123',              # To continue work, say ready
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   'change region',    # Assignment complete.  For next assignment, say ready
                                   '2',                # Region?
                                   'yes',              # selection region 2, correct?
                                   'take a break',     # Work ID?
                                   'no',               # Take a break, correct?
                                   'take a break',     # Work ID?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes',              # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '1234',             # Work ID?
                                   'yes',              # 1234, correct?
                                   'ready',            # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',            # position 2, ID Store 222, has a goal time of 15 minutes, say ready
                                   'ready',            # Aisle A 1
                                   '00',               # S 1 Pick 5 ,  ID Store 111, 
                                   '5',                # Quantity?
                                   '00',               # S 1 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Aisle A 2
                                   '00',               # S 2 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Load position 1 ID Store 111 at Location 1
                                   '1000',               # Confirm Delivery
                                   'left',             # Position?
                                   'yes',              # left, correct?
                                   'ready',            # Load position 2 ID Store 222 at Location 1
                                   '1000',               # Confirm Delivery
                                   'right',            # Position?
                                   'yes',              # right, correct?
                                   'take a break',     # Assignment complete.  For next assignment, say ready
                                   'no',               # Take a break, correct?
                                   'take a break',     # Assignment complete.  For next assignment, say ready
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   'change function',  # Assignment complete.  For next assignment, say ready
                                   'yes',              # change function, correct?
                                   '10',               # Function?
                                   'yes',              # loading, correct?
                                   '1',                # Region?
                                   'yes',              # loading region 1, correct?
                                   'ready',            # To start loading, say ready
                                   '123ready',         # Load?
                                   'yes',              # 123, correct?
                                   'ready',            # load has 13 licenses, To continue say ready
                                   '13',               # Door 5
                                   '123',              # Trailer ID?
                                   'yes',              # 123, correct?
                                   'take a break',     # License?
                                   'no',               # Take a break, correct?
                                   'take a break',     # License?
                                   'yes',              # Take a break, correct?
                                   '1',                # Break type?
                                   'yes!',             # lunch, correct?
                                   'ready',            # To continue work, say ready
                                   '123!',             # Password?
                                   '-')                # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'take a break, correct?',
                              'Vehicle Type?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Safety checklist. say yes, no or numeric value to verify each check',
                              'brakes',
                              'brakes, failed, correct?',
                              'Your vehicle has failed, can you do a quick repair?',
                              'take a break, correct?',
                              'Your vehicle has failed, can you do a quick repair?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'brakes',
                              'tires',
                              'Function?',
                              'take a break, correct?',
                              'Function?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'take a break, correct?',
                              'Region?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Region?',
                              'selection region 1, correct?',
                              'How many assignments?',
                              'take a break, correct?',
                              'How many assignments?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'take a break, correct?',
                              'To receive work, say ready',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'position 1, ID Store 111, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 222, has a goal time of 15 minutes, say ready',
                              'Aisle A 1',
                              'take a break, correct?',
                              'Aisle A 1',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Aisle A 1',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'take a break, correct?',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'Quantity?',
                              'take a break, correct?',
                              'Quantity?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Quantity?',
                              'S 1 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Aisle A 2',
                              'S 2 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Picking complete',
                              'Load position 1 ID Store 111 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Load position 2 ID Store 222 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'right, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'take a break, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Assignment complete.  For next assignment, say ready',
                              'How many assignments?',
                              'Region?',
                              'selection region 3, correct?',
                              'To receive work, say ready',
                              'position 1, ID Store 111, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 222, has a goal time of 15 minutes, say ready',
                              'Aisle A 1',
                              'S 1',
                              'take a break, correct?',
                              'S 1',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'S 1',
                              'Pick 5 , ID Store 111,  ',
                              'take a break, correct?',
                              'Pick 5 , ID Store 111,  ',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Pick 5 , ID Store 111,  ',
                              'S 1',
                              'Pick 5 , ID Store 222,  ',
                              'Aisle A 2',
                              'S 2',
                              'Pick 5 , ID Store 222,  ',
                              'Picking complete',
                              'Load position 1 ID Store 111 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'right, correct?',
                              'Load position 2 ID Store 222 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Assignment complete.  For next assignment, say ready',
                              'Region?',
                              'selection region 2, correct?',
                              'Work ID?',
                              'take a break, correct?',
                              'Work ID?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Work ID?',
                              '1234, correct?',
                              'position 1, ID Store 111, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 222, has a goal time of 15 minutes, say ready',
                              'Aisle A 1',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'Quantity?',
                              'S 1 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Aisle A 2',
                              'S 2 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Picking complete',
                              'Load position 1 ID Store 111 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Load position 2 ID Store 222 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'right, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'take a break, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Assignment complete.  For next assignment, say ready',
                              'change function, correct?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'loading region 1, correct?',
                              'To start loading, say ready',
                              'Route?',
                              '123, correct?',
                              'route 123 has 5 stops 5 remaining, To continue say ready',
                              'Door 5',
                              'Trailer ID?',
                              '123, correct?',
                              'Container?',
                              'take a break, correct?',
                              'Container?',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Container?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTCoreSafetyCheck', '-1', '', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '111'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '222'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTStopAssignment', '1'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '111'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '222'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTStopAssignment', '1'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '111'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '222'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTStopAssignment', '1'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTLoadingRequestRoute', '123'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
