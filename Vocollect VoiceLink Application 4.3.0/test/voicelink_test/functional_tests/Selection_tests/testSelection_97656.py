# Verify correct behavior if a list of containers is returned in response to 
# the first container LUT, but none are open
# Regression test case Selection | Get Containers and Print Labels | ID 97656
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97656(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97656(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('3,Region 103,1,1,1,1,1,0,1,0,2,1,1,1,0,1,1,4,0,0,0,,,,,0,0,1,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'B,0,2,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,0,00,,,Item Description,Size,UPC 13,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,3,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,0,00,,,Item Description,Size,UPC 14,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,5,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,0,00,,,Item Description,Size,UPC 13,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,6,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,7,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,0,00,,,Item Description,Size,UPC 12,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,8,L2,1,pre 2,A 1,post 2,S 1,5,,ITEM13,0,0.0,0.0,0,00,,,Item Description,Size,UPC 13,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 'N,0,9,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,0,00,,,Item Description,Size,UPC 14,12345,Store 123,0,,store,,0,0,lot text,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('1,0000000002,02,12345,Store 123,,A,0,0,\n'
                                 '3,0000000004,04,12345,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('1,0000000003,03,12345,Store 123,,O,0,0,\n'
                                 '2,0000000004,04,12345,Store 123,,A,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '3',      # Region?
                                   'yes',    # selection region 3, correct?
                                   'ready',  # To receive work, say ready
                                   'ready',  # ID Store 123, has a goal time of 15 minutes, say ready
                                   'ready',  # Open 03
                                   '3!',     # Printer?
                                   'yes',    # printer <spell>3</spell>, correct?
                                   '-')      # Base summary?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 3, correct?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Open 03',
                              'Printer?',
                              'printer <spell>3</spell>, correct?',
                              'Base summary?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '3', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
