# Verify change function command
# Regression test case Selection | Global Words | ID 97634
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97634(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97634(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('3,new grocery,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('2,frozen food,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('2,frozen food,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('2,frozen food,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1234A,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,222,Store 222,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1,00,1011,00,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Location 1,00,1011,00,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1234A,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,222,Store 222,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1,00,1011,00,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Location 1,00,1011,00,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,1,5,3,3,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,1,5,3,3,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,1,5,3,3,0,\n'
                                 '\n')
        self.set_server_response('1,123,1,123,5,13,route 123 has 5 stops 5 remaining,0,\n'
                                 '\n')
        self.set_server_response('1,A12345,12345,N,1,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')        
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   'change function',  # Region?
                                   'no',               # change function, correct?
                                   'change function',  # Region?
                                   'yes',              # change function, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '1',                # Region?
                                   'yes',              # selection region 1, correct?
                                   'change function',  # How many assignments?
                                   'no',               # change function, correct?
                                   'change function',  # How many assignments?
                                   'yes',              # change function, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '3',                # Region?
                                   'yes',              # selection region 3, correct?
                                   'change function',  # To receive work, say ready
                                   'no',               # change function, correct?
                                   'change function',  # To receive work, say ready
                                   'yes',              # change function, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '2',                # Region?
                                   'yes',              # selection region 2, correct?
                                   'change function',  # Work ID?
                                   'no',               # change function, correct?
                                   'change function',  # Work ID?
                                   'yes',              # change function, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '2',                # Region?
                                   'yes',              # selection region 2, correct?
                                   'change function',  # Work ID?
                                   'yes',              # change function, correct?
                                   '3!',               # Function?
                                   'yes',              # normal assignments, correct?
                                   '2',                # Region?
                                   'yes',              # selection region 2, correct?
                                   '1234',             # Work ID?
                                   'yes',              # 1234, correct?
                                   'ready',            # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',            # position 2, ID Store 222, has a goal time of 15 minutes, say ready
                                   'ready',            # Aisle A 1
                                   '00',               # S 1 Pick 5 ,  ID Store 111, 
                                   '5',                # Quantity?
                                   '00',               # S 1 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Aisle A 2
                                   '00',               # S 2 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Load position 1 ID Store 111 at Location 1
                                   '00',               # Confirm Delivery
                                   'right',            # Position?
                                   'yes',              # right, correct?
                                   'ready',            # Load position 2 ID Store 222 at Location 1
                                   '00',               # Confirm Delivery
                                   'left',             # Position?
                                   'yes',              # left, correct?
                                   'change function',  # Assignment complete.  For next assignment, say ready
                                   'no',               # change function, correct?
                                   'ready',            # Assignment complete.  For next assignment, say ready
                                   '1234',             # Work ID?
                                   'yes',              # 1234, correct?
                                   'ready',            # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',            # position 2, ID Store 222, has a goal time of 15 minutes, say ready
                                   'ready',            # Aisle A 1
                                   '00',               # S 1 Pick 5 ,  ID Store 111, 
                                   '5',                # Quantity?
                                   '00',               # S 1 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Aisle A 2
                                   '00',               # S 2 Pick 5 ,  ID Store 222, 
                                   '5',                # Quantity?
                                   'ready',            # Load position 1 ID Store 111 at Location 1
                                   '00',               # Confirm Delivery
                                   'left',             # Position?
                                   'yes',              # left, correct?
                                   'ready',            # Load position 2 ID Store 222 at Location 1
                                   '00',               # Confirm Delivery
                                   'right',            # Position?
                                   'yes',              # right, correct?
                                   'change function',  # Assignment complete.  For next assignment, say ready
                                   'yes',              # change function, correct?
                                   '10',               # Function?
                                   'yes',              # loading, correct?
                                   'change function',  # Region?
                                   'no',               # change function, correct?
                                   'change function',  # Region?
                                   'yes',              # change function, correct?
                                   '10',               # Function?
                                   'yes',              # loading, correct?
                                   '1',                # Region?
                                   'yes',              # loading region 1, correct?
                                   'change function',  # To start loading, say ready
                                   'no',               # change function, correct?
                                   'change function',  # To start loading, say ready
                                   'yes',              # change function, correct?
                                   '10',               # Function?
                                   'yes',              # loading, correct?
                                   '1',                # Region?
                                   'yes',              # loading region 1, correct?
                                   'ready',            # To start loading, say ready
                                   'change function',  # Route?
                                   'no',               # change function, correct?
                                   'change function',  # Route?
                                   'yes',              # change function, correct?
                                   '10',               # Function?
                                   'yes',              # loading, correct?
                                   '1',                # Region?
                                   'yes',              # loading region 1, correct?
                                   'ready',            # To start loading, say ready
                                   '123ready',         # Route?
                                   'yes',              # 123, correct?
                                   'ready',            # route has 13 containers, To continue say ready
                                   '13',               # Door 5
                                   '123',              # Trailer ID?
                                   'yes',              # 123, correct?
                                   '12345',            # Container?
                                   'left',             # Position?
                                   'yes',              # left, correct?
                                   'complete route',   # Container?
                                   '1',                # Printer?
                                   '-')                

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'change function, correct?',
                              'Region?',
                              'change function, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'How many assignments?',
                              'change function, correct?',
                              'How many assignments?',
                              'change function, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 3, correct?',
                              'To receive work, say ready',
                              'change function, correct?',
                              'To receive work, say ready',
                              'change function, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'Work ID?',
                              'change function, correct?',
                              'Work ID?',
                              'change function, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'Work ID?',
                              'change function, correct?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'Work ID?',
                              '1234, correct?',
                              'position 1, ID Store 111, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 222, has a goal time of 15 minutes, say ready',
                              'Aisle A 1',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'Quantity?',
                              'S 1 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Aisle A 2',
                              'S 2 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Picking complete',
                              'Load position 1 ID Store 111 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'right, correct?',
                              'Load position 2 ID Store 222 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'change function, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'Work ID?',
                              '1234, correct?',
                              'position 1, ID Store 111, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 222, has a goal time of 15 minutes, say ready',
                              'Aisle A 1',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'Quantity?',
                              'S 1 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Aisle A 2',
                              'S 2 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Picking complete',
                              'Load position 1 ID Store 111 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Load position 2 ID Store 222 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'right, correct?',
                              'Assignment complete.  For next assignment, say ready',
                              'change function, correct?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'change function, correct?',
                              'Region?',
                              'change function, correct?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'loading region 1, correct?',
                              'To start loading, say ready',
                              'change function, correct?',
                              'To start loading, say ready',
                              'change function, correct?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'loading region 1, correct?',
                              'To start loading, say ready',
                              'Route?',
                              'change function, correct?',
                              'Route?',
                              'change function, correct?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'loading region 1, correct?',
                              'To start loading, say ready',
                              'Route?',
                              '123, correct?',
                              'route 123 has 5 stops 5 remaining, To continue say ready',
                              'Door 5',
                              'Trailer ID?',
                              '123, correct?',
                              'Container?',
                              'Position?',
                              'left, correct?',
                              'Container?',
                              'Printer?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '3', '3'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '111'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '222'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTStopAssignment', '1'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '111'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '222'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTStopAssignment', '1'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTLoadingRequestRoute', '123'],
                                      ['prTaskLUTLoadingRequestContainer', '12345', '1'],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', 'A12345', '', 'left', '123'],
                                      ['prTaskLUTLoadingCompleteRoute', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
