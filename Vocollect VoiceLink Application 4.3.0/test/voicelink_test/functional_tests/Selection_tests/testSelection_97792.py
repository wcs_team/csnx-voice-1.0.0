# Verify if correct flow is taken after error code 4 is returned in response to the Request Work LUT.
# Regression test case Selection | Request Assignment Manually | ID 97792
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97792(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97792(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '5,selection region 5,0,\n'
                                 '6,selection region 6,0,\n'
                                 '7,selection region 7,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,0,5,1,1,0,0,0,0,1,0,0,0,1,1,4,0,1,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1234,4,\n'
                                 '2345,4,\n'
                                 '3456,4,\n'
                                 '\n')
        self.set_server_response('1234,4,\n'
                                 '2345,4,\n'
                                 '3456,4,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '1',      # Region?
                                   'yes',    # selection region 1, correct?
                                   '1234',   # Work ID?
                                   'yes',    # 1234, correct?
                                   'yes',    # found 3 work IDs, do you want to review the list?
                                   'no',     # <spell>1234</spell>, select?
                                   'no',     # <spell>2345</spell>, select?
                                   'no',     # <spell>3456</spell>, select?
                                   'yes',    # <spell>1234</spell>, select?
                                   '-')      # found 3 work IDs, do you want to review the list?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'Work ID?',
                              '1234, correct?',
                              'found 3 work IDs, do you want to review the list?',
                              '<spell>1234</spell>, select?',
                              '<spell>2345</spell>, select?',
                              '<spell>3456</spell>, select?',
                              'End of matching work IDs, starting over.',
                              '<spell>1234</spell>, select?',
                              'found 3 work IDs, do you want to review the list?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTRequestWork', '1234', '1', '1'],
                                      ['prTaskLUTRequestWork', '1234', '0', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
