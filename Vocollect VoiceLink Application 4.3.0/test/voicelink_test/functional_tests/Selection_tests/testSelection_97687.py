# Verify Put prompt for multiple assignments, if there are 3 assignments 
# Regression test case Selection | Put Prompt | ID 97687
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97687(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97687(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,1,5,1,1,0,1,0,1,1,1,0,0,1,1,4,0,1,1,,,,,0,1,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,0,\n'
                                 '1,0,43,Wet way,2,7.00,233,00,0,0,This is not new,0,\n'
                                 '1,0,44,Red way,3,9.00,244,00,0,0,Few new,0,\n'
                                 '\n')
        self.set_server_response('N,0,402,893,1,pre 302,right 402,post 502,slot 891,150,Pack,893,1,2.56,8.71,0,,,,Item 891 Site,890,890,42,1030,0,0,0,,0,0,,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,401,897,1,pre 311,right 411,post 511,slot 890,15,Pack,992,1,7.5,11.0,0,890,,,Item 890 Site,890,890,43,1040,0,0,0,,0,0,Lot Lot,Pick Message Site 1,0,0,1,1,0,\n'
                                 'N,1,406,891,1,pre 303,right 403,post 503,slot 897,19,Pack,894,1,10.0,16.0,0,890,,,Item 890 Site,890,890,44,1030000,0,0,0,,0,0,Lot very,Pick Message Site 1,0,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('23452,0000000123,34,42,Store 2,0,O,0,0,\n'
                                 '34563,0000000567,35,43,Store 3,0,O,0,0,\n'
                                 '45647,0000004444,36,44,Store 7,0,O,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Location 1231,678,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',             # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',              # Password?
                                   '3!',                # Function?
                                   'yes',               # normal assignments, correct?
                                   '2',                 # Region?
                                   'yes',               # selection region 2, correct?
                                   '3',                 # How many assignments?
                                   'ready',             # To receive work, say ready
                                   'no',                # Pick in reverse order?
                                   'ready',             # position 1, ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'ready',             # position 2, ID Wet way, has a goal time of 7.0 minutes, say ready
                                   'ready',             # position 3, ID Red way, has a goal time of 9.0 minutes, say ready
                                   'ready',             # pre 302
                                   'ready',             # Aisle right 402
                                   'ready',             # post 502
                                   'ready',             # slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1
                                   '5!',                # Quantity?
                                   'yes',               # You said 5 asked for 150. Is this a short product?
                                   '98',                # Put 5 in 1
                                   '#765',              # Put 5 in 1
                                   '34',                # Put 5 in 1
                                   '8.71',              # weight 1 of 5
                                   '23243BC3ready',     # serial number 1 of 5
                                   '2.56',              # weight 2 of 5
                                   '#123SGSAGF3',       # serial number 2 of 5
                                   '2.57',              # weight 3 of 5
                                   '32323ready',        # serial number 3 of 5
                                   '8.70',              # weight 4 of 5
                                   '23TTT4Cready',      # serial number 4 of 5
                                   '5.55',              # weight 5 of 5
                                   '234RRRready',       # serial number 5 of 5
                                   'ready',             # Entries complete
                                   'ready',             # pre 311
                                   'ready',             # Aisle right 411
                                   'ready',             # post 511
                                   '890',               # slot 890 Pick 15 Pack, item 890 site,  Pick Message Site 1
                                   '7!',                # Quantity?
                                   'yes',               # You said 7 asked for 15. Is this a short product?
                                   '35',                # Put 7 in 2
                                   '7.50',              # weight 1 of 7
                                   'R23ready',          # serial number 1 of 7
                                   '11.99',             # weight 2 of 7
                                   'ready',             # 11.99 out of range
                                   '11.00',             # weight 2 of 7
                                   '#4569C444',         # serial number 2 of 7
                                   '7.54',              # weight 3 of 7
                                   '345ready',          # serial number 3 of 7
                                   '5.89',              # weight 4 of 7
                                   'ready',             # 5.89 out of range
                                   '8.88',              # weight 4 of 7
                                   '#345CDEDG',         # serial number 4 of 7
                                   '8.99',              # weight 5 of 7
                                   'RRRR345ready',      # serial number 5 of 7
                                   '9.99',              # weight 6 of 7
                                   '#45',               # serial number 6 of 7
                                   '10.00',             # weight 7 of 7
                                   'RRRTTTT34B7ready',  # serial number 7 of 7
                                   'ready',             # Entries complete
                                   'ready',             # pre 303
                                   'ready',             # Aisle right 403
                                   'ready',             # post 503
                                   '890',               # slot 897 Pick 19 Pack, item 890 site,  Pick Message Site 1
                                   '2!',                # Quantity?
                                   'yes',               # You said 2 asked for 19. Is this a short product?
                                   '#0000004444',       # Put 2 in 3
                                   '10.00',             # weight 1 of 2
                                   '#45ABD',            # serial number 1 of 2
                                   '11.11',             # weight 2 of 2
                                   '456Rready',         # serial number 2 of 2
                                   'ready',             # Entries complete
                                   'ready',             # Deliver position 1 ID Dry way to Location 1231
                                   '678',               # Confirm Delivery
                                   'ready',             # Deliver position 2 ID Wet way to Location 1231
                                   '678',               # Confirm Delivery
                                   'ready',             # Deliver position 3 ID Red way to Location 1231
                                   '678',               # Confirm Delivery
                                   '-')                 # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 2, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'position 1, ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'position 2, ID Wet way, has a goal time of 7.0 minutes, say ready',
                              'position 3, ID Red way, has a goal time of 9.0 minutes, say ready',
                              'pre 302',
                              'Aisle right 402',
                              'post 502',
                              'slot 891 Pick 150 Pack, item 891 site,  Pick Message Site 1',
                              'Quantity?',
                              'You said 5 asked for 150. Is this a short product?',
                              'Put 5 in 1',
                              'Wrong 98. Try again.',
                              'Put 5 in 1',
                              'Wrong 765. Try again.',
                              'Put 5 in 1',
                              'weight 1 of 5',
                              'serial number 1 of 5',
                              'weight 2 of 5',
                              'serial number 2 of 5',
                              'weight 3 of 5',
                              'serial number 3 of 5',
                              'weight 4 of 5',
                              'serial number 4 of 5',
                              'weight 5 of 5',
                              'serial number 5 of 5',
                              'Entries complete',
                              'pre 311',
                              'Aisle right 411',
                              'post 511',
                              'slot 890 Pick 15 Pack, item 890 site,  Pick Message Site 1',
                              'Quantity?',
                              'You said 7 asked for 15. Is this a short product?',
                              'Put 7 in 2',
                              'weight 1 of 7',
                              'serial number 1 of 7',
                              'weight 2 of 7',
                              '11.99 out of range',
                              'weight 2 of 7',
                              'serial number 2 of 7',
                              'weight 3 of 7',
                              'serial number 3 of 7',
                              'weight 4 of 7',
                              '5.89 out of range',
                              'weight 4 of 7',
                              'serial number 4 of 7',
                              'weight 5 of 7',
                              'serial number 5 of 7',
                              'weight 6 of 7',
                              'serial number 6 of 7',
                              'weight 7 of 7',
                              'serial number 7 of 7',
                              'Entries complete',
                              'pre 303',
                              'Aisle right 403',
                              'post 503',
                              'slot 897 Pick 19 Pack, item 890 site,  Pick Message Site 1',
                              'Quantity?',
                              'You said 2 asked for 19. Is this a short product?',
                              'Put 2 in 3',
                              'weight 1 of 2',
                              'serial number 1 of 2',
                              'weight 2 of 2',
                              'serial number 2 of 2',
                              'Entries complete',
                              'Picking complete',
                              'Deliver position 1 ID Dry way to Location 1231',
                              'Confirm Delivery',
                              'Deliver position 2 ID Wet way to Location 1231',
                              'Confirm Delivery',
                              'Deliver position 3 ID Red way to Location 1231',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTGetAssignment', '3', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '23452', '402', '', '8.71', '23243BC3'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '23452', '402', '', '2.56', '123SGSAGF3'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '23452', '402', '', '2.57', '32323'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '0', '23452', '402', '', '8.70', '23TTT4C'],
                                      ['prTaskLUTPicked', '1', '42', '893', '1', '1', '23452', '402', '', '5.55', '234RRR'],
                                      ['prTaskLUTPicked', '1', '43', '897', '1', '0', '34563', '401', '', '7.50', 'R23'],
                                      ['prTaskLUTPicked', '1', '43', '897', '1', '0', '34563', '401', '', '11.00', '4569C444'],
                                      ['prTaskLUTPicked', '1', '43', '897', '1', '0', '34563', '401', '', '7.54', '345'],
                                      ['prTaskLUTPicked', '1', '43', '897', '1', '0', '34563', '401', '', '8.88', '345CDEDG'],
                                      ['prTaskLUTPicked', '1', '43', '897', '1', '0', '34563', '401', '', '8.99', 'RRRR345'],
                                      ['prTaskLUTPicked', '1', '43', '897', '1', '0', '34563', '401', '', '9.99', '45'],
                                      ['prTaskLUTPicked', '1', '43', '897', '1', '1', '34563', '401', '', '10.00', 'RRRTTTT34B7'],
                                      ['prTaskLUTPicked', '1', '44', '891', '1', '0', '45647', '406', '', '10.00', '45ABD'],
                                      ['prTaskLUTPicked', '1', '44', '891', '1', '1', '45647', '406', '', '11.11', '456R'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '42'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '43'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '44'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
