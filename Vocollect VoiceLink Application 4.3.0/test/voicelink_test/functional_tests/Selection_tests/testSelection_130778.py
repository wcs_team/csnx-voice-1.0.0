# Test that operator is not prompted to go back for shorts if the region profile is 
# set to Never allow go back for shorts.
# Regression test case Selection | Go back for Shorts | ID 130778
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_130778(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_130778(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,3,1,1,0,0,0,1,1,0,0,0,1,1,4,0,0,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,111,Store 111,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '1,0,222,Store 222,2,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('B,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,111,Store 111,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L1,1,,A 1,,S 1,5,,ITEM11,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 'N,0,1,L2,1,,A 2,,S 2,5,,ITEM12,0,0.0,0.0,0,00,,,,,,222,Store 222,0,,store,,0,0,,,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Location 1,00,1011,00,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Location 1,00,1011,00,1,1,0,0,\n'
                                 '\n')
        self.set_server_response('R001,A00001,00001,N,1,0, \n\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '1',      # Region?
                                   'yes',    # selection region 1, correct?
                                   '1',      # How many assignments?
                                   'ready',  # To receive work, say ready
                                   'ready',  # position 1, ID Store 111, has a goal time of 15 minutes, say ready
                                   'ready',  # position 2, ID Store 222, has a goal time of 15 minutes, say ready
                                   'no',     # Base summary?
                                   'yes',    # Pick base items?
                                   'ready',  # Aisle A 1
                                   '00',     # S 1 Pick 5 ,  ID Store 111, 
                                   '2',      # Quantity?
                                   'yes',    # You said 2 asked for 5. Is this a short product?
                                   '00',     # S 1 Pick 5 ,  ID Store 222, 
                                   '5',      # Quantity?
                                   'ready',  # Aisle A 2
                                   '00',     # S 2 Pick 5 ,  ID Store 222, 
                                   '5',      # Quantity?
                                   'ready',  # Load position 1 ID Store 111 at Location 1
                                   '00',     # Confirm Delivery
                                   'left',   # Position?
                                   'yes',    # left, correct?
                                   'ready',  # Load position 2 ID Store 222 at Location 1
                                   '00',     # Confirm Delivery
                                   'right',  # Position?
                                   'yes',    # right, correct?
                                   '-')      # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'position 1, ID Store 111, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 222, has a goal time of 15 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'Aisle A 1',
                              'S 1 Pick 5 ,  ID Store 111, ',
                              'Quantity?',
                              'You said 2 asked for 5. Is this a short product?',
                              'S 1 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Aisle A 2',
                              'S 2 Pick 5 ,  ID Store 222, ',
                              'Quantity?',
                              'Picking complete',
                              'Load position 1 ID Store 111 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'left, correct?',
                              'Load position 2 ID Store 222 at 1011',
                              'Confirm Delivery',
                              'Position?',
                              'right, correct?',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRPicked', '1', '111', 'L1', '2', '1', '', '1', '', '', ''],
                                      ['prTaskODRUpdateStatus', '1', '', '2', 'N'],
                                      ['prTaskODRPicked', '1', '222', 'L1', '5', '1', '', '1', '', '', ''],
                                      ['prTaskODRPicked', '1', '222', 'L2', '5', '1', '', '1', '', '', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '111'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'left', ''],
                                      ['prTaskLUTGetDeliveryLocation', '1', '222'],
                                      ['prTaskLUTLoadingRequestContainer', '0', False],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', '0', '', 'right', ''],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
