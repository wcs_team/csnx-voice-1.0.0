# Verify that Re-pick skips updates all the skipped picks in pick list
# Regression test case Selection | Skipping | ID 97715
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97715(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97715(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '3,coffee,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '6,selection region 106,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '9,selection region 109,0,\n'
                                 '13,selection region 113,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('2,Region 102,1,1,3,1,1,1,2,0,0,1,0,0,0,1,0,0,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,,0,\n'
                                 '1,0,12346,Store 346,2,15,R12,0,0,0,,0,\n'
                                 '1,0,12347,Store 347,3,15,R12,0,0,0,,0,\n'
                                 '\n')
        self.set_server_response('S,0,3,L1,1,pre 1,A 1,post 1,S 1,15,,ITEM9,0,2.0,8.7,00,,,,Item Description 9,Size,UPC 14,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,4,L2,1,pre 2,A 2,post 2,S 2,5,,ITEM14,0,2.0,8.7,00,,,,Item Description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,4,L3,1,pre 3,A 3,post 3,S 3,1,,ITEM8,0,2.0,8.7,00,,,,Item Description 8,Size,UPC 12,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,1,0,0,\n'
                                 'N,0,5,L4,1,pre 4,A 4,post 4,S 4,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'S,0,6,L5,1,pre 5,A 5,post 5,S 5,3,,ITEM12,0,2.0,8.7,00,,,,Item Description 12,Size,UPC 12,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,2.0,8.7,00,,,,Item Description 12,Size,UPC 12,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,8,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'S,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,2.0,8.7,00,,,,Item Description 10,Size,UPC 12,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,2.0,8.7,00,,,,Item Description 14,Size,UPC 14,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'S,0,11,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',          # Password?
                                   '3!',            # Function?
                                   'yes',           # normal assignments, correct?
                                   '2!',            # Region?
                                   'yes',           # selection region 102, correct?
                                   '3',             # How many assignments?
                                   'ready',         # To receive work, say ready
                                   'ready',         # position 1, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',         # position 2, ID Store 346, has a goal time of 15 minutes, say ready
                                   'ready',         # position 3, ID Store 347, has a goal time of 15 minutes, say ready
                                   'repick skips',  # pre 2
                                   'yes',           # repick skips, correct?
                                   'repick skips',  # pre 1
                                   'yes',           # repick skips, correct?
                                   '-')             # pre 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 102, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'position 1, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 346, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 347, has a goal time of 15 minutes, say ready',
                              'pre 2',
                              'repick skips, correct?',
                              'pre 1',
                              'repick skips, correct?',
                              'no skips to repick, returning to picking',
                              'pre 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTGetAssignment', '3', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTUpdateStatus', '1', '', '2', 'N'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
