# Verify that the application speaks an appropriate message if the operator 
# picks less than the pick quantity.
# Regression test case Selection | Pick Prompt | ID 97785
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97785(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97785(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '5,selection region 5,0,\n'
                                 '6,selection region 6,0,\n'
                                 '7,selection region 7,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,1,1,1,0,0,0,0,1,0,0,0,1,1,4,0,1,0,,,,,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,42,Dry way,1,5.00,211,00,0,0,This is new,0,\n'
                                 '\n')
        self.set_server_response('B,1,400,891,1,pre 300,right 400,post 500,slot 891,23,Pack,891,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                 'N,1,401,892,1,pre 301,right 401,post 501,slot 890,11,Pack,892,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                 'B,1,402,893,1,pre 302,right 402,post 502,slot 891,22,Pack,893,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                 'B,1,403,894,1,pre 303,right 403,post 503,slot 893,13,Pack,894,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                 'N,1,405,895,1,pre 304,right 404,Post 504,Slot 890,12,Pack,895,0,0.0,0.0,0,890,,,Item 891 Site,890,890,42,1030000,0,0,0,,,0,,Pick Message Site 1,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '1',      # Region?
                                   'yes',    # selection region 1, correct?
                                   'ready',  # To receive work, say ready
                                   'no',     # Pick in reverse order?
                                   'ready',  # ID Dry way, has a goal time of 5.0 minutes, say ready
                                   'no',     # Base summary?
                                   'no',     # Pick base items?
                                   'ready',  # pre 300
                                   'ready',  # Aisle right 400
                                   'ready',  # post 500
                                   '890',    # slot 891 Pick 23 Pack,   Pick Message Site 1
                                   '7!',     # Quantity?
                                   'no',     # You said 7 asked for 23. Is this a short product?
                                   '-')      # Quantity?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'To receive work, say ready',
                              'Pick in reverse order?',
                              'receiving picks in normal order, please wait',
                              'ID Dry way, has a goal time of 5.0 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'pre 300',
                              'Aisle right 400',
                              'post 500',
                              'slot 891 Pick 23 Pack,   Pick Message Site 1',
                              'Quantity?',
                              'You said 7 asked for 23. Is this a short product?',
                              'Quantity?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskODRUpdateStatus', '1', '', '2', 'N'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
