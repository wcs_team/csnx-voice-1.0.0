##TestLink ID 95267 :: Test Case Verify if work ID can be scanned.

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_95267(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_95267(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '3,coffee,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                 '2,pallet jack,1,0,\n'
                                 '\n')
        self.set_server_response('brakes,B,0,\n'
                                 'tires,B,0,\n'
                                 'mirrors,B,0,\n'
                                 'lights,B,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '6,selection region 106,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '9,selection region 109,0,\n'
                                 '13,selection region 113,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('1,Region 101,1,0,4,1,1,0,1,0,0,1,1,1,0,0,0,4,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,0,\n'
                                 '\n')
        self.set_server_response(',0,\n'
                                 '\n')
        self.set_server_response(',0,\n'
                                 '\n')
        self.set_server_response(',0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,,0,\n'
                                 '1,0,12346,Store 346,2,15,R12,0,0,0,,0,\n'
                                 '1,0,12347,Store 347,3,15,R12,0,0,0,,0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,15,,ITEM9,0,2.0,8.7,00,,,,Item Description 9,Size,UPC 14,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'B,0,4,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,2.0,8.7,00,,,,Item Description 14,Size,UPC 14,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,4,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM8,0,2.0,8.7,00,,,,Item Description 8,Size,UPC 12,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,1,0,0,\n'
                                 'N,0,5,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'B,0,6,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM12,0,2.0,8.7,00,,,,Item Description 12,Size,UPC 12,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,7,L1,1,pre 1,A 1,post 1,S 1,4,,ITEM12,0,2.0,8.7,00,,,,Item Description 12,Size,UPC 12,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,8,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,9,L1,1,pre 1,A 1,post 1,S 1,2,,ITEM10,0,2.0,8.7,00,,,,Item Description 10,Size,UPC 12,12345,Store 345,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'B,0,10,L1,1,pre 1,A 1,post 1,S 1,3,,ITEM14,0,2.0,8.7,00,,,,Item Description 14,Size,UPC 14,12347,Store 347,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 'N,0,11,L2,1,pre 2,A 1,post 2,S 1,1,,ITEM13,0,2.0,8.7,00,,,,Item Description 13,Size,UPC 13,12346,Store 346,0,0,store,,0,1,lot code,pick message,0,0,0,1,0,\n'
                                 '\n')
        self.set_server_response(',,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('1,0000000001,01,12345,Store 345,,O,0,0,\n'
                                 '2,0000000002,02,12345,Store 345,,A,0,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '1',        # Vehicle Type?
                                   'yes',      # fork lift, correct?
                                   'yes',      # brakes
                                   'yes',      # tires
                                   'yes',      # mirrors
                                   'yes',      # lights
                                   '3!',       # Function?
                                   'yes',      # normal assignments, correct?
                                   '1!',       # Region?
                                   'yes',      # selection region 101, correct?
                                   '#12345',   # Work ID?
                                   '#1234A',   # Work ID?
                                   '#1234B',   # Work ID?
                                   'no more',  # Work ID?
                                   'ready',    # position 1, ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',    # position 2, ID Store 346, has a goal time of 15 minutes, say ready
                                   'ready',    # position 3, ID Store 347, has a goal time of 15 minutes, say ready
                                   '-')        # Open 01 Position 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Safety checklist. say yes, no or numeric value to verify each check',
                              'brakes',
                              'tires',
                              'mirrors',
                              'lights',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 101, correct?',
                              'Work ID?',
                              'Work ID?',
                              'Work ID?',
                              'Work ID?',
                              'position 1, ID Store 345, has a goal time of 15 minutes, say ready',
                              'position 2, ID Store 346, has a goal time of 15 minutes, say ready',
                              'position 3, ID Store 347, has a goal time of 15 minutes, say ready',
                              'Open 01 Position 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskLUTCoreSafetyCheck', '-1', '', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTRequestWork', '12345', '0', '1'],
                                      ['prTaskLUTRequestWork', '1234A', '0', '1'],
                                      ['prTaskLUTRequestWork', '1234B', '0', '1'],
                                      ['prTaskLUTGetAssignment', '', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTContainer', '1', '', '', '', '', '0', ''],
                                      ['prTaskLUTContainer', '1', '12345', '', '', '', '2', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
