# Verify that 'Assignment complete. For next assignment, say ready' is spoken at the completion of an assignment
# Regression test case Selection | Complete Assignment | ID 98182
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_98182(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_98182(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '3,coffee,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('1,fork lift,0,0,\n'
                                 '2,pallet jack,1,0,\n'
                                 '\n')
        self.set_server_response('brakes,B,0,\n'
                                 'tires,B,0,\n'
                                 'mirrors,B,0,\n'
                                 'lights,B,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 101,0,\n'
                                 '2,selection region 102,0,\n'
                                 '3,selection region 103,0,\n'
                                 '4,selection region 104,0,\n'
                                 '6,selection region 106,0,\n'
                                 '7,selection region 107,0,\n'
                                 '8,selection region 108,0,\n'
                                 '9,selection region 109,0,\n'
                                 '13,selection region 113,0,\n'
                                 '14,selection region 114,0,\n'
                                 '15,selection region 115,0,\n'
                                 '\n')
        self.set_server_response('2,Region 102,1,1,1,1,1,0,2,0,2,1,0,0,0,1,1,0,0,0,2,XXXXX,XXX,XXXXXX,XXX,0,0,0,2,0,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 345,1,15,R12,0,0,0,,0,\n'
                                 '\n')
        self.set_server_response('N,0,3,L1,1,pre 1,A 1,post 1,S 1,1,,ITEM9,0,0.0,0.0,00,00,,,Item Description 9,Size,UPC 14,12345,Store 345,0,0,store,,0,0,lot text,pick message,0,0,0,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Location 11,01,0,1,0,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '1',      # Vehicle Type?
                                   'yes',    # fork lift, correct?
                                   'yes',    # brakes
                                   'yes',    # tires
                                   'yes',    # mirrors
                                   'yes',    # lights
                                   '3!',     # Function?
                                   'yes',    # normal assignments, correct?
                                   '2!',     # Region?
                                   'yes',    # selection region 102, correct?
                                   'ready',  # To receive work, say ready
                                   'ready',  # ID Store 345, has a goal time of 15 minutes, say ready
                                   'ready',  # pre 1
                                   'ready',  # Aisle A 1
                                   'ready',  # post 1
                                   '00',     # S 1
                                   '1',      # Pick 1 ,  item description 9, pick message
                                   '1!',     # Printer?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Deliver to Location 11
                                   '01',     # Confirm Delivery
                                   '-')      # Assignment complete.  For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Vehicle Type?',
                              'fork lift, correct?',
                              'Safety checklist. say yes, no or numeric value to verify each check',
                              'brakes',
                              'tires',
                              'mirrors',
                              'lights',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 102, correct?',
                              'To receive work, say ready',
                              'ID Store 345, has a goal time of 15 minutes, say ready',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 1 ,  item description 9, pick message',
                              'Picking complete',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Deliver to Location 11',
                              'Confirm Delivery',
                              'Assignment complete.  For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreSendVehicleIDs', '1', ''],
                                      ['prTaskLUTCoreSafetyCheck', '-1', '', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '2', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '0', '0'],
                                      ['prTaskLUTPicked', '1', '12345', 'L1', '1', '1', '', '3', '', '', ''],
                                      ['prTaskLUTPrint', '1', '12345', '1', '', '1', '0'],
                                      ['prTaskLUTGetDeliveryLocation', '1', '12345'],
                                      ['prTaskLUTStopAssignment', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
