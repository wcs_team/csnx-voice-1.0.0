# Test that if there are multiple chase assignments in a region, all the chase assignments are picked
# before normal assignment is picked
# Regression test case Selection | Normal and Chase Assignments | ID 130782
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_130782(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_130782(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,2,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('101,Region 101,0,\r\n'
                                 '107,Region 107,0,\r\n'
                                 '115,Region 115,0,\r\n'
                                 '111,Region 111,0,\r\n'
                                 '112,Region 112,0,\r\n'
                                 '113,Region 113,0,\r\n'
                                 '102,Region 102,0,\r\n'
                                 '103,Region 103,0,\r\n'
                                 '104,Region 104,0,\r\n'
                                 '105,Region 105,0,\r\n'
                                 '106,Region 106,0,\r\n'
                                 '110,Region 110,0,\r\n'
                                 '109,Region 109,0,\r\n'
                                 '108,Region 108,0,\r\n'
                                 '114,Region 114,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('102,Region 102,2,1,1,1,1,0,0,1,0,1,0,0,0,2,0,-1,0,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,0,2,0,0,\r\n'
                                 '102,Region 102,1,1,1,1,1,0,2,0,2,1,0,0,0,0,1,-1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,0,2,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('13,1,34709,800000001,1,0.00,7,       00,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,0,34710,2201,102,Building 899 Site 1                               ,8 9 9                                             ,Bay 899                                           ,8 9 9                                             ,3,,899,0,0.0,0.0,        0,899,,,Item 899 Site 1,899,899,34709,800000001,7,0,7,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,34711,2229,102,Building 927 Site 1                               ,9 2 7                                             ,Bay 927                                           ,9 2 7                                             ,6,,927,0,0.0,0.0,        0,927,,,Item 927 Site 1,927,927,34709,800000001,7,0,7,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('14,1,34712,800000003,1,0.00,8,       00,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,0,34713,2092,102,Building 790 Site 1                               ,7 9 0                                             ,Bay 790                                           ,7 9 0                                             ,7,,790,0,0.0,0.0,        0,790,,,Item 790 Site 1,790,790,34712,800000003,8,0,8,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,,,,,2,There are no assignments available.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('15,0,5378,1020009,1,9.00,9,       00,0,0,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,0,5379,2149,102,Building 847 Site 1                               ,8 4 7                                             ,Bay 847                                           ,8 4 7                                             ,13,,847,0,0.0,0.0,        0,847,,,Item 847 Site 1,847,847,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5380,2107,102,Building 805 Site 1                               ,8 0 5                                             ,Bay 805                                           ,8 0 5                                             ,10,,805,0,0.0,0.0,        0,805,,,Item 805 Site 1,805,805,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5381,2258,102,Building 956 Site 1                               ,9 5 6                                             ,Bay 956                                           ,9 5 6                                             ,13,,956,0,0.0,0.0,        0,956,,,Item 956 Site 1,956,956,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5382,2212,102,Building 910 Site 1                               ,9 1 0                                             ,Bay 910                                           ,9 1 0                                             ,5,,910,0,0.0,0.0,        0,910,,,Item 910 Site 1,910,910,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5383,2302,102,,1 0 0 0                                           ,Bay 1000                                          ,1 0 0 0                                           ,11,,1000,0,0.0,0.0,        0,1000,,,Item 1000 Site 1,1000,1000,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5384,2259,102,Building 957 Site 1                               ,9 5 7                                             ,Bay 957                                           ,9 5 7                                             ,9,,957,0,0.0,0.0,        0,957,,,Item 957 Site 1,957,957,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5385,2053,102,Building 751 Site 1                               ,7 5 1                                             ,Bay 751                                           ,7 5 1                                             ,10,,751,0,0.0,0.0,        0,751,,,Item 751 Site 1,751,751,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5386,2223,102,Building 921 Site 1                               ,9 2 1                                             ,Bay 921                                           ,9 2 1                                             ,7,,921,0,0.0,0.0,        0,921,,,Item 921 Site 1,921,921,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5387,2070,102,Building 768 Site 1                               ,7 6 8                                             ,Bay 768                                           ,7 6 8                                             ,11,,768,0,0.0,0.0,        0,768,,,Item 768 Site 1,768,768,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 'N,0,5388,2166,102,Building 864 Site 1                               ,8 6 4                                             ,Bay 864                                           ,8 6 4                                             ,9,,864,0,0.0,0.0,        0,864,,,Item 864 Site 1,864,864,5378,1020009,9,0,9,,,0,,Pick Message Site 1,0,0,0,0,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   'yes',    # 123, correct?
                                   '6',      # Function?
                                   'yes',    # Normal And Chase Assignments, correct?
                                   '102',    # Region?
                                   'yes',    # Region 102, correct?
                                   'ready',  # To receive work, say ready
                                   'ready',  # chase assignment, ID 800000001, say ready
                                   '1!',     # Printer?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Building 899 Site 1
                                   'ready',  # Aisle 8 9 9
                                   'ready',  # Bay 899
                                   '899',    # 8 9 9 Pick 3 ,   Pick Message Site 1
                                   'ready',  # Building 927 Site 1
                                   'ready',  # Aisle 9 2 7
                                   'ready',  # Bay 927
                                   '927!',   # 9 2 7 Pick 6 ,   Pick Message Site 1
                                   'ready',  # Assignment complete.  For next assignment, say ready
                                   'ready',  # chase assignment, ID 800000003, say ready
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Building 790 Site 1
                                   'ready',  # Aisle 7 9 0
                                   'ready',  # Bay 790
                                   '790!',   # 7 9 0 Pick 7 ,   Pick Message Site 1
                                   'ready',  # Assignment complete.  For next assignment, say ready
                                   '-')      # ID 1020009, has a goal time of 9.0 minutes, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              '123, correct?',
                              'Function?',
                              'Normal And Chase Assignments, correct?',
                              'Region?',
                              'Region 102, correct?',
                              'To receive work, say ready',
                              'chase assignment, ID 800000001, say ready',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Building 899 Site 1',
                              'Aisle 8 9 9',
                              'Bay 899',
                              '8 9 9 Pick 3 ,   Pick Message Site 1',
                              'Building 927 Site 1',
                              'Aisle 9 2 7',
                              'Bay 927',
                              '9 2 7 Pick 6 ,   Pick Message Site 1',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready',
                              'chase assignment, ID 800000003, say ready',
                              'printer <spell>1</spell>, correct?',
                              'Building 790 Site 1',
                              'Aisle 7 9 0',
                              'Bay 790',
                              '7 9 0 Pick 7 ,   Pick Message Site 1',
                              'Picking complete',
                              'Assignment complete.  For next assignment, say ready',
                              'ID 1020009, has a goal time of 9.0 minutes, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '6'],
                                      ['prTaskLUTPickingRegion', '102', '6'],
                                      ['prTaskLUTGetAssignment', '1', '2'],
                                      ['prTaskLUTGetPicks', '13', '0', '0', '0'],
                                      ['prTaskLUTPrint', '13', '34709', '0', '', '1', '0'],
                                      ['prTaskODRPicked', '13', '34709', '2201', '3', '1', '', '34710', '', '', ''],
                                      ['prTaskODRPicked', '13', '34709', '2229', '6', '1', '', '34711', '', '', ''],
                                      ['prTaskLUTStopAssignment', '13'],
                                      ['prTaskLUTGetAssignment', '1', '2'],
                                      ['prTaskLUTGetPicks', '14', '0', '0', '0'],
                                      ['prTaskLUTPrint', '14', '34712', '0', '', '1', '0'],
                                      ['prTaskODRPicked', '14', '34712', '2092', '7', '1', '', '34713', '', '', ''],
                                      ['prTaskLUTStopAssignment', '14'],
                                      ['prTaskLUTGetAssignment', '1', '2'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '15', '0', '1', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
