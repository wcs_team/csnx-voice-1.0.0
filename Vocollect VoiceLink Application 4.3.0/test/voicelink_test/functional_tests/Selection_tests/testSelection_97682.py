# Verify that Quantity value is returned correctly when operator speaks
# 'Quantity' looping through a Lot
# Regression test case Selection | Information Vocabs | ID 97682
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSelection_97682(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSelection_97682(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('Vocollect,123,0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,selection region 1,0,\n'
                                 '2,selection region 2,0,\n'
                                 '3,selection region 3,0,\n'
                                 '4,selection region 4,0,\n'
                                 '\n')
        self.set_server_response('1,dry grocery,1,1,2,1,1,0,0,0,2,1,0,0,0,1,1,4,1,0,0,XXXXX,XXX,XXXXXX,XXX,1,0,0,2,0,\n'
                                 '\n')
        self.set_server_response('1,0,12345,Store 123,1,15,R12,0,0,0,Override summary prompt,0,\n'
                                 '\n')
        self.set_server_response('B,0,1,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM12,0,0.0,0.0,00,00,,,Item Description,Size,UPC,12345,Store 123,0,0,,,,1,,pick message,0,0,0,0,0,\n'
                                 'N,0,3,L1,1,pre 1,A 1,post 1,S 1,5,,ITEM14,0,0.0,0.0,00,00,,,Item Description,Size,UPC,12345,Store 123,0,0,store new,,35,1,Lot text 1234,pick message,0,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('1234A, 0,\n'
                                 '1234B, 0,\n'
                                 '\n')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',             # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',              # Password?
                                   '3!',                # Function?
                                   'yes',               # normal assignments, correct?
                                   '1',                 # Region?
                                   'yes',               # selection region 1, correct?
                                   '1',                 # How many assignments?
                                   'ready',             # To receive work, say ready
                                   'ready',             # ID Store 123, has a goal time of 15 minutes, say ready
                                   'no',                # Base summary?
                                   'yes',               # Pick base items?
                                   'ready',             # pre 1
                                   'ready',             # Aisle A 1
                                   'ready',             # post 1
                                   '00',                # S 1
                                   '5',                 # Pick 5 ,   pick message
                                   '01!',               # Lot Number
                                   '3!',                # Quantity for this Lot Number
                                   'yes',               # 1 2 3 4 A
                                   'quantity',          # Pick 2 ,   pick message
                                   'how much more',     # Pick 2 ,   pick message
                                   'repeat last pick',  # Pick 2 ,   pick message
                                   '-')                 # Pick 2 ,   pick message

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'normal assignments, correct?',
                              'Region?',
                              'selection region 1, correct?',
                              'How many assignments?',
                              'To receive work, say ready',
                              'ID Store 123, has a goal time of 15 minutes, say ready',
                              'Base summary?',
                              'Pick base items?',
                              'pre 1',
                              'Aisle A 1',
                              'post 1',
                              'S 1',
                              'Pick 5 ,   pick message',
                              'Lot Number',
                              'Quantity for this Lot Number',
                              '1 2 3 4 A',
                              'Pick 2 ,   pick message',
                              'total quantity is <spell>2</spell> ',
                              'Pick 2 ,   pick message',
                              '7 remaining at 2 line items',
                              'Pick 2 ,   pick message',
                              'Repeat last pick not valid.',
                              'Pick 2 ,   pick message')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'],
                                      ['prTaskLUTPickingRegion', '1', '3'],
                                      ['prTaskLUTGetAssignment', '1', '1'],
                                      ['prTaskLUTGetPicks', '1', '0', '1', '0'],
                                      ['prTaskLUTSendLot', '01', '3', '12345', '1'],
                                      ['prTaskODRPicked', '1', '12345', 'L1', '3', '0', '', '1', '1234A', '', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
