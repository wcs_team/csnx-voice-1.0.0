##TestLink ID 109644 :: Test Case Verify overpacking application flow

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109644(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109644(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,1,1,1,4,0,0,3,0,4,4,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002033,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('546,5,5,31,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,626,10,2002033,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,1,        0,1000,Item 1000 Site 1,eaches,1,0,0,\r\n'
                                 'N,627,11,2002033,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,14,        0,1001,Item 1001 Site 1,eaches,1,1,0,\r\n'
                                 'N,628,10,2002033,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,11,        0,1012,Item 1012 Site 1,cases,0,12,0,\r\n'
                                 'N,629,12,2002033,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,4,        0,1000,Item 1000 Site 1,eaches,0,0,0,\r\n'
                                 'N,630,8,2002033,,1007                                              ,,1007                                              ,1007,1007,1007,13,        0,1013,Item 1013 Site 1,,1,13,0,\r\n'
                                 'N,631,9,2002033,,1008                                              ,,1008                                              ,1008,1008,1008,2,        0,1005,Item 1005 Site 1,boxes,1,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('12,0000000144,144,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002033,1001,Item 1001 Site 1,0,N,0,\r\n'
                                 '2002033,1013,Item 1013 Site 1,0,N,0,\r\n'
                                 '2002033,1000,Item 1000 Site 1,0,N,0,\r\n'
                                 '2002033,1012,Item 1012 Site 1,12,N,0,\r\n'
                                 '2002033,1005,Item 1005 Site 1,5,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '8',              # Function?
                                   'yes',            # Put To Store, correct?
                                   '2!',             # Region?
                                   'yes',            # Region 2, correct?
                                   'ready',          # To start Put to store work, say ready
                                   '2033',           # License?
                                   'ready',          # Assignment has 5 slot locations 5 items, and a total expected residual of 31, say ready
                                   'ready',          # Building 1000 Site 1
                                   'ready',          # Aisle <spell>1009</spell>
                                   'ready',          # Bay 1000
                                   '1009',           # 1009
                                   '1000',           # wrong 1009, try again
                                   '2!',             # item 1000 site 1, Put 1, eaches
                                   'yes',            # Is this an overpack?
                                   '1!',             # item 1000 site 1, Put 1, eaches
                                   'ready',          # Aisle <spell>1010</spell>
                                   'ready',          # Bay 1000
                                   '1000',           # 1010
                                   '16!',            # item 1001 site 1, Put 14, eaches
                                   'yes',            # Is this an overpack?
                                   '15!',            # item 1001 site 1, Put 14, eaches
                                   'yes',            # Is this an overpack?
                                   '135',            # Container?
                                   'yes',            # 135, correct?
                                   'ready',          # Aisle <spell>1009</spell>
                                   'ready',          # Bay 1000
                                   '1009',           # 1009
                                   '1000',           # wrong 1009, try again
                                   '20!',            # item 1012 site 1, Put 11, cases
                                   'yes',            # Is this an overpack?
                                   '12!',            # item 1012 site 1, Put 11, cases
                                   'yes',            # Is this an overpack?
                                   '11!',            # item 1012 site 1, Put 11, cases
                                   'ready',          # Aisle <spell>1011</spell>
                                   'ready',          # Bay 1000
                                   '1000',           # 1011
                                   '4!',             # item 1000 site 1, Put 4, eaches
                                   'new container',  # Container?
                                   'yes',            # new container, correct?
                                   '1!',             # Printer?
                                   'yes',            # printer <spell>1</spell>, correct?
                                   'ready',          # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',          # Building 1000 Site 1
                                   'ready',          # Aisle <spell>1011</spell>
                                   'ready',          # Bay 1000
                                   '1000',           # 1011
                                   '4!',             # item 1000 site 1, Put 4, eaches
                                   '134',            # Container?
                                   'yes',            # 134, correct?
                                   'ready',          # Aisle <spell>1007</spell>
                                   '1007',           # 1007
                                   '27!',            # item 1013 site 1, Put 13, 
                                   'yes',            # Is this an overpack?
                                   '26!',            # item 1013 site 1, Put 13, 
                                   'yes',            # Is this an overpack?
                                   'ready',          # Aisle <spell>1008</spell>
                                   '1008',           # 1008
                                   '2!',             # item 1005 site 1, Put 2, boxes
                                   'no',             # Assignment complete.  Do you have residuals?
                                   '-')              # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'Getting work.',
                              'Assignment has 5 slot locations 5 items, and a total expected residual of 31, say ready',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1000 site 1, Put 1, eaches',
                              'Is this an overpack?',
                              'Cannot overpack. There are no residuals for this item.',
                              'item 1000 site 1, Put 1, eaches',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'item 1001 site 1, Put 14, eaches',
                              'Is this an overpack?',
                              'Can only overpack 1 items.',
                              'item 1001 site 1, Put 14, eaches',
                              'Is this an overpack?',
                              'Container?',
                              '135, correct?',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1012 site 1, Put 11, cases',
                              'Is this an overpack?',
                              'Overpacking not allowed.',
                              'item 1012 site 1, Put 11, cases',
                              'Is this an overpack?',
                              'Overpacking not allowed.',
                              'item 1012 site 1, Put 11, cases',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'item 1000 site 1, Put 4, eaches',
                              'Container?',
                              'new container, correct?',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'item 1000 site 1, Put 4, eaches',
                              'Container?',
                              '134, correct?',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'item 1013 site 1, Put 13, ',
                              'Is this an overpack?',
                              'Can only overpack 13 items.',
                              'item 1013 site 1, Put 13, ',
                              'Is this an overpack?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1005 site 1, Put 2, boxes',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '2033', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '546'],
                                      ['prTaskLUTPtsPut', '546', '10', '1000', '626', '1', '', '2002033', '0'],
                                      ['prTaskLUTPtsPut', '546', '11', '1001', '627', '15', '', '2002033', '0'],
                                      ['prTaskLUTPtsPut', '546', '11', '1001', '627', '15', '135', '2002033', '0'],
                                      ['prTaskLUTPtsPut', '546', '10', '1012', '628', '11', '', '2002033', '0'],
                                      ['prTaskLUTPtsPut', '546', '12', '1000', '629', '4', '', '2002033', '0'],
                                      ['prTaskLUTPtsContainer', '2', '12', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '546', '1', '12', '0000000144'],
                                      ['prTaskLUTPtsPut', '546', '12', '1000', '629', '4', '', '2002033', '0'],
                                      ['prTaskLUTPtsPut', '546', '12', '1000', '629', '4', '134', '2002033', '0'],
                                      ['prTaskLUTPtsPut', '546', '8', '1013', '630', '26', '', '2002033', '0'],
                                      ['prTaskLUTPtsPut', '546', '9', '1005', '631', '2', '', '2002033', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '546'],
                                      ['prTaskLUTPtsStopAssignment', '546'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
