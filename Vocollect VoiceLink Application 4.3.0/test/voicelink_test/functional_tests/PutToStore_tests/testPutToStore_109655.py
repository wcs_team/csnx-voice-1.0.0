##TestLink ID 109655 :: Test Case Verify item description not spoken on put if only one item

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109655(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109655(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,1,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003011,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003021,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003031,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003041,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003051,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003031,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('387,6,1,0,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,311,1,3003031,,1000                                              ,Bay 1000                                          ,1000                                              ,1000,1000,1000000000,12,        0,1013,Item 1013 Site 1,cases,0,0,0,\r\n'
                                 'N,312,6,3003031,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,16,        0,1013,Item 1013 Site 1,,1,0,0,\r\n'
                                 'N,313,9,3003031,,1008                                              ,,1008                                              ,1008,1008,1008,16,        0,1013,Item 1013 Site 1,cases,0,0,0,\r\n'
                                 'N,314,4,3003031,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,10,        0,1013,Item 1013 Site 1,eaches,0,0,0,\r\n'
                                 'N,315,2,3003031,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,16,        0,1013,Item 1013 Site 1,boxes,0,0,0,\r\n'
                                 'N,316,3,3003031,,1002                                              ,Bay 1002                                          ,1002                                              ,,1002,2222222222,10,        0,1013,Item 1013 Site 1,boxes,1,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003031,1013,Item 1013 Site 1,0,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '3!',       # Region?
                                   'yes',      # Region 3, correct?
                                   'ready',    # To start Put to store work, say ready
                                   'ready',    # Go to flow through location
                                   '1',        # License?
                                   'yes',      # 1, correct?
                                   'yes',      # Found 5 licenses. Do you want to review the list?
                                   'no',       # 3003011, select?
                                   'no',       # 3003021, select?
                                   'yes',      # 3003031, select?
                                   'no more',  # License?
                                   'ready',    # Assignment has 6 slot locations 1 item, say ready
                                   'ready',    # Aisle 1000
                                   'ready',    # Bay 1000
                                   '1000',     # 1000
                                   '12!',      # Put 12, cases
                                   'ready',    # Building 1005 Site 1
                                   'ready',    # Aisle 1005
                                   '1005',     # 1005
                                   '16!',      # Put 16, 
                                   '3',        # Container?
                                   'yes',      # 3, correct?
                                   'ready',    # Aisle 1008
                                   '1008',     # 1008
                                   '16!',      # Put 16, cases
                                   '6',        # Container?
                                   'yes',      # 6, correct?
                                   'ready',    # Building 1003 Site 1
                                   'ready',    # Aisle 1003
                                   '1003',     # 1003
                                   '10!',      # Put 10, eaches
                                   'ready',    # Aisle 1001
                                   'ready',    # Bay 1001
                                   '1001',     # 1001
                                   '16!',      # Put 16, boxes
                                   '4',        # Container?
                                   'yes',      # 4, correct?
                                   'ready',    # Aisle 1002
                                   'ready',    # Bay 1002
                                   '1002',     # 1002
                                   'ready',    # 1002
                                   '10!',      # Put 10, boxes
                                   'no',       # Assignment complete.  Do you have residuals?
                                   '-')        # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '1, correct?',
                              'Found 5 licenses. Do you want to review the list?',
                              '3003011, select?',
                              '3003021, select?',
                              '3003031, select?',
                              'License?',
                              'Getting work.',
                              'Assignment has 6 slot locations 1 item, say ready',
                              'Aisle <spell>1000</spell>',
                              'Bay 1000',
                              '1000',
                              'Put 12, cases',
                              'Building 1005 Site 1',
                              'Aisle <spell>1005</spell>',
                              '1005',
                              'Put 16, ',
                              'Container?',
                              '3, correct?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'Put 16, cases',
                              'Container?',
                              '6, correct?',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'Put 10, eaches',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Put 16, boxes',
                              'Container?',
                              '4, correct?',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 10, boxes',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '1', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003031', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '387'],
                                      ['prTaskLUTPtsPut', '387', '1', '1013', '311', '12', '', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '6', '1013', '312', '16', '', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '6', '1013', '312', '16', '3', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '9', '1013', '313', '16', '', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '9', '1013', '313', '16', '6', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '4', '1013', '314', '10', '', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '2', '1013', '315', '16', '', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '2', '1013', '315', '16', '4', '3003031', '0'],
                                      ['prTaskLUTPtsPut', '387', '3', '1013', '316', '10', '', '3003031', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '387'],
                                      ['prTaskLUTPtsStopAssignment', '387'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
