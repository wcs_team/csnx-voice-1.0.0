##TestLink ID 109652 :: Test Case Verify operator cannot overpack after exhausting residual quantity

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109652(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109652(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,0,1,1,4,0,0,3,1,0,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001025,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('550,4,4,17,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,580,3,1001025,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,12,        0,1001,Item 1001 Site 1,,1,1,0,\r\n'
                                 'N,581,3,1001025,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,13,        0,1005,Item 1005 Site 1,eaches,1,5,0,\r\n'
                                 'N,582,3,1001025,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,10,        0,1009,Item 1009 Site 1,boxes,0,9,0,\r\n'
                                 'N,583,12,1001025,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,12,        0,1002,Item 1002 Site 1,,0,2,0,\r\n'
                                 'N,584,6,1001025,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,1,        0,1002,Item 1002 Site 1,,1,2,0,\r\n'
                                 'N,585,4,1001025,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,10,        0,1001,Item 1001 Site 1,,1,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('12,0000000129,129,0,\r\n'
                                 '12,0000000134,134,0,\r\n'
                                 '12,0000000144,144,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001025,1002,Item 1002 Site 1,2,N,0,\r\n'
                                 '1001025,1001,Item 1001 Site 1,0,N,0,\r\n'
                                 '1001025,1005,Item 1005 Site 1,5,N,0,\r\n'
                                 '1001025,1009,Item 1009 Site 1,9,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',              # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',               # Password?
                                   '8',                  # Function?
                                   'yes',                # Put To Store, correct?
                                   '1!',                 # Region?
                                   'yes',                # Region 1, correct?
                                   'ready',              # To start Put to store work, say ready
                                   'ready',              # Go to flow through location
                                   '1025',               # License?
                                   'ready',              # Assignment has 4 slot locations 4 items, and a total expected residual of 17, say ready
                                   'ready',              # Aisle <spell>1002</spell>
                                   'ready',              # Bay 1002
                                   '1002',               # 1002
                                   '13!',                # item 1001 site 1, Put 12, 
                                   'yes',                # Is this an overpack?
                                   '1002',               # 1002
                                   '13!',                # item 1005 site 1, Put 13, eaches
                                   '1002',               # 1002
                                   '10!',                # item 1009 site 1, Put 10, boxes
                                   'ready',              # Building 1000 Site 1
                                   'ready',              # Aisle <spell>1011</spell>
                                   'ready',              # Bay 1000
                                   '1000',               # 1011
                                   '12!',                # item 1002 site 1, Put 12, 
                                   'review containers',  # Container?
                                   'no',                 # review containers, correct?
                                   'review containers',  # Container?
                                   'yes',                # review containers, correct?
                                   'ready',              # 0 0 0 0 0 0 0 1 2 9
                                   'talkman help',       # 0 0 0 0 0 0 0 1 3 4
                                   'stop',               # 0 0 0 0 0 0 0 1 3 4
                                   '134',                # 0 0 0 0 0 0 0 1 3 4
                                   'ready',              # 0 0 0 0 0 0 0 1 3 4
                                   'cancel',             # 0 0 0 0 0 0 0 1 4 4
                                   '144',                # Container?
                                   'yes',                # 144, correct?
                                   'ready',              # Building 1005 Site 1
                                   'ready',              # Aisle <spell>1005</spell>
                                   '1005',               # 1005
                                   '1!',                 # item 1002 site 1, Put 1, 
                                   '456',                # Container?
                                   'yes',                # 456, correct?
                                   'ready',              # Building 1003 Site 1
                                   'ready',              # Aisle <spell>1003</spell>
                                   '1003',               # 1003
                                   '11!',                # item 1001 site 1, Put 10, 
                                   'yes',                # Is this an overpack?
                                   '10!',                # item 1001 site 1, Put 10, 
                                   'no',                 # Assignment complete.  Do you have residuals?
                                   '-')                  # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              'Getting work.',
                              'Assignment has 4 slot locations 4 items, and a total expected residual of 17, say ready',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'item 1001 site 1, Put 12, ',
                              'Is this an overpack?',
                              '1002',
                              'item 1005 site 1, Put 13, eaches',
                              '1002',
                              'item 1009 site 1, Put 10, boxes',
                              'Building 1000 Site 1',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'item 1002 site 1, Put 12, ',
                              'Container?',
                              'review containers, correct?',
                              'Container?',
                              'review containers, correct?',
                              '0 0 0 0 0 0 0 1 2 9',
                              '0 0 0 0 0 0 0 1 3 4',
                              '[standard help]',
                              '0 0 0 0 0 0 0 1 3 4',
                              '0 0 0 0 0 0 0 1 4 4',
                              'Container?',
                              '144, correct?',
                              'Building 1005 Site 1',
                              'Aisle <spell>1005</spell>',
                              '1005',
                              'item 1002 site 1, Put 1, ',
                              'Container?',
                              '456, correct?',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'item 1001 site 1, Put 10, ',
                              'Is this an overpack?',
                              'Cannot overpack. There are no residuals for this item.',
                              'item 1001 site 1, Put 10, ',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1025', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '550'],
                                      ['prTaskLUTPtsPut', '550', '3', '1001', '580', '13', '', '1001025', '0'],
                                      ['prTaskLUTPtsPut', '550', '3', '1005', '581', '13', '', '1001025', '0'],
                                      ['prTaskLUTPtsPut', '550', '3', '1009', '582', '10', '', '1001025', '0'],
                                      ['prTaskLUTPtsPut', '550', '12', '1002', '583', '12', '', '1001025', '0'],
                                      ['prTaskLUTPtsContainer', '0', '12', ''],
                                      ['prTaskLUTPtsPut', '550', '12', '1002', '583', '12', '144', '1001025', '0'],
                                      ['prTaskLUTPtsPut', '550', '6', '1002', '584', '1', '', '1001025', '0'],
                                      ['prTaskLUTPtsPut', '550', '6', '1002', '584', '1', '456', '1001025', '0'],
                                      ['prTaskLUTPtsPut', '550', '4', '1001', '585', '10', '', '1001025', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '550'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=1001025'],
                                      ['prTaskLUTPtsStopAssignment', '550'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
