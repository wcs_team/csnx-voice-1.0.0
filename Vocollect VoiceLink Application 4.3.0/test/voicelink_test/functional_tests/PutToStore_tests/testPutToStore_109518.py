##TestLink ID 109518 :: Test Case Test error message when license not found

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109518(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109518(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,1,1,1,0,1,1,3,1,0,2,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',1061,License 1009999 not found\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001004,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('385,6,5,33,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '1!',       # Region?
                                   'yes',      # Region 1, correct?
                                   'ready',    # To start Put to store work, say ready
                                   'ready',    # Go to flow through location
                                   '1009999',  # License?
                                   'ready',    # License?
                                   'yes',      # 1009999, correct?
                                   'ready',    # License 1009999 not found, To continue say ready
                                   '1001004',  # License?
                                   'ready',    # License?
                                   'yes',      # 1001004, correct?
                                   '-')        # Assignment has 6 slot locations 5 items, and a total expected residual of 33, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '1009999, correct?',
                              'License 1009999 not found, To continue say ready',
                              'License?',
                              '1001004, correct?',
                              'Getting work.',
                              'Assignment has 6 slot locations 5 items, and a total expected residual of 33, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1009999', '0'],
                                      ['prTaskLUTPtsVerifyLicense', '1001004', '0'],
                                      ['prTaskLUTPtsGetAssignment'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
