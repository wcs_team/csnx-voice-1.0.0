##TestLink ID 109860 :: Test Case Verify Review Containers can be spoken at Slot prompt
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_119639(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_119639(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,0,1,1,4,0,0,3,1,0,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001019,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('530,5,6,61,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,547,3,1001019,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,15,        0,1013,Item 1013 Site 1,,1,13,0,\r\n'
                                 'N,548,10,1001019,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,14,        0,1010,Item 1010 Site 1,eaches,0,10,0,\r\n'
                                 'N,549,4,1001019,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,12,        0,1008,Item 1008 Site 1,eaches,1,8,0,\r\n'
                                 'N,550,12,1001019,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,4,        0,1004,Item 1004 Site 1,,0,4,0,\r\n'
                                 'N,551,8,1001019,,1007                                              ,,1007                                              ,1007,1007,1007,2,        0,1014,Item 1014 Site 1,boxes,1,14,0,\r\n'
                                 'N,552,8,1001019,,1007                                              ,,1007                                              ,1007,1007,1007,12,        0,1012,Item 1012 Site 1,cases,1,12,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,036,036,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',              # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',               # Password?
                                   '8',                  # Function?
                                   'yes',                # Put To Store, correct?
                                   '1!',                 # Region?
                                   'yes',                # Region 1, correct?
                                   'ready',              # To start Put to store work, say ready
                                   'ready',              # Go to flow through location
                                   '1019',               # License?
                                   'ready',              # Assignment has 5 slot locations 6 items, and a total expected residual of 61, say ready
                                   'ready',              # Aisle <spell>1002</spell>
                                   'ready',              # Bay 1002
                                   'talkman help',       # 1002
                                   'review containers',  # 1002
                                   'yes',                # review containers, correct?
                                   'talkman help',       # 0 3 6
                                   'ready',              # 0 3 6
                                   '1002',               # 1002
                                   '15!',                # item 1013 site 1, Put 15, 
                                   '-')                  # Building 1000 Site 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              'Getting work.',
                              'Assignment has 5 slot locations 6 items, and a total expected residual of 61, say ready',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'speak or scan the check digits or say review containers, skip slot, repick skips, description, license, pass assignment, item number, location,  or quantity',
                              '1002',
                              'review containers, correct?',
                              '0 3 6',
                              '[standard help]',
                              '0 3 6',
                              '1002',
                              'item 1013 site 1, Put 15, ',
                              'Building 1000 Site 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1019', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '530'],
                                      ['prTaskLUTPtsContainer', '0', '3', ''],
                                      ['prTaskLUTPtsPut', '530', '3', '1013', '547', '15', '', '1001019', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
