##TestLink ID 109624 :: Test Case Test when assignment has no residual/user says yes to residual

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109624(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109624(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,1,1,1,0,1,1,3,1,0,2,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001055,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('362,6,1,0,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,120,2,1001055,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,11,        0,1009,Item 1009 Site 1,boxes,0,0,0,\r\n'
                                 'N,121,12,1001055,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,5,        0,1009,Item 1009 Site 1,eaches,1,0,0,\r\n'
                                 'N,122,10,1001055,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,11,        0,1009,Item 1009 Site 1,eaches,1,0,0,\r\n'
                                 'N,123,6,1001055,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,16,        0,1009,Item 1009 Site 1,,1,0,0,\r\n'
                                 'N,124,1,1001055,,1000                                              ,Bay 1000                                          ,1000                                              ,1000,1000,1000000000,7,        0,1009,Item 1009 Site 1,eaches,1,0,0,\r\n'
                                 'N,125,8,1001055,,1007                                              ,,1007                                              ,1007,1007,1007,13,        0,1009,Item 1009 Site 1,cases,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001055,1009,Item 1009 Site 1,0,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '1!',       # Region?
                                   'yes',      # Region 1, correct?
                                   'ready',    # To start Put to store work, say ready
                                   'ready',    # Go to flow through location
                                   '1001055',  # License?
                                   'ready',    # License?
                                   'yes',      # 1001055, correct?
                                   'ready',    # Assignment has 6 slot locations 1 item, say ready
                                   'ready',    # Aisle 1001
                                   'ready',    # Bay 1001
                                   '1001',     # 1001
                                   '11!',      # Put 11, boxes
                                   '014',      # Container?
                                   'yes',      # 014, correct?
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle 1011
                                   'ready',    # Bay 1000
                                   '1011',     # 1011
                                   '1000',     # 1011
                                   '5!',       # Put 5, eaches
                                   '011',      # Container?
                                   'yes',      # 011, correct?
                                   'ready',    # Aisle 1009
                                   'ready',    # Bay 1000
                                   '1009',     # 1009
                                   '1000',     # 1009
                                   '11!',      # Put 11, eaches
                                   '075',      # Container?
                                   'yes',      # 075, correct?
                                   'ready',    # Building 1005 Site 1
                                   'ready',    # Aisle 1005
                                   '1005',     # 1005
                                   '16!',      # Put 16, 
                                   '033',      # Container?
                                   'yes',      # 033, correct?
                                   'ready',    # Aisle 1000
                                   'ready',    # Bay 1000
                                   '1000',     # 1000
                                   '7!',       # Put 7, eaches
                                   '028',      # Container?
                                   'yes',      # 028, correct?
                                   'ready',    # Aisle 1007
                                   '1007',     # 1007
                                   '13!',      # Put 13, cases
                                   '043',      # Container?
                                   'yes',      # 043, correct?
                                   'yes',      # Assignment complete.  Do you have residuals?
                                   'ready',    # return residual to residual return location, say ready
                                   '00',       # Check Digit?
                                   '-')        # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '1001055, correct?',
                              'Getting work.',
                              'Assignment has 6 slot locations 1 item, say ready',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Put 11, boxes',
                              'Container?',
                              '014, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'wrong 1011, try again',
                              'Put 5, eaches',
                              'Container?',
                              '011, correct?',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'Put 11, eaches',
                              'Container?',
                              '075, correct?',
                              'Building 1005 Site 1',
                              'Aisle <spell>1005</spell>',
                              '1005',
                              'Put 16, ',
                              'Container?',
                              '033, correct?',
                              'Aisle <spell>1000</spell>',
                              'Bay 1000',
                              '1000',
                              'Put 7, eaches',
                              'Container?',
                              '028, correct?',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'Put 13, cases',
                              'Container?',
                              '043, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'return residual to residual return location, say ready',
                              'Check Digit?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1001055', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '362'],
                                      ['prTaskLUTPtsPut', '362', '2', '1009', '120', '11', '014', '1001055', '0'],
                                      ['prTaskLUTPtsPut', '362', '12', '1009', '121', '5', '011', '1001055', '0'],
                                      ['prTaskLUTPtsPut', '362', '10', '1009', '122', '11', '075', '1001055', '0'],
                                      ['prTaskLUTPtsPut', '362', '6', '1009', '123', '16', '033', '1001055', '0'],
                                      ['prTaskLUTPtsPut', '362', '1', '1009', '124', '7', '028', '1001055', '0'],
                                      ['prTaskLUTPtsPut', '362', '8', '1009', '125', '13', '043', '1001055', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '362'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsnoresiduals', '0', ''],
                                      ['prTaskLUTPtsStopAssignment', '362'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
