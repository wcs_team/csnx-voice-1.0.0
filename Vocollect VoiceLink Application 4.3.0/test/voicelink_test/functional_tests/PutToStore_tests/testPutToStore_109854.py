##TestLink ID 109854 :: Test Case Verify Repick Skips can be spoken at Location and Slot prompts

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109854(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109854(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,2,0,1,1,0,3,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003013,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003033,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003043,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003053,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003013,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('534,3,3,13,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,826,3,3003013,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,15,        0,1004,Item 1004 Site 1,cases,1,4,0,\r\n'
                                 'N,827,2,3003013,,1001                                              ,Bay 1001                                          ,1001                                              ,,1001,1001,12,        0,1009,Item 1009 Site 1,boxes,1,9,0,\r\n'
                                 'N,829,4,3003013,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,13,        0,1000,Item 1000 Site 1,boxes,1,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003013,1000,Item 1000 Site 1,0,N,0,\r\n'
                                 '3003013,1009,Item 1009 Site 1,9,N,0,\r\n'
                                 '3003013,1004,Item 1004 Site 1,4,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003033,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003043,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003053,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003033,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('535,4,1,77,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,323,5,3003033,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,3,        0,1015,Item 1015 Site 1,eaches,1,77,0,\r\n'
                                 'N,324,3,3003033,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,10,        0,1015,Item 1015 Site 1,eaches,0,77,0,\r\n'
                                 'N,325,7,3003033,,1006                                              ,,1006                                              ,1006,1006,1006,16,        0,1015,Item 1015 Site 1,,1,77,0,\r\n'
                                 'N,326,10,3003033,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,7,        0,1015,Item 1015 Site 1,,1,77,0,\r\n'
                                 'N,327,10,3003033,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,10,        0,1015,Item 1015 Site 1,boxes,1,77,0,\r\n'
                                 'N,328,3,3003033,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,10,        0,1015,Item 1015 Site 1,,0,77,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003033,1015,Item 1015 Site 1,77,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',          # Password?
                                   '8',             # Function?
                                   'yes',           # Put To Store, correct?
                                   '3!',            # Region?
                                   'yes',           # Region 3, correct?
                                   'ready',         # To start Put to store work, say ready
                                   'ready',         # Go to flow through location
                                   '3',             # License?
                                   'yes',           # 3, correct?
                                   'yes',           # Found 4 licenses. Do you want to review the list?
                                   'yes',           # 3003013, select?
                                   'no more',       # License?
                                   'ready',         # Assignment has 3 slot locations 3 items, and a total expected residual of 13, say ready
                                   'skip aisle',    # Aisle <spell>1002</spell>
                                   'yes',           # Skip aisle, correct?
                                   'ready',         # Aisle <spell>1001</spell>
                                   'ready',         # Bay 1001
                                   'skip slot',     # 1001
                                   'yes',           # Skip slot, correct?
                                   'ready',         # Building 1003 Site 1
                                   'ready',         # Aisle <spell>1003</spell>
                                   '1003',          # 1003
                                   '13!',           # item 1000 site 1, Put 13, boxes
                                   'ready',         # To put skips, say ready
                                   'ready',         # Aisle <spell>1002</spell>
                                   'ready',         # Bay 1002
                                   '1002',          # 1002
                                   '15!',           # item 1004 site 1, Put 15, cases
                                   'ready',         # Aisle <spell>1001</spell>
                                   'ready',         # Bay 1001
                                   '1001',          # 1001
                                   'ready',         # 1001
                                   '12!',           # item 1009 site 1, Put 12, boxes
                                   'no',            # Assignment complete.  Do you have residuals?
                                   'ready',         # For next assignment, say ready
                                   '3',             # License?
                                   'yes',           # 3, correct?
                                   'yes',           # Found 3 licenses. Do you want to review the list?
                                   'yes',           # 3003033, select?
                                   'no more',       # License?
                                   'ready',         # Assignment has 4 slot locations 1 item, and a total expected residual of 77, say ready
                                   'ready',         # Building 1004 Site 1
                                   'skip aisle',    # Aisle <spell>1004</spell>
                                   'yes',           # Skip aisle, correct?
                                   'ready',         # Aisle <spell>1002</spell>
                                   'ready',         # Bay 1002
                                   'skip slot',     # 1002
                                   'yes',           # Skip slot, correct?
                                   'ready',         # Aisle <spell>1006</spell>
                                   '1006',          # 1006
                                   '16!',           # Put 16, 
                                   'repick skips',  # Building 1000 Site 1
                                   'yes',           # repick skips, correct?
                                   'ready',         # Building 1004 Site 1
                                   'ready',         # Aisle <spell>1004</spell>
                                   '1004',          # 1004
                                   '3!',            # Put 3, eaches
                                   'ready',         # Aisle <spell>1002</spell>
                                   'ready',         # Bay 1002
                                   '1002',          # 1002
                                   '10!',           # Put 10, eaches
                                   'ready',         # Building 1000 Site 1
                                   'repick skips',  # Aisle <spell>1009</spell>
                                   'no',            # repick skips, correct?
                                   'ready',         # Aisle <spell>1009</spell>
                                   'repick skips',  # Bay 1000
                                   'no',            # repick skips, correct?
                                   'ready',         # Bay 1000
                                   'repick skips',  # 1009
                                   'no',            # repick skips, correct?
                                   '1009',          # 1009
                                   '1000',          # wrong 1009, try again
                                   '7!',            # Put 7, 
                                   '1000',          # 1009
                                   '10!',           # Put 10, boxes
                                   'ready',         # Aisle <spell>1002</spell>
                                   'ready',         # Bay 1002
                                   '1002',          # 1002
                                   '10!',           # Put 10, 
                                   'no',            # Assignment complete.  Do you have residuals?
                                   '-')             # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '3, correct?',
                              'Found 4 licenses. Do you want to review the list?',
                              '3003013, select?',
                              'License?',
                              'Getting work.',
                              'Assignment has 3 slot locations 3 items, and a total expected residual of 13, say ready',
                              'Aisle <spell>1002</spell>',
                              'Skip aisle, correct?',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Skip slot, correct?',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'item 1000 site 1, Put 13, boxes',
                              'To put skips, say ready',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'item 1004 site 1, Put 15, cases',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'item 1009 site 1, Put 12, boxes',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready',
                              'License?',
                              '3, correct?',
                              'Found 3 licenses. Do you want to review the list?',
                              '3003033, select?',
                              'License?',
                              'Getting work.',
                              'Assignment has 4 slot locations 1 item, and a total expected residual of 77, say ready',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              'Skip aisle, correct?',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Skip slot, correct?',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'Put 16, ',
                              'Building 1000 Site 1',
                              'repick skips, correct?',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'Put 3, eaches',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 10, eaches',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'repick skips, correct?',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              'repick skips, correct?',
                              'Bay 1000',
                              '1009',
                              'repick skips, correct?',
                              '1009',
                              'wrong 1009, try again',
                              'Put 7, ',
                              '1009',
                              'Put 10, boxes',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 10, ',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '3', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003013', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '534'],
                                      ['prTaskODRPtsUpdateStatus', '534', '3', '1', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '534', '2', '0', 'S'],
                                      ['prTaskLUTPtsPut', '534', '4', '1000', '829', '13', '', '3003013', '0'],
                                      ['prTaskODRPtsUpdateStatus', '534', '', '2', 'N'],
                                      ['prTaskLUTPtsPut', '534', '3', '1004', '826', '15', '', '3003013', '0'],
                                      ['prTaskLUTPtsPut', '534', '2', '1009', '827', '12', '', '3003013', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '534'],
                                      ['prTaskLUTPtsStopAssignment', '534'],
                                      ['prTaskLUTPtsVerifyLicense', '3', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003033', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '535'],
                                      ['prTaskODRPtsUpdateStatus', '535', '5', '1', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '535', '3', '0', 'S'],
                                      ['prTaskLUTPtsPut', '535', '7', '1015', '325', '16', '', '3003033', '0'],
                                      ['prTaskODRPtsUpdateStatus', '535', '', '2', 'N'],
                                      ['prTaskLUTPtsPut', '535', '5', '1015', '323', '3', '', '3003033', '0'],
                                      ['prTaskLUTPtsPut', '535', '3', '1015', '324', '10', '', '3003033', '0'],
                                      ['prTaskLUTPtsPut', '535', '10', '1015', '326', '7', '', '3003033', '0'],
                                      ['prTaskLUTPtsPut', '535', '10', '1015', '327', '10', '', '3003033', '0'],
                                      ['prTaskLUTPtsPut', '535', '3', '1015', '328', '10', '', '3003033', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '535'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=3003033'],
                                      ['prTaskLUTPtsStopAssignment', '535'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
