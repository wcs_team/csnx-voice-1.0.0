##TestLink ID 109646 :: Test Case Residual available/exp resid loc ,unexp resid loc different, multiple item

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109646(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109646(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,1,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003024,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003044,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003054,4,Multiple records exist for license entered by operator.\r\n'
                                 '3052054,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3052054,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('368,3,4,28,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,1025,7,3052054,,1006                                              ,,1006                                              ,1006,1006,1006,11,        0,1001,Item 1001 Site 1,,1,1,0,\r\n'
                                 'N,1026,10,3052054,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,10,        0,1004,Item 1004 Site 1,boxes,1,4,0,\r\n'
                                 'N,1027,10,3052054,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,11,        0,1009,Item 1009 Site 1,cases,1,9,0,\r\n'
                                 'N,1028,9,3052054,,1008                                              ,,1008                                              ,1008,1008,1008,14,        0,1014,Item 1014 Site 1,eaches,1,14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3052054,1009,Item 1009 Site 1,9,N,0,\r\n'
                                 '3052054,1004,Item 1004 Site 1,4,N,0,\r\n'
                                 '3052054,1014,Item 1014 Site 1,14,N,0,\r\n'
                                 '3052054,1001,Item 1001 Site 1,1,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '3!',       # Region?
                                   'yes',      # Region 3, correct?
                                   'ready',    # To start Put to store work, say ready
                                   'ready',    # Go to flow through location
                                   '4',        # License?
                                   'yes',      # 4, correct?
                                   'yes',      # Found 4 licenses. Do you want to review the list?
                                   'no',       # 3003024, select?
                                   'no',       # 3003044, select?
                                   'no',       # 3003054, select?
                                   'yes',      # 3052054, select?
                                   'no more',  # License?
                                   'ready',    # Assignment has 3 slot locations 4 items, and a total expected residual of 28, say ready
                                   'ready',    # Aisle 1006
                                   '1006',     # 1006
                                   '11!',      # item 1001 site 1, Put 11, 
                                   '00074',    # Container?
                                   'no',       # 0, correct?
                                   '4',        # Container?
                                   'yes',      # 4, correct?
                                   'reay',     # Building 1000 Site 1
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle 1009
                                   'ready',    # Bay 1000
                                   '1009',     # 1009
                                   '1000',     # 1009
                                   '10!',      # item 1004 site 1, Put 10, boxes
                                   '5',        # Container?
                                   'yes',      # 5, correct?
                                   '1000',     # 1009
                                   '11!',      # item 1009 site 1, Put 11, cases
                                   '5',        # Container?
                                   'yes',      # 5, correct?
                                   'ready',    # Aisle 1008
                                   '1008',     # 1008
                                   '14!',      # item 1014 site 1, Put 14, eaches
                                   '6',        # Container?
                                   'yes',      # 6, correct?
                                   'yes',      # Assignment complete.  Do you have residuals?
                                   '9!',       # How many for item Item 1009 Site 1?
                                   'yes',      # 9, correct?
                                   '4!',       # How many for item Item 1004 Site 1?
                                   'yes',      # 4, correct?
                                   '14!',      # How many for item Item 1014 Site 1?
                                   'yes',      # 14, correct?
                                   '1!',       # How many for item Item 1001 Site 1?
                                   'yes',      # 1, correct?
                                   'ready',    # return residual to residual return location, say ready
                                   '00',       # Check Digit?
                                   '-')        # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '4, correct?',
                              'Found 4 licenses. Do you want to review the list?',
                              '3003024, select?',
                              '3003044, select?',
                              '3003054, select?',
                              '3052054, select?',
                              'License?',
                              'Getting work.',
                              'Assignment has 3 slot locations 4 items, and a total expected residual of 28, say ready',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'item 1001 site 1, Put 11, ',
                              'Container?',
                              '0, correct?',
                              'Container?',
                              '4, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1004 site 1, Put 10, boxes',
                              'Container?',
                              '5, correct?',
                              '1009',
                              'item 1009 site 1, Put 11, cases',
                              'Container?',
                              '5, correct?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1014 site 1, Put 14, eaches',
                              'Container?',
                              '6, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'Confirm residuals for license 3052054',
                              'How many for item Item 1009 Site 1?',
                              '9, correct?',
                              'How many for item Item 1004 Site 1?',
                              '4, correct?',
                              'How many for item Item 1014 Site 1?',
                              '14, correct?',
                              'How many for item Item 1001 Site 1?',
                              '1, correct?',
                              'return residual to residual return location, say ready',
                              'Check Digit?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '4', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3052054', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '368'],
                                      ['prTaskLUTPtsPut', '368', '7', '1001', '1025', '11', '', '3052054', '0'],
                                      ['prTaskLUTPtsPut', '368', '7', '1001', '1025', '11', '4', '3052054', '0'],
                                      ['prTaskLUTPtsPut', '368', '10', '1004', '1026', '10', '', '3052054', '0'],
                                      ['prTaskLUTPtsPut', '368', '10', '1004', '1026', '10', '5', '3052054', '0'],
                                      ['prTaskLUTPtsPut', '368', '10', '1009', '1027', '11', '', '3052054', '0'],
                                      ['prTaskLUTPtsPut', '368', '10', '1009', '1027', '11', '5', '3052054', '0'],
                                      ['prTaskLUTPtsPut', '368', '9', '1014', '1028', '14', '', '3052054', '0'],
                                      ['prTaskLUTPtsPut', '368', '9', '1014', '1028', '14', '6', '3052054', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '368'],
                                      ['prTaskLUTPtsStopAssignment', '368'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
