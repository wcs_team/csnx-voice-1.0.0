##TestLink ID 109612 :: Test Case Verify last slot can be skipped if previous slots skipped

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109612(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109612(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,0,1,1,4,0,0,3,1,0,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001059,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('500,6,1,5,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,144,7,1001059,,1006                                              ,,1006                                              ,1006,1006,1006,2,        0,1013,Item 1013 Site 1,cases,0,5,0,\r\n'
                                 'N,145,5,1001059,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,11,        0,1013,Item 1013 Site 1,,0,5,0,\r\n'
                                 'N,146,4,1001059,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,12,        0,1013,Item 1013 Site 1,,1,5,0,\r\n'
                                 'N,147,10,1001059,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,13,        0,1013,Item 1013 Site 1,boxes,1,5,0,\r\n'
                                 'N,148,3,1001059,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,7,        0,1013,Item 1013 Site 1,,1,5,0,\r\n'
                                 'N,149,2,1001059,,1001                                              ,Bay 1001                                          ,1001                                              ,,1001,1001,3,        0,1013,Item 1013 Site 1,eaches,1,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001059,1013,Item 1013 Site 1,5,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',      # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',       # Password?
                                   '8',          # Function?
                                   'yes',        # Put To Store, correct?
                                   '1!',         # Region?
                                   'yes',        # Region 1, correct?
                                   'ready',      # To start Put to store work, say ready
                                   'ready',      # Go to flow through location
                                   '1059',       # License?
                                   'ready',      # Assignment has 6 slot locations 1 item, and a total expected residual of 5, say ready
                                   'ready',      # Aisle <spell>1006</spell>
                                   'skip slot',  # 1006
                                   'yes',        # Skip slot, correct?
                                   'ready',      # Building 1004 Site 1
                                   'ready',      # Aisle <spell>1004</spell>
                                   'skip slot',  # 1004
                                   'yes',        # Skip slot, correct?
                                   'ready',      # Building 1003 Site 1
                                   'ready',      # Aisle <spell>1003</spell>
                                   'skip slot',  # 1003
                                   'yes',        # Skip slot, correct?
                                   'ready',      # Building 1000 Site 1
                                   'ready',      # Aisle <spell>1009</spell>
                                   'ready',      # Bay 1000
                                   'skip slot',  # 1009
                                   'yes',        # Skip slot, correct?
                                   'ready',      # Aisle <spell>1002</spell>
                                   'ready',      # Bay 1002
                                   'skip slot',  # 1002
                                   'yes',        # Skip slot, correct?
                                   'readyu',     # Aisle <spell>1001</spell>
                                   'ready',      # Bay 1001
                                   'skip slot',  # 1001
                                   'yes',        # Skip slot, correct?
                                   'ready',      # To put skips, say ready
                                   'ready',      # Aisle <spell>1006</spell>
                                   '1006',       # 1006
                                   '2!',         # Put 2, cases
                                   'ready',      # Building 1004 Site 1
                                   'ready',      # Aisle <spell>1004</spell>
                                   '1004',       # 1004
                                   '11!',        # Put 11, 
                                   'ready',      # Building 1003 Site 1
                                   'ready',      # Aisle <spell>1003</spell>
                                   '1003',       # 1003
                                   '12!',        # Put 12, 
                                   'ready',      # Building 1000 Site 1
                                   'ready',      # Aisle <spell>1009</spell>
                                   'ready',      # Bay 1000
                                   '1009',       # 1009
                                   '1000',       # wrong 1009, try again
                                   '13!',        # Put 13, boxes
                                   'ready',      # Aisle <spell>1002</spell>
                                   'ready',      # Bay 1002
                                   '1002',       # 1002
                                   '7!',         # Put 7, 
                                   'ready',      # Aisle <spell>1001</spell>
                                   'ready',      # Bay 1001
                                   'skip slot',  # 1001
                                   '1001',       # 1001
                                   'ready',      # 1001
                                   '3!',         # Put 3, eaches
                                   'no',         # Assignment complete.  Do you have residuals?
                                   '-')          # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              'Getting work.',
                              'Assignment has 6 slot locations 1 item, and a total expected residual of 5, say ready',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'Skip slot, correct?',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'Skip slot, correct?',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'Skip slot, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'Skip slot, correct?',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Skip slot, correct?',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Skip slot, correct?',
                              'To put skips, say ready',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'Put 2, cases',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'Put 11, ',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'Put 12, ',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'Put 13, boxes',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 7, ',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Last slot, skip slot not allowed',
                              '1001',
                              'Put 3, eaches',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1059', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '500'],
                                      ['prTaskODRPtsUpdateStatus', '500', '7', '0', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '500', '5', '0', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '500', '4', '0', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '500', '10', '0', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '500', '3', '0', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '500', '2', '0', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '500', '', '2', 'N'],
                                      ['prTaskLUTPtsPut', '500', '7', '1013', '144', '2', '', '1001059', '0'],
                                      ['prTaskLUTPtsPut', '500', '5', '1013', '145', '11', '', '1001059', '0'],
                                      ['prTaskLUTPtsPut', '500', '4', '1013', '146', '12', '', '1001059', '0'],
                                      ['prTaskLUTPtsPut', '500', '10', '1013', '147', '13', '', '1001059', '0'],
                                      ['prTaskLUTPtsPut', '500', '3', '1013', '148', '7', '', '1001059', '0'],
                                      ['prTaskLUTPtsPut', '500', '2', '1013', '149', '3', '', '1001059', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '500'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=1001059'],
                                      ['prTaskLUTPtsStopAssignment', '500'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
