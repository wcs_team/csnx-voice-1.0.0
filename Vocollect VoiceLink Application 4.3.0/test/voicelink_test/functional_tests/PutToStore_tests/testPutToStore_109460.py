##TestLink ID 109460 :: Test Case Verify Skip Aisle command functions correctly

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109460(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109460(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,2,1,5,0,0,5,0,4,1,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002003,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002004,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('383,6,2,106,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,172,3,2002004,,1002                                              ,Bay 1002                                          ,1002                                              ,,1002,2222222222,11,        0,1002,Item 1002 Site 1,,0,7,0,\r\n'
                                 'N,167,9,2002003,,1008                                              ,,1008                                              ,1008,1008,1008,4,        0,1001,Item 1001 Site 1,eaches,1,99,0,\r\n'
                                 'N,169,9,2002004,,1008                                              ,,1008                                              ,1008,1008,1008,8,        0,1002,Item 1002 Site 1,eaches,0,7,0,\r\n'
                                 'N,168,11,2002004,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,16,        0,1002,Item 1002 Site 1,cases,0,7,0,\r\n'
                                 'N,173,11,2002004,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,10,        0,1002,Item 1002 Site 1,,1,7,0,\r\n'
                                 'N,164,12,2002003,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,4,        0,1001,Item 1001 Site 1,,0,99,0,\r\n'
                                 'N,170,12,2002004,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,3,        0,1002,Item 1002 Site 1,boxes,0,7,0,\r\n'
                                 'N,165,5,2002003,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,14,        0,1001,Item 1001 Site 1,cases,1,99,0,\r\n'
                                 'N,171,5,2002004,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,1,        0,1002,Item 1002 Site 1,eaches,0,7,0,\r\n'
                                 'N,166,6,2002003,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,5,        0,1001,Item 1001 Site 1,cases,0,99,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002004,1002,Item 1002 Site 1,7,N,0,\r\n'
                                 '2002003,1001,Item 1001 Site 1,99,N,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',       # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',        # Password?
                                   '8',           # Function?
                                   'yes',         # Put To Store, correct?
                                   '2!',          # Region?
                                   'yes',         # Region 2, correct?
                                   'ready',       # To start Put to store work, say ready
                                   '02003',       # License?
                                   '02004',       # License?
                                   'ready',       # Assignment has 6 slot locations 2 items, and a total expected residual of 106, say ready
                                   'ready',       # Aisle 1002
                                   'ready',       # Bay 1002
                                   '1002',        # 1002
                                   'ready',       # 1002
                                   '11!',         # item 1002 site 1, Put 11, 
                                   'skip aisle',  # Aisle 1008
                                   'yes',         # Skip aisle, correct?
                                   'ready',       # Building 1000 Site 1
                                   'skip aisle',  # Aisle 1010
                                   'yes',         # Skip aisle, correct?
                                   'ready',       # Aisle 1011
                                   'ready',       # Bay 1000
                                   '1000',        # 1011
                                   '4!',          # item 1001 site 1, Put 4, 
                                   '1000',        # 1011
                                   '3!',          # item 1002 site 1, Put 3, boxes
                                   'ready',       # Building 1004 Site 1
                                   'ready',       # Aisle 1004
                                   '1004',        # 1004
                                   '14!',         # item 1001 site 1, Put 14, cases
                                   '1004',        # 1004
                                   '1!',          # item 1002 site 1, Put 1, eaches
                                   'ready',       # Building 1005 Site 1
                                   'ready',       # Aisle 1005
                                   '1005',        # 1005
                                   '5!',          # item 1001 site 1, Put 5, cases
                                   '00002',       # Container?
                                   'yes',         # 00002, correct?
                                   'ready',       # To put skips, say ready
                                   'ready',       # Aisle 1008
                                   '1008',        # 1008
                                   '4!',          # item 1001 site 1, Put 4, eaches
                                   '00076',       # Container?
                                   'yes',         # 00076, correct?
                                   '1008',        # 1008
                                   '8!',          # item 1002 site 1, Put 8, eaches
                                   '00076',       # Container?
                                   'yes',         # 00076, correct?
                                   'ready',       # Building 1000 Site 1
                                   'ready',       # Aisle 1010
                                   'ready',       # Bay 1000
                                   '1010',        # 1010
                                   '1000',        # 1010
                                   '16!',         # item 1002 site 1, Put 16, cases
                                   '00037',       # Container?
                                   'yes',         # 00037, correct?
                                   '1000',        # 1010
                                   '10!',         # item 1002 site 1, Put 10, 
                                   '00037',       # Container?
                                   'yes',         # 00037, correct?
                                   '-')           # Assignment complete.  Do you have residuals?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'License?',
                              'Getting work.',
                              'Assignment has 6 slot locations 2 items, and a total expected residual of 106, say ready',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'item 1002 site 1, Put 11, ',
                              'Aisle <spell>1008</spell>',
                              'Skip aisle, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Skip aisle, correct?',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'item 1001 site 1, Put 4, ',
                              '1011',
                              'item 1002 site 1, Put 3, boxes',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'item 1001 site 1, Put 14, cases',
                              '1004',
                              'item 1002 site 1, Put 1, eaches',
                              'Building 1005 Site 1',
                              'Aisle <spell>1005</spell>',
                              '1005',
                              'item 1001 site 1, Put 5, cases',
                              'Container?',
                              '00002, correct?',
                              'To put skips, say ready',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1001 site 1, Put 4, eaches',
                              'Container?',
                              '00076, correct?',
                              '1008',
                              'item 1002 site 1, Put 8, eaches',
                              'Container?',
                              '00076, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'wrong 1010, try again',
                              'item 1002 site 1, Put 16, cases',
                              'Container?',
                              '00037, correct?',
                              '1010',
                              'item 1002 site 1, Put 10, ',
                              'Container?',
                              '00037, correct?',
                              'Assignment complete.  Do you have residuals?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '02003', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '02004', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '383'],
                                      ['prTaskLUTPtsPut', '383', '3', '1002', '172', '11', '', '2002004', '0'],
                                      ['prTaskODRPtsUpdateStatus', '383', '9', '1', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '383', '11', '1', 'S'],
                                      ['prTaskLUTPtsPut', '383', '12', '1001', '164', '4', '', '2002003', '0'],
                                      ['prTaskLUTPtsPut', '383', '12', '1002', '170', '3', '', '2002004', '0'],
                                      ['prTaskLUTPtsPut', '383', '5', '1001', '165', '14', '', '2002003', '0'],
                                      ['prTaskLUTPtsPut', '383', '5', '1002', '171', '1', '', '2002004', '0'],
                                      ['prTaskLUTPtsPut', '383', '6', '1001', '166', '5', '', '2002003', '0'],
                                      ['prTaskLUTPtsPut', '383', '6', '1001', '166', '5', '00002', '2002003', '0'],
                                      ['prTaskODRPtsUpdateStatus', '383', '', '2', 'N'],
                                      ['prTaskLUTPtsPut', '383', '9', '1001', '167', '4', '', '2002003', '0'],
                                      ['prTaskLUTPtsPut', '383', '9', '1001', '167', '4', '00076', '2002003', '0'],
                                      ['prTaskLUTPtsPut', '383', '9', '1002', '169', '8', '', '2002004', '0'],
                                      ['prTaskLUTPtsPut', '383', '9', '1002', '169', '8', '00076', '2002004', '0'],
                                      ['prTaskLUTPtsPut', '383', '11', '1002', '168', '16', '', '2002004', '0'],
                                      ['prTaskLUTPtsPut', '383', '11', '1002', '168', '16', '00037', '2002004', '0'],
                                      ['prTaskLUTPtsPut', '383', '11', '1002', '173', '10', '', '2002004', '0'],
                                      ['prTaskLUTPtsPut', '383', '11', '1002', '173', '10', '00037', '2002004', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '383'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
