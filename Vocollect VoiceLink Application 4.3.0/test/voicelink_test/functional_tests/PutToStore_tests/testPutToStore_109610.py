##TestLink ID 109610 :: Test Case Verify slot cannot be skipped if not allowed in region settings

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109610(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109610(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,1,1,4,0,0,3,0,4,4,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002056,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('497,2,2,17,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,744,4,2002056,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,10,        0,1004,Item 1004 Site 1,,0,4,0,\r\n'
                                 'N,745,1,2002056,,1000                                              ,Bay 1000                                          ,1000                                              ,1000,1000,1000000000,13,        0,1013,Item 1013 Site 1,eaches,0,13,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002056,1013,Item 1013 Site 1,13,N,0,\r\n'
                                 '2002056,1004,Item 1004 Site 1,4,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',       # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',        # Password?
                                   '8',           # Function?
                                   'yes',         # Put To Store, correct?
                                   '2!',          # Region?
                                   'yes',         # Region 2, correct?
                                   'ready',       # To start Put to store work, say ready
                                   '2056',        # License?
                                   'ready',       # Assignment has 2 slot locations 2 items, and a total expected residual of 17, say ready
                                   'ready',       # Building 1003 Site 1
                                   'ready',       # Aisle <spell>1003</spell>
                                   'skip slot',   # 1003
                                   '1003',        # 1003
                                   '10!',         # item 1004 site 1, Put 10, 
                                   'skip aisle',  # Aisle <spell>1000</spell>
                                   'ready',       # Aisle <spell>1000</spell>
                                   'ready',       # Bay 1000
                                   '1000',        # 1000
                                   '13!',         # item 1013 site 1, Put 13, eaches
                                   'no',          # Assignment complete.  Do you have residuals?
                                   '-')           # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'Getting work.',
                              'Assignment has 2 slot locations 2 items, and a total expected residual of 17, say ready',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'skip slot not allowed',
                              '1003',
                              'item 1004 site 1, Put 10, ',
                              'Aisle <spell>1000</spell>',
                              'Last aisle. Skip aisle not allowed.',
                              'Aisle <spell>1000</spell>',
                              'Bay 1000',
                              '1000',
                              'item 1013 site 1, Put 13, eaches',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '2056', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '497'],
                                      ['prTaskLUTPtsPut', '497', '4', '1004', '744', '10', '', '2002056', '0'],
                                      ['prTaskLUTPtsPut', '497', '1', '1013', '745', '13', '', '2002056', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '497'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=2002056'],
                                      ['prTaskLUTPtsStopAssignment', '497'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
