##TestLink ID 109642 :: Test Case Test when assignment has no residual/user says yes to resid prompt
##This test is run in  region 2 since data was not available in region 1(testcase is for region1)

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109642(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109642(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,2,1,5,0,0,5,0,4,1,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002009,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('365,2,1,0,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,198,1,2002009,,1000                                              ,Bay 1000                                          ,1000                                              ,1000,1000,1000000000,14,        0,1007,Item 1007 Site 1,,0,0,0,\r\n'
                                 'N,199,3,2002009,,1002                                              ,Bay 1002                                          ,1002                                              ,,1002,2222222222,12,        0,1007,Item 1007 Site 1,boxes,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002009,1007,Item 1007 Site 1,0,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '2!',       # Region?
                                   'yes',      # Region 2, correct?
                                   'ready',    # To start Put to store work, say ready
                                   '02009',    # License?
                                   'no more',  # License?
                                   'ready',    # Assignment has 2 slot locations 1 item, say ready
                                   'ready',    # Aisle 1000
                                   'ready',    # Bay 1000
                                   '1000',     # 1000
                                   '14!',      # Put 14, 
                                   'ready',    # Aisle 1002
                                   'ready',    # Bay 1002
                                   '1002',     # 1002
                                   'ready',    # 1002
                                   '12!',      # Put 12, boxes
                                   '00017',    # Container?
                                   'yes',      # 00017, correct?
                                   'yes',      # Assignment complete.  Do you have residuals?
                                   '0!',       # Confirm residuals for license 2002009
                                   'yes',      # 0, correct?
                                   'ready',    # return residual to residual return location, say ready
                                   '00',       # Check Digit?
                                   '-')        # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'License?',
                              'Getting work.',
                              'Assignment has 2 slot locations 1 item, say ready',
                              'Aisle <spell>1000</spell>',
                              'Bay 1000',
                              '1000',
                              'Put 14, ',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 12, boxes',
                              'Container?',
                              '00017, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'Confirm residuals for license 2002009',
                              '0, correct?',
                              'return residual to residual return location, say ready',
                              'Check Digit?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '02009', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '365'],
                                      ['prTaskLUTPtsPut', '365', '1', '1007', '198', '14', '', '2002009', '0'],
                                      ['prTaskLUTPtsPut', '365', '3', '1007', '199', '12', '', '2002009', '0'],
                                      ['prTaskLUTPtsPut', '365', '3', '1007', '199', '12', '00017', '2002009', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '365'],
                                      ['prTaskLUTPtsStopAssignment', '365'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
