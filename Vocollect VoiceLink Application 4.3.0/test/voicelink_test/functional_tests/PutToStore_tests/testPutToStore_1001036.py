##TestLink ID 109618 :: Test Case Test complete assignment flow when skips not found

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_1001036(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_1001036(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,1,1,1,0,1,1,3,1,0,2,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001036,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('360,6,1,9,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,30,2,1001036,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,15,        0,1006,Item 1006 Site 1,boxes,1,9,0,\r\n'
                                 'N,31,5,1001036,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,11,        0,1006,Item 1006 Site 1,,1,9,0,\r\n'
                                 'N,32,4,1001036,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,12,        0,1006,Item 1006 Site 1,cases,1,9,0,\r\n'
                                 'N,33,9,1001036,,1008                                              ,,1008                                              ,1008,1008,1008,15,        0,1006,Item 1006 Site 1,boxes,1,9,0,\r\n'
                                 'N,34,7,1001036,,1006                                              ,,1006                                              ,1006,1006,1006,15,        0,1006,Item 1006 Site 1,,0,9,0,\r\n'
                                 'N,35,10,1001036,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,15,        0,1006,Item 1006 Site 1,cases,0,9,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('9,0000000076,076,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001036,1006,Item 1006 Site 1,9,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '8',              # Function?
                                   'yes',            # Put To Store, correct?
                                   '1!',             # Region?
                                   'yes',            # Region 1, correct?
                                   'ready',          # To start Put to store work, say ready
                                   'ready',          # Go to flow through location
                                   '1001036',        # License?
                                   'ready',          # License?
                                   'yes',            # 1001036, correct?
                                   'ready',          # Assignment has 6 slot locations 1 item, and a total expected residual of 9, say ready
                                   'ready',          # Aisle 1001
                                   'ready',          # Bay 1001
                                   '1001',           # 1001
                                   '15!',            # Put 15, boxes
                                   '014',            # Container?
                                   'yes',            # 014, correct?
                                   'ready',          # Building 1004 Site 1
                                   'ready',          # Aisle 1004
                                   '1004',           # 1004
                                   '11!',            # Put 11, 
                                   '	035',           # Container?
                                   '035',            # Container?
                                   'yes',            # 035, correct?
                                   'ready',          # Building 1003 Site 1
                                   'ready',          # Aisle 1003
                                   '1003',           # 1003
                                   '12!',            # Put 12, cases
                                   '040',            # Container?
                                   'yes',            # 040, correct?
                                   'ready',          # Aisle 1008
                                   '1008',           # 1008
                                   '15!',            # Put 15, boxes
                                   'new container',  # Container?
                                   'yes',            # new container, correct?
                                   '1!',             # Printer?
                                   'yes',            # printer 1, correct?
                                   'ready',          # Retrieve label from printer 1, then say ready.
                                   'ready',          # Aisle 1008
                                   '1008',           # 1008
                                   '15!',            # Put 15, boxes
                                   '076',            # Container?
                                   'yes',            # 076, correct?
                                   'ready',          # Aisle 1006
                                   '1006',           # 1006
                                   '15!',            # Put 15, 
                                   '074',            # Container?
                                   'yes',            # 074, correct?
                                   'ready',          # Building 1000 Site 1
                                   'ready',          # Aisle 1009
                                   'ready',          # Bay 1000
                                   '1009',           # 1009
                                   '1000',           # 1009
                                   '15!',            # Put 15, cases
                                   '075',            # Container?
                                   'yes',            # 075, correct?
                                   'no',             # Assignment complete.  Do you have residuals?
                                   '-')              # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '1001036, correct?',
                              'Getting work.',
                              'Assignment has 6 slot locations 1 item, and a total expected residual of 9, say ready',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Put 15, boxes',
                              'Container?',
                              '014, correct?',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'Put 11, ',
                              'Container?',
                              '035, correct?',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'Put 12, cases',
                              'Container?',
                              '040, correct?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'Put 15, boxes',
                              'Container?',
                              'new container, correct?',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'Put 15, boxes',
                              'Container?',
                              '076, correct?',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'Put 15, ',
                              'Container?',
                              '074, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'Put 15, cases',
                              'Container?',
                              '075, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1001036', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '360'],
                                      ['prTaskLUTPtsPut', '360', '2', '1006', '30', '15', '014', '1001036', '0'],
                                      ['prTaskLUTPtsPut', '360', '5', '1006', '31', '11', '035', '1001036', '0'],
                                      ['prTaskLUTPtsPut', '360', '4', '1006', '32', '12', '040', '1001036', '0'],
                                      ['prTaskLUTPtsContainer', '2', '9', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '360', '1', '9', '0000000076'],
                                      ['prTaskLUTPtsPut', '360', '9', '1006', '33', '15', '076', '1001036', '0'],
                                      ['prTaskLUTPtsPut', '360', '7', '1006', '34', '15', '074', '1001036', '0'],
                                      ['prTaskLUTPtsPut', '360', '10', '1006', '35', '15', '075', '1001036', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '360'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=1001036'],
                                      ['prTaskLUTPtsStopAssignment', '360'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
