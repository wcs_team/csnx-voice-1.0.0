##TestLink ID 109462 :: Test Case Verify operator cannot skip aisle if region does not allow skip aisle

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109462(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109462(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,1,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003035,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003045,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003055,4,Multiple records exist for license entered by operator.\r\n'
                                 '3062055,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003045,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('384,3,1,1,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,385,12,3003045,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,1,        0,1011,Item 1011 Site 1,cases,0,1,0,\r\n'
                                 'N,386,11,3003045,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,15,        0,1011,Item 1011 Site 1,cases,0,1,0,\r\n'
                                 'N,387,4,3003045,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,10,        0,1011,Item 1011 Site 1,,0,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',       # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',        # Password?
                                   '8',           # Function?
                                   'yes',         # Put To Store, correct?
                                   '3!',          # Region?
                                   'yes',         # Region 3, correct?
                                   'ready',       # To start Put to store work, say ready
                                   'ready',       # Go to flow through location
                                   '5',           # License?
                                   'yes',         # 5, correct?
                                   'yes',         # Found 4 licenses. Do you want to review the list?
                                   'no',          # 3003035, select?
                                   'yes',         # 3003045, select?
                                   'no more',     # License?
                                   'ready',       # Assignment has 3 slot locations 1 item, and a total expected residual of 1, say ready
                                   'ready',       # Building 1000 Site 1
                                   'skip aisle',  # Aisle 1011
                                   'ready',       # Aisle 1011
                                   'ready',       # Bay 1000
                                   'skip slot',   # 1011
                                   '1011',        # 1011
                                   '1000',        # 1011
                                   '1!',          # Put 1, cases
                                   '-')           # Aisle 1010

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '5, correct?',
                              'Found 4 licenses. Do you want to review the list?',
                              '3003035, select?',
                              '3003045, select?',
                              'License?',
                              'Getting work.',
                              'Assignment has 3 slot locations 1 item, and a total expected residual of 1, say ready',
                              'Building 1000 Site 1',
                              'Aisle <spell>1011</spell>',
                              'skip aisle not allowed',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'skip slot not allowed',
                              '1011',
                              'wrong 1011, try again',
                              'Put 1, cases',
                              'Aisle <spell>1010</spell>')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '5', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003045', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '384'],
                                      ['prTaskLUTPtsPut', '384', '12', '1011', '385', '1', '', '3003045', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
