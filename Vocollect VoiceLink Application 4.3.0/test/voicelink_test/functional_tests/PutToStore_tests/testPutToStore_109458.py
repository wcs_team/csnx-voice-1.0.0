##TestLink ID 109458 :: Test Case Test when single license is returned
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109458(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109458(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,2,1,5,0,0,5,0,4,1,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002001,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002002,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('382,6,2,198,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,157,2,2002001,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,7,        0,1015,Item 1015 Site 1,eaches,1,99,0,\r\n'
                                 'N,159,2,2002001,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,10,        0,1015,Item 1015 Site 1,eaches,0,99,0,\r\n'
                                 'N,162,3,2002002,,1002                                              ,Bay 1002                                          ,1002                                              ,,1002,2222222222,15,        0,1000,Item 1000 Site 1,,1,99,0,\r\n'
                                 'N,160,8,2002001,,1007                                              ,,1007                                              ,1007,1007,1007,14,        0,1015,Item 1015 Site 1,boxes,0,99,0,\r\n'
                                 'N,161,12,2002001,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,15,        0,1015,Item 1015 Site 1,boxes,1,99,0,\r\n'
                                 'N,156,4,2002001,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,13,        0,1015,Item 1015 Site 1,,0,99,0,\r\n'
                                 'N,163,4,2002002,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,2,        0,1000,Item 1000 Site 1,cases,0,99,0,\r\n'
                                 'N,158,6,2002001,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,5,        0,1015,Item 1015 Site 1,cases,0,99,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '8',      # Function?
                                   'yes',    # Put To Store, correct?
                                   '2!',     # Region?
                                   'yes',    # Region 2, correct?
                                   'ready',  # To start Put to store work, say ready
                                   '02001',  # License?
                                   '02002',  # License?
                                   'ready',  # Assignment has 6 slot locations 2 items, and a total expected residual of 198, say ready
                                   '-')      # Aisle 1001

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'License?',
                              'Getting work.',
                              'Assignment has 6 slot locations 2 items, and a total expected residual of 198, say ready',
                              'Aisle <spell>1001</spell>')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '02001', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '02002', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '382'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
