##TestLink ID 109648 :: Test Case Verify application correctly tracks residual quantities

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109648(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109648(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,0,1,1,4,0,0,3,1,0,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001027,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('548,6,5,33,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,592,11,1001027,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,15,        0,1010,Item 1010 Site 1,boxes,1,10,0,\r\n'
                                 'N,593,9,1001027,,1008                                              ,,1008                                              ,1008,1008,1008,4,        0,1008,Item 1008 Site 1,boxes,0,8,0,\r\n'
                                 'N,594,2,1001027,,1001                                              ,Bay 1001                                          ,1001                                              ,,1001,1001,14,        0,1000,Item 1000 Site 1,boxes,0,0,0,\r\n'
                                 'N,595,3,1001027,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,12,        0,1011,Item 1011 Site 1,boxes,1,11,0,\r\n'
                                 'N,596,5,1001027,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,13,        0,1010,Item 1010 Site 1,boxes,1,10,0,\r\n'
                                 'N,597,7,1001027,,1006                                              ,,1006                                              ,1006,1006,1006,11,        0,1004,Item 1004 Site 1,eaches,1,4,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Container 432 not found or is already closed.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,No open container. Open a new container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,0000000145,145,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001027,1000,Item 1000 Site 1,0,N,0,\r\n'
                                 '1001027,1011,Item 1011 Site 1,11,N,0,\r\n'
                                 '1001027,1010,Item 1010 Site 1,1,N,0,\r\n'
                                 '1001027,1004,Item 1004 Site 1,0,N,0,\r\n'
                                 '1001027,1008,Item 1008 Site 1,8,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001021,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('549,5,4,31,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,560,3,1001021,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,13,        0,1009,Item 1009 Site 1,eaches,1,9,0,\r\n'
                                 'N,561,8,1001021,,1007                                              ,,1007                                              ,1007,1007,1007,3,        0,1005,Item 1005 Site 1,eaches,0,5,0,\r\n'
                                 'N,562,9,1001021,,1008                                              ,,1008                                              ,1008,1008,1008,12,        0,1007,Item 1007 Site 1,cases,0,7,0,\r\n'
                                 'N,563,4,1001021,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,3,        0,1007,Item 1007 Site 1,eaches,1,7,0,\r\n'
                                 'N,564,12,1001021,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,1,        0,1010,Item 1010 Site 1,,1,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('12,0000000129,129,0,\r\n'
                                 '12,0000000134,134,0,\r\n'
                                 '12,0000000144,144,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001021,1009,Item 1009 Site 1,9,N,0,\r\n'
                                 '1001021,1010,Item 1010 Site 1,10,N,0,\r\n'
                                 '1001021,1001,Item 1001 Site 1,1,N,0,\r\n'
                                 '1001021,1005,Item 1005 Site 1,5,N,0,\r\n'
                                 '1001021,1007,Item 1007 Site 1,0,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',              # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',               # Password?
                                   '8',                  # Function?
                                   'yes',                # Put To Store, correct?
                                   '1!',                 # Region?
                                   'yes',                # Region 1, correct?
                                   'ready',              # To start Put to store work, say ready
                                   'ready',              # Go to flow through location
                                   '1027',               # License?
                                   'ready',              # Assignment has 6 slot locations 5 items, and a total expected residual of 33, say ready
                                   'ready',              # Building 1000 Site 1
                                   'ready',              # Aisle <spell>1010</spell>
                                   'ready',              # Bay 1000
                                   '1000',               # 1010
                                   '30!',                # item 1010 site 1, Put 15, boxes
                                   'yes',                # Is this an overpack?
                                   '24!',                # item 1010 site 1, Put 15, boxes
                                   'yes',                # Is this an overpack?
                                   '432',                # Container?
                                   'yes',                # 432, correct?
                                   '136',                # Container?
                                   'yes',                # 136, correct?
                                   'ready',              # Aisle <spell>1008</spell>
                                   '1008',               # 1008
                                   '4!',                 # item 1008 site 1, Put 4, boxes
                                   'ready',              # Aisle <spell>1001</spell>
                                   'ready',              # Bay 1001
                                   '1001',               # 1001
                                   'ready',              # 1001
                                   '15!',                # item 1000 site 1, Put 14, boxes
                                   'yes',                # Is this an overpack?
                                   '14!',                # item 1000 site 1, Put 14, boxes
                                   '1!',                 # Printer?
                                   'yes',                # printer <spell>1</spell>, correct?
                                   'read',               # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',              # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',              # Aisle <spell>1001</spell>
                                   'ready',              # Bay 1001
                                   '1001',               # 1001
                                   'ready',              # 1001
                                   '14!',                # item 1000 site 1, Put 14, boxes
                                   'ready',              # Aisle <spell>1002</spell>
                                   'ready',              # Bay 1002
                                   '1002',               # 1002
                                   '12!',                # item 1011 site 1, Put 12, boxes
                                   'ready',              # Building 1004 Site 1
                                   'ready',              # Aisle <spell>1004</spell>
                                   '1004',               # 1004
                                   '13!',                # item 1010 site 1, Put 13, boxes
                                   'ready',              # Aisle <spell>1006</spell>
                                   '1006',               # 1006
                                   '15!',                # item 1004 site 1, Put 11, eaches
                                   'yes',                # Is this an overpack?
                                   'no',                 # Assignment complete.  Do you have residuals?
                                   'ready',              # For next assignment, say ready
                                   '1021',               # License?                ----                                                     Testing 3rd bullet in the test case Summary
                                   'ready',              # Assignment has 5 slot locations 4 items, and a total expected residual of 31, say ready
                                   'ready',              # Aisle <spell>1002</spell>
                                   'ready',              # Bay 1002
                                   '1002',               # 1002
                                   '13!',                # item 1009 site 1, Put 13, eaches
                                   'ready',              # Aisle <spell>1007</spell>
                                   '1007',               # 1007
                                   '3!',                 # item 1005 site 1, Put 3, eaches
                                   'ready',              # Aisle <spell>1008</spell>
                                   '1008',               # 1008
                                   '15!',                # item 1007 site 1, Put 12, cases
                                   'yes',                # Is this an overpack?
                                   '12!',                # item 1007 site 1, Put 12, cases
                                   'ready',              # Building 1003 Site 1
                                   'ready',              # Aisle <spell>1003</spell>
                                   '1003',               # 1003
                                   '10!',                # item 1007 site 1, Put 3, eaches
                                   'yes',                # Is this an overpack?
                                   'ready',              # Building 1000 Site 1
                                   'ready',              # Aisle <spell>1011</spell>
                                   'ready',              # Bay 1000
                                   '1000',               # 1011
                                   '1!',                 # item 1010 site 1, Put 1, 
                                   '144',                # Container?
                                   'no',                 # 144, correct?
                                   'talkman help',       # Container?
                                   'review containers',  # Container?
                                   'yes',                # review containers, correct?
                                   'talkman help',       # 0 0 0 0 0 0 0 1 2 9
                                   'ready',              # 0 0 0 0 0 0 0 1 2 9
                                   'ready',              # 0 0 0 0 0 0 0 1 3 4
                                   'ready',              # 0 0 0 0 0 0 0 1 4 4
                                   '134',                # Container?
                                   'yes',                # 134, correct?
                                   'no',                 # Assignment complete.  Do you have residuals?
                                   '-')                  # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              'Getting work.',
                              'Assignment has 6 slot locations 5 items, and a total expected residual of 33, say ready',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'item 1010 site 1, Put 15, boxes',
                              'Is this an overpack?',
                              'Can only overpack 10 items.',
                              'item 1010 site 1, Put 15, boxes',
                              'Is this an overpack?',
                              'Container?',
                              '432, correct?',
                              'Container 432 not found or is already closed.',
                              'Container?',
                              '136, correct?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1008 site 1, Put 4, boxes',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'item 1000 site 1, Put 14, boxes',
                              'Is this an overpack?',
                              'Overpacking not allowed.',
                              'item 1000 site 1, Put 14, boxes',
                              'Opening new container.',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'item 1000 site 1, Put 14, boxes',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'item 1011 site 1, Put 12, boxes',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'item 1010 site 1, Put 13, boxes',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'item 1004 site 1, Put 11, eaches',
                              'Is this an overpack?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready',
                              'License?',
                              'Getting work.',
                              'Assignment has 5 slot locations 4 items, and a total expected residual of 31, say ready',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'item 1009 site 1, Put 13, eaches',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'item 1005 site 1, Put 3, eaches',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1007 site 1, Put 12, cases',
                              'Is this an overpack?',
                              'Overpacking not allowed.',
                              'item 1007 site 1, Put 12, cases',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'item 1007 site 1, Put 3, eaches',
                              'Is this an overpack?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'item 1010 site 1, Put 1, ',
                              'Container?',
                              '144, correct?',
                              'Container?',
                              'Speak or scan the container number or say review containers, repick skips, description, license, pass assignment, item number, location, new container,  or quantity',
                              'Container?',
                              'review containers, correct?',
                              '0 0 0 0 0 0 0 1 2 9',
                              '[standard help]',
                              '0 0 0 0 0 0 0 1 2 9',
                              '0 0 0 0 0 0 0 1 3 4',
                              '0 0 0 0 0 0 0 1 4 4',
                              'Container?',
                              '134, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1027', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '548'],
                                      ['prTaskLUTPtsPut', '548', '11', '1010', '592', '24', '', '1001027', '0'],
                                      ['prTaskLUTPtsPut', '548', '11', '1010', '592', '24', '432', '1001027', '0'],
                                      ['prTaskLUTPtsPut', '548', '11', '1010', '592', '24', '136', '1001027', '0'],
                                      ['prTaskLUTPtsPut', '548', '9', '1008', '593', '4', '', '1001027', '0'],
                                      ['prTaskLUTPtsPut', '548', '2', '1000', '594', '14', '', '1001027', '0'],
                                      ['prTaskLUTPtsContainer', '2', '2', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '548', '1', '2', '0000000145'],
                                      ['prTaskLUTPtsPut', '548', '2', '1000', '594', '14', '', '1001027', '0'],
                                      ['prTaskLUTPtsPut', '548', '3', '1011', '595', '12', '', '1001027', '0'],
                                      ['prTaskLUTPtsPut', '548', '5', '1010', '596', '13', '', '1001027', '0'],
                                      ['prTaskLUTPtsPut', '548', '7', '1004', '597', '15', '', '1001027', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '548'],
                                      ['prTaskLUTPtsStopAssignment', '548'],
                                      ['prTaskLUTPtsVerifyLicense', '1021', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '549'],
                                      ['prTaskLUTPtsPut', '549', '3', '1009', '560', '13', '', '1001021', '0'],
                                      ['prTaskLUTPtsPut', '549', '8', '1005', '561', '3', '', '1001021', '0'],
                                      ['prTaskLUTPtsPut', '549', '9', '1007', '562', '12', '', '1001021', '0'],
                                      ['prTaskLUTPtsPut', '549', '4', '1007', '563', '10', '', '1001021', '0'],
                                      ['prTaskLUTPtsPut', '549', '12', '1010', '564', '1', '', '1001021', '0'],
                                      ['prTaskLUTPtsContainer', '0', '12', ''],
                                      ['prTaskLUTPtsPut', '549', '12', '1010', '564', '1', '134', '1001021', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '549'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=1001021'],
                                      ['prTaskLUTPtsStopAssignment', '549'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
