##TestLink ID 109452 :: Test Case Test speaking cancel when reviewing a license

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109452(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109452(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,1,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003007,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003037,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003047,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003057,4,Multiple records exist for license entered by operator.\r\n'
                                 '3082057,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003037,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003007,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003047,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003057,4,Multiple records exist for license entered by operator.\r\n'
                                 '3082057,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',   # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',    # Password?
                                   '8',       # Function?
                                   'yes',     # Put To Store, correct?
                                   '3!',      # Region?
                                   'yes',     # Region 3, correct?
                                   'ready',   # To start Put to store work, say ready
                                   'ready',   # Go to flow through location
                                   '7',       # License?
                                   'yes',     # 7, correct?
                                   'yes',     # Found 5 licenses. Do you want to review the list?
                                   'no',      # 3003007, select?
                                   'yes',     # 3003037, select?
                                   '7',       # License?
                                   'yes',     # 7, correct?
                                   'yes',     # Found 4 licenses. Do you want to review the list?
                                   'cancel',  # 3003007, select?
                                   'no',      # cancel this license request?
                                   'cancel',  # 3003047, select?
                                   'yes',     # cancel this license request?
                                   '-')       # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '7, correct?',
                              'Found 5 licenses. Do you want to review the list?',
                              '3003007, select?',
                              '3003037, select?',
                              'License?',
                              '7, correct?',
                              'Found 4 licenses. Do you want to review the list?',
                              '3003007, select?',
                              'cancel this license request?',
                              '3003047, select?',
                              'cancel this license request?',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '7', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003037', '0'],
                                      ['prTaskLUTPtsVerifyLicense', '7', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
