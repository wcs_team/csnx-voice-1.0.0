##TestLink ID 109608 :: Test Case Verify check digit entry at slot prompt

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109608(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109608(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,2,1,5,0,0,5,0,4,1,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002051,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('386,3,4,35,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,718,11,2002051,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,15,        0,1011,Item 1011 Site 1,eaches,1,11,0,\r\n'
                                 'N,719,4,2002051,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,13,        0,1008,Item 1008 Site 1,boxes,1,8,0,\r\n'
                                 'N,720,3,2002051,,1002                                              ,Bay 1002                                          ,1002                                              ,,1002,2222222222,2,        0,1004,Item 1004 Site 1,,1,4,0,\r\n'
                                 'N,721,11,2002051,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,4,        0,1012,Item 1012 Site 1,,1,12,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002051,1008,Item 1008 Site 1,8,N,0,\r\n'
                                 '2002051,1012,Item 1012 Site 1,12,N,0,\r\n'
                                 '2002051,1011,Item 1011 Site 1,11,N,0,\r\n'
                                 '2002051,1004,Item 1004 Site 1,4,N,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '2!',       # Region?
                                   'yes',      # Region 2, correct?
                                   'ready',    # To start Put to store work, say ready
                                   '02051',    # License?
                                   'no more',  # License?
                                   'ready',    # Assignment has 3 slot locations 4 items, and a total expected residual of 35, say ready
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle 1010
                                   'ready',    # Bay 1000
                                   'ready',    # 1010
                                   '1111',     # 1010
                                   '1000',     # 1010
                                   '15!',      # item 1011 site 1, Put 15, eaches
                                   '00037',    # Container?
                                   'yes',      # 00037, correct?
                                   'ready',    # Building 1003 Site 1
                                   'ready',    # Aisle 1003
                                   '1003',     # 1003
                                   '13!',      # item 1008 site 1, Put 13, boxes
                                   'ready',    # Aisle 1002
                                   'ready',    # Bay 1002
                                   'ready',    # 1002
                                   '2!',       # item 1004 site 1, Put 2, 
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle 1010
                                   'ready',    # Bay 1000
                                   '1000',     # 1010
                                   '4!',       # item 1012 site 1, Put 4, 
                                   '00037',    # Container?
                                   'yes',      # 00037, correct?
                                   '-')        # Assignment complete.  Do you have residuals?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'License?',
                              'Getting work.',
                              'Assignment has 3 slot locations 4 items, and a total expected residual of 35, say ready',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'wrong 1111, try again',
                              'item 1011 site 1, Put 15, eaches',
                              'Container?',
                              '00037, correct?',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'item 1008 site 1, Put 13, boxes',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'item 1004 site 1, Put 2, ',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'item 1012 site 1, Put 4, ',
                              'Container?',
                              '00037, correct?',
                              'Assignment complete.  Do you have residuals?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '02051', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '386'],
                                      ['prTaskLUTPtsPut', '386', '11', '1011', '718', '15', '', '2002051', '0'],
                                      ['prTaskLUTPtsPut', '386', '11', '1011', '718', '15', '00037', '2002051', '0'],
                                      ['prTaskLUTPtsPut', '386', '4', '1008', '719', '13', '', '2002051', '0'],
                                      ['prTaskLUTPtsPut', '386', '3', '1004', '720', '2', '', '2002051', '0'],
                                      ['prTaskLUTPtsPut', '386', '11', '1012', '721', '4', '', '2002051', '0'],
                                      ['prTaskLUTPtsPut', '386', '11', '1012', '721', '4', '00037', '2002051', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '386'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
