##TestLink ID 109660 :: Test Case Verify partial command with one open container allowed

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109660(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109660(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,1,1,4,0,0,3,0,4,4,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2082055,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('494,3,4,28,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,1009,7,2082055,,1006                                              ,,1006                                              ,1006,1006,1006,11,        0,1001,Item 1001 Site 1,,1,1,0,\r\n'
                                 'N,1010,10,2082055,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,10,        0,1004,Item 1004 Site 1,boxes,1,4,0,\r\n'
                                 'N,1011,10,2082055,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,11,        0,1009,Item 1009 Site 1,cases,1,9,0,\r\n'
                                 'N,1012,9,2082055,,1008                                              ,,1008                                              ,1008,1008,1008,14,        0,1014,Item 1014 Site 1,eaches,1,14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('7,0000000123,123,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('7,0000000124,124,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('10,0000000125,125,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('10,0000000126,126,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('10,0000000127,127,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2082055,1014,Item 1014 Site 1,14,N,0,\r\n'
                                 '2082055,1009,Item 1009 Site 1,9,N,0,\r\n'
                                 '2082055,1004,Item 1004 Site 1,4,N,0,\r\n'
                                 '2082055,1001,Item 1001 Site 1,1,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '8',      # Function?
                                   'yes',    # Put To Store, correct?
                                   '2!',     # Region?
                                   'yes',    # Region 2, correct?
                                   'ready',  # To start Put to store work, say ready
                                   '2055',   # License?
                                   'ready',  # Assignment has 3 slot locations 4 items, and a total expected residual of 28, say ready
                                   'ready',  # Aisle <spell>1006</spell>
                                   '1006',   # 1006
                                   '3!',     # item 1001 site 1, Put 11, 
                                   'yes',    # Is this a partial?
                                   '1!',     # Printer?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Aisle <spell>1006</spell>
                                   '1006',   # 1006
                                   '4!',     # item 1001 site 1, Put 8, 
                                   'yes',    # Is this a partial?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Aisle <spell>1006</spell>
                                   '1006',   # 1006
                                   '4!',     # item 1001 site 1, Put 4, 
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1009</spell>
                                   'ready',  # Bay 1000
                                   '1000',   # 1009
                                   '3!',     # item 1004 site 1, Put 10, boxes
                                   'yes',    # Is this a partial?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1009</spell>
                                   'ready',  # Bay 1000
                                   '1000',   # 1009
                                   '4!',     # item 1004 site 1, Put 7, boxes
                                   'yes',    # Is this a partial?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1009</spell>
                                   'ready',  # Bay 1000
                                   '1000',   # 1009
                                   '3!',     # item 1004 site 1, Put 3, boxes
                                   '1000',   # 1009
                                   '3!',     # item 1009 site 1, Put 11, cases
                                   'yes',    # Is this a partial?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1009</spell>
                                   'ready',  # Bay 1000
                                   '1000',   # 1009
                                   '8!',     # item 1009 site 1, Put 8, cases
                                   'ready',  # Aisle <spell>1008</spell>
                                   '1008',   # 1008
                                   '14!',    # item 1014 site 1, Put 14, eaches
                                   'no',     # Assignment complete.  Do you have residuals?
                                   'ready',  # For next assignment, say ready
                                   '-')      # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'Getting work.',
                              'Assignment has 3 slot locations 4 items, and a total expected residual of 28, say ready',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'item 1001 site 1, Put 11, ',
                              'Is this a partial?',
                              'Opening new container.',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'item 1001 site 1, Put 8, ',
                              'Is this a partial?',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'item 1001 site 1, Put 4, ',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'item 1004 site 1, Put 10, boxes',
                              'Is this a partial?',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'item 1004 site 1, Put 7, boxes',
                              'Is this a partial?',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'item 1004 site 1, Put 3, boxes',
                              '1009',
                              'item 1009 site 1, Put 11, cases',
                              'Is this a partial?',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'item 1009 site 1, Put 8, cases',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1014 site 1, Put 14, eaches',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '2055', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '494'],
                                      ['prTaskLUTPtsPut', '494', '7', '1001', '1009', '3', '', '2082055', '1'],
                                      ['prTaskLUTPtsContainer', '2', '7', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '494', '1', '7', '0000000123'],
                                      ['prTaskLUTPtsPut', '494', '7', '1001', '1009', '4', '', '2082055', '1'],
                                      ['prTaskLUTPtsContainer', '2', '7', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '494', '1', '7', '0000000124'],
                                      ['prTaskLUTPtsPut', '494', '7', '1001', '1009', '4', '', '2082055', '0'],
                                      ['prTaskLUTPtsPut', '494', '10', '1004', '1010', '3', '', '2082055', '1'],
                                      ['prTaskLUTPtsContainer', '2', '10', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '494', '1', '10', '0000000125'],
                                      ['prTaskLUTPtsPut', '494', '10', '1004', '1010', '4', '', '2082055', '1'],
                                      ['prTaskLUTPtsContainer', '2', '10', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '494', '1', '10', '0000000126'],
                                      ['prTaskLUTPtsPut', '494', '10', '1004', '1010', '3', '', '2082055', '0'],
                                      ['prTaskLUTPtsPut', '494', '10', '1009', '1011', '3', '', '2082055', '1'],
                                      ['prTaskLUTPtsContainer', '2', '10', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '494', '1', '10', '0000000127'],
                                      ['prTaskLUTPtsPut', '494', '10', '1009', '1011', '8', '', '2082055', '0'],
                                      ['prTaskLUTPtsPut', '494', '9', '1014', '1012', '14', '', '2082055', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '494'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=2082055'],
                                      ['prTaskLUTPtsStopAssignment', '494'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
