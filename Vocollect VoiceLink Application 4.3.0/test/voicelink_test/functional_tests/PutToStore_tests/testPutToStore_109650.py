##TestLink ID 109650 :: Test Case Test when exp and unexp resid loc are different/multiple licenses/mult item
##TestLink ID 109662 :: Test Case Test flow when exp resid loc and unexp resid loc different, qty <> exp qty

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109650(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109650(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,1,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003019,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003029,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003039,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003049,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003059,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003019,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003029,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003039,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003049,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003059,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',1231,Reserved license 3003019 and license 3003029 contain the same item number 1010.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('373,6,6,47,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,860,5,3003019,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,12,        0,1013,Item 1013 Site 1,boxes,1,13,0,\r\n'
                                 'N,861,7,3003019,,1006                                              ,,1006                                              ,1006,1006,1006,11,        0,1010,Item 1010 Site 1,cases,1,10,0,\r\n'
                                 'N,862,4,3003019,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,14,        0,1008,Item 1008 Site 1,cases,0,8,0,\r\n'
                                 'N,863,8,3003019,,1007                                              ,,1007                                              ,1007,1007,1007,12,        0,1009,Item 1009 Site 1,boxes,0,9,0,\r\n'
                                 'N,864,6,3003019,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,13,        0,1007,Item 1007 Site 1,eaches,1,7,0,\r\n'
                                 'N,865,10,3003019,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,4,        0,1000,Item 1000 Site 1,boxes,1,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003019,1008,Item 1008 Site 1,8,N,0,\r\n'
                                 '3003019,1009,Item 1009 Site 1,9,N,0,\r\n'
                                 '3003019,1007,Item 1007 Site 1,7,N,0,\r\n'
                                 '3003019,1013,Item 1013 Site 1,13,N,0,\r\n'
                                 '3003019,1000,Item 1000 Site 1,0,N,0,\r\n'
                                 '3003019,1010,Item 1010 Site 1,10,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003029,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003039,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003049,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003059,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003029,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('374,2,2,14,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,917,2,3003029,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,4,        0,1014,Item 1014 Site 1,boxes,1,14,0,\r\n'
                                 'N,918,10,3003029,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,1,        0,1010,Item 1010 Site 1,boxes,1,0,0,\r\n'
                                 'N,919,10,3003029,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,4,        0,1014,Item 1014 Site 1,,1,14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003029,1010,Item 1010 Site 1,0,N,0,\r\n'
                                 '3003029,1014,Item 1014 Site 1,14,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '3!',       # Region?
                                   'yes',      # Region 3, correct?
                                   'ready',    # To start Put to store work, say ready
                                   'ready',    # Go to flow through location
                                   '9',        # License?
                                   'yes',      # 9, correct?
                                   'yes',      # Found 5 licenses. Do you want to review the list?
                                   'yes',      # 3003019, select?
                                   '9',        # License?
                                   'yes',      # 9, correct?
                                   'yes',      # Found 4 licenses. Do you want to review the list?
                                   'yes',      # 3003029, select?
                                   'ready',    # Reserved license 3003019 and license 3003029 contain the same item number 1010., To continue say ready
                                   'no more',  # License?
                                   'ready',    # Assignment has 6 slot locations 6 items, and a total expected residual of 47, say ready
                                   'ready',    # Building 1004 Site 1
                                   'ready',    # Aisle 1004
                                   '1004',     # 1004
                                   '12!',      # item 1013 site 1, Put 12, boxes
                                   '2',        # Container?
                                   'yes',      # 2, correct?
                                   'ready',    # Aisle 1006
                                   '1006',     # 1006
                                   '11!',      # item 1010 site 1, Put 11, cases
                                   '4',        # Container?
                                   'yes',      # 4, correct?
                                   'ready',    # Building 1003 Site 1
                                   'ready',    # Aisle 1003
                                   '1003',     # 1003
                                   '14!',      # item 1008 site 1, Put 14, cases
                                   'ready',    # Aisle 1007
                                   '1007',     # 1007
                                   '12!',      # item 1009 site 1, Put 12, boxes
                                   'ready',    # Building 1005 Site 1
                                   'ready',    # Aisle 1005
                                   '1005',     # 1005
                                   '13!',      # item 1007 site 1, Put 13, eaches
                                   '3',        # Container?
                                   'yes',      # 3, correct?
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle 1009
                                   'ready',    # Bay 1000
                                   '1009',     # 1009
                                   '1000',     # 1009
                                   '4!',       # item 1000 site 1, Put 4, boxes
                                   '5',        # Container?
                                   'yes',      # 5, correct?
                                   'yes',      # Assignment complete.  Do you have residuals?
                                   '8!',       # How many for item Item 1008 Site 1?
                                   'yes',      # 8, correct?
                                   '9!',       # How many for item Item 1009 Site 1?
                                   'yes',      # 9, correct?
                                   '7!',       # How many for item Item 1007 Site 1?
                                   'yes',      # 7, correct?
                                   '13!',      # How many for item Item 1013 Site 1?
                                   'yes',      # 13, correct?
                                   '0!',       # How many for item Item 1000 Site 1?
                                   '\yes',     # 0, correct?
                                   'yes',      # 0, correct?
                                   '10!',      # How many for item Item 1010 Site 1?
                                   'yes',      # 10, correct?
                                   'ready',    # return residual to residual return location, say ready
                                   '00',       # Check Digit?
                                   'ready',    # For next assignment, say ready
                                   '9',        # License?
                                   'yes',      # 9, correct?
                                   'yes',      # Found 4 licenses. Do you want to review the list?
                                   'yes',      # 3003029, select?
                                   'no more',  # License?
                                   'ready',    # Assignment has 2 slot locations 2 items, and a total expected residual of 14, say ready
                                   'ready',    # Aisle 1001
                                   'ready',    # Bay 1001
                                   '1001',     # 1001
                                   '4!',       # item 1014 site 1, Put 4, boxes
                                   '2',        # Container?
                                   'yes',      # 2, correct?
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle 1009
                                   'ready',    # Bay 1000
                                   '1009',     # 1009
                                   '1000',     # 1009
                                   '1!',       # item 1010 site 1, Put 1, boxes
                                   '5',        # Container?
                                   'yes',      # 5, correct?
                                   '1000',     # 1009
                                   '4!',       # item 1014 site 1, Put 4, 
                                   '5',        # Container?
                                   'yes',      # 5, correct?
                                   'yes',      # Assignment complete.  Do you have residuals?
                                   '10!',      # How many for item Item 1010 Site 1?
                                   'yes',      # 10, correct?
                                   'ready',    # return residual to unexpected residual return location, say ready
                                   '11',       # Check Digit?
                                   '-')        # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '9, correct?',
                              'Found 5 licenses. Do you want to review the list?',
                              '3003019, select?',
                              'License?',
                              '9, correct?',
                              'Found 4 licenses. Do you want to review the list?',
                              '3003029, select?',
                              'Reserved license 3003019 and license 3003029 contain the same item number 1010., To continue say ready',
                              'License?',
                              'Getting work.',
                              'Assignment has 6 slot locations 6 items, and a total expected residual of 47, say ready',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'item 1013 site 1, Put 12, boxes',
                              'Container?',
                              '2, correct?',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'item 1010 site 1, Put 11, cases',
                              'Container?',
                              '4, correct?',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'item 1008 site 1, Put 14, cases',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'item 1009 site 1, Put 12, boxes',
                              'Building 1005 Site 1',
                              'Aisle <spell>1005</spell>',
                              '1005',
                              'item 1007 site 1, Put 13, eaches',
                              'Container?',
                              '3, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1000 site 1, Put 4, boxes',
                              'Container?',
                              '5, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'Confirm residuals for license 3003019',
                              'How many for item Item 1008 Site 1?',
                              '8, correct?',
                              'How many for item Item 1009 Site 1?',
                              '9, correct?',
                              'How many for item Item 1007 Site 1?',
                              '7, correct?',
                              'How many for item Item 1013 Site 1?',
                              '13, correct?',
                              'How many for item Item 1000 Site 1?',
                              '0, correct?',
                              'How many for item Item 1010 Site 1?',
                              '10, correct?',
                              'return residual to residual return location, say ready',
                              'Check Digit?',
                              'For next assignment, say ready',
                              'License?',
                              '9, correct?',
                              'Found 4 licenses. Do you want to review the list?',
                              '3003029, select?',
                              'License?',
                              'Getting work.',
                              'Assignment has 2 slot locations 2 items, and a total expected residual of 14, say ready',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'item 1014 site 1, Put 4, boxes',
                              'Container?',
                              '2, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1010 site 1, Put 1, boxes',
                              'Container?',
                              '5, correct?',
                              '1009',
                              'item 1014 site 1, Put 4, ',
                              'Container?',
                              '5, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'Confirm residuals for license 3003029',
                              'How many for item Item 1010 Site 1?',
                              '10, correct?',
                              'return residual to unexpected residual return location, say ready',
                              'Check Digit?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '9', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003019', '0'],
                                      ['prTaskLUTPtsVerifyLicense', '9', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003029', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '373'],
                                      ['prTaskLUTPtsPut', '373', '5', '1013', '860', '12', '', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '5', '1013', '860', '12', '2', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '7', '1010', '861', '11', '', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '7', '1010', '861', '11', '4', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '4', '1008', '862', '14', '', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '8', '1009', '863', '12', '', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '6', '1007', '864', '13', '', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '6', '1007', '864', '13', '3', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '10', '1000', '865', '4', '', '3003019', '0'],
                                      ['prTaskLUTPtsPut', '373', '10', '1000', '865', '4', '5', '3003019', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '373'],
                                      ['prTaskLUTPtsStopAssignment', '373'],
                                      ['prTaskLUTPtsVerifyLicense', '9', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003029', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '374'],
                                      ['prTaskLUTPtsPut', '374', '2', '1014', '917', '4', '', '3003029', '0'],
                                      ['prTaskLUTPtsPut', '374', '2', '1014', '917', '4', '2', '3003029', '0'],
                                      ['prTaskLUTPtsPut', '374', '10', '1010', '918', '1', '', '3003029', '0'],
                                      ['prTaskLUTPtsPut', '374', '10', '1010', '918', '1', '5', '3003029', '0'],
                                      ['prTaskLUTPtsPut', '374', '10', '1014', '919', '4', '', '3003029', '0'],
                                      ['prTaskLUTPtsPut', '374', '10', '1014', '919', '4', '5', '3003029', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '374'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.unexpectedresidual', '0', 'task.licenseNumber=3003029|task.itemNumber=1010'],
                                      ['prTaskLUTPtsStopAssignment', '374'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
