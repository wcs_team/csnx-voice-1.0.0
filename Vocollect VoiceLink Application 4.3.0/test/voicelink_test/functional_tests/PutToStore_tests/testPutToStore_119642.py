##TestLink ID 119642 :: Test Case Verify sign off permitted if enabled by region setting

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109444(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109444(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,3,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003049,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003059,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003049,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('527,3,1,44,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,400,10,3003049,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,6,        0,1015,Item 1015 Site 1,boxes,1,44,0,\r\n'
                                 'N,401,7,3003049,,1006                                              ,,1006                                              ,1006,1006,1006,1,        0,1015,Item 1015 Site 1,eaches,1,44,0,\r\n'
                                 'N,402,6,3003049,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,1,        0,1015,Item 1015 Site 1,boxes,0,44,0,\r\n'
                                 'N,403,10,3003049,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,10,        0,1015,Item 1015 Site 1,,0,44,0,\r\n'
                                 'N,404,10,3003049,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,3,        0,1015,Item 1015 Site 1,eaches,0,44,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',     # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',      # Password?
                                   '8!',        # Function?
                                   'yes',       # Put To Store, correct?
                                   '3!',        # Region?
                                   'yes',       # Region 3, correct?
                                   'ready',     # To start Put to store work, say ready
                                   'ready',     # Go to flow through location
                                   '9',         # License?
                                   'yes',       # 9, correct?
                                   'signoff',   # Found 2 licenses. Do you want to review the list?
                                   'sign off',  # Found 2 licenses. Do you want to review the list?
                                   'no',        # Sign off, correct?
                                   'yes',       # Found 2 licenses. Do you want to review the list?
                                   'sign off',  # 3003049, select?
                                   'no',        # Sign off, correct?
                                   'yes',       # 3003049, select?
                                   'sign off',  # License?
                                   'no',        # Sign off, correct?
                                   'no more',   # License?
                                   'sign off',  # Assignment has 3 slot locations 1 item, and a total expected residual of 44, say ready
                                   'no',        # Sign off, correct?
                                   'ready',     # Assignment has 3 slot locations 1 item, and a total expected residual of 44, say ready
                                   'sign off',  # Building 1000 Site 1
                                   'no',        # Sign off, correct?
                                   'ready',     # Building 1000 Site 1
                                   'sign off',  # Aisle <spell>1009</spell>
                                   'no',        # Sign off, correct?
                                   'ready',     # Aisle <spell>1009</spell>
                                   'sign off',  # Bay 1000
                                   'no',        # Sign off, correct?
                                   'ready',     # Bay 1000
                                   'sign off',  # 1009
                                   'no',        # Sign off, correct?
                                   '1009',      # 1009
                                   'sign off',  # wrong 1009, try again
                                   'no',        # Sign off, correct?
                                   '1000',      # wrong 1009, try again
                                   'sign off',  # Put 6, boxes
                                   'no',        # Sign off, correct?
                                   '6!',        # Put 6, boxes
                                   'sign off',  # Aisle <spell>1006</spell>
                                   'yes',       # Sign off, correct?
                                   '-')         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '9, correct?',
                              'Found 2 licenses. Do you want to review the list?',
                              'Sign off, correct?',
                              'Found 2 licenses. Do you want to review the list?',
                              '3003049, select?',
                              'Sign off, correct?',
                              '3003049, select?',
                              'License?',
                              'Sign off, correct?',
                              'License?',
                              'Getting work.',
                              'Assignment has 3 slot locations 1 item, and a total expected residual of 44, say ready',
                              'Sign off, correct?',
                              'Assignment has 3 slot locations 1 item, and a total expected residual of 44, say ready',
                              'Building 1000 Site 1',
                              'Sign off, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Sign off, correct?',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              'Sign off, correct?',
                              'Bay 1000',
                              '1009',
                              'Sign off, correct?',
                              '1009',
                              'wrong 1009, try again',
                              'Sign off, correct?',
                              'wrong 1009, try again',
                              'Put 6, boxes',
                              'Sign off, correct?',
                              'Put 6, boxes',
                              'Aisle <spell>1006</spell>',
                              'Sign off, correct?',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '9', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003049', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '527'],
                                      ['prTaskLUTPtsPut', '527', '10', '1015', '400', '6', '', '3003049', '0'],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
