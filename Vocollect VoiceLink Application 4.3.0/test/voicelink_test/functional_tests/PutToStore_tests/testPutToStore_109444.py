##TestLink ID 109444 :: Test Case Verify operator is correctly directed to put locations

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109444(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109444(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\n'
                                 '2,Region 2,0,\n'
                                 '3,Region 3,0,\n'
                                 '4,Region 4,0,\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,5,1,5,0,0,5,0,4,1,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\n'
                                 '\n')
        self.set_server_response(',0,\n'
                                 '\n')
        self.set_server_response(',0,\n'
                                 '\n')
        self.set_server_response(',0,\n'
                                 '\n')
        self.set_server_response('283,8,7,76,residual return location,00,residual return location,11,-1,-1,0,\n'
                                 '\n')
        self.set_server_response('N,651,1,2002038,,1000                                              ,Bay 1000                                          ,1000                                              ,1000,1000,1000000000,3,        0,1008,Item 1008 Site 1,boxes,0,8,0,\n'
                                 'N,647,2,2002038,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,4,        0,1015,Item 1015 Site 1,cases,1,15,0,\n'
                                 'N,697,2,2002038,,1001                                              ,Bay 1002                                          ,1002                                              ,1002,1002,1111111111,5,        0,1015,Item 1015 Site 1,cases,1,15,0,\n'
                                 'N,646,7,2002037,,1006                                              ,,1006                                              ,1006,1006,1006,4,        0,1011,Item 1011 Site 1,,1,11,0,\n'
                                 'N,645,9,2002037,,1008                                              ,,1008                                              ,1008,1008,1008,4,        0,1014,Item 1014 Site 1,,0,14,0,\n'
                                 'N,644,10,2002037,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,3,        0,1005,Item 1005 Site 1,eaches,1,5,0,\n'
                                 'N,649,12,2002038,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,15,        0,1010,Item 1010 Site 1,eaches,0,10,0,\n'
                                 'N,652,12,2002038,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,1,        0,1015,Item 1015 Site 1,cases,1,15,0,\n'
                                 'N,650,4,2002038,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,1,        0,1008,Item 1008 Site 1,cases,0,8,0,\n'
                                 'N,648,5,2002038,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,15,        0,1013,Item 1013 Site 1,,0,13,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('2002037,1014,Item 1014 Site 1,14,N,0,\n'
                                 '2002038,1015,Item 1015 Site 1,15,N,0,\n'
                                 '2002037,1005,Item 1005 Site 1,5,N,0,\n'
                                 '2002038,1013,Item 1013 Site 1,13,N,0,\n'
                                 '2002038,1010,Item 1010 Site 1,10,N,0,\n'
                                 '2002037,1011,Item 1011 Site 1,11,N,0,\n'
                                 '2002038,1008,Item 1008 Site 1,8,N,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8!',       # Function?
                                   'yes',      # put to store, correct?
                                   '2',        # Region?
                                   'yes',      # Region 2, correct?
                                   'ready',    # To start Put to store work, say ready
                                   '02037',    # License?
                                   '02038',    # License?
                                   'no more',  # License?
                                   'ready',    # Assignment has 8 slot locations 7 items, and a total expected residual of 76, say ready
                                   'ready',    # Aisle <spell>1000</spell>
                                   'ready',    # Bay 1000
                                   '1000',     # 1000
                                   '3!',       # item 1008 site 1, Put 3, boxes
                                   'ready',    # Aisle <spell>1001</spell>
                                   'ready',    # Bay 1001
                                   '1001',     # 1001
                                   '4!',       # item 1015 site 1, Put 4, cases
                                   'ready',    # Bay 1002
                                   '1002',     # 1002
                                   '5!',       # item 1015 site 1, Put 5, cases
                                   'ready',    # Aisle <spell>1006</spell>
                                   '1006',     # 1006
                                   '4!',       # item 1011 site 1, Put 4, 
                                   'ready',    # Aisle <spell>1008</spell>
                                   '1008',     # 1008
                                   '4!',       # item 1014 site 1, Put 4, 
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle <spell>1009</spell>
                                   'ready',    # Bay 1000
                                   '1009',     # 1009
                                   '1000',     # wrong 1009, try again
                                   '3!',       # item 1005 site 1, Put 3, eaches
                                   'ready',    # Aisle <spell>1011</spell>
                                   'ready',    # Bay 1000
                                   '1000',     # 1011
                                   '15!',      # item 1010 site 1, Put 15, eaches
                                   '1000',     # 1011
                                   '1!',       # item 1015 site 1, Put 1, cases
                                   'ready',    # Building 1003 Site 1
                                   'ready',    # Aisle <spell>1003</spell>
                                   '1003',     # 1003
                                   '1!',       # item 1008 site 1, Put 1, cases
                                   'ready',    # Building 1004 Site 1
                                   'ready',    # Aisle <spell>1004</spell>
                                   '1004',     # 1004
                                   '15!',      # item 1013 site 1, Put 15, 
                                   'no',       # Assignment complete.  Do you have residuals?
                                   'ready',    # For next assignment, say ready
                                   '-')        # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'put to store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'License?',
                              'License?',
                              'Getting work.',
                              'Assignment has 8 slot locations 7 items, and a total expected residual of 76, say ready',
                              'Aisle <spell>1000</spell>',
                              'Bay 1000',
                              '1000',
                              'item 1008 site 1, Put 3, boxes',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'item 1015 site 1, Put 4, cases',
                              'Bay 1002',
                              '1002',
                              'item 1015 site 1, Put 5, cases',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'item 1011 site 1, Put 4, ',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1014 site 1, Put 4, ',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1005 site 1, Put 3, eaches',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'item 1010 site 1, Put 15, eaches',
                              '1011',
                              'item 1015 site 1, Put 1, cases',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'item 1008 site 1, Put 1, cases',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'item 1013 site 1, Put 15, ',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '02037', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '02038', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '283'],
                                      ['prTaskLUTPtsPut', '283', '1', '1008', '651', '3', '', '2002038', '0'],
                                      ['prTaskLUTPtsPut', '283', '2', '1015', '647', '4', '', '2002038', '0'],
                                      ['prTaskLUTPtsPut', '283', '2', '1015', '697', '5', '', '2002038', '0'],
                                      ['prTaskLUTPtsPut', '283', '7', '1011', '646', '4', '', '2002037', '0'],
                                      ['prTaskLUTPtsPut', '283', '9', '1014', '645', '4', '', '2002037', '0'],
                                      ['prTaskLUTPtsPut', '283', '10', '1005', '644', '3', '', '2002037', '0'],
                                      ['prTaskLUTPtsPut', '283', '12', '1010', '649', '15', '', '2002038', '0'],
                                      ['prTaskLUTPtsPut', '283', '12', '1015', '652', '1', '', '2002038', '0'],
                                      ['prTaskLUTPtsPut', '283', '4', '1008', '650', '1', '', '2002038', '0'],
                                      ['prTaskLUTPtsPut', '283', '5', '1013', '648', '15', '', '2002038', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '283'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=2002037'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=2002038'],
                                      ['prTaskLUTPtsStopAssignment', '283'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
