##TestLink ID 109636 :: Test Case Test when assignment has no residual/user says no to resid prompt

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109636(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109636(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,1,1,1,0,1,1,3,1,0,2,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001056,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('363,5,1,0,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,126,11,1001056,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,16,        0,1010,Item 1010 Site 1,,0,0,0,\r\n'
                                 'N,127,8,1001056,,1007                                              ,,1007                                              ,1007,1007,1007,12,        0,1010,Item 1010 Site 1,,0,0,0,\r\n'
                                 'N,128,2,1001056,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,6,        0,1010,Item 1010 Site 1,boxes,0,0,0,\r\n'
                                 'N,129,5,1001056,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,10,        0,1010,Item 1010 Site 1,boxes,1,0,0,\r\n'
                                 'N,130,9,1001056,,1008                                              ,,1008                                              ,1008,1008,1008,6,        0,1010,Item 1010 Site 1,boxes,0,0,0,\r\n'
                                 'N,131,2,1001056,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,14,        0,1010,Item 1010 Site 1,,0,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001056,1010,Item 1010 Site 1,0,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',    # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',     # Password?
                                   '8',        # Function?
                                   'yes',      # Put To Store, correct?
                                   '1!',       # Region?
                                   'yes',      # Region 1, correct?
                                   'ready',    # To start Put to store work, say ready
                                   'ready',    # Go to flow through location
                                   '1001056',  # License?
                                   'ready',    # License?
                                   'yes',      # 1001056, correct?
                                   'ready',    # Assignment has 5 slot locations 1 item, say ready
                                   'ready',    # Building 1000 Site 1
                                   'ready',    # Aisle 1010
                                   'ready',    # Bay 1000
                                   '1010',     # 1010
                                   '1000',     # 1010
                                   '16!',      # Put 16, 
                                   '037',      # Container?
                                   'yes',      # 037, correct?
                                   'ready',    # Aisle 1007
                                   'ready',    # 1007
                                   '1007',     # 1007
                                   '12!',      # Put 12, 
                                   '043',      # Container?
                                   'yes',      # 043, correct?
                                   'ready',    # Aisle 1001
                                   'ready',    # Bay 1001
                                   '1001',     # 1001
                                   '6!',       # Put 6, boxes
                                   '032',      # Container?
                                   'yes',      # 032, correct?
                                   'ready',    # Building 1004 Site 1
                                   'ready',    # Aisle 1004
                                   '1004',     # 1004
                                   '10!',      # Put 10, boxes
                                   '035',      # Container?
                                   'yes',      # 035, correct?
                                   'ready',    # Aisle 1008
                                   '1008',     # 1008
                                   '6!',       # Put 6, boxes
                                   '076',      # Container?
                                   'yes',      # 076, correct?
                                   'ready',    # Aisle 1001
                                   'ready',    # Bay 1001
                                   '1001',     # 1001
                                   '14!',      # Put 14, 
                                   '032',      # Container?
                                   'yes',      # 032, correct?
                                   'no',       # Assignment complete.  Do you have residuals?
                                   '-')        # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '1001056, correct?',
                              'Getting work.',
                              'Assignment has 5 slot locations 1 item, say ready',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'wrong 1010, try again',
                              'Put 16, ',
                              'Container?',
                              '037, correct?',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'Put 12, ',
                              'Container?',
                              '043, correct?',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Put 6, boxes',
                              'Container?',
                              '032, correct?',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'Put 10, boxes',
                              'Container?',
                              '035, correct?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'Put 6, boxes',
                              'Container?',
                              '076, correct?',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Put 14, ',
                              'Container?',
                              '032, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1001056', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '363'],
                                      ['prTaskLUTPtsPut', '363', '11', '1010', '126', '16', '037', '1001056', '0'],
                                      ['prTaskLUTPtsPut', '363', '8', '1010', '127', '12', '043', '1001056', '0'],
                                      ['prTaskLUTPtsPut', '363', '2', '1010', '128', '6', '032', '1001056', '0'],
                                      ['prTaskLUTPtsPut', '363', '5', '1010', '129', '10', '035', '1001056', '0'],
                                      ['prTaskLUTPtsPut', '363', '9', '1010', '130', '6', '076', '1001056', '0'],
                                      ['prTaskLUTPtsPut', '363', '2', '1010', '131', '14', '032', '1001056', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '363'],
                                      ['prTaskLUTPtsStopAssignment', '363'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
