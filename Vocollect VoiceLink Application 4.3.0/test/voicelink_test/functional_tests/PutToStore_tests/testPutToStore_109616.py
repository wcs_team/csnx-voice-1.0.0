##TestLink ID 109616 :: Test Case Test complete assignment when skips are found/no to residual


from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109616(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109616(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,1,1,1,0,1,1,3,1,0,2,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001024,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('359,4,5,24,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,575,10,1001024,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,3,        0,1000,Item 1000 Site 1,eaches,0,0,0,\r\n'
                                 'N,576,8,1001024,,1007                                              ,,1007                                              ,1007,1007,1007,3,        0,1010,Item 1010 Site 1,boxes,1,10,0,\r\n'
                                 'N,577,1,1001024,,1000                                              ,Bay 1000                                          ,1000                                              ,1000,1000,1000000000,4,        0,1005,Item 1005 Site 1,eaches,1,5,0,\r\n'
                                 'N,578,10,1001024,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,11,        0,1001,Item 1001 Site 1,eaches,0,1,0,\r\n'
                                 'N,579,3,1001024,,1002                                              ,Bay 1002                                          ,1002                                              ,,1002,2222222222,2,        0,1008,Item 1008 Site 1,boxes,0,8,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('10,0000000075,075,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001024,1001,Item 1001 Site 1,1,N,0,\r\n'
                                 '1001024,1000,Item 1000 Site 1,0,N,0,\r\n'
                                 '1001024,1005,Item 1005 Site 1,5,N,0,\r\n'
                                 '1001024,1008,Item 1008 Site 1,8,N,0,\r\n'
                                 '1001024,1010,Item 1010 Site 1,10,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '8',              # Function?
                                   'yes',            # Put To Store, correct?
                                   '1!',             # Region?
                                   'yes',            # Region 1, correct?
                                   'ready',          # To start Put to store work, say ready
                                   'ready',          # Go to flow through location
                                   '1001024',        # License?
                                   'ready',          # License?
                                   'yes',            # 1001024, correct?
                                   'ready',          # Assignment has 4 slot locations 5 items, and a total expected residual of 24, say ready
                                   'ready',          # Building 1000 Site 1
                                   'ready',          # Aisle 1009
                                   'ready',          # Bay 1000
                                   '1009',           # 1009
                                   '1000',           # 1009
                                   'ready',          # item 1000 site 1, Put 3, eaches
                                   '3!',             # item 1000 site 1, Put 3, eaches
                                   'new container',  # Container?
                                   'yes',            # new container, correct?
                                   '1!',             # Printer?
                                   'yes',            # printer 1, correct?
                                   'ready',          # Retrieve label from printer 1, then say ready.
                                   'ready',          # Building 1000 Site 1
                                   'ready',          # Aisle 1009
                                   'ready',          # Bay 1000
                                   '1000',           # 1009
                                   '3!',             # item 1000 site 1, Put 3, eaches
                                   '075',            # Container?
                                   'yes',            # 075, correct?
                                   'ready',          # Aisle 1007
                                   '1007',           # 1007
                                   '3!',             # item 1010 site 1, Put 3, boxes
                                   '043',            # Container?
                                   'yes',            # 043, correct?
                                   'skip aisle',     # Aisle 1000
                                   'yes',            # Skip aisle, correct?
                                   'ready',          # Building 1000 Site 1
                                   'ready',          # Aisle 1009
                                   'ready',          # Bay 1000
                                   'skip slot',      # 1009
                                   'yes',            # Skip slot, correct?
                                   'ready',          # Aisle 1002
                                   'ready',          # Bay 1002
                                   'ready',          # 1002
                                   '2!',             # item 1008 site 1, Put 2, boxes
                                   '017',            # Container?
                                   'yes',            # 017, correct?
                                   'ready',          # To put skips, say ready
                                   'ready',          # Aisle 1000
                                   'ready',          # Bay 1000
                                   '1000',           # 1000
                                   '4!',             # item 1005 site 1, Put 4, eaches
                                   '028',            # Container?
                                   'yes',            # 028, correct?
                                   'ready',          # Building 1000 Site 1
                                   'ready',          # Aisle 1009
                                   'ready',          # Bay 1000
                                   '1000',           # 1009
                                   '11!',            # item 1001 site 1, Put 11, eaches
                                   '075',            # Container?
                                   'yes',            # 075, correct?
                                   'no',             # Assignment complete.  Do you have residuals?
                                   '-')              # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '1001024, correct?',
                              'Getting work.',
                              'Assignment has 4 slot locations 5 items, and a total expected residual of 24, say ready',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1000 site 1, Put 3, eaches',
                              'Container?',
                              'new container, correct?',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'item 1000 site 1, Put 3, eaches',
                              'Container?',
                              '075, correct?',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'item 1010 site 1, Put 3, boxes',
                              'Container?',
                              '043, correct?',
                              'Aisle <spell>1000</spell>',
                              'Skip aisle, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'Skip slot, correct?',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'item 1008 site 1, Put 2, boxes',
                              'Container?',
                              '017, correct?',
                              'To put skips, say ready',
                              'Aisle <spell>1000</spell>',
                              'Bay 1000',
                              '1000',
                              'item 1005 site 1, Put 4, eaches',
                              'Container?',
                              '028, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'item 1001 site 1, Put 11, eaches',
                              'Container?',
                              '075, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1001024', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '359'],
                                      ['prTaskLUTPtsContainer', '2', '10', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '359', '1', '10', '0000000075'],
                                      ['prTaskLUTPtsPut', '359', '10', '1000', '575', '3', '075', '1001024', '0'],
                                      ['prTaskLUTPtsPut', '359', '8', '1010', '576', '3', '043', '1001024', '0'],
                                      ['prTaskODRPtsUpdateStatus', '359', '1', '1', 'S'],
                                      ['prTaskODRPtsUpdateStatus', '359', '10', '0', 'S'],
                                      ['prTaskLUTPtsPut', '359', '3', '1008', '579', '2', '017', '1001024', '0'],
                                      ['prTaskODRPtsUpdateStatus', '359', '', '2', 'N'],
                                      ['prTaskLUTPtsPut', '359', '1', '1005', '577', '4', '028', '1001024', '0'],
                                      ['prTaskLUTPtsPut', '359', '10', '1001', '578', '11', '075', '1001024', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '359'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=1001024'],
                                      ['prTaskLUTPtsStopAssignment', '359'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
