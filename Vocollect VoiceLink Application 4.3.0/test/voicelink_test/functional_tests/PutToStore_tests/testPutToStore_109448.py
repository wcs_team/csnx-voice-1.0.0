##TestLink ID 109448 :: Test Case Verify last aisle cannot be skipped
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109448(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109448(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,2,1,5,0,0,5,0,4,1,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002013,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('379,5,1,0,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,217,9,2002013,,1008                                              ,,1008                                              ,1008,1008,1008,13,        0,1011,Item 1011 Site 1,eaches,1,0,0,\r\n'
                                 'N,218,10,2002013,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,10,        0,1011,Item 1011 Site 1,,1,0,0,\r\n'
                                 'N,219,2,2002013,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,8,        0,1011,Item 1011 Site 1,eaches,1,0,0,\r\n'
                                 'N,220,1,2002013,,1000                                              ,Bay 1000                                          ,1000                                              ,1000,1000,1000000000,6,        0,1011,Item 1011 Site 1,,1,0,0,\r\n'
                                 'N,221,11,2002013,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,14,        0,1011,Item 1011 Site 1,cases,0,0,0,\r\n'
                                 'N,222,9,2002013,,1008                                              ,,1008                                              ,1008,1008,1008,4,        0,1011,Item 1011 Site 1,eaches,1,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002013,1011,Item 1011 Site 1,0,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',       # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',        # Password?
                                   '8',           # Function?
                                   'yes',         # Put To Store, correct?
                                   '2!',          # Region?
                                   'yes',         # Region 2, correct?
                                   'ready',       # To start Put to store work, say ready
                                   '02013',       # License?
                                   'no more',     # License?
                                   'ready',       # Assignment has 5 slot locations 1 item, say ready
                                   'ready',       # Aisle 1008
                                   '1008',        # 1008
                                   '13!',         # Put 13, eaches
                                   '00076',       # Container?
                                   'yes',         # 00076, correct?
                                   'ready',       # Building 1000 Site 1
                                   'ready',       # Aisle 1009
                                   'ready',       # Bay 1000
                                   '1009',        # 1009
                                   '1000',        # 1009
                                   '10!',         # Put 10, 
                                   '00075',       # Container?
                                   'yes',         # 00075, correct?
                                   'ready',       # Aisle 1001
                                   'ready',       # Bay 1001
                                   '1001',        # 1001
                                   '8!',          # Put 8, eaches
                                   '00032',       # Container?
                                   'yes',         # 00032, correct?
                                   'ready',       # Aisle 1000
                                   'ready',       # Bay 1000
                                   '1000',        # 1000
                                   '6!',          # Put 6, 
                                   'ready ',      # Building 1000 Site 1
                                   'ready',       # Aisle 1010
                                   'ready',       # Bay 1000
                                   '1010',        # 1010
                                   '1000',        # 1010
                                   '14!',         # Put 14, cases
                                   '00037',       # Container?
                                   'yes',         # 00037, correct?
                                   'skip aisle',  # Aisle 1008
                                   'ready',       # Aisle 1008
                                   '1008',        # 1008
                                   '4!',          # Put 4, eaches
                                   '00076',       # Container?
                                   'yes',         # 00076, correct?
                                   'no',          # Assignment complete.  Do you have residuals?
                                   '-')           # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'License?',
                              'Getting work.',
                              'Assignment has 5 slot locations 1 item, say ready',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'Put 13, eaches',
                              'Container?',
                              '00076, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'Put 10, ',
                              'Container?',
                              '00075, correct?',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Put 8, eaches',
                              'Container?',
                              '00032, correct?',
                              'Aisle <spell>1000</spell>',
                              'Bay 1000',
                              '1000',
                              'Put 6, ',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'wrong 1010, try again',
                              'Put 14, cases',
                              'Container?',
                              '00037, correct?',
                              'Aisle <spell>1008</spell>',
                              'Last aisle. Skip aisle not allowed.',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'Put 4, eaches',
                              'Container?',
                              '00076, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '02013', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '379'],
                                      ['prTaskLUTPtsPut', '379', '9', '1011', '217', '13', '', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '9', '1011', '217', '13', '00076', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '10', '1011', '218', '10', '', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '10', '1011', '218', '10', '00075', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '2', '1011', '219', '8', '', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '2', '1011', '219', '8', '00032', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '1', '1011', '220', '6', '', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '11', '1011', '221', '14', '', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '11', '1011', '221', '14', '00037', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '9', '1011', '222', '4', '', '2002013', '0'],
                                      ['prTaskLUTPtsPut', '379', '9', '1011', '222', '4', '00076', '2002013', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '379'],
                                      ['prTaskLUTPtsStopAssignment', '379'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
