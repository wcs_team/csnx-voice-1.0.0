##TestLink ID 109622 :: Test Case Test Residual available/exp resid loc ,unexp resid loc different, one item

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109622(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109622(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,1,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003024,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003034,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003044,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003054,4,Multiple records exist for license entered by operator.\r\n'
                                 '3052054,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003034,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('361,2,1,77,residual return location,00,unexpected residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,329,6,3003034,Building 1005 Site 1                              ,1005                                              ,,1005                                              ,1005,1005,5555555555,14,        0,1000,Item 1000 Site 1,cases,1,77,0,\r\n'
                                 'N,330,7,3003034,,1006                                              ,,1006                                              ,1006,1006,1006,8,        0,1000,Item 1000 Site 1,,0,77,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003034,1000,Item 1000 Site 1,77,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',          # Password?
                                   '8',             # Function?
                                   'yes',           # Put To Store, correct?
                                   '3!',            # Region?
                                   'yes',           # Region 3, correct?
                                   'ready',         # To start Put to store work, say ready
                                   'ready',         # Go to flow through location
                                   '4',             # License?
                                   'yes',           # 4, correct?
                                   'yes',           # Found 5 licenses. Do you want to review the list?
                                   'no',            # 3003024, select?
                                   'yes',           # 3003034, select?
                                   'no more',       # License?
                                   'ready',         # Assignment has 2 slot locations 1 item, and a total expected residual of 77, say ready
                                   'ready',         # Building 1005 Site 1
                                   'ready',         # Aisle 1005
                                   '1005',          # 1005
                                   '14!',           # Put 14, cases
                                   '033',           # Container?
                                   'no',            # 0, correct?
                                   '3',             # Container?
                                   'yes',           # 3, correct?
                                   'ready',         # Aisle 1006
                                   '1006',          # 1006
                                   '8!',            # Put 8, 
                                   '4',             # Container?
                                   'yes',           # 4, correct?
                                   'yes',           # Assignment complete.  Do you have residuals?
                                   'talkman help',  # Confirm residuals for license 3003034
                                   '77!',           # Confirm residuals for license 3003034
                                   'yes',           # 77, correct?
                                   'ready',         # return residual to residual return location, say ready
                                   '00',            # Check Digit?
                                   '-')             # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '4, correct?',
                              'Found 5 licenses. Do you want to review the list?',
                              '3003024, select?',
                              '3003034, select?',
                              'License?',
                              'Getting work.',
                              'Assignment has 2 slot locations 1 item, and a total expected residual of 77, say ready',
                              'Building 1005 Site 1',
                              'Aisle <spell>1005</spell>',
                              '1005',
                              'Put 14, cases',
                              'Container?',
                              '0, correct?',
                              'Container?',
                              '3, correct?',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'Put 8, ',
                              'Container?',
                              '4, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'Confirm residuals for license 3003034',
                              'Speak the residual amount for the license.',
                              'Confirm residuals for license 3003034',
                              '77, correct?',
                              'return residual to residual return location, say ready',
                              'Check Digit?',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '4', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '3003034', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '361'],
                                      ['prTaskLUTPtsPut', '361', '6', '1000', '329', '14', '', '3003034', '0'],
                                      ['prTaskLUTPtsPut', '361', '6', '1000', '329', '14', '3', '3003034', '0'],
                                      ['prTaskLUTPtsPut', '361', '7', '1000', '330', '8', '', '3003034', '0'],
                                      ['prTaskLUTPtsPut', '361', '7', '1000', '330', '8', '4', '3003034', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '361'],
                                      ['prTaskLUTPtsStopAssignment', '361'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
