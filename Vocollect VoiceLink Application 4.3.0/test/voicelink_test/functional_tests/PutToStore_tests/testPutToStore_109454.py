##TestLink ID 109454 :: Test Case Test flow after last license is reviewed when multiple license returned

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109454(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109454(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,0,0,0,1,0,1,2,0,1,1,0,1,1,1,0,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3003035,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003045,4,Multiple records exist for license entered by operator.\r\n'
                                 '3003055,4,Multiple records exist for license entered by operator.\r\n'
                                 '3062055,4,Multiple records exist for license entered by operator.\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '8',      # Function?
                                   'yes',    # Put To Store, correct?
                                   '3!',     # Region?
                                   'yes',    # Region 3, correct?
                                   'ready',  # To start Put to store work, say ready
                                   'ready',  # Go to flow through location
                                   '5',      # License?
                                   'yes',    # 5, correct?
                                   'yes',    # Found 4 licenses. Do you want to review the list?
                                   'no',     # 3003035, select?
                                   'no',     # 3003045, select?
                                   'no',     # 3003055, select?
                                   'no',     # 3062055, select?
                                   'no',     # 3003035, select?
                                   '-')      # 3003045, select?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '5, correct?',
                              'Found 4 licenses. Do you want to review the list?',
                              '3003035, select?',
                              '3003045, select?',
                              '3003055, select?',
                              '3062055, select?',
                              'End of matching licenses. Starting over',
                              '3003035, select?',
                              '3003045, select?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '3'],
                                      ['prTaskLUTPtsGetFTLocation', '3'],
                                      ['prTaskLUTPtsVerifyLicense', '5', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
