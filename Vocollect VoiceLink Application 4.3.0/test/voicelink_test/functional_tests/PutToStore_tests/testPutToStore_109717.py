##TestLink ID 109717 :: Test Case Test close container command

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109717(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109717(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,1,1,1,4,0,0,3,0,4,4,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('11,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,3,Multiple open containers. Please specify a container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('11,0000000133,133,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('8,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('8,0000000088,088,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('8,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,4,No open container at location.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002007,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('543,5,1,7,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,186,8,2002007,,1007                                              ,,1007                                              ,1007,1007,1007,1,        0,1005,Item 1005 Site 1,eaches,0,7,0,\r\n'
                                 'N,187,4,2002007,Building 1003 Site 1                              ,1003                                              ,,1003                                              ,1003,1003,3123123123,14,        0,1005,Item 1005 Site 1,boxes,0,7,0,\r\n'
                                 'N,188,3,2002007,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,10,        0,1005,Item 1005 Site 1,eaches,0,7,0,\r\n'
                                 'N,189,7,2002007,,1006                                              ,,1006                                              ,1006,1006,1006,13,        0,1005,Item 1005 Site 1,,1,7,0,\r\n'
                                 'N,190,3,2002007,,1002                                              ,Bay 1002                                          ,1002                                              ,1002,1002,2222222222,13,        0,1005,Item 1005 Site 1,eaches,1,7,0,\r\n'
                                 'N,191,11,2002007,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,8,        0,1005,Item 1005 Site 1,boxes,0,7,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,4,No open container at location.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,No open container. Open a new container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('8,0000000137,137,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1040,Printer 2 undefined\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('4,0000000040,040,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,No open container. Open a new container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('4,0000000138,138,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('4,0000000139,139,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,3,Multiple open containers. Please specify a container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('4,0000000138,138,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,036,036,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,4,No open container at location.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,No open container. Open a new container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,0000000140,140,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('11,0000000141,141,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,3,Multiple open containers. Please specify a container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,5,Container 133 was not found or is already closed.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Multiple open containers. Specify container.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002007,1005,Item 1005 Site 1,7,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '8',                # Function?
                                   'yes',              # Put To Store, correct?
                                   '2!',               # Region?
                                   'yes',              # Region 2, correct?
                                   'ready',            # To start Put to store work, say ready
                                   'close container',  # License?
                                   'yes',              # close container, correct?
                                   '1010',             # Location?
                                   '1000',             # Check digit?
                                   '133',              # Container to close?
                                   'yes',              # 133, correct?
                                   'close container',  # License?
                                   'yes',              # close container, correct?
                                   '1007',             # Location?
                                   '1007',             # Check digit?
                                   'close container',  # License?
                                   'yes',              # close container, correct?
                                   '1007',             # Location?
                                   '1007',             # Check digit?
                                   'ready',            # No open container at location., To continue say ready
                                   '2007',             # License?
                                   'ready',            # Assignment has 5 slot locations 1 item, and a total expected residual of 7, say ready
                                   'ready',            # Aisle <spell>1007</spell>
                                   '1007',             # 1007
                                   'close container',  # Put 1, eaches
                                   'yes',              # close container, correct?
                                   'ready',            # No open container at location., To continue say ready
                                   '1!',               # Put 1, eaches
                                   '2!',               # Printer?
                                   'yes',              # printer <spell>2</spell>, correct?
                                   '1',                # Printer 2 undefined, To continue say ready
                                   'ready',            # Printer 2 undefined, To continue say ready
                                   'no',               # printer <spell>2</spell>, correct?
                                   '1!',               # Printer?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',            # Aisle <spell>1007</spell>
                                   '1007',             # 1007
                                   '1!',               # Put 1, eaches
                                   'ready',            # Building 1003 Site 1
                                   'ready',            # Aisle <spell>1003</spell>
                                   '1003',             # 1003
                                   'close container',  # Put 14, boxes
                                   'yes',              # close container, correct?
                                   '14!',              # Put 14, boxes
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',            # Building 1003 Site 1
                                   'ready',            # Aisle <spell>1003</spell>
                                   '1003',             # 1003
                                   'new container',    # Put 14, boxes
                                   'yes',              # new container, correct?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',            # Building 1003 Site 1
                                   'ready',            # Aisle <spell>1003</spell>
                                   '1003',             # 1003
                                   'close container',  # Put 14, boxes
                                   'yes',              # close container, correct?
                                   '138',              # Container to close?
                                   'yes',              # 138, correct?
                                   '14!',              # Put 14, boxes
                                   'ready',            # Aisle <spell>1002</spell>
                                   'ready',            # Bay 1002
                                   '1002',             # 1002
                                   'close container',  # Put 10, eaches
                                   'yes',              # close container, correct?
                                   'close container',  # Put 10, eaches
                                   'yes',              # close container, correct?
                                   'ready',            # No open container at location., To continue say ready
                                   '10!',              # Put 10, eaches
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',            # Aisle <spell>1002</spell>
                                   'ready',            # Bay 1002
                                   '1002',             # 1002
                                   '10!',              # Put 10, eaches
                                   'ready',            # Aisle <spell>1006</spell>
                                   '1006',             # 1006
                                   '13!',              # Put 13, 
                                   'ready',            # Aisle <spell>1002</spell>
                                   'ready',            # Bay 1002
                                   '1002',             # 1002
                                   '13!',              # Put 13, eaches
                                   'ready',            # Building 1000 Site 1
                                   'ready',            # Aisle <spell>1010</spell>
                                   'ready',            # Bay 1000
                                   '1000',             # 1010
                                   'close container',  # Put 8, boxes
                                   'no',               # close container, correct?
                                   'new container',    # Put 8, boxes
                                   'yes',              # new container, correct?
                                   'yes',              # printer <spell>1</spell>, correct?
                                   'ready',            # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',            # Building 1000 Site 1
                                   'ready',            # Aisle <spell>1010</spell>
                                   'ready',            # Bay 1000
                                   '1000',             # 1010
                                   'close container',  # Put 8, boxes
                                   'yes',              # close container, correct?
                                   '141',              # Container to close?
                                   'no',               # 141, correct?
                                   '133',              # Container to close?
                                   'yes',              # 133, correct?
                                   'ready',            # Container 133 was not found or is already closed., To continue say ready
                                   '8!',               # Put 8, boxes
                                   '141',              # Container?
                                   'yes',              # 141, correct?
                                   'no',               # Assignment complete.  Do you have residuals?
                                   'sign off',         # For next assignment, say ready
                                   'yes',              # Sign off, correct?
                                   '-')                # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'close container, correct?',
                              'Location?',
                              'Check digit?',
                              'Container to close?',
                              '133, correct?',
                              'License?',
                              'close container, correct?',
                              'Location?',
                              'Check digit?',
                              'License?',
                              'close container, correct?',
                              'Location?',
                              'Check digit?',
                              'No open container at location., To continue say ready',
                              'License?',
                              'Getting work.',
                              'Assignment has 5 slot locations 1 item, and a total expected residual of 7, say ready',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'Put 1, eaches',
                              'close container, correct?',
                              'No open container at location., To continue say ready',
                              'Put 1, eaches',
                              'Opening new container.',
                              'Printer?',
                              'printer <spell>2</spell>, correct?',
                              'Printer 2 undefined, To continue say ready',
                              'printer <spell>2</spell>, correct?',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'Put 1, eaches',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'Put 14, boxes',
                              'close container, correct?',
                              'Put 14, boxes',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'Put 14, boxes',
                              'new container, correct?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1003 Site 1',
                              'Aisle <spell>1003</spell>',
                              '1003',
                              'Put 14, boxes',
                              'close container, correct?',
                              'Container to close?',
                              '138, correct?',
                              'Put 14, boxes',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 10, eaches',
                              'close container, correct?',
                              'Put 10, eaches',
                              'close container, correct?',
                              'No open container at location., To continue say ready',
                              'Put 10, eaches',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 10, eaches',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'Put 13, ',
                              'Aisle <spell>1002</spell>',
                              'Bay 1002',
                              '1002',
                              'Put 13, eaches',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'Put 8, boxes',
                              'close container, correct?',
                              'Put 8, boxes',
                              'new container, correct?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'Put 8, boxes',
                              'close container, correct?',
                              'Container to close?',
                              '141, correct?',
                              'Container to close?',
                              '133, correct?',
                              'Container 133 was not found or is already closed., To continue say ready',
                              'Put 8, boxes',
                              'Container?',
                              '141, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready',
                              'Sign off, correct?',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyCustomerLocation', '0', '1010', '1000'],
                                      ['prTaskLUTPtsContainer', '1', '11', ''],
                                      ['prTaskLUTPtsContainer', '1', '11', '133'],
                                      ['prTaskLUTPtsVerifyCustomerLocation', '0', '1007', '1007'],
                                      ['prTaskLUTPtsContainer', '1', '8', ''],
                                      ['prTaskLUTPtsVerifyCustomerLocation', '0', '1007', '1007'],
                                      ['prTaskLUTPtsContainer', '1', '8', ''],
                                      ['prTaskLUTPtsVerifyLicense', '2007', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '543'],
                                      ['prTaskLUTPtsContainer', '1', '8', ''],
                                      ['prTaskLUTPtsPut', '543', '8', '1005', '186', '1', '', '2002007', '0'],
                                      ['prTaskLUTPtsContainer', '2', '8', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '543', '2', '8', '0000000137'],
                                      ['prTaskLUTPtsPrintContainerLabel', '543', '1', '8', '0000000137'],
                                      ['prTaskLUTPtsPut', '543', '8', '1005', '186', '1', '', '2002007', '0'],
                                      ['prTaskLUTPtsContainer', '1', '4', ''],
                                      ['prTaskLUTPtsPut', '543', '4', '1005', '187', '14', '', '2002007', '0'],
                                      ['prTaskLUTPtsContainer', '2', '4', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '543', '1', '4', '0000000138'],
                                      ['prTaskLUTPtsContainer', '2', '4', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '543', '1', '4', '0000000139'],
                                      ['prTaskLUTPtsContainer', '1', '4', ''],
                                      ['prTaskLUTPtsContainer', '1', '4', '138'],
                                      ['prTaskLUTPtsPut', '543', '4', '1005', '187', '14', '', '2002007', '0'],
                                      ['prTaskLUTPtsContainer', '1', '3', ''],
                                      ['prTaskLUTPtsContainer', '1', '3', ''],
                                      ['prTaskLUTPtsPut', '543', '3', '1005', '188', '10', '', '2002007', '0'],
                                      ['prTaskLUTPtsContainer', '2', '3', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '543', '1', '3', '0000000140'],
                                      ['prTaskLUTPtsPut', '543', '3', '1005', '188', '10', '', '2002007', '0'],
                                      ['prTaskLUTPtsPut', '543', '7', '1005', '189', '13', '', '2002007', '0'],
                                      ['prTaskLUTPtsPut', '543', '3', '1005', '190', '13', '', '2002007', '0'],
                                      ['prTaskLUTPtsContainer', '2', '11', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '543', '1', '11', '0000000141'],
                                      ['prTaskLUTPtsContainer', '1', '11', ''],
                                      ['prTaskLUTPtsContainer', '1', '11', '133'],
                                      ['prTaskLUTPtsPut', '543', '11', '1005', '191', '8', '', '2002007', '0'],
                                      ['prTaskLUTPtsPut', '543', '11', '1005', '191', '8', '141', '2002007', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '543'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=2002007'],
                                      ['prTaskLUTPtsStopAssignment', '543'],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
