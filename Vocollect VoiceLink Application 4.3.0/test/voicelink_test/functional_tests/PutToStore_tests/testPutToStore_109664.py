##TestLink ID 109664 :: Test Case Verify partial command with overpack and short product

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109664(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109664(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,0,1,1,0,1,1,4,0,0,3,0,4,4,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002059,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('495,6,6,31,residual return location,00,residual return location,11,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,758,5,2002059,Building 1004 Site 1                              ,1004                                              ,,1004                                              ,1004,1004,4444444444,3,        0,1000,Item 1000 Site 1,boxes,1,0,0,\r\n'
                                 'N,759,12,2002059,Building 1000 Site 1                              ,1011                                              ,Bay 1000                                          ,1011                                              ,1000,1011,1011,4,        0,1004,Item 1004 Site 1,,1,4,0,\r\n'
                                 'N,760,11,2002059,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,1,        0,1009,Item 1009 Site 1,,1,9,0,\r\n'
                                 'N,761,8,2002059,,1007                                              ,,1007                                              ,1007,1007,1007,14,        0,1006,Item 1006 Site 1,boxes,1,6,0,\r\n'
                                 'N,762,10,2002059,Building 1000 Site 1                              ,1009                                              ,Bay 1000                                          ,1009                                              ,1000,1009,1009,10,        0,1011,Item 1011 Site 1,,1,11,0,\r\n'
                                 'N,763,9,2002059,,1008                                              ,,1008                                              ,1008,1008,1008,3,        0,1001,Item 1001 Site 1,eaches,1,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('5,0000000128,128,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('12,0000000129,129,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('10,0000000130,130,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2002059,1001,Item 1001 Site 1,1,N,0,\r\n'
                                 '2002059,1000,Item 1000 Site 1,0,N,0,\r\n'
                                 '2002059,1004,Item 1004 Site 1,2,N,0,\r\n'
                                 '2002059,1011,Item 1011 Site 1,0,N,0,\r\n'
                                 '2002059,1009,Item 1009 Site 1,1,N,0,\r\n'
                                 '2002059,1006,Item 1006 Site 1,6,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '8',      # Function?
                                   'yes',    # Put To Store, correct?
                                   '2!',     # Region?
                                   'yes',    # Region 2, correct?
                                   'ready',  # To start Put to store work, say ready
                                   '2059',   # License?
                                   'ready',  # Assignment has 6 slot locations 6 items, and a total expected residual of 31, say ready
                                   'ready',  # Building 1004 Site 1
                                   'ready',  # Aisle <spell>1004</spell>
                                   '1004',   # 1004
                                   '1!',     # item 1000 site 1, Put 3, boxes
                                   'yes',    # Is this a partial?
                                   '1!',     # Printer?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Building 1004 Site 1
                                   'ready',  # Aisle <spell>1004</spell>
                                   '1004',   # 1004
                                   '5!',     # item 1000 site 1, Put 2, boxes
                                   'yes',    # Is this an overpack?
                                   '2!',     # item 1000 site 1, Put 2, boxes
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1011</spell>
                                   'ready',  # Bay 1000
                                   '1011',   # 1011
                                   '1000',   # wrong 1011, try again
                                   '1!',     # item 1004 site 1, Put 4, 
                                   'yes',    # Is this a partial?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1011</spell>
                                   'ready',  # Bay 1000
                                   '1000',   # 1011
                                   '5!',     # item 1004 site 1, Put 3, 
                                   'yes',    # Is this an overpack?
                                   'ready',  # Aisle <spell>1010</spell>
                                   'ready',  # Bay 1000
                                   'ready',  # 1010
                                   '1010',   # 1010
                                   '1000',   # wrong 1010, try again
                                   '15!',    # item 1009 site 1, Put 1, 
                                   'yes',    # Is this an overpack?
                                   '9!',     # item 1009 site 1, Put 1, 
                                   'yes',    # Is this an overpack?
                                   'ready',  # Aisle <spell>1007</spell>
                                   '1007',   # 1007
                                   '14!',    # item 1006 site 1, Put 14, boxes
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1009</spell>
                                   'ready',  # Bay 1000
                                   '1009',   # 1009
                                   '1000',   # wrong 1009, try again
                                   '4!',     # item 1011 site 1, Put 10, 
                                   'yes',    # Is this a partial?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   'ready',  # Retrieve label from printer <spell>1</spell>, then say ready.
                                   'ready',  # Building 1000 Site 1
                                   'ready',  # Aisle <spell>1009</spell>
                                   'ready',  # Bay 1000
                                   '1000',   # 1009
                                   '17!',    # item 1011 site 1, Put 6, 
                                   'yes',    # Is this an overpack?
                                   'ready',  # Aisle <spell>1008</spell>
                                   '1008',   # 1008
                                   '2!',     # item 1001 site 1, Put 3, eaches
                                   'no',     # Is this a partial?
                                   'yes',    # Is this a short?
                                   'no',     # Assignment complete.  Do you have residuals?
                                   'ready',  # For next assignment, say ready
                                   '-')      # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'To start Put to store work, say ready',
                              'License?',
                              'Getting work.',
                              'Assignment has 6 slot locations 6 items, and a total expected residual of 31, say ready',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'item 1000 site 1, Put 3, boxes',
                              'Is this a partial?',
                              'Opening new container.',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1004 Site 1',
                              'Aisle <spell>1004</spell>',
                              '1004',
                              'item 1000 site 1, Put 2, boxes',
                              'Is this an overpack?',
                              'Cannot overpack. There are no residuals for this item.',
                              'item 1000 site 1, Put 2, boxes',
                              'Building 1000 Site 1',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'wrong 1011, try again',
                              'item 1004 site 1, Put 4, ',
                              'Is this a partial?',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1011</spell>',
                              'Bay 1000',
                              '1011',
                              'item 1004 site 1, Put 3, ',
                              'Is this an overpack?',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'wrong 1010, try again',
                              'item 1009 site 1, Put 1, ',
                              'Is this an overpack?',
                              'Can only overpack 9 items.',
                              'item 1009 site 1, Put 1, ',
                              'Is this an overpack?',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'item 1006 site 1, Put 14, boxes',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'wrong 1009, try again',
                              'item 1011 site 1, Put 10, ',
                              'Is this a partial?',
                              'Opening new container.',
                              'printer <spell>1</spell>, correct?',
                              'Retrieve label from printer <spell>1</spell>, then say ready.',
                              'Building 1000 Site 1',
                              'Aisle <spell>1009</spell>',
                              'Bay 1000',
                              '1009',
                              'item 1011 site 1, Put 6, ',
                              'Is this an overpack?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'item 1001 site 1, Put 3, eaches',
                              'Is this a partial?',
                              'Is this a short?',
                              'Assignment complete.  Do you have residuals?',
                              'For next assignment, say ready',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '2'],
                                      ['prTaskLUTPtsGetFTLocation', '2'],
                                      ['prTaskLUTPtsVerifyLicense', '2059', '1'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '495'],
                                      ['prTaskLUTPtsPut', '495', '5', '1000', '758', '1', '', '2002059', '1'],
                                      ['prTaskLUTPtsContainer', '2', '5', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '495', '1', '5', '0000000128'],
                                      ['prTaskLUTPtsPut', '495', '5', '1000', '758', '2', '', '2002059', '0'],
                                      ['prTaskLUTPtsPut', '495', '12', '1004', '759', '1', '', '2002059', '1'],
                                      ['prTaskLUTPtsContainer', '2', '12', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '495', '1', '12', '0000000129'],
                                      ['prTaskLUTPtsPut', '495', '12', '1004', '759', '5', '', '2002059', '0'],
                                      ['prTaskLUTPtsPut', '495', '11', '1009', '760', '9', '', '2002059', '0'],
                                      ['prTaskLUTPtsPut', '495', '8', '1006', '761', '14', '', '2002059', '0'],
                                      ['prTaskLUTPtsPut', '495', '10', '1011', '762', '4', '', '2002059', '1'],
                                      ['prTaskLUTPtsContainer', '2', '10', ''],
                                      ['prTaskLUTPtsPrintContainerLabel', '495', '1', '10', '0000000130'],
                                      ['prTaskLUTPtsPut', '495', '10', '1011', '762', '17', '', '2002059', '0'],
                                      ['prTaskLUTPtsPut', '495', '9', '1001', '763', '2', '', '2002059', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '495'],
                                      ['prTaskODRCorePostNotification', 'voicelink.notification.task.puttostore.hostexpectsresiduals', '0', 'task.licenseNumber=2002059'],
                                      ['prTaskLUTPtsStopAssignment', '495'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
