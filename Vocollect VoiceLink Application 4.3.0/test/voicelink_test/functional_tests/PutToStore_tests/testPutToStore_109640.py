##TestLink ID 109640 :: Test Case Test residual exists and resid loc and unexp recid loc are same
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutToStore_109640(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutToStore_109640(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '11,Region 11,0,\r\n'
                                 '12,Region 12,0,\r\n'
                                 '13,Region 13,0,\r\n'
                                 '14,Region 14,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,1,1,1,1,1,1,1,1,0,1,1,3,1,0,2,0,X                                                 ,X                                                 ,X                                                 ,X                                                 ,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('flow through location,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001057,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('364,5,1,5,residual return location,00,residual return location,00,-1,-1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('N,132,7,1001057,,1006                                              ,,1006                                              ,1006,1006,1006,12,        0,1011,Item 1011 Site 1,eaches,1,5,0,\r\n'
                                 'N,133,9,1001057,,1008                                              ,,1008                                              ,1008,1008,1008,14,        0,1011,Item 1011 Site 1,,1,5,0,\r\n'
                                 'N,134,8,1001057,,1007                                              ,,1007                                              ,1007,1007,1007,1,        0,1011,Item 1011 Site 1,eaches,0,5,0,\r\n'
                                 'N,135,11,1001057,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,4,        0,1011,Item 1011 Site 1,cases,0,5,0,\r\n'
                                 'N,136,11,1001057,Building 1000 Site 1                              ,1010                                              ,Bay 1000                                          ,1010                                              ,1000,1010,1010,1,        0,1011,Item 1011 Site 1,,1,5,0,\r\n'
                                 'N,137,2,1001057,,1001                                              ,Bay 1001                                          ,1001                                              ,1001,1001,1111111111,6,        0,1011,Item 1011 Site 1,cases,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1001057,1011,Item 1011 Site 1,5,N,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',          # Password?
                                   '8',             # Function?
                                   'yes',           # Put To Store, correct?
                                   '1!',            # Region?
                                   'yes',           # Region 1, correct?
                                   'ready',         # To start Put to store work, say ready
                                   'ready',         # Go to flow through location
                                   '1001057',       # License?
                                   'ready',         # License?
                                   'yes',           # 1001057, correct?
                                   'ready',         # Assignment has 5 slot locations 1 item, and a total expected residual of 5, say ready
                                   'ready',         # Aisle 1006
                                   '1006',          # 1006
                                   '12!',           # Put 12, eaches
                                   '074',           # Container?
                                   'yes',           # 074, correct?
                                   'ready',         # Aisle 1008
                                   '1008',          # 1008
                                   '14!',           # Put 14, 
                                   '076',           # Container?
                                   'yes',           # 076, correct?
                                   'ready',         # Aisle 1007
                                   '1007',          # 1007
                                   '1!',            # Put 1, eaches
                                   '043',           # Container?
                                   'yes',           # 043, correct?
                                   'ready',         # Building 1000 Site 1
                                   'ready',         # Aisle 1010
                                   'ready',         # Bay 1000
                                   '1010',          # 1010
                                   '1000',          # 1010
                                   '4!',            # Put 4, cases
                                   '037',           # Container?
                                   'yes',           # 037, correct?
                                   '1000',          # 1010
                                   '1!',            # Put 1, 
                                   '037',           # Container?
                                   'yes',           # 037, correct?
                                   'ready',         # Aisle 1001
                                   'ready',         # Bay 1001
                                   '1001',          # 1001
                                   '6!',            # Put 6, cases
                                   '032',           # Container?
                                   'yes',           # 032, correct?
                                   'yes',           # Assignment complete.  Do you have residuals?
                                   'ready',         # return residual to residual return location, say ready
                                   'talkman help',  # Check Digit?
                                   'location',      # Check Digit?
                                   'ready',         # residual return location, say ready
                                   'take a break',  # Check Digit?
                                   'no',            # Take a break, correct?
                                   'signoff',       # Check Digit?
                                   'sign off',      # Check Digit?
                                   'no',            # Sign off, correct?
                                   '11',            # Check Digit?
                                   '00',            # Check Digit?
                                   '-')             # For next assignment, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put To Store, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'To start Put to store work, say ready',
                              'Go to flow through location',
                              'License?',
                              '1001057, correct?',
                              'Getting work.',
                              'Assignment has 5 slot locations 1 item, and a total expected residual of 5, say ready',
                              'Aisle <spell>1006</spell>',
                              '1006',
                              'Put 12, eaches',
                              'Container?',
                              '074, correct?',
                              'Aisle <spell>1008</spell>',
                              '1008',
                              'Put 14, ',
                              'Container?',
                              '076, correct?',
                              'Aisle <spell>1007</spell>',
                              '1007',
                              'Put 1, eaches',
                              'Container?',
                              '043, correct?',
                              'Building 1000 Site 1',
                              'Aisle <spell>1010</spell>',
                              'Bay 1000',
                              '1010',
                              'wrong 1010, try again',
                              'Put 4, cases',
                              'Container?',
                              '037, correct?',
                              '1010',
                              'Put 1, ',
                              'Container?',
                              '037, correct?',
                              'Aisle <spell>1001</spell>',
                              'Bay 1001',
                              '1001',
                              'Put 6, cases',
                              'Container?',
                              '032, correct?',
                              'Assignment complete.  Do you have residuals?',
                              'return residual to residual return location, say ready',
                              'Check Digit?',
                              'Speak the check digits or say location',
                              'Check Digit?',
                              'residual return location, say ready',
                              'Check Digit?',
                              'take a break, correct?',
                              'Check Digit?',
                              'Sign off, correct?',
                              'Check Digit?',
                              'wrong 11, try again',
                              'For next assignment, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTPtsValidRegions'],
                                      ['prTaskLUTPtsGetRegionConfiguration', '1'],
                                      ['prTaskLUTPtsGetFTLocation', '1'],
                                      ['prTaskLUTPtsVerifyLicense', '1001057', '0'],
                                      ['prTaskLUTPtsGetAssignment'],
                                      ['prTaskLUTPtsGetPuts', '364'],
                                      ['prTaskLUTPtsPut', '364', '7', '1011', '132', '12', '074', '1001057', '0'],
                                      ['prTaskLUTPtsPut', '364', '9', '1011', '133', '14', '076', '1001057', '0'],
                                      ['prTaskLUTPtsPut', '364', '8', '1011', '134', '1', '043', '1001057', '0'],
                                      ['prTaskLUTPtsPut', '364', '11', '1011', '135', '4', '037', '1001057', '0'],
                                      ['prTaskLUTPtsPut', '364', '11', '1011', '136', '1', '037', '1001057', '0'],
                                      ['prTaskLUTPtsPut', '364', '2', '1011', '137', '6', '032', '1001057', '0'],
                                      ['prTaskLUTPtsGetExpectedResidual', '364'],
                                      ['prTaskLUTPtsStopAssignment', '364'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
