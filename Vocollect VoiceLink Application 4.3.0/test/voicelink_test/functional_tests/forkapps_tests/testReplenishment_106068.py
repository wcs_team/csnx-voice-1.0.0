##TestLink ID 106068 :: Test Case Override qty/partial, quantity specified, override pickup quantity with large number
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testReplenishment(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testReplenishment(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,replenishment region 1,0,\n'
                                       '2,replenishment region 2,0,\n'
                                       '3,replenishment region 3,0,\n'
                                       '4,replenishment region 4,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('3,replenishment region 3,1,0,1,1,0,0,2,5,2,exception location,0,0,\n'
                                       '4,replenishment region 4,1,1,1,1,1,1,5,0,0,Exception Location,1,0,\n'
                                       '\n')
        self.set_server_response('301535,0000001535,0,4,771,Item 771 Site 1,13,Building 480 Site 1,480,Bay 480,480,00,1021535,Building 771 Site 1,771,Bay 771,771,771,771,772,5,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('ALT PRE 1,ALT A 1,ALT POST 1,ALT S 1,99,ALTSCAN,Alt Location,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',     # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',      # Password?
                                   '2!',        # Function?
                                   'yes',       # replenishment, correct?
                                   '4',         # Region?
                                   'yes',       # replenishment region 4, correct?
                                   'no',        # Another Region?
                                   'ready',     # To start replenishment, say ready
                                   'ready',     # Building 480 Site 1
                                   'ready',     # Aisle 480
                                   'ready',     # Bay 480
                                   '00!',       # 4 8 0, pick up 13
                                   '15!',       # quantity
                                   'yes',       # You said 15, expected 13, correct?
                                   'ready',     # Building 771 Site 1
                                   'ready',     # Aisle 771
                                   'ready',     # Bay 771
                                   '771',       # 7 7 1
                                   '13!',       # quantity
                                   'yes',       # You said 13, expected 15, is this a partial?
                                   'ready',     # ALT PRE 1
                                   'ready',     # Aisle ALT A 1
                                   'ready',     # ALT POST 1
                                   '99!',       # A L T   S   1
                                   'quantity',  # quantity
                                   '2!',        # quantity
                                   '-')         # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'replenishment, correct?',
                              'Region?',
                              'replenishment region 4, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'Building 480 Site 1',
                              'Aisle 480',
                              'Bay 480',
                              '4 8 0, pick up 13',
                              'quantity',
                              'You said 15, expected 13, correct?',
                              'Building 771 Site 1',
                              'Aisle 771',
                              'Bay 771',
                              '7 7 1',
                              'quantity',
                              'You said 13, expected 15, is this a partial?',
                              'ALT PRE 1',
                              'Aisle ALT A 1',
                              'ALT POST 1',
                              'A L T   S   1',
                              'quantity',
                              '2',
                              'quantity',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '4', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTForkGetReplenishment'],
                                      ['prTaskLUTForkReplenishmentLicense', '301535', '13', '772', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '2', '301535', '772', '2'],
                                      ['prTaskLUTForkReplenishmentLicense', '301535', '2', 'Alt Location', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
