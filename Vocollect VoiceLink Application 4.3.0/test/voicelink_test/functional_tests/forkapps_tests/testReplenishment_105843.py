##TestLink ID - 105843 :: Override qty, quantity specified, override pickup quantity with smaller qty

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testReplenishment(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testReplenishment(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,replenishment region 1,0,\n'
                                       '2,replenishment region 2,0,\n'
                                       '3,replenishment region 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,replenishment region 1,1,1,1,0,1,0,5,5,2,exception location,1,0,\n'
                                       '\n')
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,Slot 54321,00,58425148,Building 2,43,Bay 76,Slot 22345,00,51528610,12,25,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '2!',     # Function?
                                   'yes',    # replenishment, correct?
                                   '1',      # Region?
                                   'yes',    # replenishment region 1, correct?
                                   'no',     # Another Region?
                                   'ready',  # To start replenishment, say ready
                                   'ready',  # Building 1
                                   'ready',  # Aisle 24
                                   'ready',  # Bay 23
                                   '00!',    # S l o t   5 4 3 2 1, pick up 25
                                   '20!',    # quantity
                                   'yes',    # You said 20, expected 25, correct?
                                   'ready',  # Building 2
                                   'ready',  # Aisle 43
                                   'ready',  # Bay 76
                                   '00!',    # S l o t   2 2 3 4 5
                                   '-')      # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'replenishment, correct?',
                              'Region?',
                              'replenishment region 1, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'Building 1',
                              'Aisle 24',
                              'Bay 23',
                              'S l o t   5 4 3 2 1, pick up 25',
                              'quantity',
                              'You said 20, expected 25, correct?',
                              'Building 2',
                              'Aisle 43',
                              'Bay 76',
                              'S l o t   2 2 3 4 5',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '1', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTForkGetReplenishment'],
                                      ['prTaskLUTForkReplenishmentLicense', '115647', '20', '12', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
