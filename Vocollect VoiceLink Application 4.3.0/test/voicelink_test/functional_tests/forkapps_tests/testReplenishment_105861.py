##TestLink ID - 105861 :: Override loc/cancel, quantity specified, cancel at location prompt
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testReplenishment(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testReplenishment(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,replenishment region 1,0,\n'
                                       '2,replenishment region 2,0,\n'
                                       '3,replenishment region 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('1,replenishment region 1,1,1,1,0,1,0,5,5,2,exception location,1,0,\n'
                                       '\n')
        self.set_server_response('115647,524685,0,1,47835,Diet Dr Pepper 12-pack,000000025,Building 1,24,Bay 23,54321,00,58425148,Building 2,43,Bay 76,22345,00,51528610,12,21,0,\n'
                                       '\n')
        self.set_server_response('ALT PRE 1,ALT A 1,ALT POST 1,ALT S 1,99,ALTSCAN,Alt Location,0,\n'
                                       '\n')
        self.set_server_response('1,reason 1,0,\n'
                                       '2,reason 2,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '2!',           # Function?
                                   'yes',          # replenishment, correct?
                                   '1',            # Region?
                                   'yes',          # replenishment region 1, correct?
                                   'no',           # Another Region?
                                   'ready',        # To start replenishment, say ready
                                   'ready',        # Building 1
                                   'ready',        # Aisle 24
                                   'ready',        # Bay 23
                                   '00!',          # 5 4 3 2 1, pick up 25
                                   '25!',          # quantity
                                   'ready',        # Building 2
                                   'ready',        # Aisle 43
                                   'ready',        # Bay 76
                                   'override',     # 2 2 3 4 5
                                   'yes',          # Override, correct?
                                   'license',      # ALT PRE 1
                                   'location',     # ALT PRE 1
                                   'quantity',     # ALT PRE 1
                                   'description',  # ALT PRE 1
                                   'cancel',       # ALT PRE 1
                                   'yes',          # Cancel, correct?
                                   '1',            # Reason?
                                   'yes',          # 1, reason 1, correct?
                                   'ready',        # Deliver to exception location
                                   '-')            # 1 5 6 4 7 canceled, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'replenishment, correct?',
                              'Region?',
                              'replenishment region 1, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'Building 1',
                              'Aisle 24',
                              'Bay 23',
                              '5 4 3 2 1, pick up 25',
                              'quantity',
                              'Building 2',
                              'Aisle 43',
                              'Bay 76',
                              '2 2 3 4 5',
                              'Override, correct?',
                              'ALT PRE 1',
                              '5 2 4 6 8 5',
                              'ALT PRE 1',
                              'ALT PRE 1, Aisle <spell>ALT A 1</spell>, ALT POST 1, Slot <spell>ALT S 1</spell>',
                              'ALT PRE 1',
                              '25',
                              'ALT PRE 1',
                              'diet dr pepper 12-pack',
                              'ALT PRE 1',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>1</spell>, reason 1, correct?',
                              'Deliver to exception location',
                              '2 4 6 8 5 canceled, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '1', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTForkGetReplenishment'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '2', '115647', '12', '25'],
                                      ['prTaskLUTCoreGetReasonCodes', '2', '1'],
                                      ['prTaskLUTForkReplenishmentLicense', '115647', '25', '', '2', '1', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
