##TestLink ID 120573 :: Verify Change Function and Change Region commands
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testReplenishment_120573(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testReplenishment_120573(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,0,1,1,0,0,2,5,2,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,1,0,1,0,99,3,2,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,0,1,1,0,0,2,5,2,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('202343,0000002343,0,3,1070,Item 1070 Site 1,6,Building 181 Site 1,181,Bay 181,181,12343,1012343,Building 1070 Site 1,1070,Bay 1070,1070,1070,1070,1071,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,0,1,0,1,0,99,3,2,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,0,1,1,0,0,2,5,2,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('202346,0000002346,1,3,1073,Item 1073 Site 1,9,Building 178 Site 1,178,Bay 178,178,22346,1022346,Building 1073 Site 1,1073,Bay 1073,1073,1073,1073,1074,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('101,Region 101,0,\r\n'
                                 '102,Region 102,0,\r\n'
                                 '103,Region 103,0,\r\n'
                                 '104,Region 104,0,\r\n'
                                 '105,Region 105,0,\r\n'
                                 '106,Region 106,0,\r\n'
                                 '107,Region 107,0,\r\n'
                                 '108,Region 108,0,\r\n'
                                 '109,Region 109,0,\r\n'
                                 '110,Region 110,0,\r\n'
                                 '111,Region 111,0,\r\n'
                                 '112,Region 112,0,\r\n'
                                 '113,Region 113,0,\r\n'
                                 '114,Region 114,0,\r\n'
                                 '115,Region 115,0,\r\n'
                                 '116,Region 115Satish,0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '2',                # Function?
                                   'yes',              # Replenishment, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   'change function',  # To start replenishment, say ready
                                   'yes',              # change function, correct?
                                   '2',                # Function?
                                   'yes',              # Replenishment, correct?
                                   '2',                # Region?
                                   'yes',              # Region 2, correct?
                                   'no',               # Another Region?
                                   'change region',    # To start replenishment, say ready
                                   'yes',              # Change region, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   'ready',            # To start replenishment, say ready
                                   'ready',            # Building 181 Site 1
                                   'ready',            # Aisle 181
                                   'ready',            # Bay 181
                                   '12343',            # 1 8 1
                                   'ready',            # Building 1070 Site 1
                                   'ready',            # Aisle 1070
                                   'ready',            # Bay 1070
                                   '1070',             # 1 0 7 0
                                   'change region',    # Complete. Say ready.
                                   'yes',              # Change region, correct?
                                   '2',                # Region?
                                   'yes',              # Region 2, correct?
                                   'no',               # Another Region?
                                   'change region',    # To start replenishment, say ready
                                   'yes',              # Change region, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   'ready',            # To start replenishment, say ready
                                   'ready',            # Building 178 Site 1
                                   'ready',            # Aisle 178
                                   'ready',            # Bay 178
                                   '22346',            # 1 7 8, license 4 6
                                   'ready',            # Building 1073 Site 1
                                   'ready',            # Aisle 1073
                                   'ready',            # Bay 1073
                                   '1073',             # 1 0 7 3
                                   'change function',  # Complete. Say ready.
                                   'yes',              # change function, correct?
                                   '3',                # Function?
                                   'yes',              # Normal Assignments, correct?
                                   '-')                # Region?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Replenishment, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'change function, correct?',
                              'Function?',
                              'Replenishment, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'Change region, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'Building 181 Site 1',
                              'Aisle 181',
                              'Bay 181',
                              '1 8 1',
                              'Building 1070 Site 1',
                              'Aisle 1070',
                              'Bay 1070',
                              '1 0 7 0',
                              'Complete. Say ready.',
                              'Change region, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'Change region, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'To start replenishment, say ready',
                              'Building 178 Site 1',
                              'Aisle 178',
                              'Bay 178',
                              '1 7 8, license 4 6',
                              'Building 1073 Site 1',
                              'Aisle 1073',
                              'Bay 1073',
                              '1 0 7 3',
                              'Complete. Say ready.',
                              'change function, correct?',
                              'Function?',
                              'Normal Assignments, correct?',
                              'Region?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '3', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '2', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '3', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTForkGetReplenishment'],
                                      ['prTaskLUTForkReplenishmentLicense', '202343', '6', '1071', '1', '', '*'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '2', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTForkValidReplenishmentRegions'],
                                      ['prTaskLUTForkRequestReplenishmentRegion', '3', '0'],
                                      ['prTaskLUTForkReplenishmentRegionConfiguration'],
                                      ['prTaskLUTForkGetReplenishment'],
                                      ['prTaskLUTForkReplenishmentLicense', '202346', '9', '1074', '1', '', '*'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTRegionPermissionsForWorkType', '3'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
