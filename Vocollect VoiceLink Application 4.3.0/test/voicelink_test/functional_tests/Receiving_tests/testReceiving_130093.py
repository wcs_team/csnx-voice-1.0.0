##ID 130093 :: Test Case Test speaking Purchase Order

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testReceiving_130093(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testReceiving_130093(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,receiving region 1,0,\n'
                                 '2,receiving region 2,0,\n'
                                 '3,receiving region 3,0,\n'
                                 '\n')
        self.set_server_response('1,receiving region 1,0,0,\n'
                                 '\n')
        self.set_server_response('123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n'
                                 '124,421,Charmin toilet paper,15,0,,,0,,0,\n'
                                 '125,521,Target brand napkins,4,0,,,0,,0,\n'
                                 '\n')
        self.set_server_response('some item info,3,0,\n'
                                 '\n')
        self.set_server_response('some item info,3,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '12',     # Function?
                                   'yes',    # receiving, correct?
                                   '1',      # Region?
                                   'yes',    # receiving region 1, correct?
                                   '456!',   # Purchase order?
                                   'no',     # 456, correct?
                                   '456!',   # Purchase order?
                                   'yes',    # 456, correct?
                                   '123!',   # SKU?
                                   'no',     # 123, correct?
                                   '123!',   # SKU?
                                   'yes',    # 123, correct?
                                   '111!',   # LPN?
                                   'no',     # 111, correct?
                                   '111!',   # LPN?
                                   'yes',    # 111, correct?
                                   '2!',     # Quantity?
                                   '123!',   # SKU?
                                   'yes',    # 123, correct?
                                   '111!',   # LPN?
                                   'yes',    # 111, correct?
                                   '8!',     # Quantity?
                                   '124!',   # SKU?
                                   '-')      # 124, correct?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'receiving, correct?',
                              'Region?',
                              'receiving region 1, correct?',
                              'Purchase order?',
                              '456, correct?',
                              'Purchase order?',
                              '456, correct?',
                              'SKU?',
                              '123, correct?',
                              'SKU?',
                              '123, correct?',
                              'LPN?',
                              '111, correct?',
                              'LPN?',
                              '111, correct?',
                              'Quantity?',
                              'There are 8 remaining',
                              'SKU?',
                              '123, correct?',
                              'LPN?',
                              '111, correct?',
                              'Quantity?',
                              'There are 0 remaining',
                              'SKU?',
                              '124, correct?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTReceivingValidRegions'],
                                      ['prTaskLUTReceivingGetRegionConfiguration', '1'],
                                      ['prTaskLUTReceivingValidPO', '456'],
                                      ['prTaskLUTReceivingItemReceived', '123', '111', '2', ''],
                                      ['prTaskLUTReceivingItemReceived', '123', '111', '8', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
