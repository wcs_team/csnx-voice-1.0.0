##ID 130095 :: Test Case Test when non-existent SKU is spoken

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testReceiving_130095(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testReceiving_130095(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,receiving region 1,0,\n'
                                 '2,receiving region 2,0,\n'
                                 '3,receiving region 3,0,\n'
                                 '\n')
        self.set_server_response('1,receiving region 1,0,0,\n'
                                 '\n')
        self.set_server_response('123,321,Charmin paper towels,10,0,,,0,Paper products,0,\n'
                                 '124,421,Charmin toilet paper,15,0,,,0,,0,\n'
                                 '125,521,Target brand napkins,4,0,,,0,,0,\n'
                                 '\n')
        self.set_server_response('Color dup Ricoh 2200,1,NW,0,\n'
                                 'B W dup Ricoh 2100,2,NE,0,\n'
                                 'B W single Ricoh 2200,3,S,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('some item info,3,0,\n'
                                 '\n')
        self.set_server_response('some item info,3,0,\n'
                                 '\n')
        self.set_server_response('Color dup Ricoh 2200,1,NW,0,\n'
                                 'B W dup Ricoh 2100,2,NE,0,\n'
                                 'B W single Ricoh 2200,3,S,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('some item info,3,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '12',     # Function?
                                   'yes',    # receiving, correct?
                                   '1',      # Region?
                                   'yes',    # receiving region 1, correct?
                                   '456!',   # Purchase order?
                                   'yes',    # 456, correct?
                                   '111!',   # SKU?
                                   'yes',    # 111, correct?
                                   '2234!',  # SKU not found. Scan or speak UPC.
                                   'yes',    # 2234, correct?
                                   '1!',     # Choose the item from the list
                                   '1!',     # Printer?
                                   'yes',    # printer <spell>1</spell>, correct?
                                   '123!',   # SKU?
                                   'yes',    # 123, correct?
                                   '111!',   # LPN?
                                   'yes',    # 111, correct?
                                   '10!',    # Quantity?
                                   '124!',   # SKU?
                                   'yes',    # 124, correct?
                                   '122!',   # LPN?
                                   'yes',    # 122, correct?
                                   '10!',    # Quantity?
                                   '999!',   # SKU?
                                   'yes',    # 999, correct?
                                   'D111!',  # SKU not found. Scan or speak UPC.
                                   'yes',    # <spell>D111</spell>, correct?
                                   '2!',     # Choose the item from the list
                                   'yes',    # printer <spell>1</spell>, correct?
                                   '123!',   # SKU?
                                   'yes',    # 123, correct?
                                   'yes',    # Wrong SKU. Would you like to re enter the SKU?
                                   '125!',   # SKU?
                                   'yes',    # 125, correct?
                                   'yes',    # Wrong SKU. Would you like to re enter the SKU?
                                   '124!',   # SKU?
                                   'yes',    # 124, correct?
                                   '2235!',  # LPN?
                                   'yes',    # 2235, correct?
                                   '10!',    # Quantity?
                                   'no',     # You said 10, only 5 remaining. Authorize?
                                   '5!',     # Quantity?
                                   '-')      # SKU?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'receiving, correct?',
                              'Region?',
                              'receiving region 1, correct?',
                              'Purchase order?',
                              '456, correct?',
                              'SKU?',
                              '111, correct?',
                              'SKU not found. Scan or speak UPC.',
                              '2234, correct?',
                              'Choose the item from the list',
                              'Printer?',
                              'printer <spell>1</spell>, correct?',
                              'SKU?',
                              '123, correct?',
                              'LPN?',
                              '111, correct?',
                              'Quantity?',
                              'There are 0 remaining',
                              'SKU?',
                              '124, correct?',
                              'LPN?',
                              '122, correct?',
                              'Quantity?',
                              'There are 5 remaining',
                              'SKU?',
                              '999, correct?',
                              'SKU not found. Scan or speak UPC.',
                              '<spell>D111</spell>, correct?',
                              'Choose the item from the list',
                              'printer <spell>1</spell>, correct?',
                              'SKU?',
                              '123, correct?',
                              'Wrong SKU. Would you like to re enter the SKU?',
                              'SKU?',
                              '125, correct?',
                              'Wrong SKU. Would you like to re enter the SKU?',
                              'SKU?',
                              '124, correct?',
                              'LPN?',
                              '2235, correct?',
                              'Quantity?',
                              'You said 10, only 5 remaining. Authorize?',
                              'Quantity?',
                              'There are 0 remaining',
                              'SKU?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTReceivingValidRegions'],
                                      ['prTaskLUTReceivingGetRegionConfiguration', '1'],
                                      ['prTaskLUTReceivingValidPO', '456'],
                                      ['prTaskLutReceivingListOfPrinters', 'Empty'],
                                      ['prTaskLutReceivingPrint', '123', '1'],
                                      ['prTaskLUTReceivingItemReceived', '123', '111', '10', ''],
                                      ['prTaskLUTReceivingItemReceived', '124', '122', '10', ''],
                                      ['prTaskLutReceivingListOfPrinters', 'Empty'],
                                      ['prTaskLutReceivingPrint', '124', '1'],
                                      ['prTaskLUTReceivingItemReceived', '124', '2235', '5', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
