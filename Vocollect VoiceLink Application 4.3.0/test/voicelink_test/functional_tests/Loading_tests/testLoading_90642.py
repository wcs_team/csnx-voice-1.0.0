##TestLink ID 90642 :: Test Case Loading - Specify Region
##Verify the selector hears an error message if an error or warning is returned in response to the Valid loading regions LUT.

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testLoading_90642(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testLoading_90642(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,98,Error 98\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,99,Error 99\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,5,3,3,0,\n'
                                 '\n')
        self.set_server_response('1,123,1,789,5,13,route 123 has 5 stops, 5 remaining,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',     # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',      # Password?
                                   '10',        # Function?
                                   'yes',       # loading, correct?
                                   'sign off',  # Error 98, say sign off
                                   'ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '10',     # Function?
                                   'yes',    # loading, correct?
                                   'ready',  # Error 99, say ready
                                   'ready',  # To start loading, say ready
                                   '-')      # Printer?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'loading, correct?',
                              'Error 98, say sign off',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'loading, correct?',
                              'Error 99, say ready',
                              'To start loading, say ready',
                              'Printer?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTLoadingRequestRoute', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
