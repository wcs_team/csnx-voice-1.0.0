##TestLink ID 95611 :: Test Case Verify if Reprint report can be spoken for Automatic issuance
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testLoading_95611(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testLoading_95611(self):
        #queue of LUT responses
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,1,5,3,3,0,\n'
                                 '\n')
        self.set_server_response('1,123,0,,5,13,route 123 has 5 stops 5 remaining,0,\n'
                                 '\n')
        self.set_server_response('1,A12345,12345,N,1,0,\n'
                                 '1,B12345,12345,N,1,0,\n'
                                 '1,A11111,11111,N,1,0,\n'
                                 '1,A22222,22222,N,1,0,\n'
                                 '1,A33333,33333,N,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '10!',            # Function?
                                   'yes',            # loading, correct?
                                   '1',              # Region?
                                   'yes',            # loading region 1, correct?
                                   'ready',          # To start loading, say ready
                                   '123ready',       # Route?
                                   'yes',            # 123, correct?
                                   'ready',          # route has 5 containers, To continue say ready
                                   '13',             # Door 5
                                   '12345',          # Container?
                                   'yes',            # Container <spell>A12345</spell>?
                                   'left',           # Position?
                                   'yes',            # left, correct?
                                   'complete route', # Container?
                                   '12',             # Printer?
                                   'yes',            # 12, correct?
                                   'ready',          # Route complete, for next route say ready
                                   '-')              # Route?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'loading region 1, correct?',
                              'To start loading, say ready',
                              'Route?',
                              '123, correct?',
                              'route 123 has 5 stops 5 remaining, To continue say ready',
                              'Door 5',
                              'Container?',
                              'Container <spell>A12345</spell>?',
                              'Position?',
                              'left, correct?',
                              'Container?',
                              'Printer?',
                              'printer <spell>12</spell>, correct?',
                              'Route complete, for next route say ready',
                              'Route?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTLoadingRequestRoute', '123'],
                                      ['prTaskLUTLoadingRequestContainer', '12345', '1'],
                                      ['prTaskLUTLoadingUpdateContainer', 'L', 'A12345', '', 'left', ''],
                                      ['prTaskLUTLoadingCompleteRoute', '1'],
                                      ['prTaskLUTLoadingPrint', '1', '12'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
