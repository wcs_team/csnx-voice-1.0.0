##TestLink ID 91261 :: Test Case Unload when license is not loaded
##TestLink ID 91263 :: Test Case Unload when license is loaded 
##TestLink ID 91324 :: Test Case Verify if attempt to un-load the same license gives error

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testLoading_UnloadLicense1(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testLoading_UnloadLicense1(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,1,5,3,3,0,\n'
                                 '\n')
        self.set_server_response('1,123,0,,5,13,route 123 has 5 stops 5 remaining,0,\n'
                                 '\n')
        self.set_server_response('1,A12345,12345,N,1,0,\n'
                                 '1,B12345,12345,N,1,0,\n'
                                 '1,A11111,11111,L,1,0,\n'
                                 '1,A22222,22222,C,1,0,\n'
                                 '1,A33333,33333,N,1,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',           # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',            # Password?
                                   '10!',             # Function?
                                   'yes',             # loading, correct?
                                   '1',               # Region?
                                   'yes',             # loading region 1, correct?
                                   'ready',           # To start loading, say ready
                                   '123ready',        # Route?
                                   'yes',             # 123, correct?
                                   'ready',           # route has 5 containers, To continue say ready
                                   '13',              # Door 5
                                   'unload container',  # Container?
                                   '12345',           # Container to unload?
                                   'yes',             # Container <spell>A12345</spell>?
                                   'unload container',  # Container?
                                   '11111',           # Container to unload?
                                   'unload container',  # Container?
                                   '11111',           # Container to unload?
                                   '-')               # Container?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'loading region 1, correct?',
                              'To start loading, say ready',
                              'Route?',
                              '123, correct?',
                              'route 123 has 5 stops 5 remaining, To continue say ready',
                              'Door 5',
                              'Container?',
                              'Container to unload?',
                              'Container <spell>A12345</spell>?',
                              'Container?',
                              'Container to unload?',
                              'Container?',
                              'Container to unload?',
                              'Container?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTLoadingRequestRoute', '123'],
                                      ['prTaskLUTLoadingRequestContainer', '12345', '1'],
                                      ['prTaskLUTLoadingUpdateContainer', 'N', 'A12345', '', '', ''],
                                      ['prTaskLUTLoadingUpdateContainer', 'N', 'A11111', '', '', ''],
                                      ['prTaskLUTLoadingUpdateContainer', 'N', 'A11111', '', '', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
