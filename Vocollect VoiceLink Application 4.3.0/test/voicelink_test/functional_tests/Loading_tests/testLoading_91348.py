##TestLink ID 91348 :: Test Case Error when trying to consolidate a license with itself 
##TestLink ID 91350 :: Test Case Error when trying to load already Consolidated License 

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testLoading_91348(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testLoading_91348(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,0,\n'
                                 '2,loading region 2,0,\n'
                                 '3,loading region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,loading region 1,1,3,3,2,0,\n'
                                 '\n')
        self.set_server_response('1,123,0,,5,13,route 123 has 5 stops 5 remaining,0,\n'
                                 '\n')
        self.set_server_response('1,11145,111,N,0,0,\n'
                                 '1,22245,222,N,1,0,\n'
                                 '3,33373,333,N,1,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '10!',          # Function?
                                   'yes',          # loading, correct?
                                   '1',            # Region?
                                   'yes',          # loading region 1, correct?
                                   'ready',        # To start loading, say ready
                                   '123ready',     # Route?
                                   'yes',          # 123, correct?
                                   'ready',        # route 123 has 5 stops 5 remaining, To continue say ready
                                   '13',           # Door 5
                                   'consolidate',  # Container?
                                   '111',          # Main Container?
                                   '111',          # Container to consolidate?
                                   '222',          # Container to consolidate?
                                   'yes',          # Consolidate 22245 into 11145
                                   '222',          # Container?
                                   'left',          # Position?
                                   '-')            # Container?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'loading, correct?',
                              'Region?',
                              'loading region 1, correct?',
                              'To start loading, say ready',
                              'Route?',
                              '123, correct?',
                              'route 123 has 5 stops 5 remaining, To continue say ready',
                              'Door 5',
                              'Container?',
                              'Main Container?',
                              'Container to consolidate?',
                              'Container cannot be consolidated',
                              'Container to consolidate?',
                              'Consolidate 245 into 145',
                              'Container?',
                              'Position?',
                              'left, correct?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTLoadingValidRegions'],
                                      ['prTaskLUTLoadingRequestRegion', '1'],
                                      ['prTaskLUTLoadingRegionConfiguration'],
                                      ['prTaskLUTLoadingRequestRoute', '123'],
                                      ['prTaskLUTLoadingRequestContainer', '111', '1'],
                                      ['prTaskLUTLoadingUpdateContainer', 'C', '22245', '11145', '', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
