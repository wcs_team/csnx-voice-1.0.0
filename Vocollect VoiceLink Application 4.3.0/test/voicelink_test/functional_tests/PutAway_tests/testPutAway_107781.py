from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107781(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107781(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000002391,3,1095,1094,Building 1094 Site 1,1094,Bay 1094,1094,1094,1094,Item 1094 Site 1,6,5,0,\r\n'
                                 '0000020151,3,70,69,Building 69 Site 1,69,Bay 69,69,69,69,Item 69 Site 1,0,5,0,\r\n'
                                 '0000020152,3,71,70,Building 70 Site 1,70,Bay 70,70,70,70,Item 70 Site 1,0,10,0,\r\n'
                                 '0000020436,3,,,,,,,,197,Item 197 Site 1,11,10,0,\r\n'
                                 '0000020438,3,,,,,,,,199,Item 199 Site 1,13,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   '12',               # check digits?
                                   '002391',           # License?
                                   '020436',           # License?
                                   '020152',           # License?
                                   'no more',          # License?
                                   'ready',            # Put away license 9 1
                                   'ready',            # Building 1094 Site 1
                                   'ready',            # Aisle 1094
                                   'ready',            # Bay 1094
                                   '1094',             # 1 0 9 4
                                   'quantity',         # quantity
                                   '6!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   'release license',  # Put away license 5 1
                                   'yes',              # Release License, correct?
                                   'ready',            # 5 1 released, say ready
                                   'ready',            # Put away license 5 2
                                   'ready',            # Building 70 Site 1
                                   'ready',            # Aisle 70
                                   'ready',            # Bay 70
                                   '70',               # 7 0
                                   'quantity',         # quantity
                                   '13!',              # quantity
                                   'yes',              # 13, correct?
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 3 6
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '11!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '-')                # Put away license 3 8

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>91</spell>',
                              'Building 1094 Site 1',
                              'Aisle 1094',
                              'Bay 1094',
                              '1 0 9 4',
                              'quantity',
                              '6',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>51</spell>',
                              'Release License, correct?',
                              '5 1 released, say ready',
                              'Put away license <spell>52</spell>',
                              'Building 70 Site 1',
                              'Aisle 70',
                              'Bay 70',
                              '7 0',
                              'quantity',
                              'quantity not available',
                              'quantity',
                              '13, correct?',
                              'Complete. Say ready.',
                              'Put away license <spell>36</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '11',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>38</spell>')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '002391', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020436', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020152', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000002391', '6', '1095', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020151', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020152', '13', '71', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020436', '11', '1303', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
