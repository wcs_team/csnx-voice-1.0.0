##TestLink ID 107767 :: Test Case Tests with licenses that have loc specified but no qty
##Since this is run against LUT/odr server, the flow is a little different since there is no
##management of licenses like VoiceLink host. When run against VL, VL asks the operator to
##speak one license at a time. Whereas when run against LUT/ODR, all the licenses are returned
##in response to prTaskLUTForkGetPutAway resulting in operator not asked every time for 
##license number. That is the only difference in the flow

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107767(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107767(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,put away region 1,0,\n'
                                 '2,put away region 2,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('2,put away region 2,1,1,1,0,1,0,1,0,5,4,0,2,1,exception location,1,0,\n'
                                 '\n')
        self.set_server_response('locID,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('12345,2,67890,67890656521,Building 1,23,Bay 33,890,00,9998741,Coca-Cola 12-pack,,15.5,0,\n'
                                 '12346,2,67891,67890656522,Building 1,23,Bay 34,891,00,9998742,Diet-Pepsi 12-pack,,30,0,\n'
                                 '12347,2,67892,67890656523,Building 1,23,Bay 35,892,00,9998743,Dr Pepper1 18-pack,,27,0,\n'
                                 '12348,2,67893,67890656524,Building 1,23,Bay 35,892,00,9998744,Dr Pepper2 18-pack,,27,0,\n'
                                 '12349,2,67894,67890656525,Building 1,23,Bay 35,893,00,9998745,Dr Pepper3 18-pack,,27,0,\n'
                                 '12350,2,67895,67890656526,Building 1,24,Bay 36,894,00,9998746,Dr Pepper4 18-pack,,27,0,\n'
                                 '12351,2,67896,67890656527,Building 1,25,Bay 37,895,00,9998747,Dr Pepper5 18-pack,,27,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1!',               # Function?
                                   'yes',              # put away, correct?
                                   '2',                # Region?
                                   'yes',              # put away region 2, correct?
                                   'no',               # Another Region?
                                   '12345ready',       # Start location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   '2345',             # License?
                                   'yes',              # 2345, correct?
                                   'ready',            # Put away license 1 2 3 4 5
                                   'quantity',         # quantity
                                   '10!',              # quantity
                                   'yes',              # 10, correct?
                                   'ready',            # Building 1
                                   'ready',            # Aisle 23
                                   'ready',            # Bay 33
                                   '00!',              # 8 9 0
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 1 2 3 4 6
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '1',                # Reason?
                                   'yes',              # 1, reason 1, correct?
                                   'ready',            # Deliver to exception location
                                   'ready',            # 1 2 3 4 6 canceled, say ready
                                   'ready',            # Put away license 1 2 3 4 7
                                   '18!',              # quantity
                                   'yes',              # 18, correct?
                                   'ready',            # Building 1
                                   'ready',            # Aisle 23
                                   'ready',            # Bay 35
                                   'cancel',           # 8 9 2
                                   'yes',              # Cancel, correct?
                                   '1',                # Reason?
                                   'yes',              # 1, reason 1, correct?
                                   'ready',            # Deliver to exception location
                                   'ready',            # 1 2 3 4 7 canceled, say ready
                                   'ready',            # Put away license 1 2 3 4 8
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 1 2 3 4 8 released, say ready
                                   'ready',            # Put away license 1 2 3 4 9
                                   '14!',              # quantity
                                   'yes',              # 14, correct?
                                   'ready',            # Building 1
                                   'ready',            # Aisle 23
                                   'ready',            # Bay 35
                                   'partial',          # 8 9 3
                                   'release license',  # 8 9 3
                                   'yes',              # Release License, correct?
                                   '-')                # 1 2 3 4 9 released, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'put away, correct?',
                              'Region?',
                              'put away region 2, correct?',
                              'Another Region?',
                              'Start location?',
                              '12345, correct?',
                              'check digits?',
                              'License?',
                              '2345, correct?',
                              'getting work',
                              'Put away license <spell>12345</spell>',
                              'quantity',
                              'quantity not available',
                              'quantity',
                              '10, correct?',
                              'Building 1',
                              'Aisle 23',
                              'Bay 33',
                              '8 9 0',
                              'Complete. Say ready.',
                              'Put away license <spell>12346</spell>',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>1</spell>, reason 1, correct?',
                              'Deliver to exception location',
                              '1 2 3 4 6 canceled, say ready',
                              'Put away license <spell>12347</spell>',
                              'quantity',
                              '18, correct?',
                              'Building 1',
                              'Aisle 23',
                              'Bay 35',
                              '8 9 2',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>1</spell>, reason 1, correct?',
                              'Deliver to exception location',
                              '1 2 3 4 7 canceled, say ready',
                              'Put away license <spell>12348</spell>',
                              'quantity',
                              'Release License, correct?',
                              '1 2 3 4 8 released, say ready',
                              'Put away license <spell>12349</spell>',
                              'quantity',
                              '14, correct?',
                              'Building 1',
                              'Aisle 23',
                              'Bay 35',
                              '8 9 3',
                              'partial not allowed in this region',
                              '8 9 3',
                              'Release License, correct?',
                              '1 2 3 4 9 released, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '2', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '2345', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '12345', '10', '67890', '1', '', '*'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '12346', '0', '', '2', '1', '*'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '12347', '18', '', '2', '1', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '12348', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '12349', '0', '', '3', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
