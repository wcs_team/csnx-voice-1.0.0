# Verify cancel and release license commands work correctly in PutAway Region 104
# Rename the file testGenerator_PutAway_107783.txt to testGenerator_PutAway_107783.py and copy the file to the
# root level of the VoiceLink project and run to regenerate the python unit test if necessary.
# Generated using PutAway_1.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107783(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()
        
    def testPutAway_107783(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,Region 1,0,\n'
                                       '2,Region 2,0,\n'
                                       '3,Region 3,0,\n'
                                       '4,Region 4,0,\n'
                                       '5,Region 5,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('4,Region 4,1,1,1,1,1,1,1,1,2,5,0,0,4,Exception Location,1,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('52125,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53125,4,,,,,,,,,,0,0,0,\n'
                                       '53126,4,,,,,,,,,,0,0,0,\n'
                                       '53128,4,,,,,,,,,,0,0,0,\n'
                                       '53129,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53310,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('2,Location Extra Full,0,\n'
                                       '25,Got Tired,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53311,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('2,Location Extra Full,0,\n'
                                       '25,Got Tired,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53312,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('2,Location Extra Full,0,\n'
                                       '25,Got Tired,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53313,4,,,,,,,,,,0,0,0,\n'
                                       '53314,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('2,Location Extra Full,0,\n'
                                       '25,Got Tired,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53415,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53416,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53420,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('53431,4,,,,,,,,,,0,0,0,\n'
                                       '53433,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1!',               # Function?
                                   'yes',              # put away, correct?
                                   '4',                # Region?
                                   'yes',              # Region 4, correct?
                                   'no',               # Another Region?
                                   '101ready',         # Start location?
                                   'yes',              # 101, correct?
                                   '101ready',         # check digits?
                                   '52125',            # License?
                                   'yes',              # 52125, correct?
                                   'no more',          # License?
                                   '231!',             # quantity
                                   'yes',              # 231, correct?
                                   '10ready',          # Location?
                                   'yes',              # 10, correct?
                                   '10ready',          # check digits?
                                   '231!',             # quantity
                                   'ready',            # Complete. Say ready.
                                   '53125',            # License?
                                   'yes',              # 53125, correct?
                                   '53126',            # License?
                                   'yes',              # 53126, correct?
                                   '53128',            # License?
                                   'yes',              # 53128, correct?
                                   '53129',            # License?
                                   'yes',              # 53129, correct?
                                   'ready',            # Put away license 2 5
                                   '26!',              # quantity
                                   'yes',              # 26, correct?
                                   '11ready',          # Location?
                                   'yes',              # 11, correct?
                                   '11ready',          # check digits?
                                   '26!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 2 6
                                   '8!',               # quantity
                                   'yes',              # 8, correct?
                                   '102ready',         # Location?
                                   'yes',              # 102, correct?
                                   '102ready',         # check digits?
                                   '8!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 2 8
                                   '288!',             # quantity
                                   'yes',              # 288, correct?
                                   '103ready',         # Location?
                                   'yes',              # 103, correct?
                                   '103ready',         # check digits?
                                   '288!',             # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 2 9
                                   '30!',              # quantity
                                   'yes',              # 30, correct?
                                   '13ready',          # Location?
                                   'yes',              # 13, correct?
                                   '13ready',          # check digits?
                                   '30!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '53310',            # License?
                                   'yes',              # 53310, correct?
                                   'no more',          # License?
                                   '25!',              # quantity
                                   'yes',              # 25, correct?
                                   '102ready',         # Location?
                                   'yes',              # 102, correct?
                                   '102ready',         # check digits?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '25',               # Reason?
                                   'yes',              # 25, Got Tired, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 1 0 canceled, say ready
                                   '53311',            # License?
                                   'yes',              # 53311, correct?
                                   'no more',          # License?
                                   '26!',              # quantity
                                   'yes',              # 26, correct?
                                   'cancel',           # Location?
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 1 1 canceled, say ready
                                   '53312',            # License?
                                   'yes',              # 53312, correct?
                                   'no more',          # License?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '25',               # Reason?
                                   'yes',              # 25, Got Tired, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 1 2 canceled, say ready
                                   '53313',            # License?
                                   'yes',              # 53313, correct?
                                   '53314',            # License?
                                   'yes',              # 53314, correct?
                                   'no more',          # License?
                                   'ready',            # Put away license 1 3
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 1 3 canceled, say ready
                                   'ready',            # Put away license 1 4
                                   '15!',              # quantity
                                   'yes',              # 15, correct?
                                   '102ready',         # Location?
                                   'yes',              # 102, correct?
                                   '102ready',         # check digits?
                                   '15!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '53415',            # License?
                                   'yes',              # 53415, correct?
                                   'no more',          # License?
                                   '29!',              # quantity
                                   'yes',              # 29, correct?
                                   '11ready',          # Location?
                                   'yes',              # 11, correct?
                                   '11ready',          # check digits?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 1 5 released, say ready
                                   '53416',            # License?
                                   'yes',              # 53416, correct?
                                   'no more',          # License?
                                   '29!',              # quantity
                                   'yes',              # 29, correct?
                                   'release license',  # Location?
                                   'yes',              # Release License, correct?
                                   'ready',            # 1 6 released, say ready
                                   '53420',            # License?
                                   'yes',              # 53420, correct?
                                   'no more',          # License?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 2 0 released, say ready
                                   '53431',            # License?
                                   'yes',              # 53431, correct?
                                   '53433',            # License?
                                   'yes',              # 53433, correct?
                                   'no more',          # License?
                                   'release license',  # Put away license 3 1
                                   'yes',              # Release License, correct?
                                   'ready',            # 3 1 released, say ready
                                   'ready',            # Put away license 3 3
                                   '34!',              # quantity
                                   'yes',              # 34, correct?
                                   '12ready',          # Location?
                                   'yes',              # 12, correct?
                                   '12ready',          # check digits?
                                   '34!',              # quantity
                                   '-')                # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'put away, correct?',
                              'Region?',
                              'Region 4, correct?',
                              'Another Region?',
                              'Start location?',
                              '101, correct?',
                              'check digits?',
                              'License?',
                              '52125, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '231, correct?',
                              'Location?',
                              '10, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '53125, correct?',
                              'License?',
                              '53126, correct?',
                              'License?',
                              '53128, correct?',
                              'License?',
                              '53129, correct?',
                              'getting work',
                              'Put away license <spell>25</spell>',
                              'quantity',
                              '26, correct?',
                              'Location?',
                              '11, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>26</spell>',
                              'quantity',
                              '8, correct?',
                              'Location?',
                              '102, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>28</spell>',
                              'quantity',
                              '288, correct?',
                              'Location?',
                              '103, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>29</spell>',
                              'quantity',
                              '30, correct?',
                              'Location?',
                              '13, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '53310, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '25, correct?',
                              'Location?',
                              '102, correct?',
                              'check digits?',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>25</spell>, Got Tired, correct?',
                              'Deliver to Exception Location',
                              '1 0 canceled, say ready',
                              'License?',
                              '53311, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '26, correct?',
                              'Location?',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '1 1 canceled, say ready',
                              'License?',
                              '53312, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>25</spell>, Got Tired, correct?',
                              'Deliver to Exception Location',
                              '1 2 canceled, say ready',
                              'License?',
                              '53313, correct?',
                              'License?',
                              '53314, correct?',
                              'License?',
                              'getting work',
                              'Put away license <spell>13</spell>',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '1 3 canceled, say ready',
                              'Put away license <spell>14</spell>',
                              'quantity',
                              '15, correct?',
                              'Location?',
                              '102, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '53415, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '29, correct?',
                              'Location?',
                              '11, correct?',
                              'check digits?',
                              'quantity',
                              'Release License, correct?',
                              '1 5 released, say ready',
                              'License?',
                              '53416, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '29, correct?',
                              'Location?',
                              'Release License, correct?',
                              '1 6 released, say ready',
                              'License?',
                              '53420, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              'Release License, correct?',
                              '2 0 released, say ready',
                              'License?',
                              '53431, correct?',
                              'License?',
                              '53433, correct?',
                              'License?',
                              'getting work',
                              'Put away license <spell>31</spell>',
                              'Release License, correct?',
                              '3 1 released, say ready',
                              'Put away license <spell>33</spell>',
                              'quantity',
                              '34, correct?',
                              'Location?',
                              '12, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '4', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '101', '101', '1'],
                                      ['prTaskLUTForkVerifyLicense', '52125', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '10', '10', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '52125', '231', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53125', '0'],
                                      ['prTaskLUTForkVerifyLicense', '53126', '0'],
                                      ['prTaskLUTForkVerifyLicense', '53128', '0'],
                                      ['prTaskLUTForkVerifyLicense', '53129', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '11', '11', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '53125', '26', 'locID', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '102', '102', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '53126', '8', 'locID', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '103', '103', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '53128', '288', 'locID', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '13', '13', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '53129', '30', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53310', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '102', '102', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '53310', '25', '', '2', '25', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53311', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '53311', '26', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53312', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '53312', '0', '', '2', '25', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53313', '0'],
                                      ['prTaskLUTForkVerifyLicense', '53314', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '53313', '0', '', '2', '2', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '102', '102', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '53314', '15', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53415', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '11', '11', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '53415', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53416', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '53416', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53420', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '53420', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '53431', '0'],
                                      ['prTaskLUTForkVerifyLicense', '53433', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '53431', '0', '', '3', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '53433', '34', 'locID', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
