##TestLink ID 107863 :: Test Case Test with licenses that have Location specified but not Qty - part2

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107863(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107863(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020127,3,62,61,Building 61 Site 1,61,Bay 61,61,61,61,Item 61 Site 1,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020138,3,65,64,Building 64 Site 1,64,Bay 64,64,64,64,Item 64 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020139,3,66,65,Building 65 Site 1,65,Bay 65,65,65,65,Item 65 Site 1,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020139,3,66,65,Building 65 Site 1,65,Bay 65,65,65,65,Item 65 Site 1,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020140,3,67,66,Building 66 Site 1,66,Bay 66,66,66,66,Item 66 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020140,3,67,66,Building 66 Site 1,66,Bay 66,66,66,66,Item 66 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   '12',               # check digits?
                                   '020127',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 61 Site 1
                                   'ready',            # Aisle 61
                                   'ready',            # Bay 61
                                   'partial',          # 6 1
                                   'yes',              # partial, correct?
                                   '61',               # 6 1
                                   '7!',               # quantity
                                   'yes',              # 7, correct?
                                   'ready',            # Complete. Say ready.
                                   '020138',           # License?
                                   'no more',          # License?
                                   'rready',           # Building 64 Site 1
                                   'ready',            # Building 64 Site 1
                                   'ready',            # Aisle 64
                                   'ready',            # Bay 64
                                   'partial',          # 6 4
                                   'yes',              # partial, correct?
                                   '64',               # 6 4
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 3 8 canceled, say ready
                                   '020139',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 65 Site 1
                                   'ready',            # Aisle 65
                                   'ready',            # Bay 65
                                   'partial',          # 6 5
                                   'yes',              # partial, correct?
                                   '65',               # 6 5
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 3 9 released, say ready
                                   '020139',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 65 Site 1
                                   'ready',            # Aisle 65
                                   'ready',            # Bay 65
                                   'override',         # 6 5
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '9!',               # quantity
                                   'no',               # 9, correct?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 3 9 canceled, say ready
                                   '020140',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 66 Site 1
                                   'ready',            # Aisle 66
                                   'ready',            # Bay 66
                                   'override',         # 6 6
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 4 0 released, say ready
                                   '020140',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 66 Site 1
                                   'ready',            # Aisle 66
                                   'ready',            # Bay 66
                                   'override',         # 6 6
                                   'yes',              # Override, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '14!',              # quantity
                                   'yes',              # 14, correct?
                                   'ready',            # Complete. Say ready.
                                   '-')                # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 61 Site 1',
                              'Aisle 61',
                              'Bay 61',
                              '6 1',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '7, correct?',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 64 Site 1',
                              'Aisle 64',
                              'Bay 64',
                              '6 4',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '3 8 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 65 Site 1',
                              'Aisle 65',
                              'Bay 65',
                              '6 5',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              'Release License, correct?',
                              '3 9 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 65 Site 1',
                              'Aisle 65',
                              'Bay 65',
                              '6 5',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '9, correct?',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '3 9 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 66 Site 1',
                              'Aisle 66',
                              'Bay 66',
                              '6 6',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Release License, correct?',
                              '4 0 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 66 Site 1',
                              'Aisle 66',
                              'Bay 66',
                              '6 6',
                              'Override, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '14, correct?',
                              'Complete. Say ready.',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '020127', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020127', '7', '62', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020138', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020138', '0', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020139', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020139', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020139', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020139', '66', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020139', '0', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020140', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020140', '67', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020140', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020140', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020140', '67', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020140', '14', '1303', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
