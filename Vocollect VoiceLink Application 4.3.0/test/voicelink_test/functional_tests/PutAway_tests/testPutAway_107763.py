##TestLink ID 107763 :: Test Case Tests with Licenses that have Qty and Loc specified

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSample(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSample(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,1,1,0,1,0,1,0,5,5,0,2,1,Exception Location,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000184,2,86,85,Building 85 Site 1,85,Bay 85,85,85,85,Item 85 Site 1,8,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000187,2,89,88,Building 88 Site 1,88,Bay 88,88,88,88,Item 88 Site 1,11,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000196,2,90,89,Building 89 Site 1,89,Bay 89,89,89,89,Item 89 Site 1,20,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000199,2,93,92,Building 92 Site 1,92,Bay 92,92,92,92,Item 92 Site 1,7,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000202,2,96,95,Building 95 Site 1,95,Bay 95,95,95,95,Item 95 Site 1,6,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000199,2,93,92,Building 92 Site 1,92,Bay 92,92,92,92,Item 92 Site 1,7,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000213,2,99,98,Building 98 Site 1,98,Bay 98,98,98,98,Item 98 Site 1,15,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000216,2,102,101,Building 101 Site 1,101,Bay 101,101,101,101,Item 101 Site 1,18,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000228,2,106,105,Building 105 Site 1,105,Bay 105,105,105,105,Item 105 Site 1,14,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000231,2,109,108,Building 108 Site 1,108,Bay 108,108,108,108,Item 108 Site 1,17,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '2',                # Region?
                                   'yes',              # Region 2, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   'ready',            # Start location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   '00184',            # License?
                                   'yes',              # 00184, correct?
                                   '8!',               # quantity
                                   'ready',            # Building 85 Site 1
                                   'ready',            # Aisle 85
                                   'ready',            # Bay 85
                                   '85',               # 8 5
                                   'ready',            # Complete. Say ready.
                                   '00187',            # License?
                                   'yes',              # 00187, correct?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 0 0 1 8 7 canceled, say ready
                                   '00196',            # License?
                                   'yes',              # 00196, correct?
                                   '20!',              # quantity
                                   'ready',            # Building 89 Site 1
                                   'ready',            # Aisle 89
                                   'ready',            # Bay 89
                                   'cancel',           # 8 9
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 0 0 1 9 6 canceled, say ready
                                   '00199',            # License?
                                   'yes',              # 00199, correct?
                                   '7!',               # quantity
                                   'ready',            # Building 92 Site 1
                                   'ready',            # Aisle 92
                                   'ready',            # Bay 92
                                   'release license',  # 9 2
                                   'yes',              # Release License, correct?
                                   'ready',            # 0 0 1 9 9 released, say ready
                                   '00202',            # License?
                                   'yes',              # 00202, correct?
                                   '6!',               # quantity
                                   'ready',            # Building 95 Site 1
                                   'ready',            # Aisle 95
                                   'ready',            # Bay 95
                                   'partial',          # 9 5
                                   '95',               # 9 5
                                   'ready',            # Complete. Say ready.
                                   '00199',            # License?
                                   'yes',              # 00199, correct?
                                   '4!',               # quantity
                                   'yes',              # You said 4, expected 7, correct?
                                   'ready',            # Building 92 Site 1
                                   'ready',            # Aisle 92
                                   'ready',            # Bay 92
                                   '92',               # 9 2
                                   'ready',            # Complete. Say ready.
                                   '00213',            # License?
                                   'yes',              # 00213, correct?
                                   '10!',              # quantity
                                   'no',               # You said 10, expected 15, correct?
                                   '20!',              # quantity
                                   'yes',              # You said 20, expected 15, correct?
                                   'ready',            # Building 98 Site 1
                                   'ready',            # Aisle 98
                                   'ready',            # Bay 98
                                   '98',               # 9 8
                                   'ready',            # Complete. Say ready.
                                   '00216',            # License?
                                   'yes',              # 00216, correct?
                                   '1!',               # quantity
                                   'no',               # You said 1, expected 18, correct?
                                   '22!',              # quantity
                                   'no',               # You said 22, expected 18, correct?
                                   '18!',              # quantity
                                   'ready',            # Building 101 Site 1
                                   'ready',            # Aisle 101
                                   'ready',            # Bay 101
                                   '101',              # 1 0 1
                                   'ready',            # Complete. Say ready.
                                   '00228',            # License?
                                   'yes',              # 00228, correct?
                                   '10!',              # quantity
                                   'yes',              # You said 10, expected 14, correct?
                                   'ready',            # Building 105 Site 1
                                   'ready',            # Aisle 105
                                   'ready',            # Bay 105
                                   'cancel',           # 1 0 5
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 0 0 2 2 8 canceled, say ready
                                   '00231',            # License?
                                   'yes',              # 00231, correct?
                                   '10!',              # quantity
                                   'yes',              # You said 10, expected 17, correct?
                                   'ready',            # Building 108 Site 1
                                   'ready',            # Aisle 108
                                   'ready',            # Bay 108
                                   'release license',  # 1 0 8
                                   'yes',              # Release License, correct?
                                   '-')                # 0 0 2 3 1 released, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'Another Region?',
                              'Start location?',
                              '12345, correct?',
                              'check digits?',
                              'License?',
                              '00184, correct?',
                              'getting work',
                              'quantity',
                              'Building 85 Site 1',
                              'Aisle 85',
                              'Bay 85',
                              '8 5',
                              'Complete. Say ready.',
                              'License?',
                              '00187, correct?',
                              'getting work',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '0 0 1 8 7 canceled, say ready',
                              'License?',
                              '00196, correct?',
                              'getting work',
                              'quantity',
                              'Building 89 Site 1',
                              'Aisle 89',
                              'Bay 89',
                              '8 9',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '0 0 1 9 6 canceled, say ready',
                              'License?',
                              '00199, correct?',
                              'getting work',
                              'quantity',
                              'Building 92 Site 1',
                              'Aisle 92',
                              'Bay 92',
                              '9 2',
                              'Release License, correct?',
                              '0 0 1 9 9 released, say ready',
                              'License?',
                              '00202, correct?',
                              'getting work',
                              'quantity',
                              'Building 95 Site 1',
                              'Aisle 95',
                              'Bay 95',
                              '9 5',
                              'partial not allowed in this region',
                              '9 5',
                              'Complete. Say ready.',
                              'License?',
                              '00199, correct?',
                              'getting work',
                              'quantity',
                              'You said 4, expected 7, correct?',
                              'Building 92 Site 1',
                              'Aisle 92',
                              'Bay 92',
                              '9 2',
                              'Complete. Say ready.',
                              'License?',
                              '00213, correct?',
                              'getting work',
                              'quantity',
                              'You said 10, expected 15, correct?',
                              'quantity',
                              'You said 20, expected 15, correct?',
                              'Building 98 Site 1',
                              'Aisle 98',
                              'Bay 98',
                              '9 8',
                              'Complete. Say ready.',
                              'License?',
                              '00216, correct?',
                              'getting work',
                              'quantity',
                              'You said 1, expected 18, correct?',
                              'quantity',
                              'You said 22, expected 18, correct?',
                              'quantity',
                              'Building 101 Site 1',
                              'Aisle 101',
                              'Bay 101',
                              '1 0 1',
                              'Complete. Say ready.',
                              'License?',
                              '00228, correct?',
                              'getting work',
                              'quantity',
                              'You said 10, expected 14, correct?',
                              'Building 105 Site 1',
                              'Aisle 105',
                              'Bay 105',
                              '1 0 5',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '0 0 2 2 8 canceled, say ready',
                              'License?',
                              '00231, correct?',
                              'getting work',
                              'quantity',
                              'You said 10, expected 17, correct?',
                              'Building 108 Site 1',
                              'Aisle 108',
                              'Bay 108',
                              '1 0 8',
                              'Release License, correct?',
                              '0 0 2 3 1 released, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '2', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '00184', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000184', '8', '86', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00187', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000187', '11', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00196', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000196', '20', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00199', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000199', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00202', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000202', '6', '96', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00199', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000199', '4', '93', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00213', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000213', '20', '99', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00216', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000216', '18', '102', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00228', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000228', '10', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00231', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000231', '0', '', '3', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
