##TestLink ID 107775 :: Test Case Test with Licences that have Qty and Location specified - part1
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testtestPutAway_107876(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testtestPutAway_107876(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000966,3,444,443,Building 443 Site 1,443,Bay 443,443,443,443,Item 443 Site 1,5,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000978,3,448,447,Building 447 Site 1,447,Bay 447,447,447,447,Item 447 Site 1,17,10,0,\r\n'
                                 '0000000981,3,451,450,Building 450 Site 1,450,Bay 450,450,450,450,Item 450 Site 1,20,5,0,\r\n'
                                 '0000000996,3,458,457,Building 457 Site 1,457,Bay 457,457,457,457,Item 457 Site 1,19,10,0,\r\n'
                                 '0000001008,3,462,461,Building 461 Site 1,461,Bay 461,461,461,461,Item 461 Site 1,16,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001026,3,472,471,Building 471 Site 1,471,Bay 471,471,471,471,Item 471 Site 1,18,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001038,3,476,475,Building 475 Site 1,475,Bay 475,475,475,475,Item 475 Site 1,14,10,0,\r\n'
                                 '0000001041,3,479,478,Building 478 Site 1,478,Bay 478,478,478,478,Item 478 Site 1,17,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001053,3,483,482,Building 482 Site 1,482,Bay 482,482,482,482,Item 482 Site 1,13,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001056,3,486,485,Building 485 Site 1,485,Bay 485,485,485,485,Item 485 Site 1,16,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001068,3,490,489,Building 489 Site 1,489,Bay 489,489,489,489,Item 489 Site 1,12,10,0,\r\n'
                                 '0000001071,3,493,492,Building 492 Site 1,492,Bay 492,492,492,492,Item 492 Site 1,15,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',     # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',      # Password?
                                   '1',         # Function?
                                   'yes',       # Put Away, correct?
                                   '3',         # Region?
                                   'yes',       # Region 3, correct?
                                   'no',        # Another Region?
                                   '12345',     # Start location?
                                   '12',        # check digits?
                                   '000966',    # License?
                                   'no more',   # License?
                                   'ready',     # Building 443 Site 1
                                   'ready',     # Aisle 443
                                   'ready',     # Bay 443
                                   '443',       # 4 4 3
                                   '5!',        # quantity
                                   'ready',     # Complete. Say ready.
                                   '000978',    # License?
                                   '000981',    # License?
                                   '000996',    # License?
                                   '001008',    # License?
                                   'ready',     # Put away license 7 8
                                   'ready',     # Building 447 Site 1
                                   'ready',     # Aisle 447
                                   'ready',     # Bay 447
                                   '447',       # 4 4 7
                                   '17!',       # quantity
                                   'ready',     # Complete. Say ready.
                                   'ready',     # Put away license 8 1
                                   'ready',     # Building 450 Site 1
                                   'ready',     # Aisle 450
                                   'ready',     # Bay 450
                                   '450',       # 4 5 0
                                   '20!',       # quantity
                                   'ready',     # Complete. Say ready.
                                   'ready',     # Put away license 9 6
                                   'ready',     # Building 457 Site 1
                                   'ready',     # Aisle 457
                                   'ready',     # Bay 457
                                   '457',       # 4 5 7
                                   '19!',       # quantity
                                   'ready',     # Complete. Say ready.
                                   'ready',     # Put away license 0 8
                                   'ready',     # Building 461 Site 1
                                   'ready',     # Aisle 461
                                   'ready',     # Bay 461
                                   '461',       # 4 6 1
                                   '16!',       # quantity
                                   'ready',     # Complete. Say ready.
                                   '001026',    # License?
                                   'no more',   # License?
                                   'ready',     # Building 471 Site 1
                                   'ready',     # Aisle 471
                                   'ready',     # Bay 471
                                   'override',  # 4 7 1
                                   'yes',       # Override, correct?
                                   '12345',     # Location?
                                   '12',        # check digits?
                                   '8!',        # quantity
                                   'no',        # You said 8, expected 18, is this a partial?
                                   '18!',       # quantity
                                   'ready',     # Complete. Say ready.
                                   '001038',    # License?
                                   '001041',    # License?
                                   'no more',   # License?
                                   'ready',     # Put away license 3 8
                                   'ready',     # Building 475 Site 1
                                   'ready',     # Aisle 475
                                   'ready',     # Bay 475
                                   'override',  # 4 7 5
                                   'yes',       # Override, correct?
                                   '12345',     # Location?
                                   '12',        # check digits?
                                   '14!',       # quantity
                                   'ready',     # Complete. Say ready.
                                   'ready',     # Put away license 4 1
                                   'ready',     # Building 478 Site 1
                                   'ready',     # Aisle 478
                                   'ready',     # Bay 478
                                   '478',       # 4 7 8
                                   '17!',       # quantity
                                   'ready',     # Complete. Say ready.
                                   '001053',    # License?
                                   'no more',   # License?
                                   'ready',     # Building 482 Site 1
                                   'ready',     # Aisle 482
                                   'ready',     # Bay 482
                                   '482',       # 4 8 2
                                   'cancel',    # quantity
                                   'yes',       # Cancel, correct?
                                   'yes',       # Reason?
                                   '2!',        # Reason?
                                   'yes',       # 2, Location Extra Full, correct?
                                   'ready',     # Deliver to Exception Location
                                   'ready',     # 5 3 canceled, say ready
                                   '001056',    # License?
                                   'no more',   # License?
                                   'ready',     # Building 485 Site 1
                                   'rready',    # Aisle 485
                                   'ready',     # Aisle 485
                                   'ready',     # Bay 485
                                   'cancel',    # 4 8 5
                                   'yes',       # Cancel, correct?
                                   '2!',        # Reason?
                                   'yes',       # 2, Location Extra Full, correct?
                                   'ready',     # Deliver to Exception Location
                                   'ready',     # 5 6 canceled, say ready
                                   '001068',    # License?
                                   '001071',    # License?
                                   'no more',   # License?
                                   'ready',     # Put away license 6 8
                                   'ready',     # Building 489 Site 1
                                   'ready',     # Aisle 489
                                   'ready',     # Bay 489
                                   'cancel',    # 4 8 9
                                   'yes',       # Cancel, correct?
                                   '2!',        # Reason?
                                   'yes',       # 2, Location Extra Full, correct?
                                   'ready',     # Deliver to Exception Location
                                   'ready',     # 6 8 canceled, say ready
                                   'ready',     # Put away license 7 1
                                   'ready',     # Building 492 Site 1
                                   'ready',     # Aisle 492
                                   'ready',     # Bay 492
                                   '492',       # 4 9 2
                                   '15!',       # quantity
                                   '-')         # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 443 Site 1',
                              'Aisle 443',
                              'Bay 443',
                              '4 4 3',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>78</spell>',
                              'Building 447 Site 1',
                              'Aisle 447',
                              'Bay 447',
                              '4 4 7',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>81</spell>',
                              'Building 450 Site 1',
                              'Aisle 450',
                              'Bay 450',
                              '4 5 0',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>96</spell>',
                              'Building 457 Site 1',
                              'Aisle 457',
                              'Bay 457',
                              '4 5 7',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>08</spell>',
                              'Building 461 Site 1',
                              'Aisle 461',
                              'Bay 461',
                              '4 6 1',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 471 Site 1',
                              'Aisle 471',
                              'Bay 471',
                              '4 7 1',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'You said 8, expected 18, is this a partial?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>38</spell>',
                              'Building 475 Site 1',
                              'Aisle 475',
                              'Bay 475',
                              '4 7 5',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>41</spell>',
                              'Building 478 Site 1',
                              'Aisle 478',
                              'Bay 478',
                              '4 7 8',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 482 Site 1',
                              'Aisle 482',
                              'Bay 482',
                              '4 8 2',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '5 3 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 485 Site 1',
                              'Aisle 485',
                              'Bay 485',
                              '4 8 5',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '5 6 canceled, say ready',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>68</spell>',
                              'Building 489 Site 1',
                              'Aisle 489',
                              'Bay 489',
                              '4 8 9',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '6 8 canceled, say ready',
                              'Put away license <spell>71</spell>',
                              'Building 492 Site 1',
                              'Aisle 492',
                              'Bay 492',
                              '4 9 2',
                              'quantity',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '000966', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000966', '5', '444', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '000978', '0'],
                                      ['prTaskLUTForkVerifyLicense', '000981', '0'],
                                      ['prTaskLUTForkVerifyLicense', '000996', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001008', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000978', '17', '448', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000981', '20', '451', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000996', '19', '458', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001008', '16', '462', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001026', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001026', '472', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001026', '18', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001038', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001041', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001038', '476', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001038', '14', '1303', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001041', '17', '479', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001053', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001053', '13', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001056', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001056', '16', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001068', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001071', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001068', '12', '', '2', '2', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001071', '15', '493', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
