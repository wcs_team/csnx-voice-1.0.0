##TestLink ID 107851 :: Test Case Tests with License that have Qty and Location specified - part2

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testtestPutAway_107876(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testtestPutAway_107876(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001083,3,497,496,Building 496 Site 1,496,Bay 496,496,496,496,Item 496 Site 1,11,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001083,3,497,496,Building 496 Site 1,496,Bay 496,496,496,496,Item 496 Site 1,11,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001083,3,497,496,Building 496 Site 1,496,Bay 496,496,496,496,Item 496 Site 1,11,5,0,\r\n'
                                 '0000001086,3,500,499,Building 499 Site 1,499,Bay 499,499,499,499,Item 499 Site 1,14,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001083,3,497,496,Building 496 Site 1,496,Bay 496,496,496,496,Item 496 Site 1,11,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001098,3,504,503,Building 503 Site 1,503,Bay 503,503,503,503,Item 503 Site 1,6,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001101,3,507,506,Building 506 Site 1,506,Bay 506,506,506,506,Item 506 Site 1,9,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001113,3,511,510,Building 510 Site 1,510,Bay 510,510,510,510,Item 510 Site 1,5,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001116,3,514,513,Building 513 Site 1,513,Bay 513,513,513,513,Item 513 Site 1,8,10,0,\r\n'
                                 '0000001128,3,518,517,Building 517 Site 1,517,Bay 517,517,517,517,Item 517 Site 1,20,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   '12',               # check digits?
                                   '001083',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 496 Site 1
                                   'ready',            # Aisle 496
                                   'ready',            # Bay 496
                                   '496',              # 4 9 6
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 8 3 released, say ready
                                   '001083',           # License?
                                   'ready',            # License?
                                   'no more',          # License?
                                   'ready',            # Building 496 Site 1
                                   'ready',            # Aisle 496
                                   'ready',            # Bay 496
                                   'release license',  # 4 9 6
                                   'yes',              # Release License, correct?
                                   'ready',            # 8 3 released, say ready
                                   '001083',           # License?
                                   '001086',           # License?
                                   'no more',          # License?
                                   'release license',  # Put away license 8 3
                                   'yes',              # Release License, correct?
                                   'ready',            # 8 3 released, say ready
                                   'ready',            # Put away license 8 6
                                   'ready',            # Building 499 Site 1
                                   'ready',            # Aisle 499
                                   'ready',            # Bay 499
                                   '499',              # 4 9 9
                                   '14!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '001083',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 496 Site 1
                                   'ready',            # Aisle 496
                                   'ready',            # Bay 496
                                   '496',              # 4 9 6
                                   '11!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '001098',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 503 Site 1
                                   'ready',            # Aisle 503
                                   'ready',            # Bay 503
                                   '503',              # 5 0 3
                                   'quantity',         # quantity
                                   '4!',               # quantity
                                   'yes',              # You said 4, expected 6, is this a partial?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '2!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '001101',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 506 Site 1
                                   'ready',            # Aisle 506
                                   'ready',            # Bay 506
                                   'partial',          # 5 0 6
                                   'yes',              # partial, correct?
                                   '506',              # 5 0 6
                                   'quantity',         # quantity
                                   '7!',               # quantity
                                   'yes',              # 7, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '2!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '001113',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 510 Site 1
                                   'ready',            # Aisle 510
                                   'ready',            # Bay 510
                                   'partial',          # 5 1 0
                                   'yes',              # partial, correct?
                                   '510',              # 5 1 0
                                   '2!',               # quantity
                                   'yes',              # 2, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '2!',               # quantity
                                   'yes',              # You said 2, expected 3, is this a partial?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '1!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '001116',           # License?
                                   '001128',           # License?
                                   'no more',          # License?
                                   'ready',            # Put away license 1 6
                                   'ready',            # Building 513 Site 1
                                   'ready',            # Aisle 513
                                   'ready',            # Bay 513
                                   'partial',          # 5 1 3
                                   'yes',              # partial, correct?
                                   '513',              # 5 1 3
                                   'yes',              # quantity
                                   'quantity',         # quantity
                                   '5!',               # quantity
                                   'yes',              # 5, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '3!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 2 8
                                   'ready',            # Building 517 Site 1
                                   'ready',            # Aisle 517
                                   'ready',            # Bay 517
                                   '517',              # 5 1 7
                                   '20!',              # quantity
                                   '-')                # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 496 Site 1',
                              'Aisle 496',
                              'Bay 496',
                              '4 9 6',
                              'quantity',
                              'Release License, correct?',
                              '8 3 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 496 Site 1',
                              'Aisle 496',
                              'Bay 496',
                              '4 9 6',
                              'Release License, correct?',
                              '8 3 released, say ready',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>83</spell>',
                              'Release License, correct?',
                              '8 3 released, say ready',
                              'Put away license <spell>86</spell>',
                              'Building 499 Site 1',
                              'Aisle 499',
                              'Bay 499',
                              '4 9 9',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 496 Site 1',
                              'Aisle 496',
                              'Bay 496',
                              '4 9 6',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 503 Site 1',
                              'Aisle 503',
                              'Bay 503',
                              '5 0 3',
                              'quantity',
                              '6',
                              'quantity',
                              'You said 4, expected 6, is this a partial?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '2',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 506 Site 1',
                              'Aisle 506',
                              'Bay 506',
                              '5 0 6',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '9',
                              'quantity',
                              '7, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '2',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 510 Site 1',
                              'Aisle 510',
                              'Bay 510',
                              '5 1 0',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '2, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '3',
                              'quantity',
                              'You said 2, expected 3, is this a partial?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '1',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>16</spell>',
                              'Building 513 Site 1',
                              'Aisle 513',
                              'Bay 513',
                              '5 1 3',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '8',
                              'quantity',
                              '5, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>28</spell>',
                              'Building 517 Site 1',
                              'Aisle 517',
                              'Bay 517',
                              '5 1 7',
                              'quantity',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '001083', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001083', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001083', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001083', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001083', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001086', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001083', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001086', '14', '500', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001083', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001083', '11', '497', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001098', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001098', '4', '504', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001098', '504', '2'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001098', '2', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001101', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001101', '7', '507', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001101', '507', '2'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001101', '2', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001113', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001113', '2', '511', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001113', '511', '3'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001113', '2', '1303', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001113', '1303', '1'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001113', '1', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001116', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001128', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001116', '5', '514', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001116', '514', '3'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001116', '3', '1303', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001128', '20', '518', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
