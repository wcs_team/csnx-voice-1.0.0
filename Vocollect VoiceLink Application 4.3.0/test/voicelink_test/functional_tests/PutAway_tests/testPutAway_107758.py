##TestLink ID 107758 :: Test Case Tests with Licenses that have location specified but not Qty

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107783(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107783(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,put away region 1,0,\n'
                                 '2,put away region 2,0,\n'
                                 '3,put away region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,put away region 1,0,0,1,0,0,0,0,0,99,4,100,2,1,exception location,0,0,\n'
                                 '2,put away region 2,1,1,1,0,1,0,1,0,99,4,100,2,1,exception location,1,0,\n'
                                 '3,put away region 3r,1,1,1,1,0,1,0,1,2,4,5,2,4,exception location,0,0,\n'
                                 '4,put away region 4,1,1,1,1,1,1,1,1,2,2,100,100,4,exception location,1,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('12345,1,67890,67890656521,Building 1,23,Bay 33,890,00,9998741,Coca-Cola 12-pack,,15.5,0,\n'
                                 '12346,1,67891,67890656522,Building 1,23,Bay 34,891,00,9998742,Diet-Pepsi 12-pack,,30,0,\n'
                                 '12347,1,67892,67890656523,Building 1,23,Bay 35,892,00,9998743,Dr Pepper 18-pack,,27,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1!',               # Function?
                                   'yes',              # put away, correct?
                                   '1',                # Region?
                                   'yes',              # put away region 1, correct?
                                   'no',               # Another Region?
                                   '2345',             # License?
                                   'ready',            # Put away license 1 2 3 4 5
                                   'ready',            # Building 1
                                   'ready',            # Aisle 23
                                   'ready',            # Bay 33
                                   '00!',              # 8 9 0
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 1 2 3 4 6
                                   'ready',            # Building 1
                                   'ready',            # Aisle 23
                                   'ready',            # Bay 34
                                   'cancel',           # 8 9 1
                                   'yes',              # Cancel, correct?
                                   '1',                # Reason?
                                   'yes',              # 1, reason 1, correct?
                                   'ready',            # Deliver to exception location
                                   'ready',            # 1 2 3 4 6 canceled, say ready
                                   'ready',            # Put away license 1 2 3 4 7
                                   'ready',            # Building 1
                                   'ready',            # Aisle 23
                                   'ready',            # Bay 35
                                   'partial',          # 8 9 2
                                   'override',         # 8 9 2
                                   'release license',  # 8 9 2
                                   '00!',              # 8 9 2
                                   '-')                # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'put away, correct?',
                              'Region?',
                              'put away region 1, correct?',
                              'Another Region?',
                              'License?',
                              'getting work',
                              'Put away license <spell>12345</spell>',
                              'Building 1',
                              'Aisle 23',
                              'Bay 33',
                              '8 9 0',
                              'Complete. Say ready.',
                              'Put away license <spell>12346</spell>',
                              'Building 1',
                              'Aisle 23',
                              'Bay 34',
                              '8 9 1',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>1</spell>, reason 1, correct?',
                              'Deliver to exception location',
                              '1 2 3 4 6 canceled, say ready',
                              'Put away license <spell>12347</spell>',
                              'Building 1',
                              'Aisle 23',
                              'Bay 35',
                              '8 9 2',
                              'partial not allowed in this region',
                              '8 9 2',
                              'Override not allowed in this region',
                              '8 9 2',
                              'Release License not allowed in this region',
                              '8 9 2',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '1', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTForkVerifyLicense', '2345', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '12345', '0', '67890', '1', '', '*'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '12346', '0', '', '2', '1', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '12347', '0', '67892', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
