##TestLink ID 107861 :: Test Case Normal, put one license, qty/loc specified, errors on reserving license

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107861(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107861(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1062,License 000241 is not available\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1062,License 000306 is not available\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001233,3,566,565,Building 565 Site 1,565,Bay 565,565,565,565,Item 565 Site 1,7,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('99,License 001233 already selected\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001233,3,566,565,Building 565 Site 1,565,Bay 565,565,565,565,Item 565 Site 1,7,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   '12',               # check digits?
                                   '000241',           # License? ----------- license that was already put away
                                   'ready',            # License 000241 is not available, To continue say ready
                                   '000306',           # License? ----------- license that was cancelled
                                   'ready',            # License 000306 is not available, To continue say ready
                                   '001233',           # License? ---------- test to release a license and then pick it up again
                                   'no more',          # License?
                                   'ready',            # Building 565 Site 1
                                   'ready',            # Aisle 565
                                   'ready',            # Bay 565
                                   'release license',  # 5 6 5
                                   'yes',              # Release License, correct?
                                   'ready',            # 3 3 released, say ready
                                   '001233',           # License?
                                   '001233',           # License? ---------- test to reserve an already selected license
                                   'ready',            # License 001233 already selected, say ready
                                   'no more',          # License?
                                   'ready',            # Building 565 Site 1
                                   'ready',            # Aisle 565
                                   'ready',            # Bay 565
                                   '565',              # 5 6 5
                                   'quantity',         # quantity
                                   '7!',               # quantity
                                   '-')                # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License 000241 is not available, To continue say ready',
                              'License?',
                              'License 000306 is not available, To continue say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 565 Site 1',
                              'Aisle 565',
                              'Bay 565',
                              '5 6 5',
                              'Release License, correct?',
                              '3 3 released, say ready',
                              'License?',
                              'License?',
                              'License 001233 already selected, say ready',
                              'License?',
                              'getting work',
                              'Building 565 Site 1',
                              'Aisle 565',
                              'Bay 565',
                              '5 6 5',
                              'quantity',
                              '7',
                              'quantity',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '000241', '0'],
                                      ['prTaskLUTForkVerifyLicense', '000306', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001233', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001233', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001233', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001233', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001233', '7', '566', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
