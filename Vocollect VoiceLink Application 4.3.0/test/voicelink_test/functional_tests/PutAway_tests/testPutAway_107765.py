##TestLink ID 107765 :: Test Case Normal, quantity/location specified, errors on reserving licenses

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testtestPutAway_107765(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testtestPutAway_107765(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,1,1,0,1,0,1,0,5,5,0,2,1,Exception Location,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1062,License 00032 is not available\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1062,License 00062 is not available\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000243,2,113,112,Building 112 Site 1,112,Bay 112,112,112,112,Item 112 Site 1,13,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000246,2,116,115,Building 115 Site 1,115,Bay 115,115,115,115,Item 115 Site 1,16,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000246,2,116,115,Building 115 Site 1,115,Bay 115,115,115,115,Item 115 Site 1,16,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '2',                # Region?
                                   'yes',              # Region 2, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   'ready',            # Start location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   '00032',            # License?
                                   'yes',              # 00032, correct? --- this license was already Put
                                   'ready',            # License 00032 is not available, To continue say ready
                                   '00062',            # License?
                                   'yes',              # 00062, correct? --- This license was cancelled
                                   'ready',            # License 00062 is not available, To continue say ready
                                   '00243',            # License?
                                   'yes',              # 00243, correct?
                                   '13!',              # quantity
                                   'ready',            # Building 112 Site 1
                                   'ready',            # Aisle 112
                                   'ready',            # Bay 112
                                   '112',              # 1 1 2
                                   'ready',            # Complete. Say ready.
                                   '00246',            # License?
                                   'yes',              # 00246, correct?
                                   '16!',              # quantity
                                   'ready',            # Building 115 Site 1
                                   'ready',            # Aisle 115
                                   'ready',            # Bay 115
                                   'release license',  # 1 1 5
                                   'yes',              # Release License, correct?
                                   'ready',            # 0 0 2 4 6 released, say ready
                                   '00246',            # License?
                                   'yes',              # 00246, correct?
                                   '16!',              # quantity
                                   'ready',            # Building 115 Site 1
                                   'ready',            # Aisle 115
                                   'ready',            # Bay 115
                                   '115',              # 1 1 5
                                   '-')                # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'Another Region?',
                              'Start location?',
                              '12345, correct?',
                              'check digits?',
                              'License?',
                              '00032, correct?',
                              'License 00032 is not available, To continue say ready',
                              'License?',
                              '00062, correct?',
                              'License 00062 is not available, To continue say ready',
                              'License?',
                              '00243, correct?',
                              'getting work',
                              'quantity',
                              'Building 112 Site 1',
                              'Aisle 112',
                              'Bay 112',
                              '1 1 2',
                              'Complete. Say ready.',
                              'License?',
                              '00246, correct?',
                              'getting work',
                              'quantity',
                              'Building 115 Site 1',
                              'Aisle 115',
                              'Bay 115',
                              '1 1 5',
                              'Release License, correct?',
                              '0 0 2 4 6 released, say ready',
                              'License?',
                              '00246, correct?',
                              'getting work',
                              'quantity',
                              'Building 115 Site 1',
                              'Aisle 115',
                              'Bay 115',
                              '1 1 5',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '2', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '00032', '0'],
                                      ['prTaskLUTForkVerifyLicense', '00062', '0'],
                                      ['prTaskLUTForkVerifyLicense', '00243', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000243', '13', '113', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00246', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000246', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00246', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000246', '16', '116', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
