##TestLink ID 107769 :: Test Case Tests with Licenses that have qty specified but not location

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107783(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107783(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',1001,Invalid password for Operator.Id\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Region 2,1,1,1,0,1,0,1,0,5,5,0,2,1,Exception Location,1,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050366,2,,,,,,,,168,Item 168 Site 1,5,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050367,2,,,,,,,,169,Item 169 Site 1,6,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050376,2,,,,,,,,170,Item 170 Site 1,15,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050377,2,,,,,,,,171,Item 171 Site 1,16,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050377,2,,,,,,,,171,Item 171 Site 1,16,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050377,2,,,,,,,,171,Item 171 Site 1,16,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',1034,Location identifier 12345 not found.\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050378,2,,,,,,,,172,Item 172 Site 1,17,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050392,2,,,,,,,,178,Item 178 Site 1,15,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050393,2,,,,,,,,179,Item 179 Site 1,16,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050394,2,,,,,,,,180,Item 180 Site 1,17,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000050395,2,,,,,,,,181,Item 181 Site 1,18,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '1234!',            # Password?
                                   'ready',            # Invalid password for Operator.Id, To continue say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '2',                # Region?
                                   'yes',              # Region 2, correct?
                                   'no',               # Another Region?
                                   '12345ready',       # Start location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   '50366',            # License?
                                   'yes',              # 50366, correct?
                                   '5!',               # quantity
                                   '12345ready',       # Location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   'ready',            # Complete. Say ready.
                                   '50367',            # License?
                                   'yes',              # 50367, correct?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 5 0 3 6 7 canceled, say ready
                                   '50376',            # License?
                                   'yes',              # 50376, correct?
                                   'quantity',         # quantity
                                   '15!',              # quantity
                                   'cancel',           # Location?
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 5 0 3 7 6 canceled, say ready
                                   '50377',            # License?
                                   'yes',              # 50377, correct?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 5 0 3 7 7 released, say ready
                                   '50377',            # License?
                                   'yes',              # 50377, correct?
                                   '17!',              # quantity
                                   'no',               # You said 17, expected 16, correct?
                                   '16!',              # quantity
                                   'release license',  # Location?
                                   'yes',              # Release License, correct?
                                   'ready',            # 5 0 3 7 7 released, say ready
                                   '50377',            # License?
                                   'yes',              # 50377, correct?
                                   '16!',              # quantity
                                   'partial',          # Location?
                                   '12345ready',       # Location?
                                   'yes',              # 12345, correct?
                                   '99',               # check digits?
                                   'ready',            # Location identifier 12345 not found., To continue say ready
                                   '12345ready',       # Location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   'ready',            # Complete. Say ready.
                                   '50378',            # License?
                                   'yes',              # 50378, correct?
                                   '12!',              # quantity
                                   'yes',              # You said 12, expected 17, correct?
                                   '12345ready',       # Location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   'ready',            # Complete. Say ready.
                                   '50392',            # License?
                                   'yes',              # 50392, correct?
                                   '12!',              # quantity
                                   'no',               # You said 12, expected 15, correct?
                                   '19!',              # quantity
                                   'yes',              # You said 19, expected 15, correct?
                                   '12345ready',       # Location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   'ready',            # Complete. Say ready.
                                   '50393',            # License?
                                   'yes',              # 50393, correct?
                                   '19!',              # quantity
                                   'no',               # You said 19, expected 16, correct?
                                   '16!',              # quantity
                                   '12345ready',       # Location?
                                   'yes',              # 12345, correct?
                                   '12',               # check digits?
                                   'ready',            # Complete. Say ready.
                                   '50394',            # License?
                                   'yes',              # 50394, correct?
                                   '11!',              # quantity
                                   'yes',              # You said 11, expected 17, correct?
                                   'cancel',           # Location?
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 5 0 3 9 4 canceled, say ready
                                   '50395',            # License?
                                   'yes',              # 50395, correct?
                                   '17!',              # quantity
                                   'yes',              # You said 17, expected 18, correct?
                                   'release license',  # Location?
                                   'yes',              # Release License, correct?
                                   '-')                # 5 0 3 9 5 released, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Invalid password for Operator.Id, To continue say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 2, correct?',
                              'Another Region?',
                              'Start location?',
                              '12345, correct?',
                              'check digits?',
                              'License?',
                              '50366, correct?',
                              'getting work',
                              'quantity',
                              'Location?',
                              '12345, correct?',
                              'check digits?',
                              'Complete. Say ready.',
                              'License?',
                              '50367, correct?',
                              'getting work',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '5 0 3 6 7 canceled, say ready',
                              'License?',
                              '50376, correct?',
                              'getting work',
                              'quantity',
                              '15',
                              'quantity',
                              'Location?',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '5 0 3 7 6 canceled, say ready',
                              'License?',
                              '50377, correct?',
                              'getting work',
                              'quantity',
                              'Release License, correct?',
                              '5 0 3 7 7 released, say ready',
                              'License?',
                              '50377, correct?',
                              'getting work',
                              'quantity',
                              'You said 17, expected 16, correct?',
                              'quantity',
                              'Location?',
                              'Release License, correct?',
                              '5 0 3 7 7 released, say ready',
                              'License?',
                              '50377, correct?',
                              'getting work',
                              'quantity',
                              'Location?',
                              'partial not allowed in this region',
                              'Location?',
                              '12345, correct?',
                              'check digits?',
                              'Location identifier 12345 not found., To continue say ready',
                              'Location?',
                              '12345, correct?',
                              'check digits?',
                              'Complete. Say ready.',
                              'License?',
                              '50378, correct?',
                              'getting work',
                              'quantity',
                              'You said 12, expected 17, correct?',
                              'Location?',
                              '12345, correct?',
                              'check digits?',
                              'Complete. Say ready.',
                              'License?',
                              '50392, correct?',
                              'getting work',
                              'quantity',
                              'You said 12, expected 15, correct?',
                              'quantity',
                              'You said 19, expected 15, correct?',
                              'Location?',
                              '12345, correct?',
                              'check digits?',
                              'Complete. Say ready.',
                              'License?',
                              '50393, correct?',
                              'getting work',
                              'quantity',
                              'You said 19, expected 16, correct?',
                              'quantity',
                              'Location?',
                              '12345, correct?',
                              'check digits?',
                              'Complete. Say ready.',
                              'License?',
                              '50394, correct?',
                              'getting work',
                              'quantity',
                              'You said 11, expected 17, correct?',
                              'Location?',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '5 0 3 9 4 canceled, say ready',
                              'License?',
                              '50395, correct?',
                              'getting work',
                              'quantity',
                              'You said 17, expected 18, correct?',
                              'Location?',
                              'Release License, correct?',
                              '5 0 3 9 5 released, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '1234'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '2', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '50366', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050366', '5', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50367', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050367', '6', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50376', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050376', '15', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50377', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050377', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50377', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050377', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50377', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '99', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050377', '16', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50378', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050378', '12', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50392', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050392', '19', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50393', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050393', '16', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50394', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050394', '11', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '50395', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000050395', '0', '', '3', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
