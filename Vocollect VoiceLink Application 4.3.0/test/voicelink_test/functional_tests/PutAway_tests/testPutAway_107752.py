##TestLink ID 107752 :: Test Case Tests with licenses that have quantity and location specified
##The following tests are covered
##Normal, quantity/location specified
##Cancel, quantity/location specified, cancel at put location
##Normal, quantity/location specified, attempt commands that aren't allowed

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testSample(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testSample(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,0,1,0,0,0,0,0,5,5,0,2,1,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000091,1,42,41,Building 41 Site 1,41,Bay 41,41,41,41,Item 41 Site 1,15,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000094,1,45,44,Building 44 Site 1,44,Bay 44,44,44,44,Item 44 Site 1,18,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000000097,1,48,47,Building 47 Site 1,47,Bay 47,47,47,47,Item 47 Site 1,5,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '1',                # Region?
                                   'yes',              # Region 1, correct?
                                   'no',               # Another Region?
                                   '00091',            # License?
                                   'ready',            # Building 41 Site 1
                                   'ready',            # Aisle 41
                                   'rready',           # Bay 41
                                   'ready',            # Bay 41
                                   '41',               # 4 1
                                   'ready',            # Complete. Say ready.
                                   '00094',            # License?
                                   'ready',            # Building 44 Site 1
                                   'ready',            # Aisle 44
                                   'ready',            # Bay 44
                                   'cancel',           # 4 4
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 0 0 0 9 4 canceled, say ready
                                   '00097',            # License?
                                   'ready',            # Building 47 Site 1
                                   'ready',            # Aisle 47
                                   'ready',            # Bay 47
                                   'partial',          # 4 7
                                   'override',         # 4 7
                                   'release license',  # 4 7
                                   '47',               # 4 7
                                   '-')                # Complete. Say ready.

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 1, correct?',
                              'Another Region?',
                              'License?',
                              'getting work',
                              'Building 41 Site 1',
                              'Aisle 41',
                              'Bay 41',
                              '4 1',
                              'Complete. Say ready.',
                              'License?',
                              'getting work',
                              'Building 44 Site 1',
                              'Aisle 44',
                              'Bay 44',
                              '4 4',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '0 0 0 9 4 canceled, say ready',
                              'License?',
                              'getting work',
                              'Building 47 Site 1',
                              'Aisle 47',
                              'Bay 47',
                              '4 7',
                              'partial not allowed in this region',
                              '4 7',
                              'Override not allowed in this region',
                              '4 7',
                              'Release License not allowed in this region',
                              '4 7',
                              'Complete. Say ready.')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '1', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTForkVerifyLicense', '00091', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000091', '15', '42', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00094', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000094', '18', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '00097', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000000097', '5', '48', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
