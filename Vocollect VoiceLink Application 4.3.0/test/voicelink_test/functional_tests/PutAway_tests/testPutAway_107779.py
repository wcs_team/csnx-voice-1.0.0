##TestLink ID 107779 :: Test Case Test with Licenses that have Qty specified but not Location - part1

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107779(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107779(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020303,3,,,,,,,,139,Item 139 Site 1,10,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020304,3,,,,,,,,140,Item 140 Site 1,11,10,0,\r\n'
                                 '0000020305,3,,,,,,,,141,Item 141 Site 1,12,5,0,\r\n'
                                 '0000020306,3,,,,,,,,142,Item 142 Site 1,13,10,0,\r\n'
                                 '0000020307,3,,,,,,,,143,Item 143 Site 1,14,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020320,3,,,,,,,,148,Item 148 Site 1,11,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020321,3,,,,,,,,149,Item 149 Site 1,12,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020331,3,,,,,,,,151,Item 151 Site 1,6,5,0,\r\n'
                                 '0000020332,3,,,,,,,,152,Item 152 Site 1,7,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020333,3,,,,,,,,153,Item 153 Site 1,8,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020333,3,,,,,,,,153,Item 153 Site 1,8,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020333,3,,,,,,,,153,Item 153 Site 1,8,5,0,\r\n'
                                 '0000020334,3,,,,,,,,154,Item 154 Site 1,9,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020333,3,,,,,,,,153,Item 153 Site 1,8,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020335,3,,,,,,,,155,Item 155 Site 1,10,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020336,3,,,,,,,,156,Item 156 Site 1,11,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020346,3,,,,,,,,157,Item 157 Site 1,5,10,0,\r\n'
                                 '0000020347,3,,,,,,,,158,Item 158 Site 1,6,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020348,3,,,,,,,,159,Item 159 Site 1,7,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020349,3,,,,,,,,160,Item 160 Site 1,8,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020350,3,,,,,,,,161,Item 161 Site 1,5,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   '12',               # check digits?
                                   '020303',           # License?
                                   'no more',          # License?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '10!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '020304',           # License?
                                   '020305',           # License?
                                   '020306',           # License?
                                   '020307',           # License?
                                   'ready',            # Put away license 0 4
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '11!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 0 5
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '12!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 0 6
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '13!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 0 7
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '14!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '020320',           # License?
                                   'no more',          # License?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 2 0 canceled, say ready
                                   '020321',           # License?
                                   'no more',          # License?
                                   'cancel',           # Location?
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 2 1 canceled, say ready
                                   '020331',           # License?
                                   '020332',           # License?
                                   'no more',          # License?
                                   'ready',            # Put away license 3 1
                                   'cancel',           # Location?
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 3 1 canceled, say ready
                                   'ready',            # Put away license 3 2
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '7!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '020333',           # License?
                                   'no more',          # License?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 3 3 released, say ready
                                   '020333',           # License?
                                   'no more',          # License?
                                   'release license',  # Location?
                                   'yes',              # Release License, correct?
                                   'ready',            # 3 3 released, say ready
                                   '020333',           # License?
                                   '020334',           # License?
                                   'no more',          # License?
                                   'release license',  # Put away license 3 3
                                   'yes',              # Release License, correct?
                                   'ready',            # 3 3 released, say ready
                                   'ready',            # Put away license 3 4
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '9!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '020333',           # License?
                                   'no more',          # License?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '5!',               # quantity
                                   'yes',              # You said 5, expected 8, is this a partial?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '3!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '020335',           # License?
                                   'no more',          # License?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '8!',               # quantity
                                   'yes',              # 8, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '2!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '020336',           # License?
                                   'no more',          # License?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '5!',               # quantity
                                   'yes',              # 5, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '4!',               # quantity
                                   'yes',              # You said 4, expected 6, is this a partial?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '2!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '020346',           # License?
                                   '020347',           # License?
                                   'no more',          # License?
                                   'ready',            # Put away license 4 6
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '3!',               # quantity
                                   'yes',              # 3, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '2!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 4 7
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '6!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '020348',           # License?
                                   'no more',          # License?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '7!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '020349',           # License?
                                   'no more',          # License?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 4 9 canceled, say ready
                                   '020350',           # License?
                                   'no more',          # License?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 5 0 released, say ready
                                   '-')                # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '10',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>04</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>05</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>06</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>07</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '2 0 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '2 1 canceled, say ready',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>31</spell>',
                              'Location?',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '3 1 canceled, say ready',
                              'Put away license <spell>32</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Release License, correct?',
                              '3 3 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'Release License, correct?',
                              '3 3 released, say ready',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>33</spell>',
                              'Release License, correct?',
                              '3 3 released, say ready',
                              'Put away license <spell>34</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'You said 5, expected 8, is this a partial?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '3',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '10',
                              'quantity',
                              '8, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '2',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '11',
                              'quantity',
                              '5, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '6',
                              'quantity',
                              'You said 4, expected 6, is this a partial?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '2',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>46</spell>',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '5',
                              'quantity',
                              '3, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '2',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>47</spell>',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '6',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '7',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '4 9 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Release License, correct?',
                              '5 0 released, say ready',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '020303', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020303', '10', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020304', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020305', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020306', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020307', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020304', '11', '1303', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020305', '12', '1303', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020306', '13', '1303', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020307', '14', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020320', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020320', '11', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020321', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020321', '12', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020331', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020332', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020331', '6', '', '2', '2', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020332', '7', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020333', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020333', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020333', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020333', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020333', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020334', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020333', '0', '', '3', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020334', '9', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020333', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020333', '5', '1303', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020333', '1303', '3'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020333', '3', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020335', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020335', '8', '1303', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020335', '1303', '2'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020335', '2', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020336', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020336', '5', '1303', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020336', '1303', '6'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020336', '4', '1303', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020336', '1303', '2'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020336', '2', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020346', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020347', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020346', '3', '1303', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020346', '1303', '2'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020346', '2', '1303', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020347', '6', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020348', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020348', '7', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020349', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020349', '8', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020350', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020350', '0', '', '3', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
