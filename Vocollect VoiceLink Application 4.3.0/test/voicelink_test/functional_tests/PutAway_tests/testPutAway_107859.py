##TestLink ID 107859 :: Test Case Tests with License that have Qty and Location specified - part3

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testtestPutAway_107859(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testtestPutAway_107859(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001146,3,528,527,Building 527 Site 1,527,Bay 527,527,527,527,Item 527 Site 1,6,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001176,3,541,540,Building 540 Site 1,540,Bay 540,540,540,540,Item 540 Site 1,20,10,0,\r\n'
                                 '0000001188,3,545,544,Building 544 Site 1,544,Bay 544,544,544,544,Item 544 Site 1,16,10,0,\r\n'
                                 '0000001191,3,548,547,Building 547 Site 1,547,Bay 547,547,547,547,Item 547 Site 1,19,5,0,\r\n'
                                 '0000001203,3,552,551,Building 551 Site 1,551,Bay 551,551,551,551,Item 551 Site 1,9,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001188,3,545,544,Building 544 Site 1,544,Bay 544,544,544,544,Item 544 Site 1,16,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001206,3,555,554,Building 554 Site 1,554,Bay 554,554,554,554,Item 554 Site 1,12,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001218,3,559,558,Building 558 Site 1,558,Bay 558,558,558,558,Item 558 Site 1,8,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001218,3,559,558,Building 558 Site 1,558,Bay 558,558,558,558,Item 558 Site 1,8,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001218,3,559,558,Building 558 Site 1,558,Bay 558,558,558,558,Item 558 Site 1,8,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001221,3,562,561,Building 561 Site 1,561,Bay 561,561,561,561,Item 561 Site 1,11,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000001221,3,562,561,Building 561 Site 1,561,Bay 561,561,561,561,Item 561 Site 1,11,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1061,License 999999 not found\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   '12',               # check digits?
                                   '001146',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 527 Site 1
                                   'ready',            # Aisle 527
                                   'ready',            # Bay 527
                                   'partial',          # 5 2 7
                                   'yes',              # partial, correct?
                                   '527',              # 5 2 7
                                   'quantity',         # quantity
                                   '6!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '001176',           # License?
                                   '001188',           # License?
                                   '001191',           # License?
                                   '001203',           # License?
                                   'ready',            # Put away license 7 6
                                   'ready',            # Building 540 Site 1
                                   'ready',            # Aisle 540
                                   'ready',            # Bay 540
                                   'cancel',           # 5 4 0
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 7 6 canceled, say ready
                                   'release license',  # Put away license 8 8
                                   'yes',              # Release License, correct?
                                   'ready',            # 8 8 released, say ready
                                   'ready',            # Put away license 9 1
                                   'ready',            # Building 547 Site 1
                                   'ready',            # Aisle 547
                                   'ready',            # Bay 547
                                   'partial',          # 5 4 7
                                   'yes',              # partial, correct?
                                   '547',              # 5 4 7
                                   'quantity',         # quantity
                                   '10!',              # quantity
                                   'yes',              # 10, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '9!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 0 3
                                   'ready',            # Building 551 Site 1
                                   'ready',            # Aisle 551
                                   'ready',            # Bay 551
                                   'override',         # 5 5 1
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '9!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '001188',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 544 Site 1
                                   'ready',            # Aisle 544
                                   'ready',            # Bay 544
                                   'partial',          # 5 4 4
                                   'yes',              # partial, correct?
                                   '544',              # 5 4 4
                                   'quantity',         # quantity
                                   '10!',              # quantity
                                   'yes',              # 10, correct?
                                   'cancel',           # Location?
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 8 8 canceled, say ready
                                   '001206',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 554 Site 1
                                   'ready',            # Aisle 554
                                   'ready',            # Bay 554
                                   'partial',          # 5 5 4
                                   'yes',              # partial, correct?
                                   '554',              # 5 5 4
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 0 6 canceled, say ready
                                   '001218',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 558 Site 1
                                   'ready',            # Aisle 558
                                   'ready',            # Bay 558
                                   'partial',          # 5 5 8
                                   'yes',              # partial, correct?
                                   '558',              # 5 5 8
                                   'quantity',         # quantity
                                   '6!',               # quantity
                                   'yes',              # 6, correct?
                                   'release license',  # Location?
                                   'yes',              # Release License, correct?
                                   'ready',            # 1 8 released, say ready
                                   '001218',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 558 Site 1
                                   'ready',            # Aisle 558
                                   'ready',            # Bay 558
                                   'partial',          # 5 5 8
                                   'yes',              # partial, correct?
                                   '558',              # 5 5 8
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 1 8 released, say ready
                                   '001218',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 558 Site 1
                                   'ready',            # Aisle 558
                                   'ready',            # Bay 558
                                   'override',         # 5 5 8
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 1 8 canceled, say ready
                                   '001221',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 561 Site 1
                                   'ready',            # Aisle 561
                                   'ready',            # Bay 561
                                   'override',         # 5 6 1
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 2 1 released, say ready
                                   '001221',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 561 Site 1
                                   'ready',            # Aisle 561
                                   'ready',            # Bay 561
                                   'override',         # 5 6 1
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '5!',               # quantity
                                   'yes',              # You said 5, expected 11, is this a partial?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   'quantity',         # quantity
                                   '6!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '999999',           # License?
                                   'ready',            # License 999999 not found, To continue say ready
                                   '-')                # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 527 Site 1',
                              'Aisle 527',
                              'Bay 527',
                              '5 2 7',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '6',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>76</spell>',
                              'Building 540 Site 1',
                              'Aisle 540',
                              'Bay 540',
                              '5 4 0',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '7 6 canceled, say ready',
                              'Put away license <spell>88</spell>',
                              'Release License, correct?',
                              '8 8 released, say ready',
                              'Put away license <spell>91</spell>',
                              'Building 547 Site 1',
                              'Aisle 547',
                              'Bay 547',
                              '5 4 7',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '19',
                              'quantity',
                              '10, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '9',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>03</spell>',
                              'Building 551 Site 1',
                              'Aisle 551',
                              'Bay 551',
                              '5 5 1',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '9',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 544 Site 1',
                              'Aisle 544',
                              'Bay 544',
                              '5 4 4',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '16',
                              'quantity',
                              '10, correct?',
                              'Location?',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '8 8 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 554 Site 1',
                              'Aisle 554',
                              'Bay 554',
                              '5 5 4',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '0 6 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 558 Site 1',
                              'Aisle 558',
                              'Bay 558',
                              '5 5 8',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '8',
                              'quantity',
                              '6, correct?',
                              'Location?',
                              'Release License, correct?',
                              '1 8 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 558 Site 1',
                              'Aisle 558',
                              'Bay 558',
                              '5 5 8',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              'Release License, correct?',
                              '1 8 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 558 Site 1',
                              'Aisle 558',
                              'Bay 558',
                              '5 5 8',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '1 8 canceled, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 561 Site 1',
                              'Aisle 561',
                              'Bay 561',
                              '5 6 1',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              'Release License, correct?',
                              '2 1 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 561 Site 1',
                              'Aisle 561',
                              'Bay 561',
                              '5 6 1',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '11',
                              'quantity',
                              'You said 5, expected 11, is this a partial?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '6',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              'License 999999 not found, To continue say ready',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '001146', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001146', '6', '528', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001176', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001188', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001191', '0'],
                                      ['prTaskLUTForkVerifyLicense', '001203', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001176', '20', '', '2', '2', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001188', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001191', '10', '548', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001191', '548', '9'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001191', '9', '1303', '1', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001203', '552', '9'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001203', '9', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001188', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001188', '10', '545', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001188', '545', '6'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001188', '6', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001206', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001206', '12', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001218', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001218', '6', '559', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001218', '559', '2'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001218', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001218', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001218', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001218', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001218', '559', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001218', '8', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001221', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001221', '562', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001221', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '001221', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001221', '562', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001221', '5', '1303', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000001221', '1303', '6'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000001221', '6', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '999999', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
