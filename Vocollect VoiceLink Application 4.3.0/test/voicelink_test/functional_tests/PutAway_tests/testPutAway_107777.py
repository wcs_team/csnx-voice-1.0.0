##TestLink ID 107777 :: Test Case Test with licenses that have Location specified but not Qty - part1

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_107777(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testPutAway_107777(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Lunch,0,\r\n'
                                 '2,Battery Change,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Put Away,0,\r\n'
                                 '2,Replenishment,0,\r\n'
                                 '3,Normal Assignments,0,\r\n'
                                 '4,Chase Assignments,0,\r\n'
                                 '6,Normal And Chase Assignments,0,\r\n'
                                 '7,Line Loading,0,\r\n'
                                 '8,Put To Store,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1,Region 1,0,\r\n'
                                 '2,Region 2,0,\r\n'
                                 '3,Region 3,0,\r\n'
                                 '4,Region 4,0,\r\n'
                                 '5,Region 5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('3,Region 3,1,1,1,1,0,1,0,1,2,6,5,2,4,Exception Location,0,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020106,3,49,48,Building 48 Site 1,48,Bay 48,48,48,48,Item 48 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020107,3,50,49,Building 49 Site 1,49,Bay 49,49,49,49,Item 49 Site 1,0,5,0,\r\n'
                                 '0000020108,3,51,50,Building 50 Site 1,50,Bay 50,50,50,50,Item 50 Site 1,0,10,0,\r\n'
                                 '0000020109,3,52,51,Building 51 Site 1,51,Bay 51,51,51,51,Item 51 Site 1,0,5,0,\r\n'
                                 '0000020110,3,53,52,Building 52 Site 1,52,Bay 52,52,52,52,Item 52 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020111,3,54,53,Building 53 Site 1,53,Bay 53,53,53,53,Item 53 Site 1,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020112,3,55,54,Building 54 Site 1,54,Bay 54,54,54,54,Item 54 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020121,3,56,55,Building 55 Site 1,55,Bay 55,55,55,55,Item 55 Site 1,0,5,0,\r\n'
                                 '0000020122,3,57,56,Building 56 Site 1,56,Bay 56,56,56,56,Item 56 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020123,3,58,57,Building 57 Site 1,57,Bay 57,57,57,57,Item 57 Site 1,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020123,3,58,57,Building 57 Site 1,57,Bay 57,57,57,57,Item 57 Site 1,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020124,3,59,58,Building 58 Site 1,58,Bay 58,58,58,58,Item 58 Site 1,0,10,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0000020126,3,61,60,Building 60 Site 1,60,Bay 60,60,60,60,Item 60 Site 1,0,10,0,\r\n'
                                 '0000020127,3,62,61,Building 61 Site 1,61,Bay 61,61,61,61,Item 61 Site 1,0,5,0,\r\n'
                                 '0000020136,3,63,62,Building 62 Site 1,62,Bay 62,62,62,62,Item 62 Site 1,0,10,0,\r\n'
                                 '0000020137,3,64,63,Building 63 Site 1,63,Bay 63,63,63,63,Item 63 Site 1,0,5,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('2,Location Extra Full,0,\r\n'
                                 '25,Got Tired,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response(',,,,,,,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('1303,0,\r\n'
                                 '\n'
                                 '\n')
        self.set_server_response('0,\r\n'
                                 '\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1',                # Function?
                                   'yes',              # Put Away, correct?
                                   '3',                # Region?
                                   'yes',              # Region 3, correct?
                                   'no',               # Another Region?
                                   '12345',            # Start location?
                                   '12',               # check digits?
                                   '020106',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 48 Site 1
                                   'ready',            # Aisle 48
                                   'ready',            # Bay 48
                                   '48',               # 4 8
                                   'quantity',         # quantity
                                   '4!',               # quantity
                                   'yes',              # 4, correct?
                                   'ready',            # Complete. Say ready.
                                   '020107',           # License?
                                   '020108',           # License?
                                   '020109',           # License?
                                   '020110',           # License?
                                   'ready',            # Put away license 0 7
                                   'ready',            # Building 49 Site 1
                                   'ready',            # Aisle 49
                                   'ready',            # Bay 49
                                   '49',               # 4 9
                                   '7!',               # quantity
                                   'yes',              # 7, correct?
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 0 8
                                   'ready',            # Building 50 Site 1
                                   'ready',            # Aisle 50
                                   'ready',            # Bay 50
                                   '50',               # 5 0
                                   '5!',               # quantity
                                   'yes',              # 5, correct?
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 0 9
                                   'ready',            # Building 51 Site 1
                                   'ready',            # Aisle 51
                                   'ready',            # Bay 51
                                   '51',               # 5 1
                                   '9!',               # quantity
                                   'yes',              # 9, correct?
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 1 0
                                   'ready',            # Building 52 Site 1
                                   'ready',            # Aisle 52
                                   'ready',            # Bay 52
                                   '52',               # 5 2
                                   '23!',              # quantity
                                   'yes',              # 23, correct?
                                   'ready',            # Complete. Say ready.
                                   '020111',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 53 Site 1
                                   'ready',            # Aisle 53
                                   'ready',            # Bay 53
                                   'override',         # 5 3
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '1!',               # quantity
                                   'yes',              # 1, correct?
                                   'ready',            # Complete. Say ready.
                                   '020112',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 54 Site 1
                                   'ready',            # Aisle 54
                                   'ready',            # Bay 54
                                   '54',               # 5 4
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 1 2 canceled, say ready
                                   '020121',           # License?
                                   '020122',           # License?
                                   'no more',          # License?
                                   'ready',            # Put away license 2 1
                                   'ready',            # Building 55 Site 1
                                   'ready',            # Aisle 55
                                   'ready',            # Bay 55
                                   'cancel',           # 5 5
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 2 1 canceled, say ready
                                   'ready',            # Put away license 2 2
                                   'ready',            # Building 56 Site 1
                                   'ready',            # Aisle 56
                                   'ready',            # Bay 56
                                   '56',               # 5 6
                                   '17!',              # quantity
                                   'yes',              # 17, correct?
                                   'ready',            # Complete. Say ready.
                                   '020123',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 57 Site 1
                                   'ready',            # Aisle 57
                                   'ready',            # Bay 57
                                   '57',               # 5 7
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   'ready',            # 2 3 released, say ready
                                   '020123',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 57 Site 1
                                   'ready',            # Aisle 57
                                   'ready',            # Bay 57
                                   'partial',          # 5 7
                                   'yes',              # partial, correct?
                                   '57',               # 5 7
                                   '7!',               # quantity
                                   'yes',              # 7, correct?
                                   'ready',            # Complete. Say ready.
                                   '020124',           # License?
                                   'no more',          # License?
                                   'ready',            # Building 58 Site 1
                                   'ready',            # Aisle 58
                                   'ready',            # Bay 58
                                   'partial',          # 5 8
                                   'yes',              # partial, correct?
                                   '58',               # 5 8
                                   '8!',               # quantity
                                   'yes',              # 8, correct?
                                   'ready',            # Complete. Say ready.
                                   '020126',           # License?
                                   '020127',           # License?
                                   '020136',           # License?
                                   '020137',           # License?
                                   'ready',            # Put away license 2 6
                                   'ready',            # Building 60 Site 1
                                   'ready',            # Aisle 60
                                   'ready',            # Bay 60
                                   'cancel',           # 6 0
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 2 6 canceled, say ready
                                   'release license',  # Put away license 2 7
                                   'yes',              # Release License, correct?
                                   'ready',            # 2 7 released, say ready
                                   'ready',            # Put away license 3 6
                                   'ready',            # Building 62 Site 1
                                   'ready',            # Aisle 62
                                   'ready',            # Bay 62
                                   'partial',          # 6 2
                                   'yes',              # partial, correct?
                                   '62',               # 6 2
                                   '6!',               # quantity
                                   'yes',              # 6, correct?
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 3 7
                                   'ready',            # Building 63 Site 1
                                   'ready',            # Aisle 63
                                   'ready',            # Bay 63
                                   'override',         # 6 3
                                   'yes',              # Override, correct?
                                   '12345',            # Location?
                                   '12',               # check digits?
                                   '8!',               # quantity
                                   'yes',              # 8, correct?
                                   'ready',            # Complete. Say ready.
                                   '-')                # License?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'Put Away, correct?',
                              'Region?',
                              'Region 3, correct?',
                              'Another Region?',
                              'Start location?',
                              'check digits?',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 48 Site 1',
                              'Aisle 48',
                              'Bay 48',
                              '4 8',
                              'quantity',
                              'quantity not available',
                              'quantity',
                              '4, correct?',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>07</spell>',
                              'Building 49 Site 1',
                              'Aisle 49',
                              'Bay 49',
                              '4 9',
                              'quantity',
                              '7, correct?',
                              'Complete. Say ready.',
                              'Put away license <spell>08</spell>',
                              'Building 50 Site 1',
                              'Aisle 50',
                              'Bay 50',
                              '5 0',
                              'quantity',
                              '5, correct?',
                              'Complete. Say ready.',
                              'Put away license <spell>09</spell>',
                              'Building 51 Site 1',
                              'Aisle 51',
                              'Bay 51',
                              '5 1',
                              'quantity',
                              '9, correct?',
                              'Complete. Say ready.',
                              'Put away license <spell>10</spell>',
                              'Building 52 Site 1',
                              'Aisle 52',
                              'Bay 52',
                              '5 2',
                              'quantity',
                              '23, correct?',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 53 Site 1',
                              'Aisle 53',
                              'Bay 53',
                              '5 3',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '1, correct?',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 54 Site 1',
                              'Aisle 54',
                              'Bay 54',
                              '5 4',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '1 2 canceled, say ready',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>21</spell>',
                              'Building 55 Site 1',
                              'Aisle 55',
                              'Bay 55',
                              '5 5',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '2 1 canceled, say ready',
                              'Put away license <spell>22</spell>',
                              'Building 56 Site 1',
                              'Aisle 56',
                              'Bay 56',
                              '5 6',
                              'quantity',
                              '17, correct?',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 57 Site 1',
                              'Aisle 57',
                              'Bay 57',
                              '5 7',
                              'quantity',
                              'Release License, correct?',
                              '2 3 released, say ready',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 57 Site 1',
                              'Aisle 57',
                              'Bay 57',
                              '5 7',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '7, correct?',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'getting work',
                              'Building 58 Site 1',
                              'Aisle 58',
                              'Bay 58',
                              '5 8',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '8, correct?',
                              'Complete. Say ready.',
                              'License?',
                              'License?',
                              'License?',
                              'License?',
                              'getting work',
                              'Put away license <spell>26</spell>',
                              'Building 60 Site 1',
                              'Aisle 60',
                              'Bay 60',
                              '6 0',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '2 6 canceled, say ready',
                              'Put away license <spell>27</spell>',
                              'Release License, correct?',
                              '2 7 released, say ready',
                              'Put away license <spell>36</spell>',
                              'Building 62 Site 1',
                              'Aisle 62',
                              'Bay 62',
                              '6 2',
                              'partial, correct?',
                              'confirm location',
                              'quantity',
                              '6, correct?',
                              'Complete. Say ready.',
                              'Put away license <spell>37</spell>',
                              'Building 63 Site 1',
                              'Aisle 63',
                              'Bay 63',
                              '6 3',
                              'Override, correct?',
                              'Location?',
                              'check digits?',
                              'quantity',
                              '8, correct?',
                              'Complete. Say ready.',
                              'License?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '3', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '1'],
                                      ['prTaskLUTForkVerifyLicense', '020106', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020106', '4', '49', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020107', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020108', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020109', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020110', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020107', '7', '50', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020108', '5', '51', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020109', '9', '52', '1', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020110', '23', '53', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020111', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020111', '54', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020111', '1', '1303', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020112', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020112', '0', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020121', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020122', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020121', '0', '', '2', '2', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020122', '17', '57', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020123', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020123', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020123', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020123', '7', '58', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020124', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020124', '8', '59', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '020126', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020127', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020136', '0'],
                                      ['prTaskLUTForkVerifyLicense', '020137', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020126', '0', '', '2', '2', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020127', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020136', '6', '63', '1', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '0000020137', '64', '0'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12345', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '0000020137', '8', '1303', '1', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
