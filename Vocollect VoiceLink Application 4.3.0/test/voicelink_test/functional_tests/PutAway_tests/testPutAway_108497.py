# Verify partial with release and cancel commands works correctly in PutAway Region 104
# Rename the file testGenerator_PutAway_108497.txt to testGenerator_PutAway_108497.py and copy the file to the
# root level of the VoiceLink project and run to regenerate the python unit test if necessary.
# Generated using PutAway_1.xml
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testPutAway_108497(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()
        
    def testPutAway_108497(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                       '\n')
        self.set_server_response('1,lunch,0,\n'
                                       '2,battery,0,\n'
                                       '\n')
        self.set_server_response('0,0,\n'
                                       '\n')
        self.set_server_response(',,0,0,\n'
                                       '\n')
        self.set_server_response('1,put away,0,\n'
                                       '2,replenishment,0,\n'
                                       '3,normal assignments,0,\n'
                                       '4,chase assignments,0,\n'
                                       '6,normal and chase assignments,0,\n'
                                       '7,line loading,0,\n'
                                       '8,put to store,0,\n'
                                       '9,cycle counting,0,\n'
                                       '10,loading,0,\n'
                                       '11,back stocking,0,\n'
                                       '\n')
        self.set_server_response('1,Region 1,0,\n'
                                       '2,Region 2,0,\n'
                                       '3,Region 3,0,\n'
                                       '4,Region 4,0,\n'
                                       '5,Region 5,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('4,Region 4,1,1,1,1,1,1,1,1,2,5,0,0,4,Exception Location,1,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('61234,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('63123,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('65123,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('65124,4,,,,,,,,,,0,0,0,\n'
                                       '65125,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('65127,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('65422,4,,,,,,,,,,0,0,0,\n'
                                       '65423,4,,,,,,,,,,0,0,0,\n'
                                       '65129,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('2,Location Extra Full,0,\n'
                                       '25,Got Tired,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('65989,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('2,Location Extra Full,0,\n'
                                       '25,Got Tired,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('65888,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('2,Location Extra Full,0,\n'
                                       '25,Got Tired,0,\n'
                                       '3,reason 3,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('97171,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response(',,,,,,,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')
        self.set_server_response('98172,4,,,,,,,,,,0,0,0,\n'
                                       '\n')
        self.set_server_response('locID,0,\n'
                                       '\n')
        self.set_server_response('0,\n'
                                       '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '1!',               # Function?
                                   'yes',              # put away, correct?
                                   '4',                # Region?
                                   'yes',              # Region 4, correct?
                                   'no',               # Another Region?
                                   '101ready',         # Start location?
                                   'yes',              # 101, correct?
                                   '101ready',         # check digits?
                                   '61234',            # License?
                                   'yes',              # 61234, correct?
                                   'no more',          # License?
                                   '124!',             # quantity
                                   'yes',              # 124, correct?
                                   '10ready',          # Location?
                                   'yes',              # 10, correct?
                                   '10ready',          # check digits?
                                   '24!',              # quantity
                                   'yes',              # You said 24, expected 124, is this a partial?
                                   '12ready',          # Location?
                                   'yes',              # 12, correct?
                                   '12ready',          # check digits?
                                   '100!',             # quantity
                                   'ready',            # Complete. Say ready.
                                   '63123',            # License?
                                   'yes',              # 63123, correct?
                                   'no more',          # License?
                                   '25!',              # quantity
                                   'yes',              # 25, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '11ready',          # Location?
                                   'yes',              # 11, correct?
                                   '11ready',          # check digits?
                                   '12!',              # quantity
                                   'yes',              # 12, correct?
                                   '101ready',         # Location?
                                   'yes',              # 101, correct?
                                   '101ready',         # check digits?
                                   '13!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '65123',            # License?
                                   'yes',              # 65123, correct?
                                   'no more',          # License?
                                   '33!',              # quantity
                                   'yes',              # 33, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '102ready',         # Location?
                                   'yes',              # 102, correct?
                                   '102ready',         # check digits?
                                   '11!',              # quantity
                                   'yes',              # 11, correct?
                                   '11ready',          # Location?
                                   'yes',              # 11, correct?
                                   '11ready',          # check digits?
                                   '10!',              # quantity
                                   'yes',              # You said 10, expected 22, is this a partial?
                                   '12ready',          # Location?
                                   'yes',              # 12, correct?
                                   '12ready',          # check digits?
                                   '12!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '65124',            # License?
                                   'yes',              # 65124, correct?
                                   '65125',            # License?
                                   'yes',              # 65125, correct?
                                   'no more',          # License?
                                   'ready',            # Put away license 2 4
                                   '15!',              # quantity
                                   'yes',              # 15, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '103ready',         # Location?
                                   'yes',              # 103, correct?
                                   '103ready',         # check digits?
                                   '5!',               # quantity
                                   'yes',              # 5, correct?
                                   '12ready',          # Location?
                                   'yes',              # 12, correct?
                                   '12ready',          # check digits?
                                   '10!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   'ready',            # Put away license 2 5
                                   '26!',              # quantity
                                   'yes',              # 26, correct?
                                   '102ready',         # Location?
                                   'yes',              # 102, correct?
                                   '102ready',         # check digits?
                                   '26!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '65127',            # License?
                                   'yes',              # 65127, correct?
                                   'no more',          # License?
                                   '15!',              # quantity
                                   'yes',              # 15, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12ready',          # Location?
                                   'yes',              # 12, correct?
                                   '12ready',          # check digits?
                                   '15!',              # quantity
                                   'ready',            # Complete. Say ready.
                                   '65422',            # License?
                                   'yes',              # 65422, correct?
                                   '65423',            # License?
                                   'yes',              # 65423, correct?
                                   '65129',            # License?
                                   'yes',              # 65129, correct?
                                   'no more',          # License?
                                   'ready',            # Put away license 2 2
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 2 2 canceled, say ready
                                   'release license',  # Put away license 2 3
                                   'yes',              # Release License, correct?
                                   'ready',            # 2 3 released, say ready
                                   'ready',            # Put away license 2 9
                                   '5!',               # quantity
                                   'yes',              # 5, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '102ready',         # Location?
                                   'yes',              # 102, correct?
                                   '102ready',         # check digits?
                                   '2!',               # quantity
                                   'yes',              # 2, correct?
                                   '103ready',         # Location?
                                   'yes',              # 103, correct?
                                   '103ready',         # check digits?
                                   '3!',               # quantity
                                   'ready',            # Complete. Say ready.
                                   '65989',            # License?
                                   'yes',              # 65989, correct?
                                   'no more',          # License?
                                   '12!',              # quantity
                                   'yes',              # 12, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12ready',          # Location?
                                   'yes',              # 12, correct?
                                   '12ready',          # check digits?
                                   '9!',               # quantity
                                   'yes',              # 9, correct?
                                   'cancel',           # Location?
                                   'yes',              # Cancel, correct?
                                   '25',               # Reason?
                                   'yes',              # 25, Got Tired, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 8 9 canceled, say ready
                                   '65888',            # License?
                                   'yes',              # 65888, correct?
                                   'no more',          # License?
                                   '32!',              # quantity
                                   'yes',              # 32, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '11ready',          # Location?
                                   'yes',              # 11, correct?
                                   '11ready',          # check digits?
                                   'cancel',           # quantity
                                   'yes',              # Cancel, correct?
                                   '2!',               # Reason?
                                   'yes',              # 2, Location Extra Full, correct?
                                   'ready',            # Deliver to Exception Location
                                   'ready',            # 8 8 canceled, say ready
                                   '97171',            # License?
                                   'yes',              # 97171, correct?
                                   'no more',          # License?
                                   '25!',              # quantity
                                   'yes',              # 25, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '12ready',          # Location?
                                   'yes',              # 12, correct?
                                   '12ready',          # check digits?
                                   '9!',               # quantity
                                   'yes',              # 9, correct?
                                   'release license',  # Location?
                                   'yes',              # Release License, correct?
                                   'ready',            # 7 1 released, say ready
                                   '98172',            # License?
                                   'yes',              # 98172, correct?
                                   'no more',          # License?
                                   '13!',              # quantity
                                   'yes',              # 13, correct?
                                   'partial',          # Location?
                                   'yes',              # partial, correct?
                                   '102ready',         # Location?
                                   'yes',              # 102, correct?
                                   '102ready',         # check digits?
                                   'release license',  # quantity
                                   'yes',              # Release License, correct?
                                   '-')                # 7 2 released, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'put away, correct?',
                              'Region?',
                              'Region 4, correct?',
                              'Another Region?',
                              'Start location?',
                              '101, correct?',
                              'check digits?',
                              'License?',
                              '61234, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '124, correct?',
                              'Location?',
                              '10, correct?',
                              'check digits?',
                              'quantity',
                              'You said 24, expected 124, is this a partial?',
                              'Location?',
                              '12, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '63123, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '25, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '11, correct?',
                              'check digits?',
                              'quantity',
                              '12, correct?',
                              'Location?',
                              '101, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '65123, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '33, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '102, correct?',
                              'check digits?',
                              'quantity',
                              '11, correct?',
                              'Location?',
                              '11, correct?',
                              'check digits?',
                              'quantity',
                              'You said 10, expected 22, is this a partial?',
                              'Location?',
                              '12, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '65124, correct?',
                              'License?',
                              '65125, correct?',
                              'License?',
                              'getting work',
                              'Put away license <spell>24</spell>',
                              'quantity',
                              '15, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '103, correct?',
                              'check digits?',
                              'quantity',
                              '5, correct?',
                              'Location?',
                              '12, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'Put away license <spell>25</spell>',
                              'quantity',
                              '26, correct?',
                              'Location?',
                              '102, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '65127, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '15, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '12, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '65422, correct?',
                              'License?',
                              '65423, correct?',
                              'License?',
                              '65129, correct?',
                              'License?',
                              'getting work',
                              'Put away license <spell>22</spell>',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '2 2 canceled, say ready',
                              'Put away license <spell>23</spell>',
                              'Release License, correct?',
                              '2 3 released, say ready',
                              'Put away license <spell>29</spell>',
                              'quantity',
                              '5, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '102, correct?',
                              'check digits?',
                              'quantity',
                              '2, correct?',
                              'Location?',
                              '103, correct?',
                              'check digits?',
                              'quantity',
                              'Complete. Say ready.',
                              'License?',
                              '65989, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '12, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '12, correct?',
                              'check digits?',
                              'quantity',
                              '9, correct?',
                              'Location?',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>25</spell>, Got Tired, correct?',
                              'Deliver to Exception Location',
                              '8 9 canceled, say ready',
                              'License?',
                              '65888, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '32, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '11, correct?',
                              'check digits?',
                              'quantity',
                              'Cancel, correct?',
                              'Reason?',
                              '<spell>2</spell>, Location Extra Full, correct?',
                              'Deliver to Exception Location',
                              '8 8 canceled, say ready',
                              'License?',
                              '97171, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '25, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '12, correct?',
                              'check digits?',
                              'quantity',
                              '9, correct?',
                              'Location?',
                              'Release License, correct?',
                              '7 1 released, say ready',
                              'License?',
                              '98172, correct?',
                              'License?',
                              'getting work',
                              'quantity',
                              '13, correct?',
                              'Location?',
                              'partial, correct?',
                              'Location?',
                              '102, correct?',
                              'check digits?',
                              'quantity',
                              'Release License, correct?',
                              '7 2 released, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTForkValidPutAwayRegions'],
                                      ['prTaskLUTForkRequestPutAwayRegion', '4', '0'],
                                      ['prTaskLUTForkPutAwayRegionConfiguration'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '101', '101', '1'],
                                      ['prTaskLUTForkVerifyLicense', '61234', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '10', '10', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '61234', '24', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '61234', 'locID', '100'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '61234', '100', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '63123', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '11', '11', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '63123', '12', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '63123', 'locID', '13'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '101', '101', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '63123', '13', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '65123', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '102', '102', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65123', '11', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '65123', 'locID', '22'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '11', '11', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65123', '10', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '65123', 'locID', '12'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65123', '12', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '65124', '0'],
                                      ['prTaskLUTForkVerifyLicense', '65125', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '103', '103', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65124', '5', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '65124', 'locID', '10'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65124', '10', 'locID', '1', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '102', '102', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65125', '26', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '65127', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65127', '15', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '65422', '0'],
                                      ['prTaskLUTForkVerifyLicense', '65423', '0'],
                                      ['prTaskLUTForkVerifyLicense', '65129', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '65422', '0', '', '2', '2', '*'],
                                      ['prTaskLUTForkPutAwayLicense', '65423', '0', '', '3', '', '*'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '102', '102', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65129', '2', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '65129', 'locID', '3'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '103', '103', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65129', '3', 'locID', '1', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '65989', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '65989', '9', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '65989', 'locID', '3'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '65989', '3', '', '2', '25', '*'],
                                      ['prTaskLUTForkVerifyLicense', '65888', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '11', '11', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '1', '1'],
                                      ['prTaskLUTForkPutAwayLicense', '65888', '32', '', '2', '2', '*'],
                                      ['prTaskLUTForkVerifyLicense', '97171', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '12', '12', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '97171', '9', 'locID', '0', '', '*'],
                                      ['prTaskLUTForkGetAlternatePutLocation', '1', '97171', 'locID', '16'],
                                      ['prTaskLUTForkPutAwayLicense', '97171', '0', '', '3', '', '*'],
                                      ['prTaskLUTForkVerifyLicense', '98172', '0'],
                                      ['prTaskLUTForkGetPutAway'],
                                      ['prTaskLUTCoreVerifyLocation', '0', '102', '102', '0'],
                                      ['prTaskLUTForkPutAwayLicense', '98172', '0', '', '3', '', '*'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
