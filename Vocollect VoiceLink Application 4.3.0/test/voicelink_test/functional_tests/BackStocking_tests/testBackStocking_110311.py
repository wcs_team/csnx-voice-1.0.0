##TestLink ID 110311 :: Test Case Verify Back Stocking Operator Build LPN Stop Build
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110311(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110311(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('staging area,,12345SCAN,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,12345,00013,00000,ITEM1,UCN,UPC,Description,0,0,,0,U1,prep message,0,\n'
                                 'N,12346SCAN,12346,0008,00000,ITEM1,UCN,UPC,Description,0,0,22222,0,U2,prep message,0,\n'
                                 'N,12347SCAN,12347,00025,00000,ITEM1,UCN,UPC,Description,0,0,33333,0,U3,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,trip summary,preformance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 1,2,post 2,2,00,22222,222,1234,13,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,1,post 1,3,00,22222,222,1234,13,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,1,post 2,4,00,22222,222,1234,13,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 3,1,post 1,5,00,22222,222,1234,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,pre 3,1,post 2,6,00,22222,222,1234,13,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '11',             # Function?
                                   'yes',            # back stocking, correct?
                                   '1',              # Region?
                                   'yes',            # back stocking region 1, correct?
                                   'ready',          # go to staging area
                                   '42589',          # License Plate?
                                   'yes',            # 42589, correct?
                                   'license plate',  # Carton?
                                   '12345!',         # Carton?
                                   '2!',             # Quantity?
                                   'yes',            # 2, correct?
                                   '12345!',         # Carton?
                                   '0!',             # Quantity?
                                   'yes',            # 0, correct?
                                   '12345!',         # Carton?
                                   'cancel',         # Quantity?
                                   'no',             # cancel, correct?
                                   '5!',             # Quantity?
                                   'yes',            # 5, correct?
                                   '12345!',         # Carton?
                                   'cancel',         # Quantity?
                                   'yes',            # cancel, correct?
                                   '12345!',         # Carton?
                                   '1',              # Quantity?
                                   'yes',            # 1, correct?
                                   '12345!',         # Carton?
                                   '8',              # Quantity?
                                   'ready',          # You said 8, only asked for 5, try again.
                                   '12345!',         # Carton?
                                   'quantity',       # Quantity?
                                   '5',              # Quantity?
                                   '12346!',         # Carton?
                                   '2',              # Quantity?
                                   'yes',            # 2, correct?
                                   'talkman help',   # Carton?
                                   'all cartons',    # Carton?
                                   'ready',          # All cartons not allowed, to continue, say ready.
                                   '12346!',         # Carton?
                                   'quantity',       # Quantity?
                                   'talkman help',   # Quantity?
                                   'description',    # Quantity?
                                   'carton ID',      # Quantity?
                                   'UPC',            # Quantity?
                                   'license plate',  # Quantity?
                                   'case code',      # Quantity?
                                   'item number',    # Quantity?
                                   'UPC',            # Quantity?
                                   '2',              # Quantity?
                                   'yes',            # 2, correct?
                                   '12346!',         # Carton?
                                   'case code',      # Quantity?
                                   'cancel',         # Quantity?
                                   'yes',            # cancel, correct?
                                   'stop',           # Carton?
                                   'yes',            # Stop build, correct?
                                   'ready',          # trip summary, say ready
                                   '-')              # pre 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'back stocking, correct?',
                              'Region?',
                              'back stocking region 1, correct?',
                              'go to staging area',
                              'License Plate?',
                              '42589, correct?',
                              'Carton?',
                              'LPN 42589',
                              'Carton?',
                              'Quantity?',
                              '2, correct?',
                              'Carton?',
                              'Quantity?',
                              '0, correct?',
                              'Carton?',
                              'Quantity?',
                              'cancel, correct?',
                              'Quantity?',
                              '5, correct?',
                              'Carton?',
                              'Quantity?',
                              'cancel, correct?',
                              'Carton?',
                              'Quantity?',
                              '1, correct?',
                              'Carton?',
                              'Quantity?',
                              'You said 8, only asked for 5, try again.',
                              'Carton?',
                              'Quantity?',
                              '5',
                              'Quantity?',
                              'Carton?',
                              'Quantity?',
                              '2, correct?',
                              'Carton?',
                              'Speak or scan the carton ID or say license plate, stop,  or all cartons',
                              'Carton?',
                              'All cartons not allowed, to continue, say ready.',
                              'Carton?',
                              'Quantity?',
                              '6',
                              'Quantity?',
                              'enter the quantity picked. or say description, carton ID, UPC, license plate, case code, item number, cancel,  or quantity',
                              'Quantity?',
                              'description',
                              'Quantity?',
                              '12346',
                              'Quantity?',
                              'UPC',
                              'Quantity?',
                              'LPN 42589',
                              'Quantity?',
                              'UCN',
                              'Quantity?',
                              'ITEM1',
                              'Quantity?',
                              'UPC',
                              'Quantity?',
                              '2, correct?',
                              'Carton?',
                              'Quantity?',
                              'UCN',
                              'Quantity?',
                              'cancel, correct?',
                              'Carton?',
                              'Stop build, correct?',
                              'trip summary, say ready',
                              'pre 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '42589'],
                                      ['prTaskLUTStockingCarton', '42589', '0', 'U1', '2', '', '0'],
                                      ['prTaskLUTStockingCarton', '42589', '0', 'U1', '0', '', '0'],
                                      ['prTaskLUTStockingCarton', '42589', '0', 'U1', '5', '', '0'],
                                      ['prTaskLUTStockingCarton', '42589', '0', 'U1', '1', '', '0'],
                                      ['prTaskLUTStockingCarton', '42589', '0', 'U1', '5', '', '0'],
                                      ['prTaskLUTStockingCarton', '42589', '0', 'U2', '2', '', '0'],
                                      ['prTaskLUTStockingCarton', '42589', '0', 'U2', '2', '', '0'],
                                      ['prTaskLUTStockingCarton', '42589', '0', '', '', '', '1'],
                                      ['prTaskLUTStockingGetTrip', '42589', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
