##TestLink ID 110361 :: Test Case Test Trip complete when residual is available

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110361(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110361(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,,13,0,ITEM1,UCN1,UPC-12345,Pepsi 12 pack,1,0,12345,21,U1,prep message,0,\n'
                                 'N,123456CAN,,13,0,ITEM2,UCN2,UPC-12346,Coke 6 pack,1,0,12346,24,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes,Last trip was 115 percent. Daily average is 105 percent,loc1,11111,N,pre 1,1,post 1,1,00,22222,333,1234,13,item1,description1,15 pack,UPC-101,UCN-989,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 1,2,post 2,2,00,22222,333,1234,13,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,1,post 1,3,00,22222,333,1234,13,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '11',     # Function?
                                   'yes',    # back stocking, correct?
                                   '1',      # Region?
                                   'yes',    # back stocking region 1, correct?
                                   'ready',  # go to Staging area 5 C System Driven
                                   '17846',  # License Plate 17846
                                   'ready',  # 12345, pick 13, prep message
                                   '13',     # Quantity?
                                   'ready',  # 12346, pick 13, prep message
                                   '13',     # Quantity?
                                   'ready',  # Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes, say ready
                                   'ready',  # pre 1
                                   'ready',  # Aisle 1
                                   'ready',  # post 1
                                   '00',     # 1
                                   '333!',   # 1 2 3 4 put 13, stocker prep
                                   'ready',  # Aisle 2
                                   'ready',  # post 2
                                   '00',     # 2
                                   '333!',   # 1 2 3 4 put 13, stocker prep
                                   'ready',  # pre 2
                                   'ready',  # Aisle 1
                                   'ready',  # post 1
                                   '00',     # 3
                                   '333!',   # 1 2 3 4 put 13, stocker prep
                                   'yes',    # Do you have residuals?
                                   '55',     # Deliver residuals to drop zone.
                                   '77',     # wrong 55, try again
                                   '99',     # Deliver residuals to drop zone.
                                   '11',     # wrong 99, try again
                                   '-')      # License Plate 17846

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'back stocking, correct?',
                              'Region?',
                              'back stocking region 1, correct?',
                              'go to Staging area 5 C System Driven',
                              'License Plate 17846',
                              '<spell>12345</spell>, pick 13, prep message',
                              'Quantity?',
                              '<spell>12346</spell>, pick 13, prep message',
                              'Quantity?',
                              'Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes, say ready',
                              'pre 1',
                              'Aisle <spell>1</spell>',
                              'post 1',
                              '1',
                              '<spell>1234</spell> put 13, stocker prep',
                              'Aisle <spell>2</spell>',
                              'post 2',
                              '2',
                              '<spell>1234</spell> put 13, stocker prep',
                              'pre 2',
                              'Aisle <spell>1</spell>',
                              'post 1',
                              '3',
                              '<spell>1234</spell> put 13, stocker prep',
                              'Do you have residuals?',
                              'Deliver residuals to drop zone.',
                              'wrong 55, try again',
                              'wrong 77, try again',
                              'Deliver residuals to drop zone.',
                              'wrong 99, try again',
                              'License Plate 17846')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '17846'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '17846', '0'],
                                      ['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '13', '0', ''],
                                      ['prTaskODRStockingStocked', '2', 'loc2', '2', '1234', 'item2', '13', '0', ''],
                                      ['prTaskODRStockingStocked', '3', 'loc3', '3', '1234', 'item3', '13', '0', ''],
                                      ['prTaskLUTStockingStopLicense', '17846'],
                                      ['prTaskLUTStockingGetLPN', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
