##TestLink  ID 110377 :: Test Case Test Additonal Vocabs

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110377(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110377(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,,13,0,ITEM1,UCN1,UPC-12345,Pepsi 12 pack,1,0,12345,21,U1,prep message,0,\n'
                                 'N,123456CAN,,13,0,ITEM2,UCN2,UPC-12346,Coke 6 pack,1,0,12346,24,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes,Last trip was 115 percent. Daily average is 105 percent,loc1,11111,N,pre 1,1,post 1,1,00,22222,333,1234,13,item1,description1,15 pack,UPC-101,UCN-989,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 1,2,post 2,2,00,22222,333,1234,13,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,1,post 1,3,00,22222,333,1234,13,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '11',               # Function?
                                   'yes',              # back stocking, correct?
                                   'description',      # Region?
                                   'ready',            # 1,back stocking region 1
                                   'stop',             # 2,back stocking region 2
                                   '1',                # Region?
                                   'yes',              # back stocking region 1, correct?
                                   'sign off',         # go to Staging area 5 C System Driven
                                   'no',               # Sign off, correct?
                                   'ready',            # go to Staging area 5 C System Driven
                                   'talkman help',     # License Plate 17846
                                   'location',         # License Plate 17846
                                   '17846',            # License Plate 17846
                                   'license plate',    # 12345, pick 13, prep message
                                   'cancel',           # 12345, pick 13, prep message
                                   'no',               # Cancel carton, correct?
                                   'stop',             # 12345, pick 13, prep message
                                   'no',               # Stop build, correct?
                                   'skip carton',      # 12345, pick 13, prep message
                                   'no',               # skip carton, correct?
                                   'repick skips',     # 12345, pick 13, prep message
                                   'how much more',    # 12345, pick 13, prep message
                                   'carton id',        # 12345, pick 13, prep message
                                   'carton',           # 12345, pick 13, prep message
                                   'case code',        # 12345, pick 13, prep message
                                   'item number',      # 12345, pick 13, prep message
                                   'carton ID',        # 12345, pick 13, prep message
                                   'description',      # 12345, pick 13, prep message
                                   'quantity',         # 12345, pick 13, prep message
                                   'UPC',              # 12345, pick 13, prep message
                                   'say again',        # 12345, pick 13, prep message
                                   'sign off',         # 12345, pick 13, prep message
                                   'no',               # Sign off, correct?
                                   'ready',            # 12345, pick 13, prep message
                                   '13',               # Quantity?
                                   'ready',            # 12346, pick 13, prep message
                                   '13',               # Quantity?
                                   'ready',            # Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes, say ready
                                   'repick skips',     # pre 1
                                   'how much more',    # pre 1
                                   'UPC',              # pre 1
                                   'case code',        # pre 1
                                   'ready',            # pre 1
                                   'case code',        # Aisle 1
                                   'skip aisle',       # Aisle 1
                                   'no',               # Skip aisle, correct?
                                   'how much more',    # Aisle 1
                                   'repick skips',     # Aisle 1
                                   'ready',            # Aisle 1
                                   'sign off',         # post 1
                                   'no',               # Sign off, correct?
                                   'ready',            # post 1
                                   'skip slot',        # 1
                                   'no',               # Skip slot, correct?
                                   'sign off',         # 1
                                   'no',               # Sign off, correct?
                                   'carton ID',        # 1
                                   'case code',        # 1
                                   'item number',      # 1
                                   'description',      # 1
                                   'quantity',         # 1
                                   'UPC',              # 1
                                   'location',         # 1
                                   'take a break',     # 1
                                   'no',               # Take a break, correct?
                                   '00',               # 1
                                   'short product',    # 1 2 3 4 put 13, stocker prep
                                   '333!',             # 
                                   '5!',               # How many did you put?
                                   'yes',              # 5, correct?
                                   'description',      # Reason?
                                   'ready',            # 1,reason 1
                                   'stop',             # 2,reason 2
                                   '1',                # Reason?
                                   'yes',              # reason 1, correct?
                                   'ready',            # Aisle 2
                                   'ready',            # post 2
                                   '00',               # 2
                                   'cancel',           # 1 2 3 4 put 13, stocker prep
                                   'no',               # Cancel carton, correct?
                                   'sign off',         # 1 2 3 4 put 13, stocker prep
                                   'no',               # Sign off, correct?
                                   'carton ID',        # 1 2 3 4 put 13, stocker prep
                                   'case code',        # 1 2 3 4 put 13, stocker prep
                                   'item number',      # 1 2 3 4 put 13, stocker prep
                                   'description',      # 1 2 3 4 put 13, stocker prep
                                   'quantity',         # 1 2 3 4 put 13, stocker prep
                                   'UPC',              # 1 2 3 4 put 13, stocker prep
                                   'how much more',    # 1 2 3 4 put 13, stocker prep
                                   'location',         # 1 2 3 4 put 13, stocker prep
                                   'repeat last put',  # 1 2 3 4 put 13, stocker prep
                                   '333!',             # 1 2 3 4 put 13, stocker prep
                                   'ready',            # pre 2
                                   'ready',            # Aisle 1
                                   'ready',            # post 1
                                   '00',               # 3
                                   'short product',    # 1 2 3 4 put 13, stocker prep
                                   '333!',             # 
                                   'carton ID',        # How many did you put?
                                   'case code',        # How many did you put?
                                   'item number',      # How many did you put?
                                   'description',      # How many did you put?
                                   'quantity',         # How many did you put?
                                   'UPC',              # How many did you put?
                                   'how much more',    # How many did you put?
                                   'location',         # How many did you put?
                                   '13!',              # How many did you put?
                                   'yes',              # 13, correct?
                                   'no',               # Do you have residuals?
                                   '-')                # License Plate 17846

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'back stocking, correct?',
                              'Region?',
                              '1, back stocking region 1',
                              '2, back stocking region 2',
                              'Region?',
                              'back stocking region 1, correct?',
                              'go to Staging area 5 C System Driven',
                              'Sign off, correct?',
                              'go to Staging area 5 C System Driven',
                              'License Plate 17846',
                              'Speak or scan the license plate number or say change region, location,  or change function',
                              'License Plate 17846',
                              'go to Staging area 5 C System Driven',
                              'License Plate 17846',
                              '<spell>12345</spell>, pick 13, prep message',
                              'LPN 17846',
                              '<spell>12345</spell>, pick 13, prep message',
                              'Cancel carton, correct?',
                              '<spell>12345</spell>, pick 13, prep message',
                              'Stop build, correct?',
                              '<spell>12345</spell>, pick 13, prep message',
                              'skip carton, correct?',
                              '<spell>12345</spell>, pick 13, prep message',
                              'No skips to repick.',
                              '<spell>12345</spell>, pick 13, prep message',
                              '26 remaining at 2 line items',
                              '<spell>12345</spell>, pick 13, prep message',
                              'UCN1',
                              '<spell>12345</spell>, pick 13, prep message',
                              'ITEM1',
                              '<spell>12345</spell>, pick 13, prep message',
                              '12345',
                              '<spell>12345</spell>, pick 13, prep message',
                              'pepsi 12 pack',
                              '<spell>12345</spell>, pick 13, prep message',
                              '13',
                              '<spell>12345</spell>, pick 13, prep message',
                              'UPC-12345',
                              '<spell>12345</spell>, pick 13, prep message',
                              'Sign off, correct?',
                              '<spell>12345</spell>, pick 13, prep message',
                              'Quantity?',
                              '<spell>12346</spell>, pick 13, prep message',
                              'Quantity?',
                              'Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes, say ready',
                              'pre 1',
                              'No skips to repick.',
                              'pre 1',
                              '39 remaining at 3 line items',
                              'pre 1',
                              'Aisle <spell>1</spell>',
                              'Skip aisle, correct?',
                              'Aisle <spell>1</spell>',
                              '39 remaining at 3 line items',
                              'Aisle <spell>1</spell>',
                              'No skips to repick.',
                              'Aisle <spell>1</spell>',
                              'post 1',
                              'Sign off, correct?',
                              'post 1',
                              '1',
                              'Skip slot, correct?',
                              '1',
                              'Sign off, correct?',
                              '1',
                              '333',
                              '1',
                              'UCN-989',
                              '1',
                              'item1',
                              '1',
                              'description1',
                              '1',
                              '13',
                              '1',
                              'UPC-101',
                              '1',
                              'pre 1,, Aisle <spell>1</spell>,, post 1,, <spell>1</spell>',
                              '1',
                              'take a break, correct?',
                              '1',
                              '<spell>1234</spell> put 13, stocker prep',
                              'Verify carton.',
                              'How many did you put?',
                              '5, correct?',
                              'Reason?',
                              '1, reason 1',
                              '2, reason 2',
                              'Reason?',
                              'reason 1, correct?',
                              'Aisle <spell>2</spell>',
                              'post 2',
                              '2',
                              '<spell>1234</spell> put 13, stocker prep',
                              'Cancel carton, correct?',
                              '<spell>1234</spell> put 13, stocker prep',
                              'Sign off, correct?',
                              '<spell>1234</spell> put 13, stocker prep',
                              '333',
                              '<spell>1234</spell> put 13, stocker prep',
                              'UCN',
                              '<spell>1234</spell> put 13, stocker prep',
                              'item2',
                              '<spell>1234</spell> put 13, stocker prep',
                              'description2',
                              '<spell>1234</spell> put 13, stocker prep',
                              '13',
                              '<spell>1234</spell> put 13, stocker prep',
                              'UPC',
                              '<spell>1234</spell> put 13, stocker prep',
                              '39 remaining at 3 line items',
                              '<spell>1234</spell> put 13, stocker prep',
                              'pre 1,, Aisle <spell>2</spell>,, post 2,, <spell>2</spell>',
                              '<spell>1234</spell> put 13, stocker prep',
                              'last put was aisle <spell>1</spell>, <spell>1</spell>, put 5 of 13',
                              '<spell>1234</spell> put 13, stocker prep',
                              'pre 2',
                              'Aisle <spell>1</spell>',
                              'post 1',
                              '3',
                              '<spell>1234</spell> put 13, stocker prep',
                              'Verify carton.',
                              'How many did you put?',
                              '333',
                              'How many did you put?',
                              'UCN',
                              'How many did you put?',
                              'item3',
                              'How many did you put?',
                              'description3',
                              'How many did you put?',
                              '13',
                              'How many did you put?',
                              'UPC',
                              'How many did you put?',
                              '26 remaining at 2 line items',
                              'How many did you put?',
                              'pre 2,, Aisle <spell>1</spell>,, post 1,, <spell>3</spell>',
                              'How many did you put?',
                              '13, correct?',
                              'Do you have residuals?',
                              'License Plate 17846')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '17846'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '17846', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '5', '8', '1'],
                                      ['prTaskODRStockingStocked', '2', 'loc2', '2', '1234', 'item2', '13', '0', ''],
                                      ['prTaskODRStockingStocked', '3', 'loc3', '3', '1234', 'item3', '13', '0', ''],
                                      ['prTaskLUTStockingStopLicense', '17846'],
                                      ['prTaskLUTStockingGetLPN', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
