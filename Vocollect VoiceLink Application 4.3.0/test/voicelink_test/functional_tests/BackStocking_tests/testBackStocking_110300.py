##TestLink ID 110300 :: Tests Case Test system build LPN when operator stops a build

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110300(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110300(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,,13,0,ITEM1,UCN1,UPC-12345,Pepsi 12 pack,1,0,12345,21,U1,prep message,0,\n'
                                 'N,123456CAN,,13,0,ITEM2,UCN2,UPC-12346,Coke 6 pack,1,0,12346,24,U1,prep message,0,\n'
                                 'N,12347SCAN,,13,0,ITEM3,UCN3,UPC-12347,Gatorade 24 pack,1,0,12347,35,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,trip summary,preformance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 1,2,post 2,2,00,22222,222,1234,13,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,1,post 1,3,00,22222,222,1234,13,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,1,post 2,4,00,22222,222,1234,13,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 3,1,post 1,5,00,22222,222,1234,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,pre 3,1,post 2,6,00,22222,222,1234,13,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '11',     # Function?
                                   'yes',    # back stocking, correct?
                                   '1',      # Region?
                                   'yes',    # back stocking region 1, correct?
                                   'ready',  # go to Staging area 5 C System Driven
                                   '17846',  # License Plate 17846
                                   'ready',  # 12345, pick 13, prep message
                                   '13',     # Quantity?
                                   'stop',   # 12346, pick 13, prep message
                                   'yes',    # Stop build, correct?
                                   '1',      # Reason?
                                   'yes',    # reason 1, correct?
                                   'ready',  # trip summary, say ready
                                   '-')      # pre 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready', 
                              'Password?', 
                              'Function?', 
                              'back stocking, correct?', 
                              'Region?', 
                              'back stocking region 1, correct?', 
                              'go to Staging area 5 C System Driven', 
                              'License Plate 17846', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'Quantity?', 
                              '<spell>12346</spell>, pick 13, prep message', 
                              'Stop build, correct?', 
                              'Reason?', 
                              'reason 1, correct?', 
                              'trip summary, say ready', 
                              'pre 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '17846'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '', '1', '1'],
                                      ['prTaskLUTStockingGetTrip', '17846', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
