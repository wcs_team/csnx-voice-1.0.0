##TestLink ID 110456 :: Test Case Verify putaway in reverse order region setting works correctly.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110456(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110456(self):
        #TODO: Change test because of change region
        return
    
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('2,back stocking region 2,xxxxx,xx,xxxxxx,1,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('staging area,,233SCAN,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,12345,00013,00000,ITEM1,UCN,UPC,Description,0,1,,0,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,"trip 1 54 items, 30 minutes","performance, 3 LPNs, 4 hours",loc1,11111,N,pre 1,21,post 1,11,00,22222,222,1234,23,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 2,42,post 2,12,12,22222,123,4123,3,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,11,post 1,3,03,22222,123,4123,133,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,11,post 2,4,04,22222,33,4433,123,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,432,3432,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,pre 2,11,post 2,5,05,22222,333,3432,33,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,5,05,22222,123,44123,12,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,22,,22222,222,3222,3,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('2,back stocking region 2,xxxxx,xx,xxxxxx,1,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('staging area,,233SCAN,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,12345,00013,00000,ITEM1,UCN,UPC,Description,0,1,,0,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,"trip 1 54 items, 30 minutes","performance, 3 LPNs, 4 hours",loc1,11111,N,pre 1,21,post 1,11,00,22222,222,1234,23,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 2,42,post 2,12,12,22222,123,4123,3,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,11,post 1,3,03,22222,123,4123,133,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,11,post 2,4,04,22222,33,4433,123,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,432,3432,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,pre 2,11,post 2,5,05,22222,333,3432,33,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,5,05,22222,123,44123,12,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,22,,22222,222,3222,3,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,xxxxx,xx,xxxxxx,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('staging area,,233SCAN,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,12345,00013,00000,ITEM1,UCN,UPC,Description,0,1,,0,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,"trip 1 54 items, 30 minutes","performance, 3 LPNs, 4 hours",loc1,11111,N,pre 1,21,post 1,11,00,22222,222,1234,23,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 2,42,post 2,12,12,22222,123,4123,3,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,11,post 1,3,03,22222,123,4123,133,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,11,post 2,4,04,22222,33,4433,123,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,432,3432,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,pre 2,11,post 2,5,05,22222,333,3432,33,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,5,05,22222,123,44123,12,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,22,,22222,222,3222,3,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '11',             # Function?
                                   'yes',            # back stocking, correct?
                                   '2',              # Region?
                                   'yes',            # back stocking region 2, correct?
                                   'ready',          # go to staging area
                                   '234!',           # License Plate?
                                   'yes',            # 234, correct?
                                   '12345!',         # Carton?
                                   '13',             # Quantity?
                                   'yes',            # Put away in reverse order?
                                   'ready',          # trip 1 54 items, 30 minutes, say ready
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 1
                                   '00',             # 11
                                   '222!',           # 1 2 3 4 put 23, stocker prep
                                   'change region',  # pre 2
                                   'yes',            # Change region, correct?
                                   '2',              # Region?
                                   'yes',            # back stocking region 2, correct?
                                   '235!',           # License Plate?
                                   'yes',            # 235, correct?
                                   '12345!',         # Carton?
                                   '13',             # Quantity?
                                   'no',             # Put away in reverse order?
                                   'ready',          # trip 1 54 items, 30 minutes, say ready
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 1
                                   '00',             # 11
                                   '222!',           # 1 2 3 4 put 23, stocker prep
                                   'change region',  # pre 2
                                   'yes',            # Change region, correct?
                                   '1',              # Region?
                                   'yes',            # back stocking region 1, correct?
                                   '236!',           # License Plate?
                                   'yes',            # 236, correct?
                                   '12345!',         # Carton?
                                   '13',             # Quantity?
                                   'ready',          # trip 1 54 items, 30 minutes, say ready
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 1
                                   '00',             # 11
                                   '222!',           # 1 2 3 4 put 23, stocker prep
                                   'sign off',       # pre 2
                                   'yes',            # Sign off, correct?
                                   '-')              # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'back stocking, correct?',
                              'Region?',
                              'back stocking region 2, correct?',
                              'go to staging area',
                              'License Plate?',
                              '234, correct?',
                              'Carton?',
                              'Quantity?',
                              'Build complete.',
                              'Put away in reverse order?',
                              'Receiving work in reverse order.',
                              'trip 1 54 items, 30 minutes, say ready',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 1',
                              '11',
                              '<spell>1234</spell> put 23, stocker prep',
                              'pre 2',
                              'Change region, correct?',
                              'Region?',
                              'back stocking region 2, correct?',
                              'License Plate?',
                              '235, correct?',
                              'Carton?',
                              'Quantity?',
                              'Build complete.',
                              'Put away in reverse order?',
                              'trip 1 54 items, 30 minutes, say ready',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 1',
                              '11',
                              '<spell>1234</spell> put 23, stocker prep',
                              'pre 2',
                              'Change region, correct?',
                              'Region?',
                              'back stocking region 1, correct?',
                              'License Plate?',
                              '236, correct?',
                              'Carton?',
                              'Quantity?',
                              'Build complete.',
                              'trip 1 54 items, 30 minutes, say ready',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 1',
                              '11',
                              '<spell>1234</spell> put 23, stocker prep',
                              'pre 2',
                              'Sign off, correct?',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '2', '11'],
                                      ['prTaskLUTStockingGetLPN', '2'],
                                      ['prTaskLUTStockingValidateLPN', '234'],
                                      ['prTaskLUTStockingCarton', '234', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '234', '1'],
                                      ['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '23', '0', ''],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '2', '11'],
                                      ['prTaskLUTStockingGetLPN', '2'],
                                      ['prTaskLUTStockingValidateLPN', '235'],
                                      ['prTaskLUTStockingCarton', '235', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '235', '0'],
                                      ['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '23', '0', ''],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '236'],
                                      ['prTaskLUTStockingCarton', '236', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '236', '0'],
                                      ['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '23', '0', ''],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
