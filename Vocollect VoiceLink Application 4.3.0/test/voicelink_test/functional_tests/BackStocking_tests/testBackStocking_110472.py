##TestLink ID 110472 :: Test Case Verify Repick Skips region setting when repick skips is disabled.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110472(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110472(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('2,back stocking region 2,xxxxx,xx,xxxxxx,1,1,1,0,0,0,0,\n'
                                 '\n')
        self.set_server_response('staging area,,233SCAN,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,12345,00013,00000,ITEM1,UCN,UPC,Description,0,1,,0,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,"trip 1 54 items, 30 minutes","performance, 3 LPNs, 4 hours",loc1,11111,N,pre 1,21,post 1,11,00,22222,222,1234,23,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 2,42,post 2,12,12,22222,123,4123,3,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,11,post 1,3,03,22222,123,4123,133,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,11,post 2,4,04,22222,33,4433,123,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,432,3432,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,333,3432,33,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,5,05,22222,123,44123,12,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc7,11111,N,,11,,22,,22222,222,3222,3,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,333,3432,53,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '1,1,"trip 1 54 items, 30 minutes","performance, 3 LPNs, 4 hours",loc11,11111,N,pre 1,21,post 4,11,00,22222,222,1234,213,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('staging area,,233SCAN,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '11',             # Function?
                                   'yes',            # back stocking, correct?
                                   '2',              # Region?
                                   'yes',            # back stocking region 2, correct?
                                   'ready',          # go to staging area
                                   '425!',           # License Plate?
                                   'yes',            # 425, correct?
                                   '12345!',         # Carton?
                                   '13',             # Quantity?
                                   'no',             # Put away in reverse order?
                                   'ready',          # trip 1 54 items, 30 minutes, say ready
                                   'ready',          # pre 1
                                   'skip aisle',     # Aisle 21
                                   'yes',            # Skip aisle, correct?
                                   'repick skips',   # pre 2
                                   'ready',          # pre 2
                                   'repick skips',   # Aisle 42
                                   'ready',          # Aisle 42
                                   'repick skips',   # post 2
                                   'ready',          # post 2
                                   'skip slot',      # 12
                                   'no',             # Skip slot, correct?
                                   '12',             # 12
                                   '123!',           # 4 1 2 3 put 3, stocker prep
                                   'ready',          # Aisle 11
                                   'ready',          # post 1
                                   'skip slot',      # 3
                                   'yes',            # Skip slot, correct?
                                   'repick skips',   # post 2
                                   'ready',          # post 2
                                   '04',             # 4
                                   '33!',            # 4 4 3 3 put 123, stocker prep
                                   'skip slot',      # 5
                                   'yes',            # Skip slot, correct?
                                   'ready',          # Aisle 11
                                   '05',             # 5
                                   '123!',           # 4 4 1 2 3 put 12, stocker prep
                                   'ready',          # 22
                                   '222!',           # 3 2 2 2 put 3, stocker prep
                                   'ready',          # Trip complete.  To go back for skips, say ready.
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 1
                                   'skip slot',      # 11
                                   'yes',            # Skip slot, correct?
                                   'ready',          # pre 2
                                   'ready',          # Aisle 11
                                   'ready',          # post 1
                                   '03',             # 3
                                   'short product',  # 4 1 2 3 put 133, stocker prep
                                   '123!',           # 
                                   '11!',            # How many did you put?
                                   'yes',            # 11, correct?
                                   '1',              # Reason?
                                   'yes',            # reason 1, correct?
                                   'ready',          # post 2
                                   '05',             # 5
                                   '132!',           # 3 4 3 2 put 13, stocker prep
                                   '432!',           # wrong 132, try again
                                   '05',             # 5
                                   '333!',           # 3 4 3 2 put 33, stocker prep
                                   '05',             # 5
                                   '333!',           # 3 4 3 2 put 53, stocker prep
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 4
                                   '00',             # 11
                                   '222!',           # 1 2 3 4 put 213, stocker prep
                                   'ready',          # Trip complete.  To go back for skips, say ready.
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 1
                                   '00',             # 11
                                   '222!',           # 1 2 3 4 put 23, stocker prep
                                   'no',             # Do you have residuals?
                                   'sign off',       # License Plate?
                                   'yes',            # Sign off, correct?
                                   'ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '-')              # Password?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'back stocking, correct?',
                              'Region?',
                              'back stocking region 2, correct?',
                              'go to staging area',
                              'License Plate?',
                              '425, correct?',
                              'Carton?',
                              'Quantity?',
                              'Build complete.',
                              'Put away in reverse order?',
                              'trip 1 54 items, 30 minutes, say ready',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'Skip aisle, correct?',
                              'pre 2',
                              'Repick skips not allowed.',
                              'pre 2',
                              'Aisle <spell>42</spell>',
                              'Repick skips not allowed.',
                              'Aisle <spell>42</spell>',
                              'post 2',
                              'Repick skips not allowed.',
                              'post 2',
                              '12',
                              'Skip slot, correct?',
                              '12',
                              '<spell>4123</spell> put 3, stocker prep',
                              'Aisle <spell>11</spell>',
                              'post 1',
                              '3',
                              'Skip slot, correct?',
                              'post 2',
                              'Repick skips not allowed.',
                              'post 2',
                              '4',
                              '<spell>4433</spell> put 123, stocker prep',
                              '5',
                              'Skip slot, correct?',
                              'Aisle <spell>11</spell>',
                              '5',
                              '<spell>44123</spell> put 12, stocker prep',
                              '22',
                              '<spell>3222</spell> put 3, stocker prep',
                              'Trip complete.  To go back for skips, say ready.',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 1',
                              '11',
                              'Skip slot, correct?',
                              'pre 2',
                              'Aisle <spell>11</spell>',
                              'post 1',
                              '3',
                              '<spell>4123</spell> put 133, stocker prep',
                              'Verify carton.',
                              'How many did you put?',
                              '11, correct?',
                              'Reason?',
                              'reason 1, correct?',
                              'post 2',
                              '5',
                              '<spell>3432</spell> put 13, stocker prep',
                              'wrong 132, try again',
                              '5',
                              '<spell>3432</spell> put 33, stocker prep',
                              '5',
                              '<spell>3432</spell> put 53, stocker prep',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 4',
                              '11',
                              '<spell>1234</spell> put 213, stocker prep',
                              'Trip complete.  To go back for skips, say ready.',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 1',
                              '11',
                              '<spell>1234</spell> put 23, stocker prep',
                              'Do you have residuals?',
                              'License Plate?',
                              'Sign off, correct?',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '2', '11'],
                                      ['prTaskLUTStockingGetLPN', '2'],
                                      ['prTaskLUTStockingValidateLPN', '425'],
                                      ['prTaskLUTStockingCarton', '425', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '425', '0'],
                                      ['prTaskODRStockingStocked', '2', 'loc2', '2', '4123', 'item2', '3', '0', ''],
                                      ['prTaskODRStockingStocked', '4', 'loc4', '4', '4433', 'item4', '123', '0', ''],
                                      ['prTaskODRStockingStocked', '6', 'loc6', '6', '44123', 'item6', '12', '0', ''],
                                      ['prTaskODRStockingStocked', '6', 'loc7', '6', '3222', 'item6', '3', '0', ''],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskODRStockingStocked', '3', 'loc3', '3', '4123', 'item3', '11', '122', '1'],
                                      ['prTaskODRStockingStocked', '5', 'loc5', '5', '3432', 'item5', '13', '0', ''],
                                      ['prTaskODRStockingStocked', '6', 'loc5', '6', '3432', 'item6', '33', '0', ''],
                                      ['prTaskODRStockingStocked', '6', 'loc5', '6', '3432', 'item6', '53', '0', ''],
                                      ['prTaskODRStockingStocked', '1', 'loc11', '1', '1234', 'item1', '213', '0', ''],
                                      ['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '23', '0', ''],
                                      ['prTaskLUTStockingStopLicense', '425'],
                                      ['prTaskLUTStockingGetLPN', '2'],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
