##TestLink ID 110290 :: Test Case Test system build LPN when all cartons flag set to 0

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110290(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110290(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')
        self.set_server_response('N,12346SCAN,,13,0,ITEM1,UCN,UPC-12346,Pepsi 12 pack,1,0,12345,21,U1,prep message,0,\n'
                                 'N,12345SCAN,,13,0,ITEM1,UCN,UPC-12345,Coke 6 pack,1,0,12346,24,U1,prep message,0,\n'
                                 'N,12347SCAN,,13,0,ITEM1,UCN,UPC-12347,Gatorade 24 pack,1,0,12347,35,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '11',             # Function?
                                   'yes',            # back stocking, correct?
                                   '1',              # Region?
                                   'yes',            # back stocking region 1, correct?
                                   'ready',          # go to Staging area 5 C System Driven
                                   '17846',          # License Plate 17846
                                   'talkman help',   # 12345, pick 13, prep message
                                   'description',    # 12345, pick 13, prep message
                                   'how much more',  # 12345, pick 13, prep message
                                   'upc',            # 12345, pick 13, prep message
                                   'UPC',            # 12345, pick 13, prep message
                                   'license plate',  # 12345, pick 13, prep message
                                   'case code',      # 12345, pick 13, prep message
                                   'quantity',       # 12345, pick 13, prep message
                                   'stop',           # 12345, pick 13, prep message
                                   'no',             # Stop build, correct?
                                   'cancel',         # 12345, pick 13, prep message
                                   'no',             # Cancel carton, correct?
                                   'skip carton',    # 12345, pick 13, prep message
                                   'no',             # skip carton, correct?
                                   'repick skips',   # 12345, pick 13, prep message
                                   'take a break',   # 12345, pick 13, prep message
                                   'yes',            # Take a break, correct?
                                   '2',              # Break type?
                                   'yes',            # battery, correct?
                                   'ready',          # To continue work, say ready
                                   '123',            # Password?
                                   'sign off',       # 12345, pick 13, prep message
                                   'no',             # Sign off, correct?
                                   'ready',          # 12345, pick 13, prep message
                                   '13',             # Quantity?
                                   '-')              # 12346, pick 13, prep message

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready', 
                              'Password?', 
                              'Function?', 
                              'back stocking, correct?', 
                              'Region?', 
                              'back stocking region 1, correct?', 
                              'go to Staging area 5 C System Driven', 
                              'License Plate 17846', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'Speak or scan the carton ID or say how much more, repick skips, description, carton ID, stop, UPC, license plate, cancel, case code, item number, ready, skip carton,  or quantity', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'pepsi 12 pack', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              '39 remaining at 3 line items', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'UPC-12346', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'LPN 17846', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'UCN', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              '13', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'Stop build, correct?', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'Cancel carton, correct?', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'skip carton, correct?', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'No skips to repick.', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'take a break, correct?', 
                              'Break type?', 
                              'battery, correct?', 
                              'To continue work, say ready', 
                              'Password?', 
                              '<spell>12345</spell>, pick 13, prep message', 'Sign off, correct?', 
                              '<spell>12345</spell>, pick 13, prep message', 'Quantity?', 
                              '<spell>12346</spell>, pick 13, prep message')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '17846'],
                                      ['prTaskODRCoreSendBreakInfo', '2', '0', 'battery'],
                                      ['prTaskODRCoreSendBreakInfo', '2', '1', 'battery'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
