##TestLink ID 110480 :: Verify that Put Away flow is correct with quantity verification turned off.
# Modified from original test to verify that licenses, cartons, carton verification and check digits
# can be scanned.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110480(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110480(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,xxxxx,xx,xxxxxx,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('staging area,,233SCAN,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,12345,00013,00000,ITEM1,UCN,UPC,Description,0,1,,0,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,"trip 1 54 items, 30 minutes","performance, 3 LPNs, 4 hours",loc1,11111,N,pre 1,21,post 1,11,00,22222,222,1234,23,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 2,42,post 2,12,12,1212,,4123,3,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,11,post 1,3,03,,,4123,133,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,11,post 2,4,04,22222,33,4433,123,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,432,3432,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,333,3432,33,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,,11,,5,05,22222,123,44123,12,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc7,11111,N,,11,,22,,22222,222,3222,3,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc5,11111,N,pre 2,11,post 2,5,05,22222,333,3432,53,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '1,1,"trip 1 54 items, 30 minutes","performance, 3 LPNs, 4 hours",loc11,11111,N,pre 1,21,post 4,11,00,22222,222,1234,213,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('1,reason 1,0,\n'
                                 '22,reason 22,0,\n'
                                 '333,reason 333,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,reason 1,0,\n'
                                 '22,reason 22,0,\n'
                                 '333,reason 333,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('1,reason 1,0,\n'
                                 '22,reason 22,0,\n'
                                 '333,reason 333,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,reason 1,0,\n'
                                 '22,reason 22,0,\n'
                                 '333,reason 333,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1,reason 1,0,\n'
                                 '22,reason 22,0,\n'
                                 '333,reason 333,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response('staging area,,233SCAN,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '11',             # Function?
                                   'yes',            # back stocking, correct?
                                   '1',              # Region?
                                   'yes',            # back stocking region 1, correct?
                                   'ready',          # go to staging area
                                   '#519',           # License Plate?
                                   '#12345SCAN',     # Carton?
                                   '13',             # Quantity?
                                   'ready',          # trip 1 54 items, 30 minutes, say ready
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 1
                                   '#00',            # 11
                                   'short product',  # 1 2 3 4 put 23, stocker prep
                                   '#22222',         # 
                                   '45!',            # How many did you put?
                                   'yes',            # 45, correct?
                                   '23!',            # How many did you put?
                                   'yes',            # 23, correct?
                                   'ready',          # pre 2
                                   'ready',          # Aisle 42
                                   'ready',          # post 2
                                   '#12',            # 12
                                   'short product',  # 4 1 2 3 put 3, stocker prep
                                   '#1212',          # 
                                   '3!',             # How many did you put?
                                   'no',             # 3, correct?
                                   '2!',             # How many did you put?
                                   'yes',            # 2, correct?
                                   '333',            # Reason?
                                   'yes',            # reason 333, correct?
                                   'ready',          # Aisle 11
                                   'ready',          # post 1
                                   '03',             # 3
                                   'ready',          # 4 1 2 3 put 133, stocker prep
                                   'ready',          # post 2
                                   '04',             # 4
                                   'short product',  # 4 4 3 3 put 123, stocker prep
                                   '33!',            # 
                                   '12!',            # How many did you put?
                                   'yes',            # 12, correct?
                                   '2!',             # Reason?
                                   '22!',            # Reason?
                                   'yes',            # reason 22, correct?
                                   '05',             # 5
                                   'cancel',         # 3 4 3 2 put 13, stocker prep
                                   'yes',            # Cancel carton, correct?
                                   '1!',             # Reason?
                                   'yes',            # reason 1, correct?
                                   '05',             # 5
                                   '323!',           # 3 4 3 2 put 33, stocker prep
                                   '423!',           # wrong 323, try again
                                   '333!',           # 3 4 3 2 put 33, stocker prep
                                   'ready',          # Aisle 11
                                   '05',             # 5
                                   'quantity',       # 4 4 1 2 3 put 12, stocker prep
                                   'short product',  # 4 4 1 2 3 put 12, stocker prep
                                   '152!',           # 
                                   '162!',           # wrong 152, try again
                                   'quantity',       # 4 4 1 2 3 put 12, stocker prep
                                   '123!',           # 4 4 1 2 3 put 12, stocker prep
                                   '11!',            # How many did you put?
                                   'yes',            # 11, correct?
                                   '144',            # Reason?
                                   '333',            # Reason?
                                   'yes',            # reason 333, correct?
                                   'ready',          # 22
                                   '222!',           # 3 2 2 2 put 3, stocker prep
                                   'ready',          # pre 2
                                   'ready',          # Aisle 11
                                   'ready',          # post 2
                                   '05',             # 5
                                   '333!',           # 3 4 3 2 put 53, stocker prep
                                   'ready',          # pre 1
                                   'ready',          # Aisle 21
                                   'ready',          # post 4
                                   '00',             # 11
                                   'cancel',         # 1 2 3 4 put 213, stocker prep
                                   'yes',            # Cancel carton, correct?
                                   '22!',            # Reason?
                                   'yes',            # reason 22, correct?
                                   'talkman help',   # Deliver residuals to drop zone.
                                   '11',             # Deliver residuals to drop zone.
                                   'sign off',       # License Plate?
                                   'yes',            # Sign off, correct?
                                   '-')              # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'back stocking, correct?',
                              'Region?',
                              'back stocking region 1, correct?',
                              'go to staging area',
                              'License Plate?',
                              'Carton?',
                              'Quantity?',
                              'Build complete.',
                              'trip 1 54 items, 30 minutes, say ready',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 1',
                              '11',
                              '<spell>1234</spell> put 23, stocker prep',
                              'Verify carton.',
                              'How many did you put?',
                              '45, correct?',
                              'You said 45, only asked for 23, try again.',
                              'How many did you put?',
                              '23, correct?',
                              'pre 2',
                              'Aisle <spell>42</spell>',
                              'post 2',
                              '12',
                              '<spell>4123</spell> put 3, stocker prep',
                              'Verify carton.',
                              'How many did you put?',
                              '3, correct?',
                              'How many did you put?',
                              '2, correct?',
                              'Reason?',
                              'reason 333, correct?',
                              'Aisle <spell>11</spell>',
                              'post 1',
                              '3',
                              '<spell>4123</spell> put 133, stocker prep',
                              'post 2',
                              '4',
                              '<spell>4433</spell> put 123, stocker prep',
                              'Verify carton.',
                              'How many did you put?',
                              '12, correct?',
                              'Reason?',
                              'wrong 2, try again',
                              'Reason?',
                              'reason 22, correct?',
                              '5',
                              '<spell>3432</spell> put 13, stocker prep',
                              'Cancel carton, correct?',
                              'Reason?',
                              'reason 1, correct?',
                              '5',
                              '<spell>3432</spell> put 33, stocker prep',
                              'wrong 323, try again',
                              'wrong 423, try again',
                              '<spell>3432</spell> put 33, stocker prep',
                              'Aisle <spell>11</spell>',
                              '5',
                              '<spell>44123</spell> put 12, stocker prep',
                              '12',
                              '<spell>44123</spell> put 12, stocker prep',
                              'Verify carton.',
                              'wrong 152, try again',
                              'wrong 162, try again',
                              '<spell>44123</spell> put 12, stocker prep',
                              '12',
                              '<spell>44123</spell> put 12, stocker prep',
                              'How many did you put?',
                              '11, correct?',
                              'Reason?',
                              'wrong 144, try again',
                              'Reason?',
                              'reason 333, correct?',
                              '22',
                              '<spell>3222</spell> put 3, stocker prep',
                              'pre 2',
                              'Aisle <spell>11</spell>',
                              'post 2',
                              '5',
                              '<spell>3432</spell> put 53, stocker prep',
                              'pre 1',
                              'Aisle <spell>21</spell>',
                              'post 4',
                              '11',
                              '<spell>1234</spell> put 213, stocker prep',
                              'Cancel carton, correct?',
                              'Reason?',
                              'reason 22, correct?',
                              'Deliver residuals to drop zone.',
                              'Say check digits',
                              'Deliver residuals to drop zone.',
                              'License Plate?',
                              'Sign off, correct?',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '519'],
                                      ['prTaskLUTStockingCarton', '519', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '519', '0'],
                                      ['prTaskODRStockingStocked', '1', 'loc1', '1', '1234', 'item1', '23', '0', ''],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskODRStockingStocked', '2', 'loc2', '2', '4123', 'item2', '2', '1', '333'],
                                      ['prTaskODRStockingStocked', '3', 'loc3', '3', '4123', 'item3', '133', '0', ''],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskODRStockingStocked', '4', 'loc4', '4', '4433', 'item4', '12', '111', '22'],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskODRStockingStocked', '5', 'loc5', '5', '3432', 'item5', '0', '13', '1'],
                                      ['prTaskODRStockingStocked', '6', 'loc5', '6', '3432', 'item6', '33', '0', ''],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskODRStockingStocked', '6', 'loc6', '6', '44123', 'item6', '11', '1', '333'],
                                      ['prTaskODRStockingStocked', '6', 'loc7', '6', '3222', 'item6', '3', '0', ''],
                                      ['prTaskODRStockingStocked', '6', 'loc5', '6', '3432', 'item6', '53', '0', ''],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskODRStockingStocked', '1', 'loc11', '1', '1234', 'item1', '0', '213', '22'],
                                      ['prTaskLUTStockingStopLicense', '519'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
