##TestLink ID 110292 :: Test Case Test system build LPN when spoken qty > pick qty
##TestLink ID 110296 :: Test Case Test system build LPN when spoken qty = 0
##TestLink ID 110294 :: Test Case Test system build LPN when spoken qty < pick qty

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110292(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110292(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')
        self.set_server_response('N,12346SCAN,,13,0,ITEM1,UCN,UPC-12346,Pepsi 12 pack,1,0,12345,21,U1,prep message,0,\n'
                                 'N,12345SCAN,,13,0,ITEM1,UCN,UPC-12345,Coke 6 pack,1,0,12346,24,U1,prep message,0,\n'
                                 'N,12347SCAN,,13,0,ITEM1,UCN,UPC-12347,Gatorade 24 pack,1,0,12347,35,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes,Last trip was 115 percent. Daily average is 105 percent,loc1,11111,N,pre 1,1,post 1,1,00,22222,333,1234,13,item1,description1,15 pack,UPC-101,UCN-989,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 1,2,post 2,2,00,22222,333,1234,13,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,1,post 1,3,00,22222,333,1234,13,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '11',             # Function?
                                   'yes',            # back stocking, correct?
                                   '1',              # Region?
                                   'yes',            # back stocking region 1, correct?
                                   'ready',          # go to Staging area 5 C System Driven
                                   '17846',          # License Plate 17846
                                   '20!',            # 12345, pick 13, prep message
                                   'ready',          # 12345, pick 13, prep message
                                   '20',             # Quantity?
                                   '8!',             # Quantity?
                                   'yes',            # You said 8. Is this a short product?
                                   '1',              # Reason?
                                   'yes',            # reason 1, correct?
                                   'ready',          # 12346, pick 13, prep message
                                   '0!',             # Quantity?
                                   'yes',            # You said 0. Is this a short product?
                                   '2',              # Reason?
                                   'yes',            # reason 2, correct?
                                   'ready',          # 12347, pick 13, prep message
                                   '13',             # Quantity?
                                   'how much more',  # 12345, pick 5, prep message
                                   'ready',          # 12345, pick 5, prep message
                                   'quantity',       # Quantity?
                                   '3',              # Quantity?
                                   'yes',            # You said 3. Is this a short product?
                                   '1',              # Reason?
                                   'yes',            # reason 1, correct?
                                   '13!',            # 12346, pick 13, prep message
                                   'ready',          # 12346, pick 13, prep message
                                   '13',             # Quantity?
                                   'ready',          # 12345, pick 2, prep message
                                   '2',              # Quantity?
                                   '-')              # Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes, say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready', 
                              'Password?', 'Function?', 'back stocking, correct?', 
                              'Region?', 'back stocking region 1, correct?', 
                              'go to Staging area 5 C System Driven', 
                              'License Plate 17846', 
                              '<spell>12345</spell>, pick 13, prep message', 
                              'Wrong 20', '<spell>12345</spell>, pick 13, prep message', 
                              'Quantity?', 'You said 20, only asked for 13, try again.', 
                              'Quantity?', 'You said 8. Is this a short product?', 
                              'Reason?', 'reason 1, correct?', 
                              '<spell>12346</spell>, pick 13, prep message', 
                              'Quantity?', 
                              'You said 0. Is this a short product?', 
                              'Reason?', 
                              'reason 2, correct?', 
                              '<spell>12347</spell>, pick 13, prep message', 
                              'Quantity?', 'Going back for shorts and skips.', 
                              '<spell>12345</spell>, pick 5, prep message', 
                              '18 remaining at 2 line items', 
                              '<spell>12345</spell>, pick 5, prep message', 
                              'Quantity?', '5', 'Quantity?', 
                              'You said 3. Is this a short product?', 
                              'Reason?', 'reason 1, correct?', 
                              '<spell>12346</spell>, pick 13, prep message', 
                              'Wrong 13', 
                              '<spell>12346</spell>, pick 13, prep message', 
                              'Quantity?', 
                              'Going back for shorts and skips.', 
                              '<spell>12345</spell>, pick 2, prep message', 
                              'Quantity?', 
                              'Assignment  1 has 13 cartons at twenty locations with a goal time of 35 minutes, say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '17846'],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '8', '1', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '0', '2', '0'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTCoreGetReasonCodes', '11', '1'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '3', '1', '0'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '13', '', '0'],
                                      ['prTaskLUTStockingCarton', '17846', '0', 'U1', '2', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '17846', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
