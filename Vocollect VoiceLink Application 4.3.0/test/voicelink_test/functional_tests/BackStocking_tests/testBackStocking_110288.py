##TestLink ID 110288 :: Test Case Test system build LPN flow when all cartons=1

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110288(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110288(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('Staging area 5 C System Driven,17846,1234567890,0,\n'
                                 'Staging area 5 B Operator Driven,,,,0,\n'
                                 '\n')
        self.set_server_response('N,12346SCAN,,13,0,ITEM1,UCN,UPC-12346,Pepsi 12 pack,1,1,12345,21,U1,prep message,0,\n'
                                 'N,12345SCAN,,13,0,ITEM1,UCN,UPC-12345,Coke 6 pack,1,1,12346,24,U1,prep message,0,\n'
                                 'N,12347SCAN,,13,0,ITEM1,UCN,UPC-12347,Gatorade 24 pack,1,1,12347,35,U1,prep message,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,trip summary,preformance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 1,2,post 2,2,00,22222,222,1234,13,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,1,post 1,3,00,22222,222,1234,13,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,1,post 2,4,00,22222,222,1234,13,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 3,1,post 1,5,00,22222,222,1234,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,pre 3,1,post 2,6,00,22222,222,1234,13,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',          # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',           # Password?
                                   '11',             # Function?
                                   'yes',            # back stocking, correct?
                                   '1',              # Region?
                                   'yes',            # back stocking region 1, correct?
                                   'ready',          # go to Staging area 5 C System Driven
                                   '17846',          # License Plate 17846
                                   'license plate',  # This is a full pallet assignment.  To continue say ready.
                                   'take a break',   # This is a full pallet assignment.  To continue say ready.
                                   'yes',            # Take a break, correct?
                                   '1',              # Break type?
                                   'yes',            # lunch, correct?
                                   'ready',          # To continue work, say ready
                                   '123',            # Password?
                                   'ready',          # This is a full pallet assignment.  To continue say ready.
                                   'ready',          # trip summary, say ready
                                   'ready',          # pre 1
                                   '-')              # Aisle 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready', 
                              'Password?', 
                              'Function?', 
                              'back stocking, correct?', 
                              'Region?', 
                              'back stocking region 1, correct?', 
                              'go to Staging area 5 C System Driven', 
                              'License Plate 17846', 
                              'This is a full pallet assignment.  To continue say ready.', 
                              'LPN 17846', 
                              'This is a full pallet assignment.  To continue say ready.', 
                              'take a break, correct?', 
                              'Break type?', 'lunch, correct?', 
                              'To continue work, say ready', 
                              'Password?', 
                              'This is a full pallet assignment.  To continue say ready.', 
                              'trip summary, say ready', 
                              'pre 1', 
                              'Aisle <spell>1</spell>')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '17846'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTStockingCarton', '17846', '1', '', '', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '17846', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
