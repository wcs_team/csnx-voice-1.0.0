##TestLink ID 110309 :: Test Case Verify Back Stocking Operator Build LPN

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testBackStocking_110309(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testBackStocking_110309(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,0,\n'
                                 '2,back stocking region 2,0,\n'
                                 '3,back stocking region 3,0,\n'
                                 '\n')
        self.set_server_response('1,back stocking region 1,pre,aisle,post,0,1,1,0,1,1,0,\n'
                                 '\n')
        self.set_server_response('staging area,,12345SCAN,0,\n'
                                 '\n')
        self.set_server_response('N,12345SCAN,12345,00013,00000,ITEM1,UCN,UPC,Description,0,0,,0,U1,prep message,0,\n'
                                 'N,12346SCAN,12346,0008,00000,ITEM1,UCN,UPC,Description,0,0,22222,0,U2,prep message,0,\n'
                                 'N,12347SCAN,12347,00025,00000,ITEM1,UCN,UPC,Description,0,0,33333,0,U3,prep message,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('1,1,trip summary,preformance summary,loc1,11111,N,pre 1,1,post 1,1,00,22222,222,1234,13,item1,description1,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '2,2,trip summary,preformance summary,loc2,11111,N,pre 1,2,post 2,2,00,22222,222,1234,13,item2,description2,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '3,3,trip summary,preformance summary,loc3,11111,N,pre 2,1,post 1,3,00,22222,222,1234,13,item3,description3,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '4,4,trip summary,preformance summary,loc4,11111,N,pre 2,1,post 2,4,00,22222,222,1234,13,item4,description4,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '5,5,trip summary,preformance summary,loc5,11111,N,pre 3,1,post 1,5,00,22222,222,1234,13,item5,description5,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '6,6,trip summary,preformance summary,loc6,11111,N,pre 3,1,post 2,6,00,22222,222,1234,13,item6,description6,pack,UPC,UCN,stocker prep,drop zone,11,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',          # Password?
                                   '11',            # Function?
                                   'yes',           # back stocking, correct?
                                   '1',             # Region?
                                   'yes',           # back stocking region 1, correct?
                                   'ready',         # go to staging area
                                   '428!',          # License Plate?
                                   'yes',           # 428, correct?
                                   'talkman help',  # Carton?
                                   '12315!',        # Carton?
                                   '12345!',        # Carton?
                                   '8!',            # Quantity?
                                   'yes',           # 8, correct?
                                   '12346!',        # Carton?
                                   '5',             # Quantity?
                                   'yes',           # 5, correct?
                                   '12345!',        # Carton?
                                   '9',             # Quantity?
                                   'ready',         # You said 9, only asked for 5, try again.
                                   '12345!',        # Carton?
                                   '5',             # Quantity?
                                   '12347!',        # Carton?
                                   '20',            # Quantity?
                                   'yes',           # 20, correct?
                                   '12347!',        # Carton?
                                   '1',             # Quantity?
                                   'yes',           # 1, correct?
                                   '12346!',        # Carton?
                                   '9',             # Quantity?
                                   'ready',         # You said 9, only asked for 3, try again.
                                   '12345!',        # Carton?
                                   '12346!',        # Carton?
                                   '2',             # Quantity?
                                   'no',            # 2, correct?
                                   '12346!',        # Carton?
                                   '3',             # Quantity?
                                   '12347!',        # Carton?
                                   '4',             # Quantity?
                                   'ready',         # trip summary, say ready
                                   '-')             # pre 1

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'back stocking, correct?',
                              'Region?',
                              'back stocking region 1, correct?',
                              'go to staging area',
                              'License Plate?',
                              '428, correct?',
                              'Carton?',
                              'Speak or scan the carton ID or say license plate, stop,  or all cartons',
                              'Carton?',
                              'Wrong 12315',
                              'Carton?',
                              'Quantity?',
                              '8, correct?',
                              'Carton?',
                              'Quantity?',
                              '5, correct?',
                              'Carton?',
                              'Quantity?',
                              'You said 9, only asked for 5, try again.',
                              'Carton?',
                              'Quantity?',
                              'Carton?',
                              'Quantity?',
                              '20, correct?',
                              'Carton?',
                              'Quantity?',
                              '1, correct?',
                              'Carton?',
                              'Quantity?',
                              'You said 9, only asked for 3, try again.',
                              'Carton?',
                              'Wrong 12345',
                              'Carton?',
                              'Quantity?',
                              '2, correct?',
                              'Carton?',
                              'Quantity?',
                              'Carton?',
                              'Quantity?',
                              'Build complete.',
                              'trip summary, say ready',
                              'pre 1')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTStockingValidRegions'],
                                      ['prTaskLUTStockingGetRegionConfiguration', '1', '11'],
                                      ['prTaskLUTStockingGetLPN', '1'],
                                      ['prTaskLUTStockingValidateLPN', '428'],
                                      ['prTaskLUTStockingCarton', '428', '0', 'U1', '8', '', '0'],
                                      ['prTaskLUTStockingCarton', '428', '0', 'U2', '5', '', '0'],
                                      ['prTaskLUTStockingCarton', '428', '0', 'U1', '5', '', '0'],
                                      ['prTaskLUTStockingCarton', '428', '0', 'U3', '20', '', '0'],
                                      ['prTaskLUTStockingCarton', '428', '0', 'U3', '1', '', '0'],
                                      ['prTaskLUTStockingCarton', '428', '0', 'U2', '3', '', '0'],
                                      ['prTaskLUTStockingCarton', '428', '0', 'U3', '4', '', '0'],
                                      ['prTaskLUTStockingGetTrip', '428', '0'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
