##TestLink ID 109994 :: Test Case Test flow when multiple regions are returned

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testCycleCounting_109994(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testCycleCounting_109994(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('H,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '9!',               # Function?
                                   'yes',              # cycle counting, correct?
                                   'no more',          # Region?
                                   'all',              # Region?
                                   'no',               # all, correct?
                                   'description',      # Region?
                                   'ready',            # 2,Produce
                                   'ready',            # 4,Dry Goods
                                   'cancel',           # 8,Juice
                                   'ready',            # 8,Juice
                                   'description',      # Region?
                                   'ready',            # 2,Produce
                                   'stop',             # 4,Dry Goods
                                   '2',                # Region?
                                   'no',               # Produce, correct?
                                   '2',                # Region?
                                   'no',               # Produce, correct?
                                   '1',                # Region?
                                   '4',                # Region?
                                   'no',               # Dry Goods, correct?
                                   'sign off',         # Region?
                                   'no',               # Sign off, correct?
                                   'take a break',     # Region?
                                   'no',               # Take a break, correct?
                                   'change function',  # Region?
                                   'no',               # change function, correct?
                                   '2',                # Region?
                                   'yes',              # Produce, correct?
                                   'yes',              # Another Region?
                                   '4',                # Region?
                                   'yes',              # Dry Goods, correct?
                                   'no more',          # Another Region?
                                   '-')                # next location?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'No more not valid until a region is specified.',
                              'Region?',
                              'all, correct?',
                              'Region?',
                              '2, Produce',
                              '4, Dry Goods',
                              '8, Juice',
                              'Region?',
                              '2, Produce',
                              '4, Dry Goods',
                              'Region?',
                              'Produce, correct?',
                              'Region?',
                              'Produce, correct?',
                              'Region?',
                              'Not authorized for <spell>1</spell>, Try again.',
                              'Region?',
                              'Dry Goods, correct?',
                              'Region?',
                              'Sign off, correct?',
                              'Region?',
                              'take a break, correct?',
                              'Region?',
                              'change function, correct?',
                              'Region?',
                              'Produce, correct?',
                              'Another Region?',
                              'Region?',
                              'Dry Goods, correct?',
                              'Another Region?',
                              'next location?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegion', '2', '0'],
                                      ['prTaskLUTCycleCountingRegion', '4', '0'],
                                      ['prTaskLUTCycleCountingMode'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
