##ID 130135 :: Test Case Test user can access Inventory Control from VoiceLink

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testReceiving_130135(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testReceiving_130135(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '12,receiving,0,\n'
                                 '\n')
        self.set_server_response('1,cycle counting region 1,0,\n'
                                 '2,cycle counting region 2,0,\n'
                                 '3,cycle counting region 3,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('H,0,\n'
                                 '\n')
        self.set_server_response('pre 1,1,post 1,S1,00,LocID,11,0,\n'
                                 '\n')
        self.set_server_response('A432 S012,,125,,101,00,11,Chocolate bunnies,Cases - Dry good,UPC 1,3,packs,K,10,5,chocolate_bunny.png,0,\n'
                                 'A432 S011,,125,,104,00,11,Chocolate eggs,Eaches - Dry good,UPC 1,3,eaches,B,16,6,chocolate_eggz.png,0,\n'
                                 'A432 S010,,125,,108,00,11,Chocolate candy bars,Cases - Dry good,UPC 2,3,cases,K,12,4,chocolate_candy.png,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('1,Short,0,\n'
                                 '2,Overage,0,\n'
                                 '3,Damage,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '9!',     # Function?
                                   'yes',    # cycle counting, correct?
                                   '1',      # Region?
                                   'yes',    # cycle counting region 1, correct?
                                   'no',     # Another Region?
                                   'ready',  # next location?
                                   'ready',  # pre 1
                                   'ready',  # Aisle 1
                                   'ready',  # post 1
                                   '00!',    # S1
                                   'yes',    # Is location empty?
                                   'yes',    # Are there 3 packs of Chocolate bunnies present?
                                   '2!',     # How many eaches of Chocolate eggs are present?
                                   '2!',     # How many eaches of Chocolate eggs are present?
                                   '1',      # Expected 3 you said 2, reason code?
                                   'yes',    # Short, correct?
                                   'yes',    # Are there 3 cases of Chocolate candy bars present?
                                   '-')      # next location?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'cycle counting region 1, correct?',
                              'Another Region?',
                              'next location?',
                              'pre 1',
                              'Aisle 1',
                              'post 1',
                              'S1',
                              'Is location empty?',
                              'Are there 3 packs of Chocolate bunnies present?',
                              'How many eaches of Chocolate eggs are present?',
                              'recount ,, aisle 125,, ,, Slot 104',
                              'How many eaches of Chocolate eggs are present?',
                              'Expected 3 you said 2, reason code?',
                              'Short, correct?',
                              'Are there 3 cases of Chocolate candy bars present?',
                              'next location?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCycleCountingRegion', '1', '0'],
                                      ['prTaskLUTCycleCountingMode'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskODRItemCountUpdate', 'A432 S012', 'Chocolate bunnies', '3', 'packs', 'N', ''],
                                      ['prTaskLUTCycleCountingReserveChain', 'A432 S011', 'Chocolate eggs'],
                                      ['prTaskLUTCoreGetReasonCodes', '9', '1'],
                                      ['prTaskODRItemCountUpdate', 'A432 S011', 'Chocolate eggs', '2', 'eaches', 'N', '1'],
                                      ['prTaskODRItemCountUpdate', 'A432 S010', 'Chocolate candy bars', '3', 'cases', 'N', '1'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
