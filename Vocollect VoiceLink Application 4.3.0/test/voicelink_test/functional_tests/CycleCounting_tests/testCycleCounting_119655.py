##TestLink ID 119655 :: Test Case Verify sign off is available during cycle counting

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testCycleCounting_119655(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testCycleCounting_119655(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('H,0,\n'
                                 '\n')
        self.set_server_response('Building 56,,,56,5656,560,3256,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,0035,123,Washers,ABC,125,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Building 56,,,56,5656,560,3256,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,0035,123,Washers,ABC,125,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',     # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',      # Password?
                                   '9!',        # Function?
                                   'yes',       # cycle counting, correct?
                                   '8',         # Region?
                                   'yes',       # Juice, correct?
                                   'yes',       # Another Region?
                                   '4',         # Region?
                                   'yes',       # Dry Goods, correct?
                                   'no',        # Another Region?
                                   'ready',     # next location?
                                   'sign off',  # Building 56
                                   'no',        # Sign off, correct?
                                   'ready',     # Building 56
                                   'sign off',  # 56
                                   'no',        # Sign off, correct?
                                   '5656!',     # 56
                                   'sign off',  # Is location empty?
                                   'no',        # Sign off, correct?
                                   'no',        # Is location empty?
                                   '3256!',     # 
                                   'sign off',  # How many Cases are present?
                                   'no',        # Sign off, correct?
                                   '5!',        # How many Cases are present?
                                   '5!',        # How many Cases are present?
                                   '2',         # Expected 125 you said 5, reason code?
                                   'yes',       # reason 2, correct?
                                   'ready',     # next location?
                                   '3256!',     # 56
                                   'sign off',  # How many Cases are present?
                                   'yes',       # Sign off, correct?
                                   '-')         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'Juice, correct?',
                              'Another Region?',
                              'Region?',
                              'Dry Goods, correct?',
                              'Another Region?',
                              'next location?',
                              'Building 56',
                              'Sign off, correct?',
                              'Building 56',
                              '56',
                              'Sign off, correct?',
                              '56',
                              'Is location empty?',
                              'Sign off, correct?',
                              'Is location empty?',
                              'You must speak the product ID. Try again.',
                              'How many Cases are present?',
                              'Sign off, correct?',
                              'How many Cases are present?',
                              'recount Building 56,, aisle 556,, Bay 56,, Slot 56',
                              'How many Cases are present?',
                              'Expected 125 you said 5, reason code?',
                              'reason 2, correct?',
                              'next location?',
                              '56',
                              'How many Cases are present?',
                              'Sign off, correct?',
                              'Welcome to the Vocollect system. Current operator is Operator.Name. Say ready')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCycleCountingRegion', '8', '0'],
                                      ['prTaskLUTCycleCountingRegion', '4', '0'],
                                      ['prTaskLUTCycleCountingMode'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskLUTCycleCountingReserveChain', '560', '123'],
                                      ['prTaskLUTCoreGetReasonCodes', '9', '1'],
                                      ['prTaskODRItemCountUpdate', '560', '123', '5', 'Cases', 'N', '2'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskLUTCoreSignOff'],
                                      ['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
