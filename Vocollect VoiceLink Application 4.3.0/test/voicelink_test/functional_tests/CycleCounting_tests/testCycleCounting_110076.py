# Verify Locations, PVID's and check digits can be scanned in the cycle counting application.
# Rename the file testGeneratorCycleCounting_110076.txt to testGeneratorCycleCounting_110076.py and 
# run to regenerate the python unit test if necessary.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest
import time

class testCycleCounting_110076(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testCycleCounting_110076(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('H,0,\n'
                                 '\n')
        self.set_server_response('Building 56,556,Bay 56,56,5656,560,,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,,123,Washers,ABC,7,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Building 56,556,Bay 56,56,5656,560,A2134BC8,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,A2134BC8,123,Washers,ABC,17,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('1A26321B,Building 78,778,Bay 78,78,78,0AB78,22,Wrenches,4B1,11,Cases,K,0,\n'
                                 '1A26321B,Building 79,779,Bay 79,79,79,079,333,Sockets,,4,Eaches,K,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',      # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',       # Password?
                                   '9!',         # Function?
                                   'yes',        # cycle counting, correct?
                                   '4',          # Region?
                                   'yes',        # Dry Goods, correct?
                                   'no',         # Another Region?
                                   'ready',      # next location?
                                   'ready',      # Building 56
                                   'ready',      # Aisle 556
                                   'ready',      # Bay 56
                                   '#A123',      # 56
                                   '#5656',      # 56
                                   '7!',         # How many Cases are present?
                                   'ready',      # next location?
                                   '#123AbC',    # 56
                                   '#A2134bc8',  # 56
                                   '#A2134BC8',  # 56
                                   '17!',        # How many Cases are present?
                                   '#1A26321B',  # next location?
                                   'yes',        # Are there 11 Cases of 22 present?
                                   'yes',        # Are there 4 Eaches of 333 present?
                                   '-')          # next location?

        #run main application
        self.assertRaises(EndOfApplication, main)
        time.sleep(1) #allow ODR's to catch up

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'Dry Goods, correct?',
                              'Another Region?',
                              'next location?',
                              'Building 56',
                              'Aisle 556',
                              'Bay 56',
                              '56',
                              'wrong <spell>A123</spell>, try again',
                              'How many Cases are present?',
                              'next location?',
                              '56',
                              'wrong <spell>123AbC</spell>, try again',
                              'wrong <spell>A2134bc8</spell>, try again',
                              '56',
                              'How many Cases are present?',
                              'next location?',
                              'Are there 11 Cases of 22 present?',
                              'Are there 4 Eaches of 333 present?',
                              'next location?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCycleCountingRegion', '4', '0'],
                                      ['prTaskLUTCycleCountingMode'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskODRItemCountUpdate', '560', '123', '7', 'Cases', 'N', ''],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskODRItemCountUpdate', '560', '123', '17', 'Cases', 'N', ''],
                                      ['prTaskLUTCycleCountingAssignment', '1', '1A26321B', ''],
                                      ['prTaskODRItemCountUpdate', '1A26321B', '22', '11', 'Cases', 'N', ''],
                                      ['prTaskODRItemCountUpdate', '1A26321B', '333', '4', 'Eaches', 'N', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
