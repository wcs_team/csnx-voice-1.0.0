# Verify Host directed cycle counting flow is correct when the operator specifies a location.
# Rename the file testGeneratorCycleCounting_109982.txt to testGeneratorCycleCounting_109982.py and 
# run to regenerate the python unit test if necessary.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testCycleCounting_109982(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testCycleCounting_109982(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('H,0,\n'
                                 '\n')
        self.set_server_response('Building 56,556,Bay 56,56,5656,560,,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,,123,Washers,ABC,7,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Building 78,778,Bay 78,78,78,780,0783,0,\n'
                                 '\n')
        self.set_server_response('7780,Building 78,778,Bay 78,78,78,0AB78,22,Wrenches,4B1,11,Cases,K,0,\n'
                                 '790,Building 79,779,Bay 79,79,79,079,333,Sockets,,4,Eaches,K,0,\n'
                                 '\n')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('1A2B,Building 78,778,Bay 78,78,78,,22,Wrenches,4B1,12,Cases,B,0,\n'
                                 '1A2B,Building 78,778,Bay 78,78,78,079,333,Sockets,,442,Eaches,K,0,\n'
                                 '\n')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Building 56,556,Bay 56,56,,560,,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,,123,Washers,ABC,75,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Building 56,556,Bay 56,56,,560,,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,,123,Washers,ABC,75,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response(',,,,,,,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',  # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',   # Password?
                                   '9!',     # Function?
                                   'yes',    # cycle counting, correct?
                                   '2',      # Region?
                                   'yes',    # Produce, correct?
                                   'yes',    # Another Region?
                                   '8',      # Region?
                                   'yes',    # Juice, correct?
                                   'no',     # Another Region?
                                   'ready',  # next location?
                                   'ready',  # Building 56
                                   'ready',  # Aisle 556
                                   'ready',  # Bay 56
                                   '5656!',  # 56
                                   '4!',     # How many Cases are present?
                                   '7!',     # How many Cases are present?
                                   '7!',     # How many Cases are present?
                                   'ready',  # next location?
                                   'ready',  # Building 78
                                   'ready',  # Aisle 778
                                   'ready',  # Bay 78
                                   '055!',   # 78
                                   '78!',    # 78
                                   'yes',    # Is location empty?
                                   'no',     # Are there 11 Cases of 22 present?
                                   '24!',    # How many Cases of 22 are present?
                                   '24!',    # How many Cases of 22 are present?
                                   '2',      # Expected 11 you said 24, reason code?
                                   'yes',    # reason 2, correct?
                                   'yes',    # Are there 4 Eaches of 333 present?
                                   '1A2B!',  # next location?
                                   '78!',    # Check digit?
                                   '22!',    # How many Cases of 22 are present?
                                   '12!',    # How many Cases of 22 are present?
                                   '12!',    # How many Cases of 22 are present?
                                   'no',     # Are there 442 Eaches of 333 present?
                                   '212!',   # How many Eaches of 333 are present?
                                   '212!',   # How many Eaches of 333 are present?
                                   '3',      # Expected 442 you said 212, reason code?
                                   'yes',    # reason 3, correct?
                                   'ready',  # next location?
                                   'ready',  # Building 56
                                   'ready',  # Aisle 556
                                   'ready',  # Bay 56
                                   'ready',  # 56
                                   '7!',     # How many Cases are present?
                                   '75!',    # How many Cases are present?
                                   '75!',    # How many Cases are present?
                                   'ready',  # next location?
                                   'ready',  # 56
                                   '75!',    # How many Cases are present?
                                   'ready',  # next location?
                                   'ready',  # No more assignments.
                                   '-')      # Region?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'Produce, correct?',
                              'Another Region?',
                              'Region?',
                              'Juice, correct?',
                              'Another Region?',
                              'next location?',
                              'Building 56',
                              'Aisle 556',
                              'Bay 56',
                              '56',
                              'How many Cases are present?',
                              'recount Building 56,, aisle 556,, Bay 56,, Slot 56',
                              'How many Cases are present?',
                              'recount Building 56,, aisle 556,, Bay 56,, Slot 56',
                              'How many Cases are present?',
                              'next location?',
                              'Building 78',
                              'Aisle 778',
                              'Bay 78',
                              '78',
                              'wrong 055, try again',
                              'Is location empty?',
                              'Are there 11 Cases of 22 present?',
                              'How many Cases of 22 are present?',
                              'recount Building 78,, aisle 778,, Bay 78,, Slot 78',
                              'How many Cases of 22 are present?',
                              'Expected 11 you said 24, reason code?',
                              'reason 2, correct?',
                              'Are there 4 Eaches of 333 present?',
                              'next location?',
                              'Check digit?',
                              'How many Cases of 22 are present?',
                              'recount Building 78,, aisle 778,, Bay 78,, Slot 78',
                              'How many Cases of 22 are present?',
                              'recount Building 78,, aisle 778,, Bay 78,, Slot 78',
                              'How many Cases of 22 are present?',
                              'Are there 442 Eaches of 333 present?',
                              'How many Eaches of 333 are present?',
                              'recount Building 78,, aisle 778,, Bay 78,, Slot 78',
                              'How many Eaches of 333 are present?',
                              'Expected 442 you said 212, reason code?',
                              'reason 3, correct?',
                              'next location?',
                              'Building 56',
                              'Aisle 556',
                              'Bay 56',
                              '56',
                              'How many Cases are present?',
                              'recount Building 56,, aisle 556,, Bay 56,, Slot 56',
                              'How many Cases are present?',
                              'recount Building 56,, aisle 556,, Bay 56,, Slot 56',
                              'How many Cases are present?',
                              'next location?',
                              '56',
                              'How many Cases are present?',
                              'next location?',
                              'Incorrect location.',
                              'next location?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCycleCountingRegion', '2', '0'],
                                      ['prTaskLUTCycleCountingRegion', '8', '0'],
                                      ['prTaskLUTCycleCountingMode'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskLUTCycleCountingReserveChain', '560', '123'],
                                      ['prTaskODRItemCountUpdate', '560', '123', '7', 'Cases', 'N', ''],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskLUTCycleCountingReserveChain', '7780', '22'],
                                      ['prTaskLUTCoreGetReasonCodes', '9', '1'],
                                      ['prTaskODRItemCountUpdate', '7780', '22', '24', 'Cases', 'N', '2'],
                                      ['prTaskODRItemCountUpdate', '790', '333', '4', 'Eaches', 'N', '2'],
                                      ['prTaskLUTCycleCountingAssignment', '0', '1A2B', '78'],
                                      ['prTaskLUTCycleCountingReserveChain', '1A2B', '22'],
                                      ['prTaskODRItemCountUpdate', '1A2B', '22', '12', 'Cases', 'N', ''],
                                      ['prTaskLUTCycleCountingReserveChain', '1A2B', '333'],
                                      ['prTaskLUTCoreGetReasonCodes', '9', '1'],
                                      ['prTaskODRItemCountUpdate', '1A2B', '333', '212', 'Eaches', 'N', '3'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskLUTCycleCountingReserveChain', '560', '123'],
                                      ['prTaskODRItemCountUpdate', '560', '123', '75', 'Cases', 'N', ''],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskODRItemCountUpdate', '560', '123', '75', 'Cases', 'N', ''],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment','','',''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
