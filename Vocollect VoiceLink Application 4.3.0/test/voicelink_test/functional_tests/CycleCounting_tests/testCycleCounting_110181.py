##TestLink ID 110181 :: Test Case Verify additional vocabulary words function correctly for Operator directed

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest
import time

class testCycleCounting_110181(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testCycleCounting_110181(self):
        
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('P,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('78745E,,,,63K,,,0012,Cigarettes,2100,12,Cartons,K,0,\n'
                                 '4534L,,,,65M,,,3450,,0543,8,Boxes,B,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',            # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',             # Password?
                                   '9!',               # Function?
                                   'yes',              # cycle counting, correct?
                                   '2',                # Region?
                                   'yes',              # Produce, correct?
                                   'no',               # Another Region?
                                   'talkman help',     # next location?
                                   'change region',    # next location?
                                   'no',               # Change region, correct?
                                   'change function',  # next location?
                                   'no',               # change function, correct?
                                   '560!',             # next location?
                                   '123!',             # Check digit?
                                   'talkman help',     # Are there 12 Cartons of 0012 present?
                                   'location',         # Are there 12 Cartons of 0012 present?
                                   'UPC',              # Are there 12 Cartons of 0012 present?
                                   'description',      # Are there 12 Cartons of 0012 present?
                                   'quantity',         # Are there 12 Cartons of 0012 present?
                                   'item number',      # Are there 12 Cartons of 0012 present?
                                   'yes',              # Are there 12 Cartons of 0012 present?
                                   'talkman help',     # How many Boxes of 3450 are present?
                                   'location',         # How many Boxes of 3450 are present?
                                   'item number',      # How many Boxes of 3450 are present?
                                   'UPC',              # How many Boxes of 3450 are present?
                                   'quantity',         # How many Boxes of 3450 are present?
                                   'description',      # How many Boxes of 3450 are present?
                                   '8!',               # How many Boxes of 3450 are present?
                                   'sign off',         # next location?
                                   'no',               # Sign off, correct?
                                   '-')                # next location?

        #run main application
        self.assertRaises(EndOfApplication, main)
        time.sleep(1) #allow ODR's to catch up

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'Produce, correct?',
                              'Another Region?',
                              'next location?',
                              'Say or scan a location or say change region,  or change function',
                              'next location?',
                              'Change region, correct?',
                              'next location?',
                              'change function, correct?',
                              'next location?',
                              'Check digit?',
                              'Are there 12 Cartons of 0012 present?',
                              '[standard help]',
                              'Are there 12 Cartons of 0012 present?',
                              ', , Slot 63K',
                              'Are there 12 Cartons of 0012 present?',
                              '2 1 0 0',
                              'Are there 12 Cartons of 0012 present?',
                              'cigarettes',
                              'Are there 12 Cartons of 0012 present?',
                              '12 Cartons',
                              'Are there 12 Cartons of 0012 present?',
                              '0012',
                              'Are there 12 Cartons of 0012 present?',
                              'How many Boxes of 3450 are present?',
                              'Say the quantity or say location, item number, UPC, quantity,  or description',
                              'How many Boxes of 3450 are present?',
                              ', , Slot 65M',
                              'How many Boxes of 3450 are present?',
                              '3450',
                              'How many Boxes of 3450 are present?',
                              '0 5 4 3',
                              'How many Boxes of 3450 are present?',
                              'quantity not available',
                              'How many Boxes of 3450 are present?',
                              'description not available',
                              'How many Boxes of 3450 are present?',
                              'next location?',
                              'Sign off, correct?',
                              'next location?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCycleCountingRegion', '2', '0'],
                                      ['prTaskLUTCycleCountingMode'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingAssignment', '0', '560', '123'],
                                      ['prTaskODRItemCountUpdate', '78745E', '0012', '12', 'Cartons', 'N', ''],
                                      ['prTaskODRItemCountUpdate', '4534L', '3450', '8', 'Boxes', 'N', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
