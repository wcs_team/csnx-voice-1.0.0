# Verify Host directed cycle counting flow is correct.
# Rename the file testGeneratorCycleCounting_109874.txt to testGeneratorCycleCounting_109874.py and 
# run to regenerate the python unit test if necessary.
from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest

class testCycleCounting_109874(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testCycleCounting_109874(self):
        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('H,0,\n'
                                 '\n')
        self.set_server_response('Building 56,556,Bay 56,56,5656,560,3256,0,\n'
                                 '\n')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,,123,Washers,ABC,7,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Building 78,778,Bay 78,78,78,780,078,0,\n'
                                 '\n')
        self.set_server_response('780,Building 78,778,Bay 78,78,78,078,22,Wrenches,4B1,11,Cases,K,0,\n'
                                 '790,Building 79,779,Bay 79,79,79,079,333,Sockets,,4,Eaches,K,0,\n'
                                 '\n')
        self.set_server_response('L9999,P99999,I9,10,0,\n'
                                 'L8888,P88888,I8,10,0,\n'
                                 'L7777,P77777,I7,10,0,\n'
                                 '\n')
        self.set_server_response('1,reason 1,0,\n'
                                 '2,reason 2,0,\n'
                                 '3,reason 3,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response(',,,,,,,,,,,,,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',        # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',         # Password?
                                   '9!',           # Function?
                                   'yes',          # cycle counting, correct?
                                   '8',            # Region?
                                   'yes',          # Juice, correct?
                                   'yes',          # Another Region?
                                   '4',            # Region?
                                   'yes',          # Dry Goods, correct?
                                   'no',           # Another Region?
                                   'ready',        # next location?
                                   'ready',        # Building 56
                                   'ready',        # Aisle 556
                                   'ready',        # Bay 56
                                   '5656!',        # 56
                                   'no',           # Is location empty?
                                   '3256!',        # 
                                   '5!',           # How many Cases are present?
                                   '5!',           # How many Cases are present?
                                   '2',            # Expected 7 you said 5, reason code?
                                   'yes',          # reason 2, correct?
                                   'ready',        # next location?
                                   'ready',        # Building 78
                                   'ready',        # Aisle 778
                                   'ready',        # Bay 78
                                   '078!',         # 78
                                   'no',           # Are there 11 Cases of 22 present?
                                   'description',  # How many Cases of 22 are present?
                                   '9!',           # How many Cases of 22 are present?
                                   '6!',           # How many Cases of 22 are present?
                                   '6!',           # How many Cases of 22 are present?
                                   '1',            # Expected 11 you said 6, reason code?
                                   'yes',          # reason 1, correct?
                                   'yes',          # Are there 4 Eaches of 333 present?
                                   'ready',        # next location?
                                   'ready',        # No more assignments.
                                   '-')            # Region?

        #run main application
        self.assertRaises(EndOfApplication, main)

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'Juice, correct?',
                              'Another Region?',
                              'Region?',
                              'Dry Goods, correct?',
                              'Another Region?',
                              'next location?',
                              'Building 56',
                              'Aisle 556',
                              'Bay 56',
                              '56',
                              'Is location empty?',
                              'You must speak the product ID. Try again.',
                              'How many Cases are present?',
                              'recount Building 56,, aisle 556,, Bay 56,, Slot 56',
                              'How many Cases are present?',
                              'Expected 7 you said 5, reason code?',
                              'reason 2, correct?',
                              'next location?',
                              'Building 78',
                              'Aisle 778',
                              'Bay 78',
                              '78',
                              'Are there 11 Cases of 22 present?',
                              'How many Cases of 22 are present?',
                              'wrenches',
                              'How many Cases of 22 are present?',
                              'recount Building 78,, aisle 778,, Bay 78,, Slot 78',
                              'How many Cases of 22 are present?',
                              'recount Building 78,, aisle 778,, Bay 78,, Slot 78',
                              'How many Cases of 22 are present?',
                              'Expected 11 you said 6, reason code?',
                              'reason 1, correct?',
                              'Are there 4 Eaches of 333 present?',
                              'next location?',
                              'Incorrect location.',
                              'next location?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCycleCountingRegion', '8', '0'],
                                      ['prTaskLUTCycleCountingRegion', '4', '0'],
                                      ['prTaskLUTCycleCountingMode'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskLUTCycleCountingReserveChain', '560', '123'],
                                      ['prTaskLUTCoreGetReasonCodes', '9', '1'],
                                      ['prTaskODRItemCountUpdate', '560', '123', '5', 'Cases', 'N', '2'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskLUTCycleCountingReserveChain', '780', '22'],
                                      ['prTaskLUTCoreGetReasonCodes', '9', '1'],
                                      ['prTaskODRItemCountUpdate', '780', '22', '6', 'Cases', 'N', '1'],
                                      ['prTaskODRItemCountUpdate', '790', '333', '4', 'Eaches', 'N', '1'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskLUTCycleCountingAssignment','','',''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
