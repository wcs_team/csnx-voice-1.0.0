##TestLink ID 110220 :: Test Case Verify Take a Break while Cycle Counting

from BaseVLTestCase import BaseVLTestCase, EndOfApplication, BOTH_SERVERS #Needs to be first import
from main import main
import unittest
import time

class testCycleCounting_110220(BaseVLTestCase):

    def setUp(self):
        self.start_server(BOTH_SERVERS)
        self.clear()

    def testCycleCounting_110220(self):

        #queue of LUT responses
        self.set_server_response('Vocollect,Operator.Id,0,0,\n'
                                 '\n')
        self.set_server_response('1,lunch,0,\n'
                                 '2,battery,0,\n'
                                 '\n')
        self.set_server_response('0,0,\n'
                                 '\n')
        self.set_server_response(',,0,0,\n'
                                 '\n')
        self.set_server_response('1,put away,0,\n'
                                 '2,replenishment,0,\n'
                                 '3,normal assignments,0,\n'
                                 '4,chase assignments,0,\n'
                                 '6,normal and chase assignments,0,\n'
                                 '7,line loading,0,\n'
                                 '8,put to store,0,\n'
                                 '9,cycle counting,0,\n'
                                 '10,loading,0,\n'
                                 '11,back stocking,0,\n'
                                 '\n')
        self.set_server_response('2,Produce,0,\n'
                                 '4,Dry Goods,0,\n'
                                 '8,Juice,0,\n'
                                 '\n')
        self.set_server_response('0,\n'
                                 '\n')
        self.set_server_response('H,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Building 56,,,56,5656,560,3256,0,\n'
                                 '\n')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('Y')
        self.set_server_response('560,Building 56,556,Bay 56,56,5656,,123,Washers,ABC,7,Cases,B,0,\n'
                                 '\n')
        self.set_server_response('Y')

        #queue of Dialog Responses
        self.post_dialog_responses('ready',         # Welcome to the Vocollect system. Current operator is Operator.Name. Say ready
                                   '123!',          # Password?
                                   '9!',            # Function?
                                   'yes',           # cycle counting, correct?
                                   '2',             # Region?
                                   'yes',           # Produce, correct?
                                   'no',            # Another Region?
                                   'take a break',  # next location?
                                   'yes',           # Take a break, correct?
                                   '2',             # Break type?
                                   'yes',           # battery, correct?
                                   'ready',         # To continue work, say ready
                                   '123',           # Password?
                                   'ready',         # next location?
                                   'take a break',  # Building 56
                                   'yes',           # Take a break, correct?
                                   '1',             # Break type?
                                   'no',            # lunch, correct?
                                   '1',             # Break type?
                                   'yes',           # lunch, correct?
                                   'ready',         # To continue work, say ready
                                   '123',           # Password?
                                   'ready',         # Building 56
                                   'take a break',  # 56
                                   'yes',           # Take a break, correct?
                                   '5',             # Break type?
                                   '1',             # Break type?
                                   'yes',           # lunch, correct?
                                   'ready',         # To continue work, say ready
                                   '123',           # Password?
                                   '3256!',         # 56
                                   'quantity',      # How many Cases are present?
                                   '7!',            # How many Cases are present?
                                   '-')             # next location?

        #run main application
        self.assertRaises(EndOfApplication, main)
        time.sleep(1) #allow ODR's to catch up          

        #validate prompts
        self.validate_prompts('Welcome to the Vocollect system. Current operator is Operator.Name. Say ready',
                              'Password?',
                              'Function?',
                              'cycle counting, correct?',
                              'Region?',
                              'Produce, correct?',
                              'Another Region?',
                              'next location?',
                              'take a break, correct?',
                              'Break type?',
                              'battery, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'next location?',
                              'Building 56',
                              'take a break, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              'Building 56',
                              '56',
                              'take a break, correct?',
                              'Break type?',
                              'wrong 5, try again',
                              'Break type?',
                              'lunch, correct?',
                              'To continue work, say ready',
                              'Password?',
                              '56',
                              'How many Cases are present?',
                              'quantity not available',
                              'How many Cases are present?',
                              'next location?')

        #validate LUT Requests
        self.validate_server_requests(['prTaskLUTCoreConfiguration', 'en_US', 'Default', 'CT-43-03-001'],
                                      ['prTaskLUTCoreBreakTypes'],
                                      ['prTaskLUTCoreSignOn', '123'],
                                      ['prTaskLUTCoreValidVehicleTypes', '0'],
                                      ['prTaskLUTCoreValidFunctions', '0'],
                                      ['prTaskLUTCycleCountingRegions'],
                                      ['prTaskLUTCycleCountingRegion', '2', '0'],
                                      ['prTaskLUTCycleCountingMode'],
                                      ['prTaskODRCoreSendBreakInfo', '2', '0', 'battery'],
                                      ['prTaskODRCoreSendBreakInfo', '2', '1', 'battery'],
                                      ['prTaskLUTCycleCountingLocation'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '0', 'lunch'],
                                      ['prTaskODRCoreSendBreakInfo', '1', '1', 'lunch'],
                                      ['prTaskLUTCycleCountingAssignment', '', '', ''],
                                      ['prTaskODRItemCountUpdate', '560', '123', '7', 'Cases', 'N', ''])

        #validate log messages

if __name__ == '__main__':
    unittest.main()
