from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_yes_no, prompt_only , prompt_digits_required
from vocollect_core.dialog.functions import prompt_digits
from common.VoiceLinkLut import VoiceLinkLutOdr

from VoiceLinkDialogs import prompt_alpha_numeric_suggested

from vocollect_core import itext, obj_factory, class_factory
from .VarWeightsAndSerialNum import WeightsSerialNumbers
from selection.LotTracking import LotTrackingTask
from selection.NewContainer import NewContainer
from selection.CloseContainer import CloseContainer
from selection.OpenContainer import OpenContainer
from cyclecounting.CycleCountingCountItem import CycleCountCountItemTask
from selection.SharedConstants import PICK_PROMPT_TASK_NAME, INTIALIZE_PUT,\
    CHECK_TARGET_CONTAINER, SLOT_VERIFICATION, CASE_LABEL_CD, ENTER_QTY,\
    QUANTITY_VERIFICATION, LOT_TRACKING, WEIGHT_SERIAL, XMIT_PICKS,\
    CHECK_PARTIAL, CLOSE_TARGET_CONT, NEXT_STEP, CYCLE_COUNT, PUT_PROMPT

class PickPromptTask(class_factory.get(TaskBase)):
    '''Pick prompts base task
    Known implementations PickPromptMultipleTask, PickPromptSingleTask
    
     Steps:
            1. Check target containers
            2. Slot verification
            3. Case Label Check Digits
            4. Enter Quantity
            5. Quantity Verification
            6. Lot Tracking
            7. Initialize put records
            8. Put Prompt
            9. Weights and serial numbers
            10. Transmit picks
            11. Check for partial (should container be close and opened)
            12. Check close target container
            13. Check if done or where to return to in pick prompt
            14. Cycle count if needed
          
     Parameters
            region - region operator is picking in 
            assignment_lut - assignments currently working on
            picks - pick list for specific location
            container_lut - current list of containers
            taskRunner (Default = None) - Task runner object
            callingTask (Default = None) - Calling task (should be a pick prompt task)
    '''
    
    #----------------------------------------------------------
    def __init__(self, 
                 region,  
                 assignment_lut, 
                 picks, 
                 container_lut,
                 auto_short,
                 taskRunner = None, 
                 callingTask = None):
        super(PickPromptTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = PICK_PROMPT_TASK_NAME

        #Luts and lut records       
        self._region = region
        self._assignment_lut = assignment_lut
        self._container_lut = container_lut
        
        #Picked LUT/ODR
        self._picked_lut = VoiceLinkLutOdr('prTaskLUTPicked', 'prTaskODRPicked', self._region['useLuts'])

        #Configure Object
        self.config(picks, auto_short)
        
    #----------------------------------------------------------
    def config(self, picks, auto_short):
        self.current_state = None
        self.next_state = None
        self.previous_state = None
        
        self._picks = picks
        self.dynamic_vocab.set_pick_prompt_task(self.name)
        
        #main variables
        self._expected_quantity = 0
        self._picked_quantity = 0
        self._short_product = False
        self._partial = False
        
        for pick in self._picks:
            self._expected_quantity += (pick['qtyToPick'] - pick['qtyPicked'])
       
        #lot tracking variables
        self._lot_number = None
        self._lot_quantity = 0

        #put variables
        self._put_quantity = 0
        self._curr_assignment = None
        self._curr_container = None
        self._puts = []
        
        #weights and serial numbers
        self._weights = []
        self._serial_numbers = []
        
        #Initialize commonly used variables
        self._uom = ''
        self._message = ''
        self._description = ''
        self._id_description = ''
        self._pvid = ''
        self._set_variables()
        self._skip_prompt = False
        
        #check if auto shorting
        if auto_short:
            self.current_state = INTIALIZE_PUT
            self._set_short_product(0)

            
    #----------------------------------------------------------
    def _set_variables(self):
        ''' used to initialize variables used throughout pick prompt '''
        if len(self._picks) == 0:
            return
          
        #adding commas so as to speak with pause
        self._uom = self._picks[0]['UOM'] + ','
        self._message = self._picks[0]['pickMessage'] 
        self._pvid = self._picks[0]['spokenProdID']
        if self._picks[0]['multiItemLocation'] == 1:
            self._description = self._picks[0]['description'].lower()+ ',' if self._picks[0]['description'] != '' else '' 
         
        if len(self._assignment_lut) > 1 and self._region['containerType'] == 0:
            self._id_description = self._picks[0]['idDescription']
            if self._id_description != '':
                self._id_description = itext('selection.pick.prompt.id', self._id_description + ',')
  
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States'''

        self.addState(CHECK_TARGET_CONTAINER,   self.check_target_container)
        self.addState(SLOT_VERIFICATION,        self.slot_verification)
        self.addState(CASE_LABEL_CD,            self.case_label_cd)
        self.addState(ENTER_QTY,                self.enter_qty)
        self.addState(QUANTITY_VERIFICATION,    self.verify_entered_quantity)
        self.addState(LOT_TRACKING,             self.lot_tracking)
        self.addState(INTIALIZE_PUT,            self.intialize_puts)
        self.addState(PUT_PROMPT,               self.put_prompt)
        self.addState(WEIGHT_SERIAL,            self.weight_and_serial_numbers)
        self.addState(XMIT_PICKS,               self.xmit_picks)
        self.addState(CHECK_PARTIAL,            self.check_partial)
        self.addState(CLOSE_TARGET_CONT,        self.check_close_target_container)
        self.addState(NEXT_STEP,                self.next_step)
        self.addState(CYCLE_COUNT,              self.cycle_count)
                
    #----------------------------------------------------------
    def check_target_container(self):
        ''' Check if picking to target containers and make sure specified target
        container exists and is opne. If exists and open and not active then 
        prompt operator to reopen, if does not exist or not opened, then call open
        container to open new target container  
        
        Note: assumes only 1 assignment
        '''
        
        target = self._picks[0]['targetContainer']
        if target == 0: #not picking target containers
            return
        elif target != self._assignment_lut[0]['activeTargetContainer']:
            spoken_container = None
            self._assignment_lut[0]['activeTargetContainer'] = target
            for container in self._container_lut:
                if container['targetConatiner'] == target and container['status'] == 'O':
                    spoken_container = container['spokenValidation']
            
            if spoken_container is not None:
                prompt_only(itext('selection.pick.prompt.reopen.container', 
                                       spoken_container))
            else:
                self.launch(obj_factory.get(OpenContainer,
                                              self._region,
                                              self._assignment_lut[0],
                                              self._picks,
                                              self._container_lut,
                                              self._assignment_lut.has_multiple_assignments(),
                                              self.taskRunner, self))
                
    #----------------------------------------------------------
    def slot_verification(self):
        #do nothing in base class, should be overridden in other classes
        pass

    #----------------------------------------------------------
    def case_label_cd(self):
        ''' Check and prompt for case Label Check digits '''
        #check if case label check digits exist, if not return
        if self._picks[0]['caseLabel'] == '':
            return

        additional_vocabulary={'short product' : False, 
                               'skip slot' : False,
                               'partial' : False}

        clcd = ''
        for pick in self._picks:
            if pick['status'] != 'P':
                clcd = pick['caseLabel']
                break
                
        result = prompt_digits_required(itext('selection.case.label.cd'), 
                                        itext('selection.case.label.cd.help'), 
                                        [clcd], 
                                        None,
                                        additional_vocabulary,
                                        self._skip_prompt)

        self._skip_prompt = False
        if result == 'short product':
            self.next_state = CASE_LABEL_CD
            self._validate_short_product()
            prompt_only(itext('selection.pick.prompt.check.digit'))
            self._skip_prompt = True #don't repeat main prompt

        elif result == 'partial':
            self.next_state = CASE_LABEL_CD
            self._validate_partial(self._expected_quantity)
            prompt_only(itext('selection.pick.prompt.check.digit'))
            self._skip_prompt = True #don't repeat main prompt

        elif result == 'skip slot':
            self.next_state = CASE_LABEL_CD
            self._skip_slot()
        
    #----------------------------------------------------------
    def enter_qty(self):
        ''' Enter quantity'''
        self._picked_quantity = self._expected_quantity
         
    #----------------------------------------------------------
    def verify_entered_quantity(self):
        '''verifies the entered quantity'''
        #If the quantity verification is set prompt for quantity     
        if(self._picked_quantity > self._expected_quantity):
            prompt_only(itext("selection.pick.prompt.pick.quantity.greater", self._picked_quantity, self._expected_quantity))
            self.next_state = ENTER_QTY
        
        #check if quantity less than expected                                   
        elif self._picked_quantity <= self._expected_quantity and not self._partial:

            #if already doing a short product, then confirm spoken quantity
            if self._short_product:
                if prompt_yes_no(itext("selection.pick.prompt.short.product.verify",  self._picked_quantity)):
                    self._short_product = False
                    self._set_short_product(self._picked_quantity)
                else:
                    self.next_state = ENTER_QTY
            
            
            #not doing short, but quantity less than expected, ask if short
            elif self._picked_quantity < self._expected_quantity:
                self._short_product = prompt_yes_no(itext("selection.pick.prompt.pick.quantity.less",  
                                                          self._picked_quantity, 
                                                          self._expected_quantity ),
                                                    True, 6)
                if self._short_product:
                    self._short_product = False
                    self._set_short_product(self._picked_quantity)
                else:
                    self.next_state = ENTER_QTY
                    
        #quantity 0 and partial spoken
        elif self._picked_quantity == 0 and self._partial:
            prompt_only(itext('selection.pick.prompt.pick.quantity.zero'))
            self.next_state = ENTER_QTY
            self._partial = False
        
    #---------------------------------------------------------   
    def lot_tracking(self):
        '''Lot tracking'''
        if self._picked_quantity > 0 and self._picks[0]['captureLot']:
            self._lot_quantity=0
            self.launch(obj_factory.get(LotTrackingTask,
                                          self._region, self._assignment_lut,
                    self._picks, self._picked_quantity, 
                    self.taskRunner, self))
        else:
            self._lot_quantity = self._picked_quantity
         
    #---------------------------------------------------------   
    def intialize_puts(self):
        ''' initialize variables for putting. always done even if no put prompt required '''

        #build put list
        assignmentId = None
        self._curr_container = None
        self._puts = []
        self._put_quantity = 0
        
        for pick in self._picks:
            if pick['status'] != 'P': #Non picked picks
                #check if first pick, or matches first pick
                if assignmentId is None or assignmentId == pick['assignmentID']:
                    if assignmentId is None: 
                        assignmentId = pick['assignmentID']
                    self._puts.append(pick)
                    self._put_quantity += (pick['qtyToPick'] - pick['qtyPicked'])

        #adjust put quantity if greater then lot quantity
        if self._put_quantity > self._lot_quantity:
            self._put_quantity = self._lot_quantity

        #get current assignment record for puts
        self._curr_assignment = None
        for assignment in self._assignment_lut:
            if assignment['assignmentID'] == assignmentId: 
                self._curr_assignment = assignment
                break

        #not picking to containers, no put prompt needed
        if self._region['containerType'] == 0:
            self.next_state = WEIGHT_SERIAL

        #no quantity to put
        if self._put_quantity <= 0:
            self.next_state = WEIGHT_SERIAL

    #---------------------------------------------------------   
    def put_prompt(self):
        '''Put Prompt'''

        expected_value = []
        scan_value = ''     
        
        #Do put prompt if multiple assignment or multiple open containers
        if (self._assignment_lut.has_multiple_assignments() or 
            self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID'])):

            open_containers = self._container_lut.get_open_containers(self._curr_assignment['assignmentID'])

            # determine what the expected container value is
            if (self._puts[0]['targetContainer'] != 0):
                if ((self._curr_assignment['activeTargetContainer'] == self._puts[0]['targetContainer']) or
                    (len(open_containers) == 1)):
                    expected_value.append(open_containers[0]['spokenValidation'])
                    scan_value = open_containers[0]['scannedValidation']
                    
            elif (len(open_containers) == 1):
                expected_value.append(open_containers[0]['spokenValidation'])
                scan_value = open_containers[0]['scannedValidation']
                
                
            #indicates at put prompt
            dynamic_vocab = {}
            if not self._partial and self._assignment_lut.has_multiple_assignments():
                dynamic_vocab['partial'] = False
                
            #determine prompts
            if self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID']):
                prompt = itext('selection.put.prompt.for.container')
            elif self._partial:
                prompt = itext('selection.put.prompt.for.container')
            else:
                prompt = itext('selection.put.prompt.for.container.multiple', 
                                    self._put_quantity, self._curr_assignment['position'])

            result, scanned = prompt_alpha_numeric_suggested(prompt,
                                                   itext('selection.put.prompt.for.container.help'),
                                                   expected_value, 
                                                   scan_value,
                                                   self._region['spokenContainerLen'],
                                                   self._region['spokenContainerLen'],
                                                   False, True, 
                                                   dynamic_vocab)


            if result == 'partial':
                self.next_state = PUT_PROMPT
                if self._validate_partial(self._put_quantity, True):
                    self._get_partial_quantity()
            else:
                if not self._validate_container(result, scanned):
                    self.next_state = PUT_PROMPT
        else:
            #should only be 1 open container for assignment
            self._validate_container(None, False)
         
    #----------------------------------------------------------`
    def weight_and_serial_numbers(self):
        if (self._put_quantity > 0):
            if self._picks[0]['variableWeight'] or self._picks[0]['serialNumber']:
                self.launch(obj_factory.get(WeightsSerialNumbers,
                                              int(self._put_quantity), 
                                              self._picks, 
                                              self.taskRunner, self))
        
    #----------------------------------------------------------
    def xmit_picks(self):
        '''Transmit Pick'''
         
        pick = None
        weight = None
        serial_number = None
        containerID = None
        
        #1. Find a pick to transmit
        for p in self._puts:
            if p['status'] != 'P':
                pick = p
                break

        #should not occur
        if pick is None:
            raise Exception('Unknown Error transmitting picks')
         
        #2. Determine quantity to apply, and weight and serial number, and container
        quantity = pick['qtyToPick'] - pick['qtyPicked']
        if quantity > self._put_quantity:
            quantity = self._put_quantity
            
        if quantity > 0 and (len(self._weights) > 0 or len(self._serial_numbers) > 0):
            quantity = 1
            if len(self._weights) > 0:
                weight = "%.2f" % self._weights[0]
            if len(self._serial_numbers) > 0:
                serial_number = self._serial_numbers[0]

        if self._curr_container is not None:
            containerID = self._curr_container['systemContainerID']
        
        #3. check if pick is complete (still more quantity to apply, but not applying all now)
        complete = True
        if self._picked_quantity > quantity and quantity < pick['qtyToPick'] - pick['qtyPicked']:
            complete = False
        elif self._partial and quantity < pick['qtyToPick'] - pick['qtyPicked']:
            complete = False

        #if pick isn't complete, and there is no quantity
        # then simply return and continue to next state
        # there is no reason to send Picked information
        if not complete and quantity <= 0:
            return
            
        #4. Transmit pick record
        result = self._picked_lut.do_transmit(self._curr_assignment['groupID'], 
                                              pick['assignmentID'], 
                                              pick['locationID'], 
                                              quantity, 
                                              int(complete), 
                                              containerID, 
                                              pick['sequence'], 
                                              self._lot_number,
                                              weight, 
                                              serial_number)
    
        #5. Check results and update variables 
        if result < 0: # Error contacting host
            self.next_state = XMIT_PICKS 
        elif result > 0: # Error from host
            self.next_state = XMIT_PICKS
        else: #success
            #update pick information
            pick['qtyPicked'] = pick['qtyPicked'] + quantity
            if complete:
                pick['status'] = 'P'
                
            #update pick quantities
            self._put_quantity -= quantity
            self._lot_quantity -= quantity
            self._picked_quantity -= quantity
            self._expected_quantity -= quantity
            if len(self._weights) > 0:
                self._weights.pop(0)
            if len(self._serial_numbers) > 0:
                self._serial_numbers.pop(0)
            
    #------------------------------------------------------------                    
    def check_partial(self):
        ''' check if partial and close and open containers '''
        if self._partial and self._put_quantity <= 0:
            self._partial = False
            if not self._container_lut.multiple_open_containers(self._curr_assignment['assignmentID']):
                self._lot_quantity = 0
                self.launch(obj_factory.get(NewContainer,
                                              self._region,
                                              self._curr_assignment, 
                                              self._puts,
                                              self._container_lut,
                                              self._assignment_lut.has_multiple_assignments(),
                                              self._curr_container,
                                              self.taskRunner, self))

    #------------------------------------------------------------                    
    def check_close_target_container(self):
        target = self._puts[0]['targetContainer']
      
        #check if target containers, return if not
        if target == 0:
            return 
        
        #check if all picks picked and shorts/go back for shorts    
        for pick in self.callingTask._picks_lut:
            # if pick in same container and not picked (or shorted with go back for short) then 
            # return without closing
            if pick['targetContainer'] == target:
                if pick['status'] != 'P':
                    return
                elif pick['qtyToPick'] - pick['qtyPicked'] > 0 and self._region['goBackForShorts'] != 0:
                    return
            
        #if here, then we need to close the target container
        self.launch(obj_factory.get(CloseContainer,
                                      self._region,
                                      self._curr_assignment, 
                                      self._puts,
                                      self._container_lut,
                                      self._assignment_lut.has_multiple_assignments(),
                                      False,
                                      self._curr_container,
                                      self.taskRunner, self))
    
    #------------------------------------------------------------                    
    def next_step(self):
        ''' determine next step '''
        if self._put_quantity > 0:
            self.next_state = XMIT_PICKS #Still more to transmit
        elif self._lot_quantity > 0:
            self.next_state = INTIALIZE_PUT #Still more to put
        elif self._picked_quantity > 0 or self._expected_quantity > 0:
            self._lot_number = None

            #Re-prompt for remaining picks
            if(self._region['pickPromptType'] == '2'):
                self.next_state = ENTER_QTY
            else:
                self.next_state = SLOT_VERIFICATION
                
        else:                       #Short remaining picks
            self._lot_number = None
            for pick in self._picks:
                if pick['status'] != 'P':
                    self.next_state = INTIALIZE_PUT
                    break

    #------------------------------------------------------------                    
    def cycle_count(self):
        if self._picks[0]['cycleCount']:
            self.launch(obj_factory.get(CycleCountCountItemTask,
                                          self._picks[0]['locationID'],
                                          False,
                                          self._picks[0]['checkDigits'],
                                          False,
                                          self.taskRunner,
                                          self),
                        '')
    
    #------------------------------------------------------------                    
    def _set_short_product(self, quantity):
        self._expected_quantity = quantity
        self._picked_quantity = quantity
        self._lot_quantity = quantity
        self._put_quantity = quantity
        
    #------------------------------------------------------------
    def _verify_product_slot(self, result, is_scanned):
        '''Verifies product/slot depending on spoken check digit or spoken/scanned pvid or ready spoken'''
        #if ready is spoken and check digits is not blank prompt for check digit
        if result == 'ready' and self._picks[0]["checkDigits"] != '':
            prompt_only(itext('selection.pick.prompt.speak.check.digit'), True)
            self.next_state = SLOT_VERIFICATION
        #if pvid is scanned verify it matches the scanned product id
        elif is_scanned:
            if self._picks[0]["scannedProdID"] != result:
                prompt_only(itext('generic.wrongValue.prompt', result))
                self.next_state = SLOT_VERIFICATION
        #if check digit is spoken and both pvid and check digits are same prompt for identical short product
        elif result == self._picks[0]["checkDigits"] and self._pvid == result:
            if prompt_yes_no(itext('selection.pick.prompt.identical.product.short.product')):
                self._set_short_product(0)
                self.next_state = LOT_TRACKING
        #if ready or check digits  is spoken  and pvid is not blank prompt for short product
        elif result in [self._picks[0]["checkDigits"], 'ready'] and (self._pvid != '' or self._picks[0]["scannedProdID"] != ''):
            if prompt_yes_no(itext("selection.pick.prompt.short.product")):
                self._set_short_product(0)
                self.next_state = LOT_TRACKING
            else:
                prompt_only(itext('selection.pick.prompt.wrong.pvid'))
                self.next_state = SLOT_VERIFICATION

    #------------------------------------------------------------
    def _skip_slot(self):
        #check if region allows
        if not self._region['skipSlotAllowed']:
            prompt_only(itext('generic.skip.slot.notallowed'))
            return

        location_id = self._picks[0]['locationID']
        allowed = False
        pass_status = 'N'
        skip_status = 'S'
        #if not pick by pick, then check if another slot and pass
        if not self._region['pickByPick']:
            for pick in self.callingTask._picks_lut:
                if (pick['status'] not in ['P', 'X'] 
                    and pick['locationID'] != location_id): 
                    allowed = True
    
                if pick['status'] == 'B': #must be in base item pass
                    pass_status = 'B'
                    skip_status = 'N'
            
            #Last slot, not allowed to skip
            if not allowed:
                prompt_only(itext('generic.skip.slot.last'))
                return

        #confirm skip slot
        if prompt_yes_no(itext('generic.skip.slot.confirm')):
            for pick in self.callingTask._picks_lut:
                if pick['status'] == pass_status:
                    if pick['locationID'] == location_id:
                        pick['status'] = skip_status
            
            
            #send update status
            update_status = VoiceLinkLutOdr('prTaskLUTUpdateStatus', 
                                            'prTaskODRUpdateStatus', 
                                            self._region['useLuts'])
            
            while update_status.do_transmit(self._assignment_lut[0]['groupID'],
                                            self._picks[0]['locationID'],
                                            '0', skip_status) != 0:
                pass
            
            #End the pick prompt
            self.next_state = '' 
    
    #------------------------------------------------------------
    def _validate_partial(self, expected_qty, from_put_prompt = False):
        self._partial = False
        
        #check if picking to containers
        if self._region['containerType'] == 0:
            prompt_only(itext('selection.pick.prompt.partial.err.no.container'), False)
        
        #multiple pick prompt, and not at quantity prompt
        elif self._region['pickPromptType'] == '2' and self.current_state in [SLOT_VERIFICATION, CASE_LABEL_CD]:
            prompt_only(itext('selection.pick.prompt.partial.nothere.1'))

        #quantity verification on and not at quantity prompt
        elif self._region["qtyVerification"] == "1" and self.current_state in [SLOT_VERIFICATION, CASE_LABEL_CD]:
            prompt_only(itext('selection.pick.prompt.partial.nothere.2'))

        #check if target containers
        elif self._picks[0]['targetContainer'] != 0:
            prompt_only(itext('selection.pick.prompt.partial.err.target'), False)

        #check current expected quantity
        elif expected_qty == 1:
            prompt_only(itext('selection.pick.prompt.partial.err.qtyone'), False)
        
        #if multiple assignments, partial only allowed at put prompt
        elif self._assignment_lut.has_multiple_assignments() and not from_put_prompt:
            prompt_only(itext('selection.pick.prompt.partial.err.putprompt'), False)
        
        #prompt user to confirm partial command
        elif prompt_yes_no(itext('selection.pick.prompt.partial.confirm'), False):
            self._short_product = False
            self._partial = True

        return self._partial
    
    #----------------------------------------------------------
    def _get_partial_quantity(self):
        got_qty = False
        while not got_qty:
            qty = prompt_digits(itext('selection.pick.prompt.partial.quantity'), 
                                itext('selection.pick.prompt.put.quantity.help'), 
                                1, len(str(self._put_quantity)), 
                                False, False)
            qty = int(qty)
            if qty > self._put_quantity:
                prompt_only(itext('selection.pick.prompt.pick.quantity.greater', 
                                       qty, self._put_quantity))
            elif qty == 0:
                got_qty = True
                self._partial = False
            elif qty == self._put_quantity:
                got_qty = True
                self._partial = False
            else:
                got_qty = True
                self._put_quantity = qty

    #----------------------------------------------------------
    def _validate_container(self, containerId, scanned):
        '''validate spoken containers or scanned container is correct'''
        
        #check for single open container
        if containerId == None:
            containers = self._container_lut.get_open_containers(self._curr_assignment['assignmentID'])
            if len(containers) > 0:
                self._curr_container = containers[0]
        
        elif self._puts[0]['targetContainer'] == 0:
            open_container = self._container_lut.match_open_container(containerId, 
                                                                      self._curr_assignment['assignmentID'], 
                                                                      scanned)
            if open_container is None:
                prompt_only(itext('selection.put.prompt.wrong.container', containerId))
                return False
            else:
                self._curr_container = open_container

        elif self._puts[0]['targetContainer'] > 0:
            open_container = self._container_lut.match_open_container(containerId, 
                                                                      self._curr_assignment['assignmentID'], 
                                                                      scanned)
            if (open_container is None or 
                open_container['targetConatiner'] != self._curr_assignment['activeTargetContainer']):
                prompt_only(itext('selection.put.prompt.wrong.container', containerId))
                return False
            else:
                self._curr_container = open_container
            
        return True

    #----------------------------------------------------------
    def _validate_short_product(self):

        #multiple pick prompt, and not at quantity prompt
        if self._region['pickPromptType'] == '2' and self.current_state in [SLOT_VERIFICATION, CASE_LABEL_CD]:
            prompt_only(itext('selection.pick.prompt.short.product.nothere.1'))

        #quantity verification on and not at quantity prompt
        elif self._region["qtyVerification"] == "1" and self.current_state in [SLOT_VERIFICATION, CASE_LABEL_CD]:
            prompt_only(itext('selection.pick.prompt.short.product.nothere.2'))

        else:
            #Short is valid
            self._short_product= True
            self._partial = False

        return self._short_product
        
