from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_yes_no, prompt_only, prompt_digits, prompt_yes_no_cancel, prompt_ready
from vocollect_core import itext, obj_factory, class_factory
from selection.SelectionLuts import WorkRequestLut
from Globals import change_function
from selection.SharedConstants import GET_ASSIGNMENT_TASK_NAME,\
    GET_ASSIGN_XMIT_PICK, GET_ASSIGN_START, GET_ASSIGN_XMIT_ASSIGNMENT,\
    GET_ASSIGN_PROMPT_REVERSE, GET_ASSIGN_ENTER_WORKID, GET_ASSIGN_XMIT_WORKID,\
    GET_ASSIGN_REVIEW_WORKIDS

###############################################################
# Base Get Assignment Class
###############################################################
class GetAssignmentBase(class_factory.get(TaskBase)):
    ''' Base class for getting assignment. Known extensions are 
        GetAssignmentAutoIssuance, GetAssignmentManualIssuance
    
     Steps:
            1. Start
            2. Transmit Assignment LUT
            3. Prompt for reverse order
            4. Transmit Picks LUT
          
     Parameters
            region_rec - regions record to get assignment for  
            assignment_lut - assignment LUT
            picks_lut - Picks LUT
            taskRunner (Default = None) - Task runner object
            callingTask (Default = None) - Calling task (should be a pick prompt task)
    '''

    def __init__(self, region_lut, assignment_lut, picks_lut, pick_only,
                 taskRunner = None, callingTask = None):
        super(GetAssignmentBase, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = GET_ASSIGNMENT_TASK_NAME


        self._region_config_lut = region_lut
        self._region_config_rec = region_lut.get_current_region_record()
        self._assignment_lut = assignment_lut
        self._picks_lut = picks_lut
    
        self._number_work_ids = 0
        self._inprogress_work = False
        self._pick_only = pick_only
        
        if self._pick_only:
            self.current_state = GET_ASSIGN_XMIT_PICK

        if not hasattr(self._picks_lut, '_picking_order'):
            self._picks_lut._picking_order = 0

        if hasattr(callingTask, '_inprogress_work'):
            self._inprogress_work = callingTask._inprogress_work
            callingTask._inprogress_work = False
        
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        self.addState(GET_ASSIGN_START, self.start)
        self.addState(GET_ASSIGN_XMIT_ASSIGNMENT, self.xmit_assignments)
        self.addState(GET_ASSIGN_PROMPT_REVERSE, self.prompt_reverse)
        self.addState(GET_ASSIGN_XMIT_PICK, self.xmit_picks)

    #----------------------------------------------------------
    def start(self):
        pass
    
    #----------------------------------------------------------
    def xmit_assignments(self):
        ''' transmit get assignment '''
        
        work_ids = ''
        if self._region_config_rec['autoAssign'] == '1':
            work_ids = self._number_work_ids
            
        result = self._assignment_lut.do_transmit(work_ids, 
                                                  self._region_config_rec['assignmentType'])
        if result < 0:
            self.next_state = GET_ASSIGN_XMIT_ASSIGNMENT
        elif result == 2:
            self.next_state = ''
        elif result > 0:
            self.next_state = GET_ASSIGN_START
            
    #----------------------------------------------------------
    def prompt_reverse(self):
        self._picks_lut._picking_order = 0
        if self._region_config_rec['allowReversePicking'] == '1':
            if prompt_yes_no(itext('selection.pick.reverse.order'), True):
                self._picks_lut._picking_order = 1
                prompt_only(itext('selection.get.reverse.order'))
            else:
                prompt_only(itext('selection.get.normal.order'))
                
    #----------------------------------------------------------
    def xmit_picks(self):
        pass_assignment = False
        for assignment in self._assignment_lut:
            if assignment['passAssignment'] == '1':
                pass_assignment = True
        
        result = self._picks_lut.do_transmit(self._assignment_lut[0]['groupID'],
                                                  int(pass_assignment),
                                                  int(self._region_config_rec['goBackForShorts']),
                                                  self._picks_lut._picking_order)
        
        if result < 0:
            self.next_state = GET_ASSIGN_XMIT_PICK
        elif result > 0:
            self.next_state = GET_ASSIGN_START
        elif self._region_config_rec['pickByPick']:
            self._picks_lut.change_status('N')
        elif self._pick_only:
            self._picks_lut.change_status('X', 'N')
            self._picks_lut.change_status('N', 'B')
            self._picks_lut.change_status('N', 'G')
            self._picks_lut.change_status('N', 'S')
        
###############################################################
# Auto Get Assignment Single Assignment
###############################################################
class GetAssignmentAuto(class_factory.get(GetAssignmentBase)):
    ''' Get Assignments using auto issuance
    
     Steps (change from base class):
            1. Start (Overridden)
            2. Transmit Assignment LUT (Overridden)
            3. Prompt for reverse order (Same as Base)
            4. Transmit Picks LUT (Same as Base)
          
     Parameters (Same as base)
    '''
    
    #----------------------------------------------------------
    def __init__(self, region_lut, assignment_lut, picks_lut, pick_only,
                 taskRunner = None, callingTask = None):
        super(GetAssignmentAuto, self).__init__(region_lut, 
                                                assignment_lut, 
                                                picks_lut, 
                                                pick_only,
                                                taskRunner, callingTask)
        #Set task name
        self.name = "getAssignmentAuto"

    #----------------------------------------------------------
    def initializeStates(self):
        super(GetAssignmentAuto, self).initializeStates()
        
    #----------------------------------------------------------
    def start(self):
        ''' check for in-progress work '''
        self._number_work_ids = 1

        if self._inprogress_work:
            self.next_state = GET_ASSIGN_XMIT_ASSIGNMENT
        else:
            max_ids = self._region_config_rec['maxNumberWordID']
            if max_ids > 1:
                self._number_work_ids = 0
                while self._number_work_ids == 0:
                    self._number_work_ids = prompt_digits(itext('selection.get.number.workids'), 
                                                          itext('selection.get.number.workids.help'), 
                                                          1, len(str(max_ids)), 
                                                          False, False,
                                                          {'change function' : False,
                                                           'change region' : False})
                    
                    if self._number_work_ids == 'change function':
                        change_function()
                        self.next_state = GET_ASSIGN_START
                    elif self._number_work_ids == 'change region':
                        self.callingTask.change_region()
                        self.next_state = GET_ASSIGN_START
                    else:
                        self._number_work_ids = int(self._number_work_ids)
                        if self._number_work_ids > max_ids or self._number_work_ids <= 0:
                            prompt_only(itext('selection.get.number.workids.error', max_ids))
                            self._number_work_ids = 0
                        else:
                            start_work = False
                            while not start_work:
                                result = prompt_ready(itext('selection.start.picking'), False,
                                                      {'change function' : False,
                                                       'change region' : False})
                                if result == 'change function':
                                    change_function()
                                elif result == 'change region':
                                    self.callingTask.change_region()
                                else:
                                    start_work = True

    #----------------------------------------------------------
    def xmit_assignments(self):
        ''' transmit assignment request '''
        GetAssignmentBase.xmit_assignments(self)

        if not self._inprogress_work and self.next_state is None:
            max_ids = self._region_config_rec['maxNumberWordID']
            assignments = self._assignment_lut.number_of_assignments()
            if max_ids > 1 and assignments < self._number_work_ids:
                if assignments == 1:
                    prompt_ready(itext('selection.only.available.single', assignments))
                else:
                    prompt_ready(itext('selection.only.available.plural', assignments))
                     

###############################################################
# Manual Get Assignment Single Assignment
###############################################################
class GetAssignmentManual(class_factory.get(GetAssignmentBase)):
    ''' Get Assignments using manual issuance
    
     Steps (change from base class):
            1. Start (Overridden)
            2. Enter work ID (Inserted)
            3. Transmit work ID (Inserted)
            4. Review work ID (Inserted)
            5. Transmit Assignment LUT (Same as Base)
            6. Prompt for reverse order (Same as Base)
            7. Transmit Picks LUT (Same as Base)
          
     Parameters (Same as base)
    '''

    #----------------------------------------------------------
    def __init__(self, region_lut, assignment_lut, picks_lut, pick_only,
                 taskRunner = None, callingTask = None):
        super(GetAssignmentManual, self).__init__(region_lut, 
                                                  assignment_lut, 
                                                  picks_lut, 
                                                  pick_only,
                                                  taskRunner, callingTask)
        #Set task name
        self.name = "getAssignmentManual"

        self._workid = None
        self._workid_scanned = False
        self._work_request_lut = obj_factory.get(WorkRequestLut,
                                                   'prTaskLUTRequestWork')
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        self.addState(GET_ASSIGN_START, self.start)
        self.addState(GET_ASSIGN_ENTER_WORKID, self.enter_workid)
        self.addState(GET_ASSIGN_XMIT_WORKID, self.xmit_workid)
        self.addState(GET_ASSIGN_REVIEW_WORKIDS, self.review_workids)
        self.addState(GET_ASSIGN_XMIT_ASSIGNMENT, self.xmit_assignments)
        self.addState(GET_ASSIGN_PROMPT_REVERSE, self.prompt_reverse)
        self.addState(GET_ASSIGN_XMIT_PICK, self.xmit_picks)

    #----------------------------------------------------------
    def start(self):
        ''' check if getting in-progress work '''
        if self._inprogress_work:
            self.next_state = GET_ASSIGN_XMIT_ASSIGNMENT
    
    #----------------------------------------------------------
    def enter_workid(self):
        ''' enter a work ID '''
        #Determine length of work ID for operator to enter
        self._workid_scanned = True
        min_length = 1
        max_length = 100
        help = itext('selection.get.workid.help')
        if self._region_config_rec['workIDLen'] > 0:
            self._workid_scanned = False
            min_length = self._region_config_rec['workIDLen']
            max_length = min_length
            help=itext('selection.get.workid.help.len', max_length)
        
        #speak or scan work ID
        self._workid, self._workid_scanned = prompt_digits(itext('selection.get.workid'), 
                                                           help, 
                                                           min_length, max_length, True, True,
                                                           {'no more' : False,
                                                            'change function' : False,
                                                            'change region' : False})
        
        #set scanned value if work ID was spoken, but region was set to speak all
        if self._region_config_rec['workIDLen'] <= 0:
            self._workid_scanned = True
        
        if self._workid == 'no more':
            if self._number_work_ids == 0:
                if self._region_config_lut.has_another_region_record():
                    self._assignment_lut.last_error = 2
                    self.next_state = ''
                else:
                    prompt_only(itext('selection.must.select.workid'))
                    self.next_state = GET_ASSIGN_ENTER_WORKID
            else:
                self.next_state = GET_ASSIGN_XMIT_ASSIGNMENT
        elif self._workid == 'change function':
            change_function()
            self.next_state = GET_ASSIGN_ENTER_WORKID
        elif self._workid == 'change region':
            self.callingTask.change_region()
            self.next_state = GET_ASSIGN_ENTER_WORKID
    
    #----------------------------------------------------------
    def xmit_workid(self):
        ''' transmit work id entered '''
        result = self._work_request_lut.do_transmit(self._workid, 
                                                    int(not self._workid_scanned), 
                                                    self._region_config_rec['assignmentType'])
        if result < 0:
            self.next_state = GET_ASSIGN_XMIT_WORKID
        elif result > 0:
            self.next_state = GET_ASSIGN_ENTER_WORKID
    
    #----------------------------------------------------------
    def review_workids(self):
        ''' check and process results of work ID transmit '''
        result = self._work_request_lut[0]['errorCode']
        
        if result == 3: #host says we are done, let it move to xmit assignment
            self._number_work_ids += 1
        
        elif result == 4: #Duplicates returned
            if prompt_yes_no(itext('selection.request.work.multiple', 
                                        len(self._work_request_lut))):
                while True:
                    for dup in self._work_request_lut:
                        prompt_result = 'cancel'
                        while prompt_result == 'cancel':
                            prompt_result = prompt_yes_no_cancel(itext('selection.request.work.select', 
                                                                            dup['workid']))
    
                            if prompt_result == 'yes': #User spoke yes, so retransmit selected ID
                                self._workid = dup['workid']
                                self._workid_scanned = True
                                self.next_state = GET_ASSIGN_XMIT_WORKID
                                return
                            elif prompt_result == 'cancel': #User selected cancel
                                if prompt_yes_no(itext('selection.request.work.cancel')):
                                    self.next_state = GET_ASSIGN_ENTER_WORKID
                                    return
                            
                    prompt_only(itext('selection.request.work.end'))
            else:
                self.next_state = GET_ASSIGN_ENTER_WORKID
        else:
            self._number_work_ids += 1
            if self._number_work_ids < self._region_config_rec['maxNumberWordID']:
                self.next_state = GET_ASSIGN_ENTER_WORKID

