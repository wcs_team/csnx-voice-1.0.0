from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_ready, prompt_only, prompt_yes_no,\
     prompt_words, prompt_float, prompt_alpha_numeric
from vocollect_core import itext, class_factory
from selection.SharedConstants import VAR_WEIGHT_SERIAL_NUM_TASK_NAME,\
    CAPTURE_WEIGHT, CAPTURE_SERIAL_NUMBER, CHECK_MORE

class WeightsSerialNumbers(class_factory.get(TaskBase)):
    ''' Variable weight and serial number entry for Voice Link
    
    Steps:
            1. Check if weight should be entered
            2. Capture a weight
            3. Check if serial number should be entered
            4. Capture serial number
            5. Check is any more weights or serial number should be entered, or if done
            
    Parameters
            quantity - Number of item to enter weight or serial number for
            picks - the current list of picks currently be worked on
            taskRunner (Default = None) - Task runner object
            callingTask (Default = None) - Calling task (should be a pick prompt task)
    '''
    
    #----------------------------------------------------------
    def __init__(self, quantity, picks, taskRunner = None, callingTask = None):
        super(WeightsSerialNumbers, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = VAR_WEIGHT_SERIAL_NUM_TASK_NAME
        self.dynamic_vocab.set_pick_prompt_task(self.name)

        self._quantity_to_capture = quantity
        self._picks = picks
        self._current_capture = 1
        self._weights = []
        self._serial_numbers = []
         
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States'''
        self.addState(CAPTURE_WEIGHT, self.capture_weight)
        self.addState(CAPTURE_SERIAL_NUMBER, self.capture_serial_number)
        self.addState(CHECK_MORE, self.check_more)

    #----------------------------------------------------------
    def capture_weight(self):
        ''' capture a single weight '''
        if not self._picks[0]['variableWeight']:
            return
        
        weight = prompt_float(itext('selection.weights.prompt.weight', self._current_capture, self._quantity_to_capture), 
                              itext('selection.weights.help.weight'), 
                              2, False, False, 
                              {'undo entry' : False,
                               'review last' : False,
                               'which item' : False,
                               'short product' : False})
    
        if not self._process_word(weight):
            try:
                weight = float(weight)
            except:
                weight = 0.0
            
            min = self._picks[0]['variableWeightMin']
            max = self._picks[0]['variableWeightMax']
                
            if weight == 0:
                prompt_only(itext('selection.weights.prompt.weight.zero'), True)
                self.next_state = CAPTURE_WEIGHT
            elif weight < self._picks[0]['variableWeightMin'] or weight > self._picks[0]['variableWeightMax']:
                ready = False
                while not ready:
                    command = prompt_ready(itext('selection.weights.prompt.weight.outofrange', weight), True, 
                                           {'override' : True,
                                            'description' : False})
                    if command == 'description':
                        prompt_only(itext('selection.weights.prompt.weight.description', 
                                               '%s' % str(self._picks[0]['variableWeightMin']), 
                                               '%s' % str(self._picks[0]['variableWeightMax'])), True) 
                    elif command == 'override':
                        self._weights.append(weight)
                        ready = True
                    else:
                        self.next_state = CAPTURE_WEIGHT
                        ready = True
            else:
                self._weights.append(weight)
            
    #----------------------------------------------------------
    def capture_serial_number(self):
        if not self._picks[0]['serialNumber']:
            return
        
        ''' enter a single serial number '''
        result = prompt_alpha_numeric(itext('selection.weights.prompt.serial',
                                            self._current_capture, 
                                            self._quantity_to_capture), 
                                      itext('selection.weights.help.serial'), 
                                      1, 
                                      None, 
                                      False, 
                                      True, 
                                      {'undo entry' : False,
                                       'review last' : False,
                                       'which item' : False,
                                       'short product' : False})
        
        if not self._process_word(result[0]):
            self._serial_numbers.append(result[0])
            
    #----------------------------------------------------------
    def check_more(self):
        ''' check if more weights or serial numbers should be entered, otherwise end entry '''
        if self._current_capture <= self._quantity_to_capture:
            self._current_capture += 1
        
        if self._current_capture <= self._quantity_to_capture:
            self.next_state = CAPTURE_WEIGHT
        else:
            vocab = prompt_ready(itext('selection.weights.complete'), True, 
                                 {'review last' : False, 
                                  'undo entry' : False})
            if not self._process_word(vocab):
                self.callingTask._weights = self._weights
                self.callingTask._serial_numbers = self._serial_numbers
    
    #==========================================================
    #Additional vocab functions
    def _process_word(self, vocab):
        ''' process the additional vocabulary word available
        
        Parameters:
                vocab - vocab word to process
                
        return - True if vocabulary processed, otherwise false
        '''
        #user spoke review last
        if vocab == 'review last':
            self.next_state = self.current_state 

            if self._current_capture <= 1:
                prompt_only(itext('generic.not.valid.at.this.time'), False)
            else:
                item = self._current_capture - 1

                prompt_key = ''
                weight = ''
                serial = ''
                if len(self._weights) > 0 and len(self._serial_numbers) > 0:
                    prompt_key = 'selection.weights.prompt.review.weight.and.serial'
                    weight = str(self._weights[item-1])
                    serial = str(self._serial_numbers[item-1])
                elif len(self._weights) > 0:
                    prompt_key = 'selection.weights.prompt.review.weight.only'
                    weight = str(self._weights[item-1])
                elif len(self._serial_numbers) > 0:
                    prompt_key = 'selection.weights.prompt.review.serial.only'
                    serial = str(self._serial_numbers[item-1])
                
                prompt_ready(itext(prompt_key, 
                                   str(item),
                                   weight,
                                   serial), 
                             True)
            
            return True
        
        #user spoke undo entry
        elif vocab == 'undo entry':
            self.next_state = self.current_state 
            if self._current_capture <= 1:
                prompt_only(itext('generic.not.valid.at.this.time'), False)
            else:
                entered = False
                while not entered:
                    command = prompt_words(itext('selection.weights.prompt.undo'), True, 
                                           {'all entries' : False, 
                                            'last entry' : False, 
                                            'continue' : False})
                    if command == 'continue':
                        entered = True
                    elif command == 'all entries':
                        if prompt_yes_no(itext('selection.weights.prompt.undo.all'), True):
                            entered = True
                            del self._weights[:]
                            del self._serial_numbers[:]
                            self._current_capture = 1
                            self.next_state = CAPTURE_WEIGHT 
                    elif command == 'last entry':
                        if prompt_yes_no(itext('selection.weights.prompt.undo.last',
                                                    str(self._current_capture - 1)), True):
                            entered = True
                            self._current_capture -= 1
                            if self._current_capture == 1:
                                del self._weights[:]
                                del self._serial_numbers[:]
                            else:
                                if len(self._weights) > 0:
                                    self._weights = self._weights[0:self._current_capture - 1]
                                if len(self._serial_numbers) > 0:
                                    self._serial_numbers = self._serial_numbers[0:self._current_capture - 1]
                                    
                            self.next_state = CAPTURE_WEIGHT 
                
            return True
        
        #user spoke short product
        elif vocab == 'short product':
            self.next_state = self.current_state
            if self._picks[0]['captureLot']:
                prompt_only(itext('selection.weights.prompt.short.not.allowed'), True)
            else:
                if prompt_yes_no(itext('selection.weights.prompt.short', 
                                            self._current_capture - 1, self._quantity_to_capture), True):

                    self._current_capture = self._current_capture - 1
                    self._quantity_to_capture = self._current_capture
                    self.next_state = ''

                    self.callingTask._set_short_product(self._current_capture)
                    if len(self._weights) > 0:
                        self._weights = self._weights[0:self._current_capture]
                    if len(self._serial_numbers) > 0:
                        self._serial_numbers = self._serial_numbers[0:self._current_capture]
                    
                    self.callingTask._weights = self._weights
                    self.callingTask._serial_numbers = self._serial_numbers
                    
            return True
        
        #user spoke which item
        elif vocab == 'which item':
            self.next_state = self.current_state
            return True

        #Not a known vocabulary word
        return False 
