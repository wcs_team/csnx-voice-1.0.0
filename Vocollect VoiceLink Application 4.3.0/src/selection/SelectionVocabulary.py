from . import PickPrompt
from vocollect_core.dialog.functions import prompt_only, prompt_yes_no, prompt_ready
from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary
from common.VoiceLinkLut import VoiceLinkLutOdr
from vocollect_core import itext, obj_factory, class_factory
from selection.NewContainer import NewContainer
from selection.CloseContainer import CloseContainer
from selection.SelectionPrint import SelectionPrintTask
from selection.ReviewContents import ReviewContents
from selection.SharedConstants import PICK_ASSIGNMENT_CHECK_NEXT_PICK,\
    PICK_ASSIGNMENT_TASK_NAME, PICK_PROMPT_TASK_NAME, SELECTION_TASK_NAME

class SelectionVocabulary(class_factory.get(DynamicVocabulary)):
    ''' Definition of additional vocabulary used throughout VoiceLink selection
    
    Parameters
            runner - Task runner object
    '''
    
    def __init__(self, runner):
        self.vocabs = {'UPC':              obj_factory.get(Vocabulary,'UPC', self._upc, False),
                       'how much more':    obj_factory.get(Vocabulary,'how much more', self._how_much_more, False),
                       'store number':     obj_factory.get(Vocabulary,'store number', self._store_number, False),
                       'route number':     obj_factory.get(Vocabulary,'route number', self._route_number, False),
                       'item number':      obj_factory.get(Vocabulary,'item number', self._item_number, False),
                       'description':      obj_factory.get(Vocabulary,'description', self._description, False),
                       'location':         obj_factory.get(Vocabulary,'location', self._location, False),
                       'quantity':         obj_factory.get(Vocabulary,'quantity', self._quantity, False),
                       'repeat last pick': obj_factory.get(Vocabulary,'repeat_last_pick', self._repeat_last_pick, False),
                       'repick skips':     obj_factory.get(Vocabulary,'repick skips', self._repick_skips, False),
                       'new container':    obj_factory.get(Vocabulary,'new container', self._new_container, False),
                       'close container':  obj_factory.get(Vocabulary,'close container', self._close_container, False),
                       'reprint labels':   obj_factory.get(Vocabulary,'reprint_labels', self._reprint_labels, False),
                       'review contents':  obj_factory.get(Vocabulary,'review_contents', self._review_contents, False),
                       'review cluster':   obj_factory.get(Vocabulary,'review_cluster', self._review_cluster, False),
                       'pass assignment':  obj_factory.get(Vocabulary,'pass assignment', self._pass_assignment, False)
                       }

        self.runner = runner

        #previous pick information
        self.previous_picks = []
        self.pick_tasks = []
        self.clear()

        self._pass_inprogress = False
        
    def clear(self):
        ''' clears current information '''
        self.region_config_rec = None
        self.pick_lut = None
        self.assignment_lut = None
        self.current_picks = []
        
    def new_assignment(self, config_rec, assignment_lut, pick_lut, container_lut):
        ''' Called when a new assignment is started to initialize information
        
        Parameters: 
                config_ref - regions configuration record
                assignment_lut - assignment LUT that is being picked
                pick_lut - Pick LUT that is being picked
                container_lut - Container LUT that is being picked
        '''
        self.clear()
        self.region_config_rec = config_rec
        self.assignment_lut = assignment_lut
        self.pick_lut = pick_lut
        self.container_lut = container_lut
        self._pass_inprogress = False
        if assignment_lut is not None: 
            for assignment in assignment_lut:
                if assignment['passAssignment'] == '1':
                    self._pass_inprogress = True
        
    def next_pick(self, picks):
        ''' Called when switching to a new pick
        
        Parameters:
            picks - list of picks for the current pick
        '''

        #save current picks as previous picks 
        #only if something was picked
        if len(self.current_picks) == 0:
            self.previous_picks = self.current_picks
        else:
            for pick in self.current_picks:
                if pick['status'] == 'P':
                    self.previous_picks = self.current_picks
                    break

        self.current_picks = picks
        if self.current_picks is None:
            self.current_picks = []
        
    def set_pick_prompt_task(self, task):
        ''' set task name as a pick prompt task '''
        if task not in self.pick_tasks:
            self.pick_tasks.append(task)
                         
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''
        current_task = self.runner.get_current_task().name
        
        #available while picking
        if vocab == 'store number':
            return len(self.current_picks) > 0
        
        elif vocab == 'route number':
            return len(self.current_picks) > 0

        elif vocab == 'how much more':
            return len(self.current_picks) > 0

        elif vocab == 'repeat last pick':
            return len(self.current_picks) > 0 or len(self.previous_picks) 
        
        elif vocab == 'new container':
            return len(self.current_picks) > 0
                                
        elif vocab == 'close container':
            #operator not allowed to close container after entering a quantity
            after_qty_prompt = False
            pick_prompt = self.runner.findTask(PICK_PROMPT_TASK_NAME)
            if pick_prompt is not None:
                after_qty_prompt = (pick_prompt.current_state not in [PickPrompt.SLOT_VERIFICATION,
                                                                     PickPrompt.CASE_LABEL_CD,
                                                                     PickPrompt.ENTER_QTY,
                                                                     PickPrompt.QUANTITY_VERIFICATION])

            return (len(self.current_picks) > 0 
                    and self.region_config_rec['containerType'] != 0
                    and not after_qty_prompt)
        
        elif vocab == 'reprint labels':
            return len(self.current_picks) > 0
               
        elif vocab == 'repick skips':
            return len(self.current_picks) > 0 
        
        elif vocab == 'review cluster':
            return len(self.current_picks) > 0 

        elif vocab == 'review contents':
            return len(self.current_picks) > 0 
                            
        elif vocab == 'UPC':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)
        
        elif vocab == 'item number':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)

        elif vocab == 'description':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)

        elif vocab == 'location':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)
        
        elif vocab == 'quantity':
            return (len(self.current_picks) > 0 
                    and current_task in self.pick_tasks)
        
        elif vocab == 'pass assignment':
            return len(self.current_picks) > 0 

        return False
    
        
    #Functions to execute words? 
    def _upc(self):
        ''' speak UPC information '''
        if self.current_picks[0]['UPC'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(' '.join(self.current_picks[0]['UPC']))
        return True 

    def _store_number(self):
        ''' speak store number information '''
        if len(self.assignment_lut) > 1 or self.current_picks[0]['store'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(' '.join(self.current_picks[0]['store']))
        return True 

    def _route_number(self):
        ''' speak route number information '''
        if len(self.assignment_lut) > 1 or self.assignment_lut[0]['route'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(' '.join(self.assignment_lut[0]['route']))
        return True 
    
    def _item_number(self):
        ''' speak item number information '''
        if self.current_picks[0]['itemNumber'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_picks[0]['itemNumber'])
        return True 

    def _description(self):
        ''' speak description information '''
        if self.current_picks[0]['description'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_picks[0]['description'].lower())
        return True 
    
    def _location(self):
        prompt_key = 'selection.location.prompt'
        if self.current_picks[0]['preAisle'] =='':
            prompt_key = 'selection.location.prompt.nopreaisle'
            
        if self.current_picks[0]['aisle'] =='':
            prompt_key = 'selection.location.prompt.noaisle'
            
        if self.current_picks[0]['postAisle'] =='':
            prompt_key = 'selection.location.prompt.nopostaisle'    
            
        if self.current_picks[0]['preAisle'] =='' and self.current_picks[0]['aisle'] == '':
            prompt_key = 'selection.location.prompt.nopreaisle.noaisle'
            
        if self.current_picks[0]['preAisle'] =='' and self.current_picks[0]['postAisle'] == '':            
            prompt_key = 'selection.location.prompt.nopreaisle.nopostaisle'
                        
        if self.current_picks[0]['preAisle'] =='' and self.current_picks[0]['aisle'] == '' and self.current_picks[0]['postAisle'] == '':
            prompt_key = 'selection.location.nopreaisle.noaisle.nopostaisle'
        
        ''' speak location information '''
        prompt_only(itext(prompt_key, 
                          self.current_picks[0]['preAisle'],
                          self.current_picks[0]['aisle'],
                          self.current_picks[0]['postAisle'],
                          self.current_picks[0]['slot']))
        return True 
    
    def _how_much_more(self):
        ''' speak how much more '''
        lines = 0
        quantity = 0
        for pick in self.pick_lut:
            if pick['status'] != 'P':
                lines += 1
                quantity += (pick['qtyToPick'] - pick['qtyPicked'])
                
        if lines == 1:
            prompt_only(itext('selection.howmuchmore.singular', quantity, lines))
        else:
            prompt_only(itext('selection.howmuchmore.plural', quantity, lines))
        return True 
    
    def _quantity(self):
        ''' check if at pick or put prompt and speaks quantity '''
        
        current_task = self.runner.get_current_task()
        put_prompt = current_task.current_state == PickPrompt.PUT_PROMPT
        if put_prompt:
            uom = ''
            if self.current_picks[0]['UOM'] != '':
                uom = ' ' + self.current_picks[0]['UOM']
            prompt_only(str(current_task._put_quantity) + uom)
        else:
            qty = 0
            for pick in self.current_picks:
                qty += pick['qtyToPick'] - pick['qtyPicked'] 
            
            prompt_only(itext('selection.quantity', qty, self.current_picks[0]['UOM']))
        
        return True

    def _repeat_last_pick(self):
        ''' speak last pick information '''
        if len(self.previous_picks) > 0:
            picked = 0
            total = 0
            for pick in self.previous_picks:
                picked += pick['qtyPicked']
                total += pick['qtyToPick']
            if self.previous_picks[0]['aisle']:
                prompt_only(itext('selection.repeatlastpick', 
                                   self.previous_picks[0]['aisle'],
                                   self.previous_picks[0]['slot'], 
                                   picked, total))
            else:
                prompt_only(itext('selection.repeatlastpick.no.aisle',                      
                                  self.previous_picks[0]['slot'], 
                                  picked, total))
        else:
            prompt_only(itext('selection.repeatlastpick.notvalid')) 
        
        return True
    
    def _repick_skips(self):
        ''' process the repick skips command '''

        #Repick not allow if in partial, or region that doesn't allow
        pick_task = self.runner.findTask(PICK_PROMPT_TASK_NAME)
        if ((self.region_config_rec['repickSkips'] != '1') or
            (pick_task is not None and pick_task._partial)):
            prompt_only(itext('generic.repick.skips.notallowed'))
            
        elif prompt_yes_no(itext('generic.repick.skips.confirm'), True):
            if self.pick_lut.has_picks(None, ['S']):
                
                for pick in self.pick_lut:
                    if pick['status'] == 'S':
                        pick['status'] = 'N'
                        
                #send update status
                update_status = VoiceLinkLutOdr('prTaskLUTUpdateStatus', 
                                                'prTaskODRUpdateStatus', 
                                                self.region_config_rec['useLuts'])
                while update_status.do_transmit(self.assignment_lut[0]['groupID'],
                                                '',
                                                '2', 'N') != 0:
                    pass
                
                pick_task = self.runner.findTask(PICK_ASSIGNMENT_TASK_NAME)
                if pick_task is not None:
                    pick_task.aisle_direction=''
                    pick_task.pre_aisle_direction=''
                    pick_task.post_aisle_direction=''
                    self.runner.return_to(pick_task.name, 
                                          PICK_ASSIGNMENT_CHECK_NEXT_PICK)
                    
            else:
                prompt_only(itext('generic.repick.skips.noskips'))

        return True
    
    def _new_container(self):
        ''' launch new container task'''
        put_prompt = False
        pick_task = self.runner.findTask(PICK_PROMPT_TASK_NAME)
        if pick_task is not None:
            put_prompt = pick_task.current_state == PickPrompt.PUT_PROMPT

        if self.region_config_rec['containerType'] == 0:
            prompt_only(itext('selection.new.container.not.allowed'))
        elif len(self.assignment_lut) == 1:
            self._launch_new_container(self.assignment_lut[0])
        elif put_prompt:
            self._launch_new_container(pick_task._curr_assignment)
        else:
            prompt_only(itext('selection.new.container.multiple.put.prompt'))
        
        return True
        
    def _close_container(self):
        '''launch close container task'''
        current_task = self.runner.get_current_task()
        close_container_command = True
        current_task.launch(obj_factory.get(CloseContainer,
                                              self.region_config_rec,
                                              self.assignment_lut[0], 
                                              self.current_picks,
                                              self.container_lut,
                                              self.assignment_lut.has_multiple_assignments(),
                                              close_container_command,
                                              None,
                                              current_task.taskRunner, 
                                              current_task), 
                            current_task.current_state)
        
    def _launch_new_container(self, assignment):
        '''launches new container'''
        #get open container if one exists
        containers = self.container_lut.get_open_containers(assignment['assignmentID'])
        container = None
        if len(containers) > 0:
            container = containers[0]
        
        #launch new container
        current_task = self.runner.get_current_task()
        current_task.launch(obj_factory.get(NewContainer,
                                              self.region_config_rec,
                                              assignment, 
                                              self.current_picks,
                                              self.container_lut,
                                              self.assignment_lut.has_multiple_assignments(),
                                              container,
                                              current_task.taskRunner, 
                                              current_task),  
                            current_task.current_state)
        
    def _reprint_labels(self):
        '''launch reprint label task'''
        if self.region_config_rec['printLabels'] == '0' and self.assignment_lut[0]['isChase'] == '0' or \
            self.region_config_rec['printChaseLabels'] == '0' and self.assignment_lut[0]['isChase'] == '1':
            prompt_only(itext('selection.reprintlabel.not.allowed'))
        else:
            if prompt_yes_no(itext('generic.correct.confirm', 'reprint labels')):
                current_task = self.runner.get_current_task()
                current_task.launch(obj_factory.get(SelectionPrintTask,
                                                      self.region_config_rec, 
                                                      self.assignment_lut[0], 
                                                      self.container_lut,  1,
                                                      current_task.taskRunner, current_task), 
                                    current_task.current_state)
        return True


    def _review_cluster(self):
        '''review cluster loops through assignments and reviews'''        
        if self.region_config_rec['containerType'] == 0 or self.assignment_lut.has_multiple_assignments() == False:
            prompt_only(itext('selection.review.cluster.not.valid'))
        else:
            if prompt_yes_no(itext('generic.correct.confirm', 'Review cluster')):
                for assignment in self.assignment_lut:
                    prompt_ready(itext('selection.review.cluster.prompt', assignment['idDescription'], assignment['position']))
                    
        return True
                    
    
    def _review_contents(self):
        '''review contents'''        
        if self.region_config_rec['containerType'] == 0:
            prompt_only(itext('selection.review.content.not.valid'))
        else: 
            if prompt_yes_no(itext('generic.correct.confirm', 'Review contents')):
                current_task = self.runner.get_current_task()
                current_task.launch(obj_factory.get(ReviewContents,
                                                      self.region_config_rec,
                                                      self.assignment_lut[0], 
                                                      self.container_lut,
                                                      current_task.taskRunner, 
                                                      current_task), 
                                    current_task.current_state)
        return True

    def _pass_assignment(self):
        ''' Pass assignment '''
        pick_prompt = self.runner.findTask(PICK_PROMPT_TASK_NAME)
        pick_task = self.runner.findTask(PICK_ASSIGNMENT_TASK_NAME)
        selection_task = self.runner.findTask(SELECTION_TASK_NAME)
        if (not self.region_config_rec['passAllowed']  #Pass not allowed by region
            or (selection_task is not None         #go back pass (not pick by pick mode)
                and selection_task.pick_only 
                and not self.region_config_rec['pickByPick'])
            or self._pass_inprogress               #pass already in progress
            or self.pick_lut.has_picks(None, 'B')  #In base pick pass
            or self.assignment_lut[0]['isChase'] == '1' #chase assignment
            or pick_prompt is not None and pick_prompt._partial # partial in progress
            or pick_task is None):                 #not spoken during picking
            
            prompt_only(itext('selection.pass.not.allowed'))
        
        elif prompt_yes_no(itext('selection.pass.confirm'), True):
            self._pass_inprogress = True
            self.pick_lut.change_status('X', 'N')
            for assignment in self.assignment_lut:
                if self.pick_lut.has_picks(assignment, 'X'):
                    assignment['passAssignment'] = '1'
                else:
                    assignment['passAssignment'] = '0'
                    
            self.runner.return_to(pick_task.name, 
                                  PICK_ASSIGNMENT_CHECK_NEXT_PICK)

        return True