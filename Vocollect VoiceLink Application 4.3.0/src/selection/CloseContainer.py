from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_only, prompt_alpha_numeric 
from vocollect_core import itext, obj_factory, class_factory
from selection.SelectionPrint import SelectionPrintTask
from selection.DeliverAssignment import DeliverAssignmentTask
from selection.SharedConstants import CLOSE_CONTAINER_TASK_NAME,\
    VERIFY_CLOSE_CONTAINER, CLOSE_PROMPT_FOR_CONTAINER, CLOSE_CONTAINER_CLOSE,\
    CLOSE_CONTAINER_PRINT_LABEL, DELIVER, CLOSE_RETURN_AFTER_DELIVERY,\
    PICK_ASSIGNMENT_TASK_NAME, PICK_ASSIGNMENT_CHECK_NEXT_PICK

###############################################
# Close Container Class                       #   
###############################################
class CloseContainer(class_factory.get(TaskBase)):
    '''Process close container
    
     Steps:
            1. Verify Close Container
            2. Prompt for container
            3. Close Container
            4. Print Label
            5. Deliver Closed container
            6. Return after delivery (returns to aisle prompts) 
          
     Parameters
            region - region operator is picking in 
            assignment - assignment that operator is closing a container for
            container_lut - container LUT
            multiple_assignments - is operator working on multiple assignments or not
            picks_lut - Picks LUT
            close_container_command - if close container command is used
            current_container - current container for the assignment
            taskRunner (Default = None) - Task runner object
            callingTask (Default = None) - Calling task (should be a pick prompt task)
    '''
    
    #----------------------------------------------------------

    def __init__(self, region,  assignment, picks_lut, 
                 container_lut,
                 multiple_assignments,
                 close_container_command,
                 current_container,
                 taskRunner = None, callingTask = None):
        super(CloseContainer, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = CLOSE_CONTAINER_TASK_NAME

        self._region = region
        self._assignment = assignment
        self._container_lut = container_lut
        self._multiple_assignments = multiple_assignments
        self._picks = picks_lut
        self._close_container_command = close_container_command
        self._container = current_container
        self._container_id = ''
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        self.addState(VERIFY_CLOSE_CONTAINER, self.verify_close_container)
        self.addState(CLOSE_PROMPT_FOR_CONTAINER, self.prompt_for_container)
        self.addState(CLOSE_CONTAINER_CLOSE, self.close_container)
        self.addState(CLOSE_CONTAINER_PRINT_LABEL, self.print_label)
        self.addState(DELIVER, self.deliver)
        self.addState(CLOSE_RETURN_AFTER_DELIVERY, self.return_after_delivery)
        
    #----------------------------------------------------------
    def verify_close_container(self):
        '''verify close container is allowed or not'''
        if self._close_container_command:
            self.dynamic_vocab = None#if close container command is used set dynamic vocab to None.
            if self._region['containerType'] != 0:
                #if operator is working on multiple assignments close container is not allowed
                if self._multiple_assignments:
                    prompt_only(itext('selection.close.container.not.allowed.multiple.assignments'))
                    self.next_state = ''
                #If picks have target containers close container is not allowed
                elif self._picks[0]['targetContainer'] > 0:
                    prompt_only(itext('selection.close.container.not.allowed.target.containers'))
                    self.next_state = ''
                #If only one open container exists for assignment can't close the only container
                elif self._container_lut.has_containers(self._assignment, 'O'):
                    if not self._container_lut.multiple_open_containers(self._assignment['assignmentID']):
                        prompt_only(itext('selection.close.container.not.allowed.only.container'))
                        self.next_state = ''
                else:
                    self.next_state = ''
            else:
                #container type is not set to pick to containers don't close containers
                prompt_only(itext('selection.close.container.not.allowed'))
                self.next_state = ''
        else:
            self.next_state = CLOSE_CONTAINER_CLOSE
            
    #----------------------------------------------------------
    def prompt_for_container(self):
        '''prompt for container to be closed'''
        #Prompt for container to be closed
        self._container_id, container_scanned = prompt_alpha_numeric(itext('selection.close.container.prompt.for.container'), 
                                              itext('selection.close.container.prompt.for.container.help'), 
                                              self._region['spokenContainerLen'], self._region['spokenContainerLen'],
                                              confirm=True,scan=True)
        valid_container = self._container_lut.valid_container_for_assignment(self._assignment, self._container_id, container_scanned)
        #Verify if the container is a valid container or is already closed
        if not valid_container:
            prompt_only(itext('selection.close.container.not.valid', self._container_id))
            self.next_state = CLOSE_PROMPT_FOR_CONTAINER
        else:
            self._container =  self._container_lut._get_container(self._container_id, container_scanned)
            if self._container['status'] == 'C':
                prompt_only(itext('selection.close.container.already.closed', self._container_id))
                self.next_state = CLOSE_PROMPT_FOR_CONTAINER
            if container_scanned:
                self._container_id = ''
         
            
    #----------------------------------------------------------
    def close_container(self):
        '''close container send up a LUT'''
        #send container LUT
        if self._container != None:
            operation = 1
       
            result = -1
            while result < 0:
                result = self._container_lut.do_transmit(self._assignment['groupID'], 
                                                 self._assignment['assignmentID'], '', self._container['systemContainerID'], self._container_id, operation , '')
        
            if result != 0:
                self.next_state = CLOSE_CONTAINER_CLOSE
           
    #----------------------------------------------------------
    def print_label(self):
        ''' Print labels if region print label is set'''
        if self._region['printLabels'] == '2':
            if  self._container['printed']  == 0:
                self.launch(obj_factory.get(SelectionPrintTask,
                                              self._region, 
                                              self._assignment,  
                                              self._container_lut, 
                                              0, 
                                              self.taskRunner, self))
            
    #----------------------------------------------------------
    def deliver(self):
        '''deliver assignment if region delivery is set to true'''
        if self._region['delivery'] != '2' and self._region['delContWhenClosed']=='1':
            self.launch(obj_factory.get(DeliverAssignmentTask,
                                          self._region,
                                          self._assignment,
                                          self._multiple_assignments,
                                          self.taskRunner, self))
             
    #----------------------------------------------------------
    def return_after_delivery(self):
        ''' return to slot verification if delivered the assignment'''
        if  self._region['delivery'] != '2' and self._region['delContWhenClosed']=='1':
            if self._close_container_command:
                self.task = self.taskRunner.findTask(PICK_ASSIGNMENT_TASK_NAME)
        
                if self.task != None:
                    self.task._aisle_direction = ''
                    self.task._pre_aisle_direction = ''
                    self.task._post_aisle_direction = ''
                    self.taskRunner.return_to(self.task.name, 
                                              PICK_ASSIGNMENT_CHECK_NEXT_PICK)
