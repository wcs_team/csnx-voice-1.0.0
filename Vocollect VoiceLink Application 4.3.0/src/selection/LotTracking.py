from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_yes_no, prompt_ready, prompt_digits, prompt_alpha_numeric
from vocollect_core import itext, class_factory, obj_factory
from common.VoiceLinkLut import VoiceLinkLut, VoiceLinkLutOdr
from selection.SharedConstants import LOT_TRACKING_TASK_NAME, ENTER_LOT,\
    ENTER_QUANTITY, SEND_LOT

class LotTrackingTask(class_factory.get(TaskBase)):
    '''Lot tracking implementation if the capture lot is set
     
    Steps:
         1. Enter Lot Number
         2. Enter Quantity 
         3. Transmit Lot Number
            
    Parameters
            region - region operator is working in
            assignment_lut - assignments currently working on
            picks - the current list of picks currently be worked on
            quantity_expected - The quantity spoken at the pick prompt
            taskRunner (Default = None) - Task runner object
            callingTask (Default = None) - Calling task (should be a pick prompt task)
     
    '''
    
    def __init__(self, region, assignment_lut, picks, quantity_expected, 
                 taskRunner = None, callingTask = None):
         
        #Sets region before TaskBase init
        super(LotTrackingTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = LOT_TRACKING_TASK_NAME

        self.region = region
         
        self._picks = picks
        self._assignment_lut = assignment_lut
        self._expected_quantity = quantity_expected
        self._lot_quantity = 0
        self._lot_number_spoken_no_times_in_row = 0
        self._incorrect_lot_returned_times = 0
        self._previous_lot_spoken=None
        self._lot_number=None
        self._lot_prompt=''

        #LUT's for lot tracking
        self._send_lot_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTSendLot')
        self._failed_lot_lut = obj_factory.get(VoiceLinkLutOdr, 'prTaskLUTFailedLotNumber', 'prTaskODRFailedLotNumber', self.region['useLuts'])
        self._valid_lots_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTValidLots')
         
        self.dynamic_vocab.set_pick_prompt_task(self.name)

        self._next_pick = None
        self._set_variables()
        
    #----------------------------------------------------------
    def _set_variables(self):
        if len(self._picks) > 0:
            self._next_pick = self._picks[0]
            for pick in self._picks:
                if pick['status'] not in ['P']:
                    self._next_pick = pick
                    break
                
            if self._next_pick['lotText'] == '':
                self._lot_prompt = itext('lot.tracking.generic.lot.number')
            else:
                self._lot_prompt = self._next_pick['lotText']
            
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States'''
        self.addState(ENTER_LOT, self.enter_lot_number)
        self.addState(ENTER_QUANTITY, self.enter_quantity)
        self.addState(SEND_LOT, self.send_lot)
       
     
    #--------------------------------------------------
    def enter_lot_number(self):
        '''associating quantity to lot text'''
        
        result = prompt_alpha_numeric(self._lot_prompt,
                                      itext("lot.tracking.lot.text.help"), 
                                      confirm=False, 
                                      scan=True, 
                                      additional_vocab={'description' : False, 
                                                                              'short product' : False})
        if result[0]  == 'description':
            self._request_description()
        elif result[0] == 'short product':
            if prompt_yes_no(itext('lot.tracking.short.product.prompt', self._expected_quantity)) :
                self.callingTask._set_short_product(0)
                self.next_state = ''
            else:
                self.next_state = ENTER_LOT
        else:
            self._lot_number = result[0]
            self.next_state = ENTER_QUANTITY
            
             
    #-----------------------------------------------------
    def enter_quantity(self):
        ''' verify valid quantity spoken '''
        self._lot_quantity = prompt_digits(itext("lot.tracking.lot.quantity",  self._lot_prompt),
                                           itext("lot.tracking.lot.quantity.help"), 
                                           confirm=False)
         
        if int(self._lot_quantity) > int(self._expected_quantity):
            prompt_ready(itext("lot.tracking.quantity.greater", self._lot_quantity, self._expected_quantity))
            self.next_state=ENTER_QUANTITY
     
        self._lot_quantity = int(self._lot_quantity)
              
    #-------------------------------------------------------    
    def send_lot(self):
        ''' send lot information to host ''' 
        result = self._send_lot_lut.do_transmit(self._lot_number, self._lot_quantity,
                                                self._next_pick['assignmentID'],  
                                                self._next_pick['sequence'])
        if result < 0:
            self.next_state = SEND_LOT
        if result > 0:
            #coming through first time
            if self._previous_lot_spoken == None:
                self._previous_lot_spoken= self._lot_number
            if self._lot_number == self._previous_lot_spoken :
                self._lot_number_spoken_no_times_in_row =  self._lot_number_spoken_no_times_in_row + 1
                #same lot failed 3 times send a failed lut
                if self._lot_number_spoken_no_times_in_row == 3:
                    self._failed_lot()
                else:
                    self.next_state = ENTER_LOT   
            else:
                self._lot_number_spoken_no_times_in_row = 1 
                self.next_state = ENTER_LOT
                self._previous_lot_spoken = self._lot_number
               
        if result == 0 :
            number_of_records = len(self._send_lot_lut)
            if len(self._send_lot_lut) > 1:
                for lot in self._send_lot_lut:
                    if prompt_yes_no(' '.join(lot['fullLotNumber'])):
                        self.callingTask._lot_number = lot['fullLotNumber']
                        self.callingTask._lot_quantity = self._lot_quantity
                        self._incorrect_lot_returned_times = 0
                        break;
                    else:
                        #said no to all the 3 records
                        number_of_records = number_of_records -1
                        if number_of_records == 0:
                            self.next_state = ENTER_LOT
                            
                            if self._previous_lot_spoken == None:
                                self._previous_lot_spoken= self._lot_number
                                
                            #same lot failed 3 times send a failed lut   
                            if self._lot_number == self._previous_lot_spoken:
                                self._incorrect_lot_returned_times = self._incorrect_lot_returned_times + 1
                                if self._incorrect_lot_returned_times == 3:
                                    self._failed_lot()
                            else:
                                self._incorrect_lot_returned_times = 1
                                self._previous_lot_spoken = self._lot_number
            else:
                self.callingTask._lot_number = self._send_lot_lut[0]['fullLotNumber']
                self.callingTask._lot_quantity = self._lot_quantity
     
    #------------------------------------------------------                
    def _failed_lot(self):
        '''send failed lut'''
        result = self._failed_lot_lut.do_transmit(self._assignment_lut[0]["groupID"],
                                                  self._next_pick['assignmentID'], 
                                                  self._next_pick['sequence'],  
                                                  self._lot_number)
        if result != 0:
            self._failed_lot()
        else:
            self.callingTask._set_short_product(0)
            self.next_state = ''
                   
    #------------------------------------------------------    
    def _request_description(self):
        ''' request description'''
        #request description
        result = self._valid_lots_lut.do_transmit(self._assignment_lut[0]["groupID"], 
                                                  self._next_pick['assignmentID'], 
                                                  self._next_pick['locationID'], 
                                                  self._next_pick['itemNumber'],  
                                                  self._next_pick['sequence'])
        
        if result == 0: 
            for lot in self._valid_lots_lut:
                next_description = prompt_ready(lot['ID'], False, {'stop':False})
                if next_description == 'stop':
                    break
        
        self.next_state =  ENTER_LOT
