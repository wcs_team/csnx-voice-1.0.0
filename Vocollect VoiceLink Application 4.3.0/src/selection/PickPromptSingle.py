from vocollect_core.dialog.functions import prompt_digits, prompt_digits_required , prompt_only

from vocollect_core import itext, class_factory
from selection.PickPrompt import PickPromptTask, SLOT_VERIFICATION, ENTER_QTY,\
    QUANTITY_VERIFICATION, LOT_TRACKING


#----------------------------------------------------------
#Single pick prompt task
#----------------------------------------------------------
class PickPromptSingleTask(class_factory.get(PickPromptTask)):
    '''Pick prompts for regions with single pick prompt defined
     Extends PickPromptTask
     
     Steps:
            1. Slot verification (Overridden)
            2. Enter Quantity (Overridden)
            
            Remaining steps are the same as base
          
     Parameters
             Same as base class
    '''
    
    #----------------------------------------------------------
    def slot_verification(self):
        '''override for Single Prompts''' 
        
        #if pick prompt type is 1 prompt only slot assuming they are picking just 1
        additional_vocabulary={'short product' : False, 
                               'ready' : False, 
                               'skip slot' : False,
                               'partial' : False}
      
        if self._region['pickPromptType'] == '1' and self._expected_quantity == 1:
            prompt = itext("selection.pick.prompt.single.slot.only", 
                                self._picks[0]["slot"],
                                self._uom, self._description, self._id_description, self._message)
        else:
            prompt = itext("selection.pick.prompt.single.pick.quantity", 
                                self._picks[0]["slot"],
                                self._expected_quantity, self._uom,  self._description, self._id_description, self._message)
   
        result, is_scanned = prompt_digits_required(prompt,
                                                    itext("selection.pick.prompt.checkdigit.help"), 
                                                    [self._picks[0]["checkDigits"], self._pvid], 
                                                    [self._picks[0]["scannedProdID"]], 
                                                    additional_vocabulary,
                                                    self._skip_prompt)
      
        self._skip_prompt = False
        if result == 'short product':
            self.next_state = SLOT_VERIFICATION
            self._validate_short_product()
            prompt_only(itext('selection.pick.prompt.check.digit'), True)
            self._skip_prompt = True #don't repeat main prompt
                
        elif result == 'partial':
            self.next_state = SLOT_VERIFICATION
            self._validate_partial(self._expected_quantity)
            prompt_only(itext('selection.pick.prompt.check.digit'), True)
            self._skip_prompt = True #don't repeat main prompt

        elif result == 'skip slot':
            self.next_state = SLOT_VERIFICATION
            self._skip_slot()
        else:
            self._verify_product_slot(result, is_scanned)
            
    
    #---------------------------------------------------------     
    def enter_qty(self):
        '''override to enter quantity to just prompt for quantity'''

        #Quantity verification on, or shorting, or partial, or qty verification failed        
        if (self._region["qtyVerification"] == "1"
              or self.previous_state in [QUANTITY_VERIFICATION, ENTER_QTY] #Qty verification must of failed, so reprompt quantity
              or self._short_product 
              or self._partial):
            
            additional_vocabulary={'skip slot' : False, 'partial' : False}
            
            if self._short_product:
                prompt = itext('selection.pick.prompt.short.product.quantity')
            elif self._partial:
                prompt = itext('selection.pick.prompt.partial.quantity')
            else:
                prompt = itext("selection.pick.prompt.single.prompt.quantity")

            result = prompt_digits(prompt,
                                   itext("selection.pick.prompt.pick.quantity.help"),
                                   1, len(str(self._expected_quantity)), 
                                   False, False,
                                   additional_vocabulary, hints=[str(self._expected_quantity)])
        
            if result == 'skip slot':
                self.next_state = ENTER_QTY
                self._skip_slot()
            elif result == 'partial':
                self.next_state = ENTER_QTY
                self._validate_partial(self._expected_quantity, False)
            else:
                self._picked_quantity = int(result)
                if self._picked_quantity == self._expected_quantity:
                    self._partial = False
                    
        #No prompting assume expected quantity.
        else:
            self._picked_quantity = self._expected_quantity
            self.next_state=LOT_TRACKING  
