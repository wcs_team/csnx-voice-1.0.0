from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_only, prompt_alpha_numeric, prompt_yes_no, prompt_ready
from vocollect_core import itext, class_factory
from selection.SharedConstants import REVIEW_CONTENTS_TASK_NAME,\
    REVIEW_PROMPT_FOR_CONTAINER, XMIT_CONTAINER_REVIEW, REVIEW_CONTAINER
from selection.SelectionLuts import ReviewContentsLut

###############################################
#Review contents class                        #   
###############################################
class ReviewContents(class_factory.get(TaskBase)):
    
    '''process Review contents
    
     Steps:
        1) If there are multiple open or closed containers prompt for container
        2) Transmit review container lut
        3) Prompt operator with the contents received
          
     Parameters
        assignment_lut - assignments currently working on
        container_lut - current container lut
        taskRunner (Default = None) - Task runner object
        callingTask (Default = None) - Calling task (should be a pick prompt task)'''
    
    #----------------------------------------------------------

    def __init__(self, region, assignment, container_lut,
                 taskRunner = None, callingTask = None):
        TaskBase.__init__(self, taskRunner, callingTask)
        super(ReviewContents, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = REVIEW_CONTENTS_TASK_NAME

        self._region = region
        self._assignment = assignment
        self._container_lut = container_lut
        self.container = None
        
        self._review_contents_lut = ReviewContentsLut('prTaskLUTContainerReview')    
   
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        self.addState(REVIEW_PROMPT_FOR_CONTAINER, self.prompt_for_container)
        self.addState(XMIT_CONTAINER_REVIEW, self.xmit_container_review)
        self.addState(REVIEW_CONTAINER, self.review_container)
        
    #----------------------------------------------------------
    def prompt_for_container(self):
        ''' prompt for container '''
        if self._container_lut._has_open_closed_containers():
            container_id, container_scanned = prompt_alpha_numeric(itext('selection.close.container.prompt.for.container'), 
                                              itext('selection.close.container.prompt.for.container.help'), 
                                              self._region['spokenContainerLen'], self._region['spokenContainerLen'],
                                              confirm=False,scan=True)
                    
            self.container = self._container_lut._get_container(container_id, container_scanned)
            if self.container == None:
                prompt_only(itext('selection.close.container.not.valid', container_id))
                self.next_state = REVIEW_PROMPT_FOR_CONTAINER
        else:
            # get only available container
            self.container = self._container_lut._get_container_open_closed()
            
    #----------------------------------------------------------
    def xmit_container_review(self):
        ''' review container'''
        result = self._review_contents_lut.do_transmit(self.container['systemContainerID'], self._assignment['groupID'], 
                                             self.container['assignmentID'])
        
        if result != 0:
            self.next_state = ''
        elif not prompt_yes_no(itext('selection.review.contents.for.container', self._review_contents_lut[0]['containerID'])):
            self.next_state = ''
            
    def review_container(self):
        '''review the container'''
        for container_review in self._review_contents_lut:
            result = ''
            while result != 'ready':
                result = prompt_ready(itext('selection.review.contents.prompt', 
                                                  container_review['description'].lower(),  container_review['quantity']),
                                                  False, {'location' : False, 'stop':  False})
                if result == 'location':
                    prompt_only(container_review['location'])
                elif result == 'stop':
                    return;
                
        prompt_only(itext('selection.review.content.complete'))
                        
                 
            
