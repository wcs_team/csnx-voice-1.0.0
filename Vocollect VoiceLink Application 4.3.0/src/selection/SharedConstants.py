#---------------------------------------------------------------
#Begin Assignment
BEGIN_ASSIGNMENT_TASK_NAME                  = 'beginAssignment'

#States - begin assignment
BEGIN_ASSIGN_SUMMARY                        = 'beginAssignmentSummary'
BEGIN_ASSIGN_PRINT                          = 'beginAssignmentPrint'
BEGIN_ASSIGN_BASE_INTIALIIZE                = 'beginAssignmentBaseItemInitialize'
BEGIN_ASSIGN_BASE_START                     = 'beginAssignmentBaseItemStart'
BEGIN_ASSIGN_BASE_NEXT                      = 'beginAssignmentBaseItemNext'
BEGIN_ASSIGN_BASE_PROMPT                    = 'beginAssignmentBaseItemPrompt'
BEGIN_ASSIGN_BASE_PICK                      = 'beginAssignmentBaseItemPick'

#---------------------------------------------------------------
#Close Container
CLOSE_CONTAINER_TASK_NAME                   = 'closeContainer'

#States - close container
VERIFY_CLOSE_CONTAINER                      = 'verifyCloseContainer'
CLOSE_PROMPT_FOR_CONTAINER                  = 'promptForContainer'
CLOSE_CONTAINER_CLOSE                       = 'closeContainerClose'
CLOSE_CONTAINER_PRINT_LABEL                 = 'closeContainerPrintLabel' 
DELIVER                                     = 'deliver'
CLOSE_RETURN_AFTER_DELIVERY                 = 'closeReturnAfterDelivery'

#---------------------------------------------------------------
#Deliver Assignments
DELIVER_ASSIGNMENT_TASK_NAME                = 'deliverAssignment'

#States - Deliver assignment
ASSIGNMENT_DELIVER                          = 'selectionDeliveryAssignmentDeliver'
ASSIGNMENT_DELIVER_LOAD                     = 'selectionDeliveryAssignmentDeliverLoad'
ASSIGNMENT_DELIVER_VERIFY                   = 'selectionDeliveryAssignmentDeliverVerify'
ASSIGNMENT_LOAD                             = 'selectionDeliveryAssignmentLoad'

#---------------------------------------------------------------
#Get Assignment
GET_ASSIGNMENT_TASK_NAME                    = 'getAssignment'

#States - Get Assignment
GET_ASSIGN_START                            = 'getAssignmentStart'
GET_ASSIGN_PROMPT_REVERSE                   = 'promptReverse'
GET_ASSIGN_XMIT_ASSIGNMENT                  = 'xmitAssignment'
GET_ASSIGN_XMIT_PICK                        = 'xmitPicks'
GET_ASSIGN_ENTER_WORKID                     = 'getAssignEnterWordId'
GET_ASSIGN_XMIT_WORKID                      = 'getAssignxMitWordId'
GET_ASSIGN_REVIEW_WORKIDS                   = 'getAssignReviewWordId'

#---------------------------------------------------------------
#Get Container Print Label
GET_CONTAINER_PRINT_LABEL_TASK_NAME         = 'getContainerPrintLabel'

#States - Get Container Print Label
START                                       = 'start'
GET_NEXT_ASSIGNMENT                         = 'getNextAssignment'
CHECK_PICKS                                 = 'checkPicks'
CHECK_CHASE_ASSIGNMENT                      = 'checkChaseAssignment'
NO_PICK_TO_CONTAINER                        = 'noPickToContainer'
PROMPT_FOR_CONTAINER                        = 'promptForContainer'
NO_PRINT_LABEL                              = 'noPrintLabel'
PRE_CREATE_CONTAINERS                       = 'preCreateContainers'
GET_CONTAINER                               = 'getContainer'
NEW_CONTAINER                               = 'newContainer'
PRINT                                       = 'print'
PRINT_RETURN                                = 'printReturn'

#---------------------------------------------------------------
#Lot Tracking
LOT_TRACKING_TASK_NAME                      = 'lotTracking'

#States - Lot Tracking
ENTER_LOT                                   = 'eneterLot'
ENTER_QUANTITY                              = 'enterQuantity'
SEND_LOT                                    = 'lotTrackingSendLot'

#---------------------------------------------------------------
#New Container
NEW_CONTAINER_TASK_NAME                     = 'newContainer'

#States - New container
CONFIRM_NEW_CONTAINER                       = 'confirmNewContainer'
CLOSE_CONTAINER                             = 'closeContainer'
OPEN_CONTAINER                              = 'openContainer'
NEW_RETURN_AFTER_DELIVERY                   = 'newReturnAfterDelivery'

#---------------------------------------------------------------
#Open Container
OPEN_CONTAINER_TASK_NAME                    = 'openContainer'

#States - Open container
OPEN_CONTAINER_OPEN                         = 'openContainerOpen'

#---------------------------------------------------------------
#Pick Assignment
PICK_ASSIGNMENT_TASK_NAME                   = 'pickAssignment'

#States - Pick Assignment
PICK_ASSIGNMENT_CHECK_NEXT_PICK             = 'pickAssignmentCheckNextPick'
VERIFY_LOCATION_REPLEN                      = 'verifyLocationReplenished'
PICK_ASSIGNMENT_PREAISLE                    = 'pickAssignmentPreAisle'
PICK_ASSIGNMENT_AISLE                       = 'pickAssignmentAisle'
PICK_ASSIGNMENT_POSTAISLE                   = 'pickAssignmentPostAisle'
PICK_ASSIGNMENT_PICKPROMPT                  = 'pickAssignmentPickPrompt'
PICK_ASSIGNMENT_END_PICKING                 = 'pickAssignemntEndPicking'

#---------------------------------------------------------------
#Pick Prompt - Multiple and Single
PICK_PROMPT_TASK_NAME                       = 'pickPrompt'

#States - Pick Prompt
CHECK_TARGET_CONTAINER                      = 'checkTargetContainer'
SLOT_VERIFICATION                           = 'slotVerification'
CASE_LABEL_CD                               = 'caseLabelCD'
ENTER_QTY                                   = 'pickPromptEnterQty'
QUANTITY_VERIFICATION                       = 'pickPromptQuantityVerifycation'
LOT_TRACKING                                = 'lotTracking'
INTIALIZE_PUT                               = 'intializePut'
PUT_PROMPT                                  = 'putPrompt'
WEIGHT_SERIAL                               = 'weightsAndSerialNumbers'
XMIT_PICKS                                  = 'pickPromptXmitPicks'
CHECK_PARTIAL                               = 'checkPartial'
CLOSE_TARGET_CONT                           = 'checkCloseTargetContainer'
NEXT_STEP                                   = 'pickPromptNextStep'
CYCLE_COUNT                                 = 'pickPromptCycleCount'

#---------------------------------------------------------------
#Review Contents
REVIEW_CONTENTS_TASK_NAME                   = 'reviewContents'

#States - Review contents
REVIEW_PROMPT_FOR_CONTAINER                 = 'promptForContainer'
XMIT_CONTAINER_REVIEW                       = 'xmitContainerReview'
REVIEW_CONTAINER                            = 'reviewContainer'

#---------------------------------------------------------------
#Selection Print
SELECTION_PRINT_TASK_NAME                   = 'selectionPrint'

#States - Selection Print
ENTER_PRINTER                               = 'enterPrinter'
CONFIRM_PRINTER                             = 'confirmPrinter'
PRINT_LABELS                                = 'printLables'
REPRINT_LABELS                              = 'reprintLables'

#---------------------------------------------------------------
#Selection Task
SELECTION_TASK_NAME                         = 'selection'

#States - Selection Task
REGIONS                                     = 'regions'
VALIDATE_REGION                             = 'validateRegion'
GET_ASSIGNMENT                              = 'getAssignment'
CHECK_ASSIGNMENT                            = 'checkAssignment'
BEGIN_ASSIGNMENT                            = 'beginAssignment'
PICK_ASSIGNMENT                             = 'pickAssignment'
PICK_REMAINING                              = 'pickRemianing'
PRINT_LABEL                                 = 'printLabel'
DELIVER_ASSIGNMENT                          = 'deliverAssignment'
COMPLETE_ASSIGNMENT                         = 'completeAssignment'
PROMPT_ASSIGNMENT_COMPLETE                  = 'promptAssignmentComplete'

#---------------------------------------------------------------
#Variable Weights and Serial Numbers
VAR_WEIGHT_SERIAL_NUM_TASK_NAME             = 'weightsSerialNumbers'

#States - Variable Weights and Serial Numbers
CAPTURE_WEIGHT                              = 'captureWeights'
CAPTURE_SERIAL_NUMBER                       = 'captureSerialNumber'
CHECK_MORE                                  = 'checkMore'
