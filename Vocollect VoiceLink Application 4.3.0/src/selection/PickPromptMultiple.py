from vocollect_core.dialog.functions import prompt_ready 
from vocollect_core.dialog.functions import prompt_digits, prompt_digits_required 

from vocollect_core import itext, class_factory
from selection.PickPrompt import PickPromptTask, SLOT_VERIFICATION, ENTER_QTY


#----------------------------------------------------------
#Multiple pick prompt task
#----------------------------------------------------------
class PickPromptMultipleTask(class_factory.get(PickPromptTask)):
    '''Pick prompts for regions with multiple pick prompt defined
     Extends PickPromptTask
     
     Steps:
            1. Slot verification (Overridden)
            2. Enter Quantity (Overridden)
            
            Remaining steps are the same as base
          
     Parameters
             Same as base class
    '''
    
    #----------------------------------------------------------
    def slot_verification(self):
        '''Multiple Prompts''' 
        result, is_scanned = prompt_digits_required(self._picks[0]["slot"],
                                                    itext("selection.pick.prompt.checkdigit.help"), 
                                                    [self._picks[0]["checkDigits"], self._pvid], 
                                                    [self._picks[0]["scannedProdID"]], 
                                                    {'ready' : False, 'skip slot' : False})
        if result == 'skip slot':
            self.next_state = SLOT_VERIFICATION
            self._skip_slot()
        else:
            self._verify_product_slot(result, is_scanned)
         
    #----------------------------------------------------------
    def enter_qty(self):
        ''' Enter quantity'''

        additional_vocabulary = {'skip slot' : False, 'partial' : False}
            
        #prompt user for quantity
        if self._region["qtyVerification"] == "1" or self._short_product or self._partial:
            if self._short_product:
                prompt = itext('selection.pick.prompt.short.product.quantity')
            elif self._partial:
                prompt = itext('selection.pick.prompt.partial.quantity')
                additional_vocabulary['short product'] = False #Add short product to vocab
            else:
                prompt = itext("selection.pick.prompt.pick.quantity", 
                                    self._expected_quantity, self._uom, self._id_description, self._description, self._message, )
            
            result = prompt_digits(prompt,
                                   itext("selection.pick.prompt.pick.quantity.help"),
                                   1, len(str(self._expected_quantity)), 
                                   False, False,
                                   additional_vocabulary, hints=[str(self._expected_quantity)])
        else:
            additional_vocabulary['short product'] = False #Add short product to vocab
            #If the quantity verification is not set ask for quantity and take what ever they say
            result = prompt_ready(itext("selection.pick.prompt.pick.quantity", 
                                             self._expected_quantity, self._uom,  self._id_description, self._description, self._message), 
                                  False, 
                                  additional_vocabulary)

        #check results
        if result == 'short product':
            self.next_state = ENTER_QTY
            self._short_product = True
            self._partial = False
        elif result == 'partial':
            self.next_state = ENTER_QTY
            self._validate_partial(self._expected_quantity)
        elif result == 'ready':
            self._picked_quantity = self._expected_quantity
        elif result == 'skip slot':
            self.next_state = ENTER_QTY
            self._skip_slot()
        else:
            self._picked_quantity = int(result)
            if self._picked_quantity == self._expected_quantity:
                self._short_product = False
                self._partial = False