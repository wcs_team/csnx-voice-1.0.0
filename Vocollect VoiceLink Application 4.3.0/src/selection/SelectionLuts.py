'''
This module contains custom Selection LUTS that extend the functionality of a 
basic LUT
'''
from vocollect_core.dialog.functions import prompt_ready
from vocollect_core import itext, class_factory
from common.VoiceLinkLut import VoiceLinkLut
from Globals import change_function
from pending_odrs import wait_for_pending_odrs


#Special Error Codes
IN_PROGRESS_SPECIFIC_REGION = 2
IN_PROGRESS_ANOTHER_FUNCTION = 3

###############################################################
# Overridden Region LUT Classes
# EXAMPLE: Extending LUT to handle special error conditions
###############################################################
class ValidRegions(class_factory.get(VoiceLinkLut)):

    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        if error == IN_PROGRESS_SPECIFIC_REGION: #Treat as a warning
            prompt_ready(itext('generic.sayReady.prompt', message))
            return 0
        elif error == IN_PROGRESS_ANOTHER_FUNCTION:
            prompt_ready(itext('generic.sayReady.prompt', message))
            return error
        else:
            return super(ValidRegions, self)._process_error(error, message)
    

class RegionConfig(class_factory.get(VoiceLinkLut)):

    def __init__(self, command):
        super(RegionConfig, self).__init__(command)
        self._current_region_rec = 0
    
    def reset_current(self):
        ''' reset current region record '''
        self._current_region_rec = 0

    def has_another_region_record(self):
        ''' returns true if there is another region record
        after the current one '''
        return self._current_region_rec < len(self) - 1
    
    def next_region_record(self):
        ''' move current record pointer to next record '''
        self._current_region_rec += 1
    
    def get_current_region_record(self):
        ''' returns the current region record '''
        if self._current_region_rec >= 0 and self._current_region_rec < len(self):
            return self[self._current_region_rec]
        else:
            return None
    
    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        if error == IN_PROGRESS_ANOTHER_FUNCTION:
            prompt_ready(itext('generic.sayReady.prompt', message))
            return error
        else:
            return super(RegionConfig, self)._process_error(error, message)
        
###############################################################
# Overridden Assignment LUT class with helper methods
###############################################################
class Assignments(class_factory.get(VoiceLinkLut)):

    def __init__(self, command, task = None):
        super(Assignments, self).__init__(command)
        self.task = task
        self.last_error = 0

    def has_multiple_assignments(self):
        '''verifies if there are  multiple assignments'''
        return self.number_of_assignments() > 1
    
    def number_of_assignments(self):
        '''verifies the number of assignments'''
        count = 0
        for assignment in self.lut_data:
            if assignment['groupID'] != '':
                count += 1
                
        return count

    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        self.last_error = error
        if error == 2: #do nothing here with this error
            return 2
        elif error > 0 and error not in [98, 99]: #allow operator to change function on normal errors
            not_ready = True
            while not_ready:
                result = prompt_ready(itext('generic.continue.prompt', message), False,
                                      {'change function' : False,
                                       'change region' : False}) 
                if result == 'change function':
                    change_function()
                elif result == 'change region':
                    if self.task is not None:
                        self.task.change_region()
                else:
                    not_ready = False
                    
            return error
        else:
            return super(Assignments, self)._process_error(error, message)
    
###############################################################
# Work Request LUT
###############################################################
class WorkRequestLut(class_factory.get(VoiceLinkLut)):
    
    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        if error == 3: #Telling operator no more
            return 0
        elif error == 4: #Telling operator multiple found
            return 0
        else:
            return super(WorkRequestLut, self)._process_error(error, message)
            
###############################################################
# Overridden pick LUT class with helper methods
###############################################################
class Picks(class_factory.get(VoiceLinkLut)):
    
    def picks_match(self, pick1, pick2, combineAssignments = True):
        '''Verifies if picks match comparing location id, UOM , item number, assignment id '''
        if (pick1['locationID'] == pick2['locationID']  
            and pick1['UOM'] == pick2['UOM']
            and pick1['itemNumber'] == pick2['itemNumber']
            and pick1['targetContainer'] == pick2['targetContainer']):
            
            if combineAssignments: 
                return True
            else:
                if pick1['assignmentID'] == pick2['assignmentID']:
                    return True

        return False
    
    def has_picks(self, assignment, status):
        ''' Verifies if an assignment has picks with specified status'''
        for pick in self.lut_data:
            if ((assignment is None or pick['assignmentID'] == assignment['assignmentID']) 
                and pick['status'] in status):
                return True
                
        return False
    
    def change_status(self, to_status, from_status=None):
        ''' change the pick status to a given status'''
        for pick in self.lut_data:
            if pick['status'] != '':
                if from_status == None:
                    pick['status'] = to_status
                else:
                    if pick['status'] == from_status:
                        pick['status'] = to_status 
    
    def has_any_shorts(self):
        ''' has any shorts'''
        for pick in self.lut_data:
            if pick['qtyToPick'] - pick['qtyPicked'] > 0 and pick['status'] in ['P']:
                return True        
        return False
    
    def has_picks_with_status(self, status):
        ''' returns if pick has status'''
        for pick in self.lut_data:
            if pick['status'] == status:
                return True
        return False
        
###############################################################
# Overridden Containers LUT class with helper methods
# EXAMPLE: Extending base LUT to add helper methods
###############################################################
class Containers(class_factory.get(VoiceLinkLut)):
    
    def has_containers(self, assignment, status):
        ''' Verifies if the assignment has containers with the status'''
        for container in self.lut_data:
            if container['assignmentID'] == assignment['assignmentID'] and container['status'] == status:
                return True
             
        return False     
     
    def multiple_open_containers(self, assignmentId):
        '''Verifies if there are multiple containers for specified assignment'''
        count = 0
        for container in self.lut_data:
            if container['assignmentID'] == assignmentId and container['status'] == 'O':
                count = count + 1
                if count == 2:
                    return True
       
        return False
    
    def get_open_containers(self, assignmentId):
        containers = []
        for container in self.lut_data:
            if container['assignmentID'] == assignmentId and container['status'] == 'O':
                containers.append(container)
                
        return containers
         
    def match_open_container(self, container_to_match_id, assignmentId,  is_scanned):
        ''' Matches open container for assignment'''
        for container in self.lut_data:
            if container['assignmentID'] == assignmentId and container['status'] == 'O':
                if is_scanned:
                    if container_to_match_id == container['scannedValidation']:
                        return container
                else:
                    if container_to_match_id == container['spokenValidation']:
                        return container  
        
        return None
    
    def valid_container_for_assignment(self, assignment, container_spoken, is_scanned):
        ''' Verifies if the container is a valid container for assignment based on region setting'''
        for container in self.lut_data:
            if container['assignmentID'] == assignment['assignmentID']:
                if is_scanned:
                    if container['scannedValidation'] == container_spoken:
                        return True
                else:
                    if container['spokenValidation'] == container_spoken:
                        return True    
        return False     
    
    def _get_container(self, container_id, is_scanned):
        ''' container for the valid container they scanned or spoken'''
        for container in self.lut_data:
            if is_scanned:
                if container_id == container['scannedValidation']:
                    return container
            else:
                if container_id == container['spokenValidation']:
                    return container   
        return None
    
    def get_closed_containers(self, assignmentId):
        containers = []
        for container in self.lut_data:
            if container['assignmentID'] == assignmentId and container['status'] == 'C':
                containers.append(container)
        return containers
    
    def _has_open_closed_containers(self):
        '''returns if there are more than one open or closed containers'''
        count = 0
        for container in self.lut_data:
            if container['status'] != 'A':
                count = count + 1
                if count == 2:
                    return True
        return False
    
    
    def _get_container_open_closed(self):
        '''Get the only open or closed container'''
        for container in self.lut_data:
            if container['status'] != 'A':
                return container
        return None
    

################################################################
# Overridden Review Contents LUT to speak different error message
################################################################
class ReviewContentsLut(class_factory.get(VoiceLinkLut)):

    def do_transmit(self, *fields):
        ''' transmit method for LUT
            Parameters:
            *fields - variable number of fields to transmit
            
            return: Returns 0 if no error occurred, otherwise the error code
        '''
        
        #first check if any pending ODRs
        wait_for_pending_odrs('VoiceLink')
        
        result = self._transmit(*fields)
        
        if result < 0:
            message = itext('generic.errorContactingHost.prompt')
            prompt_ready(itext('generic.returnToPicking.prompt', message))
            
        return result
