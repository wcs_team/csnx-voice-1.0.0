from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_ready, prompt_digits_required , prompt_only
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut
from loading.LoadingActions import LoadContainerTask
from selection.SharedConstants import DELIVER_ASSIGNMENT_TASK_NAME,\
    ASSIGNMENT_DELIVER, ASSIGNMENT_DELIVER_LOAD, ASSIGNMENT_DELIVER_VERIFY,\
    ASSIGNMENT_LOAD

class DeliverAssignmentTask(class_factory.get(TaskBase)):
    '''Process close container
    
     Steps:
            1. Deliver confirmation
            2. Direct load container
          
     Parameters
            region - region operator is picking in 
            assignment - assignment that operator is closing a container for
            multiple_assignments - true is there are multiple assignments being worked on
            taskRunner (Default = None) - Task runner object
            callingTask (Default = None) - Calling task (should be a pick prompt task)
    '''
    
    #----------------------------------------------------------
    def __init__(self, region,  assignment, multiple_assignments, 
                 taskRunner = None, callingTask = None):
        super(DeliverAssignmentTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = DELIVER_ASSIGNMENT_TASK_NAME

        self._region = region
        self._assignment = assignment
        self._multiple_assignments = multiple_assignments
        self._override = None
        self._check_digits = ''
        self._delivery_location_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTGetDeliveryLocation')
        self._loading_containers_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingRequestContainer')
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States'''
        self.addState(ASSIGNMENT_DELIVER, self.deliver)
        self.addState(ASSIGNMENT_DELIVER_LOAD, self.deliver_load)
        self.addState(ASSIGNMENT_DELIVER_VERIFY, self.delivery_verify)
        self.addState(ASSIGNMENT_LOAD, self.load)
         
    #----------------------------------------------------------
    def deliver(self):
        ''' Deliver load if deliver is set in assignment'''
        # deliver assignment if deliver assignment is set to 1
        result = -1
        while result < 0:
            result = self._request_delivery_location()
            
        if result > 0:
            self.next_state = ASSIGNMENT_DELIVER
        else:
            self._override = self._delivery_location_lut[0]["directLoad"]
            self._check_digits = self._delivery_location_lut[0]["checkDigit"]
        
    #----------------------------------------------------------
    def deliver_load(self):
        ''' Deliver/load  if direct load then load license else verify check digit and deliver'''
        # need to loop for all assignment
        #the following fields will be used for multiple assignments
        position=self._assignment["position"]
        id=self._assignment["idDescription"]
        delivery_location=self._delivery_location_lut[0]["location"]
        loading_dock_door=self._delivery_location_lut[0]["loadingDockDoor"]
        
        if self._delivery_location_lut[0]['allowOverride']:
            additional_vocab = {'override':False}
        else:
            additional_vocab = {}

        #load assignment
        if self._override:
            if self._multiple_assignments:
                prompt = itext("selection.deliver.assignment.load.multi", position, id, loading_dock_door)
            else: 
                prompt = itext("selection.deliver.assignment.load", loading_dock_door)
            self._check_digits = self._delivery_location_lut[0]["loadingDockDoorCD"]
        else:
       
            #deliver assignment
            if self._multiple_assignments:
                prompt = itext("selection.deliver.assignment.deliver.multi", position, id, delivery_location)
            else: 
                prompt = itext("selection.deliver.assignment.deliver", delivery_location)
            self._check_digits = self._delivery_location_lut[0]["checkDigit"]
        
        result = prompt_ready(prompt, priority_prompt = True, additional_vocab = additional_vocab)
        if result == 'override' :
            if self._override:
                self._override = False
            else:
                self._override = True
            self.next_state = ASSIGNMENT_DELIVER_LOAD            
        
    #----------------------------------------------------------
    def delivery_verify(self):
        if self._region["delivery"] != "1":
            return

        if self._delivery_location_lut[0]['allowOverride']:
            additional_vocab = {'override':False, 'location':False}
        else:
            additional_vocab = {'location':False}
        
        result = None
        if self._check_digits != '':
            result = prompt_digits_required(itext("selection.deliver.assignment.confirm.delivery"),
                                            itext("selection.deliver.assignment.checkdigit.help"), 
                                            [self._check_digits],
                                            additional_vocab = additional_vocab)
        else:
            result = prompt_ready(itext('selection.deliver.assignment.confirm.delivery'), 
                                  priority_prompt = True, additional_vocab = additional_vocab)
            
        if result == 'override' :
            if self._override:
                self._override = False
            else:
                self._override = True
            self.next_state = ASSIGNMENT_DELIVER_LOAD            
        elif result == 'location' :
            prompt_only(self._delivery_location_lut[0]["location"])
            self.next_state = ASSIGNMENT_DELIVER_VERIFY
            
    #----------------------------------------------------------
    def load(self):
        
        if self._override:
            #go to load container
            container = self._delivery_location_lut[0]["container"]
            
            self._request_loading_container()
            
            get_position = self._loading_containers_lut[0]["capture_position"]
            
            #launch load container
            self.launch(obj_factory.get(LoadContainerTask,
                                          container,
                                          get_position,
                                          '', 
                                          self.taskRunner, self), '')
        
    #----------------------------------------------------------
    def _request_delivery_location(self):
        ''' request delivery location for assignment'''
        # transmit LUT to get delivery information for assignment using groupID and assignmentID aprameters
        return self._delivery_location_lut.do_transmit(self._assignment["groupID"], self._assignment["assignmentID"])
    
    #-----------------------------------------------------------
    def _request_loading_container(self):
        ''' request loading container to get the loading container information '''
        # transmit LUT to get loading container information
        return self._loading_containers_lut.do_transmit(self._delivery_location_lut[0]["container"], False)
