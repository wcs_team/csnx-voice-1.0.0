from vocollect_http.httpserver import VoiceAppHTTPServer, get_serial_number,\
    handle_page_request
from vocollect_core.utilities.io_wrapper import IOWrapper

import voice
import os

from voice import log_message
import cgi

import urllib.request
from voice import get_voice_application_property
from vocollect_core.task.task_runner import TaskRunnerBase

APP_URI = get_voice_application_property('applicationURI')
FILE_DOES_NOT_EXIST_IMG = "images/image_not_found.png"

DEFAULT_PAGE = 'loading_page'

# TODO: This is prototype-quality code. It has only been tested
# with VoiceLink dialogs and the Receiving application. --jgeisler
class ReceivingVoiceAppHTTPServer(VoiceAppHTTPServer):

    #-------
    def __init__(self, request, client_address, server):
        self.HOST_NAME = get_voice_application_property('imageHost')
        super(ReceivingVoiceAppHTTPServer, self).__init__(request,
                                                   client_address,
                                                   server)
        
    #----------------------------------------------------------
    def log_request(self, code='-', size='-'):
        pass
    
    #----------------------------------------------------------
    def do_GET(self):
        
        log_message("Processing HTTP GET: " + self.path)
        
        log_message("HTTP GET: " + self.path)
        
        if (self.path == APP_URI or self.path == APP_URI + 'index.html'):
            file_obj = voice.open_vad_resource('index.html')
            if file_obj is not None:
                log_message("Returning index.html")
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                # Make sure the startup page isn't cached.
                self.send_nocache_headers()
                self.end_headers()
                self.wfile.write(file_obj.read().encode('utf-8'))
            else:
                self.send_response(404, "not found")
                self.end_headers()     
                            
        # The client is requesting the HTML state from the task object
        elif self.path.startswith(APP_URI + 'html'):
            self.write_response(handle_page_request(self.path, TaskRunnerBase, DEFAULT_PAGE))
            
        # The client is requesting data from the task object to fill in the HTML
        elif self.path.startswith(APP_URI + 'data'):
            page_name = str(self.path).split('/')[-1]
            
            if hasattr(TaskRunnerBase.get_main_runner().get_current_task(), 'get_data_map'):
                data = TaskRunnerBase.get_main_runner().get_current_task().get_data_map(page_name)
                data["serialnumber"] = get_serial_number()
                self.write_response(data, 'text/javascript')
            else:
                data = {}
                data["serialnumber"] = get_serial_number()
                data["title"] = ""
                self.write_response(data, 'text/javascript')
        # Get an item from the host server since we're not in the path
        elif self.path.startswith(APP_URI + 'host_image'):
            host_image = str(self.path).split('/')[-1].split('?')[0]
            
            local_image_location = os.path.normpath(os.path.join(voice.get_persistent_store_path(),'', 'temp_image'))
            try:
                urllib.request.urlretrieve(self.HOST_NAME + host_image, local_image_location)
                image_file = open(local_image_location, 'rb')
            except (ValueError, IOError):
                image_file = voice.open_vad_resource(FILE_DOES_NOT_EXIST_IMG, mode='rb')

            if host_image.endswith('.png'):
                content_type = 'image/png'
            elif host_image.endswith('.jpg'):
                content_type = 'image/jpg'
            else:
                content_type = 'image/png'
                image_file = voice.open_vad_resource(FILE_DOES_NOT_EXIST_IMG, mode='rb')
                
            self.send_response(200)
            self.send_header('Content-type', content_type)
            self.end_headers()
            self.wfile.write(image_file.read())
            
        else:
            self.get_resource(self.path[len(APP_URI):])    
        
    #----------------------------------------------------------            
    def do_POST(self):
        global rootnode
        
        voice.log_message("Processing HTTP POST: " + self.path)
        
        d = voice.current_dialog()        
        if d is not None:
            if self.path.startswith(APP_URI + 'advance'):
                clicked_vocab = str(self.path).split('/')[-1]
                self.log_message('\nposted: %s %s \n', clicked_vocab, d.__class__.__name__)
                d.http_post(clicked_vocab)                   
            else:
                form = cgi.FieldStorage(
                                        fp=IOWrapper(self.rfile), 
                                        headers=self.headers,
                                        environ={'REQUEST_METHOD':'POST',
                                                 'CONTENT_TYPE':self.headers['Content-Type'],
                                                 })
                if 1 == 1:
                    if 'digits' in form:
                        log_message('Posted Digits: ' + form['digits'].value)
                        if d.name == 'Digits':
                            d.http_post(form['digits'].value)
                        else:
                            log_message('Digits posted to non-digits dialog')
                    elif 'selectedItem' in form:
                        log_message('Posted selectedItem: ' + form['selectedItem'].value)
                        if d.name == 'ListSelection':
                            d.http_post(form['selectedItem'].value)
                        else:
                            log_message('SelectedItem posted to non-ListSelection dialog')
                    elif 'ReadySubmit' in form:
                        log_message('Submitted Ready: ' + form['ReadySubmit'].value)
                        if d.name == 'Ready':
                            d.http_post(form['ReadySubmit'].value)
                        else:
                            log_message('ReadySubmit posted to non-Ready dialog')
                    elif 'otherAction' in form:
                        log_message('Posted otherAction: ' + form['otherAction'].value)
                        d.http_post(form['otherAction'].value)                   
        else:
            log_message('No dialog to post to')

        self.send_response(200)
        self.end_headers()
   
