''' This override fixes the scanner not turning on before priority prompt finishes.
'''

import sys
from voice import getenv

from vocollect_core.utilities import util_methods, obj_factory


#------------------------------------------------------------------------------
#Fix to override so only A730 scanners return true for multi/rapid scan support
#------------------------------------------------------------------------------
def multiple_scans_supported_fix():
    return (util_methods.catalyst_version() >= util_methods.MULTIPLE_SCANS_VERSION 
            and getenv('Device.Subtype', None) in ['A730'])

sys.modules['vocollect_core.utilities.util_methods'].multiple_scans_supported = multiple_scans_supported_fix

from vocollect_core.dialog.digits_prompt import DigitsPromptExecutor, scan_results_exist, get_scan_result
from vocollect_core.dialog.float_prompt import FloatPromptExecutor
from vocollect_core.scanning import ScanMode, set_scan_mode
#------------------------------------------------------------------------------
# Fixes for scanning dialogs to turn scanner on before entering dialog
#------------------------------------------------------------------------------
#Fix to enable scanning before a priority prompt in all digit prompt related 
#dialogs
class DigitsPromptExecutorFix(DigitsPromptExecutor):
    
    def check_scanning_result(self):     

        if multiple_scans_supported_fix():
            result = super().check_scanning_result()
            if not result: #No scan result so dialog will be ran 
                if self.scan_mode != ScanMode.Off: #Scan mode is not off so go ahead and start scanner
                    set_scan_mode(self.scan_mode) 
                    
            #return original result        
            return result
        else:     
            if (self.scan_mode != ScanMode.Off 
            and self.required_scanned_values == None 
            and not self.priority_prompt):
                #Check for existing result, if found return it
                if scan_results_exist():
                    self.result = get_scan_result()
                    self.scanned = True
        
            return self.result != None
    
#Fix to enable scanning before a float prompt
class FloatPromptExecutorFix(FloatPromptExecutor):
    
    def check_scanning_result(self):
        result = super().check_scanning_result()
        if not result: #No scan result so dialog will be ran 
            if self.scan_mode != ScanMode.Off: #Scan mode is not off so go ahead and start scanner
                set_scan_mode(self.scan_mode) 

        #return original result        
        return result

            
obj_factory.set_override(DigitsPromptExecutor, DigitsPromptExecutorFix)
obj_factory.set_override(FloatPromptExecutor, FloatPromptExecutorFix)