#---------------------------------------------------------------
#Line Loading
LINE_LOADING_TASK_NAME = "lineLoading"

#States - Line Loading
REGIONS             = 'Region'
VALIDATE_REGION     = 'ValidateRegion'
ENTER_SPUR          = 'enterSpur'
XMIT_SPUR           = 'xmitSpur'
ENTER_CARTON        = 'enterCarton'
XMIT_CARTON         = 'xmitCarton'
ENTER_PALLET        = 'enterPallet'
XMIT_PALLET         = 'xmitPallet'

ERROR_CARTON_ALREADY_LOADED = 2

