from vocollect_core.dialog.functions import prompt_only
from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary
from vocollect_core import itext, class_factory, obj_factory

class LineLoadingVocabulary(class_factory.get(DynamicVocabulary)):
    
    def __init__(self, runner, task):
        self.vocabs = {'spur':              obj_factory.get(Vocabulary,'spur', self._spur, False),
                       'route':             obj_factory.get(Vocabulary,'route', self._route, False)
                       }

        self.runner = runner
        self.line_load_task = task

                         
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''
        
        if vocab == 'spur':
            return self.line_load_task._spur is not None
        
        elif vocab == 'route':
            return self.line_load_task._carton is not None

        return False
    
        
    #Functions to execute words? 
    def _spur(self):
        prompt_only(' '.join(self.line_load_task._spur))
        return True 

    def _route(self):
        prompt_only(itext('lineload.route',
                               self.line_load_task._carton_lut[0]['route'], 
                               self.line_load_task._carton_lut[0]['stop']))
        return True 
