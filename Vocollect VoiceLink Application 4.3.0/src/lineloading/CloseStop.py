from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_ready, prompt_digits, prompt_alpha_numeric
from vocollect_core import itext, class_factory, obj_factory
from common.VoiceLinkLut import VoiceLinkLut

ERROR_CARTON_ALREADY_LOADED = 2

#States - Line Loading
PALLET              = 'pallet'
PRINTER             = 'printer'
XMIT_CLOSE          = 'xmitClose'
END                 = 'closeStopEnd'

class CloseStop(class_factory.get(TaskBase)):
    ''' Line Loading task for the voicelink system
    
    Steps:
            1. Enter pallet number
            2. Enter printer if auto print
            3. Transmit Close LUT
            4. End Close Stop
    '''
    def __init__(self, region, taskRunner = None, callingTask = None):
        super(CloseStop, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = "closeStop"
        self._region = region
        self._pallet = None
        self._printer = None

        #define LUTs
        self._close_stop_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLineLoadingCloseStop')
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(PALLET,               self.pallet)
        self.addState(PRINTER,              self.printer)
        self.addState(XMIT_CLOSE,           self.xmit_close)
        self.addState(END,                  self.end_close)

    #----------------------------------------------------------
    def pallet(self):
        ''' enter pallet '''
        result = prompt_alpha_numeric(itext('lineload.closestop'), 
                                      itext('lineload.closestop.help'), 
                                      1, None, 
                                      True, True,
                                      {'cancel' : False})
         
        if result[0] == 'cancel':
            self.next_state = ''
        else:
            self._pallet = result[0]

    #----------------------------------------------------------
    def printer(self):
        ''' get printer '''
        if self._region['auto_print_manifest']:
            self._printer = prompt_digits(itext('generic.printer'), 
                                          itext('generic.printer.help'), 
                                          1, 2, 
                                          True, False,
                                          {'cancel' : False})
            if self._printer == 'cancel':
                self.next_state = ''

    #----------------------------------------------------------
    def xmit_close(self):
        ''' transmit close information '''
        result = self._close_stop_lut.do_transmit(self._pallet, self._printer)
        if result < 0:
            self.next_state = XMIT_CLOSE
        elif result > 0:
            self.next_state = PALLET
    
    #----------------------------------------------------------
    def end_close(self):
        ''' end close task '''
        prompt_ready(itext('lineload.closestop.end',
                                self._close_stop_lut[0]['route'],
                                self._close_stop_lut[0]['stop'],
                                self._close_stop_lut[0]['palletCount']))
    
