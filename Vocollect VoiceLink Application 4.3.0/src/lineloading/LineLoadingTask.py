from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_ready, prompt_only, prompt_digits, prompt_yes_no, prompt_alpha_numeric
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut

from common.RegionSelectionTasks import MultipleRegionTask
from lineloading.LineLoadingVocabulary import LineLoadingVocabulary
from lineloading.CloseStop import CloseStop
from Globals import change_function
from lineloading.SharedConstants import ERROR_CARTON_ALREADY_LOADED,\
    LINE_LOADING_TASK_NAME, REGIONS, VALIDATE_REGION, ENTER_SPUR, XMIT_SPUR,\
    ENTER_CARTON, XMIT_CARTON, ENTER_PALLET, XMIT_PALLET

class CartonsLut(class_factory.get(VoiceLinkLut)):
    
    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        if error == ERROR_CARTON_ALREADY_LOADED:
            if prompt_yes_no(itext('lineload.move.cartons')):
                return 0
            else:
                prompt_ready(itext('lineload.leave.on.pallet'))
                return ERROR_CARTON_ALREADY_LOADED
        else:
            return super(CartonsLut, self)._process_error(error, message)

class LineLoadingTask(class_factory.get(TaskBase)):
    ''' Line Loading task for the voicelink system
    
    Steps:
            1. Get region
            2. Validate region
            3. Enter Spur
            4. Transmit Spur
            5. Enter Carton 
            6. Transmit Carton
            7. Enter Pallet
            8. Transmit Pallet
    '''
    #----------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        super(LineLoadingTask, self).__init__(taskRunner, callingTask)
    
        #Set task name
        self.name = LINE_LOADING_TASK_NAME

        #define dynamic vocabulary
        self.dynamic_vocab = obj_factory.get(LineLoadingVocabulary,
                                               self.taskRunner, self)

        #define LUTs
        self._valid_regions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLineLoadingValidRegions')
        self._request_reqion_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLineLoadingRequestRegion')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLineLoadingRegionConfiguration')
        self._confirm_spur_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLineLoadingConfirmSpur')
        self._carton_lut = obj_factory.get(CartonsLut, 'prTaskLUTLineLoadingCarton')
        self._pallet_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLineLoadingPallet')

        #working variables
        self._region_selected = False
        self._spur = None
        self._carton = None
        self._carton_canceled = False
        self._pallet = None
        self._pallet_new = False
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #Add states
        self.addState(REGIONS,              self.regions)
        self.addState(VALIDATE_REGION,      self.validate_region)
        self.addState(ENTER_SPUR,           self.enter_spur)
        self.addState(XMIT_SPUR,            self.xmit_spur)
        self.addState(ENTER_CARTON,         self.enter_carton)
        self.addState(XMIT_CARTON,          self.xmit_carton)
        self.addState(ENTER_PALLET,         self.enter_pallet)
        self.addState(XMIT_PALLET,          self.xmit_pallet)
        
    #----------------------------------------------------------
    def regions(self):
        ''' Run multi region selection, but only allow for 1 region '''
        self.launch(obj_factory.get(MultipleRegionTask,
                                      self.taskRunner, self, True))

    #----------------------------------------------------------
    def validate_region(self):
        #check for valid regions
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
        
        if not valid:
            prompt_only(itext('generic.regionNotAuth.prompt'))
            self.next_state = ''
        else:
            self._region_selected = True
            
    #----------------------------------------------------------
    def enter_spur(self):
        self._spur = None
        self._spur = prompt_digits(itext('lineload.spur'), 
                                   itext('lineload.spur.help'), 
                                   1, None, 
                                   True)

    #----------------------------------------------------------
    def xmit_spur(self):
        result = self._confirm_spur_lut.do_transmit(self._spur)
        if result < 0:
            self.next_state = XMIT_SPUR
        elif result > 0:
            self.next_state = ENTER_SPUR
        else:
            self._spur = self._confirm_spur_lut[0]['spur']
            
    #----------------------------------------------------------
    def enter_carton(self):
        self._carton = None
        
        result = prompt_digits(itext('lineload.carton'), 
                               itext('lineload.carton.help'), 
                               1, None, 
                               True, True,
                               {'change spur' : False,
                                'close stop' : False,
                                'change function' : False})

        if result[0] == 'change spur':
            self._carton = None
            self.next_state = ENTER_SPUR
        elif result[0] == 'close stop':
            self._carton = None
            if self._region_config_lut[0]['allow_close_stop']:
                self.launch(obj_factory.get(CloseStop,
                                              self._region_config_lut[0], 
                                              self.taskRunner, 
                                              self), ENTER_CARTON)
            else:
                self.next_state = ENTER_CARTON
        elif result[0] == 'change function':
            change_function()
            self.next_state = ENTER_CARTON
        else:
            self._carton = result[0]
            
    #----------------------------------------------------------
    def xmit_carton(self):
        result = self._carton_lut.do_transmit(self._carton, 
                                              int(self._carton_canceled), 
                                              self._spur)
        if result < 0:
            self.next_state = XMIT_CARTON
        elif result > 0:
            self.next_state = ENTER_CARTON
        elif self._carton_canceled:
            prompt_ready(itext('lineload.set.aside'))
            self._carton_canceled = False
            self.next_state = ENTER_CARTON
        
    #----------------------------------------------------------
    def enter_pallet(self):
        
        if self._pallet_new:
            prompt = itext('lineload.pallet.new', 
                                ' '.join(self._carton_lut[0]['route']), 
                                ' '.join(self._carton_lut[0]['stop']))
        else:
            prompt = itext('lineload.pallet', 
                                ' '.join(self._carton_lut[0]['route']), 
                                ' '.join(self._carton_lut[0]['stop']))
        
        result = prompt_alpha_numeric(prompt, 
                               itext('lineload.pallet.help'), 
                               1, None, 
                               True, True, 
                               {'cancel' : False,
                                'new pallet' : False})
     
        if result[0] == 'cancel':
            self._pallet = None
            if prompt_yes_no(itext('lineload.carton.cancel',
                                        ' '.join(self._carton)), 
                             True):
                self._carton_canceled = True
                self._pallet_new = False
                self.next_state = XMIT_CARTON
            else:
                self.next_state = ENTER_PALLET
        elif result[0] == 'new pallet':
            self._pallet_new = True
            self._pallet = None
            self.next_state = ENTER_PALLET
        else:
            self._pallet = result[0]
    #----------------------------------------------------------
    def xmit_pallet(self):
        result = self._pallet_lut.do_transmit(self._pallet, 
                                              self._carton, 
                                              int(self._pallet_new))
        if result < 0:
            self.next_state = XMIT_PALLET
        elif result > 0:
            self.next_state = ENTER_PALLET
        else:
            self.next_state = ENTER_CARTON
            self._pallet_new = False

    #======================================================
    #Global/Dynamic Word Functions
    #======================================================
    def change_region(self):
        ''' Called when operator speaks global word "change region" '''
        if self._region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self._region_selected = False
                self.return_to(self.name, REGIONS)
