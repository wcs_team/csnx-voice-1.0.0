from vocollect_core import obj_factory
from vocollect_core.dialog.digits_prompt import DigitsPrompt

def prompt_alpha_numeric_suggested(prompt, help, suggested_value, scan_value,
                        min_length=1, max_length=10, 
                        confirm=True, scan=False, additional_vocab = {},
                        skip_prompt = False):
    ''' Wrapper function for alpha numeric entry of values where there 
        is a value that you expect the user to speak but not require
        the value to be entered.  The suggested_value will be used
        as a response expression for the recognizer.
    
    Parameters:
          prompt - main prompt to be spoken
          help - main help message to be spoken
          suggested_value - a list of alpha/digit strings a operator may speak
          scan_value (Default=None) - List of values that can be scanned, none if scanning not permitted
          min_length (Default=1) - minimum number of digits allowed
          max_length (Default=10) - Maximum number of digits allowed
          Confirm (Default=True) - Determine whether or not entered values 
                                   should be confirmed by operator
          scan (Default=False) - determines if scanning needs to be enabled
          additional_vocab (Default={}) - Dictionary of additional words and whether 
                                          or not they should be confirmed
          skip_prompt (Default = False) - skips speaking main prompt when entering
                      dialog
          
    returns: Digits entered by operator
    '''
    dialog = obj_factory.get(DigitsPrompt,
                               prompt, help, min_length, max_length, confirm, scan)

    if (len(suggested_value) > 0):
        dialog.nodes['StartHere'].response_expression = suggested_value[0]
        
    dialog.skip_prompt = skip_prompt

    dialog.invalid_count_max = 1
    dialog.set_additional_vocab(additional_vocab)
    dialog.add_additional_digits(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                                  'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                                  'U', 'V', 'W', 'X', 'Y', 'Z'])
    if scan:    
        return dialog.run(), dialog.is_scanned
    else:
        return dialog.run()