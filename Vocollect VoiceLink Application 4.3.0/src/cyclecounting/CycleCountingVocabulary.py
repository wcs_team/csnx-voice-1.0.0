from vocollect_core import itext, class_factory, obj_factory
from vocollect_core.dialog.functions import prompt_only
from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary

class CycleCountingVocabulary(class_factory.get(DynamicVocabulary)):
    ''' Definition of additional vocabulary used throughout VoiceLink selection
    
    Parameters
            runner - Task runner object
    '''
    
    def __init__(self, runner):
        self.vocabs = {'item number':       obj_factory.get(Vocabulary,'item number', self._item_number, False),
                       'description':       obj_factory.get(Vocabulary,'description', self._description, False),
                       'location':          obj_factory.get(Vocabulary,'location', self._location, False),
                       'quantity':          obj_factory.get(Vocabulary,'quantity', self._quantity, False),
                       'UPC':               obj_factory.get(Vocabulary,'UPC', self._upc, False)
                       }

        self.runner = runner
        self.location_lut = None
        self.curr_assignment = None
        
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''

        current_task = self.runner.get_current_task()

        if vocab == 'item number':
            return current_task.name in ['cycleCountCountItem'] and self.curr_assignment is not None
        elif vocab == 'description':
            return current_task.name in ['cycleCountCountItem'] and self.curr_assignment is not None
        elif vocab == 'location':
            return ((current_task.name in ['cycleCountCountItem'] and self.curr_assignment is not None)
                    or (current_task.name in ['cycleCountDirectToLocation'] and self.location_lut is not None))
        elif vocab == 'quantity':
            return current_task.name in ['cycleCountCountItem'] and self.curr_assignment is not None
        elif vocab == 'UPC':
            return current_task.name in ['cycleCountCountItem'] and self.curr_assignment is not None

        return False
    
        
    def _item_number(self):
        ''' speak item number '''
        if self.curr_assignment['item_number'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.curr_assignment['item_number'])
        return True 

    def _description(self):
        ''' speak description information '''
        if self.curr_assignment['item_description'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.curr_assignment['item_description'].lower())
        return True 

    def _location(self):
        ''' speak location information '''
        
        pre_aisle = ''
        aisle = ''
        post_aisle = ''
        slot = ''

        current_task = self.runner.get_current_task()
        if current_task.name in ['cycleCountCountItem']:
            pre_aisle = self.curr_assignment['pre_aisle']
            aisle = self.curr_assignment['aisle']
            post_aisle = self.curr_assignment['post_aisle']
            slot = self.curr_assignment['slot']
        else:
            pre_aisle = self.location_lut[0]['pre_aisle']
            aisle = self.location_lut[0]['aisle']
            post_aisle = self.location_lut[0]['post_aisle']
            slot = self.location_lut[0]['slot']

        #get proper prompt key
        key = 'cycle.location.info.prompt'
        if aisle == '':
            key = 'cycle.location.info.prompt.noaisle'
            
        prompt_only(itext(key, 
                          pre_aisle,
                          aisle,
                          post_aisle,
                          slot))
        return True 

    def _quantity(self):
        ''' speak quantity '''
        qty = self.curr_assignment['expected_qty']
        if qty < 0 or self.curr_assignment['count_mode'] == 'B':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(str(qty) + " " + self.curr_assignment['uom'])
         
        return True 

    def _upc(self):
        ''' speak UPC '''
        if self.curr_assignment['upc'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(' '.join(self.curr_assignment['upc']))
        return True 

