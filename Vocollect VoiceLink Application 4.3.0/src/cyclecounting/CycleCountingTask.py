from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_only, prompt_yes_no, prompt_ready, prompt_digits,\
    prompt_alpha_numeric
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut
from common.RegionSelectionTasks import MultipleRegionTask
from cyclecounting.CycleCountingLuts import CCLocation
from cyclecounting.CycleCountingDirectToLocation import CycleCountDirectToLocationTask
from cyclecounting.CycleCountingCountItem import CycleCountCountItemTask
from cyclecounting.CycleCountingVocabulary import CycleCountingVocabulary
from Globals import change_function

import voice
from cyclecounting.SharedConstants import CC_TASK_NAME, DEFAULT_HTML, PAGE_TITLE,\
    OPERATOR_NAME, REGIONS, VALIDATE_REGION, CYCLE_COUNT_MODE,\
    CYCLE_COUNT_NEXT_LOC, CYCLE_COUNT_GET_LOC, CYCLE_COUNT_COUNT_ITEM,\
    CYCLE_COUNT_NO_MORE
from vocollect_http.httpserver import set_display_page

class CycleCountRegionTask(class_factory.get(MultipleRegionTask)):

    def _request_region_configs(self):
        ''' No region config for cycle count, just 
        populates lut and returns 0 with out transmit '''
        self._region_config_lut.receive('1,region,0,\n\n')
        return 0
    

class CycleCountingTask(class_factory.get(TaskBase)):
    ''' Main cycle counting task
    
    Steps:
            1. Get region
            2. Validate regions
            3. Get Cycle count mode (operator or host directed)
            4. get next location
            5. get location check digit (operator directed), or direct to location (host directed)
            6. count items at location 
            7. No more to count
    '''

    #----------------------------------------------------------
    def __init__(self,
                 taskRunner = None, 
                 callingTask = None):
        TaskBase.__init__(self, taskRunner, callingTask)

        #Set task name
        self.name = CC_TASK_NAME

        #Luts for cycle counting
        self._valid_regions_lut = obj_factory.get(VoiceLinkLut,'prTaskLUTCycleCountingRegions')
        self._request_reqion_lut = obj_factory.get(VoiceLinkLut,'prTaskLUTCycleCountingRegion')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCycleCountingRegionConfigs')
        self._cycle_count_mode = obj_factory.get(VoiceLinkLut, 'prTaskLUTCycleCountingMode')
        self._location_lut = obj_factory.get(CCLocation, 'prTaskLUTCycleCountingLocation')

        #working variables
        self._region_selected = False
        self._location = ''
        self._check_digit = ''
        self._scanned = False
        
        #define and setup additional vocabulary object
        self.dynamic_vocab = obj_factory.get(CycleCountingVocabulary,
                                               self.taskRunner)
        
        self.title = PAGE_TITLE
        self.operator_name = voice.getenv('Operator.Name', OPERATOR_NAME)
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(REGIONS,                  self.regions)
        self.addState(VALIDATE_REGION,          self.validate_regions)
        self.addState(CYCLE_COUNT_MODE,         self.cycle_counting_mode)
        self.addState(CYCLE_COUNT_NEXT_LOC,     self.next_location)
        self.addState(CYCLE_COUNT_GET_LOC,      self.get_location)
        self.addState(CYCLE_COUNT_COUNT_ITEM,   self.count_item)
        self.addState(CYCLE_COUNT_NO_MORE,      self.no_more)
        
        
    #----------------------------------------------------------
    def regions(self):
        ''' Run multi region selection '''
        self._region_selected = False
        self.launch(obj_factory.get(CycleCountRegionTask, self.taskRunner, self))

    #----------------------------------------------------------
    def validate_regions(self):
        ''' validates region selection '''
        #check for valid regions
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
        
        if not valid:
            prompt_only(itext('generic.regionNotAuth.prompt'))
            self.next_state = ''
        else:
            self._region_selected = True


    #----------------------------------------------------------
    def cycle_counting_mode(self):
        ''' get cycle counting mode '''
        
        set_display_page(self, DEFAULT_HTML) 
        
        result = self._cycle_count_mode.do_transmit()
        
        if result != 0:
            self.next_state = CYCLE_COUNT_MODE

    #----------------------------------------------------------
    def next_location(self):
        ''' prompt user for next location '''
        
        #initialize variables
        self._location = ''
        self._check_digit = ''
        self._scanned = False
        self.dynamic_vocab.location_lut = None
        
        #check for mode
        additional_vocab = {'change function' : False, 'change region' : False}
        help = itext('cycle.next.location.help.operator')
        
        if self._cycle_count_mode[0]['mode_indicator'] != 'P':
            additional_vocab.update({'ready' : False})
            help = itext('cycle.next.location.help.system')
        
        self.force_refresh = True
        set_display_page(self, DEFAULT_HTML)
        
        #prompt for location (or ready)
        self._location, self._scanned = prompt_alpha_numeric(itext('cycle.next.location'), 
                                                             help, 
                                                             2, 1000, 
                                                             False, 
                                                             True, 
                                                             additional_vocab)
    
        if self._location == 'change function':
            change_function()
            self.next_state = self.current_state
            
        elif self._location == 'change region':
            self.change_region()
            self.next_state = self.current_state
            
    #----------------------------------------------------------
    def get_location(self):
        ''' either get location from host, or have operator enter check digits '''
        if self._location == 'ready': #must be host directed
            result = self._location_lut.do_transmit()
            
            if result != 0:
                if result == 2:
                    self._region_selected = False
                    self.return_to(self.name, REGIONS)
                    self.next_state = self.current_state
                if result == 3:
                    change_function()
                    self.next_state = self.current_state    
            else:
                self._location = ''
                self._check_digit = ''
                self._scanned = False
                self.dynamic_vocab.location_lut = self._location_lut
                self.launch(obj_factory.get(CycleCountDirectToLocationTask,
                                              self._location_lut,
                                              self.taskRunner,
                                              self))

        else: # operator directed so just enter CD
            self._location_lut.clear()
            if not self._scanned:
                self._check_digit = prompt_digits(itext('cycle.next.checkdigit'), 
                                                  itext('cycle.next.checkdigit.help'), 
                                                  1, 
                                                  1000, 
                                                  False, 
                                                  False, 
                                                  {'ready' : False})
                
                if self._check_digit == 'ready':
                    self._check_digit = ''
            else:
                self._check_digit = ''
            
    #----------------------------------------------------------
    def count_item(self):
        ''' launch count item task '''
        self.launch(obj_factory.get(CycleCountCountItemTask,
                                      self._location,
                                      self._scanned,
                                      self._check_digit,
                                      self._location_lut.skipped,
                                      self.taskRunner,
                                      self), 
                                      CYCLE_COUNT_NEXT_LOC)
            
    #----------------------------------------------------------
    def no_more(self):
        ''' no more to cycle count '''
        result = prompt_ready(itext('cycle.no.more'), 
                              True, 
                              {'change function' : False,
                               'change region' : False})

        if result == 'change function':
            change_function()
            self.next_state = self.current_state
        elif result == 'change region':
            self.change_region()
            self.next_state = self.current_state
        else:
            self.next_state = REGIONS

    #----------------------------------------------------------
    def get_data_map(self, page_name):
        ''' custom method to return JSON-dumpable dict of info '''
        map = {}
        
        map['title'] = self.title
        map['operator'] = self.operator_name
        
        return map

    #======================================================
    #Global/Dynamic Word Functions
    #======================================================
    def change_region(self):
        ''' Called when operator speaks global word "change region" '''
        if self._region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self._region_selected = False
                self.return_to(self.name, REGIONS)
