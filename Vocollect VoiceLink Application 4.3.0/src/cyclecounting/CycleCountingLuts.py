'''
This module contains custom Selection LUTS that extend the functionality of a 
basic LUT
'''
from common.VoiceLinkLut import VoiceLinkLut
from pending_odrs import wait_for_pending_odrs
from vocollect_core import class_factory
from voice import globalwords as gw
from vocollect_core.utilities.localization import itext
from vocollect_core.dialog.base_dialog import BaseDialog
from vocollect_core.dialog.functions import prompt_ready, prompt_words
from Globals import sign_off
from vocollect_core.utilities import obj_factory
from vocollect_core.dialog.ready_prompt import ReadyPrompt

###############################################################
class CCLocation(class_factory.get(VoiceLinkLut)):

    def __init__(self, command):
        super(CCLocation, self).__init__(command)
        #saved information from previous request
        self.previous_pre_aisle = ''
        self.previous_aisle = ''
        self.previous_post_aisle = ''
        self.skipped = False
        
    def clear(self):
        ''' clear previuos location information '''
        self.previous_pre_aisle = ''
        self.previous_aisle = ''
        self.previous_post_aisle = ''
        self.skipped = False
        if len(self) > 0:
            del self[:]
        
    def do_transmit(self, *fields):
        ''' override of transmit to save previous records information 
        before requesting next location '''
        #save previous location before request next one
        try:
            if len(self) > 0:
                self.previous_pre_aisle = self[0]['pre_aisle']
                self.previous_aisle = self[0]['aisle']
                self.previous_post_aisle = self[0]['post_aisle']
                self.skipped = False
        finally:
            return super(CCLocation, self).do_transmit(*fields)
        
    def _process_error(self, error, message):
        ''' Protected method to process error codes. 
        Override if any special errors 
        
        Parameters:
                error - error code received
                message - message received
        returns: error code modified if handled
        '''
        # just return error if error is 0
        if error == 0:
            return error
        
        #save current state of globals
        sign_off_current = gw.words['sign off'].enabled
        take_a_break_current = gw.words['take a break'].enabled

        try:
            #Disable globals
            gw.words['sign off'].enabled = False
            gw.words['take a break'].enabled = False
            
            return_error = error
            if error == 99:
                #Disable Dynamic Vocab for this prompt only
                BaseDialog.exclude_dynamic_vocab = True
                
                prompt_ready(itext('generic.sayReady.prompt', message))
                return_error = 0
                
            elif error == 98:
                #Disable Dynamic Vocab for this prompt only
                BaseDialog.exclude_dynamic_vocab = True
                
                # Need to turn off the dynamic dialog; this is really the only place we need to do this right now
                # ...If we come up with a general need for this functionality, we should think about moving
                # ...this into the prompt as a input. --jgeisler
                spoken = prompt_words(itext('generic.error.say.sign.off.prompt', message), 
                                              False, {'sign off' : False})
                if spoken == 'sign off':
                    sign_off();
                    
            elif error > 0:
                #Disable Dynamic Vocab for this prompt only
                BaseDialog.exclude_dynamic_vocab = True
                dialog = obj_factory.get(ReadyPrompt, itext('generic.sayReady.prompt', message), True)
                dialog.set_additional_vocab({ 'sign off' : False, 'change function' : False, 'take a break' :False})
                dialog.skip_prompt = False
                dialog.nodes['StartHere'].help_prompt = itext('cycle.no.more.help')
                spoken = dialog.run()

                if spoken == 'sign off':
                    sign_off();
                elif spoken == 'ready':
                    return_error = 2
                elif spoken == 'change function':
                    return_error = 3
                elif spoken == 'take a break':
                    return_error = 4      
                else:
                    return_error = 2    
        finally:
            #always re-enable globals before leaving function.
            gw.words['sign off'].enabled = sign_off_current
            gw.words['take a break'].enabled = take_a_break_current
            
        return return_error    
        
###############################################################
# Override to not speak standard error when can contact host
###############################################################
class CCReserveChain(class_factory.get(VoiceLinkLut)):
    
    def do_transmit(self, *fields):
        ''' transmit method for LUT
            Parameters:
            *fields - variable number of fields to transmit
            
            return: Returns 0 if no error occurred, otherwise the error code
        '''
        
        #first check if any pending ODRs
        wait_for_pending_odrs('VoiceLink')
        
        result = self._transmit(*fields)
        
        #don't speak error meesgae for this lut
        #if result < 0:
        #    message = itext('generic.errorContactingHost.prompt')
        #    dialogs.prompt_ready(itext('generic.sayReadyTryAgain.prompt', message))
            
        return result
        
        
