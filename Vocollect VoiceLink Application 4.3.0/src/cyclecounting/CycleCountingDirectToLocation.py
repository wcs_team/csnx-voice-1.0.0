from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_ready, prompt_digits_required, prompt_only, prompt_yes_no
from vocollect_core import itext, class_factory
from cyclecounting.SharedConstants import CC_DIRECT_TO_LOCATION_TASK_NAME,\
    CC_LOCATION_PREAISLE, CC_LOCATION_AISLE, CC_LOCATION_POSTAISLE,\
    CC_LOCATION_VALIDATE_SLOT

class CycleCountDirectToLocationTask(class_factory.get(TaskBase)):
    '''Direct operator to location to cycle count
    
    Steps:
        1. Direct to pre-aisle
        2. Direct to aisle
        3. Direct to post-aisle
        4. Validate Slot
    '''
 
    #----------------------------------------------------------
    def __init__(self,
                 location_lut,
                 taskRunner = None, 
                 callingTask = None):
        
        TaskBase.__init__(self, taskRunner, callingTask)
        
        #Set task name
        self.name = CC_DIRECT_TO_LOCATION_TASK_NAME
      
        #Luts
        self._location_lut = location_lut
        self._supress_slot_prompt = False
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States '''
        
        #get state
        self.addState(CC_LOCATION_PREAISLE,       self.pre_aisle)
        self.addState(CC_LOCATION_AISLE,          self.aisle)
        self.addState(CC_LOCATION_POSTAISLE,      self.post_asile)
        self.addState(CC_LOCATION_VALIDATE_SLOT,  self.validate_slot)

    #----------------------------------------------------------  
    def pre_aisle(self):
        ''' directing to Pre Aisle'''
        #if pre-aisle is same as pre-aisle don't prompt
        if self._location_lut[0]['pre_aisle'] != self._location_lut.previous_pre_aisle:
            if self._location_lut[0]['pre_aisle'] != '':
                prompt_ready(itext('cycle.location.pre.aisle', 
                                   self._location_lut[0]['pre_aisle']), True)

            self._location_lut.previous_post_aisle = ''
            self._location_lut.previous_aisle = ''
            self._location_lut.previous_pre_aisle = self._location_lut[0]['pre_aisle']
    
    #----------------------------------------------------------
    def aisle(self):
        ''' directing to Aisle'''
        #if aisle is same as aisle don't prompt
        if self._location_lut[0]['aisle'] != self._location_lut.previous_aisle:
            if self._location_lut[0]['aisle'] != '':
                prompt_ready(itext('cycle.location.aisle', 
                                   self._location_lut[0]['aisle']), 
                             True)

                self._location_lut.previous_post_aisle = ''
                self._location_lut.previous_aisle = self._location_lut[0]['aisle']
                


    #----------------------------------------------------------
    def post_asile(self):
        ''' directing to Post Aisle'''
        #if aisle is same as post-aisle don't prompt
        if self._location_lut[0]['post_aisle'] != self._location_lut.previous_post_aisle: 
            if self._location_lut[0]['post_aisle'] != '':
                prompt_ready(itext('cycle.location.post.aisle', 
                                   self._location_lut[0]['post_aisle']), True)

            self._location_lut.previous_post_aisle = self._location_lut[0]['post_aisle']

    #----------------------------------------------------------
    def validate_slot(self):
        ''' validate slot by PVID or Check Digits '''
        pvid = self._location_lut[0]['pvid']
        cd = self._location_lut[0]['check_digits']
        result = None
        additional_vocab = {'skip slot' : True}
        prompt = itext('cycle.location.slot', self._location_lut[0]['slot'])

        if pvid == '' and cd == '':
            result = prompt_ready(prompt, 
                                  True, 
                                  additional_vocab, 
                                  self._supress_slot_prompt) 
        else:
            if cd == '': # if no check digit, but have pvid, then allow operator to say ready
                additional_vocab['ready'] = False
            
            result, scanned = prompt_digits_required(prompt, #@UnusedVariable
                                            itext('cycle.location.slot.help'), 
                                            [cd, pvid],
                                            [True], 
                                            additional_vocab,
                                            self._supress_slot_prompt)

        #Check for skip slot
        self._supress_slot_prompt = False
        if result == 'skip slot':
            self._location_lut.skipped = True
        
        #Ready spoken and no check digits or pvid, no more checks needed
        elif result == 'ready' and cd == '' and pvid == '':
            pass
        
        #Check if ready spoken, but check digits are available (Should not be possible)
        #elif result == 'ready' and cd != '':
        #    prompt_only(itext('cycle.location.slot.validate.cd'))
        #    self.next_state = CC_LOCATION_VALIDATE_SLOT
        #    self._supress_slot_prompt = True

        #Check ready spoken with blank CD and PVID available 
        elif result == 'ready' and pvid != '' and cd == '':
            if not prompt_yes_no(itext('cycle.location.slot.empty')):
                prompt_only(itext('cycle.location.speak.pvid'))
                self._supress_slot_prompt = True
                self.next_state = CC_LOCATION_VALIDATE_SLOT
                
        #check result does not match either CD or PVID
        elif result != cd and result != pvid:
            prompt_only(itext('generic.wrongValue.prompt', result))
            self.next_state = CC_LOCATION_VALIDATE_SLOT
        
        #check matches CD, and PVID is available
        elif result == cd and result != pvid and pvid != '':
            if not prompt_yes_no(itext('cycle.location.slot.empty')):
                prompt_only(itext('cycle.location.speak.pvid'))
                self._supress_slot_prompt = True
                self.next_state = CC_LOCATION_VALIDATE_SLOT
                
