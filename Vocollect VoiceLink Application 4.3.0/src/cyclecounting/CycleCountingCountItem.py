from common.VoiceLinkLut import VoiceLinkLut, VoiceLinkOdr
from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_digits, prompt_only,\
    prompt_list_lut, prompt_words
from vocollect_core import itext, class_factory, obj_factory
from cyclecounting.CycleCountingVocabulary import CycleCountingVocabulary
import voice
from cyclecounting.SharedConstants import CC_COUNT_ITEM_TASK_NAME, PAGE_TITLE, \
    OPERATOR_NAME, CC_COUNT_GET_ASSIGNMENT, CC_COUNT_NEXT_ASSIGNMENT,\
    CC_COUNT_PROMPT_KNOWN, CC_COUNT_PROMPT_BLIND, CC_COUNT_REASON_CODE,\
    CC_COUNT_SEND_ITEM_COUNT, SHOW_TIE_BY_HIGH_HTML, SHOW_RESERVE_CHAIN_HTML,\
    SHOW_RESERVE_CHAIN_UNAVAIL_HTML, SHOW_REASON_CODES_HTML, CYCLE_COUNT_NEXT_LOC
from selection.SharedConstants import PICK_PROMPT_TASK_NAME
from cyclecounting.CycleCountingLuts import CCReserveChain
from vocollect_http.httpserver import set_display_page

class CycleCountCountItemTask(class_factory.get(TaskBase)):
    '''Direct operator to location to cycle count
    
    Steps:
        1. Get Count assignment
        2. Get next assignment record
        3. prompt known quantity
        4. prompt blind quantity
        5. send item count
    '''
 
    #----------------------------------------------------------
    def __init__(self,
                 location,
                 scanned,
                 check_digits,
                 skipped,
                 taskRunner = None, 
                 callingTask = None):
        TaskBase.__init__(self, taskRunner, callingTask)
        
        #Set task name
        self.name = CC_COUNT_ITEM_TASK_NAME
      
        self._location = location
        self._scanned = scanned
        self._check_digits = check_digits
        self._skipped = skipped
        self._calling_task = callingTask
        
        #Luts/ODRs
        self._assignment_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCycleCountingAssignment')
        self._item_count_odr = obj_factory.get(VoiceLinkOdr, 'prTaskODRItemCountUpdate')
        self._reason_code_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreGetReasonCodes')
        self._reserve_locations = obj_factory.get(CCReserveChain, 'prTaskLUTCycleCountingReserveChain')
        
        self.force_refresh = False

        self._reason_code = ''
        self._invalid_count = 0

        self.title = PAGE_TITLE
        self.operator_name = voice.getenv('Operator.Name', OPERATOR_NAME)

        self._curr_assignment = None
        self._assignment_iterator = None
        self._multi_items = False
        
        #set dynamic vocabulary
        self.dynamic_vocab = obj_factory.get(CycleCountingVocabulary,
                                               self.taskRunner)
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States '''
        
        #get state
        self.addState(CC_COUNT_GET_ASSIGNMENT,  self.get_assignment)
        self.addState(CC_COUNT_NEXT_ASSIGNMENT, self.next_assignment)
        self.addState(CC_COUNT_PROMPT_KNOWN,    self.prompt_known)
        self.addState(CC_COUNT_PROMPT_BLIND,    self.prompt_blind)
        self.addState(CC_COUNT_REASON_CODE,     self.get_reason_code)
        self.addState(CC_COUNT_SEND_ITEM_COUNT, self.send_item_count)
        
    #----------------------------------------------------------
    def get_assignment(self):
        ''' get count information for location '''
        
        self.dynamic_vocab.curr_assignment = None
        scanned = int(self._scanned)
        if self._location == '':
            scanned = ''
        
        result = self._assignment_lut.do_transmit(scanned,
                                                  self._location,
                                                  self._check_digits)
    
        if result != 0:
            if result < 0 or self.callingTask.name == PICK_PROMPT_TASK_NAME:
                self.next_state = CC_COUNT_GET_ASSIGNMENT
            else:
                self.taskRunner.return_to(self.callingTask.name, CYCLE_COUNT_NEXT_LOC)            
        elif self._assignment_lut[0]['slot'] == '':
            prompt_only(itext('cycle.count.invalid.location'))
            self.next_state = ''
        else:
            #check if multiple items
            for assignment in self._assignment_lut:
                if assignment['item_number'] != self._assignment_lut[0]['item_number']:
                    self._multi_items = True
                    break
            
            #initialize iterator
            self._assignment_iterator = iter(self._assignment_lut)
            
    #----------------------------------------------------------
    def next_assignment(self):
        ''' get next assignment '''
        try:
            self._curr_assignment = next(self._assignment_iterator)
        except:
            self.next_state = ''
            return
        
        if self._curr_assignment['errorCode'] not in [0, 99]:
            self.next_state = CC_COUNT_NEXT_ASSIGNMENT
        else:
            self.dynamic_vocab.curr_assignment = self._curr_assignment 
            self._actual_quantity = -1
            if self._skipped:
                self._actual_quantity = self._curr_assignment['expected_qty']
                self.next_state = CC_COUNT_SEND_ITEM_COUNT
                
        self._invalid_count = 0
        if self.next_state is None:
            self.force_refresh = True
            set_display_page(self, SHOW_TIE_BY_HIGH_HTML)
            
        
    #----------------------------------------------------------
    def prompt_known(self):
        ''' Prompt type is prompt for Known '''
        if self._curr_assignment['count_mode'] != 'B':
            prompt_key = 'cycle.count.prompt.known'
            if self._multi_items:
                prompt_key = 'cycle.count.prompt.known.item'
            
            if prompt_words(itext(prompt_key,
                                   self._curr_assignment['expected_qty'],
                                   self._curr_assignment['uom'],
                                   self._curr_assignment['item_number']),
                            True, {'yes' : False, 'no' : False}) == 'yes':
                self._actual_quantity = self._curr_assignment['expected_qty']
                self.next_state = CC_COUNT_SEND_ITEM_COUNT
    
    #----------------------------------------------------------
    def prompt_blind(self):
        ''' prompt operator for how many '''
        #only prompt to recount if at least 1 count has been entered
        if self._actual_quantity >= 0:
            if self._curr_assignment['aisle'] == '':
                key = 'cycle.location.info.prompt.noaisle.recount'
            else:
                key = 'cycle.count.prompt.blind.recount'    
            prompt_only(itext(key,
                              self._curr_assignment['pre_aisle'],
                              self._curr_assignment['aisle'],
                              self._curr_assignment['post_aisle'],
                              self._curr_assignment['slot']))
        else:
            #initialize actual quantity to expected
            self._actual_quantity = self._curr_assignment['expected_qty']

        prompt_key = 'cycle.count.prompt.blind'
        if self._multi_items:
            prompt_key = 'cycle.count.prompt.blind.item'
            
        qty = prompt_digits(itext(prompt_key,
                                  self._curr_assignment['uom'],
                                  self._curr_assignment['item_number']), 
                                  itext('cycle.count.prompt.blind.help'), 
                                  1, 1000, 
                                  False, False)
        
        #if currently entered quantity does not match expected (or previously entered)
        #then save as previous and re-prompt
        qty = int(qty)
        if self._actual_quantity != qty:
            self._actual_quantity = qty
            self.next_state = CC_COUNT_PROMPT_BLIND
            
        if self.next_state == CC_COUNT_PROMPT_BLIND:
            self._invalid_count += 1
            if self._invalid_count == 1:
                result = self._reserve_locations.do_transmit(self._curr_assignment['location_id'],
                                                             self._curr_assignment['item_number'])
                
                if result == 0:
                    set_display_page(self, SHOW_RESERVE_CHAIN_HTML)
                else:
                    set_display_page(self, SHOW_RESERVE_CHAIN_UNAVAIL_HTML)
                    prompt_only(itext('cycle.reserve.chain.unavailable'))
            
    #----------------------------------------------------------
    def get_reason_code(self):
        ''' if exception occurred then get reason '''
        
        self._reason_code = ''
        if (self._curr_assignment['expected_qty'] >= 0 and
            self._curr_assignment['expected_qty'] != self._actual_quantity):
            result = self._reason_code_lut.do_transmit('9', '1')
            if result != 0:
                self.next_state = CC_COUNT_REASON_CODE
            elif len(self._reason_code_lut) == 1: #only 1 returned so use it 
                if (self._reason_code_lut[0]['code'] != ''):
                    self._reason_code = self._reason_code_lut[0]['code']    
            else:
                if (self._reason_code_lut[0]['code'] != ''):
                    set_display_page(self, SHOW_REASON_CODES_HTML)
                    self._reason_code = prompt_list_lut(self._reason_code_lut, 
                                                        'code', 'description',
                                                        itext('cycle.reason', 
                                                              self._curr_assignment['expected_qty'],
                                                              self._actual_quantity), 
                                                        itext('cycle.reason.help'))
            
    #----------------------------------------------------------
    def send_item_count(self):
        ''' send count ODR '''
        self._item_count_odr.do_transmit(self._curr_assignment['location_id'],
                                         self._curr_assignment['item_number'],
                                         self._actual_quantity,
                                         self._curr_assignment['uom'],
                                         'Y' if self._skipped else 'N',
                                         self._reason_code)
        self.next_state = CC_COUNT_NEXT_ASSIGNMENT
        
    #----------------------------------------------------------
    def get_data_map(self, page_name):
        ''' custom method to return JSON-dumpable dict of info '''
        map = {}
        
        map['title'] = self.title
        map['operator'] = self.operator_name
        
        if page_name == SHOW_TIE_BY_HIGH_HTML:
            item = {}
            item['item_number'] = self._curr_assignment['item_number']
            item['location'] = self._curr_assignment['location_id']
            item['description'] = self._curr_assignment['item_description']
            item['tie'] = self._curr_assignment['tie']
            item['high'] = self._curr_assignment['high']
            item['image'] = 'host_image/' + str(self._curr_assignment['image'])
            if self._curr_assignment['count_mode'] != 'B':
                item['quantity'] = self._curr_assignment['expected_qty']
            else:
                item['quantity'] = "--"
            
            map['current_item'] = item
            
        elif page_name == SHOW_REASON_CODES_HTML:
            reasons = []
            for reason in self._reason_code_lut:
                temp_reason = {}
                temp_reason['code'] = reason['code']
                temp_reason['description'] = reason['description']
                reasons.append(temp_reason)
            
            map['reasons'] = reasons
        
        elif page_name == SHOW_RESERVE_CHAIN_HTML:
            reserves = []
            for reserve in self._reserve_locations:
                temp_reserve = {}
                temp_reserve['location'] = reserve['location_id']
                temp_reserve['pallet'] = reserve['palletID']
                temp_reserve['item'] = reserve['item']
                temp_reserve['quantity'] = reserve['quantity']
                reserves.append(temp_reserve)
            map['aisle'] = self._curr_assignment['aisle']
            map['slot'] = self._curr_assignment['slot']
            map['reserves'] = reserves

        return map
