#---------------------------------------------------------------
#Cycle Count
CC_TASK_NAME = "cycleCounting"

#States - Cycle Count
REGIONS                 = "Regions"
VALIDATE_REGION         = "validateRegion"
CYCLE_COUNT_MODE        = "getMode"
CYCLE_COUNT_NEXT_LOC    = "nextLocation"
CYCLE_COUNT_GET_LOC     = "getLocation"
CYCLE_COUNT_COUNT_ITEM  = "countItem"
CYCLE_COUNT_NO_MORE     = "noMore"

#---------------------------------------------------------------
#Cycle Counting Count Item
CC_COUNT_ITEM_TASK_NAME = 'cycleCountCountItem'

#States - Cycle Counting Count Item
CC_COUNT_GET_ASSIGNMENT     = 'ccCountGetAssignment'
CC_COUNT_NEXT_ASSIGNMENT    = 'ccCountNextAssignment'
CC_COUNT_PROMPT_KNOWN       = 'ccCountPromptKnown'
CC_COUNT_PROMPT_BLIND       = 'ccCountPromptBlind'
CC_COUNT_SEND_ITEM_COUNT    = 'ccCountSendItemCount'
CC_COUNT_REASON_CODE        = 'customCycleCountReasonCode'

#---------------------------------------------------------------
#Direct to location
CC_DIRECT_TO_LOCATION_TASK_NAME = 'cycleCountDirectToLocation'

#States - Direct to location
CC_LOCATION_PREAISLE        = 'ccLocationPreAisle'
CC_LOCATION_AISLE           = 'ccLocationAisle'
CC_LOCATION_POSTAISLE       = 'ccLocationPostAisle'
CC_LOCATION_VALIDATE_SLOT   = 'ccLocationValidateSlot'


#===============================================================
#Screens - Cycle counting
DEFAULT_HTML                    = 'waiting_for_item'
SHOW_TIE_BY_HIGH_HTML           = 'item'
SHOW_REASON_CODES_HTML          = 'reason_code_list'
SHOW_RESERVE_CHAIN_HTML         = 'reserve_chain'
SHOW_RESERVE_CHAIN_UNAVAIL_HTML = 'unavail_reserve_chain'
PAGE_TITLE                      = 'Inventory Control'
OPERATOR_NAME                   = 'No Operator'

