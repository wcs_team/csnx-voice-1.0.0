from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_ready, prompt_only, prompt_digits_required, prompt_yes_no, prompt_alpha_numeric
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut
from common.RegionSelectionTasks import MultipleRegionTask

from loading.LoadingActions import LoadContainerTask, UnloadContainerTask, ConsolidateContainerTask
from loading.LoadingVocabulary import LoadingVocabulary
from loading.LoadingPrintReportTask import LoadingPrintReportTask
from Globals import change_function
from loading.SharedConstants import LOADING_TASK_NAME, REGIONS,\
    CHECK_AUTHORIZED_REGIONS, REQUEST_ROUTE, PRINT_ROUTE, INITIALIZE_ROUTE, TRAILER_ID,\
    GET_CONTAINER, LOAD, ROUTE_COMPLETE_PRINT, CHECK_COMPLETE

#===========================================================
#Custom Container LUT
#===========================================================
class ContainerLut(class_factory.get(VoiceLinkLut)):
    
    def __init__(self, command):
        super(ContainerLut, self).__init__(command)
        self.containers = {}
        self.__dups = []

    def get_container_record(self, container, scanned = False):
        ''' get a container record received from host
        
        Parameters:
                container - full or partial container number to look up
                scanned (Default=False) - indicates if full or partial True = Full
                
        returns: Container record of container requested, none if not found
        '''
        self.__dups = []
        
        #see if container currently exists
        
        con = self._find_container(container, scanned);
        
        #not found so do transmit, append records to dictionary, and check again
        if con is None:
            result = self.do_transmit(container, int(not scanned))
            if result > 0:
                self.next_state = GET_CONTAINER
            else:
                for record in self:
                    self.containers[record['number']] = record
                
                con = self._find_container(container, scanned);

                #if still no record tell operator
                if con is None and container != '':
                    prompt_ready(itext('loading.container.not.found', container))
                    
        #return container record, none if not found
        return con
    
            
    def _find_container(self, container, scanned):
        ''' find a container record in current set of containers
        
        Parameters:
                container - full or partial container number to look up
                scanned - indicates if full or partial True = Full
                
        returns: Container record of container requested, none if not found
        '''
        record = None
        
        if scanned:
            if container in list(self.containers.keys()):
                record = self.containers[container]
        else:
            count = 0
            foundCon = None
            for con in list(self.containers.keys()):
                if self.containers[con]['partial_number'] == container:
                    foundCon = con
                    count += 1
                    
            if count > 1: #multiple containers found
                foundCon = None
                for con in list(self.containers.keys()):
                    if con not in self.__dups and self.containers[con]['partial_number'] == container:
                        self.__dups.append(con)
                        if prompt_yes_no(itext('loading.confirm.multiple', con), 
                                                    True):
                            foundCon = con
                            break
        
            if foundCon is not None:
                record = self.containers[foundCon]
        
        return record
    
    def clear(self):
        self.containers = {}
    

#===========================================================
#Main Loading Task
#===========================================================
class LoadingTask(class_factory.get(TaskBase)):
    ''' Loading task for the voicelink system
    
    Steps:
            1. Get region
            2. Validate region
            3. Request Route
            4. Print Route
            5. Initialize Route
            6. get Container
            7. Load container
            8. check if complete
    '''

    #-------------------------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        super(LoadingTask, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = LOADING_TASK_NAME

        #define LUTs
        self._valid_regions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingValidRegions')
        self._request_reqion_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingRequestRegion')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingRegionConfiguration')
        self._request_route_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingRequestRoute')
        self._containers_lut = obj_factory.get(ContainerLut, 'prTaskLUTLoadingRequestContainer')
        self._complete_route_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingCompleteRoute')
        
        #Holder for container record currently being worked 
        self.route_complete = False
        self.container_rec = None
        self.trailer_id = ''
        self.region_selected = False
        
        self.dynamic_vocab = obj_factory.get(LoadingVocabulary)
        self.dynamic_vocab.loading_task = self
        
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #set states
        self.addState(REGIONS, self.regions)
        self.addState(CHECK_AUTHORIZED_REGIONS, self.check_authorized)
        self.addState(REQUEST_ROUTE, self.request_route)
        self.addState(PRINT_ROUTE, self.print_route)
        self.addState(INITIALIZE_ROUTE, self.initialize_route)
        self.addState(TRAILER_ID, self.trailer_id_capture)
        self.addState(GET_CONTAINER, self.get_container)
        self.addState(LOAD, self.load_container)
        self.addState(ROUTE_COMPLETE_PRINT, self.route_complete_print)
        self.addState(CHECK_COMPLETE, self.check_complete)
        
    #-------------------------------------------------------------------------
    def regions(self):
        ''' Run multi region selection, but only allow for 1 region '''
        
        #Reset dynamic vocab properties
        self.dynamic_vocab.route_number = None
        self.dynamic_vocab.container_lut = None
        self.dynamic_vocab.region_config_lut = None
        self._containers_lut.clear()
        
        #launch region task
        self.launch(MultipleRegionTask(self.taskRunner, self, True))

    #-------------------------------------------------------------------------
    def check_authorized(self):
        '''Checks if the user is  '''
        #check for valid regions
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
        
        if not valid:
            prompt_only(itext('generic.regionNotAuth.prompt'))
            self.next_state = ''
        else:
            self.region_selected = True
            result = prompt_ready(itext('loading.start.loading'), False,
                            {'change function' : False,
                             'change region' : False})

            if result == 'change function':
                change_function()
                self.next_state = CHECK_AUTHORIZED_REGIONS
            elif result == 'change region':
                self.change_region()
                self.next_state = CHECK_AUTHORIZED_REGIONS
    
    #-------------------------------------------------------------------------     
    def request_route(self):
        '''
           Request for route. If region specified has auto issuance set user will not  
           be prompted for route number else will take route number input from user and 
           requests route. 
        '''
        
        #clear existing containers
        self._containers_lut.containers.clear()
        
        route_number = ''
        if  self._region_config_lut[0]['route_issuance'] ==  1:
            route_number =  prompt_alpha_numeric(itext("loading.request.route.prompt"), 
                                                 itext("loading.request.route.help"), 1, 50, True, False, 
                                                 {'change function' : False,
                                                  'change region' : False})

            if route_number == 'change function':
                change_function()
                self.next_state = REQUEST_ROUTE
                return
            elif route_number == 'change region':
                self.change_region()
                self.next_state = REQUEST_ROUTE
                return
            
        result = -1
        while result < 0:
            result = self._request_route_lut.do_transmit(route_number)
            
        if result > 0:
            self.next_state = REQUEST_ROUTE
        else:
            self.dynamic_vocab.route_number = self._request_route_lut[0]['route_number']
            self.dynamic_vocab.region_config_lut = self._region_config_lut
             
    #-------------------------------------------------------------------------        
    def print_route(self):
        '''
           If region has auto issuance set, a report will be printed for requested route
        '''
        if  self._region_config_lut[0]['route_issuance'] !=  1:
            self.launch(obj_factory.get(LoadingPrintReportTask,
                                           self._region_config_lut,
                                           self.taskRunner, self))
    
    #-------------------------------------------------------------------------    
    def initialize_route(self):
        '''
            Initializes the route. If summary prompt is set prompt is spoken.
            If door check digits are specified they will be verified. if capture 
            trailer id is specified a user input for the trailer if is verified.
        '''
        
        #verifying the summary prompt
        if self._request_route_lut[0]['summary_prompt'] != '':
            message = self._request_route_lut[0]['summary_prompt']
            prompt_ready(itext('generic.continue.prompt', message))
        
        #verifying the door check digit if is set else prompting a generic message 
        #to continue say ready
        door = self._request_route_lut[0]['door_number']
        check_digits = self._request_route_lut[0]['door_check_digit']
        if check_digits != '':
            prompt_digits_required(itext("loading.initialize.route.door.prompt",door),
                                   itext("loading.initialize.route.checkdigit.help"), 
                                   [check_digits])
        else:
            prompt_ready(itext('loading.initialize.route.door.prompt', door))
        
        #initialize dynamic vocabulary
        self.dynamic_vocab.container_lut = self._containers_lut
    #-------------------------------------------------------------------------        
    def trailer_id_capture(self):
        ''' capture trailer id '''
        #prompting for trailer is if capture trailer id is set
        self.trailer_id = ''
        if self._request_route_lut[0]['capture_trailer_id']:
            self.trailer_id = prompt_alpha_numeric(itext("loading.initialize.route.trailerid.prompt"),
                                                   itext("loading.initialize.route.trailerid.help"), 
                                                   self._region_config_lut[0]['trailer_id_digits'],
                                                   self._region_config_lut[0]['trailer_id_digits'], 
                                                   True)
            trailer_id_value = self._request_route_lut[0]['trailer_id']
            spoken_trailer_id = trailer_id_value[len(trailer_id_value) - self._region_config_lut[0]['trailer_id_digits'] : 
                                                 len(trailer_id_value)]
            if self.trailer_id == spoken_trailer_id:
                self.next_state = GET_CONTAINER
            else:
                prompt_only(itext('loading.initialize.route.trailerid.wrong.prompt', self.trailer_id))
                self.next_state = TRAILER_ID
                        
        #initialize dynamic vocabulary
        self.dynamic_vocab.container_lut = self._containers_lut
    #-------------------------------------------------------------------------            
    def get_container(self):
        ''' request a container to load '''
        
        
        self.container_rec = None
        container_num, is_scanned = prompt_alpha_numeric(itext('loading.main.container.prompt'), 
                                                       itext('loading.main.container.help'), 
                                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                                       False,
                                                       True,
                                                       {'unload container' : False, 
                                                        'consolidate' : False, 
                                                        'complete route' : False})
        
        if container_num == 'consolidate':
            if self._region_config_lut[0]['max_consolidation'] <= 1:
                prompt_ready(itext('loading.consolidation.not.allowed'))
                self.next_state = GET_CONTAINER
            else:
                self.launch(obj_factory.get(ConsolidateContainerTask,
                                               self._containers_lut, 
                                               self._region_config_lut,
                                               self.taskRunner, self), 
                            CHECK_COMPLETE) 
        elif container_num == 'unload container':
            self.launch(obj_factory.get(UnloadContainerTask,
                                           self._containers_lut, 
                                           self._region_config_lut,
                                           self.taskRunner, self), 
                        CHECK_COMPLETE) 
        elif container_num == 'complete route':
            result = -1
            while result < 0:
                result = self._complete_route_lut.do_transmit(self._request_route_lut[0]['route_id'])
            
            if result == 0: #Route complete request successful
                self.route_complete = True
                self.next_state = ROUTE_COMPLETE_PRINT
            else:
                self.next_state = GET_CONTAINER
                
        else:
            self.container_rec = self._containers_lut.get_container_record(container_num, is_scanned)
            if self.container_rec is None:
                self.next_state = GET_CONTAINER
                
    #-------------------------------------------------------------------------            
    def load_container(self):
        ''' load a specified container '''
        
        self.launch(obj_factory.get(LoadContainerTask,
                                      self.container_rec['number'], 
                                      self.container_rec['capture_position'], 
                                      self.trailer_id, 
                                      self.taskRunner, self))
            
    #------------------------------------------------------------------------- 
    def route_complete_print(self):
        ''' print the truck diagram after completing the route '''
                
        #Update container if one was selected
        if self.container_rec is not None:
            self.container_rec['status'] = 'L'
            
        #check if route was completed
        if self.route_complete and self._region_config_lut[0]['route_issuance'] ==  1:            
            self.launch(obj_factory.get(LoadingPrintReportTask,
                                           self._region_config_lut,
                                           self.taskRunner, self))
        else:
            self.next_state = GET_CONTAINER
            
    #------------------------------------------------------------------------- 
                   
    def check_complete(self):
        ''' Updates containers and checks if route was completed '''
        
        #check if route was completed
        if self.route_complete:
            #Reset dynamic vocab properties
            self.dynamic_vocab.route_number = None
            self.dynamic_vocab.container_lut = None

            result = prompt_ready(itext('loading.route.complete'), False,
                            {'change function' : False,
                             'change region' : False})
             
            if result == 'change function':
                change_function()
                self.next_state = CHECK_COMPLETE
            elif result == 'change region':
                self.change_region()
                self.next_state = CHECK_COMPLETE
            else:
                self.route_complete = False
                self.next_state = REQUEST_ROUTE
                self._containers_lut.clear()
            
        else:
            self.next_state = GET_CONTAINER

        
    #======================================================
    #Global/Dynamic Word Functions
    #-------------------------------------------------------------------------            
    def change_route(self):
        ''' check if operator wants to change route '''
        if prompt_yes_no(itext('generic.correct.confirm', 'change route'), True):
            self.return_to(self.name, REQUEST_ROUTE)

    def change_region(self):
        if self.region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self.region_selected = False
                self.return_to(self.name, REGIONS)
