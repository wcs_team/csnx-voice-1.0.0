from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary
from vocollect_core.dialog.functions import prompt_only
from vocollect_core import itext, obj_factory, class_factory


from loading.LoadingPrintReportTask import LoadingPrintReportTask

class LoadingVocabulary(class_factory.get(DynamicVocabulary)):
    ''' Definition of dynamic vocabulary available when running loading '''

    def __init__(self):
        self.vocabs = {'how much more':    obj_factory.get(Vocabulary,'how much more', self._how_much_more, False, False),
                       'route number':     obj_factory.get(Vocabulary,'route number', self._route_number, False, False),
                       'reprint report':   obj_factory.get(Vocabulary,'reprint report', self._reprint_report, False, False),
                       'change route':     obj_factory.get(Vocabulary,'change route', self._change_route, False, False)
                       }
        
        self.container_lut = None
        self.region_config_lut = None
        self.route_number = None
        self.loading_task = None
        
    def _valid(self, vocab):
        ''' determine if vocabulary word if valid at this time
            Parameters:
                    vocab - vocabulary to verify
                    
            return: True if valid otherwise false
        '''
        if vocab == 'how much more' and self.container_lut is not None:
            return True        
        if vocab == 'route number' and self.route_number is not None:
            return True
        elif vocab == 'reprint report' and self.route_number is not None:
            return True
        elif vocab == 'change route' and self.route_number is not None:
            return True
        
        return False
    
    #Functions to execute words? 
    def _how_much_more(self):
        ''' speak how much more of route still needs loaded and is known '''
        container_count = 0
        
        #Sending the request container LUT to updated list of containers
        self.container_lut.get_container_record('')
        
        for container in list(self.container_lut.containers.values()): 
            if container['status'] == 'N':
                container_count += 1
                
        prompt_only(itext('loading.how.much.more', container_count), False)
        return True
    
    def _route_number(self):
        ''' speak the current route number '''
        prompt_only(itext('loading.route.number', self.route_number), False)
        return True
        
    def _reprint_report(self):
        ''' reprint route report '''
        self.loading_task.launch(obj_factory.get(LoadingPrintReportTask,
                                                    self.region_config_lut,
                                                    self.loading_task.taskRunner, 
                                                    self.loading_task),
                                 self.loading_task.current_state)
    
    def _change_route(self):
        ''' check if operator wants to change route '''
        self.loading_task.change_route()
        return True
