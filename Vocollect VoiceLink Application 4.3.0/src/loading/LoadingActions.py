from vocollect_core.task.task import TaskBase
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core.dialog.functions import prompt_only, prompt_digits, prompt_yes_no,\
    prompt_alpha_numeric
from vocollect_core import itext, class_factory, obj_factory
from loading.SharedConstants import LOADING_LOAD_TASK_NAME, CONTAINER_LOAD,\
    LOADING_UNLOAD_TASK_NAME, UNLOAD_CONTAINER, LOADING_CONSOLIDATE_TASK_NAME,\
    CONSOLIDATE_MASTER, CONSOLIDATE_CONSOLIDATE, GET_CONTAINER

#===========================================================
#Base Load Action Task
#===========================================================
class LoadActionTask(class_factory.get(TaskBase)):
    
    def __init__(self, taskRunner = None, callingTask = None):
        ''' Base object for loading actions 
        Constructor Parameters:
                taskRunner (Default=None) - main task runner object
                callingTask (Default=None) - task if any that called this task 
        '''
        super(LoadActionTask, self).__init__(taskRunner, callingTask)

        #working variables
        self.master_container = ''
        self.status = ''
        self.container = ''
        self.trailer_id = ''
        self.position = ''
        self._route_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingUpdateContainer')
        
    def _send_action(self):
        ''' method to transmit container update lut '''
        result = -1
        while result < 0: 
            result = self._route_lut.do_transmit(self.status, 
                                                self.container,
                                                self.master_container, 
                                                self.position, 
                                                self.trailer_id)
            
        #Check route complete flag
        if (result == 0 
            and self._route_lut[0]['route_complete'] 
            and self.callingTask is not None):
            self.next_state = ''
            
        return result

#===========================================================
#Load Container Task
#===========================================================

class LoadContainerTask(class_factory.get(LoadActionTask)):

    def __init__(self, container, get_position = False, trailer_id='', 
                 taskRunner = None, callingTask = None):
        ''' Task for loading a container
        Constructor Parameters:
                container - container number being loaded
                get_position (Default=False) - should position be prompted for
                trailer_id (Default='') - Trailer ID
                taskRunner (Default=None) - main task runner object
                callingTask (Default=None) - task if any that called this task 
        '''
        super(LoadContainerTask, self).__init__(taskRunner, callingTask)
        self.status = 'L'
        self.container = container
        self.trailer_id = trailer_id
        self.get_position = get_position

        #Set task name
        self.name = LOADING_LOAD_TASK_NAME
        
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''        
        self.addState(CONTAINER_LOAD, self.load_container)
        
    #-------------------------------------------------------------------------
    def load_container(self):
        ''' Load a container '''

        #Get position if needed
        self.position = ''
        if self.get_position:
            self.position = prompt_digits(itext('loading.position'), 
                                          itext('loading.position.help'), 
                                          2, 2, True, False,
                                          {'left' : True, 'right' : True})

        #transmit and check results
        result = self._send_action()
        if result > 0 :
            self.next_state = GET_CONTAINER
#===========================================================
#Unload Container Task
#===========================================================

class UnloadContainerTask(class_factory.get(LoadActionTask)):

    def __init__(self, container_lut, region_lut, 
                 taskRunner = None, callingTask = None):
        ''' Task for unloading a container
        Constructor Parameters:
                container_lut - container Lut
                region_lut - region Lut
                taskRunner (Default=None) - main task runner object
                callingTask (Default=None) - task if any that called this task 
        '''
        super(UnloadContainerTask, self).__init__(taskRunner, callingTask)
        self.status = 'N'
        self._containers_lut = container_lut
        self._region_config_lut = region_lut
        
        #Set task name
        self.name = LOADING_UNLOAD_TASK_NAME
        
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''        
        self.addState(UNLOAD_CONTAINER, self.unload_container)
        
    #-------------------------------------------------------------------------
    def unload_container(self):
        ''' unload a container '''
        container_num, is_scanned = prompt_alpha_numeric(itext('loading.main.container.unload.prompt'), 
                                       itext('loading.main.container.unload.help'), 
                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                       False,
                                       True,
                                       {'cancel' : False})
        if container_num == 'cancel':
            #operator spoke no-more so just exit
            self.next_state = ''
        else:
            container_rec = self._containers_lut.get_container_record(container_num, is_scanned)
            if container_rec is None:
                self.next_state = UNLOAD_CONTAINER
            else:
                self.container = container_rec['number']
                if self._send_action() == 0:
                    container_rec['status'] = 'N'

#===========================================================
#Unload Container Task
#===========================================================
class ConsolidateContainerTask(class_factory.get(LoadActionTask)):

    def __init__(self, container_lut, region_lut, 
                 taskRunner = None, callingTask = None):
        ''' Task for Consolidating containers
        Constructor Parameters:
                container_lut - container Lut
                region_lut - region Lut
                taskRunner (Default=None) - main task runner object
                callingTask (Default=None) - task if any that called this task 
        '''
        super(ConsolidateContainerTask,self).__init__(taskRunner, callingTask)
        self.status = 'C'
        self._containers_lut = container_lut
        self._region_config_lut = region_lut
        
        self.consolidate_count = 1
        #Set task name
        self.name = LOADING_CONSOLIDATE_TASK_NAME
        
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''        
        self.addState(CONSOLIDATE_MASTER, self.get_master_container)
        self.addState(CONSOLIDATE_CONSOLIDATE, self.get_consolidate_container)
        
    #-------------------------------------------------------------------------
    def get_master_container(self):
        ''' get main container to consolidate to '''
        container_num, is_scanned  = prompt_alpha_numeric(itext('loading.main.container.main.prompt'), 
                                       itext('loading.main.container.main.help'), 
                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                       False,
                                       True,
                                       {'no more' : False})

        if container_num == 'no more':
            #operator spoke no-more so just exit
            self.next_state = ''
        else:
            con_rec = self._containers_lut.get_container_record(container_num, is_scanned)
            
            if con_rec is None:
                #container was not found re prompt for container
                self.next_state = CONSOLIDATE_MASTER
            else:
                #save master container number
                self.master_container = con_rec['number']
        
            #initialize count
            self.consolidate_count = 1

    #-------------------------------------------------------------------------
    def get_consolidate_container(self):
        ''' method to select consolidate container number and transmit '''
        container_num, is_scanned  = prompt_alpha_numeric(itext('loading.main.container.consolidate.prompt'), 
                                       itext('loading.main.container.consolidate.help'), 
                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                       self._region_config_lut[0]['container_digits_oper_speaks'], 
                                       False,
                                       True,
                                       {'no more' : False})
        
        if container_num == 'no more':
            #operator spoke no-more so just exit
            self.next_state = ''
        else:
            con_rec = self._containers_lut.get_container_record(container_num, is_scanned)

            if con_rec is None:
                self.next_state = CONSOLIDATE_CONSOLIDATE
            elif con_rec['number'] == self.master_container:
                #Container not allowed to be consolidated
                prompt_only(itext('loading.consolidation.status.error'))
                self.next_state = CONSOLIDATE_CONSOLIDATE
            else:
                #check if correct information was entered
                spokenContainerDigits = self._region_config_lut[0]['container_digits_oper_speaks']
                if prompt_yes_no(itext('loading.consolidation.confirm',
                                            con_rec['number'][-spokenContainerDigits:],
                                            self.master_container[-spokenContainerDigits:])):
                    self.container = con_rec['number']
                    result = self._send_action()
                    if result == 0:
                        #if no errors increment count and set status of container
                        self.consolidate_count += 1
                        con_rec['status'] = 'C'
                        if self._route_lut[0]['route_complete']:
                            return
                                    #check if we are allowed to consolidate more
                        if self.consolidate_count < self._region_config_lut[0]['max_consolidation']:
                            self.next_state = CONSOLIDATE_CONSOLIDATE
                        else:
                            self.next_state = ''
                    else:
                        if self.consolidate_count == 1:
                            self.next_state = CONSOLIDATE_MASTER
                        elif self.consolidate_count > 1:
                            self.next_state = CONSOLIDATE_CONSOLIDATE
                        else:
                            self.next_state = ''

            
