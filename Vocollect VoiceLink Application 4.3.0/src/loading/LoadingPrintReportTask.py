from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core import itext, class_factory, obj_factory
from vocollect_core.dialog.functions import prompt_only, prompt_ready,\
    prompt_alpha_numeric
from core.Print import PrintTask
from loading.SharedConstants import LOADING_PRINT_TASK_NAME, ENTER_PRINTER,\
    CONFIRM_PRINTER, XMIT_PRINT_REPORT, CONFIRM_REPORT, LOADING_TASK_NAME


#===========================================================
#Main Loading Task
#===========================================================
class LoadingPrintReportTask(class_factory.get(PrintTask)):
    ''' Loading task for Printing Route Report
    
    Steps:
            1. Enter Printer Number
            2. Confirm Printer Number
            3. Print Route report
            4. Confirm Report
    '''

    #-------------------------------------------------------------------------
    def __init__(self, region_lut, taskRunner = None, callingTask = None):
        super(LoadingPrintReportTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = LOADING_PRINT_TASK_NAME
        self._region_config_lut = region_lut
        #define luts
        self._print_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTLoadingPrint')
        
        #additional settings
        self.dynamic_vocab = None #Do not want the loading dynamics
        self.task = self.taskRunner.findTask(LOADING_TASK_NAME)
        if self.task is None:
            self.task = self.callingTask
    
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(ENTER_PRINTER, self.enter_printer)
        self.addState(CONFIRM_PRINTER, self.confirm_printer)
        self.addState(XMIT_PRINT_REPORT, self.xmit_print_report)
        self.addState(CONFIRM_REPORT, self.confirm_report)
          
    #-------------------------------------------------------------------------     
    def xmit_print_report(self):
        ''' print route report '''
        result = self._print_lut.do_transmit(self.callingTask._request_route_lut[0]['route_id'],
                                             self.task.printer_number)
        
        if result < 0:
            self.next_state = XMIT_PRINT_REPORT
        elif result > 0:
            self.next_state = ENTER_PRINTER
             
    #-------------------------------------------------------------------------        
    def confirm_report(self):
        ''' confirm report '''
        if  self._region_config_lut[0]['route_issuance'] !=  1: 
            route_number = prompt_alpha_numeric(itext("loading.request.route.prompt"), 
                                                     itext("loading.request.route.help"), 1, 50, False, False, 
                                                     {'reprint report' : False})
            
            if route_number == 'reprint report':
                self.next_state = ENTER_PRINTER
            else:
                if route_number != self.callingTask._request_route_lut[0]['route_number']:
                    while prompt_ready(itext('generic.wrongValue.ready.prompt', route_number), 
                                        True, {'route number' : False}) == 'route number':
                        prompt_only(itext('loading.route.number', 
                                               self.callingTask._request_route_lut[0]['route_number']), 
                                    False)
                    self.next_state = CONFIRM_REPORT
