#---------------------------------------------------------------
#Loading
LOADING_TASK_NAME = "loading"

#States - Loading
REGIONS                  = "region"
CHECK_AUTHORIZED_REGIONS = "checkAuthorizedRegions"
REQUEST_ROUTE            = "requestRoute"
PRINT_ROUTE              = "printRoute"
INITIALIZE_ROUTE         = "initializeRoute"
TRAILER_ID               = "trailerID"
GET_CONTAINER            = "getContainer"
LOAD                     = "load"
ROUTE_COMPLETE_PRINT     = "routeCompletePrint"
CHECK_COMPLETE           = "checkComplete"

#---------------------------------------------------------------
#Print Task
LOADING_PRINT_TASK_NAME = "loadingPrintReport"

#States - Print Task
ENTER_PRINTER       = 'enterPrinter'
CONFIRM_PRINTER     = 'confirmPrinter'
XMIT_PRINT_REPORT   = 'printReport'
CONFIRM_REPORT      = 'confirmReport'

#---------------------------------------------------------------
#Loading Actions
LOADING_CONSOLIDATE_TASK_NAME = "consolidateContainer"
LOADING_UNLOAD_TASK_NAME = "unloadContainer"
LOADING_LOAD_TASK_NAME = "loadContainer"

#States - Print Task
CONTAINER_LOAD = "containerLoad"
UNLOAD_CONTAINER = "unloadContainer"
CONSOLIDATE_MASTER      = "consolidateMaster"
CONSOLIDATE_CONSOLIDATE = "consolidateConsolidate"

