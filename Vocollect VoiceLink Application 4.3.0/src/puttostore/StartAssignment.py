from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_only, prompt_ready
from vocollect_core import itext, class_factory
from puttostore.SharedConstants import PTS_START_ASSIGNMENT_TASK_NAME,\
    GET_ASSIGNEMNT, ASSIGNMENT_SUMMARY, GET_PUTS

class StartAssignmentTask(class_factory.get(TaskBase)):
    '''Get and start Assignment
    
    Steps:
        1. Get Assignment
        2. Assignment Summary
        3. Get Puts
    '''
 
    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 assignment_lut,
                 pts_lut,
                 taskRunner = None, 
                 callingTask = None):
        super(StartAssignmentTask, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = PTS_START_ASSIGNMENT_TASK_NAME
      
        #LUTS
        self._region_config_lut = region_config_lut
        self._assignment_lut= assignment_lut
        self._pts_lut = pts_lut
        self._reserved_licenses = 0
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get state
        self.addState(GET_ASSIGNEMNT,       self.get_assignment)
        self.addState(ASSIGNMENT_SUMMARY,   self.assignment_summary)
        self.addState(GET_PUTS,             self.get_puts)

    #----------------------------------------------------------  
    def get_assignment(self):
        ''' transmit information for assignment information '''
        result = self._assignment_lut.do_transmit()
        
        if result != 0:
            self.next_state = GET_ASSIGNEMNT

    #----------------------------------------------------------  
    def assignment_summary(self):
        prompt_key = 'pts.assignment.loc'
        if self._assignment_lut[0]['totalSlots'] != 1:        
            prompt_key += '.plural'
        prompt_key += '.item'
        if self._assignment_lut[0]['totalItems'] != 1:
            prompt_key += '.plural'
        if self._assignment_lut[0]['expectedResidual'] > 0:
            prompt_key += '.residual'
            
        result = prompt_ready(itext(prompt_key, 
                                    self._assignment_lut[0]['totalSlots'],
                                    self._assignment_lut[0]['totalItems'],
                                    self._assignment_lut[0]['expectedResidual']), 
                              additional_vocab = {'performance': False})
        
        if result == 'performance':
            if self._assignment_lut[0]['perfLast'] != -1:
                prompt_ready(itext('pts.assignment.performance',
                                   self._assignment_lut[0]['perfLast'], 
                                   self._assignment_lut[0]['perfDaily']))
            else:
                prompt_only(itext('pts.assignment.performance.na'))
            self.next_state = ASSIGNMENT_SUMMARY
                
    #----------------------------------------------------------
    def get_puts(self):
        ''' transmit request for put information '''
        result = self._pts_lut.do_transmit(self._assignment_lut[0]['groupId'])
        
        if result != 0:
            self.next_state = GET_PUTS
