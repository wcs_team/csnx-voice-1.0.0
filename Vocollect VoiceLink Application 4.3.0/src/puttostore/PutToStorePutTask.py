from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_ready, prompt_digits_required, prompt_digits,\
    prompt_yes_no, prompt_only, prompt_alpha_numeric
from vocollect_core import itext, obj_factory, class_factory
from puttostore.PutToStoreLuts import Put
from puttostore.PutToStoreCloseContainerTask import PutToStoreCloseContainer
from puttostore.PutToStoreNewContainerTask import PutToStoreNewContainer
from common.VoiceLinkLut import VoiceLinkLutOdr
from puttostore.SharedConstants import PTS_PUT_TASK_NAME, VERIFY_SLOT,\
    PUT_PROMPT, VALIDATE_CONTAINER, TRANSMIT_PUT, FINISH_PUT

class PutToStorePutTask(class_factory.get(TaskBase)):
    '''Induct License task for put to store
    
    Steps:
        1. Verify Slot
        2. Put Prompt (get Quantity)
        3. Validate Container
        4. Transmit Put Information
        5. Finish Put
    '''
 
    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 assignment_lut,
                 pts_lut,
                 current_put,
                 taskRunner = None, 
                 callingTask = None):
        super(PutToStorePutTask, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = PTS_PUT_TASK_NAME
      
        self._region_config_lut = region_config_lut
        self._assignment_lut = assignment_lut
        self._pts_lut = pts_lut
        self._current_put = current_put
        self.dynamic_vocab.current_put = current_put
        
        #luts
        self._put_lut = obj_factory.get(Put, 'prTaskLUTPtsPut')
        
        #working variables
        self._qty_put = 0
        self._is_partial = False
        self._is_short = False
        self._container_id = ''
        self._validate_container = False
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' initialize the sates for this task '''
        self.addState(VERIFY_SLOT,          self.verify_slot)
        self.addState(PUT_PROMPT,           self.put_prompt)
        self.addState(VALIDATE_CONTAINER,   self.validate_container)
        self.addState(TRANSMIT_PUT,         self.transmit_put_request)
        self.addState(FINISH_PUT,           self.finish_put)

    #----------------------------------------------------------
    def verify_slot(self):
        ''' verify the slot '''
        additional_vocab = {'skip slot' : False}
        result = ''
        if self._current_put['checkDigits'] == '':
            result = prompt_ready(self._current_put['slot'], 
                                  True, 
                                  additional_vocab)
        else:
            result = prompt_digits_required(self._current_put['slot'], 
                                            itext('pts.put.confirm.slot.help'), 
                                            [self._current_put['checkDigits']], 
                                            [self._current_put['checkDigits']],
                                            additional_vocab)
            result = result[0]
            
        if result == 'skip slot':
            self.next_state = VERIFY_SLOT
            self._skip_slot()
         
    #----------------------------------------------------------
    def put_prompt(self):
        self._is_partial = False
        self._is_short = False
        self._qty_put = 0
        self._container_id = ''
        self._validate_container = self._region_config_lut[0]['validate_container_id']
        
        prompt = 'pts.put.putprompt'
        qtyToPut = self._current_put['quantityToPut'] - self._current_put['quantityPut']
        if self._assignment_lut[0]['totalItems'] > 1:
            prompt += '.item'
            
        result = prompt_digits(itext(prompt, 
                                     str(qtyToPut),
                                     self._current_put['uom'],
                                     self._current_put['itemDescription'].lower()), 
                               itext('pts.put.putprompt.help'), 
                               1, 10, 
                               False, 
                               False,
                               {'close container' : True,
                                'new container' : True } )

        if result == 'close container':
            self._close_container()
        elif result == 'new container':
            self._new_container(False)
        else:
            self._qty_put = int(result)
            if self._qty_put > qtyToPut:
                if prompt_yes_no(itext('pts.put.putptompt.overpack')):
                    if not self._current_put['allowOverPack']:
                        prompt_only(itext('pts.put.putptompt.overpack.notallowed'))
                        self.next_state = PUT_PROMPT
                    elif self._current_put['residualQuantity'] == 0:
                        prompt_only(itext('pts.put.putptompt.overpack.noresidual'))
                        self.next_state = PUT_PROMPT
                    elif self._current_put['residualQuantity'] < self._qty_put - qtyToPut:
                        prompt_only(itext('pts.put.putptompt.overpack.not.enough', 
                                          self._current_put['residualQuantity']))
                        self.next_state = PUT_PROMPT
                else:
                    self.next_state = PUT_PROMPT
            elif self._qty_put < qtyToPut:
                if prompt_yes_no(itext('pts.put.putprompt.partial'), True):
                    self._is_partial = True
                    self._is_short = False
                elif prompt_yes_no(itext('pts.put.putprompt.partial.short'), True):
                    self._is_partial = False
                    self._is_short = True
                else:
                    self.next_state = PUT_PROMPT

    #----------------------------------------------------------
    def validate_container(self):
        ''' Validate container ID if required '''
        if self._validate_container and self._qty_put > 0:
            result = prompt_alpha_numeric(itext('pts.put.container'), 
                                          itext('pts.put.container.help'), 
                                          self._region_config_lut[0]['validate_container_length'], 
                                          self._region_config_lut[0]['validate_container_length'], 
                                          True, 
                                          True,
                                          {'new container' : True})
            if result[0] == 'new container':
                self._new_container(False)
            else:
                self._container_id = result[0]

    #----------------------------------------------------------
    def transmit_put_request(self):
        ''' Transmit put request '''
        result = self._put_lut.do_transmit(self._assignment_lut[0]['groupId'],
                                           self._current_put['customerLocationID'],
                                           self._current_put['itemNumber'],
                                           self._current_put['putID'],
                                           self._qty_put,
                                           self._container_id,
                                           self._current_put['licenseNumber'],
                                           int(self._is_partial))
    
        if result in [2, 3]:
            self._validate_container = True 
            self.next_state = VALIDATE_CONTAINER
        elif result == 1:
            self._new_container()
        elif result != 0:
            self.next_state = TRANSMIT_PUT
            
    #----------------------------------------------------------
    def finish_put(self):
        ''' update appropriate put records and determine next step '''
        self._current_put['quantityPut'] += self._qty_put
        
        if self._is_short:
            for put in self._pts_lut:
                if self._current_put['itemNumber'] == put['itemNumber']:
                    put['status'] = 'P' 
        elif self._is_partial and self._current_put['quantityPut'] < self._current_put['quantityToPut']:
            if self._region_config_lut[0]['allow_multi_containers']:
                if prompt_yes_no(itext('generic.correct.confirm', 'close container')):
                    self._close_container()
                else:
                    self.next_state = PUT_PROMPT
            else:
                self._new_container()
        else:
            self._current_put['status'] = 'P'
        
        #check if residuals need updated
        if self._current_put['quantityToPut'] < self._current_put['quantityPut']:
            residual_used = self._current_put['quantityPut'] - self._current_put['quantityToPut']
            for put in self._pts_lut:
                if self._current_put['itemNumber'] == put['itemNumber']:
                    put['residualQuantity'] -= residual_used


    #----------------------------------------------------------
    def _new_container(self, prompt = True):
        if prompt:
            if self._region_config_lut[0]['system_gen_container_id']:
                prompt_only(itext('pts.put.container.new.system'))
            else:
                prompt_only(itext('pts.put.container.new.operator'))
                
        self.launch(obj_factory.get(PutToStoreNewContainer,
                                      self._region_config_lut,
                                      self._assignment_lut[0]['groupId'],
                                      self._current_put['customerLocationID'],
                                      self.taskRunner, self),
                    PUT_PROMPT)

    #----------------------------------------------------------
    def _close_container(self):
        #launch close container
        self.launch(obj_factory.get(PutToStoreCloseContainer,
                                      self._region_config_lut,
                                      self._current_put['customerLocationID'],
                                      self._container_id, 
                                      self.taskRunner, self),
                    PUT_PROMPT)

    #------------------------------------------------------------
    def _skip_slot(self):
        #check if region allows
        if not self._region_config_lut[0]['allow_skip_slot']:
            prompt_only(itext('generic.skip.slot.notallowed'))
            return

        #check for last slot
        allowed = False
        for put in self._pts_lut:
            if put['customerLocationID'] != self._current_put['customerLocationID']:
                if put['status'] in ['N', 'S']: 
                    allowed = True
        
        #Last slot, not allowed to skip
        if not allowed:
            prompt_only(itext('generic.skip.slot.last'))
            return

        #confirm skip slot
        if prompt_yes_no(itext('generic.skip.slot.confirm')):
            for put in self._pts_lut:
                if put['customerLocationID'] == self._current_put['customerLocationID']:
                    if put['status'] == 'N': 
                        put['status'] = 'S'

            update_status = VoiceLinkLutOdr('prTaskLutPtsUpdateStatus', 
                                            'prTaskODRPtsUpdateStatus', 
                                            self._region_config_lut[0]['use_lut'])
            while update_status.do_transmit(self._assignment_lut[0]['groupId'],
                                            self._current_put['customerLocationID'],
                                            0, 'S') != 0:
                pass
                
            self.next_state = '' 
