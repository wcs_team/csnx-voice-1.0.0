#---------------------------------------------------------------
#PTS Put To Store
PTS_TASK_NAME = "putToStore"

#States - Put To Store
REGIONS             = "putToStoreRegions"
VALIDATE_REGIONS    = "putToStoreNext"
GET_FT_LOCATION     = "getFTLocation"
INDUCT_LICENSE      = "inductLicense"
START_ASSIGNMENT    = "startAssignement"
PUT_ASSIGNMENT      = "putAssignement"
HANDLE_RESIDUALS    = "handleResiduals"
COMPLETE_ASSIGNMENT = "completeAssignment"

#---------------------------------------------------------------
#PTS Assignment
PTS_ASSIGNMENT_TASK_NAME = "putAssignment"

#States - PTS Assignment
PTS_ASSIGNMENT_RESET           = "ptsAssignmentReset"
PTS_ASSIGNMENT_CHECK_NEXT_PUT  = "ptsAssignmentCheckNextPut"
PTS_ASSIGNMENT_PREAISLE        = "ptsAssignmentPreAisle"
PTS_ASSIGNMENT_AISLE           = "ptsAssignmentAisle"
PTS_ASSIGNMENT_POSTAISLE       = "ptsAssignmentPostAisle"
PTS_ASSIGNMENT_PUTPROMPT       = "ptsAssignmentPutPrompt"
PTS_ASSIGNMENT_END_PUTTING     = "ptsAssignemntEndPutting"

#---------------------------------------------------------------
#PTS Start Assignment
PTS_START_ASSIGNMENT_TASK_NAME = "startAssignment"

#States - PTS Start Assignment
GET_ASSIGNEMNT = "getAssignment"
ASSIGNMENT_SUMMARY = "assignmentSummary"
GET_PUTS = "getPuts"


#---------------------------------------------------------------
#PTS Put
PTS_PUT_TASK_NAME = "putToStorePutTask"

#States - Put
VERIFY_SLOT = 'PTSverifySlot'
PUT_PROMPT = 'PTSputPrompt'
VALIDATE_CONTAINER = 'PTSValidateContainer'
TRANSMIT_PUT = 'PTSTransmitPut'
FINISH_PUT = 'PTSFinishPut'


#---------------------------------------------------------------
#PTS Close Container
PTS_CLOSE_CONTAINER_TASK_NAME = "putToStoreCloseContainerTask"

#States - PTS Close Container
PTS_CLOSE_CONT_SPECIFY_LOC    = 'ptsCloseContainerSpecifyLocation'
PTS_CLOSE_CONT_SPECIFY_CD     = 'ptsCloseContainerSpecifyCheckDigits'
PTS_CLOSE_CONT_VERIFY_LOC     = 'ptsCloseContainerVerifyLocation'
PTS_CLOSE_CONT_CLOSE_CONT     = 'ptsCloseContainerCloseContainer'
PTS_CLOSE_CONT_SPECIFY_CONT   = 'ptsCloseContainerSpecifyContainer'
PTS_CLOSE_CONT_SELECT_CONT    = 'ptsCloseContainerSelectContainer'

#---------------------------------------------------------------
#PTS New Container
PTS_NEW_CONTAINER_TASK_NAME = "putToStoreNewContainerTask"

#States - PTS New Container
PTS_NEW_CONT_SPECIFY_CONT   = 'ptsNewContainerSpecifyContainer'
PTS_NEW_CONT_OPEN_CONT      = 'ptsNewContainerOpenContainer'
PTS_NEW_CONT_PRINT          = 'ptsNewContainerPrint'
PTS_NEW_CONT_RETURN_TO      = 'ptsNewContainerReturnTo'

#---------------------------------------------------------------
#PTS PRINT
PTS_PRINT_TASK_NAME = "putToStorePrint"

#States - PTS Print
PRINT_LABELS        = 'printLables'
PRINT_RETRIEVE      = 'printRetrieveLabels'



#---------------------------------------------------------------
#PTS Handle Residuals
PTS_HANDLE_RESIDUALS_TASK_NAME = "putToStoreHandleResiduals"

#States - PTS Handle Residuals
PTS_RESIDUALS_GET_RESIDUALS    = 'ptsResidualsGetResiduals'
PTS_RESIDUALS_PROMPT_RESIDUALS = 'ptsResidualsPromptResiduals'
PTS_RESIDUALS_INIALIZE_LICENSE = 'ptsResidualsInitializeLicense'
PTS_RESIDUALS_GET_NEXT_RESID   = 'ptsResidualsGetNextResid'
PTS_RESIDUALS_PROMPT_RESID_QTY = 'ptsResidualsPromptResidQty'
PTS_RESIDUALS_DELIVER_EXCEPT   = 'ptsResidualsDeliverException'
PTS_RESIDUALS_PRINT_EXCEPT     = 'ptsResidualsPrintException'

#---------------------------------------------------------------
#PTS Induct License
PTS_INDUCT_LICENSE_TASK_NAME = "inductLicense"

#States - PTS Induct License
PROMPT_FOR_LICENSE = "promptForLicense"
RESERVE_LICENSE = "reserveLicense"
SELECT_LICENSE = "selectLicense"
INDUCT_COMPLETE = "inductComplete"


