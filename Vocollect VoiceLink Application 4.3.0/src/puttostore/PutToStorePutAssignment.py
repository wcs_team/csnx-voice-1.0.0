from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_only, prompt_ready, prompt_yes_no
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLutOdr
from puttostore.PutToStorePutTask import PutToStorePutTask
from puttostore.SharedConstants import PTS_ASSIGNMENT_TASK_NAME,\
    PTS_ASSIGNMENT_RESET, PTS_ASSIGNMENT_CHECK_NEXT_PUT, PTS_ASSIGNMENT_PREAISLE,\
    PTS_ASSIGNMENT_AISLE, PTS_ASSIGNMENT_POSTAISLE, PTS_ASSIGNMENT_PUTPROMPT,\
    PTS_ASSIGNMENT_END_PUTTING

class PutAssignmentTask(class_factory.get(TaskBase)):
    '''Get and start Assignment
    
    Steps:
        1. Reset current location information
        2. check if there is another put
        3. Direct to pre-aisle
        4. Direct to aisle
        5. Direct to post-aisle
        6. Launch put prompt task
        7. Check for more puts
    '''
 
    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 assignment_lut,
                 pts_lut,
                 taskRunner = None, 
                 callingTask = None):
        super(PutAssignmentTask, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = PTS_ASSIGNMENT_TASK_NAME
      
        #LUTS
        self._region_config_lut = region_config_lut
        self._assignment_lut = assignment_lut
        self._pts_lut = pts_lut

        #working variables
        self._current_put = None
        
        self._aisle_direction=''
        self._pre_aisle_direction=''
        self._post_aisle_direction=''

    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get state
        self.addState(PTS_ASSIGNMENT_RESET,          self.reset)
        self.addState(PTS_ASSIGNMENT_CHECK_NEXT_PUT, self.check_next_put)
        self.addState(PTS_ASSIGNMENT_PREAISLE,       self.pre_aisle)
        self.addState(PTS_ASSIGNMENT_AISLE,          self.aisle)
        self.addState(PTS_ASSIGNMENT_POSTAISLE,      self.post_asile)
        self.addState(PTS_ASSIGNMENT_PUTPROMPT,      self.put_prompt)
        self.addState(PTS_ASSIGNMENT_END_PUTTING,    self.end_puts)

    #----------------------------------------------------------  
    def reset(self):
        self._aisle_direction=''
        self._pre_aisle_direction=''
        self._post_aisle_direction=''
        
    #----------------------------------------------------------  
    def check_next_put(self):
        ''' check for the next unput put in list of puts '''
        
        self._current_put = None
        
        for put in self._pts_lut:
            if put['status'] == 'N':
                self._current_put = put
                break

        if self._current_put is None:
            self.next_state = PTS_ASSIGNMENT_END_PUTTING
            
    #----------------------------------------------------------  
    def pre_aisle(self):
        ''' directing to Pre Aisle'''
        #if pre-aisle is same as pre-aisle don't prompt
        if self._current_put["preAisle"] != self._pre_aisle_direction:
            if self._current_put["preAisle"] != '':
                prompt_ready(itext('pts.put.assignment.preaisle', 
                                        self._current_put["preAisle"]))

            self._post_aisle_direction=''
            self._aisle_direction=''
            self._pre_aisle_direction = self._current_put["preAisle"]
    
    #----------------------------------------------------------
    def aisle(self):
        ''' directing to Aisle'''
        #if aisle is same as aisle don't prompt
        result = ''
        if self._current_put["aisle"] != self._aisle_direction:
            if self._current_put["aisle"] != '':
                result = prompt_ready(itext('pts.put.assignment.aisle', 
                                            self._current_put["aisle"]), 
                                      False,
                                      {'skip aisle' : False})
                if result == 'skip aisle':
                    self.next_state = PTS_ASSIGNMENT_AISLE
                    self._skip_aisle()

            if result != 'skip aisle':
                self._post_aisle_direction=''
                self._aisle_direction = self._current_put["aisle"]
                


    #----------------------------------------------------------
    def post_asile(self):
        ''' directing to Post Aisle'''
        #if aisle is same as post-aisle don't prompt
        if self._current_put["postAisle"] != self._post_aisle_direction: 
            if self._current_put["postAisle"] != '':
                prompt_ready(itext('pts.put.assignment.postaisle', 
                                        self._current_put["postAisle"]))

            self._post_aisle_direction = self._current_put["postAisle"]

    #----------------------------------------------------------
    def put_prompt(self):
        ''' launch the start assignment task '''
        self.launch(obj_factory.get(PutToStorePutTask,
                                      self._region_config_lut,
                                      self._assignment_lut,
                                      self._pts_lut, 
                                      self._current_put,
                                      self.taskRunner, self),
                    PTS_ASSIGNMENT_CHECK_NEXT_PUT)
        
    #----------------------------------------------------------
    def end_puts(self):
        skipped = False
        for put in self._pts_lut:
            if put['status'] == 'S':
                put['status'] = 'N'
                skipped = True
            
        if skipped:
            prompt_ready(itext('pts.put.assignment.reput.skips'))
            self._update_status("", 2, "N")
            self.next_state = PTS_ASSIGNMENT_CHECK_NEXT_PUT
            
    #----------------------------------------------------------        
    def _skip_aisle(self):
        ''' method to process the skip aisle command when spoken '''
        
        #check if region allows
        if not self._region_config_lut[0]['allow_skip_aisle']:
            prompt_only(itext('generic.skip.aisle.notallowed'))
            return
         
        #check if another aisle
        curr_pre_aisle = self._current_put['preAisle']
        curr_aisle = self._current_put['aisle']
        allowed = False

        for put in self._pts_lut:
            if (put['status'] not in ['P'] 
                and (put['preAisle'] != curr_pre_aisle 
                     or put['aisle'] != curr_aisle)):
                allowed = True
        
        #last aisle not allowed to skip
        if not allowed:
            prompt_only(itext('pts.put.assignment.skip.aisle.last'))
            self._aisle_direction = ''
            return
        
        #confirm skip aisle
        if prompt_yes_no(itext('generic.skip.aisle.confirm')):
            for put in self._pts_lut:
                if put['status'] != 'P':
                    if put['preAisle'] == curr_pre_aisle and put['aisle'] == curr_aisle:
                        put['status'] = 'S'
            
            #send update status
            self._update_status(self._current_put['customerLocationID'],
                                '1', 'S')
            self.next_state = PTS_ASSIGNMENT_CHECK_NEXT_PUT
            
    #----------------------------------------------------------        
    def _update_status(self, location, scope, status):
        ''' sends an update status '''
        #send update status
        update_status = VoiceLinkLutOdr('prTaskLutPtsUpdateStatus', 
                                        'prTaskODRPtsUpdateStatus', 
                                        self._region_config_lut[0]['use_lut'])
        while update_status.do_transmit(self._assignment_lut[0]['groupId'],
                                        location,
                                        scope, status) != 0:
            pass
