'''
This module contains custom Put to Store LUTS that extend the functionality of a 
basic LUT
'''
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core.dialog.functions import prompt_only
from vocollect_core.utilities import class_factory

###############################################################
# Overridden Verify License LUT class 
###############################################################
class VerifyLicense(class_factory.get(VoiceLinkLut)):

    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        if len(self) > 1 and error not in [98, 99]:
            return 0
        else:
            return super(VerifyLicense, self)._process_error(error, message)
    
###############################################################
# Overridden Put LUT class 
###############################################################
class Put(class_factory.get(VoiceLinkLut)):

    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        if error in [1, 2]:
            return error
        elif error == 3:
            prompt_only(message)
            return error
        else:
            return super(Put, self)._process_error(error, message)
    
###############################################################
# Overridden Container LUT class 
###############################################################
class PtsContainers(class_factory.get(VoiceLinkLut)):

    def _process_error(self, error, message):
        ''' override to handle special error codes '''
        if error in [3, 8]:
            return error
        else:
            return super(PtsContainers, self)._process_error(error, message)
    
    def container_count(self):
        count = 0
        for container in self:
            if container['containerNumber'] != '':
                count += 1
        return count
