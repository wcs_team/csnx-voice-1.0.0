from vocollect_core import itext, obj_factory, class_factory
from vocollect_core.dialog.functions import prompt_only, prompt_yes_no, prompt_ready
from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary
from common.VoiceLinkLut import VoiceLinkOdr, VoiceLinkLut
from puttostore.PutToStoreLuts import PtsContainers
from Globals import change_function

import puttostore 
from puttostore.SharedConstants import PTS_TASK_NAME

class PutToStoreVocabulary(class_factory.get(DynamicVocabulary)):
    ''' Definition of additional vocabulary used throughout VoiceLink selection
    
    Parameters
            runner - Task runner object
    '''
    
    def __init__(self, assignment_lut, pts_lut, runner):
        self.vocabs = {'pass assignment':   obj_factory.get(Vocabulary,'pass assignment', self._pass_assignment, False),
                       'repick skips':      obj_factory.get(Vocabulary,'repick skips', self._repick_skips, False),
                       'review containers': obj_factory.get(Vocabulary,'review containers', self._review_containers, True),
                       'license':           obj_factory.get(Vocabulary,'license', self._license, False),
                       'location':          obj_factory.get(Vocabulary,'location', self._location, False),
                       'item number':       obj_factory.get(Vocabulary,'item number', self._item_number, False),
                       'quantity':          obj_factory.get(Vocabulary,'quantity', self._quantity, False),
                       'description':       obj_factory.get(Vocabulary,'description', self._description, False)
                       }

        self.runner = runner
        self.current_put = None
        self.region_config = None
        self._assignment_lut = assignment_lut
        self._pts_lut = pts_lut
        
        #for review containers command
        self.location_id = None
    
        
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''

        current_task = self.runner.get_current_task()

        if vocab == 'pass assignment':
            return current_task.name in ['putAssignment', 'startAssignment', 'putToStorePutTask']
        elif vocab == 'repick skips':
            return current_task.name in ['putAssignment', 'putToStorePutTask']
        elif vocab == 'review containers':
            return self.location_id is not None or current_task.name in ['putToStorePutTask']
        elif vocab == 'license':
            return current_task.name in ['putToStorePutTask']
        elif vocab == 'location':
            return current_task.name in ['putToStorePutTask']
        elif vocab == 'item number':
            return current_task.name in ['putToStorePutTask']
        elif vocab == 'quantity':
            return current_task.name in ['putToStorePutTask']
        elif vocab == 'description':
            return current_task.name in ['putToStorePutTask']

        return False
    
        
    #Functions to execute words? 
    def _pass_assignment(self):
        ''' process pass assignment command '''
        if self.region_config['allow_pass_assignment']:
            if prompt_yes_no(itext('pts.pass.confirm')):
                has_skips = False
                for put in self._pts_lut:
                    if put['status'] == 'S':
                        has_skips = True
            
                # If skips exists, pass not allowed
                if has_skips:
                    prompt_only(itext('pts.pass.with.skips'))
                else:
                    pass_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsPassAssignment')
                    result = -1
                    while result != 0:
                        result = pass_lut.do_transmit(self._assignment_lut[0]['groupId'])
                    
                    #loop whil user did not say ready
                    not_ready = True
                    while not_ready: 
                        result = prompt_ready(itext('pts.pass.complete'), False,
                                       {'change function' : False,
                                        'change region' : False}) 
                        
                        if result == 'change function':
                            change_function()
                        elif result == 'change region':
                            task = self.runner.findTask(PTS_TASK_NAME)
                            if task is not None:
                                task.change_region()
                        else:
                            not_ready = False
                            
                    self.runner.return_to('putToStore', puttostore.PutToStoreTask.INDUCT_LICENSE)   
        else:
            prompt_only(itext('pts.pass.not.allowed'))
            
        return True 

    def _repick_skips(self):
        ''' process repick skips command '''
        if self.region_config['allow_repick_skips']:
            if prompt_yes_no(itext('generic.repick.skips.confirm')):
                has_skips = False
                for put in self._pts_lut:
                    if put['status'] == 'S':
                        has_skips = True
                        put['status'] = 'N'
            
                if has_skips:
                    update_status = obj_factory.get(VoiceLinkOdr, 'prTaskODRPtsUpdateStatus')
                    update_status.do_transmit(self._assignment_lut[0]['groupId'],
                                              '', 2, 'N')
                    self.runner.return_to('putAssignment', 
                                          puttostore.PutToStorePutAssignment.PTS_ASSIGNMENT_RESET)   
                else:
                    prompt_only(itext('pts.repick.skips.noskips'))
                
        else:
            prompt_only(itext('generic.repick.skips.notallowed'))
        return True 

    def _review_containers(self):
        current_task = self.runner.task_stack[0].obj
        location_id = self.location_id
        if current_task.name == 'putToStorePutTask':
            location_id = self.current_put['customerLocationID']
        
        ''' process review containers command '''
        containers_lut = obj_factory.get(PtsContainers, 'prTaskLUTPtsContainer')
        
        #transmit to get container list
        result = -1
        while result < 0:
            result = containers_lut.do_transmit(0, location_id, '')
        
        #if result was 0, then loop through containers in list speaking container numbers
        if result == 0:
            for container in containers_lut:
                if container['containerNumber'] != '':
                    if prompt_ready(' '.join(container['containerNumber']),
                                    True,
                                    {'cancel' : False}) == 'cancel':
                        break
                
        return True 

    def _license(self):
        ''' speak current licnese '''
        prompt_only(self.current_put['licenseNumber'])
        return True 

    def _location(self):
        ''' speak location information '''
        key = 'pts.location.info.prompt'
        prompt_only(itext(key, 
                          self.current_put['preAisle'],
                          self.current_put['aisle'],
                          self.current_put['postAisle'],
                          self.current_put['slot']))
        return True 

    def _item_number(self):
        ''' speak item number '''
        prompt_only(self.current_put['itemNumber'])
        return True 

    def _quantity(self):
        ''' speak quantity '''
        qty = self.current_put['quantityToPut'] - self.current_put['quantityPut'] 
        prompt_only(str(qty) + " " + self.current_put['uom'])
        return True 

    def _description(self):
        ''' speak description information '''
        if self.current_put['itemDescription'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_put['itemDescription'].lower())
        return True 
