from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_digits, prompt_only, prompt_yes_no, prompt_words
from vocollect_core import itext, obj_factory, class_factory
from puttostore.PutToStoreLuts import VerifyLicense
from puttostore.PutToStoreCloseContainerTask import PutToStoreCloseContainer
from puttostore.SharedConstants import PTS_INDUCT_LICENSE_TASK_NAME,\
    PROMPT_FOR_LICENSE, RESERVE_LICENSE, SELECT_LICENSE, INDUCT_COMPLETE

class InductLicenseTask(class_factory.get(TaskBase)):
    '''Induct License task for put to store
    
    Steps:
        1. Prompt for License
        2. Verify license
        3. Select License
        4. Induct license
    '''
 
    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 taskRunner = None, 
                 callingTask = None):
        
        TaskBase.__init__(self, taskRunner, callingTask)
        
        #Set task name
        self.name = PTS_INDUCT_LICENSE_TASK_NAME
      
        self._region_config_lut = region_config_lut
        self._reserved_licenses = 0
        
        self._license = None
        self._license_scanned = None
        
        #LUTS
        self._verify_license_lut = obj_factory.get(VerifyLicense,
                                                     'prTaskLUTPtsVerifyLicense')
         
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get state
        self.addState(PROMPT_FOR_LICENSE,   self.prompt_for_license)
        self.addState(RESERVE_LICENSE,      self.reserve_license)
        self.addState(SELECT_LICENSE,       self.select_license)
        self.addState(INDUCT_COMPLETE,      self.induct_complete)

    #----------------------------------------------------------  
    def prompt_for_license(self):
        'Prompt for a license to reserve'

        lic_digits_to_speak = self._region_config_lut[0]['spoken_license_length']
        # license number
        self._license, self._license_scanned = prompt_digits(itext('pts.request.license'), 
                                                             itext('pts.request.license.help'), 
                                                             lic_digits_to_speak, 
                                                             lic_digits_to_speak, 
                                                             self._region_config_lut[0]['confirm_spoken_license'], 
                                                             True,
                                                             {'no more' : False,
                                                              'close container' : True})
        
        if self._license == 'no more':
            if self._reserved_licenses == 0:
                prompt_only(itext('pts.request.license.no.more'))
                self.next_state = PROMPT_FOR_LICENSE
            else:
                self._reserved_licenses = self._region_config_lut[0]['max_license_in_group'] 
                self.next_state = INDUCT_COMPLETE
        elif self._license == 'close container':
            self.launch(obj_factory.get(PutToStoreCloseContainer,
                                          self._region_config_lut,
                                          None,
                                          None, 
                                          self.taskRunner, self),
                        PROMPT_FOR_LICENSE)
        else:
            #if digits to speak is 0 (all) then treat as if it where scanned
            if not self._license_scanned and lic_digits_to_speak <= 0:
                self._license_scanned = True
            


    #----------------------------------------------------------  
    def reserve_license(self):
        ''' attempt to reserve license from host '''
        result =  self._verify_license_lut.do_transmit(self._license, 
                                                       int(not self._license_scanned))

        if result > 0 and len(self._verify_license_lut) == 1:
            self.next_state = PROMPT_FOR_LICENSE
        elif result < 0:
            self.next_state = RESERVE_LICENSE
        elif len(self._verify_license_lut) > 1:
            if not prompt_yes_no(itext('pts.request.license.multiple.found', 
                                       len(self._verify_license_lut))):
                self.next_state = PROMPT_FOR_LICENSE
                
    #----------------------------------------------------------
    def select_license(self):
        ''' check verify license response for multiple responses. If multiple
        have user select desired license '''
        
        if len(self._verify_license_lut) > 1:
            'select license  if multiple licenses were returned'
            self._license = None
            for license in self._verify_license_lut:
                result = prompt_words(itext('pts.request.license.multiple.select', 
                                            license['licenseNumber']), 
                                      additional_vocab = {'cancel':False, 
                                                          'yes': False, 
                                                          'no':False})
                if result == 'yes':
                    self._license = license['licenseNumber']
                    self._license_scanned = True
                    self.next_state = RESERVE_LICENSE
                    break 
                elif result == 'cancel':
                    if prompt_yes_no(itext('pts.request.license.cancel')):
                        self._license = result
                        self.next_state = PROMPT_FOR_LICENSE
                        break;

            #none selected
            if self._license is None:
                prompt_only(itext('pts.request.license.multiple.end'))
                self.next_state = SELECT_LICENSE

    #----------------------------------------------------------
    def induct_complete(self):
        ''' check if maximum number of licenses have been reserved '''
        self._reserved_licenses += 1
        if self._reserved_licenses < self._region_config_lut[0]['max_license_in_group']:
            self.next_state = PROMPT_FOR_LICENSE
        else:
            prompt_only(itext('pts.request.license.get.work'))
            
        
        
