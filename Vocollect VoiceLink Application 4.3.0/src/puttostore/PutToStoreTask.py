from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_only, prompt_ready, prompt_yes_no
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut

from common.RegionSelectionTasks import SingleRegionTask
from .StartAssignment import StartAssignmentTask
from puttostore.PutToStoreInductLicense import InductLicenseTask
from puttostore.PutToStorePutAssignment import PutAssignmentTask
from puttostore.PutToStoreHandleResiduals import PutToStoreHandleResiduals
from puttostore.PutToStoreVocabulary import PutToStoreVocabulary
from Globals import change_function
from puttostore.SharedConstants import PTS_TASK_NAME, REGIONS, VALIDATE_REGIONS,\
    GET_FT_LOCATION, INDUCT_LICENSE, START_ASSIGNMENT, PUT_ASSIGNMENT,\
    HANDLE_RESIDUALS, COMPLETE_ASSIGNMENT
from core.SharedConstants import CORE_TASK_NAME

###############################################################
# Main Class
###############################################################
class PutToStoreTask(class_factory.get(TaskBase)):
    ''' Put to store task for the VoiceLink system
    
    Steps:
            1. Get region
            2. Validate Regions 
            3. Get Flow Through Locations
            4. Induct License
            6. Start assignment
    '''
    
    #----------------------------------------------------------
    def __init__(self,
                 taskRunner = None, 
                 callingTask = None):
        TaskBase.__init__(self, taskRunner, callingTask)

        #Set task name
        self.name = PTS_TASK_NAME

        #define LUTS
        self._valid_regions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsValidRegions')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetRegionConfiguration')
        self._ft_location_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetFTLocation')
        self._assignment_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetAssignment')
        self._pts_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetPuts')
        self._stop_assignment = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsStopAssignment')
    
        #define and setup additional vocabulary object
        self.dynamic_vocab = obj_factory.get(PutToStoreVocabulary,
                                               self._assignment_lut,
                                               self._pts_lut, 
                                               self.taskRunner)

        self._region_selected = False
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(REGIONS,              self.regions)
        self.addState(VALIDATE_REGIONS,     self.validate_regions)
        self.addState(GET_FT_LOCATION,      self.get_flowthrough_location)
        self.addState(INDUCT_LICENSE,       self.induct_license)
        self.addState(START_ASSIGNMENT,     self.start_assignment)
        self.addState(PUT_ASSIGNMENT,       self.put_assignment)
        self.addState(HANDLE_RESIDUALS,     self.handle_residuals)
        self.addState(COMPLETE_ASSIGNMENT,  self.complete_assignment)
        
        
    #----------------------------------------------------------
    def regions(self):
        ''' Run single region put to store '''
        self.launch(obj_factory.get(SingleRegionTask,
                                      self.taskRunner, self))

    #----------------------------------------------------------
    def validate_regions(self):
        ''' validates region selection '''
        #check for valid regions
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
        
        if not valid:
            self.dynamic_vocab.region_config = None
            prompt_only(itext('generic.regionNotAuth.prompt'))
            self.next_state = ''
        else:
            self.dynamic_vocab.region_config = self._region_config_lut[0]
            self._region_selected = True
            result = prompt_ready(itext('pts.start.work'), 
                            False, {'change function' : False,
                                    'change region' : False}) 

            if result == 'change function':
                change_function()
                self.next_state = VALIDATE_REGIONS
            elif result == 'change region':
                self.change_region()
                self.next_state = VALIDATE_REGIONS
            
    #----------------------------------------------------------
    def get_flowthrough_location(self):
        '''get flow through location'''
        result = self._ft_location_lut.do_transmit(self._region_config_lut[0]['number'])
        if result != 0:
            self.next_state = GET_FT_LOCATION
        else:
            if self._ft_location_lut[0]['ftLocation'] != '':
                # get to flow through location 
                prompt_ready(itext('pts.direct.to.flow.through', 
                                   self._ft_location_lut[0]['ftLocation']))
                
    #----------------------------------------------------------
    def induct_license(self):
        ''' launch the put license task '''
        self.launch(obj_factory.get(InductLicenseTask,
                                      self._region_config_lut, 
                                      self.taskRunner, self))
        
    #----------------------------------------------------------        
    def start_assignment(self):
        ''' launch the start assignment task '''
        self.set_sign_off_allowed(self._region_config_lut[0]['allow_signoff'])
        self.launch(obj_factory.get(StartAssignmentTask,
                                      self._region_config_lut,
                                      self._assignment_lut,
                                      self._pts_lut, 
                                      self.taskRunner, self))

    #----------------------------------------------------------        
    def put_assignment(self):
        ''' launch the start assignment task '''
        self.launch(obj_factory.get(PutAssignmentTask,
                                      self._region_config_lut,
                                      self._assignment_lut,
                                      self._pts_lut, 
                                      self.taskRunner, self))
        
    #----------------------------------------------------------        
    def handle_residuals(self):
        ''' launch the start assignment task '''
        self.launch(obj_factory.get(PutToStoreHandleResiduals,
                                      self._region_config_lut,
                                      self._assignment_lut,
                                      self._pts_lut, 
                                      self.taskRunner, self))
        
    #----------------------------------------------------------        
    def complete_assignment(self):
        self.set_sign_off_allowed(True)
        result = self._stop_assignment.do_transmit(self._assignment_lut[0]['groupId'])
        
        if result != 0:
            self.next_state = COMPLETE_ASSIGNMENT
        else:
            not_ready = True
            while not_ready:
                result = prompt_ready(itext('pts.assignment.complete'), False, 
                         {'change function' : False,
                          'change region' : False}) 

                if result == 'change function':
                    change_function()
                elif result == 'change region':
                    self.change_region()
                else:
                    not_ready = False

            self.next_state = INDUCT_LICENSE

    #======================================================
    #Global/Dynamic Word Functions
    #======================================================
    def change_region(self):
        ''' Called when operator speaks global word "change region" '''
        if self._region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self._region_selected = False
                self.return_to(self.name, REGIONS)

    def set_sign_off_allowed(self, allowed = False):
        task = self.taskRunner.findTask(CORE_TASK_NAME)
        if task is not None:
            task.sign_off_allowed = allowed
