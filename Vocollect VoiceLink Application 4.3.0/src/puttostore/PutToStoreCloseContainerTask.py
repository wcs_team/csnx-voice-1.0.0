from vocollect_core.task.task import TaskBase
from vocollect_core import itext, obj_factory, class_factory
from vocollect_core.dialog.functions import prompt_alpha_numeric, prompt_digits, prompt_yes_no,\
    prompt_only, prompt_yes_no_cancel
from common.VoiceLinkLut import VoiceLinkLut
from puttostore.PutToStoreLuts import PtsContainers
from puttostore.PutToStoreVocabulary import PutToStoreVocabulary
from puttostore.SharedConstants import PTS_CLOSE_CONTAINER_TASK_NAME,\
    PTS_CLOSE_CONT_SPECIFY_LOC, PTS_CLOSE_CONT_SPECIFY_CD,\
    PTS_CLOSE_CONT_VERIFY_LOC, PTS_CLOSE_CONT_CLOSE_CONT,\
    PTS_CLOSE_CONT_SPECIFY_CONT, PTS_CLOSE_CONT_SELECT_CONT

class PutToStoreCloseContainer(class_factory.get(TaskBase)):
    
    '''Induct License task for put to store
    
    Steps:
        1. Prompt to confirm close container
        2. Specify a location is one wasn;t provided
        3. Specify Check Digits is location was spoken
        4. Transmit to verify location
        5. Transmit to close container
        6. Enter container number if one required and retransmit
        7. If multiple containers return, select one and retransmit
    '''
    
    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 location_id,
                 container_id,
                 taskRunner = None, 
                 callingTask = None):
        
        super(PutToStoreCloseContainer, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = PTS_CLOSE_CONTAINER_TASK_NAME

        #working variables
        self._container_id = container_id
        self._location_id = location_id

        self._location_entry = ''
        self._location_CD = ''
        self._loc_scanned = False
        
        #LUTs
        self._region_config_lut = region_config_lut
        self._verify_location_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsVerifyCustomerLocation')
        self._containers_lut = obj_factory.get(PtsContainers, 'prTaskLUTPtsContainer')

        #define and setup additional vocabulary object,
        #this should only enable review containers
        self.dynamic_vocab = obj_factory.get(PutToStoreVocabulary,
                                               None,
                                               None, 
                                               self.taskRunner)
        self.dynamic_vocab.location_id = self._location_id

    #----------------------------------------------------------
    def initializeStates(self):
        ''' initialize the sates for this task '''
        self.addState(PTS_CLOSE_CONT_SPECIFY_LOC,  self.specify_location)
        self.addState(PTS_CLOSE_CONT_SPECIFY_CD,   self.specify_check_digit)
        self.addState(PTS_CLOSE_CONT_VERIFY_LOC,   self.verify_location)
        self.addState(PTS_CLOSE_CONT_CLOSE_CONT,   self.close_container)
        self.addState(PTS_CLOSE_CONT_SPECIFY_CONT, self.specify_container)
        self.addState(PTS_CLOSE_CONT_SELECT_CONT,  self.select_container)
    
    #----------------------------------------------------------
    def specify_location(self):
        ''' Allow user to enter a location if one is specified '''
        self._location_entry = ''
        self._location_CD = ''
        self._loc_scanned = False

        if self._location_id is not None:
            self.next_state = PTS_CLOSE_CONT_CLOSE_CONT
        else:
            self._location_entry, self._loc_scanned = prompt_alpha_numeric(itext('pts.close.container.location'), 
                                                                           itext('pts.close.container.location.help'), 
                                                                           self._region_config_lut[0]['spoken_location_length'], 
                                                                           self._region_config_lut[0]['spoken_location_length'], 
                                                                           self._region_config_lut[0]['confirm_spoken_location'], 
                                                                           True)
            
            if self._loc_scanned:
                self.next_state = PTS_CLOSE_CONT_VERIFY_LOC
            
    #----------------------------------------------------------
    def specify_check_digit(self):
        ''' Allow user to enter Check Digits if location was spoken in '''
        additional_vocabulary = {}
        if self._region_config_lut[0]['check_digit_length'] <= 0:
            additional_vocabulary['ready'] = False
            
        min = self._region_config_lut[0]['check_digit_length']
        max = self._region_config_lut[0]['check_digit_length']
        if min == 0:
            min = 1
            max = None
            
        check_digits = prompt_digits(itext('pts.close.container.cd'), 
                                     itext('pts.close.container.cd.help'), 
                                     min, 
                                     max, 
                                     False, 
                                     True,
                                     additional_vocabulary)

        if check_digits[0] == 'ready':
            self._location_CD = ''
        else:
            self._location_CD = check_digits[0]
    
    #----------------------------------------------------------
    def verify_location(self):
        ''' Verify Location with host and get location's ID '''
        result = self._verify_location_lut.do_transmit(int(self._loc_scanned),
                                                       self._location_entry,
                                                       self._location_CD)
    
        if result != 0:
            self.next_state = PTS_CLOSE_CONT_SPECIFY_LOC
        else:
            self._location_id = self._verify_location_lut[0]['customerLocationID']
            self.dynamic_vocab.location_id = self._location_id
    
    #----------------------------------------------------------
    def close_container(self):
        ''' Send close container to host '''
        result = self._containers_lut.do_transmit(1, self._location_id, self._container_id)
    
        if result == 3:
            self.next_state = PTS_CLOSE_CONT_SPECIFY_CONT
        elif result == 8:
            self.next_state = PTS_CLOSE_CONT_SELECT_CONT
        elif result < 0:
            self.next_state = PTS_CLOSE_CONT_CLOSE_CONT
        else:
            self.next_state = ''
            
    #----------------------------------------------------------
    def specify_container(self):
        ''' If container ID is required, then have user enter and then retransmit '''
        result = prompt_alpha_numeric(itext('pts.close.container.container'), 
                                      itext('pts.close.container.container.help'), 
                                      self._region_config_lut[0]['validate_container_length'], 
                                      self._region_config_lut[0]['validate_container_length'], 
                                      True, 
                                      True)
        
        self._container_id = result[0]
        self.next_state = PTS_CLOSE_CONT_CLOSE_CONT
        
    #----------------------------------------------------------
    def select_container(self):
        ''' If multiple containers, have operator select one and retransmit '''
        count = self._containers_lut.container_count()
        if count > 0:
            for container in self._containers_lut:
                result = ''
                while result == '':
                    result = prompt_yes_no_cancel(itext('pts.close.container.select', 
                                                        container['containerNumber']),
                                                  True)
                    if result == 'yes':
                        self._container_id = container['containerNumber']
                        self.next_state = PTS_CLOSE_CONT_CLOSE_CONT
                        return
                    elif result == 'cancel':
                        if prompt_yes_no(itext('pts.close.container.cancel')):
                            self.next_state = None
                            return
                        else:
                            result = ''
            
            if (count == 1):
                prompt_only(itext('pts.close.container.select.end.singular'))
            else:
                prompt_only(itext('pts.close.container.select.end.plural'))
            self.next_state = PTS_CLOSE_CONT_SELECT_CONT
        else:
            self.next_state = ''
    
