from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core import itext, class_factory, obj_factory
from vocollect_core.dialog.functions import prompt_ready
from core.Print import PrintTask, ENTER_PRINTER
from puttostore.SharedConstants import PTS_PRINT_TASK_NAME, PRINT_LABELS,\
    PRINT_RETRIEVE, PTS_TASK_NAME

#===========================================================
#Print Task for Selection
#===========================================================
class PutToStorePrintTask(class_factory.get(PrintTask)):
    ''' Selection print task
    
     Steps:
            1. Enter Printer Number
            2. Confirm Printer Number
            3. Print
            4. reprint labels

     Parameters
            region - region operator is picking in 
            assignment_lut - assignments currently working on
            picks - pick list for specific location
            container_lut - current list of containers
            taskRunner (Default = None) - Task runner object
            callingTask (Default = None) - Calling task (should be a pick prompt task)
        
    '''
    
    #Class Constants
    LABEL_TYPE_EXCEPTION = 'exceptionLabel'
    LABEL_TYPE_NEW_CONTAINER = 'newContainerLabel'

    #-------------------------------------------------------------------------
    def __init__(self,
                 group_or_license,
                 location_or_item,
                 container_id, 
                 label_type,
                 taskRunner = None, 
                 callingTask = None):
        super(PutToStorePrintTask, self).__init__(taskRunner, callingTask)
        
        #Reset task name
        self.name = PTS_PRINT_TASK_NAME
        
        #find main put to store task, to see if printer was already entered
        self.task = self.taskRunner.findTask(PTS_TASK_NAME)
        if self.task is None:
            self.task = self.callingTask

        #working variables
        self._location_or_item = location_or_item
        self._group_or_license = group_or_license
        self._contatiner_id = container_id
        self._label_type = label_type
        
        if self._label_type == self.LABEL_TYPE_EXCEPTION:
            self._print_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsPrintExceptionLabel')
        else:
            self._print_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsPrintContainerLabel')
            
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        super(PutToStorePrintTask, self).initializeStates()
        
        #Add additional states
        self.addState(PRINT_LABELS, self.print_label)
        self.addState(PRINT_RETRIEVE, self.print_retrieve)
        
    #-------------------------------------------------------------------------
    def print_label(self):
        ''' print labels'''    
        
        if self._label_type == self.LABEL_TYPE_EXCEPTION:
            result = self._print_lut.do_transmit(self.task.printer_number,
                                                 self._group_or_license,
                                                 self._location_or_item)
        else:
            result = self._print_lut.do_transmit(self._group_or_license,
                                                 self.task.printer_number,
                                                 self._location_or_item,
                                                 self._contatiner_id)
        
        if result != 0:
            self.next_state = ENTER_PRINTER
    
    #-------------------------------------------------------------------------
    def print_retrieve(self):
        
        if self._label_type == self.LABEL_TYPE_EXCEPTION:
            prompt_ready(itext('pts.print.retrieve.exception',
                               self.task.printer_number))
        else:
            prompt_ready(itext('pts.print.retrieve.newcontainer',
                               self.task.printer_number))
    