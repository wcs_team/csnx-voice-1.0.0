from vocollect_core.task.task import TaskBase
from vocollect_core import itext, obj_factory, class_factory
from vocollect_core.dialog.functions import prompt_alpha_numeric
from puttostore.PutToStoreLuts import PtsContainers
from puttostore.PutToStorePrint import PutToStorePrintTask
from puttostore.PutToStoreVocabulary import PutToStoreVocabulary
from puttostore.SharedConstants import PTS_NEW_CONTAINER_TASK_NAME,\
    PTS_NEW_CONT_SPECIFY_CONT, PTS_NEW_CONT_OPEN_CONT, PTS_NEW_CONT_PRINT,\
    PTS_NEW_CONT_RETURN_TO, PTS_ASSIGNMENT_RESET, PTS_ASSIGNMENT_TASK_NAME

class PutToStoreNewContainer(class_factory.get(TaskBase)):
    
    '''Induct License task for put to store
    
    Steps:
        1. Prompt for container if operator specifies
        2. Transmit for new container
        3. print labels
        4. determine where to return to
    '''
    
    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 group_id,
                 location_id,
                 taskRunner = None, 
                 callingTask = None):
        
        super(PutToStoreNewContainer, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = PTS_NEW_CONTAINER_TASK_NAME

        #working variables
        self._container_id = None
        self._location_id = location_id
        self._group_id = group_id
        
        #LUTs
        self._region_config_lut = region_config_lut
        self._containers_lut = obj_factory.get(PtsContainers, 'prTaskLUTPtsContainer')

        #define and setup additional vocabulary object,
        #this should only enable review containers
        self.dynamic_vocab = obj_factory.get(PutToStoreVocabulary,
                                               None,
                                               None, 
                                               self.taskRunner)
        self.dynamic_vocab.location_id = location_id
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' initialize the sates for this task '''
        self.addState(PTS_NEW_CONT_SPECIFY_CONT, self.specify_container)
        self.addState(PTS_NEW_CONT_OPEN_CONT,    self.open_container)
        self.addState(PTS_NEW_CONT_PRINT,        self.print_labels)
        self.addState(PTS_NEW_CONT_RETURN_TO,    self.determine_return_to)
    
    #----------------------------------------------------------
    def specify_container(self):
        
        #if system generates container ID then just return
        if self._region_config_lut[0]['system_gen_container_id']:
            return
        
        result = prompt_alpha_numeric(itext('pts.new.container.container'), 
                                      itext('pts.new.container.container.help'), 
                                      self._region_config_lut[0]['validate_container_length'], 
                                      1000, 
                                      True, 
                                      True)
        
        self._container_id = result[0]
            
            
    #----------------------------------------------------------
    def open_container(self):
        ''' Send open container to host '''
        result = self._containers_lut.do_transmit(2, self._location_id, self._container_id)
    
        if result > 0:
            self.next_state = ''
        elif result < 0:
            self.next_state = PTS_NEW_CONT_OPEN_CONT
            
    #----------------------------------------------------------
    def print_labels(self):
        if self._region_config_lut[0]['system_gen_container_id']:
            self.launch(obj_factory.get(PutToStorePrintTask,
                                          self._group_id,
                                          self._containers_lut[0]['customerLocationID'],
                                          self._containers_lut[0]['containerNumber'],
                                          PutToStorePrintTask.LABEL_TYPE_NEW_CONTAINER,
                                          self.taskRunner, self))
    
    #----------------------------------------------------------
    def determine_return_to(self):
        if self._region_config_lut[0]['system_gen_container_id']:
            self.return_to(PTS_ASSIGNMENT_TASK_NAME, 
                           PTS_ASSIGNMENT_RESET)
    
