from vocollect_core.task.task import TaskBase
from vocollect_core import itext, class_factory
from vocollect_core.dialog.functions import prompt_yes_no, prompt_only, prompt_digits, prompt_ready,\
    prompt_digits_required
from common.VoiceLinkLut import VoiceLinkLut, VoiceLinkOdr
from vocollect_core.utilities import obj_factory
from puttostore.PutToStorePrint import PutToStorePrintTask
from puttostore.SharedConstants import PTS_HANDLE_RESIDUALS_TASK_NAME,\
    PTS_RESIDUALS_GET_RESIDUALS, PTS_RESIDUALS_PROMPT_RESIDUALS,\
    PTS_RESIDUALS_INIALIZE_LICENSE, PTS_RESIDUALS_GET_NEXT_RESID,\
    PTS_RESIDUALS_PROMPT_RESID_QTY, PTS_RESIDUALS_DELIVER_EXCEPT,\
    PTS_RESIDUALS_PRINT_EXCEPT

class PutToStoreHandleResiduals(class_factory.get(TaskBase)):
    
    '''Handle residuals for an assignment
    
    Steps:
        1. Get Expected Residuals
        2. prompt if operator has any residuals
        3. Initialize residuals for a individual license
        4. Get next residual for license, if none, the deliver to return location
        5. Verify quantity for license/item 
        6. deliver to exception location if quantity differs
        7. print exception labels if needed 
    '''
    
    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 assignment_lut,
                 pts_lut,
                 taskRunner = None, 
                 callingTask = None):
        
        super(PutToStoreHandleResiduals, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = PTS_HANDLE_RESIDUALS_TASK_NAME

        #LUTs
        self._region_config_lut = region_config_lut
        self._assignment_lut = assignment_lut
        self._pts_lut = pts_lut
        
        self._residuals_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTPtsGetExpectedResidual')
        self._notification_odr = obj_factory.get(VoiceLinkOdr, 'prTaskODRCorePostNotification')

        #working variables
        self._residuals_iterator = None
        self._lic_residuals = None
        self._curr_residual = None
        
        
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' initialize the sates for this task '''
        self.addState(PTS_RESIDUALS_GET_RESIDUALS,    self.get_residuals)
        self.addState(PTS_RESIDUALS_PROMPT_RESIDUALS, self.prompt_residuals)
        self.addState(PTS_RESIDUALS_INIALIZE_LICENSE, self.initialize_license)
        self.addState(PTS_RESIDUALS_GET_NEXT_RESID,   self.get_next_residual)
        self.addState(PTS_RESIDUALS_PROMPT_RESID_QTY, self.prompt_residual_qty)
        self.addState(PTS_RESIDUALS_DELIVER_EXCEPT,   self.deliver_exception)
        self.addState(PTS_RESIDUALS_PRINT_EXCEPT,     self.print_exception_label)
    
    #----------------------------------------------------------                           
    def get_residuals(self):
        ''' get expected residuals from host '''
        result = self._residuals_lut.do_transmit(self._assignment_lut[0]['groupId'])
        
        if result != 0:
            self.next_state = PTS_RESIDUALS_GET_RESIDUALS
        
    #----------------------------------------------------------
    def prompt_residuals(self):
        ''' prompt operator if they have residuals '''
        if prompt_yes_no(itext('pts.residual.check')):
            #check if there is an exception location to deliver to,
            # if not finish here
            if (self._assignment_lut[0]['exceptionLocation'] == '' or 
                (self._assignment_lut[0]['exceptionLocation'] == self._assignment_lut[0]['returnLocation']
                  and self._assignment_lut[0]['exceptionCheckdigit'] == self._assignment_lut[0]['returnCheckdigit'])):
    
                #mark all as processed and and check total expected
                total_expected = 0
                for residual in self._residuals_lut:
                    if residual['processed'] != 'P':
                        total_expected += residual['expectedResidual']
                        residual['processed'] = 'P'
    
                #post notification if no residuals where expected
                if total_expected <= 0:
                    self._post_notifications(3, self._residuals_lut[0])
    
                #Deliver to return location, then end task
                self._deliver_prompts(self._assignment_lut[0]['returnLocation'], 
                                      self._assignment_lut[0]['returnCheckdigit'])
                self.next_state = ''
        else:
            #Operator said No so post notification if any where expected
            # and then end task
            for residual in self._residuals_lut:
                if residual['expectedResidual'] > 0 and residual['processed'] == 'N':
                    self._post_notifications(2, residual)

                #mark all residuals for license as processed
                for r in self._residuals_lut:
                    if r['licenseNumber'] == residual['licenseNumber']:
                        r['processed'] = 'P'
                        
            self.next_state = ''

    #----------------------------------------------------------
    def initialize_license(self):
        ''' Initialize list of residuals for a single license '''
        #get list of residuals for next license that wasn't processed
        self._lic_residuals = []
        for residual in self._residuals_lut:
            if residual['processed'] != 'P':
                for resid in self._residuals_lut:
                    if residual['licenseNumber'] == resid['licenseNumber']:
                        self._lic_residuals.append(resid)
                break
                        
        #initialize iterator
        self._residuals_iterator = iter(self._lic_residuals)

        #if no licenses found then end task
        if len(self._lic_residuals) == 0:
            self.next_state = ''
        elif len(self._lic_residuals) > 1:
            prompt_only(itext('pts.residual.confirm.license', 
                              self._lic_residuals[0]['licenseNumber']))
            
    #----------------------------------------------------------                              
    def get_next_residual(self):
        ''' get next residual '''
        try:
            self._curr_residual = next(self._residuals_iterator)
        except:
            #All items processed without exceptions, so deliver 
            #license to return to location
            self._deliver_prompts(self._assignment_lut[0]['returnLocation'], 
                                  self._assignment_lut[0]['returnCheckdigit'])
            self.next_state = PTS_RESIDUALS_INIALIZE_LICENSE
        
    #----------------------------------------------------------                              
    def prompt_residual_qty(self):
        ''' get residual qty from operator and check if matches expected '''
        #set prompts
        prompt = itext('pts.residual.confirm.license', 
                              self._curr_residual['licenseNumber'])
        help = itext('pts.residual.confirm.license.help')
        additional_vocab = {}
        
        #if multiple items, change prompts
        if len(self._lic_residuals) > 1:
            prompt = itext('pts.residual.confirm.item',
                           self._curr_residual['itemDescription'])
            help = itext('pts.residual.confirm.item.help')
            additional_vocab = {'item number' : False}
        
        result = prompt_digits(prompt, help, 
                               1, 10, 
                               True, False, 
                               additional_vocab)
        
        if result == 'item number':
            prompt_only(self._curr_residual['itemNumber'])
            self.next_state = PTS_RESIDUALS_PROMPT_RESID_QTY
        else:
            result = int(result)
            if result == self._curr_residual['expectedResidual']:
                self._curr_residual['processed'] = 'P'
                self.next_state = PTS_RESIDUALS_GET_NEXT_RESID
            else:
                #error occured so mark all as process and continue
                #to deliver to exception location and print
                for residual in self._lic_residuals:
                    residual['processed'] = 'P'
                    
    #----------------------------------------------------------                              
    def deliver_exception(self):
        ''' deliver exceptions '''
        
        #first post notification
        self._post_notifications(1, self._curr_residual)
        self._deliver_prompts(self._assignment_lut[0]['exceptionLocation'], 
                              self._assignment_lut[0]['exceptionCheckdigit'])

    #----------------------------------------------------------                              
    def print_exception_label(self):
        ''' print exception label if required '''
        if self._region_config_lut[0]['print_exception_label']:
            self.launch(obj_factory.get(PutToStorePrintTask,
                                          self._curr_residual['licenseNumber'],
                                          self._curr_residual['itemNumber'],
                                          None,
                                          PutToStorePrintTask.LABEL_TYPE_EXCEPTION,
                                          self.taskRunner, self),
                        PTS_RESIDUALS_INIALIZE_LICENSE)

        self.next_state = PTS_RESIDUALS_INIALIZE_LICENSE

    #----------------------------------------------------------                              
    def _deliver_prompts(self, location, check_digits):
        ''' helper method for delivering to either exception location of return
        to location 
        '''
        
        #prompt for location
        prompt = itext('pts.residual.return')
        if location != '':
            prompt = itext('pts.residual.return.location', location)
        prompt_ready(prompt)
        
        #only prompt for Check digits if location and check digits provided
        if location != "" and check_digits != "":
            result = 'location'
            #loop until operator speaks check digits, only other value allowed
            # is location
            while result == 'location':
                result = prompt_digits_required(itext('pts.residual.return.cd'), 
                                                itext('pts.residual.return.cd.help'), 
                                                [check_digits], 
                                                None, 
                                                {'location' : False})
            
                if result == 'location':
                    prompt_ready(itext('generic.sayReady.prompt', location))
    
    #----------------------------------------------------------
    def _post_notifications(self, type, residual):
        ''' post a single notification '''
        residual['processed'] = 'P'
        
        message_key = ''
        message_values = ''
        if type == 2:
            message_key = 'voicelink.notification.task.puttostore.hostexpectsresiduals'
            message_values = 'task.licenseNumber='+residual['licenseNumber']
        elif type == 3:
            message_key = 'voicelink.notification.task.puttostore.hostexpectsnoresiduals'
            message_values = ''
        else:
            message_key = 'voicelink.notification.task.puttostore.unexpectedresidual'
            message_values = 'task.licenseNumber='+residual['licenseNumber']+"|"+"task.itemNumber="+residual['itemNumber']

        self._notification_odr.do_transmit(message_key, 0, message_values)
