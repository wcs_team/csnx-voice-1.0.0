from voice import globalwords as gw
from core.SharedConstants import CORE_TASK_NAME
from vocollect_core.task.task_runner import TaskRunnerBase

#-----------------------------------------------
#Global word functions
def sign_off_confirm():
    '''
    Global word change function
    '''
    task = TaskRunnerBase.get_main_runner().findTask(CORE_TASK_NAME)
    if task is not None:
        gw.words['sign off'].enabled = False
        task.sign_off_confirm()
        gw.words['sign off'].enabled = True
    
def sign_off():
    task = TaskRunnerBase.get_main_runner().findTask(CORE_TASK_NAME)
    if task is not None:
        task.sign_off()
        
def change_function():
    '''
    Global word change function
    '''
    task = TaskRunnerBase.get_main_runner().findTask(CORE_TASK_NAME)
    if task is not None:
        task.change_function()

def take_a_break():
    '''
    Global word take a break
    '''
    task = TaskRunnerBase.get_main_runner().findTask(CORE_TASK_NAME)
    if task is not None:
        gw.words['take a break'].enabled = False
        task.take_a_break()
        gw.words['take a break'].enabled = True

