#---------------------------------------------------------------
#Back Stocking Task
BACK_STOCKING_TASK_NAME                 = "backStocking"

#States - Back Stocking
REGIONS                                 = 'Regions'
VALIDATE_REGION                         = 'validateRegions'
STOCKING_GET_LICENSE                    = 'getLicense'
STOCKING_INDUCT_LICENSE                 = 'inductLicense'
STOCKING_BUILD_LICENSE                  = 'buildLicense'
STOCKING_GET_TRIP                       = 'getTrip'
STOCKING_START_TRIP                     = 'startTrip'
STOCKING_PUT_TRIP                       = 'putTrip'
STOCKING_CHECK_RESIDUALS                = 'check_residuals'
STOCKING_STOP_TRIP                      = 'stopTrip'

#---------------------------------------------------------------
#Operator Build
STOCKING_OPER_BUILD_TASK_NAME           = "backstockingOperBuildLPNTask"

#States - Operator Build
BACKSTOCKING_OPER_CONFIRM_CARTON        = 'backStockingOperConfirmCarton'
BACKSTOCKING_OPER_QUANITY               = 'backStockingOperQuantity'
BACKSTOCKING_OPER_XMIT_CARTON           = 'backStockingOperXmitCarton'
BACKSTOCKING_OPER_NEXT_STEP             = 'backStockingOperNextStep'

#---------------------------------------------------------------
#System Build
STOCKING_SYSTEM_BUILD_TASK_NAME         = "backstockingSystemBuildLPNTask"

#States - System Build
BACKSTOCKING_SYSTEM_BUILD_ALL           = 'backStockingSystemBuildAll'
BACKSTOCKING_SYSTEM_NEXT_CARTON         = 'backStockingSystemNextCarton'
BACKSTOCKING_SYSTEM_CONFIRM_CARTON      = 'backStockingSystemConfirmCarton'
BACKSTOCKING_SYSTEM_QUANITY             = 'backStockingSystemQuantity'
BACKSTOCKING_SYSTEM_REASON              = 'backStockingSystemReason'
BACKSTOCKING_SYSTEM_XMIT_CARTON         = 'backStockingSystemXmitCarton'
BACKSTOCKING_SYSTEM_NEXT_STEP           = 'backStockingSystemNextStep'

#---------------------------------------------------------------
#Put Prompt
STOCKING_PUT_PROMPT                     = "backStockingPutTask"

#States - Put Prompt
STOCKING_PUT_VERIFY_SLOT                = 'backStockingPutVerifySlot'
STOCKING_PUT_VERIFY_CARTON              = 'backStockingPutVerifyCarton'
STOCKING_PUT_GET_QUANTITY               = 'backStockingPutGetQuantity'
STOCKING_PUT_REASON_CODE                = 'backStockingPutReasonCode'
STOCKING_PUT_XMIT_PUT                   = 'backstockingPutXmitPut'

#---------------------------------------------------------------
#Put Trip
STOCKING_PUT_TRIP                       = "backStockingPutTrip"

#States - Put Trip
STOCKING_TRIP_RESET                     = "stockingTripReset"
STOCKING_TRIP_CHECK_NEXT_PUT            = "stockingTripCheckNextPut"
STOCKING_TRIP_PREAISLE                  = "stockingTripPreAisle"
STOCKING_TRIP_AISLE                     = "stockingTripAisle"
STOCKING_TRIP_POSTAISLE                 = "stockingTripPostAisle"
STOCKING_TRIP_PUTPROMPT                 = "stockingTripPutPrompt"