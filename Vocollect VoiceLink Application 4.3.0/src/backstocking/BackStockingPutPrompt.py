from vocollect_core.task.task import TaskBase
from vocollect_core import class_factory, obj_factory
from vocollect_core import itext
from vocollect_core.dialog.functions import prompt_ready, prompt_digits_required, prompt_only,\
    prompt_yes_no, prompt_digits, prompt_list_lut
from common.VoiceLinkLut import VoiceLinkOdr, VoiceLinkLut
from backstocking.SharedConstants import STOCKING_PUT_PROMPT,\
    STOCKING_PUT_VERIFY_SLOT, STOCKING_PUT_VERIFY_CARTON,\
    STOCKING_PUT_GET_QUANTITY, STOCKING_PUT_REASON_CODE, STOCKING_PUT_XMIT_PUT


class BackStockingPutPromptTask(class_factory.get(TaskBase)):
    '''Put prompt for back stocking
    
    Steps:
        1. Verify Slot
        2. Verify Carton
        3. Get Quantity
        4. Transmit Put Information
        5. Finish Put
    '''
 
    #----------------------------------------------------------
    def __init__(self,
                 region_config_rec,
                 trip_lut,
                 current_put,
                 taskRunner = None, 
                 callingTask = None):
        super().__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = STOCKING_PUT_PROMPT
      
        self._region_config_rec = region_config_rec
        self._current_put = current_put
        self._trip_lut = trip_lut
        
        #LUT Definitions
        self._reason_code_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreGetReasonCodes')
        self._put_odr = obj_factory.get(VoiceLinkOdr, 'prTaskODRStockingStocked')
        
        #working variables
        self._qty_put = 0
        self._short_spoken = False
        self._skip_prompt = False
        self._cancel = False
        self._reason_code = ''
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' initialize the sates for this task '''
        self.addState(STOCKING_PUT_VERIFY_SLOT,      self.verify_slot)
        self.addState(STOCKING_PUT_VERIFY_CARTON,    self.verify_carton)
        self.addState(STOCKING_PUT_GET_QUANTITY,     self.get_quantity)
        self.addState(STOCKING_PUT_REASON_CODE,      self.get_reason_code)
        self.addState(STOCKING_PUT_XMIT_PUT,         self.xmit_put)

    #----------------------------------------------------------
    def verify_slot(self):
        ''' verify the slot '''
        additional_vocab = {'skip slot' : False}
        result = ''
        if self._current_put['check_digit'] == '':
            result = prompt_ready(self._current_put['slot'], 
                                  True, 
                                  additional_vocab)
        else:
            result = prompt_digits_required(self._current_put['slot'], 
                                            itext('stocking.put.prompt.slot.help'), 
                                            [self._current_put['check_digit']], 
                                            [self._current_put['check_digit']],
                                            additional_vocab)
            result = result[0]
        
        if result == 'skip slot':
            self.next_state = STOCKING_PUT_VERIFY_SLOT
            self._skip_slot()

    #----------------------------------------------------------
    def verify_carton(self):
        ''' verify the carton '''
        #build additional vocab
        additional_vocab = {'cancel' : False}
        if not self._region_config_rec['require_qty_verification']:
            additional_vocab['short product'] = False
        
        #build main prompt
        prompt = itext('stocking.put.prompt.carton',
                       self._current_put['carton_id'],
                       self._current_put['quantity'],
                       self._current_put['stocker_prep'])

        #prompt operator to confirm carton
        result = ''
        if self._current_put['spoken_carton_id'] == '' and self._current_put['scanned_carton_id'] == '':
            result = prompt_ready(prompt, 
                                  True, 
                                  additional_vocab, 
                                  self._skip_prompt)
        else:
            if self._current_put['spoken_carton_id'] == '':
                additional_vocab['ready'] = ''
                
            result = prompt_digits_required(prompt, 
                                            itext('stocking.put.prompt.carton.help'), 
                                            [self._current_put['spoken_carton_id']], 
                                            [self._current_put['scanned_carton_id']],
                                            additional_vocab,
                                            self._skip_prompt)
            result = result[0]
        
        #check results
        self._skip_prompt = False    
        if result == 'short product':
            self.next_state = STOCKING_PUT_VERIFY_CARTON
            self._skip_prompt = True
            self._short_spoken = True
            prompt_only(itext('stocking.put.prompt.carton.verify'))
        elif result == 'cancel':
            if prompt_yes_no(itext('stocking.put.prompt.carton.cancel')):
                self._cancel = True
                self.next_state = STOCKING_PUT_REASON_CODE
            else:
                self.next_state = STOCKING_PUT_VERIFY_CARTON

    #----------------------------------------------------------
    def get_quantity(self):
        ''' if required ask operator for quantity put'''
        self._qty_put = 0        
        if self._short_spoken or self._region_config_rec['require_qty_verification']:
            prompt = itext('stocking.put.prompt.quantity')
            help = itext('stocking.put.prompt.quantity.help')
            if self._short_spoken:
                prompt = itext('stocking.put.prompt.quantity.short')

            #get the quantity
            result = prompt_digits(prompt, 
                                   help, 
                                   1, 
                                   10, 
                                   self._short_spoken, #only confirm if short was spoken 
                                   False)

            #check result
            self._qty_put = int(result)
            if self._qty_put > self._current_put['quantity']:
                prompt_only(itext('stocking.qty.over',
                                  self._qty_put, self._current_put['quantity']))
                self.next_state = STOCKING_PUT_GET_QUANTITY
            elif self._qty_put < self._current_put['quantity']:
                if not self._short_spoken:
                    if not prompt_yes_no(itext('stocking.put.prompt.quantity.short.confirm',
                                               self._qty_put)):
                        self.next_state = STOCKING_PUT_GET_QUANTITY
        elif not self._cancel:
            self._qty_put = self._current_put['quantity']
        
    #----------------------------------------------------------
    def get_reason_code(self):
        ''' if exception occurred then get reason '''
        self._reason_code = ''
        if self._qty_put < self._current_put['quantity'] or self._cancel:
            result = self._reason_code_lut.do_transmit('11', '1')
            if result != 0:
                self.next_state = STOCKING_PUT_REASON_CODE
            elif len(self._reason_code_lut) == 1: #only 1 returned so use it (even if blank)
                self._reason_code = self._reason_code_lut[0]['code']
            else:
                self._reason_code = prompt_list_lut(self._reason_code_lut, 
                                                    'code', 'description',
                                                    itext('stocking.reason'), 
                                                    itext('stocking.reason.help'))
    
    #----------------------------------------------------------
    def xmit_put(self):
        ''' transmit put information to host '''
        self._put_odr.do_transmit(self._current_put['id'],
                                  self._current_put['location_id'],
                                  self._current_put['sequence'],
                                  self._current_put['carton_id'],
                                  self._current_put['item_number'],
                                  self._qty_put,
                                  self._current_put['quantity'] - self._qty_put,
                                  self._reason_code)
        
        if self._cancel:
            self._current_put['status'] = 'C'
        elif self._qty_put < self._current_put['quantity']:
            self._current_put['status'] = 'R'
        else:
            self._current_put['status'] = 'P'
            
        self.dynamic_vocab.last_carton = self._current_put
        self.dynamic_vocab.last_quantity = self._qty_put
            
    #------------------------------------------------------------
    def _skip_slot(self):
        #check if region allows
        if not self._region_config_rec['allow_skip_slot']:
            prompt_only(itext('generic.skip.slot.notallowed'))
            return

        #confirm skip slot
        if prompt_yes_no(itext('generic.skip.slot.confirm')):
            for put in self._trip_lut:
                if put['location_id'] == self._current_put['location_id']:
                    if put['status'] == 'N': 
                        put['status'] = 'S'

            self.next_state = '' 
