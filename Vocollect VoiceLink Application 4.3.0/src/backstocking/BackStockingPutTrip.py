from vocollect_core.task.task import TaskBase
from vocollect_core import itext, class_factory
from vocollect_core.dialog.functions import prompt_ready, prompt_only, prompt_yes_no
from vocollect_core.utilities import obj_factory
from backstocking.BackStockingPutPrompt import BackStockingPutPromptTask
from backstocking.BackStockingVocabularyPut import BackStockingVocabularyPut
from backstocking.SharedConstants import STOCKING_PUT_TRIP, STOCKING_TRIP_RESET,\
    STOCKING_TRIP_CHECK_NEXT_PUT, STOCKING_TRIP_PREAISLE, STOCKING_TRIP_AISLE,\
    STOCKING_TRIP_POSTAISLE, STOCKING_TRIP_PUTPROMPT


class BackStockingPutTripTask(class_factory.get(TaskBase)):
    '''Back stocking Put trip
    
    Steps:
        1. Reset current location information
        2. check if there is another put
        3. Direct to pre-aisle
        4. Direct to aisle
        5. Direct to post-aisle
        6. Launch put prompt task
    '''
    
    #----------------------------------------------------------
    def __init__(self,
                 region_config_rec,
                 trip_lut,
                 taskRunner = None, 
                 callingTask = None):
        super().__init__(taskRunner, callingTask)

        #Set task name
        self.name = STOCKING_PUT_TRIP

        #Luts
        self._trip_lut = trip_lut
        self._region_config_rec = region_config_rec
        
        #working variables
        self._current_put = None
        self._trip_iter = None
        
        self._aisle_direction=''
        self._pre_aisle_direction=''
        self._post_aisle_direction=''

        #dynamic vocabulary
        self.dynamic_vocab = BackStockingVocabularyPut(self._region_config_rec, 
                                                       self.taskRunner)
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get state
        self.addState(STOCKING_TRIP_RESET,          self.reset)
        self.addState(STOCKING_TRIP_CHECK_NEXT_PUT, self.check_next_put)
        self.addState(STOCKING_TRIP_PREAISLE,       self.pre_aisle)
        self.addState(STOCKING_TRIP_AISLE,          self.aisle)
        self.addState(STOCKING_TRIP_POSTAISLE,      self.post_asile)
        self.addState(STOCKING_TRIP_PUTPROMPT,      self.put_prompt)

    #----------------------------------------------------------  
    def reset(self):
        self.dynamic_vocab.trip_lut = self._trip_lut
        self.dynamic_vocab.current_carton = None
        
        self._aisle_direction=''
        self._pre_aisle_direction=''
        self._post_aisle_direction=''
        self._trip_iter = iter(self._trip_lut)
        
    #----------------------------------------------------------  
    def check_next_put(self):
        ''' check for the next unput put in list of puts '''
        try:
            self._current_put = next(self._trip_iter)
            self.dynamic_vocab.current_carton = None
            
            if self._current_put['status'] != 'N':
                self.next_state = STOCKING_TRIP_CHECK_NEXT_PUT
        except:
            self.next_state = ''
            
    #----------------------------------------------------------  
    def pre_aisle(self):
        ''' directing to Pre Aisle'''
        #if pre-aisle is same as pre-aisle don't prompt
        if self._current_put["pre_aisle"] != self._pre_aisle_direction:
            if self._current_put["pre_aisle"] != '':
                prompt_ready(itext('stocking.put.pre.aisle', 
                                   self._current_put["pre_aisle"]))

            self._post_aisle_direction=''
            self._aisle_direction=''
            self._pre_aisle_direction = self._current_put["pre_aisle"]
    
    #----------------------------------------------------------
    def aisle(self):
        ''' directing to Aisle'''
        #if aisle is same as aisle don't prompt
        result = ''
        if self._current_put["aisle"] != self._aisle_direction:
            if self._current_put["aisle"] != '':
                result = prompt_ready(itext('stocking.put.aisle', 
                                            self._current_put["aisle"]), 
                                      False,
                                      {'skip aisle' : False})
                if result == 'skip aisle':
                    self.next_state = STOCKING_TRIP_AISLE
                    self._skip_aisle()

            if result != 'skip aisle':
                self._post_aisle_direction=''
                self._aisle_direction = self._current_put["aisle"]

    #----------------------------------------------------------
    def post_asile(self):
        ''' directing to Post Aisle'''
        #if aisle is same as post-aisle don't prompt
        if self._current_put["post_aisle"] != self._post_aisle_direction: 
            if self._current_put["post_aisle"] != '':
                prompt_ready(itext('stocking.put.post.aisle', 
                                   self._current_put["post_aisle"]))

            self._post_aisle_direction = self._current_put["post_aisle"]

    #----------------------------------------------------------
    def put_prompt(self):
        ''' launch the put prompt task '''
        self.dynamic_vocab.current_carton = self._current_put
        self.launch(obj_factory.get(BackStockingPutPromptTask,
                                      self._region_config_rec,
                                      self._trip_lut,
                                      self._current_put,
                                      self.taskRunner, self), 
                    STOCKING_TRIP_CHECK_NEXT_PUT)
        
    #----------------------------------------------------------        
    def _skip_aisle(self):
        ''' method to process the skip aisle command when spoken '''
        
        #check if region allows
        if not self._region_config_rec['allow_skip_aisle']:
            prompt_only(itext('generic.skip.aisle.notallowed'))
            return
         
        #confirm skip aisle
        if prompt_yes_no(itext('generic.skip.aisle.confirm')):
            curr_pre_aisle = self._current_put['pre_aisle']
            curr_aisle = self._current_put['aisle']
            for put in self._trip_lut:
                if put['status'] == 'N':
                    if put['pre_aisle'] == curr_pre_aisle and put['aisle'] == curr_aisle:
                        put['status'] = 'S'
            
            self.next_state = STOCKING_TRIP_CHECK_NEXT_PUT
        
