'''
This module contains custom Put to Store LUTS that extend the functionality of a 
basic LUT
'''
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core import class_factory

###############################################################
# Overridden Validate LPN LUT class 
###############################################################
class ValidateLPN(class_factory.get(VoiceLinkLut)):

    def __init__(self, command):
        super(ValidateLPN, self).__init__(command)
        self.canceled_cartons = False
        
    def do_transmit(self, *fields):
        #save previous location before request next one
        self.canceled_cartons = False
        return super().do_transmit(*fields)
    
    def valid_carton(self, carton_id, scanned):
        ''' checks if a carton value is valid '''
        for carton in self:
            if carton['status'] == 'N':
                if scanned and carton_id == carton['scanned_carton_id']:
                    return carton
                elif not scanned and carton_id == carton['spoken_carton_id']:
                    return carton

        #no match found
        return None
