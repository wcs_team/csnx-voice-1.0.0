from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_ready, prompt_only, prompt_yes_no, prompt_digits,\
    prompt_digits_required
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut
from common.RegionSelectionTasks import SingleRegionTask
from backstocking.BackStockingSystemBuildLPNTask import BackstockingSystemBuildLPNTask
from backstocking.BackStockingLuts import ValidateLPN
from backstocking.BackStockingOperBuildLPNTask import BackstockingOperBuildLPNTask
from backstocking.BackStockingPutTrip import BackStockingPutTripTask
from Globals import change_function
from backstocking.SharedConstants import BACK_STOCKING_TASK_NAME, REGIONS,\
    VALIDATE_REGION, STOCKING_GET_LICENSE, STOCKING_INDUCT_LICENSE,\
    STOCKING_BUILD_LICENSE, STOCKING_GET_TRIP, STOCKING_START_TRIP,\
    STOCKING_PUT_TRIP, STOCKING_CHECK_RESIDUALS, STOCKING_STOP_TRIP

###############################################################
# Overridden Single Region Class
###############################################################
class BackStockingRegionTask(class_factory.get(SingleRegionTask)):
    
    def _request_region_configs(self):
        ''' override to add function parameter, 
        hard coded for now since it can only be 11 '''
        return self._region_config_lut.do_transmit(self.selected_region, 
                                                   11)

###############################################################
# Main Class
###############################################################
class BackstockingTask(class_factory.get(TaskBase)):
    ''' Back Stocking task for the VoiceLink system
    
    Steps:
            1. Get region
            2. Validate Region
            3. Get License
            4. Induct License
            5. Build License
            6. Get Trip
            7. Start Trip
            8. Put Trip 
            9. Check Residuals
            10. Stop Trip
    '''
    #----------------------------------------------------------
    def __init__(self,
                 taskRunner = None, 
                 callingTask = None):
        super().__init__(taskRunner, callingTask)

        #Set task name
        self.name = BACK_STOCKING_TASK_NAME

        #define LUTs
        self._valid_regions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTStockingValidRegions')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTStockingGetRegionConfiguration')
        self._lpn_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTStockingGetLPN')
        self._valid_lpn_lut = obj_factory.get(ValidateLPN,
                                                'prTaskLUTStockingValidateLPN')
        self._trip_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTStockingGetTrip')
        self._stop_license_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTStockingStopLicense')

        #working variables
        self._region_selected = False
        self._staging_area = ''
        self._license = ''
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''

        #get region states
        self.addState(REGIONS,                  self.regions)
        self.addState(VALIDATE_REGION,          self.validate_regions)
        self.addState(STOCKING_GET_LICENSE,     self.get_license)
        self.addState(STOCKING_INDUCT_LICENSE,  self.induct_license)
        self.addState(STOCKING_BUILD_LICENSE,   self.build_license)
        self.addState(STOCKING_GET_TRIP,        self.get_trip)
        self.addState(STOCKING_START_TRIP,      self.start_trip)
        self.addState(STOCKING_PUT_TRIP,        self.put_trip)
        self.addState(STOCKING_CHECK_RESIDUALS, self.check_residuals)
        self.addState(STOCKING_STOP_TRIP,       self.stop_trip)
        
    #----------------------------------------------------------
    def regions(self):
        ''' Run single region selection '''
        self.launch(obj_factory.get(BackStockingRegionTask,
                                      self.taskRunner, self))

    #----------------------------------------------------------
    def validate_regions(self):
        ''' Temporary state until more development is done '''
        #check for valid regions, and that error code 3 
        #was not returned in either LUT
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
                self._region_selected = True

        #check if valid or not
        if not valid:
            self.next_state = ''
            prompt_only(itext('generic.regionNotAuth.prompt'))
            
    #----------------------------------------------------------
    def get_license(self):
        ''' request license information from host '''
        result = self._lpn_lut.do_transmit(self._region_config_lut[0]['number'])
        
        if result != 0:
            self.next_state = STOCKING_GET_LICENSE 
    
    #----------------------------------------------------------
    def induct_license(self):
        ''' induct license '''
        
        #prompt to go to staging area if different from previous one
        if self._staging_area != self._lpn_lut[0]['lpn_staging_area']:
            result = prompt_ready(itext('stocking.staging.area', 
                                        self._lpn_lut[0]['lpn_staging_area']),
                                  False,
                                  {'change function' : False,
                                   'change region' : False})
            if result == 'change function':
                change_function()
                self.next_state = STOCKING_INDUCT_LICENSE
                return
            elif result == 'change region':
                self.change_region()
                self.next_state = STOCKING_INDUCT_LICENSE
                return
            else:
                self._staging_area = self._lpn_lut[0]['lpn_staging_area']
    
        self._license = ''
        scanned = False #@UnusedVariable - used in condition checks
        max_len = len(self._lpn_lut[0]['lpn'])
        if max_len == 0:
            max_len = 5

        prompt_key = 'stocking.license.system'
        if self._lpn_lut[0]['lpn'] == '':
            prompt_key = 'stocking.license.operator'
            
        self._license, scanned = prompt_digits(itext(prompt_key, self._lpn_lut[0]['lpn']),
                                               itext('stocking.license.help'),
                                               1, max_len,
                                               self._lpn_lut[0]['lpn'] == '', #only confirm if unknown to start
                                               True,
                                               {'location' : False, 
                                                'change function' : False,
                                                'change region' : False})

        #check if change function spoken
        if self._license == 'change function':
            change_function()
            self.next_state = STOCKING_INDUCT_LICENSE

        #check if change region spoken
        elif self._license == 'change region':
            self.change_region()
            self.next_state = STOCKING_INDUCT_LICENSE

        #check if location was spoken
        elif self._license == 'location':
            prompt_only(itext('stocking.staging.area', self._staging_area))
            self.next_state = STOCKING_INDUCT_LICENSE

        #checks for system directed and valid license entered
        elif self._lpn_lut[0]['lpn'] != '':
            if ((scanned and self._lpn_lut[0]['scan_lpn'] != self._license)
                or (not scanned and self._lpn_lut[0]['lpn'] != self._license)):
                prompt_only(itext('stocking.invalid', self._license))
                self.next_state = STOCKING_INDUCT_LICENSE

        if self.next_state is None:
            #request LPN from host
            result = -1
            while result < 0:
                result = self._valid_lpn_lut.do_transmit(self._license)
            
            if result != 0:
                self.next_state = STOCKING_INDUCT_LICENSE

    #----------------------------------------------------------
    def build_license(self):
        ''' build license '''
        if self._valid_lpn_lut[0]['system_directed']:
            self.launch(obj_factory.get(BackstockingSystemBuildLPNTask,
                                          self._license,
                                          self._region_config_lut[0],
                                          self._valid_lpn_lut,
                                          self.taskRunner, self))
        else:
            self.launch(obj_factory.get(BackstockingOperBuildLPNTask,
                                          self._license,
                                          self._valid_lpn_lut,
                                          self.taskRunner, self))
        
    #----------------------------------------------------------
    def get_trip(self):
        ''' get trip information '''
        
        #check if reverse order allowed and prompt
        reverse = False
        if self._region_config_lut[0]['allow_reverse_order']:
            reverse = prompt_yes_no(itext('stocking.reverse'))
            if reverse:
                prompt_only(itext('stocking.reverse.confirmed'))
        
        #get trip from host
        result = -1
        while result < 0:
            result = self._trip_lut.do_transmit(self._license, int(reverse))

        if result != 0:
            self.next_state = STOCKING_GET_TRIP
        
    #----------------------------------------------------------
    def start_trip(self):
        ''' trip summary information '''
        result = prompt_ready(itext('generic.sayReady.prompt', 
                                    self._trip_lut[0]['trip_summary']), 
                              False, 
                              {'performance' : False})
        
        if result == 'performance':
            prompt_only(itext(self._trip_lut[0]['performance_summary']))
            self.next_state = STOCKING_START_TRIP
            
    #----------------------------------------------------------
    def put_trip(self):
        ''' launch the put trip task '''
        self.launch(obj_factory.get(BackStockingPutTripTask,
                                      self._region_config_lut[0],
                                      self._trip_lut,
                                      self.taskRunner, self))
        
    #----------------------------------------------------------
    def check_residuals(self):
        ''' check trip for skips or residuals '''
        canceled = self._valid_lpn_lut.canceled_cartons
        skipped = False
        for put in self._trip_lut:
            if put['status'] == 'S':
                put['status'] = 'N'
                skipped = True
            elif put['status'] == 'C':
                canceled = True
                
        if skipped:
            prompt_ready(itext('stocking.trip.complete'))
            self.next_state = STOCKING_PUT_TRIP
        elif not canceled:
            canceled = prompt_yes_no(itext('stocking.check.residuals'), True)

        #Either a put was canceled or operated had residuals
        if canceled:
            prompt = itext('stocking.residuals.deliver', self._trip_lut[0]['drop_zone'])
            if self._trip_lut[0]['drop_zone_cd'] == '':
                prompt_ready(prompt)
            else:
                prompt_digits_required(prompt, 
                                       itext('stocking.residuals.deliver.help'), 
                                       [self._trip_lut[0]['drop_zone_cd']])
                
    #----------------------------------------------------------
    def stop_trip(self):
        ''' request stop/complete license '''
        result = self._stop_license_lut.do_transmit(self._license)
        if result != 0:
            self.next_state = STOCKING_STOP_TRIP
        else:
            self.next_state = STOCKING_GET_LICENSE
    
    #======================================================
    #Global/Dynamic Word Functions
    #======================================================
    def change_region(self):
        ''' Called when operator speaks global word "change region" '''
        if self._region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self._region_selected = False
                self.return_to(self.name, REGIONS)
