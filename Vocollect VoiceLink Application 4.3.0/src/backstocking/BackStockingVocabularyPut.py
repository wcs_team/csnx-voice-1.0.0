from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary
from vocollect_core import itext, class_factory, obj_factory
from vocollect_core.dialog.functions import prompt_yes_no, prompt_only
import backstocking.BackStockingPutTrip

class BackStockingVocabularyPut(class_factory.get(DynamicVocabulary)):
    ''' Definition of additional vocabulary used in backstocking system build
    
    Parameters
            runner - Task runner object
    '''
    
    def __init__(self, region_config, runner):
        self.vocabs = {'repick skips':    obj_factory.get(Vocabulary,'repick skips', self._repick_skips, False),
                       'how much more':   obj_factory.get(Vocabulary,'how much more', self._how_much_more, False),
                       'repeat last put': obj_factory.get(Vocabulary,'repeat last put', self._repeat_last_put, False),
                       'carton ID':       obj_factory.get(Vocabulary,'carton ID', self._carton_id, False),
                       'case code':       obj_factory.get(Vocabulary,'case code', self._case_code, False),
                       'item number':     obj_factory.get(Vocabulary,'item number', self._item_number, False),
                       'description':     obj_factory.get(Vocabulary,'description', self._description, False),
                       'quantity':        obj_factory.get(Vocabulary,'quantity', self._quantity, False),
                       'location':        obj_factory.get(Vocabulary,'location', self._location, False),
                       'UPC':             obj_factory.get(Vocabulary,'UPC', self._upc, False),
                       }

        self.runner = runner
        self.region_config = region_config

        self.current_carton = None
        self.last_carton = None
        self.last_quantity = 0
        self.trip_lut = None
        
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''

        if vocab == 'repick skips':
            return self.trip_lut != None
        if vocab == 'how much more':
            return self.trip_lut != None
        if vocab == 'repeat last put':
            return self.last_carton != None
        if vocab == 'carton ID':
            return self.current_carton != None
        if vocab == 'case code':
            return self.current_carton != None
        if vocab == 'item number':
            return self.current_carton != None
        if vocab == 'description':
            return self.current_carton != None
        if vocab == 'quantity':
            return self.current_carton != None
        if vocab == 'location':
            return self.current_carton != None
        if vocab == 'UPC':
            return self.current_carton != None
        

        return False
    
        
    #Functions to execute words? 
    def _repick_skips(self):
        ''' process repick skips command '''
        if self.region_config['allow_repick_skips']:
            has_skips = False
            for carton in self.trip_lut:
                if carton['status'] == 'S':
                    has_skips = True
                    break
            
            if has_skips:
                if prompt_yes_no(itext('stocking.repick.confirm')):
                    for carton in self.trip_lut:
                        if carton['status'] == 'S':
                            carton['status'] = 'N'
                    self.runner.return_to('backStockingPutTrip',
                                          backstocking.BackStockingPutTrip.STOCKING_TRIP_RESET)
            else:
                prompt_only(itext('stocking.repick.noskips'))
        else:
            prompt_only(itext('stocking.repick.not.allowed'))
    
        return True
    
    def _how_much_more(self):
        lines = 0
        qty = 0
        for carton in self.trip_lut:
            if carton['status'] != 'P':
                lines += 1
                qty += carton['quantity']
                
        if lines == 1:
            prompt_only(itext('stocking.repick.howmuch.single', qty, lines))
        else:
            prompt_only(itext('stocking.repick.howmuch.plural', qty, lines))

        return True
    
    def _repeat_last_put(self):
        prompt_only(itext('stocking.last.put',
                          self.last_carton['aisle'],
                          self.last_carton['slot'],
                          self.last_quantity,
                          self.last_carton['quantity']))
        return True
    
    def _carton_id(self):
        if self.current_carton['spoken_carton_id'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['spoken_carton_id'])
        return True 
    
    def _case_code(self):
        if self.current_carton['UCN'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['UCN'])
        return True 
    
    def _item_number(self):
        if self.current_carton['item_number'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['item_number'])
        return True 
    
    def _description(self):
        if self.current_carton['description'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['description'].lower())
        return True 
    
    def _quantity(self):
        qty = self.current_carton['quantity']
        prompt_only(str(qty))
        return True
    
    def _location(self):
        prompt_only(itext('stocking.location',
                          self.current_carton['pre_aisle'],
                          self.current_carton['aisle'],
                          self.current_carton['post_aisle'],
                          self.current_carton['slot']))
        return True 
    
    def _upc(self):
        if self.current_carton['UPC'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['UPC'])
        return True 
    
    
