from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary
from vocollect_core import itext, class_factory, obj_factory
from vocollect_core.dialog.functions import prompt_yes_no, prompt_only
from backstocking.SharedConstants import BACKSTOCKING_SYSTEM_BUILD_ALL

class BackStockingVocabularyBuild(class_factory.get(DynamicVocabulary)):
    ''' Definition of additional vocabulary used in backstocking system build
    
    Parameters
            runner - Task runner object
    '''
    
    def __init__(self, region_config, lpn, runner):
        self.vocabs = {'repick skips':    obj_factory.get(Vocabulary,'repick skips', self._repick_skips, False),
                       'how much more':   obj_factory.get(Vocabulary,'how much more', self._how_much_more, False),
                       'carton ID':       obj_factory.get(Vocabulary,'carton ID', self._carton_id, False),
                       'case code':       obj_factory.get(Vocabulary,'case code', self._case_code, False),
                       'item number':     obj_factory.get(Vocabulary,'item number', self._item_number, False),
                       'description':     obj_factory.get(Vocabulary,'description', self._description, False),
                       'quantity':        obj_factory.get(Vocabulary,'quantity', self._quantity, False),
                       'license plate':   obj_factory.get(Vocabulary,'license plate', self._license_plate, False),
                       'UPC':             obj_factory.get(Vocabulary,'UPC', self._upc, False),
                       }

        self.runner = runner
        self.region_config = region_config
        self.lpn = lpn

        self.current_carton = None
        self.valid_lpn_lut = None
        
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''

        if vocab == 'repick skips':
            return self.valid_lpn_lut != None
        if vocab == 'how much more':
            return self.valid_lpn_lut != None
        if vocab == 'carton ID':
            return self.current_carton != None
        if vocab == 'case code':
            return self.current_carton != None
        if vocab == 'item number':
            return self.current_carton != None
        if vocab == 'description':
            return self.current_carton != None
        if vocab == 'quantity':
            return self.current_carton != None
        if vocab == 'license plate':
            return True
        if vocab == 'UPC':
            return self.current_carton != None
        

        return False
    
        
    #Functions to execute words? 
    def _repick_skips(self):
        ''' process repick skips command '''
        if self.region_config['allow_repick_skips']:
            has_skips = False
            for carton in self.valid_lpn_lut:
                if carton['status'] == 'S':
                    has_skips = True
                    break
            
            if has_skips:
                if prompt_yes_no(itext('stocking.repick.confirm')):
                    for carton in self.valid_lpn_lut:
                        if carton['status'] == 'S':
                            carton['status'] = 'N'
                    self.runner.return_to('backstockingSystemBuildLPNTask',
                                          BACKSTOCKING_SYSTEM_BUILD_ALL)
            else:
                prompt_only(itext('stocking.repick.noskips'))
    
        return True
    
    def _how_much_more(self):
        lines = 0
        qty = 0
        for carton in self.valid_lpn_lut:
            if carton['status'] != 'P':
                lines += 1
                qty += (carton['quantity_to_pick'] - carton['quantity_picked'])
                
        if lines == 1:
            prompt_only(itext('stocking.repick.howmuch.single', qty, lines))
        else:
            prompt_only(itext('stocking.repick.howmuch.plural', qty, lines))

        return True
    
    def _carton_id(self):
        #check is system or operator directed
        if self.region_config is None:
            carton = self.current_carton['spoken_carton_id']
        else:
            carton = self.current_carton['system_directed_carton_id']

        if carton == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(carton)
        return True 
    
    def _case_code(self):
        if self.current_carton['UCN'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['UCN'])
        return True 
    
    def _item_number(self):
        if self.current_carton['item_number'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['item_number'])
        return True 
    
    def _description(self):
        if self.current_carton['description'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['description'].lower())
        return True 
    
    def _quantity(self):
        qty = self.current_carton['quantity_to_pick'] - self.current_carton['quantity_picked']
        prompt_only(str(qty))
        return True
    
    def _license_plate(self):
        if self.lpn == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(itext('stocking.lpn', self.lpn))
        return True 
    
    def _upc(self):
        if self.current_carton['UPC'] == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.current_carton['UPC'])
        return True 
    
    
