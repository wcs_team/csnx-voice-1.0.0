from vocollect_core.task.task import TaskBase
from vocollect_core import itext, obj_factory, class_factory
from vocollect_core.dialog.functions import prompt_ready, prompt_only, prompt_digits, prompt_yes_no,\
    prompt_list_lut
from common.VoiceLinkLut import VoiceLinkLut
from backstocking.BackStockingVocabularyBuild import BackStockingVocabularyBuild
from backstocking.SharedConstants import STOCKING_SYSTEM_BUILD_TASK_NAME,\
    BACKSTOCKING_SYSTEM_BUILD_ALL, BACKSTOCKING_SYSTEM_NEXT_CARTON,\
    BACKSTOCKING_SYSTEM_CONFIRM_CARTON, BACKSTOCKING_SYSTEM_QUANITY,\
    BACKSTOCKING_SYSTEM_REASON, BACKSTOCKING_SYSTEM_XMIT_CARTON,\
    BACKSTOCKING_SYSTEM_NEXT_STEP

class BackstockingSystemBuildLPNTask(class_factory.get(TaskBase)):
    
    ''' Back Stocking task for system directed build of LPN
    
    Steps:
            1. Check Build All or intialize iterator
            2. get next carton from iterator
            3. confirm carton
            4. enter quantity picked
            5. check if reason coded needed and get reason code
            6. transmit up carton information
            7. once all cartons done check for go back skips/shorts
    '''
    #----------------------------------------------------------
    def __init__(self,
                 lpn,
                 region_rec,
                 valid_lpn_lut,
                 taskRunner = None, 
                 callingTask = None):
        super().__init__(taskRunner, callingTask)
    
        #Set task name
        self.name = STOCKING_SYSTEM_BUILD_TASK_NAME

        #Lut Defitions
        self._reason_code_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreGetReasonCodes')
        self._carton_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTStockingCarton')
        
        #working variables
        self._valid_lpn_lut = valid_lpn_lut
        self._valid_lpn_curr = None
        self._valid_lpn_iter = None

        self._region_rec = region_rec
        self._lpn = lpn
        
        self._quantity = 0
        self._canceled = False
        self._stopped = False
        self._shorted = False
        self._reason_code = ''
        
        #dynamic vocabulary
        self.dynamic_vocab = obj_factory.get(BackStockingVocabularyBuild,
                                               self._region_rec, 
                                               self._lpn, 
                                               self.taskRunner)
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''

        #get region states
        self.addState(BACKSTOCKING_SYSTEM_BUILD_ALL,        self.build_all)
        self.addState(BACKSTOCKING_SYSTEM_NEXT_CARTON,      self.next_carton)
        self.addState(BACKSTOCKING_SYSTEM_CONFIRM_CARTON,   self.confirm_carton)
        self.addState(BACKSTOCKING_SYSTEM_QUANITY,          self.quantity)
        self.addState(BACKSTOCKING_SYSTEM_REASON,           self.get_reason_code)
        self.addState(BACKSTOCKING_SYSTEM_XMIT_CARTON,      self.xmit_carton)
        self.addState(BACKSTOCKING_SYSTEM_NEXT_STEP,        self.next_step)
        
    #----------------------------------------------------------
    def build_all(self):
        ''' check if Full pallet '''
        if self._valid_lpn_lut[0]['all_cartons']:
            prompt_ready(itext('stocking.sys.build.all'),
                         True)
            self.next_state = BACKSTOCKING_SYSTEM_REASON
        else:
            self.dynamic_vocab.valid_lpn_lut = self._valid_lpn_lut
            self._valid_lpn_iter = iter(self._valid_lpn_lut)

    #----------------------------------------------------------
    def next_carton(self):
        ''' set next carton '''
        try:
            self._valid_lpn_curr = next(self._valid_lpn_iter)
            self.dynamic_vocab.current_carton = self._valid_lpn_curr
            if self._valid_lpn_curr['status'] != 'N':
                self.next_state = BACKSTOCKING_SYSTEM_NEXT_CARTON
        except:
            self.next_state = BACKSTOCKING_SYSTEM_NEXT_STEP
            
    #----------------------------------------------------------
    def confirm_carton(self):
        ''' confirm the carton being picked '''
        self._canceled = False
        self._stopped = False
        self._shorted = False
        self._quantity = 0
        
        prompt = itext('stocking.sys.build.carton',
                       self._valid_lpn_curr['system_directed_carton_id'],
                       self._valid_lpn_curr['quantity_to_pick'] - self._valid_lpn_curr['quantity_picked'],
                       self._valid_lpn_curr['prep_message'])
        additonal_vocab = {'ready' : False,
                           'cancel' : False,
                           'stop' : False,
                           'skip carton' : True}
            
        
        result = prompt_digits(prompt,
                               itext('stocking.sys.build.carton.help'),
                               1, 1000, 
                               False, 
                               True, 
                               additonal_vocab)

        #just need first value, don't care if it was scanned
        result = result[0]
        
        if result == 'cancel':
            if prompt_yes_no(itext('stocking.sys.build.cancel')):
                self._canceled = True
                self.next_state = BACKSTOCKING_SYSTEM_REASON
            else:
                self.next_state = BACKSTOCKING_SYSTEM_CONFIRM_CARTON
        elif result == 'stop':
            if prompt_yes_no(itext('stocking.sys.build.stop')):
                self._stopped = True
                self.next_state = BACKSTOCKING_SYSTEM_REASON
            else:
                self.next_state = BACKSTOCKING_SYSTEM_CONFIRM_CARTON
        elif result == 'skip carton':
            self._valid_lpn_curr['status'] = 'S'
            self.next_state = BACKSTOCKING_SYSTEM_NEXT_CARTON
        elif result == 'ready':
            if self._valid_lpn_curr['spoken_carton_id'] != '':
                prompt_only(itext('stocking.sys.build.ready.notallowed'))
                self.next_state = BACKSTOCKING_SYSTEM_CONFIRM_CARTON
        elif result not in [self._valid_lpn_curr['spoken_carton_id'],
                            self._valid_lpn_curr['scanned_carton_id']]:
                prompt_only(itext('stocking.wrong', result))
                self.next_state = BACKSTOCKING_SYSTEM_CONFIRM_CARTON
    
    #----------------------------------------------------------
    def quantity(self):
        ''' enter quantity picked '''
        qty = self._valid_lpn_curr['quantity_to_pick'] - self._valid_lpn_curr['quantity_picked'] 
        self._quantity = prompt_digits(itext('stocking.sys.build.quantity'), 
                                       itext('stocking.sys.build.quantity.help'), 
                                       1, 
                                       len(str(qty)),
                                       False) 

        self._quantity = int(self._quantity)
        if self._quantity > qty:
            prompt_only(itext('stocking.qty.over',
                              self._quantity, qty))
            self.next_state = BACKSTOCKING_SYSTEM_QUANITY
        elif self._quantity < qty:
            if prompt_yes_no(itext('stocking.sys.build.quantity.short', 
                                   self._quantity)):
                self._shorted = True
            else:
                self.next_state = BACKSTOCKING_SYSTEM_QUANITY
                
    #----------------------------------------------------------
    def get_reason_code(self):
        ''' if exception occured then get reason '''
        self._reason_code = ''
        if self._shorted or self._canceled or self._stopped:
            result = self._reason_code_lut.do_transmit('11', '1')
            if result != 0:
                self.next_state = BACKSTOCKING_SYSTEM_REASON
            else:
                self._reason_code = prompt_list_lut(self._reason_code_lut, 
                                                    'code', 'description',
                                                    itext('stocking.reason'), 
                                                    itext('stocking.reason.help'))
    
    #----------------------------------------------------------
    def xmit_carton(self):
        ''' Transmit carton information to validate '''

        #set values to send up
        carton = '0'
        uniqueID = self._valid_lpn_lut[0]['unique_id']
        qty = self._quantity
        if self._valid_lpn_lut[0]['all_cartons']:
            carton = '1'
            uniqueID = ''
            qty = ''
        elif self._canceled:
            carton = ''
            qty = 0
        elif self._stopped:
            qty = ''
        
        #transmit information
        result = self._carton_lut.do_transmit(self._lpn,
                                              carton,
                                              uniqueID,
                                              qty,
                                              self._reason_code,
                                              int(self._stopped))
        
        #check results
        if result == 97:
            self.next_state = BACKSTOCKING_SYSTEM_CONFIRM_CARTON
        elif result != 0:
            self.next_state = BACKSTOCKING_SYSTEM_XMIT_CARTON
        elif self._stopped:
            self.next_state = ''
        else:
            #If not doing all cartons, then update individual record
            if not self._valid_lpn_lut[0]['all_cartons']:
                self._valid_lpn_curr['quantity_picked'] += self._quantity
                self._valid_lpn_curr['status'] = 'P'
                if self._shorted:
                    self._valid_lpn_curr['status'] = 'R'

            #carton was canceled, so set flag in lut object for later reference
            if self._canceled:
                self._valid_lpn_lut.canceled_cartons = True
                
            self.next_state = BACKSTOCKING_SYSTEM_NEXT_CARTON
    
    #----------------------------------------------------------
    def next_step(self):
        ''' Check if we should go back for short and skips '''
        
        skips = False
        if self._region_rec['goback_shorts_skips']:
            for carton in self._valid_lpn_lut:
                if carton['status'] in ['S', 'R']:
                    carton['status'] = 'N'
                    skips = True
                    
            if skips:
                prompt_only(itext('stocking.sys.build.goback'))
                self.next_state = BACKSTOCKING_SYSTEM_BUILD_ALL
    