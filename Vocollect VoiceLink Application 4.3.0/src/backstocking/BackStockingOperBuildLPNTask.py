from vocollect_core.task.task import TaskBase
from vocollect_core import itext, obj_factory, class_factory
from vocollect_core.dialog.functions import prompt_ready, prompt_only, prompt_digits, prompt_yes_no
from common.VoiceLinkLut import VoiceLinkLut
from backstocking.BackStockingVocabularyBuild import BackStockingVocabularyBuild
from backstocking.SharedConstants import STOCKING_OPER_BUILD_TASK_NAME,\
    BACKSTOCKING_OPER_CONFIRM_CARTON, BACKSTOCKING_OPER_QUANITY,\
    BACKSTOCKING_OPER_XMIT_CARTON, BACKSTOCKING_OPER_NEXT_STEP

class BackstockingOperBuildLPNTask(class_factory.get(TaskBase)):
    
    ''' Back Stocking task for operator directed build of LPN
    
    Steps:
            1. enter/confirm carton
            2. enter quantity picked
            3. transmit up carton information
            4. once all cartons done check for go back skips/shorts
    '''
    #----------------------------------------------------------
    def __init__(self,
                 lpn,
                 valid_lpn_lut,
                 taskRunner = None, 
                 callingTask = None):
        super().__init__(taskRunner, callingTask)
    
        #Set task name
        self.name = STOCKING_OPER_BUILD_TASK_NAME

        #Lut Defitions
        self._carton_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTStockingCarton')
        
        #working variables
        self._valid_lpn_lut = valid_lpn_lut
        self._valid_lpn_curr = None
        self._lpn = lpn
        
        self._carton = ''
        self._quantity = 0
        self._stopped = False
        self._all_cartons = False

        #dynamic vocabulary
        self.dynamic_vocab = obj_factory.get(BackStockingVocabularyBuild,
                                               None, 
                                               self._lpn, 
                                               self.taskRunner)
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''

        #get region states
        self.addState(BACKSTOCKING_OPER_CONFIRM_CARTON,   self.confirm_carton)
        self.addState(BACKSTOCKING_OPER_QUANITY,          self.quantity)
        self.addState(BACKSTOCKING_OPER_XMIT_CARTON,      self.xmit_carton)
        self.addState(BACKSTOCKING_OPER_NEXT_STEP,        self.next_step)
        
    #----------------------------------------------------------
    def confirm_carton(self):
        ''' confirm the carton being picked '''
        self._stopped = False
        self._all_cartons = False
        self._quantity = 0
        self._valid_lpn_curr = None
        self.dynamic_vocab.current_carton = None
        
        result, scanned = prompt_digits(itext('stocking.oper.build.carton'),
                                        itext('stocking.oper.build.carton.help'),
                                        1, 1000, 
                                        False, 
                                        True, 
                                        {'stop' : False,
                                         'all cartons' : False,
                                         'license plate' : False})

        if result == 'stop':
            if prompt_yes_no(itext('stocking.oper.build.stop')):
                self._stopped = True
                self.next_state = BACKSTOCKING_OPER_XMIT_CARTON
            else:
                self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
        elif result == 'all cartons':
            if self._valid_lpn_lut[0]['all_cartons']:
                if prompt_yes_no(itext('stocking.oper.build.all')):
                    self._all_cartons = True
                    self.next_state = BACKSTOCKING_OPER_XMIT_CARTON
                else:
                    self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
            else:
                prompt_ready(itext('stocking.oper.build.all.not.allowed'))
                self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
        elif result == 'license plate':
            prompt_only(itext('stocking.oper.build.license', self._lpn))
            self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
        else:
            self._valid_lpn_curr = self._valid_lpn_lut.valid_carton(result, scanned) 
            if self._valid_lpn_curr is None:
                prompt_only(itext('stocking.wrong', result))
                self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
            else:
                self._carton = result
                self.dynamic_vocab.current_carton = self._valid_lpn_curr
    
    #----------------------------------------------------------
    def quantity(self):
        ''' enter quantity picked '''
        self._quantity = 0
        qty = self._valid_lpn_curr['quantity_to_pick'] - self._valid_lpn_curr['quantity_picked'] 
        result = prompt_digits(itext('stocking.oper.build.qty'), 
                                       itext('stocking.oper.build.qty.help'), 
                                       1, 
                                       len(str(qty)),
                                       False,
                                       False,
                                       {'cancel' : True}) 
        
        if result == 'cancel':
            self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
        else:
            self._quantity = int(result)
            if self._quantity > qty:
                prompt_ready(itext('stocking.qty.over',
                                  self._quantity, qty))
                self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
            elif self._quantity < qty:
                if not prompt_yes_no(itext('generic.correct.confirm', self._quantity)):
                    self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
                
    #----------------------------------------------------------
    def xmit_carton(self):
        ''' Transmit carton information to validate '''

        #set values to send to host
        carton = '0'
        uniqueID = ''
        if self._valid_lpn_curr is not None:
            uniqueID = self._valid_lpn_curr['unique_id']
        qty = self._quantity
        if self._all_cartons:
            carton = '2'
            uniqueID = ''
            qty = ''
        if self._stopped:
            uniqueID = ''
            qty = ''
            
        #transmit information
        result = self._carton_lut.do_transmit(self._lpn,
                                              carton,
                                              uniqueID,
                                              qty,
                                              '',
                                              int(self._stopped))
        
        #check results
        if result == 97:
            self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
        elif result != 0:
            self.next_state = BACKSTOCKING_OPER_XMIT_CARTON
        else:
            #If not doing all cartons, then update individual record
            if self._valid_lpn_curr is not None:
                self._valid_lpn_curr['quantity_picked'] += self._quantity
                if self._valid_lpn_curr['quantity_picked'] >= self._valid_lpn_curr['quantity_to_pick']:
                    self._valid_lpn_curr['status'] = 'P'

    #----------------------------------------------------------
    def next_step(self):
        ''' Check if we should go back for short and skips '''
        
        #stopped or all cartons, then done
        if self._stopped or self._all_cartons:
            return
        
        not_picked = False
        for carton in self._valid_lpn_lut:
            if carton['status'] not in ['P']:
                not_picked = True
                break
        
        if not_picked:
            self.next_state = BACKSTOCKING_OPER_CONFIRM_CARTON
        else:
            prompt_only(itext('stocking.oper.build.complete'))
