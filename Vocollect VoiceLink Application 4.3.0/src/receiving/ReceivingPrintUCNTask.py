from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core import itext, class_factory, obj_factory
from vocollect_core.dialog.functions import prompt_digits, prompt_only, prompt_yes_no
from core.Print import PrintTask
import receiving.ReceivingTask
from receiving.SharedConstants import RECEIVING_PRINT_TASK_NAME,\
    GET_PRINTER_LIST, ENTER_PRINTER, CONFIRM_PRINTER, XMIT_UCN_PRINT,\
    CONFIRM_UCN, SHOW_PRINTERS_HTML, RECEIVING_TASK_NAME
from vocollect_http.httpserver import set_display_page

#===========================================================
#Receiving UPC Printing Task
#===========================================================
class ReceivingPrintUCNTask(class_factory.get(PrintTask)):
    ''' Receiving task for Printing UPC
    
    Steps:
            1. Enter Printer Number
            2. Confirm Printer Number
            3. Print UPC
            4. Confirm UPC
    '''
    #-------------------------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        super(ReceivingPrintUCNTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = RECEIVING_PRINT_TASK_NAME

        #define luts
        self._print_lut = obj_factory.get(VoiceLinkLut, 'prTaskLutReceivingPrint')
        self._printer_list_lut = obj_factory.get(VoiceLinkLut, 'prTaskLutReceivingListOfPrinters')

        self.get_data_map = self.get_print_data_map
        self.on_entry_page = None
        self.on_entry_data_map = None
        
        self.title = callingTask.title
        self.operator_name = callingTask.operator_name
        
        #additional settings
        self.dynamic_vocab = None #Do not want the loading dynamics
        self.task = self.taskRunner.findTask(RECEIVING_TASK_NAME)
        if self.task is None:
            self.task = self.callingTask
    
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(GET_PRINTER_LIST, self.get_printer_list)
        self.addState(ENTER_PRINTER,    self.enter_printer)
        self.addState(CONFIRM_PRINTER,  self.confirm_printer)
        self.addState(XMIT_UCN_PRINT,   self.xmit_ucn_print)
        self.addState(CONFIRM_UCN,      self.confirm_ucn)
    #-------------------------------------------------------------------------
    def get_printer_list(self):
        ''' Gather the printer list through a LUT call '''
        result = self._printer_list_lut.do_transmit('Empty')
        
        if result < 0:
            self.next_state = GET_PRINTER_LIST
            return       
            
    #-------------------------------------------------------------------------
    def enter_printer(self):
        ''' Request a printer number from the operator; shows printers on the screen '''
        if not hasattr(self.task, 'printer_number') or self.task.printer_number is None:
            set_display_page(self, SHOW_PRINTERS_HTML)
            self.task.printer_number = prompt_digits(itext('generic.printer'), 
                                                            itext('generic.printer.help'), 
                                                            1, 2, 
                                                            False, #Confirm is done in next step for flow purposes 
                                                            False)
    #-------------------------------------------------------------------------
    def xmit_ucn_print(self):
        ''' print ucn '''
        result = self._print_lut.do_transmit(self.callingTask._current_item['ucn'],
                                             self.task.printer_number)
        
        if result < 0:
            self.next_state = XMIT_UCN_PRINT
        elif result > 0:
            self.next_state = ENTER_PRINTER   
        
    #-------------------------------------------------------------------------
    def confirm_ucn(self):
        ''' confirm ucn '''
        set_display_page(self, receiving.ReceivingTask.SHOW_ITEM_DETAILS_HTML)
        item_ucn = prompt_digits(itext('receiving.print.confirm.sku'),
                                    itext('receiving.print.confirm.sku.help'),
                                    scan = True)

        if int(item_ucn[0]) != int(self.callingTask._current_item['ucn']):
            reenter_sku = prompt_yes_no(itext('receiving.print.wrong.sku'))
            if reenter_sku:
                self.next_state = CONFIRM_UCN
            else:
                prompt_only(itext('receiving.print.skipping.item'))
                self.callingTask.current_state = receiving.ReceivingTask.RECEIVING_GET_ITEM
    #-------------------------------------------------------------------------
    def get_print_data_map(self, page_name):
        ''' custom method to return JSON-dumpable dict of info '''
        map = self.callingTask.get_data_map(page_name)
        
        printers = []
        for printer in self._printer_list_lut:
            printer_map = {}
            printer_map['number'] = printer['number']
            printer_map['description'] = printer['description']
            printer_map['location'] = printer['location']
            printers.append(printer_map)
            
        map['printers'] = printers

        return map