#---------------------------------------------------------------
#Receiving
RECEIVING_TASK_NAME = "receiving"

#States - Receiving
REGIONS                         = 'Regions'
VALIDATE_REGIONS                = 'validateRegions'
RECEIVING_GET_PO                = 'getPurchaseOrder'
RECEIVING_VALIDATE_PO           = 'validatePurchaseOrder'
RECEIVING_GET_ITEM              = 'getItemNumber'
RECEIVING_PRINT_UCN             = 'printUCN'
RECEIVING_GET_QUANTITY          = 'getQuantity'
RECEIVING_GET_PALLET_ID         = 'getPalletId'
RECEIVING_PALLET_COMPLETE       = 'palletComplete'

#---------------------------------------------------------------
#Receiving Print
RECEIVING_PRINT_TASK_NAME = "receivingPrintUPC"

#States - Receiving Print
GET_PRINTER_LIST    = 'getPrinterList'
ENTER_PRINTER       = 'enterPrinter'
CONFIRM_PRINTER     = 'confirmPrinter'
XMIT_UCN_PRINT      = 'printReport'
CONFIRM_UCN         = 'confirmUPC'



#================================================================
#Screens - Receiving
DEFAULT_HTML                    = 'waiting_for_po'
SHOW_PO_HTML                    = 'purchase_order'
SHOW_ITEM_DETAILS_HTML          = 'item'
SHOW_UNRECOG_ITEMS_HTML         = 'items_list'
SHOW_AUTH_HTML                  = 'authorization'

PAGE_TITLE                      = 'Receiving'
OPERATOR_NAME                   = 'No Operator'

#Screens - Receiving Printing
SHOW_PRINTERS_HTML                    = 'printer_list'

