from vocollect_core.task.task import TaskBase
from vocollect_core import obj_factory, itext, class_factory
from vocollect_core.dialog.functions import prompt_only, prompt_digits, prompt_yes_no, prompt_alpha_numeric
import voice

from common.RegionSelectionTasks import SingleRegionTask
from common.VoiceLinkLut import VoiceLinkLut
from receiving.ReceivingPrintUCNTask import ReceivingPrintUCNTask
from Globals import change_function
from receiving.SharedConstants import RECEIVING_TASK_NAME, DEFAULT_HTML,\
    PAGE_TITLE, OPERATOR_NAME, REGIONS, VALIDATE_REGIONS, RECEIVING_GET_PO,\
    RECEIVING_VALIDATE_PO, RECEIVING_GET_ITEM, RECEIVING_PRINT_UCN,\
    RECEIVING_GET_PALLET_ID, RECEIVING_GET_QUANTITY, RECEIVING_PALLET_COMPLETE,\
    SHOW_PO_HTML, SHOW_UNRECOG_ITEMS_HTML, SHOW_ITEM_DETAILS_HTML,\
    SHOW_AUTH_HTML
from vocollect_http.httpserver import set_display_page

class ReceivingTask(class_factory.get(TaskBase)):
    ''' Receiving task for the VoiceLink system
    
    Steps:
            1. Get region
            2. Validate regions
            3. Get PO
            4. Validate PO
            5. Display PO summary?
            6. Get Item
            7. Get LPN
            8. Quantity
            9. Submit quantity & LPN
            10. Next PO
    '''
    #----------------------------------------------------------
    def __init__(self,
                 taskRunner = None, 
                 callingTask = None):
        
        super(ReceivingTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = RECEIVING_TASK_NAME
        self.title = PAGE_TITLE
        self.operator_name = voice.getenv('Operator.Name', OPERATOR_NAME)
        
        self._region_conf = None
        self._purchase_orders = None
        self._region_selected = False

        self._item_ucn = None
        self._item_upc = None
        self._current_item = None
        self._pallet_id = None
        self._expiration_date = None
        
        #Define LUTs
        self._valid_regions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTReceivingValidRegions')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTReceivingGetRegionConfiguration')
        self._valid_po_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTReceivingValidPO')
        self._receive_item_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTReceivingItemReceived')

    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''

        #get region states
        self.addState(REGIONS,                      self.regions)
        self.addState(VALIDATE_REGIONS,             self.validate_regions)
        self.addState(RECEIVING_GET_PO,             self.get_purchase_order)
        self.addState(RECEIVING_VALIDATE_PO,        self.validate_purchase_order)
        self.addState(RECEIVING_GET_ITEM,           self.get_item_number)
        self.addState(RECEIVING_PRINT_UCN,          self.print_label)
        self.addState(RECEIVING_GET_PALLET_ID,      self.get_pallet_id)
        self.addState(RECEIVING_GET_QUANTITY,       self.get_quantity)
        self.addState(RECEIVING_PALLET_COMPLETE,    self.pallet_complete)
    
    #----------------------------------------------------------
    def regions(self):
        ''' Run single region receiving '''
        self._region_selected = False
        self.launch(obj_factory.get(SingleRegionTask,
                                      self.taskRunner, self))
        
    #----------------------------------------------------------
    def validate_regions(self):
        ''' validates region selection '''
        #check for valid regions
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
        
        if not valid:
            prompt_only(itext('generic.regionNotAuth.prompt'))
            self.next_state = ''
        else:
            self._region_conf = self._region_config_lut[0]
            self._region_selected = True
    
    #----------------------------------------------------------
    def get_purchase_order(self):
        ''' gets the purchase order from the operator '''
        set_display_page(self, DEFAULT_HTML)
        additional_vocab = {'change function' : False, 'change region' : False}
        
        self._purchase_orders = prompt_alpha_numeric(itext('receiving.purchase.order'), 
                                                     itext('receiving.purchase.order.help'),
                                                     scan=True,
                                                     additional_vocab = additional_vocab)[0]
                                              
        if self._purchase_orders == 'change function':
            change_function()
            self.next_state = self.current_state
            
        elif self._purchase_orders == 'change region':
            self.change_region()
            self.next_state = self.current_state

    #----------------------------------------------------------
    def validate_purchase_order(self):
        ''' validate purchase order from host '''
        result = self._valid_po_lut.do_transmit(self._purchase_orders)
        
        if result > 0:
            self.next_state = RECEIVING_GET_PO
        elif result < 0:
            self.next_state = RECEIVING_VALIDATE_PO
        else:
            set_display_page(self, SHOW_PO_HTML)
            
        # Check for hot items
        if self.any_rush_items():
            prompt_only(itext('receiving.purchase.order.rush.items'))
            
    #----------------------------------------------------------
    def get_item_number(self):
        ''' has the operator input the item number (known as SKU or UCN);
            the purchase order is shown on entry to this state
            the unrecognized items list is shown if UCN/SKU and the UPC are unknown
            the item details are shown if the UCN/SKU or the UPC are identified '''
        
        set_display_page(self, SHOW_PO_HTML)
        
        # Reset the values
        self._item_ucn = None
        self._item_upc = None
        
        self._item_ucn = prompt_alpha_numeric(itext('receiving.sku'), itext('receiving.sku.help'), scan=True)[0]
        self._current_item = self.find_item_by_attribute('ucn', self._item_ucn)
     
        # if item not found by ucn. let's try the upc
        if self._current_item == None:
            self._item_upc = prompt_alpha_numeric(itext('receiving.sku.not.found'), 
                                                  itext('receiving.sku.not.found.help'), scan=True)[0]
            self._current_item = self.find_item_by_attribute('upc',  self._item_upc)
            # Let's keep track of the UCN on the case, so the operator doesn't have to scan the UPC next time
            #self._current_item['actual_ucn'] = self._item_ucn
            
        # if still not found, the user can print a label
        if self._current_item == None:
            set_display_page(self, SHOW_UNRECOG_ITEMS_HTML)
            item_number = int(prompt_digits(itext('receiving.choose.item'), itext('receiving.choose.item.help'), 1, 10, False, False))
            
            # check if the item number is within range
            if item_number >= 1 and item_number <= len(self._valid_po_lut):
                self._current_item = self._valid_po_lut[item_number - 1]

            if self._current_item == None:
                # The item must have been outside of the range of valid items.
                prompt_only(itext('receiving.item.not.found'))
                self.next_state = RECEIVING_GET_ITEM
            else:
                self.next_state = RECEIVING_PRINT_UCN
        else:
            self.next_state = RECEIVING_GET_PALLET_ID
            set_display_page(self, SHOW_ITEM_DETAILS_HTML)
    
    #----------------------------------------------------------
    def print_label(self):
        ''' get the printer number and then print the UCN '''
        self.launch(obj_factory.get(ReceivingPrintUCNTask,
                                          self.taskRunner, self))
        
    #----------------------------------------------------------
    def get_quantity(self):
        ''' verifies that the quantities match '''
        expected_quantity = self._current_item['expected_quantity']
        remaining = expected_quantity - self._current_item['amount_received']
        self._qty_received = None
        if expected_quantity != None:
            
            if remaining > 0:
                prompt = itext('receiving.item.quantity')
                help = itext('receiving.item.quantity.help')
    
                #get the quantity
                result = prompt_digits(prompt, help, 1, 10, False, False)
    
                #check result
                self._qty_received = int(result)
                
                # too many received; can authorize an override
                if self._qty_received > remaining:
                    self.get_authorization(remaining)
                else:
                    self._current_item['amount_received'] = self._current_item['amount_received'] + \
                                                                self._qty_received
                    prompt_only(itext('receiving.item.remaining', expected_quantity - self._current_item['amount_received']))
    
    #----------------------------------------------------------
    def get_authorization(self, remaining):
        ''' checks for authorization from the supervisor '''
        authorize = prompt_yes_no(itext('receiving.authorize',
                                        self._qty_received,
                                        remaining))
        if authorize:
            set_display_page(self, SHOW_AUTH_HTML)
            authorized = prompt_digits(itext('receiving.authorize.quantity'),
                                       itext('receiving.authorize.quantity.help'), 1, 10, False, False)
            
            #TODO: this needs to verify that the code is legitimate!
            if True or authorized:
                prompt_only(itext('receiving.authorize.approved', self._qty_received))
                self._current_item['amount_received'] = self._current_item['amount_received'] + \
                                                    self._qty_received
            else:
                self.next_state = RECEIVING_GET_QUANTITY
        else:
            self.next_state = RECEIVING_GET_QUANTITY
                    
    #----------------------------------------------------------
    def get_pallet_id(self):
        ''' gets the item number from the operator '''
        set_display_page(self, SHOW_ITEM_DETAILS_HTML)
        self._pallet_id = prompt_alpha_numeric(itext('receiving.lpn'), itext('receiving.lpn.help'), scan=True)[0]

    #----------------------------------------------------------
    def pallet_complete(self):
        ''' submit lut to host with received item and lpn '''
        result = self._receive_item_lut.do_transmit(self._current_item['ucn'], self._pallet_id, self._qty_received, self._expiration_date)
        
        if result > 0:
            self.next_state = RECEIVING_GET_PO
            return
        elif result < 0:
            # SOME ERROR HAS OCCURRED
            prompt_only(itext('receiving.error')) 
            
        
        if self.have_all_items_been_received():
            set_display_page(self, SHOW_PO_HTML)
            yesno = prompt_yes_no(itext('receiving.po.complete'))
            if yesno:
                self.next_state = RECEIVING_GET_PO
                set_display_page(self, DEFAULT_HTML)
            else:
                self.next_state = RECEIVING_GET_ITEM
                set_display_page(self, SHOW_PO_HTML)
        else:
            self.next_state = RECEIVING_GET_ITEM
            set_display_page(self, SHOW_PO_HTML)
        
    #----------------------------------------------------------
    def have_all_items_been_received(self):
        ''' Determines if all items have been received '''
        for item in self._valid_po_lut:
            if item['amount_received'] < item['expected_quantity']:
                return False
        
        return True
    #----------------------------------------------------------
    def any_rush_items(self):
        ''' Checks to see if any item is flagged for immediate putaway '''
        for item in self._valid_po_lut:
            if item['rush_item']:
                return True
        
        return False
    #---------------------------------------------------------
    def find_item_by_attribute(self, attribute, value):
        ''' Helper for finding item by specified attribute and value '''
        
        # Look for this item in the list of items
        for item in self._valid_po_lut:
            if item[attribute] == value:
                return item
        return None

    #----------------------------------------------------------
    def get_data_map(self, page_name):
        ''' custom method to return JSON-dumpable dict of info '''
        map = {}
        
        map['title'] = self.title
        map['operator'] = self.operator_name
        
        if page_name == SHOW_PO_HTML:
            po_map = {}
            po_map['number'] = self._purchase_orders
            po_map['description'] = self._valid_po_lut[0]['po_description']
            items = []
            for item in self._valid_po_lut:
                item_map = {}
                item_map['ucn'] = item['ucn']
                item_map['description'] = item['description']
                item_map['quantity'] = item['expected_quantity'] - item['amount_received']
                items.append(item_map)
            po_map['items'] = items
            map['po'] = po_map
        elif page_name == SHOW_ITEM_DETAILS_HTML:
            item_map = {}
            item_map['ucn'] = self._current_item['ucn']
            item_map['description'] = self._current_item['description']
            item_map['quantity'] = self._current_item['expected_quantity'] - self._current_item['amount_received']
            item_map['po'] = self._purchase_orders
            map['current_item'] = item_map
        elif page_name == SHOW_UNRECOG_ITEMS_HTML:
            items = []
            counter = 1;
            for item in self._valid_po_lut:
                item_map = {}
                item_map['ucn'] = item['ucn']
                item_map['upc'] = item['upc']
                item_map['description'] = item['description']
                item_map['quantity'] = item['expected_quantity'] - item['amount_received']
                item_map['index'] = counter
                items.append(item_map)
                counter += 1
            map['items'] = items 

        return map
        
    #======================================================
    #Global/Dynamic Word Functions
    #======================================================
    def change_region(self):
        ''' Called when operator speaks global word "change region" '''
        if self._region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self._region_selected = False
                self.return_to(self.name, REGIONS)
