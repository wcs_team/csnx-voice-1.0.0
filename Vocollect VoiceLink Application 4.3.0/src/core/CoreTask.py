from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_only, prompt_ready, prompt_digits, prompt_list_lut_auth, prompt_yes_no, prompt_words
from vocollect_core import itext, obj_factory
from vocollect_core.utilities import class_factory
from common.VoiceLinkLut import VoiceLinkLut
from voice import globalwords as gw
from voice import getenv
from voice import get_voice_application_property

from httpserver_receiving import DEFAULT_PAGE
from vocollect_http.httpserver import set_display_page

#Tasks
from core.VehicleTask import VehicleTask
from core.TakeBreakTask import TakeBreakTask
from backstocking.BackStockingTask import BackstockingTask
from lineloading.LineLoadingTask import LineLoadingTask
from loading.LoadingTask import LoadingTask
from forkapps.PutawayTask import PutawayTask
from forkapps.ReplenishmentTask import ReplenishmentTask
from puttostore.PutToStoreTask import PutToStoreTask
from selection.SelectionTask import SelectionTask
from cyclecounting.CycleCountingTask import CycleCountingTask
from receiving.ReceivingTask import ReceivingTask
from core.SharedConstants import CORE_TASK_NAME, REQUEST_CONFIG, REQUEST_BREAKS,\
    GET_WELCOME, REQUEST_SIGNON, REQUEST_VEHICLES, REQUEST_FUNCTIONS,\
    EXECUTE_FUNCTIONS

###############################################################
# Configuration LUT
###############################################################
class ConfigurationLut(class_factory.get(VoiceLinkLut)):

    def _process_error(self, error, message):
        ''' Protected method to process error codes. 
        Override if any special errors 
        
        Parameters:
                error - error code received
                message - message received
        returns: error code modified if handled
        '''
        return_error = error
        
        if error == 99:
            prompt_ready(itext('generic.sayReady.prompt', message))
            return_error = 0
        elif error > 0:
            prompt_ready(message)
    
        return return_error


###############################################################
# SignOff LUT
###############################################################
class SignOffLut(class_factory.get(VoiceLinkLut)):

    def _process_error(self, error, message):
        ''' Protected method to process error codes. 
        Override if any special errors 
        
        Parameters:
                error - error code received
                message - message received
        returns: error code modified if handled
        '''
        return_error = error
        if error == 99:
            prompt_ready(itext('generic.sayReady.prompt', message))
            return_error = 0
        elif error == 97:
            return_error = 0
        elif error > 0:
            prompt_ready(itext('generic.continue.prompt', message))
    
        return return_error

###############################################################
# Core Task Class
###############################################################
class CoreTask(class_factory.get(TaskBase)):
    ''' Core task for the VoiceLink system
    
    Steps:
            1. Get core configuration
            2. Get list of break types
            3. Welcome prompt
            4. Sign on
            5. Run vehicle task
            6. request list of functions from host
            7. prompt for and execute specified function
    '''
    #----------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        super().__init__(taskRunner, callingTask)

        #Set task name
        self.name = CORE_TASK_NAME
        
        #working variables
        self.password = None
        self.function = None
        self.sign_off_allowed = True
        
        #Core Luts
        self._config_lut = obj_factory.get(ConfigurationLut, 'prTaskLUTCoreConfiguration')
        self._break_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreBreakTypes')
        self._signon_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreSignOn')
        self._functions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreValidFunctions')
        self._sign_off_lut = obj_factory.get(SignOffLut, 'prTaskLUTCoreSignOff')


    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(REQUEST_CONFIG,    self.request_configurations)
        self.addState(REQUEST_BREAKS,    self.request_breaks)
        self.addState(GET_WELCOME,       self.welcome)
        self.addState(REQUEST_SIGNON,    self.request_signon)
        self.addState(REQUEST_VEHICLES,  self.vehicles)
        self.addState(REQUEST_FUNCTIONS, self.request_functions)
        self.addState(EXECUTE_FUNCTIONS, self.execute_function)
        
    #----------------------------------------------------------
    def request_configurations(self):
        ''' request core configuration LUT '''
        self.sign_off_allowed = True
        gw.words['sign off'].enabled = False
        gw.words['take a break'].enabled = False
        if not self._transmit_configuration():
            self.next_state = REQUEST_CONFIG

    #----------------------------------------------------------
    def request_breaks(self):
        ''' request break type list '''
        if self._break_lut.do_transmit() != 0:
            self.next_state = REQUEST_BREAKS
        
    #----------------------------------------------------------
    def welcome(self):
        ''' welcome prompt '''
        prompt_ready(itext('core.welcome.prompt', 
                     self._config_lut[0]['customerName'],
                     getenv('Operator.Name', '')))
        
    #----------------------------------------------------------
    def request_signon(self):
        ''' Sign on process '''
        
        pass_word = prompt_digits(itext('core.password.prompt'), 
                                      itext('core.password.help'), 
                                      1, 10, 
                                      self._config_lut[0]['confirmPassword'] > 0)
        
        #check if configuration lut needs resent
        if self._config_lut[0]['operatorId'] != getenv('Operator.Id', ''):
            while not self._transmit_configuration():
                pass
        
        result = -1
        while result < 0:    
            result = self._signon_lut.do_transmit(pass_word)
        
        if result == 0 :
            self.password = pass_word
            gw.words['sign off'].enabled = True
            gw.words['take a break'].enabled = True
            if get_voice_application_property('CollectVehicleInfo') != 'true':
                self.next_state = REQUEST_FUNCTIONS
        else:
            self.password = None
            self.next_state = REQUEST_SIGNON


    #----------------------------------------------------------
    def vehicles(self):
        ''' Run vehicle types task, return to request function '''
        #EXAMPLE: Launch task from another task using launch method
        #         when calling this way, user must set next state
        #         before calling launch, otherwise after launched task
        #         completes the state is repeated (execute_function is 
        #         example of state being repeated)
        self.launch(obj_factory.get(VehicleTask, self.taskRunner))
    
    #----------------------------------------------------------
    def request_functions(self):
        ''' request function from host '''
        self.sign_off_allowed = True
        if not self.xmit_functions():
            self.next_state = REQUEST_FUNCTIONS
                
    #----------------------------------------------------------
    def execute_function(self):
        ''' prompt for function and execute it '''
        self.sign_off_allowed = True
        
        #if only 1 record returned then automatically select that function
        self.function = None
        if len(self._functions_lut) == 1:
            self.function = self._functions_lut[0]['number']
            prompt_only(self._functions_lut[0]['description'])
        #else prompt user to select function
        else:
            self.function = prompt_list_lut_auth(self._functions_lut, 
                                            'number', 'description',  
                                            itext('core.function.prompt'), 
                                            itext('core.function.help'))
        
        self.function = str(self.function)
        #Execute selected function
        if self.function == '1': #PutAway
            self.launch(obj_factory.get(PutawayTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '2': #Replenishment
            self.launch(obj_factory.get(ReplenishmentTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function in ['3', '4', '6']: #Selection
            self.launch(obj_factory.get(SelectionTask, self.function, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '7': #Line Loading
            self.launch(obj_factory.get(LineLoadingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '8': #Put to store
            self.launch(obj_factory.get(PutToStoreTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '9': #Cycle Counting
            self.launch(obj_factory.get(CycleCountingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '10': #Loading
            self.launch(obj_factory.get(LoadingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '11': #Back Stocking
            self.launch(obj_factory.get(BackstockingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        elif self.function == '12': #Receiving
            self.launch(obj_factory.get(ReceivingTask, self.taskRunner, self), 
                        EXECUTE_FUNCTIONS)
        else: #Function specified not implemented
            prompt_ready(itext('core.function.notimplemented', str(self.function)))
            self.next_state = REQUEST_FUNCTIONS 
            
    #----------------------------------------------------------
    def xmit_functions(self):
        ''' request function from host '''
        #get list of valid functions
        if self._functions_lut.do_transmit(0) != 0:
            return False
        else:
            #check if any valid functions were received.
            count = 0
            for rec in self._functions_lut:
                if rec['number'] > 0:
                    count += 1
            
            #if no valid functions
            if count == 0:
                result = prompt_words(itext("core.noFunctions.prompt"), False, {'sign off':False} )
                if result == 'sign off':
                    self.sign_off()

        return True

    def _transmit_configuration(self):
        ''' called from configuration state and password state '''
        return self._config_lut.do_transmit(getenv('SwVersion.Locale', 'en_US'), 
                                            get_voice_application_property('SiteName'), 
                                            get_voice_application_property('TaskRevision')) == 0


    #======================================================
    #Global Words Function
    def sign_off_confirm(self, allowed = False):
        ''' sign off with confirm '''
        if allowed or self.sign_off_allowed:
            if self.password != None:
                if prompt_yes_no(itext('core.signoff.confirm'), False):
                    self.sign_off()
        else:
            prompt_only(itext('core.signoff.not.allowed'))
            
    def sign_off(self):
        ''' sign off with no confirm '''
        result = -1
        while result != 0:
            result = self._sign_off_lut.do_transmit()
       
        set_display_page(self, DEFAULT_PAGE)
       
        self.password = None
        self.return_to(self.name, REQUEST_CONFIG)
    
    def change_function(self):
        ''' process change function command '''
        while not self.xmit_functions():
            pass
         
        count = 0
        for function in self._functions_lut:
            if function['number'] > 0:
                count += 1
                
        if self.function == None:
            return
        elif count == 1:
            prompt_only(itext('core.change.function.not.allowed'), True)
        else:
            if prompt_yes_no(itext('generic.correct.confirm', 'change function'), True):
                set_display_page(self, DEFAULT_PAGE)
                self.return_to(self.name, EXECUTE_FUNCTIONS)
    
    def take_a_break(self):
        ''' process take a break command '''
        count = 0
        for breaktype in self._break_lut:
            if breaktype['type'] > 0:
                count += 1
                
        if self.password == None:
            return
        elif count == 0:
            prompt_ready(itext('core.take.a.break.not.allowed'), True)
        else:
            if prompt_yes_no(itext('generic.correct.confirm', 'take a break'), True):
                
                self.launch(obj_factory.get(TakeBreakTask,
                                              self._break_lut, 
                                              self.password, 
                                              self.taskRunner),
                            self.current_state)