from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_alpha_numeric, prompt_ready, prompt_only, prompt_list_lut_auth, prompt_yes_no, prompt_digits
from vocollect_core import itext, class_factory, obj_factory
from common.VoiceLinkLut import VoiceLinkLut
from core.SharedConstants import VEHICLE_TASK_TASK_NAME, REQUEST_VEHTYPE,\
    REQUEST_VEHICLEID, REQUEST_XMIT_VEHID, NEXT_SAFETY_CHECK,\
    EXECUTE_SAFETYCHECK, COMPLETE_SAFETYCHECK

class VehicleTask(class_factory.get(TaskBase)):
    ''' Vehicle task for the voicelink system
    
    Steps:
            1. Request vehicle types, ends task if no vehicles
            2. Request vehicle IDs
            3. Transmit vehicle ID
            4. Get next safety check
            5. Execute safety check list
            6. Complete safety check
    '''
    #----------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        super().__init__(taskRunner, callingTask)

        #Set task name
        self.name = VEHICLE_TASK_TASK_NAME

        #define LUTS
        self._veh_types_lut  = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreValidVehicleTypes')
        self._veh_id_lut     = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreSendVehicleIDs')
        self._veh_safety_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreSafetyCheck')
        
        #working variables
        self.vehicle_type = ''
        self.vehicle_id = ''
        self._curr_saftey_check = None
        self._safety_check_iter = None
        self.check_response = ''
        
    
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get vehicle states
        self.addState(REQUEST_VEHTYPE,      self.request_veh_types)
        self.addState(REQUEST_VEHICLEID,    self.request_vehicle_id)
        self.addState(REQUEST_XMIT_VEHID,   self.xmit_veh_id)
        self.addState(NEXT_SAFETY_CHECK,    self.next_safety_check)
        self.addState(EXECUTE_SAFETYCHECK,  self.execute_safety_check)
        self.addState(COMPLETE_SAFETYCHECK, self.complete_safety_check)

    #----------------------------------------------------------
    def request_veh_types(self):
        ''' Request list of vehicle types from host '''
        
        self.vehicle_type = ''
        
        if self._veh_types_lut.do_transmit('0') != 0:
            self.next_state = REQUEST_VEHTYPE
        else:
            #Check for any valid vehicle types
            self.next_state = '' #no next state by default
            for veh in self._veh_types_lut:
                if veh['type'] > 0: #vehicle found so set next state
                    self.next_state = REQUEST_VEHICLEID
                    break
    
    #----------------------------------------------------------
    def request_vehicle_id(self):
        ''' Prompt operator for Vehicle type and ID if needed '''
        #Prompt user for vehicle
        self.vehicle_id = ''
        
        # Bypass vehicle type selection if there is only one type
        if len(self._veh_types_lut) > 1 and self.vehicle_type == '':
            self.vehicle_type = prompt_list_lut_auth(self._veh_types_lut, 
                                                     'type', 'description',  
                                                     itext("core.vehicleType.prompt"),
                                                     itext("core.vehicleType.help"))
        elif self.vehicle_type == '': 
            self.vehicle_type = str(self._veh_types_lut[0]['type'])
        
        #get record of vehicle selected
        veh_rec = None
        for veh in self._veh_types_lut:
            if veh['type'] == int(self.vehicle_type):
                veh_rec = veh
                break
        
        if veh_rec is not None:    
            #Get ID if required
            if veh_rec['captureId']:
                #TODOD: DYNAMIC - Needs A-Z
 
                self.vehicle_id = prompt_alpha_numeric(itext("core.vehicleID.prompt"),
                                                itext("core.vehicleID.help"), 
                                                1, 10, 
                                                True)
                
        else:
            self.next_state = ''

    #----------------------------------------------------------
    def xmit_veh_id(self):
        #send type and id back to host
        result = self._veh_id_lut.do_transmit(self.vehicle_type, self.vehicle_id)
        if result < 0:
            self.next_state = REQUEST_XMIT_VEHID
        elif result > 0:
            self.next_state = REQUEST_VEHICLEID
        else:
            #check if safety check is required
            self.next_state = ''
            if self._veh_id_lut[0]['safetyCheck'] != '':
                self._safety_check_iter = iter(self._veh_id_lut)
                prompt_only(itext("core.startSafetyCheck.prompt"))
                self.next_state = NEXT_SAFETY_CHECK
                
    #----------------------------------------------------------
    def next_safety_check(self):
        ''' get next safety check '''
        try:
            self._curr_saftey_check = next(self._safety_check_iter)
        except:
            self.next_state = COMPLETE_SAFETYCHECK
            return
        
        if self._curr_saftey_check['safetyCheck'] == '':
            self.next_state = NEXT_SAFETY_CHECK
    
    #----------------------------------------------------------
    def execute_safety_check(self):
        ''' Execute safety check list '''

        self.check_response = ''
        '''Capture check response'''
        if self._curr_saftey_check['responseType'] == 'N':
            self.check_response = prompt_digits(self._curr_saftey_check['safetyCheck'],
                                            itext("core.safetyCheckResponse.help"), 
                                            1, 5, True, False)
        elif self._curr_saftey_check['responseType'] == 'B' or self._curr_saftey_check['responseType'] == 'C':
            self.check_response = prompt_yes_no(self._curr_saftey_check['safetyCheck'], True)
            
        if self.check_response == True:
            self.next_state = NEXT_SAFETY_CHECK
        elif self.check_response == False:
            if self._curr_saftey_check['responseType'] == 'B':
                #if failed, confirm and the check type is stop on no
                self.next_state = EXECUTE_SAFETYCHECK
                if prompt_yes_no(itext("core.startSafetyCheckFail.Confirm",
                            self._curr_saftey_check['safetyCheck']), True):
                    #if confirmed, ask if quick fix can be done
                    if prompt_yes_no(itext('core.safetyCheckFailed'), True):
                        while self._veh_safety_lut.do_transmit(-2, self._curr_saftey_check['safetyCheck'], 2) != 0:
                            pass                        
                        prompt_ready(itext("core.safetyQuickRepair.prompt"), True)
                    else:
                        while self._veh_safety_lut.do_transmit(-2, self._curr_saftey_check['safetyCheck'], 1) != 0:
                            pass                        
                        prompt_only(itext('core.safetyNewVehicle.prompt'))
                        self.next_state = REQUEST_VEHTYPE
                        return
            elif self._curr_saftey_check['responseType'] == 'C':
                #if type is not stop on no
                self.next_state = NEXT_SAFETY_CHECK
                while self._veh_safety_lut.do_transmit(-4, self._curr_saftey_check['safetyCheck'], 0) != 0:
                    pass  
        else:
            #if numeric value entered
            self.next_state = NEXT_SAFETY_CHECK
            while self._veh_safety_lut.do_transmit(self.check_response, self._curr_saftey_check['safetyCheck'], 0) != 0:
                pass                        
            #prompt_ready('Sent say ready', True
                    

    #----------------------------------------------------------
    def complete_safety_check(self):
        ''' completed all safety checks so inform host '''
        
        #If here, then all checks passed or where fixed
        result = self._veh_safety_lut.do_transmit(-1, '', 0)
        
        if result != 0:
            self.next_state = COMPLETE_SAFETYCHECK
