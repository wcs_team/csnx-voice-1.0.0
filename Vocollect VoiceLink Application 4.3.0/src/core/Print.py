from vocollect_core.task.task import TaskBase
from vocollect_core import itext, class_factory
from vocollect_core.dialog.functions import prompt_digits, prompt_yes_no
from core.SharedConstants import PRINT_TASK_NAME, ENTER_PRINTER, CONFIRM_PRINTER

#===========================================================
#Main Printing task Task
#===========================================================
class PrintTask(class_factory.get(TaskBase)):
    ''' Main printing task that prompts for printer
    
    Steps:
            1. Enter Printer Number
            2. Confirm Printer Number
    '''

    #-------------------------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        super().__init__(taskRunner, callingTask)

        #Set task name
        self.name = PRINT_TASK_NAME

        self.dynamic_vocab = None #Do not want the dynamics from calling task
        
    #-------------------------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States '''
        
        #get region states
        self.addState(ENTER_PRINTER, self.enter_printer)
        self.addState(CONFIRM_PRINTER, self.confirm_printer)
        
    #-------------------------------------------------------------------------
    def enter_printer(self):
        if not hasattr(self.task, 'printer_number') or self.task.printer_number is None:
            self.task.printer_number = prompt_digits(itext('generic.printer'), 
                                                            itext('generic.printer.help'), 
                                                            1, 2, 
                                                            False, #Confirm is done in next step for flow purposes 
                                                            False)
        

    #-------------------------------------------------------------------------
    def confirm_printer(self):
        if not prompt_yes_no(itext('generic.printer.confirm', 
                                        self.task.printer_number), True):
            self.task.printer_number = None
            self.next_state = ENTER_PRINTER
    