#---------------------------------------------------------------
#Core task
CORE_TASK_NAME = "taskCore"

#States - Core
REQUEST_CONFIG     = "requestConfig"
REQUEST_BREAKS     = "requestBreaks"
GET_WELCOME        = "getWelcome"
REQUEST_SIGNON     = "requestSignOn"
REQUEST_VEHICLES   = "vehicles"
REQUEST_FUNCTIONS  = "requestFunctions"
EXECUTE_FUNCTIONS  = "executeFunctions"

#---------------------------------------------------------------
#Print Task
PRINT_TASK_NAME     = 'print' 

#Print - States
ENTER_PRINTER       = 'enterPrinter'
CONFIRM_PRINTER     = 'confirmPrinter'

#---------------------------------------------------------------
#Take a break task
TAKE_A_BREAK_TASK_NAME   = 'takeABreak'
 
#States - Take a break
SELECT_BREAK             = "selectBreak"
TRANSMIT_START_BREAK     = "startBreak"
ENTER_PASSWORD           = "enterPassword"
TRANSMIT_STOP_BREAK      = "stopBreak"

#---------------------------------------------------------------
#Vehicle Task
VEHICLE_TASK_TASK_NAME   = 'taskVehicle'

#States - Vehicle Types
REQUEST_VEHTYPE      = "requestVehType"
REQUEST_VEHICLEID    = "requestVehicle"
REQUEST_XMIT_VEHID   = "xmitVehicleId"
NEXT_SAFETY_CHECK    = "nextSafetyCheck"
EXECUTE_SAFETYCHECK  = "executeSafetyCheck"
COMPLETE_SAFETYCHECK = "completeSafetyCheck"


