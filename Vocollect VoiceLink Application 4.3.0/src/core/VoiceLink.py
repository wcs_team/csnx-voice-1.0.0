from vocollect_core.task.task_runner import TaskRunnerBase
from vocollect_core.utilities.pickler import register_pickle_obj
from vocollect_core import obj_factory, class_factory

from core.CoreTask import CoreTask 

#===============================================================
# Main Task Runner Class
#===============================================================
class VoiceLink(class_factory.get(TaskRunnerBase)):
    ''' Main Voicelink application ''' 
    password = None
    
    def __init__(self):
        super().__init__()
        
    #----------------------------------------------------------
    def startUp(self):
        ''' Main startup method launching core task '''
        self.launch(obj_factory.get(CoreTask, self), None)
        
    #----------------------------------------------------------
    def initialize(self):
        ''' Application initialization '''
        self.app_name = "VoiceLink"
        
#===============================================================
# Startup method
#===============================================================
def voicelink_startup():
    ''' Main method for starting VoiceLink application '''
    start = VoiceLink()
    start = register_pickle_obj("VoiceLink", start)

    start.execute()
        