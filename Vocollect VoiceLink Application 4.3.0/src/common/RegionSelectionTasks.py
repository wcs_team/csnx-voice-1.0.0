''' Region selection tasks for selecting a region
Task Classes:
    SingleRegionTask - Selects single region using 2 LUTs
    MutlipleRegionTask - selects single or multiple region using 3 LUTs
'''
from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_list_lut_auth, prompt_yes_no, prompt_only
from vocollect_core import itext, class_factory
from Globals import change_function

#States - common region
GET_VALID_REGIONS="getValidRegions"
REQUEST_REGIONS="requestRegions"
GET_REGION_CONFIG="getRegionConfigs"


###############################################################
# Regions Selection 2 LUTs
###############################################################
class SingleRegionTask(class_factory.get(TaskBase)):
    ''' Task to allow user to select single region using 2 LUTs. 
    The 2 LUTs must be defined in the calling task
         _valid_regions_lut - returns the list of valid regions, 
                              must have number and description fields
         _region_config_lut - used to retrieve region configurations 
    '''
    
    #----------------------------------------------------------
    #override constructor 
    def __init__(self, taskRunner = None, callingTask = None):
        super().__init__(taskRunner, callingTask)
        self.selected_region = None
        
        #set name
        self.name = "taskSingleRegion" 

        #get LUTS
        self._valid_regions_lut = callingTask._valid_regions_lut
        self._region_config_lut = callingTask._region_config_lut
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States '''
        #get region states
        self.addState(GET_VALID_REGIONS, self.get_valid_regions)
        self.addState(REQUEST_REGIONS,   self.request_regions)
        self.addState(GET_REGION_CONFIG, self.get_region_config)
        
    #----------------------------------------------------------
    def get_valid_regions(self):
        ''' Get a list of valid regions for operator '''

        #Loop until valid region received or user signs off/changes function
        if self._request_valid_regions() == 0:
            if self.next_state is None: #may have set by overridden request
                #check for valid regions
                for region in self._valid_regions_lut:
                    if region['number'] >= 0:
                        return
                
                #not authorized for any regions end task
                self.next_state = '' 
        else:
            self.next_state = GET_VALID_REGIONS
            
    #----------------------------------------------------------
    def request_regions(self):
        ''' prompt operator for a region '''
        self.selected_region = self._prompt_for_region()
        
        if self.selected_region == 'change function':
            self.next_state = self.current_state
            change_function()
        
    #----------------------------------------------------------
    def get_region_config(self):
        ''' get region configuration for selected region '''
        if self.next_state is None: #may have set by overridden request
            result = self._request_region_configs()
            if result < 0:
                self.next_state = GET_REGION_CONFIG
            elif result > 0:
                self.next_state = GET_VALID_REGIONS


    #==========================================================
    #Protected methods for override purposes
    #==========================================================
    def _prompt_for_region(self, allow_only_one = True):
        ''' gets a region from list of regions 
        
        returns: region number selected, if only 1 regions available then 
        that region is returned without prompting
        '''
        if len(self._valid_regions_lut) == 1:
            return self._valid_regions_lut[0]['number']
        else:
            additional = {'change function' : False}
            if not allow_only_one:
                additional.update({'no more' : False, 'all' : True})
            
            return prompt_list_lut_auth(self._valid_regions_lut, 'number', 'description',
                                        itext('generic.region.prompt'),
                                        itext('generic.region.help'), 
                                        additional)
            
    def _request_valid_regions(self):
        ''' method to allow overriding request of valid regions '''
        return self._valid_regions_lut.do_transmit()
    
    def _request_region_configs(self):
        ''' method to allow overriding request region configs '''
        return self._region_config_lut.do_transmit(self.selected_region)
    

###############################################################
# Regions Selection 3 LUTs (Multiple regions possible)
###############################################################
class MultipleRegionTask(class_factory.get(SingleRegionTask)):
    ''' Task to allow user to select multiple regions using 3 LUTs. 
    The 3 LUTs must be defined in the calling task
         _valid_regions_lut - returns the list of valid regions, 
         must have number and description fields
         _request_region_lut - used to validate users selection
         _region_config_lut - used to retrieve region configurations 
    '''
    #----------------------------------------------------------
    #override constructor for allow only one parameter.
    def __init__(self, taskRunner = None, callingTask = None, allow_only_one = False):
        super().__init__(taskRunner, callingTask)
        self.allow_only_one = allow_only_one
        
        #set name
        self.name = "taskMultiRegions" 
        
        #Get Luts
        self._request_reqion_lut = self.callingTask._request_reqion_lut

    #----------------------------------------------------------
    def request_regions(self):
        ''' select regions and verify with host until 
        operator selects all regions or says no more '''
        completed = False
        regions = []
        while not completed:
            all = 0
            region = self._prompt_for_region(self.allow_only_one)

            #check for all or no more response
            if region == 'all':
                region = ''
                all = 1
            elif region == 'no more':
                region = ''
                if len(regions) == 0:
                    prompt_only(itext('generic.nomore.region.prompt'))
                else:
                    completed = prompt_yes_no(itext('generic.nomore.region.confirm'))
            elif region == 'change function':
                self.next_state = self.current_state
                change_function()
                return
            
            #either requesting all or new unrequested region
            if (region == '' and all == 1) or (region != '' and region not in regions):
                result = -1
                if (self.allow_only_one):
                    result = self._request_reqion_lut.do_transmit(region)
                else:
                    result = self._request_reqion_lut.do_transmit(region, all)
                
                if result == 0:
                    if region == '':
                        completed = True
                    else:
                        regions.append(region)
                        if len(regions) == len(self._valid_regions_lut):
                            completed = True
                        elif self.allow_only_one:
                            completed = True
                            
                    #if not all regions selected then prompt operator for if they want another region
                    if not completed:
                        completed = not prompt_yes_no(itext('generic.another.region.prompt'))
    
    def _request_region_configs(self):
        ''' method to allow overriding request region configs '''
        return self._region_config_lut.do_transmit()
