import time
import configparser
import dialogs
import voice

from voice import globalwords as gw
from voice import get_voice_application_property, getenv, open_vad_resource, \
    log_message

#import from core library
from vocollect_core import itext
from vocollect_core.task.task_runner import Launch
from vocollect_core.dialog.functions import prompt_ready, prompt_words

#import from LUT ODR library
from vocollect_lut_odr.connections import LutConnection, OdrConnection 
from vocollect_lut_odr.receivers import Lut, StringField, NumericField, LutField, \
    OdrConfirmationByte, NoConfirmationError, WrongConfirmationError
from vocollect_lut_odr.transports import TransientSocketTransport

from Globals import sign_off
from pending_odrs import wait_for_pending_odrs
from pending_luts import wait_for_lut_response
from vocollect_core.dialog.base_dialog import BaseDialog
from vocollect_core.utilities import obj_factory

transport = TransientSocketTransport(str(get_voice_application_property('LUTHost')), 
                                     int(get_voice_application_property('LUTPort')),
                                     20)

lut_def_files = ["LUTDefinition.properties"]
lut_defs = None

##################################################################
#Custom Voicelink Record Formatter
##################################################################

def get_lut_def(command):
    """ Reads in the LUT Definitions """
    global lut_defs
    
    if lut_defs is None:
        lut_defs = {}
        cp = configparser.RawConfigParser()

        for file in lut_def_files:
            cp.readfp(open_vad_resource(file))
            for section in cp.sections():
                lut_defs[section] = cp.get(section, 'fields').split('\n')
    
    return lut_defs[command]

class VLRecordFormatter(object):
    """A stream-based formatter that separates fields and records, and 
    terminates record sets.
    
    Constructor Parameters:
           command_name - name of command to be sent
    """
    
    #static variables for simulated milliseconds
    current_time_str = ''
    milliseconds = 0
    
    def __init__(self, command_name):
        self._command_name = command_name
        self._record_separator = '\r\n'
        self._record_set_terminator = '\n'

    def format_record(self, fields):
        """Format the record's fields, and terminate with a record separator.

        Parameters:
                fields - fields that make up the record 
        """
        
        #build time string with simulated milliseconds
        #global milliseconds, current_time_str
        now = time.strftime("%m-%d-%y %H:%M:%S")
        if now == VLRecordFormatter.current_time_str:
            VLRecordFormatter.milliseconds += 10
        else:
            VLRecordFormatter.current_time_str = now
            VLRecordFormatter.milliseconds = 0
        now = now + '.%03d' % VLRecordFormatter.milliseconds
        
        request = self._command_name + "('"
        request = request + now + "',"
        request = request + "'" + getenv('Device.Id', '') + "',"
        request = request + "'" + getenv('Operator.Id', '') + "'"
        
        for field in fields:
            request = request + ",'" + str(field) + "'" 
        request = request + ")"

        return request + self._record_separator
        
 
    def terminate_recordset(self):
        """Return the character sequence that marks the end of a recordset."""
        return self._record_set_terminator

class BooleanField(LutField):
    """ Boolean Lut field, converts string value to boolean """
        
    def convert(self, value):
        """convert value to a boolean"""
        temp = False
        if type(value) == bool:
            temp = value
        elif type(value) == str:
            temp = (value == '1')
        elif type(value) == int:
            temp = (value == 1)

        return temp;

class VoiceLinkLut(object):
    ''' A VoiceLink standard LUT class 
    Constructor Parameters:
        command - name of the LUT command to be built
    '''
    def __init__(self, command):
        self.command_name = command
        #Call base class's init
        self.lut_data = Lut(*self._get_fields(command))
        self.formatter = VLRecordFormatter(command)
        self.fields = self.lut_data.fields
        self.connection = LutConnection(transport, 
                                        self.formatter, 
                                        self.lut_data)

        
    def do_transmit(self, *fields):
        ''' transmit method for LUT
            Parameters:
            *fields - variable number of fields to transmit
            
            return: Returns 0 if no error occurred, otherwise the error code
        '''
        
        #first check if any pending ODRs        
        wait_for_pending_odrs('VoiceLink')
        result = self._transmit(*fields)
    
        if result < 0:
            # turn off globals
            sign_off_current = gw.words['sign off'].enabled
            take_a_break_current = gw.words['take a break'].enabled
            gw.words['sign off'].enabled = False
            gw.words['take a break'].enabled = False
            message = itext('generic.errorContactingHost.prompt')

            #Disable dynamic vocabulary
            try:
                #Disable Dynamic Vocab for this prompt only
                BaseDialog.exclude_dynamic_vocab = True

                response = dialogs.prompt_ready(itext('generic.sayReadyTryAgain.prompt', message), 
                                                additional_vocab = {'sign off' : True})
                if response == 'sign off':
                    sign_off()
                    
            finally:
                # reset globals
                gw.words['take a break'].enabled = take_a_break_current
                gw.words['sign off'].enabled = sign_off_current        

        return result
    
    def _transmit(self, *fields):
        ''' transmit method for LUT
        Parameters:
            *fields - variable number of fields to transmit
            
        return: Returns 0 if no error occurred, otherwise the error code
        '''
        error = -1

        #build a list of fields
        field_list = []
        for field in fields:
            if field is None:
                field = ''
            field_list.append(str(field))

        try:
            #Transmit and wait for response 
            self.connection.append(field_list)
            wait_for_lut_response(self.connection)
            
            for record in self:
                record.parent = self
            
            #get error code and message
            error = self._process_error(self[0]['errorCode'], 
                                        self[0]['errorMessage'])
           
        except Launch as err:
            raise err
        except Exception as err:
            log_message('VoiceLink Lut Error: LUT = %s, Message = %s' % (self.command_name, str(err)))
            pass

        return error
    
    def _process_error(self, error, message):
        ''' Protected method to process error codes. 
        Override if any special errors 
        
        Parameters:
                error - error code received
                message - message received
        returns: error code modified if handled
        '''
        # just return error if error is 0
        if error == 0:
            return error
        
        #save current state of globals
        sign_off_current = gw.words['sign off'].enabled
        take_a_break_current = gw.words['take a break'].enabled

        try:
            #Disable globals
            gw.words['sign off'].enabled = False
            gw.words['take a break'].enabled = False
            
            return_error = error
            if error == 99:
                #Disable Dynamic Vocab for this prompt only
                BaseDialog.exclude_dynamic_vocab = True
                
                prompt_ready(itext('generic.sayReady.prompt', message))
                return_error = 0
                
            elif error == 98:
                #Disable Dynamic Vocab for this prompt only
                BaseDialog.exclude_dynamic_vocab = True
                
                # Need to turn off the dynamic dialog; this is really the only place we need to do this right now
                # ...If we come up with a general need for this functionality, we should think about moving
                # ...this into the prompt as a input. --jgeisler
                spoken = prompt_words(itext('generic.error.say.sign.off.prompt', message), 
                                              False, {'sign off' : False})
                if spoken == 'sign off':
                    sign_off();
                    
            elif error > 0:
                #Disable Dynamic Vocab for this prompt only
                BaseDialog.exclude_dynamic_vocab = True

                spoken = prompt_ready(itext('generic.continue.prompt', message), 
                                              True, {'sign off' : True})
                if spoken == 'sign off':
                    sign_off();
        finally:
            #always re-enable globals before leaving function.
            gw.words['sign off'].enabled = sign_off_current
            gw.words['take a break'].enabled = take_a_break_current
            
        return return_error
    
    def _get_fields(self, command):
        ''' Method to build a list of fields from properties file 
        Parameters: 
                command - command name to get fields for
        '''
        records = get_lut_def(command)
        fields = []
        for record in records:
            if (record != ""):
                tmp = record.split(",")
                if tmp[1] == 'N':
                    fields.append(NumericField(tmp[0]))
                elif tmp[1] == 'B':
                    fields.append(BooleanField(tmp[0]))
                else:
                    fields.append(StringField(tmp[0]))
                    
        fields.append(NumericField('errorCode'))
        fields.append(StringField('errorMessage'))

        return fields    

    def receive(self, data):
        return self.lut_data.receive(data)
        
    def __len__(self):
        return self.lut_data.__len__()
        
    def __getitem__(self, record):
        return self.lut_data.__getitem__(record)

    def __delitem__(self, record):
        self.lut_data.__delitem__(record)
        
    def __iter__(self):
        return self.lut_data.__iter__()
    
    def __str__(self):
        return self.lut_data.__str__()


class VLOdrConfirmationByte(OdrConfirmationByte):
    ''' Custom VoiceLink Confirmation byte. allows any number of bytes to be
    received, but only compares the first byte
    '''
    def receive(self, data):
        """Handle returned data from an ODR connection and raise an exception
        if it doesn't match expectations.
        """
        if type(data) == int:
            data = chr(data)
        elif type(data) != str:
            #check if data was utf-8 encoded
            try:
                data = data.decode("utf-8")
            except:
                temp = ''
                for byte in data:
                    temp += chr(byte)
                data = temp

        voice.log_message('Data Received: %s' % (data))

        # We always expect one byte
        if not data:
            raise NoConfirmationError
        # If a byte was specified, ensure that it matches
        if self.confirmation_byte is not None and self.confirmation_byte != data[0]:
            raise WrongConfirmationError

        return True


class VoiceLinkOdr(object):
    ''' A VoiceLink standard ODR class 
        Constructor Parameters:
        command - name of the ODR command to be built
    '''
    def __init__(self, command):
        self.command_name = command
        
        # Creates ODR connection
        transport = TransientSocketTransport(str(get_voice_application_property('ODRHost')),
                                             int(get_voice_application_property('ODRPort')),
                                             20)
        self.odr_conn = OdrConnection('VoiceLink', 
                                      transport, 
                                      VLRecordFormatter(command),
                                      VLOdrConfirmationByte())

    def do_transmit(self, *fields): 
        ''' Creates the records and then appends 
            
            Parameters: variable number of fields to transmit
        '''    
        #build a list of fields
        field_list = []
        for field in fields:
            if field is None:
                field = ''
            field_list.append(str(field))

        # Appends the list of params to the ODR connection
        self.odr_conn.append(field_list)
        
    def has_pending_odr(self):
        ''' Returns the number of pending ODRs '''
        status = self.odr_conn.status()[1]
        return status

    
class VoiceLinkLutOdr(object):
    ''' A VoiceLink LUT and ODR class
        Constructor Parameters:
        lut_command - name of the LUT command to be built
        odr_command - name of the ODR command to be built
        flag - the value for useLuts
    '''
    def __init__(self, lut_command, odr_command, flag):
        
        # Create LUT And ODR connection
        self._odr_conn = obj_factory.get(VoiceLinkOdr, odr_command)
        self._lut_conn = obj_factory.get(VoiceLinkLut, lut_command)
        #Sets Flag to use for when sending ODRs
        self.flag = flag
        
    def do_transmit(self, *fields):
        ''' Decides whether a LUT or ODR should be sent
            depending on the flag
            
            Constructor Parameters:
            *fields - variable number of fields to transmit

            Returns:  0 if ODR is sent or the result of the LUT transmit
        '''
        result = 0
        
        if self.flag == 0:
            result = 0
            # Transmit ODR
            self._odr_conn.do_transmit(*fields)
            
        # Transmit LUT
        elif self.flag == 2:
            result = self._lut_conn.do_transmit(*fields)
        else:
            result = -1
            
            # Find if there are any pending ODRs
            # If no pending ODR then transmit LUT
            if self._odr_conn.has_pending_odr() == 0:
                result = self._lut_conn._transmit(*fields)
                
            # Error either occurred sending LUT or there were pending ODRs   
            if result < 0:
                result = 0
                # Transmit ODR
                self._odr_conn.do_transmit(*fields)
                
        return result
    
    def receive(self, data):
        return self._lut_conn.receive(data)
        
    def __len__(self):
        return self._lut_conn.__len__()
        
    def __getitem__(self, record):
        return self._lut_conn.__getitem__(record)

    def __delitem__(self, record):
        self._lut_conn.__delitem__(record)
        
    def __iter__(self):
        return self._lut_conn.__iter__()
    
    def __str__(self):
        return self._lut_conn.__str__()

