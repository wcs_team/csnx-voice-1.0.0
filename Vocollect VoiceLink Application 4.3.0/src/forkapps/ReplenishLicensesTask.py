from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_digits, prompt_only, prompt_yes_no, prompt_ready
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core import itext, obj_factory, class_factory

from forkapps.VerifyLocationTask import VerifyLocationTask, Location
from forkapps.AdditionalTasks import CancelLicense
from forkapps.ForkAppsVocabulary import ForkAppsVocabulary
from Globals import change_function

from forkapps.SharedConstants import FA_REPLENISH_LICENSE_TASK_NAME,\
    FUNCTION_REPLENISHMENT, PROMPT_PICKUP_LOCATION, PROMPT_PUT_LOCATION,\
    RP_NEXT_STEP, RP_GET_REPLENISHMENT_LICENSE, RP_VERIFY_RESERVE_LOCATION,\
    RP_VERIFY_PICKUP_QUANTITY, RP_VERIFY_LOCATION, RP_VERIFY_PUT_QUANTITY,\
    RP_REPLENISH_LICENSE, RP_COMPLETE_LICENSE

import time
from core.SharedConstants import CORE_TASK_NAME, REQUEST_FUNCTIONS

RP_CHECK_INTERLEAVE = 'ReplenishmentCheckInterleave'

class ReplenishLicensesTask(class_factory.get(TaskBase)):
    ''' Replenish license task for the voicelink system
    
    Steps:
        1. Get Replenishment License
        2. Verify Reserve Location
        3. Verify pickup Quantity
        4. Verify Location
        5. verify put quantity
        6. Replenish License
        7. Complete License
        8. Determine Next step
    '''
    

    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 taskRunner = None, 
                 callingTask = None):
        super(ReplenishLicensesTask, self).__init__(taskRunner, callingTask)
        
        #Set task name
        self.name = FA_REPLENISH_LICENSE_TASK_NAME
      
        self._region_config_lut = region_config_lut
        
        #LUTS
        self._get_replenishment_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkGetReplenishment')
        self._replenish_license_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkReplenishmentLicense')
        self._get_alternate_put_location = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkGetAlternatePutLocation')
        
        #variables
        self._start_time = None
        self._region = None
        
        self._loc_info = obj_factory.get(Location,
                                           FUNCTION_REPLENISHMENT, 
                                           PROMPT_PICKUP_LOCATION)
        self._dest_loc_info = obj_factory.get(Location,
                                                FUNCTION_REPLENISHMENT, 
                                                PROMPT_PUT_LOCATION)
        self._current_license = None
        self._pick_up_qty = None
        self._put_qty = None
        self._partial = False
        
        #define and setup additional vocabulary object
        self.dynamic_vocab = obj_factory.get(ForkAppsVocabulary,
                                               FUNCTION_REPLENISHMENT, 
                                               self._replenish_license_lut, 
                                               self.taskRunner)
        self.dynamic_vocab.intialize_cancel(self.name, RP_NEXT_STEP)
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(RP_GET_REPLENISHMENT_LICENSE,    self.get_replenishment_license)
        self.addState(RP_VERIFY_RESERVE_LOCATION,      self.verify_reserve_location)
        self.addState(RP_VERIFY_PICKUP_QUANTITY,       self.verify_pickup_quantity)
        self.addState(RP_VERIFY_LOCATION,              self.verify_location)
        self.addState(RP_VERIFY_PUT_QUANTITY,          self.verify_put_quantity)
        self.addState(RP_REPLENISH_LICENSE,            self.replenish_license)
        self.addState(RP_COMPLETE_LICENSE,             self.complete)
        self.addState(RP_NEXT_STEP,                    self.next_step)
        self.addState(RP_CHECK_INTERLEAVE,             self.interleave, RP_NEXT_STEP)
        
        
    def get_replenishment_license(self):
        ''' get a replenishment from host '''
        #get replenishment
        self.set_sign_off_allowed(False)

        result = -1
        while result < 0:
            result = self._get_replenishment_lut.do_transmit()
      
        if result == 0:
            self._start_time = time.strftime("%m-%d-%y %H:%M:%S")
            #get the region we are dealing with from the license lut
            self._region = self._get_region()
            self._current_license = self._get_replenishment_lut[0]
            
            #Set additional Vocabulary
            self.dynamic_vocab.item_number = self._current_license['itemNumber']
            self.dynamic_vocab.description = self._current_license['itemDescription']
            self.dynamic_vocab.license = self._current_license['number']
            self.dynamic_vocab.license_plate = self._current_license['licensePlate']
            self.dynamic_vocab.location = self._loc_info
            self.dynamic_vocab.quantity = self._current_license['quantity']
            self.dynamic_vocab.goal_time = self._current_license['goalTime']
            self.dynamic_vocab.partial_allowed = self._region['allow_partial_put']
            self.dynamic_vocab.cancel_allowed = self._region['allow_cancel_license']
            self.dynamic_vocab.exception_location = self._region['exception_location']
            self.dynamic_vocab.start_time = self._start_time
            
            #set reserve location information
            self._loc_info.set_replenishment_region_settings(self._region)
            self._loc_info.set_reserve_from_location_replenish_license(self._current_license, 
                                                                       self._region)
            self._loc_info.quantity = self._current_license['quantity']
     
            #set destination location information
            self._dest_loc_info.set_replenishment_region_settings(self._region)
            self._dest_loc_info.set_destination_from_replenish_license(self._current_license)
            self._dest_loc_info.quantity =self._current_license['quantity']
            
        else:
            self.next_state = RP_GET_REPLENISHMENT_LICENSE
    
    def verify_reserve_location(self):
        ''' verify reserve location '''
        #verify reserve location
        self.launch(obj_factory.get(VerifyLocationTask, 
                                      self._loc_info, self.taskRunner, self))
        
    
    def verify_pickup_quantity(self):
        ''' verify pick up quantity '''
        if self._region['capture_pickup_quantity']:
            qty = prompt_digits(itext('forkapps.replen.pickup.qty'), 
                                itext('forkapps.replen.pickup.qty.help'), 
                                1, 9, 
                                self._current_license['quantity'] <= 0)
    
            #Quantity cannot be 0
            qty = int(qty)
            if qty == 0:
                prompt_only(itext('forkapps.replen.enter.value.greater.zero'))
                self.next_state = RP_VERIFY_PICKUP_QUANTITY
            
            #Expected quantity is not 0 and not equal to spoken quantity 
            elif self._current_license['quantity'] not in [0, qty]: 
                if prompt_yes_no(itext('forkapps.replen.verify.location.partial.confirm',
                                            qty, self._current_license['quantity'])):
                    if self._region['allow_override_quantity']:
                        self._pick_up_qty = qty
                    else:
                        prompt_only(itext('forkapps.replen.pickup.qty.cancel'))
                        self.launch(obj_factory.get(CancelLicense,
                                                      FUNCTION_REPLENISHMENT, 
                                                      self._loc_info.license_id, 
                                                      self._loc_info.license_plate,
                                                      self._loc_info.quantity, 
                                                      self._start_time, 
                                                      self._region['exception_location'], 
                                                      self._replenish_license_lut, 
                                                      self.name, RP_NEXT_STEP,
                                                      self.taskRunner, self), RP_NEXT_STEP)
                else:
                    self.next_state = RP_VERIFY_PICKUP_QUANTITY
            else:
                self._pick_up_qty = qty
        else:
            self._pick_up_qty = self._current_license['quantity']
        
        self.dynamic_vocab.quantity = self._pick_up_qty
        self._put_qty = self._pick_up_qty
            
    
    def verify_location(self):
        ''' verify location '''
        #verify destination location        
        self.dynamic_vocab.location = self._dest_loc_info
        
        self.launch(obj_factory.get(VerifyLocationTask,
                                      self._dest_loc_info, 
                                      self.taskRunner, self))

    
    def verify_put_quantity(self):
        ''' verify put quantity '''
        if self._region['capture_replenished_quantity'] or self._dest_loc_info.partial_spoken:
            qty = prompt_digits(itext('forkapps.replen.put.qty'), 
                                itext('forkapps.replen.put.qty.help'), 
                                1, 9, 
                                self._current_license['quantity'] <= 0)
        
            #Quantity cannot be 0
            qty = int(qty)
            if qty == 0:
                prompt_only(itext('forkapps.replen.put.qty.zero'))
                self.next_state = RP_VERIFY_PUT_QUANTITY
            
            #Value greater not allowed
            elif self._pick_up_qty > 0 and qty > self._pick_up_qty:
                prompt_only(itext('forkapps.replen.put.qty.invalid', 
                                       qty, self._pick_up_qty))
                self.next_state = RP_VERIFY_PUT_QUANTITY

            #value less, check if allowed partial, and if intended partial
            elif self._pick_up_qty > 0 and qty < self._pick_up_qty:
                if self._region['allow_partial_put']:
                    if self._dest_loc_info.partial_spoken:
                        prompt = itext('forkapps.replen.put.qty.confirm', qty)
                    else:
                        prompt = itext('forkapps.replen.put.qty.partial', 
                                            qty, self._pick_up_qty)
                    if prompt_yes_no(prompt, True):
                        self._put_qty = qty
                    else:
                        self.next_state = RP_VERIFY_PUT_QUANTITY
                else:
                    prompt_only(itext('forkapps.replen.put.qty.invalid', 
                                           qty, self._pick_up_qty))
                    self.next_state = RP_VERIFY_PUT_QUANTITY
                    

            #set quantity
            else:
                self._put_qty = qty
        else:
            self._put_qty = self._pick_up_qty    
            
    #----------------------------------------------------------
    def replenish_license(self):
        ''' replenish license '''
        if self._dest_loc_info.partial_spoken and self._pick_up_qty <= 0:
            self._partial = True
        elif self._pick_up_qty > self._put_qty and self._pick_up_qty > 0:
            self._partial = True
        else:
            self._partial = False
             
        result = self._replenish_license_lut.do_transmit(self._current_license['number'],
                                           self._put_qty,
                                           self._dest_loc_info.curr_location_id,
                                           '0' if self._partial else '1',
                                           '',
                                           self._start_time)
        if result != 0:
            self.next_state = RP_REPLENISH_LICENSE
    
    #----------------------------------------------------------
    def complete(self):
        ''' replenishment complete '''
        self.set_sign_off_allowed(True)
        if not self._partial:
            result = prompt_ready(itext('forkapps.complete'), False,
                            {'change function' : False,
                             'change region' : False}) 

            if result == 'change function':
                change_function()
                self.next_state = self.current_state
            elif result == 'change region':
                self.callingTask.change_region()
                self.next_state = self.current_state
    
    #----------------------------------------------------------
    def next_step(self):
        ''' check if there are more licenses to put away '''
        if self._put_qty is None:
            self._pick_up_qty = 0
        else:
            self._pick_up_qty -= self._put_qty
            
        self._put_qty = self._pick_up_qty
        
        #more quantity to put for this license, so get alternate location
        if self._pick_up_qty > 0 or self._partial:
            self._partial = False
            self._dest_loc_info.get_alt_location(self._pick_up_qty)
            self._start_time = time.strftime("%m-%d-%y %H:%M:%S")
            self.dynamic_vocab.start_time = self._start_time
            self.dynamic_vocab.quantity = self._pick_up_qty    
            self.next_state = RP_VERIFY_LOCATION

    #----------------------------------------------------------
    def interleave(self):
        if self.taskRunner.findTask(CORE_TASK_NAME)._signon_lut[0]['interleave']:
            self.return_to(CORE_TASK_NAME, REQUEST_FUNCTIONS) 
        else:
            self.next_state = ''                               
    
    def _get_region(self):
        ''' get matching region record for replenishment '''
        r = self._region_config_lut[0]
        for region in self._region_config_lut:
            if region['number'] == self._get_replenishment_lut[0]['region']:
                r = region
        
        return r
    
    def set_sign_off_allowed(self, allowed = False):
        task = self.taskRunner.findTask(CORE_TASK_NAME)
        if task is not None:
            task.sign_off_allowed = allowed
    