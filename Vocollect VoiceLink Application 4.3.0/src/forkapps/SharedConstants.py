#---------------------------------------------------------------
#Put away
FA_PUTAWAY_TASK_NAME = "putAway"

#States - Putaway
PA_REGIONS                 = "Regions"
PA_VALIDATE_REGION         = "validateRegion"
PA_START_LOCATION          = "startLocation"
PA_SPECIFY_LICENSES        = "specifyLicense"
PA_GET_LICENSES            = "getLicenses"
PA_PUT_LICENSES            = "putLicenses"

#---------------------------------------------------------------
#Put away License
FA_PUTAWAY_LICENSE_TASK_NAME = "putLicenseAway"

#States - Putaway Licenses
PA_INITIALIZE_LICENSE      = "initializeLicense"
PA_VERIFY_PICKUP_QTY       = "verifyPickUpQuanity"
PA_VERIFY_START_LOCATION   = "verifyStartLocation"
PA_VERIFY_LOCATION         = "verifyLocation"
PA_VERIFY_PUT_QTY          = "verifyPutQty"
PA_TRANSMIT_PUT            = "transmitPut"
PA_COMPLETE_PROMPT         = "completePrompt"
PA_NEXT_STEP               = "nextStep"


#---------------------------------------------------------------
#Replenishment
FA_REPLENISHMENT_TASK_NAME = "replenishment"

#States - Replenishment
RP_REGIONS             = "replenRegions"
RP_VALIDATE_REGION     = "validateRegion"
RP_LICENSE             = "repenishLicense" 


#---------------------------------------------------------------
#Replenish License
FA_REPLENISH_LICENSE_TASK_NAME = "replenishLicense"

#States - Replenish Licenses
RP_GET_REPLENISHMENT_LICENSE = "getReplenishmentLicense"
RP_VERIFY_RESERVE_LOCATION   = "verifyReserveLocation"
RP_VERIFY_PICKUP_QUANTITY    = "verifyPickupQuantity"
RP_VERIFY_LOCATION           = "verifyLocation"
RP_VERIFY_PUT_QUANTITY       = "verifyPutQuantity"
RP_REPLENISH_LICENSE         = "replenishLicense"
RP_COMPLETE_LICENSE          = "completeLicense" 
RP_NEXT_STEP                 = "nextStep"

#---------------------------------------------------------------
#Verify Location
FA_VERIFY_LOCATION_TASK_NAME = "putAwayVerifyLocation"

#States - verify location
DIRECT_PREAISLE         = "preAisle"
DIRECT_AISLE            = "aisle"
DIRECT_POSTAISLE        = "postAisle"
GET_LOCATION            = "getLocation"
CONFIRM_LOCATION        = "confirmLocation"
TRANSMIT_LOCATION       = "transmitLocation"
 
#Prompt Types
PROMPT_PICKUP_LOCATION = 1
PROMPT_START_LOCATION = 2
PROMPT_PUT_LOCATION = 3

#Functions
FUNCTION_PUTAWAY = 1
FUNCTION_REPLENISHMENT = 2


#---------------------------------------------------------------
#Additional Tasks
FA_CANCEL_LICENSE_TASK_NAME = "cancelLicense"
FA_RELEASE_LICENSE_TASK_NAME = "releaseLicense"

#States - Cancel License
TRANSMIT_REASON_CODES = 'transmitReasonCodes'
SELECT_REASON         = 'selectReason'
DELIVER_LICENSE       = 'deliverLicense'
TRANSMIT_PUT_LICENSE  = 'transmitPutLicense'
COMMAND_COMPLETE      = 'cancelComplete'



