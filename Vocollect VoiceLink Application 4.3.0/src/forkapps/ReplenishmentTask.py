from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_only, prompt_ready, prompt_yes_no
from common.VoiceLinkLut import VoiceLinkLut
from vocollect_core import itext, obj_factory, class_factory

from common.RegionSelectionTasks import MultipleRegionTask
from forkapps.ReplenishLicensesTask import ReplenishLicensesTask
from Globals import change_function
from forkapps.SharedConstants import FA_REPLENISHMENT_TASK_NAME, RP_REGIONS,\
    RP_VALIDATE_REGION, RP_LICENSE
     
#####################################################
# Replenishment Main Task                           #
#####################################################
class ReplenishmentTask(class_factory.get(TaskBase)):
    ''' Replenishment task for the voicelink system
    
    Steps:
            1. Get region
            2. Validate regions
            3. Replenish license
    '''

    #----------------------------------------------------------
    def __init__(self,
                 taskRunner = None, 
                 callingTask = None):
        super(ReplenishmentTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = FA_REPLENISHMENT_TASK_NAME

        self._region_selected = False

        #define LUTs
        self._valid_regions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkValidReplenishmentRegions')
        self._request_reqion_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkRequestReplenishmentRegion')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkReplenishmentRegionConfiguration')
    

    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(RP_REGIONS, self.regions)
        self.addState(RP_VALIDATE_REGION,    self.validate_regions)
        self.addState(RP_LICENSE,    self.replenish_license)
        
    #----------------------------------------------------------
    def regions(self):
        ''' Run multi-region selection '''
        self._region_selected = False
        self.launch(obj_factory.get(MultipleRegionTask,
                                      self.taskRunner, self))

    #----------------------------------------------------------
    def validate_regions(self):
        ''' validates region selection '''
        #check for valid regions
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
        
        if valid:
            self._region_selected = True
            result = prompt_ready(itext('forkapps.replen.start'), False,
                            {'change function' : False,
                             'change region' : False}) 
            
            if result == 'change function':
                change_function()
                self.next_state = self.current_state 
            elif result == 'change region':
                self.change_region()
                self.next_state = self.current_state 
        else:
            prompt_only(itext('generic.regionNotAuth.prompt'))
            self.next_state = ''
            
    #----------------------------------------------------------       
    def replenish_license(self):
        ''' launch replenish license task '''
        #launch replenishment task here
        self.launch(obj_factory.get(ReplenishLicensesTask,
                                      self._region_config_lut, 
                                      self.taskRunner, 
                                      self),
                    RP_LICENSE)
 
    #======================================================
    #Global/Dynamic Word Functions
    #======================================================
    def change_region(self):
        ''' Called when operator speaks global word "change region" '''
        if self._region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self._region_selected = False
                self.return_to(self.name, RP_REGIONS)
