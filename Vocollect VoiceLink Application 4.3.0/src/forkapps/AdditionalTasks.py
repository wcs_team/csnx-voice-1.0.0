from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_ready
from vocollect_core.dialog.list_prompt import ListPrompt
from vocollect_core import itext, class_factory, obj_factory
from common.VoiceLinkLut import VoiceLinkLut
from forkapps.SharedConstants import FA_CANCEL_LICENSE_TASK_NAME,\
    TRANSMIT_REASON_CODES, SELECT_REASON, DELIVER_LICENSE, TRANSMIT_PUT_LICENSE,\
    COMMAND_COMPLETE, FA_RELEASE_LICENSE_TASK_NAME,\
    FA_REPLENISH_LICENSE_TASK_NAME, FA_PUTAWAY_LICENSE_TASK_NAME

#=======================================================
# Cancel license task
#=======================================================
class AdditonalTaskBase(class_factory.get(TaskBase)):
    ''' base task for additional forapps tasks '''
    
    #----------------------------------------------------------
    def __init__(self, taskRunner = None, callingTask = None):
        TaskBase.__init__(self, taskRunner, callingTask)
        self._returnto_task = None
        
    #----------------------------------------------------------
    def _license_speak(self, license_id):
        digits = 0
        if self._returnto_task == FA_PUTAWAY_LICENSE_TASK_NAME:
            task = self.taskRunner.findTask(self._returnto_task)
            if task._current_region is not None:
                digits = task._current_region['lic_digits_task_speak']
        elif self._returnto_task == FA_REPLENISH_LICENSE_TASK_NAME:
            task = self.taskRunner.findTask(self._returnto_task)
            if task._region is not None:
                digits = task._region['lic_digits_task_speak']
        
        if digits > len(license_id) or digits <= 0:
            digits = len(license_id)
        
        return ' '.join(license_id[len(license_id) - digits:len(license_id)])
        

#=======================================================
# Cancel license task
#=======================================================
class CancelLicense(class_factory.get(AdditonalTaskBase)):
    ''' Put away task for the voicelink system
    
    Steps:
            1. transmit reason codes
            2. select a reason code
            3. prompt for delivery
            4. transmit information
            5. prompt complete
    '''

    #----------------------------------------------------------
    def __init__(self,
                 function_number,
                 license_id,
                 license_plate,
                 quantity,
                 start_time,
                 deliver_location,
                 put_lut,
                 returnto_task,
                 returnto_state,
                 taskRunner = None, 
                 callingTask = None):
        super(CancelLicense, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = FA_CANCEL_LICENSE_TASK_NAME

        self._function_number = function_number
        self._deliver_location = deliver_location
        self._license_id = license_id
        self._license_plate = license_plate
        self._quantity = quantity
        self._start_time = start_time
        self._returnto_task = returnto_task
        self._returnto_state = returnto_state
        
        self._reason_codes_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreGetReasonCodes')
        self._put_lut = put_lut
        
        self._reason = None
    
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(TRANSMIT_REASON_CODES,    self.transmit_reason_codes)
        self.addState(SELECT_REASON,            self.select_reason_code)
        self.addState(DELIVER_LICENSE,          self.deliver)
        self.addState(TRANSMIT_PUT_LICENSE,     self.transmit_put_license)
        self.addState(COMMAND_COMPLETE,          self.complete)
        
    #----------------------------------------------------------
    def transmit_reason_codes(self):
        ''' get list of reason codes '''
        result = self._reason_codes_lut.do_transmit(self._function_number, 1)
        
        if result != 0:
            self.next_state = TRANSMIT_REASON_CODES
    
    #----------------------------------------------------------
    def select_reason_code(self):
        ''' check if reason codes returned, if so prompt operator 
        for cancellation reason '''
         
        count = 0
        for reason in self._reason_codes_lut:
            if reason['code'] != '':
                count += 1
        
        #no reason codes so just return        
        if count <= 0:
            self._reason = None
            return
        else:
            dialog = ListPrompt(itext('forkapps.cancel.reason'), 
                                itext('forkapps.cancel.reason.help'))
            dialog.build_list_from_lut(self._reason_codes_lut, 
                                       'code', 'description')
            dialog.confirm_prompt_key = 'generic.correct.confirm2'
            self._reason = dialog.run()
         
    #----------------------------------------------------------
    def deliver(self):
        ''' if a exception location is defined, then prompt operator
        to deliver to that location '''
        if self._deliver_location not in [None, '']:
            prompt_ready(itext('forkapps.cancel.deliver', self._deliver_location))
    
        
    #----------------------------------------------------------
    def transmit_put_license(self):
        ''' transmit license information for cancellation '''
        result = self._put_lut.do_transmit(self._license_id,
                                           self._quantity,
                                           '',
                                           '2',
                                           self._reason,
                                           self._start_time)
        
        if result != 0:
            self.next_state = TRANSMIT_PUT_LICENSE
        

    #----------------------------------------------------------
    def complete(self):
        ''' prompt cancel was completed and return to defined place '''
        prompt_ready(itext('forkapps.cancel.complete', 
                           self._license_speak(self._license_plate)))
        self.return_to(self._returnto_task, self._returnto_state)
        
#=======================================================
# Release license task
#=======================================================
class ReleaseLicense(class_factory.get(AdditonalTaskBase)):
    ''' Release license task
    
    Steps:
            1. transmit put information
            2. prompt release complete
    '''

    #----------------------------------------------------------
    def __init__(self,
                 license_id,
                 start_time,
                 put_lut,
                 returnto_task,
                 returnto_state,
                 taskRunner = None, 
                 callingTask = None):
        super(ReleaseLicense, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = FA_RELEASE_LICENSE_TASK_NAME

        self._license_id = license_id
        self._start_time = start_time
        self._returnto_task = returnto_task
        self._returnto_state = returnto_state
        
        self._put_lut = put_lut
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(TRANSMIT_PUT_LICENSE,     self.transmit_put_license)
        self.addState(COMMAND_COMPLETE,          self.complete)
        
    #----------------------------------------------------------
    def transmit_put_license(self):
        ''' transmit license information for release '''
        result = self._put_lut.do_transmit(self._license_id,
                                           '0',
                                           '',
                                           '3',
                                           '',
                                           self._start_time)
        
        if result != 0:
            self.next_state = TRANSMIT_PUT_LICENSE
        
    #----------------------------------------------------------
    def complete(self):
        ''' prompt release was completed and return to defined place '''
        prompt_ready(itext('forkapps.release.complete', 
                           self._license_speak(self._license_id)))
        self.return_to(self._returnto_task, self._returnto_state)        