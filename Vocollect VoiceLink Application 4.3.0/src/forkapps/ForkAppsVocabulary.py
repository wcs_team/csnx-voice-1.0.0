from vocollect_core.dialog.functions import prompt_only, prompt_yes_no
from vocollect_core.task.dynamic_vocabulary import DynamicVocabulary, Vocabulary
from vocollect_core import itext, obj_factory , class_factory
from forkapps.AdditionalTasks import CancelLicense, ReleaseLicense
from forkapps.SharedConstants import GET_LOCATION, FA_CANCEL_LICENSE_TASK_NAME, PA_COMPLETE_PROMPT, RP_COMPLETE_LICENSE, RP_REPLENISH_LICENSE, PA_TRANSMIT_PUT

class ForkAppsVocabulary(class_factory.get(DynamicVocabulary)):
    ''' Definition of additional vocabulary used throughout VoiceLink selection
    
    Parameters
            runner - Task runner object
    '''
    
    def __init__(self, function_number, put_lut, runner):
        self.vocabs = {'item number':      obj_factory.get(Vocabulary,'item number', self._item_number, False),
                       'description':      obj_factory.get(Vocabulary,'description', self._description, False),
                       'license':          obj_factory.get(Vocabulary,'license', self._license, False),
                       'location':         obj_factory.get(Vocabulary,'location', self._location, False),
                       'quantity':         obj_factory.get(Vocabulary,'quantity', self._quantity, False),
                       'goal time':        obj_factory.get(Vocabulary,'goal time', self._goal_time, False),
                       'partial':          obj_factory.get(Vocabulary,'partial', self._partial, False),
                       'cancel':           obj_factory.get(Vocabulary,'cancel', self._cancel, False),
                       'release license':  obj_factory.get(Vocabulary,'release license', self._release_license, False)
                       }

        self.runner = runner
        self.function_number = function_number
        self._put_lut = put_lut
        
        self.cancel_return_state = None
        self.cancel_return_task = None
        
        self.release_return_state = None
        self.release_return_task = None
        
        self.clear()
        
    def clear(self):
        ''' clears current information '''
        self.item_number = None
        self.description = None
        self.license = None
        self.license_plate = None
        self.location = None
        self.quantity = None
        self.goal_time = None
        self.partial_allowed = False
        
        #values needed for cancel
        self.cancel_allowed = False
        self.exception_location = None
        self.start_time = None
        
        self.release_allowed = False
        
    def intialize_cancel(self, return_task, return_state = None):
        ''' called if cancel is to be enabled
        
        Parameters: return_task - task name to return to when cancel is 
                                  completed successfully
                    return_state - state in return task to return to 
                    
        '''
        self.cancel_return_task = return_task
        self.cancel_return_state = return_state
        
    def intialize_release(self, return_task, return_state = None):
        ''' called to enable release license
        
        Parameters: return_task - task name to return to when cancel is 
                                  completed successfully
                    return_state - state in return task to return to 
                    
        '''
        self.release_return_task = return_task
        self.release_return_state = return_state
        
    def _valid(self, vocab):
        ''' Determines if a vocabulary word is currently valid
        
        Parameters:
                vocab - vocabulary word to check
                
        returns - True if word is valid as this time, otherwise false
        '''
        #available while picking
        current_task = self.runner.get_current_task()

        if vocab == 'item number':
            return self.item_number is not None
        elif vocab == 'description':
            return (self.description is not None
                    and current_task.name != FA_CANCEL_LICENSE_TASK_NAME)
        
        elif vocab == 'license':
            return self.license_plate is not None
        elif vocab == 'location':
            return self.location is not None
        elif vocab == 'quantity':
            return self.quantity is not None
        elif vocab == 'goal time':
            return self.goal_time is not None
        elif vocab == 'partial':
            return self.location is not None and self.location.allow_partial(current_task.current_state)
        elif vocab == 'cancel':
            return (self.location is not None
                    and self.license is not None
                    and self.cancel_return_task is not None
                    and current_task.name != FA_CANCEL_LICENSE_TASK_NAME
                    and current_task.current_state != RP_COMPLETE_LICENSE
                    and current_task.current_state != PA_COMPLETE_PROMPT
                    and current_task.current_state != PA_TRANSMIT_PUT
                    and current_task.current_state != RP_REPLENISH_LICENSE)
        elif vocab == 'release license':
            return (self.license is not None
                    and self.release_return_task is not None
                    and current_task.current_state != RP_COMPLETE_LICENSE
                    and current_task.current_state != PA_COMPLETE_PROMPT
                    and current_task.current_state != PA_TRANSMIT_PUT
                    and current_task.current_state != RP_REPLENISH_LICENSE)
        return False
    
        
    #Functions to execute words? 
    def _item_number(self):
        ''' speak item number information '''
        if self.item_number == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.item_number)
        return True 

    def _description(self):
        ''' speak description information '''
        if self.description == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(self.description.lower())
        return True 

    def _license(self):
        ''' speak license information '''
        if self.license == '':
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(' '.join(self.license_plate))
        return True 

    def _location(self):
        ''' speak location information '''
        key = 'forkapps.location.info.prompt'
        if self.location.curr_aisle != '':
            key += '.aisle'
        if self.location.curr_slot != '':
            key += '.slot'
        
        prompt_only(itext(key, 
                          self.location.curr_pre_aisle,
                          self.location.curr_aisle,
                          self.location.curr_post_aisle,
                          self.location.curr_slot))
        return True 

    def _quantity(self):
        ''' speak quantity information '''
        if self.quantity == 0:
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(str(self.quantity))
        return True 
    
    def _goal_time(self):
        ''' speak goal time information '''
        if self.goal_time == 0:
            prompt_only(itext('generic.not.available', self.word.vocab))
        else:
            prompt_only(str(self.goal_time))
        return True 

    def _partial(self):
        ''' method to execute partial '''
        self.vocabs['partial'].skip_prompt = False
        current_task = self.runner.get_current_task()
        if self.partial_allowed:
            if prompt_yes_no(itext('forkapps.partial.confirm')):
                if current_task.current_state == GET_LOCATION:
                    prompt_only(itext('forkapps.location.prompt'))
                else:
                    prompt_only(itext('forkapps.partial.confirm.location'))
                self.vocabs['partial'].skip_prompt = True
                self.location.partial_spoken = True
        else:
            prompt_only(itext('forkapps.partial.notallowed'))
        return True
            
    def _cancel(self):
        ''' method to execute cancel '''
        if self.cancel_allowed:
            if prompt_yes_no(itext('forkapps.cancel.confirm')):
                self.runner.launch(obj_factory.get(CancelLicense,
                                                     self.function_number, 
                                                     self.license,
                                                     self.license_plate, 
                                                     self.quantity, 
                                                     self.start_time, 
                                                     self.exception_location,
                                                     self._put_lut, 
                                                     self.cancel_return_task, 
                                                     self.cancel_return_state, 
                                                     self.runner, None))
        else:
            prompt_only(itext('forkapps.cancel.notallowed'))
    
        return True
    
    def _release_license(self):
        ''' method to execute release license '''
        if self.release_allowed:
            if prompt_yes_no(itext('forkapps.release.confirm')):
                self.runner.launch(obj_factory.get(ReleaseLicense,
                                                     self.license, 
                                                     self.start_time, 
                                                     self._put_lut, 
                                                     self.cancel_return_task, 
                                                     self.cancel_return_state, 
                                                     self.runner, None))
        else:
            prompt_only(itext('forkapps.release.notallowed'))
        
        return True