from vocollect_core.task.task import TaskBase
from vocollect_core.dialog.functions import prompt_alpha_numeric, prompt_digits, prompt_ready,\
    prompt_required, prompt_only, prompt_yes_no
from vocollect_core import itext, class_factory, obj_factory
from common.VoiceLinkLut import VoiceLinkLut
from forkapps.SharedConstants import PROMPT_PICKUP_LOCATION, PROMPT_PUT_LOCATION,\
    GET_LOCATION, CONFIRM_LOCATION, FA_VERIFY_LOCATION_TASK_NAME,\
    DIRECT_PREAISLE, DIRECT_AISLE, DIRECT_POSTAISLE, TRANSMIT_LOCATION,\
    PROMPT_START_LOCATION

class Location(object):
    
    def __init__(self, function, prompt_type):
        #location direction values 
        self.prev_aisle = ''
        self.prev_pre_aisle = ''
        self.prev_post_aisle = ''

        #current location information - initialize to license LUT values,
        #but may change if user overrides location. That's why we do not use
        #License LUT values directly
        self.curr_aisle = ''
        self.curr_pre_aisle = ''
        self.curr_post_aisle = ''
        self.curr_slot = ''
        self.curr_cd = ''
        self.curr_scan_value = ''
        self.curr_location_id = None
        
        #variables/settings for prompting and additional vocab
        self.prompt_type = prompt_type
        self.location_length = 0
        self.check_digit_length = 0
        self.verify_location = False
        self.allow_override = False
        
        #additional variables for alternate license and prompts
        self.license_id = None
        self.function = function
        self.quantity = 0
        self.prompt_license = False
        self.capture_pickup_qty = False
        self.license_plate = None
        
        self.partial_spoken = False
        
    def get_prompt(self):
        ''' get prompt type '''
        if self.prompt_type == PROMPT_PICKUP_LOCATION:
            key = 'forkapps.location.pickup'
            if self.prompt_license:
                key += '.license'
            if self.capture_pickup_qty and self.quantity > 0:
                key += '.qty'
                
            return itext(key, 
                         ' '.join(self.curr_slot), 
                         ' '.join(str(self.license_plate)),
                         self.quantity)
        else:
            return ' '.join(self.curr_slot)
        
    def set_from_put_license(self, license):
        ''' set information from a put license record '''
        self.prev_aisle = ''
        self.prev_pre_aisle = ''
        self.prev_post_aisle = ''

        self.curr_pre_aisle = license['preAisle']
        self.curr_aisle = license['Aisle']
        self.curr_post_aisle = license['postAisle']
        self.curr_slot = license['slot']
        self.curr_cd = license['locationCD']
        self.curr_scan_value = license['locationScan']
        self.curr_location_id = license['locationId']
        self.license_id = license['licenseId'] 
        self.prompt_license = False
        self.capture_pickup_qty = False
        self.partial_spoken = False
        
    def set_reserve_from_location_replenish_license(self, license, region):
        ''' set reserve information from replenishment license '''
        self.prev_aisle = ''
        self.prev_pre_aisle = ''
        self.prev_post_aisle = ''

        self.curr_pre_aisle = license['reservePreAisle']
        self.curr_aisle = license['reserevAisle']
        self.curr_post_aisle = license['reservePostAisle']
        self.curr_slot = license['reserveSlot']
        self.curr_cd = license['reserveCD']
        self.curr_scan_value = license['reserveScannedLocation']
        self.curr_location_id = license['destinationLocationID']
        self.license_id = license['number']

        #for pick up prompt
        l_len = region['lic_digits_task_speak']
        self.license_plate = license['licensePlate']
        if l_len > 0 and l_len < len(self.license_plate):
            self.license_plate = self.license_plate[len(self.license_plate) - l_len:]
        self.prompt_license = license['requestLicenseID'] == 1
        self.capture_pickup_qty = region['capture_pickup_quantity'] == 1
        self.partial_spoken = False
        
    def set_destination_from_replenish_license(self, license):
        ''' set destination information from replenish license '''
        self.prev_aisle = ''
        self.prev_pre_aisle = ''
        self.prev_post_aisle = ''

        self.curr_pre_aisle = license['destinationPreAisle']
        self.curr_aisle = license['destinationAisle']
        self.curr_post_aisle = license['destinationPostAisle']
        self.curr_slot = license['destinationSlot']
        self.curr_cd = license['destinationCD']
        self.curr_scan_value = license['destinationScannedValidation']
        self.curr_location_id = license['destinationLocationID']
        self.license_id = license['number']
        self.partial_spoken = False
        
        #set previous aisle info from reserve location
        self.prev_pre_aisle = license['reservePreAisle']
        if self.prev_pre_aisle == self.curr_pre_aisle:
            self.prev_aisle = license['reserevAisle']
        if (self.prev_pre_aisle == self.curr_pre_aisle
            and self.prev_aisle == self.curr_aisle):
            self.prev_post_aisle = license['reservePostAisle']
        
    def set_putaway_region_settings(self, region):
        ''' set values from putaway region record '''
        self.allow_override = region['allow_override_location']
        self.check_digit_length = region['cd_digits_oper_speak']
        self.verify_location = region['verify_license_location']
        self.location_length = region['loc_digits_oper_speak']
        self.partial_spoken = False
    
    def set_replenishment_region_settings(self, region):
        ''' set values from replenishment region record '''
        # may be we dont need this just might want to rename the above functions
        self.allow_override = region['allow_override_location']
        self.check_digit_length = region['cd_digits_oper_speak']
        self.verify_location = region['verify_license_location']
        self.location_length = region['loc_digits_oper_speak']
        self.partial_spoken = False
        
    def get_alt_location(self, quantity = None):
        ''' get an alternate location '''
        self.prev_aisle = ''
        self.prev_pre_aisle = ''
        self.prev_post_aisle = ''

        alternate_loc = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkGetAlternatePutLocation')
        
        if quantity is not None:
            self.quantity = quantity
        
        result = -1
        while result != 0:
            result = alternate_loc.do_transmit(self.function, 
                                               self.license_id,
                                               self.curr_location_id,
                                               self.quantity)

        self.curr_pre_aisle = alternate_loc[0]['preAisle']
        self.curr_aisle = alternate_loc[0]['Aisle']
        self.curr_post_aisle = alternate_loc[0]['postAisle']
        self.curr_slot = alternate_loc[0]['slot']
        self.curr_cd = alternate_loc[0]['locationCD']
        self.curr_scan_value = alternate_loc[0]['locationScan']
        self.curr_location_id = alternate_loc[0]['locationId']
        self.partial_spoken = False
        
    def allow_partial(self, current_state):
        ''' check if partial is allowed '''
        if self.prompt_type == PROMPT_PUT_LOCATION:
            if current_state == GET_LOCATION and self.curr_slot == '':
                return True
            elif current_state == CONFIRM_LOCATION and self.curr_slot != '':
                return True
        
        return False
    
class VerifyLocationTask(class_factory.get(TaskBase)):
    ''' Verify location in forkapps
    
    Steps:
            1. Pre aisle direction
            2. aisle direction 
            3. post aisle direction
            4. get/confirm location
            5. enter check digits if needed
            6. transmit location information
    '''
    #----------------------------------------------------------
    def __init__(self,
                 location_info,
                 taskRunner = None, 
                 callingTask = None):
        super().__init__(taskRunner, callingTask)

        
        #Set task name
        self.name = FA_VERIFY_LOCATION_TASK_NAME

        #working variables
        self._loc_info = location_info
         
        self._location = '' 
        self._check_digits = ''
        self._scanned = False

        #define LUTs
        self._verify_location_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTCoreVerifyLocation')
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        #get region states
        self.addState(DIRECT_PREAISLE,   self.pre_aisle)
        self.addState(DIRECT_AISLE,      self.aisle)
        self.addState(DIRECT_POSTAISLE,  self.post_aisle)
        self.addState(GET_LOCATION,      self.get_location)
        self.addState(CONFIRM_LOCATION,  self.get_check_digits)
        self.addState(TRANSMIT_LOCATION, self.transmit_location)
        
        
    #----------------------------------------------------------     
    def pre_aisle(self):
        ''' directing to Pre Aisle'''
        #if pre-aisle is same as pre-aisle don't prompt
        if (self._loc_info.curr_pre_aisle != self._loc_info.prev_pre_aisle 
            and self._loc_info.curr_slot != ''):
            if self._loc_info.curr_pre_aisle != '':
                prompt_ready(itext('selection.pick.assignment.preaisle', 
                                        self._loc_info.curr_pre_aisle), True)
            
    #----------------------------------------------------------
    def aisle(self):
        ''' directing to Aisle'''
        #if aisle is same as aisle don't prompt
        if (self._loc_info.curr_aisle != self._loc_info.prev_aisle 
            and self._loc_info.curr_slot != ''):
            if self._loc_info.curr_aisle != '':
                prompt_ready(itext('selection.pick.assignment.aisle', 
                                        self._loc_info.curr_aisle), True)

    #----------------------------------------------------------
    def post_aisle(self):
        ''' directing to Post Aisle'''
        #if aisle is same as post-aisle don't prompt
        if (self._loc_info.curr_post_aisle != self._loc_info.prev_post_aisle 
            and self._loc_info.curr_slot != ''): 
            if self._loc_info.curr_post_aisle != '':
                prompt_ready(itext('selection.pick.assignment.postaisle', 
                                        self._loc_info.curr_post_aisle), True)

    #----------------------------------------------------------
    def get_location(self):
        ''' get/confirm location '''
        if self._loc_info.curr_slot == '':
            if self._loc_info.prompt_type == PROMPT_START_LOCATION:
                prompt = itext('forkapps.location.startlocation')
                help = itext('forkapps.location.startlocation.help')
            else:
                prompt = itext('forkapps.location.prompt')
                help = itext('forkapps.location.prompt.help')
                
            location, scanned = prompt_alpha_numeric(prompt,
                                                     help,
                                                     self._loc_info.location_length,
                                                     self._loc_info.location_length,
                                                     self._loc_info.verify_location,
                                                     True) 

            self._location = location
            self._scanned = scanned
            if scanned: #skip check digits prompt
                self.next_state = TRANSMIT_LOCATION
                
    #----------------------------------------------------------
    def get_check_digits(self):
        ''' get check digits if needed '''
        additional_vocab = {}
        prompt = self._loc_info.get_prompt()
        help = itext('forkapps.location.cd.help')
        result = ''
        scanned = False
        
        digits = "0123456789"
        #operator is allowed to speak alphas a the pickup location
        if self._loc_info.prompt_type == PROMPT_PICKUP_LOCATION:
            digits += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        if self._loc_info.prompt_type == PROMPT_PUT_LOCATION:
            additional_vocab['override'] = False
        
        #No slot, but need check digits to transmit validation
        if self._loc_info.curr_slot == '':
            result = prompt_digits(itext('forkapps.location.cd'),
                                   help,
                                   self._loc_info.check_digit_length,
                                   self._loc_info.check_digit_length,
                                   False, False)
            self._check_digits = result
            
                    
        #no check digit or scan location value
        elif self._loc_info.curr_cd == '' and self._loc_info.curr_scan_value == '':
            result = prompt_ready(prompt, True, additional_vocab)

        #scan value, but no check digit. use digits vid
        elif self._loc_info.curr_cd == '' and self._loc_info.curr_scan_value != '':
            additional_vocab['ready'] = False
            result, scanned = prompt_digits(prompt, help, 1000, 1000, False, True, 
                                            additional_vocab)
        #check digit, but no scan value
        elif self._loc_info.curr_cd != '' and self._loc_info.curr_scan_value == '':
            result = prompt_required(prompt, help,
                                    [self._loc_info.curr_cd],
                                    digits, 
                                    None,
                                    additional_vocab)

        #check digit, and scan value
        elif self._loc_info.curr_cd != '' and self._loc_info.curr_scan_value != '':
            result, scanned = prompt_required(prompt, help,
                                              [self._loc_info.curr_cd],
                                              digits, 
                                              [self._loc_info.curr_scan_value], 
                                              additional_vocab)
            

        #check scanned result
        if result == 'override':
            if self._loc_info.allow_override:
                if prompt_yes_no(itext('forkapps.location.override')):
                    self._loc_info.get_alt_location()
                    self.next_state = DIRECT_PREAISLE
                else:
                    self.next_state = CONFIRM_LOCATION
            else:
                prompt_only(itext('forkapps.location.override.notallowed'))
                self.next_state = CONFIRM_LOCATION
            
        elif scanned and result != self._loc_info.curr_scan_value:
            prompt_only(itext('generic.wrongValue.prompt', result))
            self.next_state = CONFIRM_LOCATION
            
            
    #----------------------------------------------------------
    def transmit_location(self):
        ''' transmit location information '''
        if self._loc_info.curr_slot == '':
            result = self._verify_location_lut.do_transmit(int(self._scanned), 
                                                           self._location,
                                                           self._check_digits,
                                                           int(self._loc_info.prompt_type == PROMPT_START_LOCATION))
            
            #set calling tasks properties
            if result == 0: #success
                self._loc_info.curr_location_id = self._verify_location_lut[0]['locationID']
            elif result < 0:
                self.next_state = TRANSMIT_LOCATION 
            else:
                self.next_state = DIRECT_PREAISLE
            
