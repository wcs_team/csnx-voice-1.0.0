from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_ready, prompt_digits, prompt_only, prompt_yes_no
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut

from forkapps.VerifyLocationTask import VerifyLocationTask, Location
from forkapps.ForkAppsVocabulary import ForkAppsVocabulary
from forkapps.AdditionalTasks import CancelLicense

from forkapps.SharedConstants import FA_PUTAWAY_LICENSE_TASK_NAME,\
    FUNCTION_PUTAWAY, PROMPT_START_LOCATION, PROMPT_PUT_LOCATION, PA_NEXT_STEP,\
    PA_INITIALIZE_LICENSE, PA_VERIFY_PICKUP_QTY, PA_VERIFY_START_LOCATION,\
    PA_VERIFY_LOCATION, PA_VERIFY_PUT_QTY, PA_TRANSMIT_PUT, PA_COMPLETE_PROMPT

import time

class PutawayLicensesTask(class_factory.get(TaskBase)):
    ''' Put away license task for the voicelink system
    
    Steps:
            1. Initialize License to put away
            2. verify pickup quantity 
            3. Get Start location if required
            4. Verify Location
            5. Verify Put Quantity
            6. transmit Information
            7. Complete prompt
            8. Determine Next Step
    '''

    #----------------------------------------------------------
    def __init__(self,
                 region_config_lut,
                 licenses_lut,
                 taskRunner = None, 
                 callingTask = None):
        super(PutawayLicensesTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = FA_PUTAWAY_LICENSE_TASK_NAME

        #Luts used in this task
        self._region_config_lut = region_config_lut
        self._licenses_lut = licenses_lut
        
        #working Variables
        self._current_license_rec = 0
        self._current_region = None
        self._current_license = None
        self._start_time = None
        
        #location variables
        self._start_location = obj_factory.get(Location,
                                                 FUNCTION_PUTAWAY, 
                                                 PROMPT_START_LOCATION)
        self._loc_info = obj_factory.get(Location,
                                           FUNCTION_PUTAWAY, 
                                           PROMPT_PUT_LOCATION)
        
        #quantity variables
        self._pick_up_qty = 0
        self._put_qty = 0
        
        #variables set by launching verify location
        self.verify_location_id = None
        self.verify_location_vocab = None

        #putaway LUT
        self._put_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkPutAwayLicense')
         
        #define and setup additional vocabulary object
        self.dynamic_vocab = obj_factory.get(ForkAppsVocabulary,
                                               1, self._put_lut, self.taskRunner)
        self.dynamic_vocab.intialize_cancel(self.name, PA_NEXT_STEP)
        self.dynamic_vocab.intialize_release(self.name, PA_NEXT_STEP)

    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(PA_INITIALIZE_LICENSE,    self.intialize_license)
        self.addState(PA_VERIFY_PICKUP_QTY,     self.verify_pickup_qty)
        self.addState(PA_VERIFY_START_LOCATION, self.start_location)
        self.addState(PA_VERIFY_LOCATION,       self.verify_location)
        self.addState(PA_VERIFY_PUT_QTY,        self.verify_put_quantity)
        self.addState(PA_TRANSMIT_PUT,          self.transmit_put)
        self.addState(PA_COMPLETE_PROMPT,       self.complete)
        self.addState(PA_NEXT_STEP,             self.next_step)

    #----------------------------------------------------------
    def intialize_license(self):
        ''' initialize license '''
        self._current_license = self._licenses_lut[self._current_license_rec]

        #set region to first region record by default, then try to find correct
        #region. 
        self._current_region = self._region_config_lut[0]
        for region in self._region_config_lut:
            if region['number'] == self._current_license['region']:
                self._current_region = region


        #set start time
        self._start_time = time.strftime("%m-%d-%y %H:%M:%S")
        
        #Set location information
        self._start_location.set_putaway_region_settings(self._current_region)
        self._loc_info.set_from_put_license(self._current_license)
        self._loc_info.set_putaway_region_settings(self._current_region)
        
        #Set additional Vocabulary
        self.dynamic_vocab.item_number = self._current_license['itemNumber']
        self.dynamic_vocab.description = self._current_license['description']
        self.dynamic_vocab.license = self._current_license['licenseId']
        self.dynamic_vocab.license_plate = self._current_license['licenseId']
        self.dynamic_vocab.location = self._loc_info
        self.dynamic_vocab.quantity = self._current_license['quantity']
        self.dynamic_vocab.goal_time = self._current_license['goalTime']
        self.dynamic_vocab.partial_allowed = self._current_region['allow_partial_put']
        self.dynamic_vocab.cancel_allowed = self._current_region['allow_cancel_license']
        self.dynamic_vocab.exception_location = self._current_region['exception_location']
        self.dynamic_vocab.start_time = self._start_time
        self.dynamic_vocab.release_allowed = self._current_region['allow_release_license']
        
        #Speak license prompt if more than 1 license
        if len(self._licenses_lut) > 1:
            license = self._current_license['licenseId']
            length = self._current_region['lic_digits_task_speak']
            if length > len(license) or length <= 0:
                length = len(license)
            license = license[len(license) - length:len(license)]
            prompt_ready(itext('forkapps.putaway.put.license', license))

    #----------------------------------------------------------
    def verify_pickup_qty(self):
        ''' verify the pick up quantity '''
        
        if self._current_region['capture_pickup_quantity']:
            qty = prompt_digits(itext('forkapps.putaway.pickup.qty'), 
                                itext('forkapps.putaway.pickup.qty.help'), 
                                1, 9, 
                                self._current_license['quantity'] <= 0)
    
            #Quantity cannot be 0
            qty = int(qty)
            if qty == 0:
                prompt_only(itext('forkapps.putaway.pickup.qty.zero'))
                self.next_state = PA_VERIFY_PICKUP_QTY
            
            #Expexected quantity is not 0 and not equal to spoken quantity 
            elif self._current_license['quantity'] not in [0, qty]: 
                if prompt_yes_no(itext('forkapps.putaway.pickup.qty.confirm',
                                            qty, self._current_license['quantity'])):
                    if self._current_region['allow_override_quantity']:
                        self._pick_up_qty = qty
                    else:
                        prompt_only(itext('forkapps.putaway.pickup.qty.cancel'))
                        self.launch(obj_factory.get(CancelLicense,
                                                      1, 
                                                      self._loc_info.license_id, 
                                                      self._loc_info.quantity, 
                                                      self._start_time, 
                                                      self._current_region['exception_location'], 
                                                      self._put_lut, 
                                                      self.name, PA_NEXT_STEP,
                                                      self.taskRunner, self), PA_NEXT_STEP)
                else:
                    self.next_state = PA_VERIFY_PICKUP_QTY
            else:
                self._pick_up_qty = qty
        else:
            self._pick_up_qty = self._current_license['quantity']
        
        self.dynamic_vocab.quantity = self._pick_up_qty
        self._put_qty = self._pick_up_qty
            
    #----------------------------------------------------------
    def start_location(self):
        ''' verify start location '''
        #Check if we need to capture start location
        if (self._current_region['capture_start_location'] == 3):        
            #launch verify location task to get start location
            self.launch(obj_factory.get(VerifyLocationTask,
                                          self._start_location, 
                                          self.taskRunner, self))

    #----------------------------------------------------------
    def verify_location(self):
        ''' either have operator confirm slot, or enter a location '''
        self.launch(obj_factory.get(VerifyLocationTask,
                                      self._loc_info, 
                                      self.taskRunner, self))
            
    #----------------------------------------------------------
    def verify_put_quantity(self):
        ''' verify the put quantity '''
        if self._current_region['capture_put_quantity'] or self._loc_info.partial_spoken:
            qty = prompt_digits(itext('forkapps.putaway.put.qty'), 
                                itext('forkapps.putaway.put.qty.help'), 
                                1, 9, 
                                self._pick_up_qty <= 0)
        
            #Quantity cannot be 0
            qty = int(qty)
            if qty == 0:
                prompt_only(itext('forkapps.putaway.put.qty.zero'))
                self.next_state = PA_VERIFY_PUT_QTY
            
            #Value greater not allowed
            elif self._pick_up_qty > 0 and qty > self._pick_up_qty:
                prompt_only(itext('forkapps.putaway.put.qty.invalid', 
                                       qty, self._pick_up_qty))
                self.next_state = PA_VERIFY_PUT_QTY

            #value less, check if allowed partial, and if intended partial, 
            elif self._pick_up_qty > 0 and qty < self._pick_up_qty:
                if self._current_region['allow_partial_put']:
                    if self._loc_info.partial_spoken:
                        prompt = itext('forkapps.putaway.put.qty.confirm.less', qty)
                    else:
                        prompt = itext('forkapps.putaway.put.qty.partial', 
                                            qty, self._pick_up_qty)
                    if prompt_yes_no(prompt, True):
                        self._put_qty = qty
                    else:
                        self.next_state = PA_VERIFY_PUT_QTY
                else:
                    prompt_only(itext('forkapps.putaway.put.qty.invalid', 
                                           qty, self._pick_up_qty))
                    self.next_state = PA_VERIFY_PUT_QTY
                    

            #set quantity
            else:
                self._put_qty = qty
        else:
            self._put_qty = self._pick_up_qty

    #----------------------------------------------------------
    def transmit_put(self):
        ''' transmit the put information to the host '''
        if self._pick_up_qty > self._put_qty and self._pick_up_qty > 0:
            partial = '0'
        else:
            partial = '1'
             
        result = self._put_lut.do_transmit(self._current_license['licenseId'],
                                           self._put_qty,
                                           self._loc_info.curr_location_id,
                                           partial,
                                           '',
                                           self._start_time)
        
        if result != 0:
            self.next_state = PA_TRANSMIT_PUT

    #----------------------------------------------------------
    def complete(self):
        ''' check complete '''
        if self._pick_up_qty - self._put_qty <= 0:
            prompt_ready(itext('forkapps.complete'))
        
    #----------------------------------------------------------
    def next_step(self):
        ''' check if there are more licenses to put away '''
        self._pick_up_qty -= self._put_qty
        if self._pick_up_qty < 0:
            self._pick_up_qty = 0
            
        self._put_qty = self._pick_up_qty
        
        #more quantity to put for this license, so get alternate location
        if self._pick_up_qty > 0:
            self._loc_info.get_alt_location(self._pick_up_qty)
            self._start_time = time.strftime("%m-%d-%y %H:%M:%S")
            self.dynamic_vocab.start_time = self._start_time
            self.dynamic_vocab.quantity = self._pick_up_qty    
            self.next_state = PA_VERIFY_LOCATION
        else:
            self._pick_up_qty = 0
            self._current_license_rec += 1
            if len(self._licenses_lut) > self._current_license_rec:
                self.next_state = PA_INITIALIZE_LICENSE

        self._loc_info.partial_spoken = False

        
