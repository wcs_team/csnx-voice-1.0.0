from vocollect_core.task.task import TaskBase

from vocollect_core.dialog.functions import prompt_only, prompt_alpha_numeric, prompt_yes_no
from vocollect_core import itext, obj_factory, class_factory
from common.VoiceLinkLut import VoiceLinkLut

from common.RegionSelectionTasks import MultipleRegionTask
from forkapps.VerifyLocationTask import VerifyLocationTask, Location
from forkapps.PutawayLicensesTask import PutawayLicensesTask
from Globals import change_function
from forkapps.SharedConstants import FA_PUTAWAY_TASK_NAME, FUNCTION_PUTAWAY,\
    PROMPT_START_LOCATION, PA_REGIONS, PA_VALIDATE_REGION, PA_START_LOCATION,\
    PA_SPECIFY_LICENSES, PA_GET_LICENSES, PA_PUT_LICENSES
from core.SharedConstants import CORE_TASK_NAME, REQUEST_FUNCTIONS

PA_CHECK_INTERLEAVE = 'PutAwayCheckInterleave'

class PutawayTask(class_factory.get(TaskBase)):
    ''' Put away task for the voicelink system
    
    Steps:
            1. Get region
            2. Validate regions
            3. Get Start Location
            4. Specify Licenses
            5. Get Licenses
            6. put licenses
    '''

    #----------------------------------------------------------
    def __init__(self,
                 taskRunner = None, 
                 callingTask = None):
        super(PutawayTask, self).__init__(taskRunner, callingTask)

        #Set task name
        self.name = FA_PUTAWAY_TASK_NAME

        self._valid_regions_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkValidPutAwayRegions')
        self._request_reqion_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkRequestPutAwayRegion')
        self._region_config_lut = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkPutAwayRegionConfiguration')
        self._verify_license = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkVerifyLicense')
        self._licenses = obj_factory.get(VoiceLinkLut, 'prTaskLUTForkGetPutAway')
        
        self._start_location = obj_factory.get(Location,
                                                 FUNCTION_PUTAWAY, 
                                                 PROMPT_START_LOCATION)
        self._license_count = 0
        
        #variables set by launching verify location
        self.verify_location_id = None
        self.verify_location_vocab = None

        self._region_selected = False
        
    #----------------------------------------------------------
    def initializeStates(self):
        ''' Initialize States and build LUTs '''
        
        #get region states
        self.addState(PA_REGIONS,                  self.regions)
        self.addState(PA_VALIDATE_REGION,          self.validate_regions)
        self.addState(PA_START_LOCATION,           self.start_location)
        self.addState(PA_SPECIFY_LICENSES,         self.specify_licenses)
        self.addState(PA_GET_LICENSES,             self.get_licenses)
        self.addState(PA_PUT_LICENSES,             self.put_licenses)
        self.addState(PA_CHECK_INTERLEAVE,         self.interleave, PA_PUT_LICENSES)
        
    #----------------------------------------------------------
    def regions(self):
        ''' Run multi region selection '''
        self._region_selected = False
        self.launch(obj_factory.get(MultipleRegionTask, self.taskRunner, self))

    #----------------------------------------------------------
    def validate_regions(self):
        ''' validates region selection '''
        #check for valid regions
        valid = False
        for region in self._valid_regions_lut:
            if region['number'] > 0:
                valid = True
        
        if not valid:
            prompt_only(itext('generic.regionNotAuth.prompt'))
            self.next_state = ''
        else:
            self._start_location.curr_location_id = None
            self._region_selected = True


    #----------------------------------------------------------
    def start_location(self):
        ''' Check if start location is required '''
        self._license_count = 0

        #Check if we need to capture start location
        if self._region_config_lut[0]['capture_start_location'] == 2:
            self._start_location.curr_location_id = None
        elif self._region_config_lut[0]['capture_start_location'] != 1:
            self._start_location.curr_location_id = ''
            
        if self._start_location.curr_location_id is None:
            #launch verify location task to get start location
            self._start_location.set_putaway_region_settings(self._region_config_lut[0])
            self.launch(obj_factory.get(VerifyLocationTask,
                                          self._start_location, 
                                          self.taskRunner, self))
            
    #----------------------------------------------------------
    def specify_licenses(self):
        ''' specify licenses '''
        self.set_sign_off_allowed(True)
        
        #determine max_len operator can speak
        max_len = self._region_config_lut[0]['lic_digits_oper_speak']
        min_len = max_len
        help = itext('forkapps.putaway.license.help')
        if max_len <= 0:
            min_len = 1
            max_len = None
            help = itext('forkapps.putaway.license.help.ready')

        #prompt for license    
        license, scanned = prompt_alpha_numeric(itext('forkapps.putaway.license'),
                                                help,
                                                min_len, max_len,
                                                self._region_config_lut[0]['verify_license_location'],
                                                True,
                                                {'no more' : False, 
                                                 'change function' : False,
                                                 'change region' : False})
        
        #process input
        if license == 'no more':
            if self._license_count == 0:
                prompt_only(itext('forkapps.putaway.license.zero'))
                self.next_state = PA_SPECIFY_LICENSES
        elif license == 'change function':
            change_function()
            self.next_state = PA_SPECIFY_LICENSES
        elif license == 'change region':
            self.change_region()
            self.next_state = PA_SPECIFY_LICENSES
        else:
            result = -1
            while result < 0:
                result = self._verify_license.do_transmit(license, int(scanned))

            if result == 0:
                self._license_count += 1
                if self._license_count < self._region_config_lut[0]['max_lic_plate']:
                    self.next_state = PA_SPECIFY_LICENSES
            else:
                self.next_state = PA_SPECIFY_LICENSES 

        #going to get work
        if self.next_state == None:
            prompt_only(itext('forkapps.putaway.license.get'))
            
    #----------------------------------------------------------
    def get_licenses(self):
        ''' request licenses from host '''
        result = self._licenses.do_transmit()
        
        if result != 0:
            self.next_state = PA_GET_LICENSES

        #reset license's entered count for next time around
        self._license_count = 0
     
    #----------------------------------------------------------
    def put_licenses(self):
        ''' launch the put license task '''
        self.set_sign_off_allowed(False)
        
        self.launch(obj_factory.get(PutawayLicensesTask,
                                      self._region_config_lut, 
                                      self._licenses, self.taskRunner, self), 
                    PA_CHECK_INTERLEAVE)
        
    #----------------------------------------------------------
    def interleave(self):        
        if self.taskRunner.findTask(CORE_TASK_NAME)._signon_lut[0]['interleave']:
            self.return_to(CORE_TASK_NAME, REQUEST_FUNCTIONS)    
        else:
            self.next_state =PA_SPECIFY_LICENSES 

    #----------------------------------------------------------
    def set_sign_off_allowed(self, allowed = False):
        task = self.taskRunner.findTask(CORE_TASK_NAME)
        if task is not None:
            task.sign_off_allowed = allowed

    #======================================================
    #Global/Dynamic Word Functions
    #======================================================
    def change_region(self):
        ''' Called when operator speaks global word "change region" '''
        if self._region_selected:
            if prompt_yes_no(itext('generic.change.region'), True):
                self._region_selected = False
                self.return_to(self.name, PA_REGIONS)
