#
# Copyright (c) 2010-2011 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
# 
 
from voice import current_dialog

##############################################################################
# These dialog wrapper functions are imported here into the default namespace
# in order to preserve backward compatibility to the 1.0 Core Library release.
# However, having them in the default namespace is not good practice, so this
# may be removed at some future time. You should use these functions directly
# from their new locations in the vocollect_core.dialog.functions module.
##############################################################################
from vocollect_core.dialog.functions import prompt_alpha, prompt_alpha_numeric                       #@UnusedImport
from vocollect_core.dialog.functions import prompt_digits, prompt_digits_required, prompt_float      #@UnusedImport
from vocollect_core.dialog.functions import prompt_list, prompt_list_lut, prompt_list_lut_auth       #@UnusedImport
from vocollect_core.dialog.functions import prompt_only, prompt_ready, prompt_required, prompt_words #@UnusedImport
from vocollect_core.dialog.functions import prompt_yes_no, prompt_yes_no_cancel                      #@UnusedImport
from vocollect_core import scanning

#############################################################################
# Callback methods associated with Core Dialog Links and Nodes. These are 
# required to be in the default namespace because of a current limitation
# in the VoiceCatalyst runtime. This limitation may be removed in the future. 
#############################################################################

#----------------------------------------------------------------------------
# General Callback Methods
#----------------------------------------------------------------------------
def confirm_entry():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().confirm_prompt()

def set_result():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    if not was_http_posted():
        current_dialog().set_result()
    
def is_barcode_scanned():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().scanned_result()

def dialog_time_out():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().dialog_time_out()

def skip_prompt():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().skip_prompt

def prompt_here():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().prompt_here()

def was_http_posted():
    return current_dialog().was_http_posted()
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''

#----------------------------------------------------------------------------
# Callback methods for list and digits dialogs
#----------------------------------------------------------------------------
def is_valid_value():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().is_valid_value()

def result_less_than_min_length():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().result_less_than_min_length()
 
def timeout_digit_entry():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().timeout_digit_entry()

def invalid_count_check():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().invalid_count_check()
    

#----------------------------------------------------------------------------
#List Dialog callback methods
#----------------------------------------------------------------------------

def reset():
    ''' resets current running dialog by setting result to None, and calling set_help '''
    if not was_http_posted():
        current_dialog().result = None
    current_dialog().set_help()
    
def list_move_first():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    current_dialog().move_first()
    
def list_has_next():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().move_next()

def allow_select():
    ''' Pass through method used from VID file. Calls same method in current running dialog instance '''
    return current_dialog().allow_select()

#----------------------------------------------------------------------------
#Digits Dialog callback methods
#----------------------------------------------------------------------------
def reset_scan():
    ''' resets current running dialog by setting result to None, and resetting scan callback method '''
    reset()
    current_dialog().set_scan_callback()
    
def trigger_scan():
    ''' auto trigger the scanner '''
    scanning.auto_trigger_scanner()