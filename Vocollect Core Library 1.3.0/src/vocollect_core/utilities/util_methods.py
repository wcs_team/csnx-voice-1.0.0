import voice

MULTIPLE_SCANS_VERSION = 2.0
MULTIPLE_HINTS_VERSION = 2.0


def catalyst_version():
    return_value = 99
    try:
        version = voice.getenv('SwVersion.ApplicationVersion')
        version = version.rsplit('_', 1)[1] #get version after last _
        version = version.replace('V', '') # remove the V
        version = float(version) # convert to float
        return_value = version
    except:
        #if any errors assume it's an older version
        pass
    
    return return_value

def multiple_scans_supported():
    return catalyst_version() >= MULTIPLE_SCANS_VERSION

def multiple_hints_supported():
    return catalyst_version() >= MULTIPLE_HINTS_VERSION


