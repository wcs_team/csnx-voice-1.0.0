#
# Copyright (c) 2010-2011 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
# 

from vocollect_core.dialog.digits_prompt import DigitsPrompt,\
    DigitsPromptExecutor
from vocollect_core import itext, class_factory
from vocollect_core.scanning import ScanMode
from vocollect_core.utilities import obj_factory

#----------------------------------------------------------------------------
#Float Dialog class
#----------------------------------------------------------------------------
class FloatPrompt(class_factory.get(DigitsPrompt)):
    ''' Wrapper Class for basic numeric float entry dialog
    
    returns: a float value entered by operator
    '''

    def __init__(self, prompt, help, #@ReservedAssignment
                 decimal_places=2, 
                 confirm=True, scan=False):
        ''' Constructor
        
        Parameters:
              prompt - main prompt to be spoken
              help - main help message to be spoken
              decimal_places (Default=2) - number of digits user speaks after decimal place
              confirm (Default=True) - Determine whether or not entered values 
                                       should be confirmed by operator
              scan (Default=False) - determines if scanning needs to be enabled
              
        '''
        super().__init__(prompt, help, 0, decimal_places, confirm, scan)

    def set_result(self):
        ''' sets result value from digits spoken so far '''
        if (self.result == None):
            self.result = self.nodes['StartHere'].last_recog
            self.nodes['Digits'].help_prompt = itext('generic.float.help', 
                                                         self.nodes['StartHere'].help_prompt, 
                                                         ' '.join(self.result)) 
        else:
            current = self.nodes['Digits'].last_recog
            if current == '.' and current in self.result:
                pass #ignore if decimal place already in result
            else:
                self.result = self.result + self.nodes['Digits'].last_recog
                self.nodes['Digits'].help_prompt = itext('generic.float.help', 
                                                         self.nodes['StartHere'].help_prompt, 
                                                         ' '.join(self.result)) 
        
    def timeout_digit_entry(self):
        ''' Function to determine if timeout occurred or 
        maximum number of digits entered
        
        returns: True if timeout exceeded or maximum number of 
        digits entered, otherwise false 
        '''
        if self.result in self.additional_vocab:
            return True
        elif self.max_length is None: #No max, so require user to speak ready as terminator
            return False
        elif self.is_timedout('Digits'):
            return True
        else:
            point = self.result.find('.')
            return point >= 0 and len(self.result[point+1:]) >= self.max_length
            
    def is_valid_value(self):
        ''' check if expected value is required '''
        
        #Value is additional vocabulary
        if self.result in self.additional_vocab:
            return True
        
        #check if correct number of decimal values entered
        if self.max_length is not None:
            point = self.result.find('.')
            if point >= 0 and len(self.result[point+1:]) == self.max_length:
                return True
            else:
                self.nodes['InvalidPrompt'].prompt = itext('generic.not.valid.float.value', self.result)
                self.is_scanned = False
                return False
        else:
            return True
    

#----------------------------------------------------------------------------
#Helper Class used for executing dialogs 
#----------------------------------------------------------------------------
class FloatPromptExecutor(DigitsPromptExecutor):
    
    def __init__(self, 
                 prompt, 
                 priority_prompt = True,
                 help_message = None,
                 decimal_places = 2, 
                 additional_vocab = {},
                 confirm=True, 
                 scan=ScanMode.Off, 
                 skip_prompt = False,
                 hints = None,
                 anchor_words=None):
        ''' Base helper class for creating, configuring, and executing a float dialog
            this classes are intended to make it easier for end users to configure
            dialogs beyond the basic function provided
        '''
        super().__init__(prompt=prompt, 
                         priority_prompt = priority_prompt,
                         help_message=help_message, 
                         additional_vocab=additional_vocab,
                         confirm=confirm, 
                         scan=scan, 
                         skip_prompt=skip_prompt,  
                         hints=hints,
                         anchor_words=anchor_words)
        
        self.decimal_places = decimal_places

    def _create_dialog(self):
        ''' Creates a dialog object and saves it to the dialog member variable.
            This class is not intended to be called directly. retrieve
            the class's dialog property to get the instance of the dialog.
        '''
        self._dialog = obj_factory.get(FloatPrompt,
                                       self.prompt, 
                                       self.help_message, 
                                       self.decimal_places, 
                                       self.confirm, 
                                       self.scan_mode)

        #change the speakable digits to include decimal point
        self._dialog.add_additional_digits(['.'])
