#
# Copyright (c) 2010-2011 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
# 
import voice
from vocollect_core.dialog.base_dialog import BaseDialog, BaseDialogExecutor
from vocollect_core import itext, class_factory
from vocollect_core.scanning import ScanMode, get_scan_result, set_scan_mode,\
    scan_results_exist, get_trigger_vocab
from vocollect_core.utilities import obj_factory
from vocollect_core.utilities.util_methods import multiple_hints_supported,\
    MULTIPLE_HINTS_VERSION

#----------------------------------------------------------------------------
#Digits wrapper class
#----------------------------------------------------------------------------
class DigitsPrompt(class_factory.get(BaseDialog)):
    ''' Wrapper Class for basic digit entry dialog
    
        returns: Digits entered by operator
    '''

    def __init__(self, prompt, help, #@ReservedAssignment
                 min_length=1, max_length=10,
                 confirm=True, scan=False):
        ''' Constructor
        
        Parameters:
              prompt - main prompt to be spoken
              help - main help message to be spoken
              min_length (Default=1) - minimum number of digits allowed
              max_length (Default=10) - Maximum number of digits allowed
              confirm (Default=True) - Determine whether or not entered values 
                                       should be confirmed by operator
              scan (Default=False) - determines if scanning needs to be enabled
              
        Additional properties
              confirm_prompt_key - resource key for confirmation prompt
              
        '''
        super().__init__('Digits')

        self.nodes['PromptHere'].prompt = prompt
        self.nodes['StartHere'].help_prompt = help

        # Turn off Speak ahead when processing enters the ConfirmPrompt node
        self.nodes['ConfirmPrompt'].is_allow_speak_ahead_node = False

        # Turn off Speak ahead when processing goes back to the StartHere node
        # because dialogs.result_less_than_min_length returned True
        # meaning the user started speaking but did not enter the minimum number 
        # of digits before timing out.
        self.links['Link21'].is_allow_speak_ahead_link = False
        
        self.help = help

        self.min_length = min_length
        self.max_length = max_length
        self.confirm = confirm
        self.scan_mode = ScanMode.convert_mode(scan)
        
        #properties for expected values
        self.expected = None
        self.is_expected_required = False
        self.invalid_key = 'generic.wrongValue.prompt'
        self.is_scanned = False
        
        self.invalid_count_max = 2
        self.invalid_count_curr = 0
        self.include_anchor = False
        
        if self.max_length is None or self.max_length <= 0:
            if 'cancel' in voice.get_all_vocabulary_from_vad():
                self.links['link_cancel'] = voice.Link('link_cancel',
                                                       self.nodes['Digits'],
                                                       self.nodes['PromptHere'],
                                                       ['cancel'])
        self.configure_scanning()
        self._final_configuration()

    def prompt_here(self):
        ''' code to execute when main prompt is entered '''
        super().prompt_here()
        self.invalid_count_curr = 0
            
    def set_help(self, additional = []):
        ''' set help includes the additional vocabulary to the help

        Parameters:
                additional - Single word, list of words, or dictionary of words.
        '''
        help = self.nodes['StartHere'].help_prompt #@ReservedAssignment
        vocab = list(self.additional_vocab.keys())
        if vocab == None:
            vocab = []

        vocab.extend(additional)
        
        if (len(vocab)) == 1:
            if not vocab[0] in help:
                help = help + ' '+itext('generic.help.dynamic.single', vocab[0]) #@ReservedAssignment
        elif (len(vocab)) > 1:
            vocabhelp = ''
            for index in range(len(vocab)-1):
                vocabhelp = vocabhelp + str(vocab[index]) + ', '
            if not vocabhelp in help:   
                help = help + ' '+ itext('generic.help.dynamic.multiple', vocabhelp, vocab[(len(vocab)-1)]) #@ReservedAssignment

        self.nodes['StartHere'].help_prompt = help

        # To address RBIRD-255, initialize the help for Digits node to be 
        # the same as StartHere -- IF the dialog requires READY, code elsewhere will
        # override the help 
        self.nodes['Digits'].help_prompt = help

        
    def set_required(self, expected, expected_scan_values = None):
        ''' configures digits VID to expect a specific value. This will also 
        set the min and max digits to enter based on expected values

        Parameters:
                expected - List of expected items.
                expected_scan_values - List of expected values for scanning
        
        Note:  This function expects at least one expected value.  If an
               empty string is passed (or a list of empty strings) then
               the voice dialog will still require "ready" to be spoken 
               otherwise the value must be entered via barcode scanning.
        '''
        
        self.expected = []
        self.expected.extend(expected)
        self.max_length = 0
        self.min_length = 1000000

        for item in expected:
            if len(str(item)) > self.max_length:
                self.max_length = len(str(item))
            if len(str(item)) < self.min_length:
                self.min_length = len(str(item))

        if self.max_length == 0:
            #all expected values are empty - set max_length to a large 
            #number so all voice input will be ignored except for READY
            self.max_length = 1000000
        
        if self.min_length == 0:
            self.min_length = self.max_length
        
            
        if expected_scan_values != None:
            self.expected.extend(expected_scan_values)
            
        self.is_expected_required = True

        self.set_hints(expected)        
    
    def set_hints(self, hints):
        ''' Adds hints to dialog to help improve recognition '''
        all_hints = []
        if multiple_hints_supported():
            #add ready to the end of hints if not already added to improve templates
            for hint in hints:
                if hint != None:
                    if hint not in all_hints and len(hint) > 1:
                        all_hints.append(hint)
                    if not self.include_anchor and len(hint) > 0: #Only add ready if not doing anchor words
                        hint_ready = hint + 'ready'
                        if hint_ready not in all_hints:
                            all_hints.append(hint_ready)
        else:
            #Only use first hint for versions of catalyst earlier than 2.0
            for hint in hints:
                if hint != None and len(hint) > 1:
                    all_hints.append(hint)
                    break;
            
            #log warning if multiple hints given
            if len(hints) > 1:
                voice.log_message('INFO: Multiple hints provided, but multiple hints not supported until version ' 
                                  + str(MULTIPLE_HINTS_VERSION)  
                                  + ' or greater. Hint set to ' + str(all_hints))
             
            
        self.nodes['StartHere'].response_expression = all_hints
    
    def invalid_count_check(self):
        ''' check how many times an invalid entry was done since main prompt '''
        self.invalid_count_curr += 1
        return self.invalid_count_curr < self.invalid_count_max
    
    def is_valid_value(self):
        ''' check if expected value is required '''
        
        #Value is additional vocabulary
        if self.result in self.additional_vocab:
            return True
        
        #check if required and result matches expected
        if self.is_expected_required and self.result not in self.expected:
            key = self.invalid_key
            if not set(self.result).issubset(set('0123456789')):
                key = 'generic.spell.wrongValue.prompt'
                
            #not a valid entry
            self.nodes['InvalidPrompt'].prompt = itext(key, self.result)
            self.is_scanned = False
        else:
            return True
      
    def result_less_than_min_length(self):
        ''' Function called from dialog to see if user entered 
        at least the minimum number of digits 
    
        returns: True if minimum entered, otherwise False
        '''

        #Value is additional vocabulary, so we don't care if shorter
        if self.result in self.additional_vocab:
            return False
        
        #if no min length, then check if anchor word was spoken or maximum was reached
        if self.min_length == None:
            return (self.nodes['Digits'].last_recog not in self.links['Link22'].vocab 
                    and len(self.result) < self.max_length)
        
        return len(self.result) < self.min_length

    def set_result(self):
        ''' sets result value to the digits collected so far'''
        if (self.result == None):
            self.result = self.nodes['StartHere'].last_recog
        else:
            self.result = self.result + self.nodes['Digits'].last_recog
        
        #Set help if operator is required to speak ready to complete entry
        if self.max_length is None or self.max_length <= 0:
            help_key = 'generic.digits.help'
            if 'cancel' in voice.get_all_vocabulary_from_vad():
                help_key = 'generic.digits.help.cancel'
            
            self.nodes['Digits'].help_prompt = itext(help_key, self.result) 
                
    def timeout_digit_entry(self):
        ''' Function to determine if timeout occurred or 
        maximum number of digits entered
        
        returns: True if timeout exceeded or maximum number of 
        digits entered, otherwise false 
        '''
        if self.result in self.additional_vocab:
            return True
        elif self.max_length is None: #No max, so require user to speak ready as terminator
            return False
        elif self.max_length <= 0: #Invalid max lenght, treat same as not set
            return False
        elif self.is_timedout('Digits'):
            return True
        else:
            return len(self.result) >= self.max_length
        
    def set_additional_vocab(self, additional_vocab):
        ''' adds additional vocab user may speak at main prompt '''
        BaseDialog.set_additional_vocab(self, additional_vocab)
        self._remove_existing_vocab('StartHere')
        self.links['FirstDigit'].vocab |= set(self.additional_vocab.keys())
        
    def add_additional_digits(self, digits):
        ''' Add additional digits like alphas to main links
        
        Parameters:
            digits - List of digits to add to links for user to speak
        '''
        self.links['FirstDigit'].vocab |= set(digits)
        self.links['AdditionalDigits'].vocab |= set(digits)
    
    def remove_digits(self, digits):
        ''' Removed digits from main links
        
        Parameters:
            digits - List of digits to remove from links 
        '''
        self.links['FirstDigit'].vocab -= set(digits) 
        self.links['AdditionalDigits'].vocab -= set(digits) 
    
    def set_anchors(self, anchor_words):
        orig = self.links['Link22'].vocab 
        #add all new anchor words
        self.links['Link22'].vocab |= set(anchor_words)
        #remove original vocab (ready) if it is not supposed to be an anchor word
        self.links['Link22'].vocab -= (orig - set(anchor_words))
        self.include_anchor = True 
    
    
    def configure_scanning(self):
        ''' Adds some additional nodes and links if a trigger_scan_vocab is set
            and scanning is on. Otherwise does nothing 
        '''
        #Only create if scanning and trigger scan is valid vocab word
        if self.scan_mode != ScanMode.Off and get_trigger_vocab() != "":
                #Add node to trigger a scan
                self.nodes['TrigerScan'] = voice.Node('TrigerScan', 
                                                      '', '', False, 
                                                      'dialogs.trigger_scan')
            
                #add vocab link to goto trigger scan node
                self.links['LinkToTriggerScan'] = voice.Link('LinkToTriggerScan',
                                                             self.nodes['StartHere'],
                                                             self.nodes['TrigerScan'],
                                                             [get_trigger_vocab()])
                
                #add default link to return to StartHere
                self.links['LinkFromTriggerScan'] = voice.Link('LinkFromTriggerScan',
                                                               self.nodes['TrigerScan'],
                                                               self.nodes['StartHere'],
                                                               [])
    def scanned_result(self):
        ''' set the scan result from global scan
        '''
        if scan_results_exist():
            self.result = get_scan_result()
            self.is_scanned = True
            return True
        
        return False
                 
    def set_scan_callback(self):
        ''' sets the scan call back
        '''
        try:
            self.nodes['Digits'].last_recog = None
            set_scan_mode(self.scan_mode)
        except:
            self.result = None

    def http_post(self, post_value):
        ''' check and set result and http_post to true '''
        self.result = post_value
        self._http_posted = True
    
    def http_get(self):
        
        self.http_get_template()
        
        prompt = self.nodes['PromptHere'].prompt
        form = self.form_template.replace("${prompt}", prompt)
        form = form.replace("${help}", self.help)
        form = form.replace("${otherActions}", self.http_other_actions_form())

        return form

    def confirm_prompt(self):
        if self.should_confirm():
            if self.include_anchor and self.result not in self.additional_vocab:
                key = 'generic.correct.anchor.confirm'
                self.nodes['ConfirmPrompt'].prompt = itext(key, self.result, 
                                                           self.nodes['Digits'].last_recog)
                return True
            else:
                return super().confirm_prompt()
        
        return False
    
    def run(self):
        '''
        Override BaseDialog run to check for alpha vocabs on links.  If
        so, change the default speech delay to 800ms.
        '''
        ret_value = None
        
        try:
            ret_value = super().run()
        
        except Exception as err:
            set_scan_mode(ScanMode.Off)
            raise err

        finally:
            # Clean up - turn off scanning if mode is not mutliple scanning
            if hasattr(self, 'scan_mode'):
                if (self.scan_mode == ScanMode.Single):
                    set_scan_mode(ScanMode.Off)
        
        return ret_value


#----------------------------------------------------------------------------
#Helper Class used for executing dialogs 
#----------------------------------------------------------------------------
class DigitsPromptExecutor(BaseDialogExecutor):
    
    def __init__(self, 
                 prompt, 
                 priority_prompt = True,
                 help_message = None, 
                 additional_vocab = {},
                 min_length=1, 
                 max_length=10, #@ReservedAssignment
                 confirm=True, 
                 scan=ScanMode.Off, 
                 skip_prompt = False,  
                 characters = None,
                 required_spoken_values = None, 
                 required_scanned_values = None,
                 anchor_words = None,
                 hints = None):
        ''' Base helper class for creating, configuring, and executing a digits dialog
            this classes are intended to make it easier for end users to configure
            dialogs beyond the basic function provided
        '''
        super().__init__(prompt=prompt,
                         help_message=help_message,
                         priority_prompt=priority_prompt,
                         additional_vocab=additional_vocab,
                         skip_prompt=skip_prompt)

        #Addtional configuration properties
        self.min_length = min_length
        self.max_length = max_length
        self.confirm = confirm
        self.scan_mode = ScanMode.convert_mode(scan)
        self.characters = characters
        self.required_spoken_values = required_spoken_values 
        self.required_scanned_values = required_scanned_values
        self.anchor_words = anchor_words
        self.hints = hints

        #additional result variables
        self.scanned = None
        self.anchor_result = None
    
    def _create_dialog(self):
        ''' Creates a dialog object and saves it to the dialog member variable.
            This class is not intended to be called directly. retrieve
            the class's dialog property to get the instance of the dialog.
        '''
        self._dialog = obj_factory.get(DigitsPrompt, 
                                       self.prompt, 
                                       self.help_message, 
                                       self.min_length, 
                                       self.max_length, 
                                       self.confirm, 
                                       self.scan_mode)
        
    def _configure_dialog(self):    
        ''' Configures dialog object based on additional settings
            This method is not intended to be called directly. retrieve
            the class's dialog property to get the instance of the dialog.
        '''
        #add additional vocab to dialog and set skip prompt property
        self.dialog.set_additional_vocab(self.additional_vocab)
        self.dialog.skip_prompt = self.skip_prompt
        self.dialog.nodes['PromptHere'].prompt_is_priority = self.priority_prompt
        if not self.priority_prompt: # scanner already triggered so do not scan again
            self.dialog.trigger_scan = False
        
        #change the speakable digits if new list is provided
        if self.characters != None:
            self.dialog.add_additional_digits(self.characters)
            self.dialog.remove_digits(
                set(['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']) - set(self.characters))
        
        #change terminate word 'ready' to other anchor words 
        if self.anchor_words != None:
            self.dialog.set_anchors(self.anchor_words)
    
        #add required values if provided, this will also set hints to required spoken values
        if self.required_spoken_values != None:
            self.dialog.set_required(self.required_spoken_values, 
                                     self.required_scanned_values)
    
        #Add hints to dialog if provided
        if self.hints != None:
            self.dialog.set_hints(self.hints)

    def check_scanning_result(self):
        ''' Method initializes scanning if necessary and checks if there may
            already be a result from scanning before even launching the dialog
            this would usually only occur when doing multiple scanning
            
            returns: True if scan result exists, otherwise false
        '''
        #require scanned values are provided, then dialog must be used, 
        #otherwise Initialize scanning and see if result exists before entering dialog
        if (self.scan_mode != ScanMode.Off 
            and self.required_scanned_values == None 
            and not self.priority_prompt):
            #Check for existing result, if found return it
            if scan_results_exist():
                self.result = get_scan_result()
                self.scanned = True
        
        return self.result != None
    
    def execute_dialog(self):
        ''' Executes the dialog
        '''
        if not self.check_scanning_result():
            #No scan result yet so launch dialog
            self.result = self.dialog.run()
            
            #get additional results
            self.scanned = self.dialog.is_scanned
            
            #get anchor_used
            self.anchor_result = None
            if (self.dialog.include_anchor != None 
                and not self.dialog.is_scanned 
                and set(self.result).issubset(self.dialog.links['FirstDigit'].vocab)):
                self.anchor_result = self.dialog.nodes['Digits'].last_recog

    def get_results(self):
        ''' Gets the results of the dialog, Will execute the dialog if it has not
            already been executed

            returns: 
                if anchor words and scanning used then tuple(result, anchor_word, scanned)
                else if anchor words then tuple(result, anchor_word)
                else if scanning then tuple(result, scanned)
                else result
        '''
        if self.result == None: #Dialog has not been executed yet
            self.execute_dialog()

        #If anchor words provided        
        if self.anchor_words != None:
            if self.scan_mode != ScanMode.Off:
                return self.result, self.anchor_result, self.scanned
            else:
                return self.result, self.anchor_result
        #standard returns
        else:
            if self.scan_mode != ScanMode.Off:
                return self.result, self.scanned
            else:
                return self.result
