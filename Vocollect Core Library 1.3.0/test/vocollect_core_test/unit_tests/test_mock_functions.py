#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#

''' this module test the mock client functions that are not
commonly used throught other testing
'''

import mock_catalyst #@UnusedImport
import unittest

import voice

from vocollect_core_test.base_test_case import BaseTestCaseCore #@UnusedImport - here for test framework
from vocollect_core.dialog.ready_prompt import ReadyPrompt

class TestDialogs(ReadyPrompt):

    ''' test dialog for getting/setting values'''
    def prompt_here(self):
        pass
        
class TestMockFunctions(BaseTestCaseCore):

    def test_audio(self):
        voice.audio.play('test/path')
        voice.audio.stop_playing()
        voice.audio.start_recording('test/record/path', 10)
        voice.audio.stop_recording()

        self.validate_prompts('Play Called on: test/path',
                              'Stop playing called', 
                              'Play Called on: test/record/path Max Seconds: 10',
                              'Stop recording called')
    
    def test_dialog(self):
        self.post_dialog_responses('ready')
        d = TestDialogs('test prompt')
        result = d.run()
        
        self.validate_prompts('test prompt')
        self.assertEquals(result, 'ready')
        self.assertEquals(voice.last_prompt_spoken, 'test prompt')
        self.assertEquals(voice.last_prompt_is_priority, False)

        self.post_dialog_responses('ready')
        d = TestDialogs('test prompt priority', True)
        result = d.run()

        self.validate_prompts('test prompt priority')
        self.assertEquals(voice.last_prompt_spoken, 'test prompt priority')
        self.assertEquals(voice.last_prompt_is_priority, True)
        
        
if __name__ == "__main__":
    unittest.main()