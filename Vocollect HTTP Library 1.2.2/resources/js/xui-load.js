
/**
 * Global variables
 */
var globals = {
	LONG_POLLING : true,
	DEFAULT_TIMER : 1,
	// Maximum default wait time; usually time to wait when the server doesn't respond
	MAX_TIMER : 10000,
	// Should the application make a request for data
	UPDATE_DATA : false
}

var current_page = "";

//XUI-optimized touch event for touch screen devices; should be changes to "click" if you plan to use
//the screen on non-touch screen devices; there are performance gains when using a touch screen device
//and using the "touchstart" event.
var EVENT_NAME = "touchstart";

if(!('on' + EVENT_NAME in document.documentElement)) {
	EVENT_NAME = "click";
}

/**
 * Used to keep the application advancings from causing issues
 * with the voice app.
 */
var pushing_the_app = false;
var on_hold = false;

/**
 * For compat with JQuery. Does nothing.
 */
var startUp = function() {
}


/**
 * On the load of the page, start off the server checks.
 */
x$(window).load(function() {
	getTheLatestPage();
});

/**
 * Deals with a Mozilla bug in the case where the server
 * wasn't accessible.
 * @param xhrObj
 * @returns {String}
 */
function checkXHRStatus(xhrObj) {
	var status = "";
	try {
		status = xhrObj.statusText;
	} catch (e) {
		status = "network_issue";
	}
	return status;
}

/**
 * Check on the page to be displayed through an async call to the server.
 */
function getTheLatestPage() {
	x$(window).xhr('html/' + current_page, {callback: updatePage, async: true});
}

function updateConnection(on) {
	if (on) {
		x$("#connectionStatus").addClass("connected");
		x$("#connectionStatus").removeClass("disconnected");
	} else {
		x$("#connectionStatus").addClass("disconnected");
		x$("#connectionStatus").removeClass("connected");
	}
}

function noLongerConnected() {
	updateConnection(false);
	timer = globals.MAX_TIMER;
}

function connected() {
	updateConnection(true);
	timer = globals.DEFAULT_TIMER;
}

/**
 * Used to push the application from the browser.
 * @param vocab - the value to be sent (as if the user spoke it)
 */
function advanceTheVoiceApp(vocab) {
	pushing_the_app = true;
	x$(window).xhr("advance/" + vocab, {method:'post', callback: pushedTheApp, async: true});
}

/**
 * Locking function. Frees up the browser to update the page
 * as soon as the application push is complete.
 */
function pushedTheApp() {
	pushing_the_app = false;
	if (on_hold) {
		on_hold = false;
		// If we're long polling, no need to check the current page
		if (!globals.LONG_POLLING)
			getTheLatestPage();
	}
}

/**
 * Plugs the response into the content page.
 * Also runs addEventsForLinks, to setup any links in the page.
 */
function loadResponseIntoContent() {
	// Plug the response into the content area
	x$('#content').html(this.responseText);
	
	// Update any links to be click-able
	addEventsForLinks();
}

/**
 * Reviews the DOM for links and adds an event for clicking.
 */
function addEventsForLinks() {
	links = x$("a");
	for (var i=0; i < links.length; i++) {
		element = links[i];
		x$(element).on(EVENT_NAME, null);
		name = x$(element).html().toLowerCase();
		x$("#" + name + "_pic").on(EVENT_NAME, linkClicked);
	}
}

var timer = globals.DEFAULT_TIMER;


/**************************** Page updating / Data retrieval ********************/

/**
 * Callback function - checks to see if anything has changed
 * If the page to be shown has changed, load the page, then 
 * load the data.
 */
function updatePage() {
	
	//A request has failed; restart
	if (this.readyState == 4 && this.status == 0) {
		setTimeout(getTheLatestPage, globals.MAX_TIMER);
		return;
	}
	
	if (this.responseText == "") return;
	// Make sure we have an OK status
	if (checkXHRStatus(this) == "OK") {
		// If we just pushed the app to change, don't update
		if (pushing_the_app) {
			on_hold = true;
			return;
		}
		
		return_data = this.responseText;
		if (return_data != "") {
			values = return_data.split('/');
			if (values[values.length - 1] == 'refresh') {
				if (globals.LONG_POLLING) {
					x$(window).xhr('data/' + page_name, {callback: updateData});
				} else {
					page_name = return_data.split('/refresh')[0];
					current_page = "";
				}
			} else {
				page_name = return_data;
			}
			
		    if (current_page != page_name) {
		        x$('#content').xhr(page_name + '.html', {callback: loadResponseIntoContent});
		        current_page = page_name;
		        if (globals.UPDATE_DATA) {
		        	x$(window).xhr('data/' + page_name, {callback: updateData});
		        }
		    }		    
		}
		connected();
	} else {
		noLongerConnected();
	}
	setTimeout(getTheLatestPage, timer);
}

/**
 * Call back function for data request.
 * Calls mergeDataIntoPage to inject returned data
 * into the html elements.
 */
function updateData() {
	// Make sure we have an OK status
	if (checkXHRStatus(this) == "OK") {
		return_data = this.responseText;
		var receiving_data = "";
	    try {
	        receiving_data = eval( "(" + return_data + ")" );  
	    } catch ( e ) {
	        if (console) {
	            console.log("Eval of data has failed.");
	        }
	    }
	    if (receiving_data != "") {
	        mergeDataIntoPage(receiving_data); 
	    }
	} else {
		noLongerConnected();
	}
 }

/**
 * Takes the given object and either injects it into the appropriate
 * HTML element, calls update list, or recursively passes the object
 * back to this function.
 * @param obj - the json object to be evaluated.
 */
function mergeDataIntoPage(obj) {
    for (attribute in obj) {
        if (typeof obj[attribute] != 'object') {
        	if (attribute == 'image') {
        		d = new Date();
        		x$("#" + attribute).attr("src", obj[attribute] + "?date=" + d.getTime());
        	} else {
        		if (x$("#" + attribute)[0] != null) {
        			x$("#" + attribute).html(obj[attribute].toString());
        		}
        	}
        } else {
            // if it has a list element
            if (x$("#" + attribute)[0] != null) {
                updateList(obj[attribute], attribute);
            } else {
                // otherwise
                mergeDataIntoPage(obj[attribute]);
            }
        }
    }
}

/**
 * Given a list object and its identifier, this method
 * creates a list of html elements, updates the appropriate 
 * attributes of the objects into the list elements, and then
 * adds it to the DOM.
 * @param listObject - list of json objects
 * @param listId - ID value of the HTML element
 */
function updateList(listObject, listId) { 
    header = x$("li.header")[0].cloneNode(true);
    clickableList = x$(header).hasClass("clickable");
    list = x$("#" + listId)[0];
    x$(list).html('inner', header);
    for (num in listObject) {
        obj = listObject[num];
        li = x$(header.cloneNode(true)).removeClass("header").attr("id",listId + "_" + num)[0];
        for (attr in obj) {
            elementByAttribute = x$(li).find("." + attr)[0]
            x$(elementByAttribute).html(obj[attr].toString());
            if (clickableList && x$(elementByAttribute).hasClass("vocabToPost")) {
            	value = obj[attr];
            	x$(li).on(EVENT_NAME, rowClicked);
            }
        }
        x$(list).html('bottom', li);
    }
}

/**
 * On Click handler for a link.
 * Passes the vocab tied to the link to the server.
 * @param event - browser event object.
 */
function linkClicked(event) {
	var value = event.currentTarget.id.split("_")[0];
	advanceTheVoiceApp(value);
}

/**
 * On Click handler for a row.
 * Parses the value from the row id and pass to the server.
 * @param event - browser event object.
 */
function rowClicked(event) {
	value = parseInt(event.currentTarget.id.split("_")[1]) + 1;
	advanceTheVoiceApp(value);
}
