/**
 * Vocollect, Inc.
 * 
 * @author jgeisler
 * 
 * For use with IE7 or IE8 browsers.
 * Will work with WebKit or Firefox, but is not optimized for those browsers.
 * 
 */


var globals = {
	// Default ping timer
	DEFAULT_TIMER : 350,
	// Maximum default wait time; usually time to wait when the server doesn't respond
	MAX_TIMER : 10000,
	//XUI-optimized touch event for touch screen devices; should be changes to "click" if you plan to use
	//the screen on non-touch screen devices; there are performance gains when using a touch screen device
	//and using the "touchstart" event.
	EVENT_NAME : "touchstart",
	LONG_POLLING : "false",
	FORWARD_HOME : "FORWARD_HOME",
	APP_HOME : "/",
	MANUALLY_CONTROL_REQUEST: false,
    LONG_POLL_ACTIVE: false
}

/**
 * Starts up the client side logic.
 * (starts fetching pages, data, etc)
 */
var startUp = function() {
	//Prevent caching of any .get requests:
	$.ajaxSetup({ cache: false });
	
	//Start the communications with the server
	comm_manager.get_current_page_name();
}


/**
 * Deals with UI stuff
 * (building tables, lists, general DOM manipulation)
 */
var ui_manager = {
	/**
	 * Given a list object and its identifier, this method
	 * creates a list of html elements, updates the appropriate 
	 * attributes of the objects into the list elements, and then
	 * adds it to the DOM.
	 * @param rows - list of json objects
	 * @param element_id - ID value of the HTML element
	 */
	build_list : function(rows, element_id) {
	    header = $("li.header")[0].cloneNode(true);
	    clickableList = $(header).hasClass("clickable");
	    list = $("#" + element_id)[0];
	    $(list).html(header);
	    for (num in rows) {
	        obj = rows[num];
	        li = $(header.cloneNode(true)).removeClass("header").attr("id",element_id + "_" + num)[0];
	        for (attr in obj) {
	            elementByAttribute = $(li).find("." + attr)[0]
	            $(elementByAttribute).html(obj[attr].toString());
	            if (clickableList && $(elementByAttribute).hasClass("vocabToPost")) {
	            	value = obj[attr];
	            	$(li).bind(globals.EVENT_NAME, event_manager.row_clicked);
	            	$(li).attr("id",element_id + "_" + obj[attr].toString());
	            }
	        }
	        $(list).append(li);
	    }
	},
	/**
	 * Given a table object and its identifier, this method
	 * creates a table of html elements, updates the appropriate 
	 * attributes of the objects into the table elements, and then
	 * adds it to the DOM.
	 * @param tableObject - list of json objects
	 * @param tableId - ID value of the HTML element
	 */
	build_table : function (tableObject, tableId) { 
	    header = $("tr.header")[0].cloneNode(true);
	    clickable = $(header).hasClass("clickable");
	    table = $("#" + tableId)[0];
	    $(table).html(header);
	    for (num in tableObject) {
	        obj = tableObject[num];
	        tr = $(header.cloneNode(true)).removeClass("header").attr("id",tableId + "_" + num)[0];
	        if (obj['selected'] == 1) {
	            $(tr).addClass("selected");
	        }
	        for (attr in obj) {
	            elementByAttribute = $(tr).find("." + attr)[0]
	            $(elementByAttribute).html(obj[attr].toString());
	            if (clickable && $(elementByAttribute).hasClass("vocabToPost")) {
	            	value = obj[attr];
	            	$(tr).bind(globals.EVENT_NAME, event_manager.row_clicked);
	            	$(tr).attr("id",tableId + "_" + obj[attr].toString());
	            }
	        }
	        $(table).append(tr);
	    }
	},
	/**
	 * Takes the given object and either injects it into the appropriate
	 * HTML element, calls update list, or recursively passes the object
	 * back to this function.
	 * @param obj - the json object to be evaluated.
	 */
	load_response_into_dom : function(obj) {
	    for (attribute in obj) {
	        if (typeof obj[attribute] != 'object') {
	        	if (attribute == 'image') {
	        		d = new Date();
	        		$("#" + attribute).attr("src", obj[attribute] + "?date=" + d.getTime());
	        	} else {
	        		if ($("#" + attribute)[0] != null) {
	        			$("#" + attribute).html(obj[attribute].toString());
	        		}
	        	}
	        } else {
                // if it has an unordered list element
                if ($("ul#" + attribute)[0] != null) {
                	ui_manager.build_list(obj[attribute], attribute);
                } else if ($("table#" + attribute)[0] != null) {
                	ui_manager.build_table(obj[attribute], attribute);
	            } else {
	                // otherwise
	            	ui_manager.load_response_into_dom(obj[attribute]);
	            }
	        }
	    }
	},
	current_page : "",
	/**
	 * Shows a green indicator when connected to the device.
	 * Shows a red indicator when disconnected.
	 */
	display_connection_status : function(on) {
		if (on) {
			$("#connectionStatus").addClass("connected");
			$("#connectionStatus").removeClass("disconnected");
		} else {
			$("#connectionStatus").addClass("disconnected");
			$("#connectionStatus").removeClass("connected");
		}
	},
	/**
	 * Reviews the DOM for links and adds an event for clicking.
	 */
	add_link_events : function() {
		var links = $("a");
		for (var i=0; i < links.length; i++) {
			var element = links[i];
			$(element).unbind(globals.EVENT_NAME);
			var name = $(element).html().toLowerCase();
			$("#" + name + "_pic").bind(globals.EVENT_NAME, event_manager.link_clicked);
		}
	},
	/**
	 * Clean up any event bound elements.
	 */
	remove_link_events : function() {
		var links = $("#content a");
		for (var i=0; i < links.length; i++) {
			var element = links[i];
			$(element).unbind(globals.EVENT_NAME);
			var name = $(element).html().toLowerCase();
			$("#" + name + "_pic").unbind(globals.EVENT_NAME);
		}
		var rows = $("#content li");
		for (row in rows) {
			$(row).unbind(globals.EVENT_NAME);
		}
	},
	
	update_content : function(content_data) {
	    //Start Long polling if flag is still set, currently flag will 
	    //be turned off if image is being loaded. Long polling will automatically
	    //be restarted when image finishes
	    if (!globals.MANUALLY_CONTROL_REQUEST) {
	        setTimeout(comm_manager.get_current_page_name, comm_manager.timer);
	    }
	
		this.remove_link_events();
		$('#content').html(content_data);
		// Update any links to be click-able
		this.add_link_events();
	}
}

/**
 * Manages events fired in the browser.
 */
var event_manager = {
    /**
     * Event method for clicking a row in a list that is clickable.
     * @param event - the click event.
     */
	row_clicked : function(event) {
		value = parseInt(event.currentTarget.id.split("_")[1]);
		comm_manager.advance_application(value);
	},
	/**
	 * Event method for clicking a link.
	 * @param event - the browser event object
	 */
	link_clicked : function(event) {
		vocab = (event.currentTarget.id).split("_")[0];
		comm_manager.advance_application(vocab);
	},
	/**
	 * The display is in contact with host.
	 */
	connected : function() {
		ui_manager.display_connection_status(true);
		comm_manager.timer = globals.DEFAULT_TIMER;
	},
	/**
	 * The display is no longer in contact with host.
	 */
	disconnected : function() {
		ui_manager.display_connection_status(false);
		comm_manager.timer = globals.MAX_TIMER;
	},
	/**
	 * Locking function. Frees up the browser to update the page
	 * as soon as the application push is complete.
	 */
	pushed_the_app : function() {
		comm_manager.push_in_progress = false;
		if (comm_manager.on_hold) {
			comm_manager.on_hold = false;
			// If we're long polling, no need to check the current page
			if (!globals.LONG_POLLING)
				comm_manager.get_current_page_name();
		}
	}
}

/**
 * Deals with interaction between this client and the server.
 * Fires off calls and handles the response.
 */
var comm_manager = {
	/**
	 * Used to push the application from the browser.
	 * @param vocab - the value to be sent (as if the user spoke it)
	 */
	advance_application : function(vocab) {
		this.push_in_progress = true;
		$.post("advance/" + vocab, event_manager.pushed_the_app);
	},
	/**
	 * GET request to determine the current page to be displayed.
	 */
	get_current_page_name : function() {
		// "html" type was added to this call to fix a bug in which Firefox
		// attempted to treat the response data as XML. -ddoubleday
		//Reset long poll start to true
		globals.MANUALLY_CONTROL_REQUEST = false;
		if (!globals.LONG_POLL_ACTIVE) {
			globals.LONG_POLL_ACTIVE = true;
			$.get('html/' + ui_manager.current_page, comm_manager.handle_page_response, "html")
				.error(function() {
					// An error has occurred. Try again in a bit.
					globals.LONG_POLL_ACTIVE = false;
					setTimeout(comm_manager.get_current_page_name, globals.MAX_TIMER);
				});
        }
	},
	/**
	 * GET request to retrieve the page data from the host
	 */
	fetch_current_data : function() {
		$.getJSON('data/' + ui_manager.current_page, comm_manager.handle_data_response);
	},
	/**
	 * GET request to retrieve the page HTML from the host
	 */
	fetch_current_page : function() {
    	$('#content').load(ui_manager.current_page + '.html',
    						comm_manager.handle_page_content_response);
	},
	/**
	 * Lock for when the voice app is advanced
	 * by the display.
	 */
	push_in_progress : false,
	/**
	 * Lock for placing the GET data requests on hold.
	 */
	on_hold : true,
	/**
	 * Amount of time between page checks.
	 */
	timer : globals.DEFAULT_TIMER,
	/**
	 * Call back function for data request.
	 * Calls ui_manager.load_response_into_dom to inject returned data
	 * into the html elements.
	 */
	handle_data_response : function(return_data, status) {
		// Make sure we have an OK status
		if (status == "success") {
		    ui_manager.load_response_into_dom(return_data); 
		} else {
			event_manager.disconnected();
		}
		
	 },
	 /**
	  * Callback function : verifies the current page matches displayed page.
	  * If the page to be shown has changed, load the page, then 
	  * load the data.
	  */
	 handle_page_response : function(return_data, status) {
		
		globals.LONG_POLL_ACTIVE = false;
		
		// Make sure we have an OK status
		if (status == "success") {
			// If we just pushed the app to change, don't update
			if (this.push_in_progress) {
				this.on_hold = true;
				return;
			}
			
			if (return_data != "") {
				values = return_data.split('/');
				if (values[values.length - 1] == 'refresh') {
					if (globals.LONG_POLLING) {
						comm_manager.fetch_current_data();
					} else {
						page_name = return_data.split('/refresh')[0];
						ui_manager.current_page = "";
					}
				} else {
					page_name = return_data;
					
					/**
					 * If page_name is equal to this special phrase,
					 * forward the browser to the device's home page.
					 */
					if (page_name == globals.FORWARD_HOME){
						window.location = globals.APP_HOME;
					}
				}
				
			    if (ui_manager.current_page != page_name) {
			    	//page changed so long pooling let image loading 
			    	//control long pooling restart
			    	ui_manager.current_page = page_name;
			    	comm_manager.fetch_current_page();
			    } else {
			    	setTimeout(comm_manager.get_current_page_name, comm_manager.timer);
			    }		    
			}
			event_manager.connected();
		} else {
			event_manager.disconnected();
		}
		
	},
	
	/**
	 * Plugs the response into the content page.
	 * Also runs addEventsForLinks, to setup any links in the page.
	 */
	handle_page_content_response : function(return_data, status) {
		if (status == "success") {
			// Plug the response into the content area
			ui_manager.update_content(return_data);
			// Get new data if it exists
			comm_manager.fetch_current_data();
		}
	}
}


/* Add your images to be cached to IMAGE_CACHE for any
 * particular page. Override this in a project-specific JS file or
 * HTML file.
 *
 * The images in the 'small' array will be cached
 * when the resolution is less than image_manager.BREAKPOINT
 * and the images in the 'medium' array when above that resolution
 * Ex:
 *
 *  var IMAGE_CACHE = { 'WELCOME_PAGE' : 
 *                           { 'small' : ['image1s.png','image2s.png'],
 *                             'medium' : ['image1m.png','image2m.png'] } }     
 */
	
var IMAGE_CACHE = {};
	
/**
 * Assistance with image cachine. 
 */	
var image_manager = {
    /**
     * Breakpoint between resolutions. Override to fit your needs.
     */
    BREAKPOINT : 572,
    /**
     * Default window size. Updated on JS load. 
     */
    size : 'small',
    /**
     * Method called from any particular page that needs caching.
     * 
     * @param this_page - the current page. images are loaded immediately.
     *                    hopefully pulling from the browser's cache
     * @param next_page - the next page's images. pulls images into
     *                    the browser's cache 
     * @param callback - called after this_page's images have all loaded
     * 
     * 
     */
    preload : function(this_page, next_page, callback) {
        if (this_page != null) {
            // NOTICE:
            // You cannot have an open poll and fetch images on iOS6 Safari.
            // You must turn  to TRUE so polling starts after image load.
            // Load this page from the cache
            globals.MANUALLY_CONTROL_REQUEST = true;
            image_manager.load_images(IMAGE_CACHE[this_page][image_manager.size], function() {
                callback();
                setTimeout(comm_manager.get_current_page_name, comm_manager.timer);
            });
        }
        
        if (next_page != null) {
            // Preload the next page into cache
            $(function() {
                image_manager.load_images(IMAGE_CACHE[next_page][image_manager.size]);
            });
        }
    },
    /**
     * For each image, preload it. When all images have loaded, execute the callback. 
     */
    load_images : function(images, callback) {
        var total = images.length;
        $(images).each(function() {
            $('<img>').load(function() {
                total = total - 1;
                if (total == 0 && callback != null) callback();
            }).attr({ src: this });
        })
    }
};

/**
 *  Sets the size value based on the window width and the breakpoint.
 */
image_manager.size = function() {
    if ($(window).width() > image_manager.BREAKPOINT) return 'medium';
    else return 'small';  
  }();
