
module("Setup tests");

test("default values should be set appropriately", function(){
	expect(5);
	
    // actual, expected
    equals(DEFAULT_TIMER, 350,
    		"The default timer value should be set to the correct default");
    equals(DEFAULT_SERIAL_TIMER, 500,
    		"The default serial timer value should be set to the correct default");
    equals(MAX_TIMER, 10000,
    		"The max timer value should be set to the correct default");
    
    var event_name = 'touchstart';
    if (!('ontouchstart' in document.documentElement)) {
    	event_name = 'click';
    }
    
    equals(EVENT_NAME, event_name,
    		"The event name value should be set to the correct default");
    equals(UPDATE_DATA, false,
    		"The update data value should be set to the correct default");
});

module("Test connection utility");

test("should be able to update the connection status class", function() {
	
	// Setup
	x$('#testContent').html(x$('#test_html').html());

	
	expect(4);
	updateConnection(true);
	
	ok(x$("#connectionStatus").hasClass("connected"),
			"Element should have class 'connected'");
	ok(!x$("#connectionStatus").hasClass("disconnected"),
			"Element should not have class 'disconnected'");
	
	updateConnection(false);

	ok(!x$("#connectionStatus").hasClass("connected"),
		"Element should not have class 'connected'");
	ok(x$("#connectionStatus").hasClass("disconnected"),
		"Element should have class 'disconnected'");
	
	// Teardown
	x$('#testContent').html("");
	
});

module("Test page_get callback funcitons");

test("should update the serial number from the response text", function() {
	
	// Setup
	x$('#testContent').html(x$('#test_html').html());

	
	expect(1);
	
	window.responseText = "123456";
	window.statusText = "success";
	
	getTheSerial();

	equal(x$('#serialnumber').html(), window.responseText,
			"Serial number should be updated with the reponse text");

	// Teardown
	x$('#testContent').html("");
});
