#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
# 
import threading
from threading import Thread
import netadapters
from vocollect_http.net import ipaddr
import socket
import voice.atunload

port = 15500


def get_bt_ip_address():
    ip_address = ""
    adapters = netadapters.get_adapters()
    for adapter in adapters:
        if "BTPAN1" in adapter:                
            ip_address = adapters[adapter]['ip_addresses'][0]['ip_address']
    
    return ip_address   

class UDPBroadcastThread(Thread):
    ''' Thread that UDP broadcasts the startup URL for the app so the
        display can find it after BT association.
    
    Extends: Thread
    
    Constructor parameters:
            root_uri - root_uri to append to the base URL for the app
            server_port - port that the HTTP server will be listening on.
                          defaults to empty, which means default HTTP port of 80.
    '''
    def __init__(self, root_uri, server_port=""):
        Thread.__init__(self)
        self.server_port = server_port if server_port == "" else ':' + server_port
        self.root_uri = root_uri
        self.running = True
        self.__shutdown_event = threading.Event()
        voice.atunload.register(self.shutdown)
        
    def shutdown(self):
        voice.log_message('SHUTDOWN: UDP Broadcast - start')
        self.running = False
        self.__shutdown_event.set()
        voice.log_message('SHUTDOWN: UDP Broadcast - end')
        
    def sendUDPBroadcast(self):
        
        found_the_adapter = False

        adapters = netadapters.get_adapters()
        
        for adapter in adapters:
            if "BTPAN1" in adapter:
                found_the_adapter = True

                try:
                    netmask = adapters[adapter]['ip_addresses'][0]['net_mask']
                    ip_address = adapters[adapter]['ip_addresses'][0]['ip_address']
                     
                    o = ipaddr.IPv4Network(ip_address  + '/' + netmask)
                
                    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                
                    url = "http://{0}{1}{2}".format(str(o.ip), self.server_port, self.root_uri)
                    
                    voice.log_message(url)
                    
                    s.sendto(url.encode(), 0, (str(o.broadcast), port))
                    s.close()
                    
                except Exception as e:
                    voice.log_message("Exception sending broadcast: " + str(e))
                    return False
        
        if not found_the_adapter:
            # Log the adapters that were found
            voice.log_message("Did not find BTPAN1, found " + str(adapters))    
                    
        return found_the_adapter
        
    def run(self):

        while(self.running):
            found_btpan = self.sendUDPBroadcast()
            if found_btpan:
                voice.log_message('BTPAN1 heartbeat')
                self.__shutdown_event.wait(5)
            else:
                voice.log_message('No BTPAN1 found, will try again') 
                self.__shutdown_event.wait(5)
            
        voice.log_message('SHUTDOWN: UDP Broadcast - thread end')

