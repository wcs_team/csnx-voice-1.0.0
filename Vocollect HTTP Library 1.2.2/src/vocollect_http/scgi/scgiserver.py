from http.client import HTTPConnection, HTTPMessage
from http.server import BaseHTTPRequestHandler
from socketserver import ThreadingTCPServer
import traceback

import vocollect_http.scgi.netstring as netstring
import voice
from voice import get_voice_application_property, getenv
from vocollect_python.email import parser

APP_SERVER = ('127.0.0.1', getenv('WebServer.Port'))

class SCGIConfigurationInterface:
    ''' Provides an interface to the SCGI registration API in Mongoose. '''
    
    APP_URL = '/app'
    ICON_URL = get_voice_application_property('applicationURI') + get_voice_application_property('applicationIcon')
    DYNAMIC_REGISTER_URL = APP_URL + '/dynamic/register'
    STATIC_REGISTER_URL = APP_URL + '/static/register'
    REGISTER_DEFAULT_URL = APP_URL + '/default/register'

    DYNAMIC_REGISTER_FORMAT = 'URL={url}\nGUI={gui}\nDisplayName={name}\nServer={server}\niconurl={iconurl}\n'
    STATIC_REGISTER_FORMAT = 'URL={url}\nGUI={gui}\nDisplayName={name}\nDirectory={path}\n'
    REGISTER_DEFAULT_FORMAT = 'APPID={appid}\n'
    
    def __init__(self, server_url):
        self.server_url = server_url
        
    def _connect_to_app_server(self):
        ''' Establish a connection to the Mongoose server '''
        return HTTPConnection(*self.server_url)
    
    def _post_registration(self, url, content):
        con = self._connect_to_app_server()
        con.request('POST', url, content)
        response = con.getresponse()
        body = response.read().decode('utf8')
        
        # In the event of an error, throw an exception.
        if response.status != 200:
            raise RuntimeError('SCGI Registration failed: {0}'.format(body))
     
        return body
    
    def register_scgi_provider(self, url, name, port, gui=True):
        ''' POSTs the registration request to the SCGI management server. '''
        content = self.DYNAMIC_REGISTER_FORMAT.format(url=url,
                                                      name=name,
                                                      gui=('yes' if gui else 'no'),
                                                      server='127.0.0.1:{0}'.format(port),
                                                      iconurl=self.ICON_URL)
        scgi_id = self._post_registration(self.DYNAMIC_REGISTER_URL, content)     
        voice.log_message('Registered SCGI provider {0} at port {1} with ID {2}', name, port, scgi_id)
        return scgi_id
    
    def register_static_content(self, url, name, path, gui=True):
        content = self.STATIC_REGISTER_FORMAT.format(url=url,
                                                     name=name,
                                                     gui=('yes' if gui else 'no'),
                                                     path=path)
        
        static_id = self._post_registration(self.STATIC_REGISTER_URL, content)
        voice.log_message('Registered static content {0} at path {1} with ID {2}', name, path, static_id)
        return static_id        
    
    def register_default_provider(self, app_id):
        content = self.REGISTER_DEFAULT_FORMAT.format(appid=app_id)
        self._post_registration(self.REGISTER_DEFAULT_URL, content)
        voice.log_message('Registered default provider {0}', app_id)

    def unregister_scgi_provider(self, app_id):
        ''' DELETEs the requested app_id from the SCGI management server. '''
        con = self._connect_to_app_server()
        con.request('DELETE', app_id)
        voice.log_message('SCGI: Unregistered ID {0}', app_id)
        

class BaseSCGIRequestHandler(BaseHTTPRequestHandler):
    '''Wrapper to allow SCGI requests to be treated like HTTP.'''
    
    def parse_request(self):
        ''' Parse request information from the SCGI headers and place
        it in the appropriate member variables. '''
        self.env = netstring.read_env(self.rfile)
        
        self.command = self.env['REQUEST_METHOD']
        self.path = self.env['REQUEST_URI']
        
        # This is a fix for VSCVVALIB-19
        self.client_address = (self.env['REMOTE_ADDR'], self.env['REMOTE_PORT'])
        
        self.request_version = ['SERVER_PROTOCOL']
        self.requestline = 'SCGI Request'

        # Fix for VSCVALIB-14 -ddoubleday 
        self.headers = self._parse_headers_from_scgi_env()
       
        # According to the SCGI spec, connections should always be closed.
        # (No HTTP keep-alive equivalent)
        self.close_connection = 1
        
        return True
                
    
    def handle_one_request(self):
        ''' Handle an SCGI request by parsing the request and handing it
        off to the appropriate do_{GET, POST, etc.} method. ''' 
        voice.log_message('Received request.')
        try:
            self.parse_request()
            mname = 'do_' + self.command
            method = getattr(self, mname)
            voice.log_message('Trying to call ' + mname)
            method()      
        except Exception as ex:
            # Exceptions from threads don't get logged automatically, so leave
            # a breadcrumb here in case no one is handling this.
            voice.log_message('Exception handling SCGI request: ' + str(ex))
            voice.log_message(traceback.format_exc())
            raise

    def _parse_headers_from_scgi_env(self, _class=HTTPMessage):
        """Parses only RFC2822 headers from the SCGI env variable.
        """
        headers = []
        for envkey in self.env.keys():
            if envkey.startswith("HTTP_"):
                http_name = envkey[5:].lower().title().replace('_', '-')
                headers.append(http_name + ': ' + self.env[envkey])
        hstring = '\n'.join(headers)
        return parser.Parser(_class=_class).parsestr(hstring)
        

class ThreadingSCGIServer(ThreadingTCPServer):
    ''' Provides support for registering/unregistering the SCGI endpoint. '''
    
    def __init__(self, server_addr, handler, uri, name):
        super().__init__(server_addr, handler)
        self.application_uri = uri
        self.name = name
        
    def serve_forever(self, *args, **kwargs):
        ''' Wrapper around the base implementation of serve_forver to manage
        SCGI registration lifetime.
        
        Registration is performed just before opening the socket and deleted
        when the server is shut down.
        '''
        app_id = None
        scgi_manager = SCGIConfigurationInterface(APP_SERVER)
        
        try:
            app_id = scgi_manager.register_scgi_provider(self.application_uri,
                                                         self.name,
                                                         self.server_address[1])

            voice.log_message(self.name + ' registered.')

            # In this single content provider implementation, always make the app the default.
            scgi_manager.register_default_provider(app_id)
            
            voice.log_message(self.name + ' set as the default content provider.')
                
            super().serve_forever(*args, **kwargs)
        except Exception as ex:
            # Exceptions from threads don't get logged automatically, so leave
            # a breadcrumb here in case no one is handling this.
            voice.log_message('Exception trying to start SCGI server: ' + str(ex))
            voice.log_message(traceback.format_exc())
            raise
        finally:
            if app_id is not None:
                scgi_manager.unregister_scgi_provider(app_id)
                   
    
