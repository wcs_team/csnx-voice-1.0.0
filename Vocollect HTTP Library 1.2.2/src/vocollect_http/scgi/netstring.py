# SCGI netstring parsing code courtesy of the python SCGI library
# available at: http://pypi.python.org/pypi/scgi/1.14

# Minor changes for python3 compatibility.
def ns_read_size(input_str):
    size = b""
    while 1:
        c = input_str.read(1)
        if c == b':':
            break
        elif not c:
            raise IOError('short netstring read')
        size = size + c
    return int(size)

def ns_reads(input_str):
    size = ns_read_size(input_str)
    data = b""
    while size > 0:
        s = input_str.read(size)
        if not s:
            raise IOError('short netstring read')
        data = data + s
        size -= len(s)
    if input_str.read(1) != b',':
        raise IOError('missing netstring terminator')
    return data

def read_env(input_str):
    headers = ns_reads(input_str)
    items = headers.split(b"\0")
    items = items[:-1]
    assert len(items) % 2 == 0, "malformed headers"
    env = {}
    for i in range(0, len(items), 2):
        env[str(items[i], 'utf8')] = str(items[i+1], 'utf8')
    return env