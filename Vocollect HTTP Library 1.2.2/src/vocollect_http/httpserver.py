#
# Copyright (c) 2009-2012 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
# 

import voice #@UnusedImport
import voice.atunload
from voice import log_message
from voice import get_voice_application_property
from voice import getenv

from vocollect_http.net.udpbroadcast import UDPBroadcastThread
from vocollect_http.scgi.scgiserver import ThreadingSCGIServer, BaseSCGIRequestHandler
from vocollect_python import cgi

from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import threading
from threading import Thread

from vocollect_core.dialog.ready_prompt import ReadyPrompt
from vocollect_core.dialog.list_prompt import ListPrompt
from vocollect_core.dialog.digits_prompt import DigitsPrompt
from vocollect_core.task.task_runner import TaskRunnerBase
from vocollect_core.utilities.localization import itext
from vocollect_core.utilities import class_factory, obj_factory


APP_NAME = get_voice_application_property('applicationName')
APP_URI = get_voice_application_property('applicationURI')
# Make sure it ends with a slash if it doesn't already.
if APP_URI and not APP_URI.endswith('/'):
    APP_URI += '/'

HOME_PAGE_FILE_NAME = 'index.html'
HOME_PAGE_PATH = APP_URI + HOME_PAGE_FILE_NAME
PAGE_REQUEST_PATH = APP_URI + 'html'
DATA_REQUEST_PATH = APP_URI + 'data'
STATUS_REQUEST_PATH = APP_URI + 'status'
POST_VOCAB_PATH = APP_URI + 'advance'

DEFAULT_PAGE = 'loading_page'

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    '''
        A multi-threaded HTTP server. This class only exists
        to provide a multiple inheritance point that combines
        HTTPServer and ThreadingMixIn behaviors.
    '''
    
    # This is a boolean used for when long polling requests
    # are released due to a shutdown.
    display_shutdown = False
    
    # This is a conditional lock that request threads acquire
    # when they want to wait until the page changes.
    display_condition = threading.Condition()
    
    def __init__(self, server_addr, handler, uri, name):
        super().__init__(server_addr, handler)

# Check getenv to see if Mongoose is running.
useSCGI = getenv('WebServer.Available', False) == 'true'
log_message("Use SCGI: " + str(useSCGI))

if useSCGI:
    class_factory.set_override(BaseHTTPRequestHandler, BaseSCGIRequestHandler)
    obj_factory.set_override(ThreadedHTTPServer, ThreadingSCGIServer)


class VoiceAppHTTPServerThread(Thread):
    '''
        Custom Thread that is created on each HTTP request.
    '''
    def __init__(self, port, handler, uri, name):

        '''
            Thread initialization.
            
            @param port: server port
            @param handler: the request handler.
        '''
        Thread.__init__(self)
        self.port = port
        self.handler = handler
        self.uri = uri
        self.name = name
        self.server = None
        
        #make this a daemon thread
        self.setDaemon(True)
        
        #register shutdown function
        voice.atunload.register(self.shutdown)
        
    def run(self):
        '''
            Main thread activity.
        '''
        try:
            self.server = obj_factory.get(ThreadedHTTPServer,
                                          ('', self.port), 
                                          self.handler,
                                          self.uri,
                                          self.name)
            voice.log_message('HTTP: server started at port ' + str(self.port))
            self.server.serve_forever(0.5)
        except KeyboardInterrupt:
            pass
            
        self.server.server_close()
        voice.log_message('SHUTDOWN: http server - end thread')

    def shutdown(self):
        '''
            Handle shutdown, releasing all blocked client threads and 
            terminating the server
        '''
        voice.log_message('SHUTDOWN: http server - start releasing long polls')
        ThreadedHTTPServer.display_shutdown = True
        ThreadedHTTPServer.display_condition.acquire()
        ThreadedHTTPServer.display_condition.notify_all()
        ThreadedHTTPServer.display_condition.release()
        voice.log_message('SHUTDOWN: http server - long polls released')
        
        voice.log_message('SHUTDOWN: http server - start shutdown')
        self.server.shutdown()
        voice.log_message('SHUTDOWN: http server - shutdown complete')


class VoiceAppHTTPServer(class_factory.get(BaseHTTPRequestHandler)):
    '''
        This extension of BaseHTTPRequestHandler handles
        requests that respect URI conventions defined
        by Vocollect as part of the web app framework
        bundled with this library.
        
        It is structured so that you can override this
        default behavior, but mostly you won't have to,
        if you follow the conventions.
        
        Note: This probably should have been named
        VoiceAppRequestHandler, but we are leaving it
        as is for now for backward compatibility.
    '''
    
    def log_message(self, msg_format, *args):
        """Log an arbitrary message to the voice debug log

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, msg_format, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client host and current date/time are prefixed to
        every message.

        """
        
        # This avoids doing a host name lookup (could hang)
        
        voice.log_message("HTTP: [%s]  %s\n" %
                          (self.client_address[:2][0],
                           msg_format%args))

    #----------------------------------------------------------

    def do_GET(self):
        '''
            Handle HTTP GET requests. This default implementation
            handles generic home page loading and AJAX commmunications
            as defined by the Vocollect HTTP Library conventions.
            
            To provide additional handlers of your own, override the
            handle_all_other_get_requests() method.
            
        '''
        
        # Ignore initial empty string, e.g.
        # /this/is/a/url --> ['', 'this', 'is', 'a', 'url']
        split_url = self.path.split('/')
        url = split_url[1];
        
        # APP_URI expects initial requests to include the trailing slash. 
        # Redirect if they don't have it.
        if (len(split_url) == 2):
            self.send_response(301)
            self.send_header('Location', '/' + url + '/') 
            return    
        
        # Capture the non-query-string portion of the path for
        # use throughout the GET request.
        self.base_path = str(self.path).split('?')[0]
        
        # The client is requesting the HTML state from the task object
        if self.base_path.startswith(PAGE_REQUEST_PATH):
            self.handle_page_request()
            
        # The client is requesting data from the app to fill in the HTML
        elif self.base_path.startswith(DATA_REQUEST_PATH):
            self.handle_data_request()
        
        # A HTTP client is requesting the status of the application
        elif self.base_path.startswith(STATUS_REQUEST_PATH):
            self.handle_status_request()
            
        # The client is requesting the home page of the app. This looks
        # for the APP_URI or index page path.
        elif (self.base_path == APP_URI or self.base_path == HOME_PAGE_PATH):
            self.handle_home_page_request()
                
        else:
            self.handle_all_other_get_requests()    
    
    #----------------------------------------------------------            
    def get_all_other_resources(self, file_obj):
        '''
            Default handler for get_resource(). It assumes
            that the returned resource is of type text/plain,
            UTF-8 encoded.
            
            Override this method to add additional handlers before
            defaulting to text/plain.
            
            @param file_obj the file object to read from.
            @return a duple of content type and the content
            to be written. 
        '''
        content_type = 'text/plain'
        content = file_obj.read().encode('utf-8')
        return content_type, content

    #----------------------------------------------------------

    def get_resource(self, file_name):
        ''' 
            Writes out the resource for return through HTTP to the client.
            Applies content-type headers as necessary.
            
            Override get_all_other_resources() to add additional
            resource handlers.
            
            @param file_name: the name of the file to return, relative
            to the VAD resource root. 
        '''
        
        self.log_message("get resource %s ", file_name)

        try:
            if (self._resource_is_binary(file_name)):
                file_obj = voice.open_vad_resource(file_name, mode='rb')
            else:
                file_obj = voice.open_vad_resource(file_name)
        
            if file_name.endswith('.html'):
                content_type = 'text/html'
                write_obj = translate_html(file_obj.read()).encode('utf-8')
            elif file_name.endswith('.js'):
                content_type = 'application/javascript'
                write_obj = file_obj.read().encode('utf-8')
            elif file_name.endswith('.css'):
                content_type = 'text/css'
                write_obj = file_obj.read().encode('utf-8')
            elif file_name.endswith('.png'):
                content_type = 'image/png'
                write_obj = file_obj.read()
            elif file_name.endswith('.jpg'):
                content_type = 'image/jpg'
                write_obj = file_obj.read()
            elif file_name.endswith('.gif'):
                content_type = 'image/gif'
                write_obj = file_obj.read()
            elif file_name.endswith('.ico'):
                content_type = 'image/x-icon'
                write_obj = file_obj.read()
            elif file_name.endswith('.apk'):
                content_type = 'application/vnd.android.package-archive apk'
                write_obj = file_obj.read()
            else:
                content_type, write_obj = self.get_all_other_resources(file_obj)
                
            self.send_response(200)            
            self.send_header('Content-type', content_type)
            self.end_headers()
            self.wfile.write(write_obj)
        except:
            self.log_message("Resource not found: %s", file_name)
            self.send_response(404, "not found")
            self.end_headers()
            
    def _resource_is_binary(self, file_name):
        '''
            The file is treated as binary if it has the following extension:
            png, jpg, gif, ico, apk 

            @return True if the file extension indicates the file
            should be treated as binary, False otherwise.
        '''
        return (file_name.endswith('.png') or file_name.endswith('.jpg') 
                or file_name.endswith('.gif') or file_name.endswith('.ico')
                or file_name.endswith('.apk'))
    
    #----------------------------------------------------------
    def write_response(self, return_string, mime_type='text/plain'):
        ''' Write the return_string back to the client.
            UTF-8 encoding is assumed
        '''
        
        self.send_response(200)
        self.send_header('Content-type', mime_type)
        self.send_nocache_headers()
        self.end_headers()
        self.wfile.write(str(return_string).encode('utf-8'))
        
    #---------------------------------------------------------
    def send_cache_headers(self):
        self.send_header( "Expires", "Thu, 15 Apr 2020 20:00:00 GMT" )
        
    #----------------------------------------------------------

    def send_nocache_headers(self):
        ''' Sets the appropriate headers on the response to notify
            the browser that the page should not be cached '''
            
        self.send_header( "Pragma", "no-cache" )
        self.send_header( "Cache-Control", "no-cache" )
        self.send_header( "Expires", 0 )

    #----------------------------------------------------------

    def handle_page_request(self):
        '''
            This function handles a request for the name of the current
            page in the application.
            
            This function will block the request thread if the 
            current page has not changed since the last request.
            This is a "long polling" technique that reduces
            server overhead.
            
        '''
        
        # Just a pass-through to the original non-member function
        # included in the 1.1.0 release.
        self.write_response(handle_page_request(
            self.base_path, TaskRunnerBase.get_main_runner(), self.get_default_page_name()))

    #----------------------------------------------------------

    def handle_home_page_request(self):
        '''
            Write the application home page to the client.
        '''
        try:
            file_obj = voice.open_vad_resource(HOME_PAGE_FILE_NAME)
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            # Make sure the startup page isn't cached.
            self.send_nocache_headers()
            self.end_headers()
            self.wfile.write(file_obj.read().encode('utf-8'))
        except:
            self.send_response(404, "not found")
            self.end_headers()


    #----------------------------------------------------------

    def handle_data_request(self):
        '''
            Handle a request to return the data for the current page.
            
            The framework is expecting you to provide a a JSON map
            object populated with keys that correspond to names in
            your HTML. The client-side framework will populate the
            data according to framework conventions.
        '''
        page_name = str(self.base_path).split('/')[-1]
        if hasattr(TaskRunnerBase.get_main_runner().get_current_task(), 'get_data_map'):
            data = TaskRunnerBase.get_main_runner().get_current_task().get_data_map(page_name)
            data["serialnumber"] = get_serial_number()
            self.write_response(str(data).replace("'", "\""), 'text/javascript')
        else:
            data = {}
            data["serialnumber"] = get_serial_number()
            data["title"] = ""
            self.write_response(str(data).replace("'", "\""), 'text/javascript')

    #----------------------------------------------------------
    def handle_status_request(self):
        '''
            Handle a request to learn the status of a voice application.
            
            No framework use at this time. The current assumption is that
            a JSON map object populated with interesting status and progress
            information is returned.
        '''
        
        if hasattr(TaskRunnerBase.get_main_runner().get_current_task(), 'progress'):
            data = TaskRunnerBase.get_main_runner().get_current_task().progress()
        else:
            data = {}
            
        self.write_response(str(data).replace("'", "\""), 'text/javascript')
                        

    #----------------------------------------------------------

    def handle_all_other_get_requests(self):
        '''
            The default GET request handler. The default implementation
            is to return the resource requested.
            
            Override this method to provide additional request handling.
        '''
        self.get_resource(self.base_path[len(APP_URI):])


    #----------------------------------------------------------

    def get_default_page_name(self):
        '''
            The name of the default page for the app, when the
            app does not define a current page name.
            
            The default implementation returns 'loading_page'
            
            Override this to define a different default page.
        '''
        return DEFAULT_PAGE

    #----------------------------------------------------------            

    def _parsePostRequest(self):
        '''
            Parse the POST request into a FieldStorage object.
            @return the FieldStorage object, which acts like 
                a Python dictionary.
        '''
        return cgi.FieldStorage(fp=self.rfile, 
                                headers=self.headers, 
                                environ={'REQUEST_METHOD':'POST', 
                                         'CONTENT_TYPE':self.headers['Content-Type']})


    #----------------------------------------------------------            

    def do_POST(self):
        '''
            Handle HTTP POST requests.
            
            The default implementation first attempts to 
            post a response to the current voice dialog, 
            if there is one. 
            
            If the post request is not recognized by 
            post_to_dialog(), then
            handle_all_other_post_requests() is called.
            That is where you will most likely override
            to add your own POST responses.
            
            This wil log an error and return a 404 response 
            to the client if not matching handler was found. 
        '''

        self.base_path = str(self.path)

        # Parse the form.
        form = self._parsePostRequest()
        
        # Attempt to post to the current dialog
        done = self.post_to_dialog(form)
        
        if not done:
            # Handle other post attempts.
            done = self.handle_all_other_post_requests(form)
        
        if not done:
            self.send_response(404, "POST did not find a matching handler")
            self.log_message("Unable to find matching POST handler")  
              
        self.end_headers()
 
    #----------------------------------------------------------            

    def post_to_dialog(self, form):
        '''
            Post to the current voice dialog. This is 
            abstracted as a separate function to make it
            easier to override.
            
            @param form: the parsed HTML Form (cgi.FieldStorage) object
            @return True if the request was handled, False otherwise.
            
            Does nothing but log a message if there is no
            current dialog to post to.
        '''
        
        done = False
        
        dlg = voice.current_dialog()
        
        if dlg is not None:
            
            done = self.post_vocab_to_dialog(dlg);
            
            if not done:
                # This was not a vocab post, try other things
                
                done = self.post_digits_to_dialog(dlg, form)
                if not done:
                    done = self.post_list_selection_to_dialog(dlg, form)
                if not done:
                    done = self.post_ready_to_dialog(dlg, form)
        else:
            self.log_message('No dialog to post to')

        return done
    
    #----------------------------------------------------------            

    def post_vocab_to_dialog(self, dlg):
        '''
            Handle a POST of a Vocabulary response via the URI path.
            This does not redirect after POST, it just returns
            the 200 response to the client if it handled the request.
            
            @param dlg: the current voice dialog
            @return True if this handled it, False if this was not
                a vocab post.
        '''
        if self.path.startswith(POST_VOCAB_PATH):
            clicked_vocab = str(self.path).split('/')[-1]
            self.log_message('\nposted: %s %s \n', clicked_vocab, dlg.__class__.__name__)
            dlg.http_post(clicked_vocab)
            self.send_response(200)
            return True
        else:
            return False


    #----------------------------------------------------------            

    def post_digits_to_dialog(self, dlg, form):
        '''
            Handle a POST of digits. The default implementation
            will actually only post the digits if the current
            dialog is an instance of DigitsPrompt class. This is
            to avoid accidentally posting the response to a dialog
            that isn't expecting it, e.g. if the screen has gotten
            out of sync with the dialog.
            
            @param dlg: the current voice dialog
            @param form: the parsed HTML Form (cgi.FieldStorage) object
            @return True if this handled it, False if this was not
                a digits post.
        '''
        if 'digits' in form:
            if isinstance(dlg, DigitsPrompt):
                dlg.http_post(form['digits'].value)
                self.log_message('Posted Digits: %s', form['digits'].value)
            else:
                self.log_message('Digits %s posted to non-DigitsPrompt dialog %s', 
                            form['digits'].value, dlg.__class__.__name__)
            self._send_redirect_response()
            return True
        else:
            return False


    #----------------------------------------------------------            

    def post_list_selection_to_dialog(self, dlg, form):
        '''
            Handle a POST of selected list item. The default implementation
            will actually only post the selection if the current
            dialog is an instance of ListPrompt class. This is
            to avoid accidentally posting the response to a dialog
            that isn't expecting it, e.g. if the screen has gotten
            out of sync with the dialog.
            
            @param dlg: the current voice dialog
            @param form: the parsed HTML Form (cgi.FieldStorage) object
            @return True if this handled it, False if this was not
                a list item post.
        '''
        if 'selectedItem' in form:
            if isinstance(dlg, ListPrompt):
                dlg.http_post(form['selectedItem'].value)
                self.log_message('Posted selectedItem: %s', form['selectedItem'].value)
            else:
                self.log_message('SelectedItem %s posted to non-ListPrompt dialog %s',
                            form['selectedItem'].value, dlg.__class__.__name__)
            self._send_redirect_response()
            return True
        else:
            return False


    #----------------------------------------------------------            

    def post_ready_to_dialog(self, dlg, form):
        '''
            Handle a POST of a Ready response. The default implementation
            will actually only post the selection if the current
            dialog is an instance of ReadyPrompt class. This is
            to avoid accidentally posting the response to a dialog
            that isn't expecting it, e.g. if the screen has gotten
            out of sync with the dialog.
            
            @param dlg: the current voice dialog
            @param form: the parsed HTML Form (cgi.FieldStorage) object
            @return True if this handled it, False if this was not
                a Ready item post.
        '''
        if 'ReadySubmit' in form:
            if isinstance(dlg, ReadyPrompt):
                dlg.http_post(form['ReadySubmit'].value)
                self.log_message('Posted Ready response: %s', form['ReadySubmit'].value)
            else:
                self.log_message('Ready response %s posted to non-ReadyPrompt dialog %s',
                            form['ReadySubmit'].value, dlg.__class__.__name__)
            self._send_redirect_response()
            return True
        else:
            return False


    #----------------------------------------------------------            

    def handle_all_other_post_requests(self, form):
        '''
            Handle a POST of any other response. The default implementation
            just returns a 404 (Not Found) response. 
            
            Override this
            if you want to provide additional behaviors not handled
            by the framework.
            
            @param form: the HTTP form object
            @return True after sending 404 response.
        '''

        self.send_response(404, "POST did not find a matching handler")
        return True

    def _send_redirect_response(self):
        '''
            Called after form post to dialog, so the app
            redisplays the current page after the POST.
            
            If you don't want redirect behavior (e.g. when
            using Javascript to submit the form), override this
            to just send the 200 response.
        '''
        self.send_response(301)
        self.send_header('Location', '/')

#-----------------------------------------
# Convenience functions
#-----------------------------------------

def display_page_changed():
    ''' 
        Notifies any long-polling clients that the page
        has changed, releasing their locks.        
    '''
    ThreadedHTTPServer.display_condition.acquire()
    ThreadedHTTPServer.display_condition.notify_all() 
    ThreadedHTTPServer.display_condition.release()

def set_display_page(task, page_name):
    ''' 
        Sets the current HTML page for the specified task and
        notifies any long-polling clients that the page
        has changed, releasing their locks.
        
        @param task: the Task to set the page attribute for. If this
        is None, then this simply acts as a lock release.
        @param page_name: the name of the new page. If this is
        None, then the page is not changed, we assume that the
        data for the page has changed, and this acts as a lock
        release so clients can refresh the data in the page.
        
    '''
    if task and page_name:
        task.page = task.name + "/" + page_name
    display_page_changed()

#-----------------------------------------
def handle_page_request(path, taskrunnerbase, default_page_name):
    '''
        This function handles a request for the name of the current
        page in the application.
        
        This function will block the request thread if the 
        current page has not changed since the last request.
        This is a "long polling" technique that reduces
        server overhead.
        
        @param path: the URI path being handled
        @param taskrunnerbase: the TaskRunnerBase main task runner
        @param default_page_name: the default page to return if
        no current task page is found.
        @return the name of the page the client should get.
         
        @deprecated: This function is left for backward compatibility
        with the 1.1.0 release. However, the preferred method is
        the member function of the VoiceAppHTTPServer. Override that
        method to change this behavior. 
    '''
    last_page_name = str(path).split('html/')[-1]

    voice.log_message("HTTP: handle page request %s" % (ThreadedHTTPServer.display_shutdown))

    # the client has no idea about which page to display
    if last_page_name == "":
        page_name = find_page_name(taskrunnerbase, default_page_name)
        
    # let's compare pages and return when the page is different
    else:
        page_name = last_page_name
        
        # using a Condition to wait for page change
        ThreadedHTTPServer.display_condition.acquire()
        
        try:
            # if the Condition pops out, verify that the page name has changed
            while last_page_name == page_name and not ThreadedHTTPServer.display_shutdown:
                # task will notify upon page value set
                voice.log_message("HTTP: Requesting Wait")
                ThreadedHTTPServer.display_condition.wait()
                voice.log_message("HTTP: Released from wait")
                # shutdown situation needs to forward to app home
                if ThreadedHTTPServer.display_shutdown:
                    voice.log_message("LONG POLL EVAL: shutting down")
                    return 'FORWARD_HOME'
                else:
                    page_name = find_page_name(taskrunnerbase, default_page_name)
    
                # If the lock was released but the page name, didn't
                # change, it means the client needs fresh data
                # on the page it already has.
                if last_page_name == page_name:
                    page_name += "/refresh"
                    break
                
        except Exception as err:
            voice.log_message("HTTP: " + str(err))
        finally:
            ThreadedHTTPServer.display_condition.release()
        
    voice.log_message("HTTP: next page is %s" % (page_name))
    return page_name

#-----------------------------------------
def find_page_name(taskrunnerbase, default_page_name):
    '''
        @return the page name associated with the current task, or
        return the default page if there is none
        
        @param taskrunnerbase: The main runner
        @param default_page_name: the fallback default page if no
            current page is found.
    '''
    current_task = taskrunnerbase.get_main_runner().get_current_task()
    if current_task != None and hasattr(current_task, 'page'):
        page_name = current_task.page
    else:
        page_name = default_page_name
    
    return page_name


#-----------------------------------------
def get_serial_number():
    ''' 
        @return the device's id from the environment, or the
        empty string if no device ID is available.
    '''
    return voice.getenv('Device.Id', '')

#-----------------------------------------
def server_startup(request_handler_class, application_uri = APP_URI, application_name = APP_NAME):
    ''' 
        Starts up the HTTP Server (if configured) and 
        UDP Broadcast (if separately configured)
        
        Server configuration is controlled by the following
        project properties:
        
            useHTTPServer: true or false
            
            httpServerPort: the port the server should listen on
            
            sendUDPBroadcast: true or false. If true, this will start
                a thread that broadcasts our address over Bluetooth
                so a Vocollect Pidion display device can connect 
                automatically.
                
        @param request_handler_class: the class that handles
            HTTP requests (must be BaseHTTPRequestHandler or 
            a subclass of it.)
        @param application_uri: the root URI for the application
            (default: the value of project property 'applicationURI'.
    '''
    
    # Section added to allow for backward compatibility (reverses
    # the order of the parameters.)
    if type(request_handler_class) == str:
        #reverse parameters
        temp = application_uri
        application_uri = request_handler_class
        request_handler_class = temp
        
    # Default name to uri if none was provided
    if application_name is None:
        application_name = application_uri        
    
    # setup the HTTPServer if applicable
    useHTTP = get_voice_application_property('useHTTPServer') == 'true'
    log_message("HTTP: UseHTTPServer: " + str(useHTTP))
    
    # server port is used for SCGI interaction
    if useSCGI:
        server_port = get_voice_application_property('scgiPort')
    else:
        server_port = get_voice_application_property('httpServerPort')    

    if useHTTP:
        server = VoiceAppHTTPServerThread(int(server_port),
                                          request_handler_class,
                                          application_uri,
                                          application_name)
        server.start()

    #send UDPBroadcast if applicable
    sendUDPBroadcast = get_voice_application_property('sendUDPBroadcast') == 'true'        
    log_message("HTTP: sendUDPBroadcast: " + str(sendUDPBroadcast))
    
    # Ignore sendUDPBroadcast if using SCGI server to avoid conflicting broadcast.
    if sendUDPBroadcast and not useSCGI:
        broadcaster = UDPBroadcastThread(application_uri, server_port)
        broadcaster.start()

#-----------------------------------------
def translate_html(contents):
    '''
        Search the contents for instances of calls to the itext()
        localization method. Evaluate each function call and replace
        it with the result.
         
        @param contents: a String containing HTML text.
        
        @note: This only supports simple itext() key replacement--no
        dynamic arguments are supported at this time.
        
        @return the contents, possibly modified by the replacement
        of itext() function calls with the text appropriate to the
        current locale.
    '''
    pos = 0
    while pos >= 0:
        pos = contents.find('itext(', pos)
        if pos >= 0:
            end = contents.find(')',pos)
            key = contents[pos+6:end]
            value = itext(key)
            contents = contents.replace('itext(' + key + ')', value)
            pos = end

    pos = 0
    while pos >= 0:
        pos = contents.find('<eval>', pos)
        if pos >= 0:
            end = contents.find('</eval>')
            code = contents[pos+6:end]
            try:
                value = eval(code)
                contents = contents.replace('<eval>' + code + '</eval>', value)
            except Exception as e:
                print(e)
                voice.log_message('Evaluated contents throw an exception')
                voice.log_message(e)
            pos = end
            
    return contents
    
