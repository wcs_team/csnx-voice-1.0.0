#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
#important used in VoiceLink project as well, 
#if changes made make sure to copy to and update that
#project as well.
import os
import sys
import socket
import _thread
import traceback
import re
import time
from xml.etree.ElementTree import ElementTree

SHUT_DOWN = '<shutdown>'
LUT_SERVER = 'lut'
ODR_SERVER = 'odr'
BOTH_SERVERS = 'both'

######################################################################
#Test server object
######################################################################
class MockServer(object):

    """ Class to be used for debugging and unit testing """
    def __init__(self, 
                 use_std_in_out = False, 
                 lut_port=15008, 
                 odr_port=15009):
        self.clear()
        self._use_std_in_out = use_std_in_out
        self._lut_server = TestServer('127.0.0.1', lut_port, self)
        self._odr_server = TestServer('127.0.0.1', odr_port, self)

        #pass through settings
        self._pass_host = None
        self._pass_odr_port = None
        self._pass_lut_port = None
        
    def clear(self):
        self._server_requests = []
        self._server_responses_sent = []

        self._server_response_by_name = {}
        self._server_response_by_expression = []
        self._server_response_by_queue = []
        
        
    ######################################
    # Server Methods
    ######################################
    def set_pass_through_host(self, host, lut_port = None, odr_port = None):
        self._pass_host = host
        if lut_port is not None:
            self._pass_lut_port = lut_port
        else:
            self._pass_lut_port = self._lut_server._port
        if odr_port is not None:
            self._pass_odr_port = odr_port
        else:
            self._pass_odr_port = self._odr_server._port
            
    def set_server_response(self, response, request_name = None):
        """ add a server response by name or to ordered queue """
        if (request_name is None):
            self._server_response_by_queue.append(response)
        else:
            self._server_response_by_name[request_name] = response
    
    def load_server_responses(self, file):
        """ load a server data XML File order load to ordered queue """
        tree = ElementTree()
        root = tree.parse(file)
        responses = []
        if root.tag == "responseQueue":
            responses = root.findall("response")
        else:
            responseQueues = root.findall("responseQueue")
            for queue in responseQueues:
                responses.extend(queue.findall("response"))
        
        del self._server_response_by_expression[:]

        for response in responses:
            response_data = ""
            expression = response.attrib["expression"]
            eor = self._replace_eor(response.attrib["endOfRecord"])
            records = response.getiterator("record")
            file = response.getiterator("file")
            
            if (len(records) > 0):
                for rec in records:
                    response_data += rec.attrib["data"] + eor
                response_data = response_data + eor
            elif len(file) == 1:
                for rec in file:
                    fn = rec.attrib["name"]
                    fn = '\\' + fn.replace('/', '\\')
                    f = open(os.getcwd() + fn, 'r')
                    lines = f.readlines()
                    f.close()
                    for line in lines:
                        response_data += line + eor
                    response_data = response_data + eor
            else:
                print('raise error here')
            
            self._server_response_by_expression.append([re.compile(expression), response_data])

    def get_server_response(self, request, received_on):
        """ get response by name, if not found then get from queue """

        #first post the request being made
        self._server_requests.append(request)
        if self._use_std_in_out:
            output = request.replace('\n', '')
            output = output.replace('\r', '')
            sys.stdout.write('Server Request: ' + output + '\n')
            sys.stdout.flush()
        
        #try to get from pass through ports
        response = self._get_server_response_from_host(request, received_on)
        
        #try to find match in named responses
        if response is None:
            sys.stdout.flush()
            for key in list(self._server_response_by_name.keys()):
                if request.startswith(key):
                    response = self._server_response_by_name[key]
                    break
            
        #if no response try to find by expression
        if response is None and len(self._server_response_by_expression) > 0:
            for resp in self._server_response_by_expression:
                match = resp[0].match(request)
                if match is not None:
                    response = resp[1]
                    break 
            
        #if no match find send next queued response
        if response is None and len(self._server_response_by_queue) > 0:
            response = self._server_response_by_queue.pop(0)
        
        self._server_responses_sent.append(response)
        return response
    
    def _get_server_response_from_host(self, request, received_on):
        ''' pass through sockets to record information going between test run
        and an actual host server
        '''
        response = None
        
        #if received on ODR port then send to host ODR port (if defined)
        try:
            if received_on == self._odr_server._port:
                if self._pass_odr_port is not None:
                    s = socket.socket()
                    s.settimeout(20)
                    s.connect((self._pass_host, self._pass_odr_port))
                    s.sendall(request.encode('utf-8'))
                    temp = s.recv(512)
                    #only expecting 1 character
                    if len(temp) > 0:
                        response = temp.decode('utf-8')
                    s.close()
    
            #if received on LUT port then send to host LUT port (if defined)
            elif received_on == self._lut_server._port:
                if self._pass_lut_port is not None:
                    s = socket.socket()
                    s.settimeout(20)
                    s.connect((self._pass_host, self._pass_lut_port))
                    s.sendall(request.encode('utf-8'))
                    
                    #wait until all data received or end of recordset received
                    temp = b''
                    while True:
                        data = s.recv(1024)
                        if not data: break
                        temp = temp + data
                        temp2 = temp.decode('utf-8')
                        if temp2.endswith('\n\n'): break
                        if temp2.endswith('\r\r'): break
                        if temp2.endswith('\r\n\r\n'): break
                        
                    if len(temp) > 0:
                        response = temp.decode('utf-8')
                    s.close()
        except Exception as err:
            print('error contacting remote host', err)
                    
        return response
    
    def _replace_eor(self, data):
        if data == "\\n":
            eor = "\n"
        if data == "\\r":
            eor = "\r"
        if data == "\\r\\n":
            eor = "\r\n"
        return eor

    def get_server_request(self):
        """ pop the next received request from queue """
        if len(self._server_requests) > 0:
            return self._server_requests.pop(0)
        else:
            return None

    def start_server(self, server = LUT_SERVER):
        """ start the socket server """
        if server in [LUT_SERVER, BOTH_SERVERS]:
            self._lut_server.start_server();

        if server in [ODR_SERVER, BOTH_SERVERS]:
            self._odr_server.start_server();
        
    def stop_server(self, server = LUT_SERVER):
        """ stop the socket server """
        if server in [LUT_SERVER, BOTH_SERVERS]:
            self._lut_server.stop_server();
            
        if server in [ODR_SERVER, BOTH_SERVERS]:
            self._odr_server.stop_server();
            
######################################################################
#Test server object
######################################################################
class TestServer():
    ''' Test server for LUTs and ODRs '''
    def __init__(self, host, port, parent):
        #Configuration variables
        self.running = False
        
        self.server = None
        self._thread = None
        
        self._host = host
        self._port = port
        self._parent = parent
        
        self.response_eor = '\n'
        self.response_eof = '\n'
        self.request_eor = '\n'
        self.request_eof = '\n'
        
    def start_server(self):
        tries = 3
        self.running = False
        while tries > 0:
            try:
                '''Starts the specified (LUT/ODR/BOTH) listen server '''
                self.server = socket.socket()
                self.server.bind(((self._host, self._port)))
                self.server.listen(5)
                self.running = True
                self._thread = _thread.start_new_thread(self._run_server, ())
                tries = 0
            except Exception as err:
                tries -= 1
                if tries <= 0:
                    raise err
                time.sleep(1.0)
        
    def _run_server(self):
        try:
            while self.running:
                try:
                    conn, addr = self.server.accept() #@UnusedVariable
                except:
                    self.running = False
                    return
                
                data = bytearray()
                wait_for_data = True
                while wait_for_data:
                    data += conn.recv(1024)
                    if (data.decode('utf-8').endswith(self.request_eor + self.request_eof)):
                        wait_for_data = False
                    elif data.decode('utf-8') == SHUT_DOWN:  
                        return
        
                data = data.decode('utf-8')
                
                #Send a queued up response if one exists, if not echo data back
                response = self._parent.get_server_response(data, self._port) 
                if response is not None:
                    sys.stdout.flush()
                    
                    conn.sendall(response.encode('utf-8'))
                else:
                    conn.sendall(data.encode('utf-8'))
                
        except Exception as err:
            print('SOCKET ERROR: ', err)
            traceback.print_exc()
        
    def stop_server(self):
        '''Stops the specified (LUT/ODR/BOTH) listen server'''
        if self.running:
            self.running = False
            #Connect to socket to get it out of accept mode
            s = socket.socket()
            s.connect(((self._host, self._port)))
            s.send(SHUT_DOWN.encode('utf-8'))
            s.close()
            self.server.close()
            self.server = None
            self._thread = None
