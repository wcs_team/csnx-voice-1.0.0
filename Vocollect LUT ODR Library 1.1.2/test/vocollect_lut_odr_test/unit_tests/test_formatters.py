#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import mock_catalyst #@UnusedImport
import unittest

from vocollect_lut_odr.formatters import RecordFormatter

class TestRecordFormatter(unittest.TestCase):
    def testDefaultSeparators__TC717(self):
        f = RecordFormatter()
        self.assertEqual(chr(10), f._record_separator) # \n
        self.assertEqual(',', f._field_separator)
    def testFormat__TC716(self):
        f = RecordFormatter(record_separator='.', field_separator=',')
        data = f.format_record([1, 2, 3])
        self.assertEqual('1,2,3.', data)
        f = RecordFormatter(record_separator='.', field_separator=';')
        data = f.format_record([1, 2, 3])
        self.assertEqual('1;2;3.', data)
        f = RecordFormatter(record_separator='\n', field_separator='|')
        data = f.format_record([1, 2, 3])
        self.assertEqual('1|2|3\n', data)
        f = RecordFormatter(record_separator='.', field_separator='\n')
        data = f.format_record([1, 2, 3])
        self.assertEqual('1\n2\n3.', data)


if __name__ == '__main__':
    unittest.main()