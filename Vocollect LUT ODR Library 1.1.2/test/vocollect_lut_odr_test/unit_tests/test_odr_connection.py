#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import mock_catalyst #@UnusedImport
import errno
import os.path
import socket
import threading
import time
import unittest

from vocollect_lut_odr_test.unit_tests.test_server import TestServer

import voice
from vocollect_lut_odr.connections import OdrConnection, WrongConfirmationError, NoConfirmationError,\
    OdrArchive
from vocollect_lut_odr.receivers import OdrConfirmationByte
from vocollect_lut_odr.transports import TransientSocketTransport
from voice import get_persistent_store_path
    
ARCHIVE_NAME = 'test_odr'
ARCHIVE_ADDR = '127.0.0.1'
ARCHIVE_PORT = 31337

#alternate archive settings
ARCHIVE_NAME_ALT = 'test_odr_alt'
ARCHIVE_ADDR_ALT = '127.0.0.1'
ARCHIVE_PORT_ALT = 31338

class FakeOdrConnection(object):
    def begin(self):
        pass
    def end(self):
        pass
    def send(self, fields):
        pass
    def close(self):
        pass

class FakeOdrTransport(object):
    def create_connection(self, *args, **kw):
        return FakeOdrConnection()

def remove_archive():
    archive_path = os.path.join(get_persistent_store_path(),
                                OdrArchive.ODR_SUBDIRECTORY,
                                ARCHIVE_NAME)
    try:
        os.remove(archive_path)
    except OSError:
        pass

    archive_path = os.path.join(get_persistent_store_path(),
                                OdrArchive.ODR_SUBDIRECTORY,
                                ARCHIVE_NAME_ALT)
    try:
        os.remove(archive_path)
    except OSError:
        pass

class TestOdrConnection(unittest.TestCase):
    def setUp(self):
        #clear prohibitor
        while not voice.task_unload_prohibitor.is_allowed():
            voice.task_unload_prohibitor.allow()

        remove_archive()
        #If an ODRConnection object is created having the same Archive Name as a previous object,
        #the new object will not actually get created, but will just receive a copy of the object with
        #the same archive name.  Since all tests below create an ODRConnection object with the same
        #Archive Name, the _connections dictionary must be cleared before each test to ensure each
        #object in each test gets a new object and not just a copy of the previous test.  Getting a copy
        #of an object from a previous test will surely give unexpected test results.
        OdrConnection._connections = {}

    def tearDown(self):
        remove_archive()
        OdrConnection.close_all_connections()
        
    def testOdrConnectionTakesArchiveName_TC661(self):
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT)
        c = OdrConnection(ARCHIVE_NAME, transport)
        self.assertEqual(ARCHIVE_NAME, c.archive_name)

    def testOdrConnectionTakesRetryTimer_TC661(self):
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT)
        c = OdrConnection(ARCHIVE_NAME, transport)
        c.set_retry_period(99)
        self.assertEqual(99, c.retry_period)

    def testMinimumRetryTimer_TC664(self):
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT)
        c = OdrConnection(ARCHIVE_NAME, transport)
        try:
            c.retry_period = 0
            self.fail("Retry period should never be set lower than 1")
        except ValueError:
            pass

    def testDefaultCounterValues_TC672(self):
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT)
        c = OdrConnection(ARCHIVE_NAME, transport)
        self.assertEqual((0,0,0,0,0), c.status())

    def testRecordSetPendingCounterIncrements_TC673(self):
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT, 5)
        c = OdrConnection(ARCHIVE_NAME, transport)
        c.retry_period = 1
        c.append(['1', '2', '3'])
        self.assertEqual((0,1,0,0,0), c.status())

        #allows ODR connection to end so test will end
        server = TestServer(ARCHIVE_ADDR, ARCHIVE_PORT)
        server.start_server();
        pending = c.status()[1]
        while pending > 0:
            time.sleep(0.1)
            pending = c.status()[1]
        server.stop_server();

    def testRecordSetPendingProhibitsTaskUnloadAndAutoSendOn(self):
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT, 5)
        c = OdrConnection(ARCHIVE_NAME, transport)
        c.retry_period = 1
        c.append(['1', '2', '3'])
        c.append(['4', '5', '6'])
        
        #validate that task load prohibitor is set
        self.assertEqual(False, voice.task_unload_prohibitor.is_allowed())

        server = TestServer(ARCHIVE_ADDR, ARCHIVE_PORT)
        server.start_server();
        pending = c.status()[1]
        while pending > 0:
            time.sleep(0.1)
            pending = c.status()[1]

        #validate that task load prohibitor is cleared
        server.stop_server()

        #validate prohibitor was cleared properly
        self.assertEqual(True, voice.task_unload_prohibitor.is_allowed())

    def testRecordSetPendingProhibitsTaskUnloadAndAutoSendOff(self):
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT, 5)
        c = OdrConnection(ARCHIVE_NAME, transport, None, None, False)
        c.retry_period = 1
        c.append(['7', '8', '9'])
        c.append(['10', '11', '12'])
        c.transmit()
        self.assertEqual(False, voice.task_unload_prohibitor.is_allowed())

        server = TestServer(ARCHIVE_ADDR, ARCHIVE_PORT)
        server.start_server();
        pending = c.status()[1]
        while pending > 0:
            time.sleep(0.1)
            pending = c.status()[1]

        #validate that task load prohibitor is cleared
        server.stop_server()

        #validate prohibitor was cleared properly
        self.assertEqual(True, voice.task_unload_prohibitor.is_allowed())

    def testProhibitorWithMultipleArchives(self):
        #validate not prohibited
        self.assertEqual(True, voice.task_unload_prohibitor.is_allowed())

        #--------------------------------------------------------
        #send ODRs to closed port for main port/archive
        transport = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT, 5)
        c = OdrConnection(ARCHIVE_NAME, transport)
        c.retry_period = 1
        c.append(['7', '8', '9'])
        c.append(['10', '11', '12'])

        #validate prohibited
        self.assertEqual(False, voice.task_unload_prohibitor.is_allowed())

        #--------------------------------------------------------
        #send ODRs to closed port for alternate port/archive
        transport_alt = TransientSocketTransport(ARCHIVE_ADDR, ARCHIVE_PORT_ALT, 5)
        c_alt = OdrConnection(ARCHIVE_NAME_ALT, transport_alt)
        c_alt.retry_period = 1
        c_alt.append(['7', '8', '9'])
        c_alt.append(['10', '11', '12'])
        
        #validate still prohibited
        self.assertEqual(False, voice.task_unload_prohibitor.is_allowed())

        #--------------------------------------------------------
        #receive first archive ODR's
        server = TestServer(ARCHIVE_ADDR, ARCHIVE_PORT)
        server.start_server();
        pending = c.status()[1]
        while pending > 0:
            time.sleep(0.1)
            pending = c.status()[1]

        #validate that task load prohibitor is cleared
        server.stop_server()

        #validate prohibitor still prohibited
        self.assertEqual(False, voice.task_unload_prohibitor.is_allowed())

        #--------------------------------------------------------
        #receive alternate archive ODR's
        server = TestServer(ARCHIVE_ADDR_ALT, ARCHIVE_PORT_ALT)
        server.start_server();
        pending = c_alt.status()[1]
        while pending > 0:
            time.sleep(0.1)
            pending = c_alt.status()[1]

        #validate that task load prohibitor is cleared
        server.stop_server()

        #validate prohibitor is now free
        self.assertEqual(True, voice.task_unload_prohibitor.is_allowed())

    def testOdrConnectionConstructsArchiveDirWithoutSlash(self):
        orig = voice.get_persistent_store_path
        try:
            # Monkeypatch the get_persistent_store_path with a slashless
            # known value
            voice.get_persistent_store_path = lambda: 'temp/noslash'
            c = OdrConnection(ARCHIVE_NAME, FakeOdrTransport())
            actual = c.archive_path
            expected = os.path.normpath(os.path.join('temp/noslash', OdrArchive.ODR_SUBDIRECTORY, ARCHIVE_NAME))
            self.assertEqual(expected, actual)
        finally:
            voice.get_persistent_store_path = orig

    def testOdrConnectionConstructsArchiveDirWithSlash(self):
        orig = voice.get_persistent_store_path
        try:
            # Monkeypatch the get_persistent_store_path with a known value
            # ending in a slash
            voice.get_persistent_store_path = lambda: 'temp/withslash/'
            c = OdrConnection(ARCHIVE_NAME, FakeOdrTransport())
            actual = c.archive_path
            expected = os.path.normpath(os.path.join('temp/withslash/', OdrArchive.ODR_SUBDIRECTORY, ARCHIVE_NAME))
            self.assertEqual(expected, actual)
        finally:
            voice.get_persistent_store_path = orig

    def testOdrConnectionIsReusedOnDuplicateArchiveName(self):
        conn1 = OdrConnection(ARCHIVE_NAME, FakeOdrTransport())
        conn2 = OdrConnection(ARCHIVE_NAME, FakeOdrTransport())
        self.assertTrue(conn1._connection is conn2._connection)

    def testOdrConnectionIsForgottenOnShutdown(self):
        conn1 = OdrConnection(ARCHIVE_NAME, FakeOdrTransport())
        #Cleanup after test to ensure there is nothing left running that could interfere with next test.
        OdrConnection.close_all_connections()
        conn2 = OdrConnection(ARCHIVE_NAME, FakeOdrTransport())
        self.assertTrue(conn1._connection is not conn2._connection)

    def testCloseAllConnectionsRemovesConnections(self):
        conn1 = OdrConnection(ARCHIVE_NAME, FakeOdrTransport()) #@UnusedVariable
        self.assertEqual(1, len(OdrConnection._connections))
        OdrConnection.close_all_connections()
        self.assertEqual(0, len(OdrConnection._connections))

    def testRetryTransmitRetriesImmediately(self):
        class FailingOdrConnection():
            def __init__(self):
                self._failure_event = threading.Event()
            def wait_for_failure(self):
                self._failure_event.wait()
                time.sleep(0.01)
            def clear_failure(self):
                self._failure_event.clear()
            
            def begin(self):
                ''' Simulate a dropped connection '''
                self._failure_event.set()
                raise socket.error(errno.ENETRESET)
            def end(self):
                pass
            def send(self, fields):
                pass
            def close(self):
                pass
        class FailingOdrTransport():
            def create_connection(self, *args, **kw):
                return FailingOdrConnection()
        
        conn = OdrConnection(ARCHIVE_NAME, FailingOdrTransport())
        conn.append(['1', '2', '3'])
        
        failconn = conn._connection._connection
        
        # Wait until the auto-transmit ODR connection fails once 
        # and starts its retry timer
        failconn.wait_for_failure()
        failconn.clear_failure()
        _, _, _, _, first_error_count = conn.status()
        _, _, first_error_time = conn.get_times()
        
        # Now tell it to retry immediately
        conn.retry_transmit()
        
        failconn.wait_for_failure()
        failconn.clear_failure()
        _, _, _, _, second_error_count = conn.status()
        _, _, second_error_time = conn.get_times()

        # Ensure we failed twice
        self.assertEqual(1, second_error_count - first_error_count)
        # Ensure we did not wait for the normal retry
        self.assertLess(second_error_time-first_error_time, 
                        conn.get_retry_period(),
                        'Second time %.2f, First Time %.2f, Time Elapsed %.2f, Expected time %.2f' 
                        % (second_error_time, first_error_time, 
                           second_error_time-first_error_time, 
                           conn.get_retry_period()))
        

class TestOdrConfirmationByte(unittest.TestCase):

    def testHexString(self):
        c = OdrConfirmationByte(0x59)
        try:
            c.receive('Y')
        except WrongConfirmationError:
            self.assert_(False, 'Error should not be raised')

    def testStringHex(self):
        c = OdrConfirmationByte('Y')
        try:
            c.receive(0x59)
        except WrongConfirmationError:
            self.assert_(False, 'Error should not be raised')

    def testWrongConfirmationByte(self):
        c = OdrConfirmationByte('Y')
        self.assertRaises(WrongConfirmationError, c.receive, 'Z')
        self.assertRaises(WrongConfirmationError, c.receive, 'YY')

    def testNoConfirmationError(self):
        c = OdrConfirmationByte('Y')
        self.assertRaises(NoConfirmationError, c.receive, '')

    def testAnyConfirmation(self):
        """No Assertions, error thrown if fails"""
        c = OdrConfirmationByte()
        c.receive('A')
        c.receive(0x59)
        c.receive(10)

if __name__ == '__main__':
    unittest.main()
