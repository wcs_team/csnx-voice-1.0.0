#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import mock_catalyst #@UnusedImport
import socket
import time
import unittest

from vocollect_lut_odr.connections import LutConnection, TransmitPendingException
from vocollect_lut_odr.formatters import RecordFormatter
from vocollect_lut_odr.transports import TransientSocketTransport
from vocollect_lut_odr_test.unit_tests.test_server import TestServer
from pending_luts import wait_for_lut_response
import os

HOST='127.0.0.1'
PORT=15018
DELAY=3

class TestLutConnection(unittest.TestCase):
    def setUp(self):
        self.server = TestServer(HOST, PORT)
        self.server.start_server()

        self.transport = TransientSocketTransport(HOST, PORT, DELAY)

    def tearDown(self):
        del self.transport
        self.server.stop_server()
        del self.server
        
    def testLutConnectionAppend_TC718(self):
        fmt = RecordFormatter("|")
        myLut = LutConnection(self.transport, fmt)
        myLut.append(['a','b','c'])
        
        myLut.wait_for_data()
        
        self.assertEqual('a|b|c\n\n', self.server.received.pop(0))
        self.assertTrue(myLut.data_ready())

    def testLutConnectionAppendDefaultFormat_TC718(self):
        myLut = LutConnection(self.transport)
        myLut.append(['a','b','c'])
        
        myLut.wait_for_data()
        
        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))
        self.assertTrue(myLut.data_ready())
        
    def testLutConnectionAppendManualTransmit_TC718(self):
        myLut = LutConnection(self.transport, None, None, False)
        myLut.append(['a','b','c'])
        myLut.transmit()
        
        myLut.wait_for_data()

        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))
        self.assertTrue(myLut.data_ready())

    def testLutConnectionAppendAsyncTransmit_TC819_TC822(self):
        self.server.is_blocking = True
        
        myLut = LutConnection(self.transport)
        myLut.append(['a','b','c'])
        
        self.assertFalse(myLut.data_ready())
        self.server.is_blocking = False
        time.sleep(1)

        self.assertTrue(len(self.server.received) > 0)
        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))
        self.assertTrue(myLut.data_ready())

    def testLutConnectionAsyncAppendTimeout_TC820(self):
        self.server.stop_server()
        self.server._delay = DELAY * 2
        self.server.start_server()

        myLut = LutConnection(self.transport)
        myLut.append(['a','b','c'])
        
        count = 0
        try:
            while not myLut.data_ready():
                time.sleep(1)
                count += 1
            self.fail("socket.timeout exception should occur")
        except socket.timeout as err:
            self.assertEqual('timed out', str(err))
            pass
            
        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))
        self.assertRaises(Exception, myLut.data_ready)
        self.assertTrue(count >= DELAY)
        
    def testLutConnectionSyncAppendTimeout_TC820(self):
        self.server.stop_server()
        self.server._delay = DELAY * 2
        self.server.start_server()

        myLut = LutConnection(self.transport)
        myLut.append(['a','b','c'])
        
        try:
            myLut.wait_for_data()
            self.fail("socket.timeout exception should occur")
        except Exception as err:
            self.assertEqual('timed out', str(err))
            pass
            
        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))
        self.assertRaises(Exception, myLut.data_ready)
        
    def testLutConnectionMultipleTransmit_TC847(self):
        myLut = LutConnection(self.transport, None, None, False)
        myLut.append(['a','b','c'])
        myLut.transmit()
        
        try:
            myLut.transmit()
            self.fail("Successive transmits should fail unless data has already been received")
        except TransmitPendingException: 
            pass

        myLut.wait_for_data()

        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))
        
        myLut.append(['a','b','c'])
        myLut.transmit()
        myLut.wait_for_data()

        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))

    def testLutConnectionAppendDuplicateAutoTransmit_TC847(self):
        myLut = LutConnection(self.transport, None, None, True)
        myLut.append(['a','b','c'])
        
        self.assertRaises(TransmitPendingException, myLut.transmit)
        
        myLut.wait_for_data()

        self.assertEqual('a,b,c\n\n', self.server.received.pop(0))
        self.assertTrue(myLut.data_ready())

    def testLutConnectionClosesSocketAfterReceipt(self):
        conn = LutConnection(self.transport, None, None, True)
        self.assertEqual(None, conn._connection._socket)
        conn.append(['a', 'b', 'c'])
        conn.wait_for_data()
        self.assertEqual(None, conn._connection._socket)
    
    def testLutBeepParameters(self):
        wav_file_path = mock_catalyst.get_persistent_store_path() + "\\error.wav"
        if os.path.isfile(wav_file_path):
            os.remove(wav_file_path)
        
        myLut = LutConnection(self.transport, None, None, True)
        self.server.enable_auto_response(False)
        myLut.append(['a','b','c'])
        
        # This should result in a beep prompt
        wait_for_lut_response(myLut, 2, (500, 0.2))

        myLut.append(['a','b','c'])
        # Wave file does not exist, therefore a beep prompt will be posted
        wait_for_lut_response(myLut, 2, 'error.wav')

        # Create the wave file so mock_catalyst thinks it exist
        
        wav_file = open(wav_file_path, "w")
        wav_file.writelines("This is a dummy file")
        wav_file.close()

        myLut.append(['a','b','c'])
        # Wave file does exist, therefore a play file appears in prompts
        wait_for_lut_response(myLut, 2, 'error.wav')
        
        print(mock_catalyst.prompts)
        
        # Good test - we get beep, beep, and a play called on...
        self.validate_prompts('beep', 
                          'beep', 
                          'Play Called on: .*')

        #Clean up
        self.server.enable_auto_response(True)
        os.remove(wav_file_path)
        

    def validate_prompts(self, *prompts):
        '''A helper method to assist in validating a list of prompts that were encountered'''
        
        count = 0
        for prompt in prompts:
            count += 1
            recorded_prompt = None
            if len(mock_catalyst.prompts) > 0:
                recorded_prompt = mock_catalyst.prompts.pop(0)
                
            self.assertRegexpMatches(recorded_prompt, prompt, 
                              "Prompt number %s: '%s' != '%s'" % (count, prompt, recorded_prompt))
        
        self.assertEquals(len(mock_catalyst.prompts), 0, 
                          'Still more recorded prompts: ' + str(mock_catalyst.prompts))
        self.assertEquals(len(mock_catalyst.response_queue), 0, 
                          'Not all queued responses used: ' + str(mock_catalyst.response_queue))
        
if __name__ == '__main__':
    unittest.main()
