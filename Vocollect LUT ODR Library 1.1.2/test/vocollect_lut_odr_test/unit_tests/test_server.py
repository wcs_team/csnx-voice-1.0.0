#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import socket
import threading
import time
import traceback

class TestServer():

    def __init__(self, host, port, delay=0, block_until_released=False):
        #Configuration variables
        self.running = False

        self.server = None
        self._thread = None

        self.host = host
        self.port = port
        self._delay = delay
        self.is_blocking = block_until_released

        self.responses = [] #Queue up data to respond with, if empty data received is echoed
        self.received = []  #List of received data

        self.response_eor = '\n'
        self.response_eof = '\n'
        self.request_eor = '\n'
        self.request_eof = '\n'
        self.listener_is_on = True
        
    def start_server(self):
        self.server = socket.socket()
        self.server.bind(((self.host, self.port)))
        self.server.listen(5)
        self.running = True
        self._thread = threading.Thread(target=self._run_server)
        self._thread.start()

    def _run_server(self):
        try:
            while self.running:
                conn, addr = self.server.accept() #@UnusedVariable
                if not self.running:
                    conn.close()
                    return

                binaryData = b''
                while not binaryData.decode('utf-8').endswith(self.request_eor + self.request_eof):
                    binaryData += conn.recv(1024)
                    
                    if not self.running:
                        return
                        
                self.received.append(binaryData.decode('utf-8'))

                #Delay the sending if necessary
                if self._delay:
                    time.sleep(self._delay)

                #Block if necessary
                while (self.is_blocking and self.running):                 
                    time.sleep(0.1)
                    
                if not self.running:
                    return
                    
                #Send a queued up response if one exists, if not echo data back
                if len(self.responses) > 0:
                    conn.sendall(self.responses.pop(0).encode('utf-8'))
                else:
                    if self.listener_is_on:
                        conn.sendall(binaryData)

        except Exception as err:
            print('SOCKET ERROR: ', err)
            traceback.print_exc()

    def enable_auto_response(self, flag = True):
        self.listener_is_on = flag
        
    def stop_server(self):
        self.running = False
        #Connect to socket to cause the accept to return
        s = socket.socket()
        s.connect(((self.host, self.port)))
        s.close()
        self._thread.join(2)

        if self._thread.is_alive():
            print("Failed to join to thread")
        
        self.server.close()
