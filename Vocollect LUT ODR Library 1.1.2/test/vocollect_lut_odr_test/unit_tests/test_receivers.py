#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import mock_catalyst #@UnusedImport
import unittest

from vocollect_lut_odr.receivers import Lut, NumericField, StringField, LutRecord,\
    OdrConfirmationByte, WrongConfirmationError, NoConfirmationError
from vocollect_lut_odr.receivers import RecordOutOfRange, InvalidRecordObject
import vocollect_lut_odr.receivers #@UnusedImport

class TestODR(unittest.TestCase):

    def testOdrConfirmationBytes(self):
        receiver_main = OdrConfirmationByte()
        #check None is invalid
        self.assertRaises(NoConfirmationError, receiver_main.receive, None)
        #check multiple bytes fail
        self.assertRaises(WrongConfirmationError, receiver_main.receive, chr(3) + chr(255))

        for i in range(256):
            rec_temp = OdrConfirmationByte(chr(i))
            #check None is invalid
            self.assertRaises(NoConfirmationError, rec_temp.receive, None)
            #check multiple bytes fail
            self.assertRaises(WrongConfirmationError, rec_temp.receive, chr(3) + chr(255))
            for j in range(256):
                #check 0-255 pass when any confirmation byte is allowed
                self.assertTrue(receiver_main.receive(chr(j)), 'any confirmation failed on ' + chr(j))
                self.assertTrue(receiver_main.receive(chr(j).encode('utf-8')), 'any confirmation failed on ' + chr(j))
                #check passes when specific confirmation byte specified
                if i == j:
                    self.assertTrue(rec_temp.receive(chr(j)), 'confirmation ' + chr(i) + ' failed on ' + chr(j))
                    self.assertTrue(rec_temp.receive(chr(j).encode('utf-8')), 'confirmation ' + chr(i) + ' failed on ' + chr(j))
                else:
                    self.assertRaises(WrongConfirmationError, rec_temp.receive, chr(j))
                    self.assertRaises(WrongConfirmationError, rec_temp.receive, chr(j).encode("utf-8"))
        
        #test bytes are valid
        receiver_main = OdrConfirmationByte(b'\xFF')
        self.assertTrue(receiver_main.receive(chr(255)), 'confirmation ' + chr(255) + ' failed')
        


class TestLut(unittest.TestCase):
    
    def testDataReceived(self):
        test_lut = Lut(StringField('field1'), StringField('field2'))
        
        
        #-----------------------------------------------------
        # Test Carriage return
        del mock_catalyst.log_messages[:]
        test_lut.receive('1,2\r3,4\r5,6\r\r')
        
        #validate data received log messages
        messages = ['Data Received: 1,2<CR>', 
                    'Data Received: 3,4<CR>',
                    'Data Received: 5,6<CR>',
                    'Data Received: <CR>',
                    'Data Records Received: 3 ']
        for i in range(len(mock_catalyst.log_messages)):
            self.assertEquals(mock_catalyst.log_messages.pop(0), messages[i])
        
        #validate fields
        self.assertEquals(test_lut[0]['field1'], '1')
        self.assertEquals(test_lut[0]['field2'], '2')
        self.assertEquals(test_lut[1]['field1'], '3')
        self.assertEquals(test_lut[1]['field2'], '4')
        self.assertEquals(test_lut[2]['field1'], '5')
        self.assertEquals(test_lut[2]['field2'], '6')
        self.assertEquals(len(test_lut), 3)
        
        #-----------------------------------------------------
        # Test Line feed
        del mock_catalyst.log_messages[:]
        test_lut.receive('1,2\n3,4\n5,6\n\n')

        #validate data received log messages
        messages = ['Data Received: 1,2<LF>', 
                    'Data Received: 3,4<LF>',
                    'Data Received: 5,6<LF>',
                    'Data Received: <LF>',
                    'Data Records Received: 3 ']
        for i in range(len(mock_catalyst.log_messages)):
            self.assertEquals(mock_catalyst.log_messages.pop(0), messages[i])
        
        #validate fields
        self.assertEquals(test_lut[0]['field1'], '1')
        self.assertEquals(test_lut[0]['field2'], '2')
        self.assertEquals(test_lut[1]['field1'], '3')
        self.assertEquals(test_lut[1]['field2'], '4')
        self.assertEquals(test_lut[2]['field1'], '5')
        self.assertEquals(test_lut[2]['field2'], '6')
        self.assertEquals(len(test_lut), 3)
        
        #-----------------------------------------------------
        # Test Carriage return line feed
        del mock_catalyst.log_messages[:]
        test_lut.receive('1,2\r\n3,4\r\n5,6\r\n\r\n')

        #validate data received log messages
        messages = ['Data Received: 1,2<CR><LF>', 
                    'Data Received: 3,4<CR><LF>',
                    'Data Received: 5,6<CR><LF>',
                    'Data Received: <CR><LF>',
                    'Data Records Received: 3 ']
        for i in range(len(mock_catalyst.log_messages)):
            self.assertEquals(mock_catalyst.log_messages.pop(0), messages[i])
        
        #validate fields
        self.assertEquals(test_lut[0]['field1'], '1')
        self.assertEquals(test_lut[0]['field2'], '2')
        self.assertEquals(test_lut[1]['field1'], '3')
        self.assertEquals(test_lut[1]['field2'], '4')
        self.assertEquals(test_lut[2]['field1'], '5')
        self.assertEquals(test_lut[2]['field2'], '6')
        self.assertEquals(len(test_lut), 3)
        
        #-----------------------------------------------------
        # Test Carriage return line feed, line feed
        del mock_catalyst.log_messages[:]

        test_lut.receive('1,2\r\n3,4\r\n5,6\r\n\n')

        #validate data received log messages
        messages = ['Data Received: 1,2<CR><LF>', 
                    'Data Received: 3,4<CR><LF>',
                    'Data Received: 5,6<CR><LF>',
                    'Data Received: <LF>',
                    'Data Records Received: 3 ']
        for i in range(len(mock_catalyst.log_messages)):
            self.assertEquals(mock_catalyst.log_messages.pop(0), messages[i])
        
        #validate fields
        self.assertEquals(test_lut[0]['field1'], '1')
        self.assertEquals(test_lut[0]['field2'], '2')
        self.assertEquals(test_lut[1]['field1'], '3')
        self.assertEquals(test_lut[1]['field2'], '4')
        self.assertEquals(test_lut[2]['field1'], '5')
        self.assertEquals(test_lut[2]['field2'], '6')
        self.assertEquals(len(test_lut), 3)
        
        
    def testParsingAndEndOfRecord(self):
        data = []
        
        #RALLYTC 770 & 774: Test embedding commas in a string field and Receiving string values for numeric field
        data.append('1,test,field3')
        data.append('2,",te,,s""t,",field3')
        data.append('a,",te,,s""t,,field3')
        data.append('1092002930029939, test\'s, Gourme\'t"a"whatfood')
        
        #RALLYTC 763: Test assigning floating point numbers to a Numeric field  
        data.append("1.0409899,test,field3")
        
        #RALLYTC 762: Test assigning invalid values to a Numeric field 
        data.append("-1.5a,test,field3")      
        
        self._receiveTests('\n', data)
        self._receiveTests('\r', data)
        self._receiveTests('\r\n', data)
    
    def testParsingWrongNumberOfFields(self):
        #RALLYTC 772: Test receiving fewer fields from the host than defined 
        myData3 = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        myData3.receive('Manhattan Associates\n\n')
        self.assertEqual('Manhattan Associates', myData3[0]['WarehouseName'])
        self.assertEqual(0, myData3[0]['Error Code'])
        self.assertEqual('', myData3[0]['Error Msg'])

        #RALLYTC 773: Test receiving more fields from the host than defined 
        myData3.receive('Walmart,4,myMessage,more,manymore\nManhattan Associate,6,\n\n')
        self.assertEqual('Walmart', myData3[0]['WarehouseName'])
        self.assertEqual(4, myData3[0]['Error Code'])
        self.assertEqual('myMessage', myData3[0]['Error Msg'])     
        
    def testIteration(self):
        myData = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        myData.receive('Manhattan Assoc,0,\nManhattan Assoc,1,\nManhattan Assoc,2,\n\n')
    
        #RALLYTC 768: Using Manual Iterator to loop through a LUT
        myIter = iter(myData)
        self.assertEqual(0, next(myIter)['Error Code'])
        self.assertEqual(1, next(myIter)['Error Code'])
        self.assertEqual(2, next(myIter)['Error Code'])
        self.assertRaises(StopIteration, next, myIter)
       
        #RALLYTC 769: Test normal iteration and multiple loops 
        count = 0
        self.assertEqual(3, len(myData))
        for rec in myData:
            self.assertEqual(count, rec['Error Code'])
            count += 1
    
            #Loop within loop
            count2 = 0
            for rec2 in myData:
                self.assertEqual(count2, rec2['Error Code'])
                count2 +=1
    
    def testDeleteRecord(self):
        myData = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        myData.receive('Manhattan Assoc,0,\nManhattan Assoc,1,\nManhattan Assoc,2,\n\n')
        
        #RALLYTC 764 & 771: Test user is able to delete a record and Test base index = 0   
        self.assertEqual(3, len(myData))
        del myData[1] 
        self.assertEqual(2, len(myData))
        self.assertEqual(0, myData[0]['Error Code'])
        self.assertEqual(2, myData[1]['Error Code'])
        
        #RALLYTC 775: Deleting a non-existent record
        self.assertRaises(IndexError, myData.__delitem__, 4)
        
        #RALLYTC 765: Test deleting multiple records    
        for i in range(len(myData)): #@UnusedVariable
            del myData[0]
        self.assertEquals(0, len(myData))

    def testAppendRecord(self):
        myData = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        myData.receive('Manhattan Assoc,0,\nManhattan Assoc,1,\nManhattan Assoc,2,\n\n')
        
        #RALLYTC 777 & 782: Test user is able to append a record object with lists and strings to a received LUT 

        #test append another LUT object
        myData2 = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        myData2.receive('Manhattan Assoc,3,\nManhattan Assoc,4,\n\n')

        self.assertEqual(3, len(myData))
        myData.append(myData2)
        self.assertEqual(5, len(myData))
        for i in range(len(myData)):
            self.assertEqual(i, myData[i]['Error Code'])
            
        #test create and append a single record object created using a string
        record = LutRecord(myData.fields, 'Field1,5') 
        myData.append(record)
        self.assertEqual(6, len(myData))
        for i in range(len(myData)):
            self.assertEqual(i, myData[i]['Error Code'])
        
        #test create and append a single record object created using a list
        record = LutRecord(myData.fields, ['Field1',6]) 
        myData.append(record)
        self.assertEqual(7, len(myData))
        for i in range(len(myData)):
            self.assertEqual(i, myData[i]['Error Code'])
        
        #test appending a list of records
        myData.append([LutRecord(myData.fields, ['Field1',7]), 
                       LutRecord(myData.fields, ['Field1',8])])
        self.assertEqual(9, len(myData))
        for i in range(len(myData)):
            self.assertEqual(i, myData[i]['Error Code'])
            
        #test appending invalid objects or lists with invalid objects
        self.assertRaises(InvalidRecordObject, myData.append, 0)
        self.assertEqual(9, len(myData))
        self.assertRaises(InvalidRecordObject, myData.append, [record, 0])
        self.assertEqual(9, len(myData))
        
        
    def testUpdateFields(self):
        myData = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        myData.receive('Manhattan Assoc,0,\nManhattan Assoc,1,\nManhattan Assoc,2,\n\n')

        #RALLYTC 761: Updating a LUT field
        myData[1]['WarehouseName'] = 'Field1 to Walmart'
        self.assertEqual('Field1 to Walmart', myData[1]['WarehouseName'])
        
        #RALLYTC 779: Test user is able to bulk update multiple LUT records
        #update fields 
        for i in myData:
            i['Error Msg'] = 'Bulk updating'
        
        #check fields
        for i in range(len(myData)):
            self.assertEqual('Bulk updating', myData[0]["Error Msg"])
            
        #test setting numeric to non numeric value
        myData[1]['Error Code'] = 'Field1 to Walmart'
        self.assertEqual(0, myData[1]['Error Code'])
        
    def testAccessingData(self):
        myData = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        myData.receive('Manhattan Assoc,0,\nManhattan Assoc,1,\nManhattan Assoc,2,\n\n')
        
        #All above tests access data, so this is only testing error conditions
        
        #RALLYTC 775: Reading a non-existent record
        self.assertRaises(RecordOutOfRange, myData.__getitem__, 4)
        
        record = myData[0]
        self.assertRaises(KeyError, record.__getitem__, 'Undefined')
        
    def testLoggingDataReceived(self):
        myData = Lut(StringField('WarehouseName'), NumericField('Error Code'), StringField("Error Msg"))
        mock_catalyst.log_messages = []

        #-------------------------------------------------------------------
        #property set to true
        vocollect_lut_odr.receivers.log_received_lut_records = True
        myData.receive('Manhattan Assoc,0,\n'
                       'Manhattan Assoc,1,\n'
                       'Manhattan Assoc,2,\n\n')
    
        #validate data received log messages
        messages = ['Data Received: Manhattan Assoc,0,<LF>', 
                    'Data Received: Manhattan Assoc,1,<LF>',
                    'Data Received: Manhattan Assoc,2,<LF>',
                    'Data Received: <LF>',
                    'Data Records Received: 3 ']
        for i in range(len(mock_catalyst.log_messages)):
            self.assertEquals(mock_catalyst.log_messages.pop(0), messages[i])
        self.assertEquals(0, len(mock_catalyst.log_messages))
        
        
        #-------------------------------------------------------------------
        #property set to false
        vocollect_lut_odr.receivers.log_received_lut_records = False
        myData.receive('Manhattan Assoc,0,\n'
                       'Manhattan Assoc,1,\n'
                       'Manhattan Assoc,2,\n\n')
    
        #validate data received log messages
        messages = ['Data Records Received: 3 ']
        for i in range(len(mock_catalyst.log_messages)):
            self.assertEquals(mock_catalyst.log_messages.pop(0), messages[i])
        self.assertEquals(0, len(mock_catalyst.log_messages))
        
    ####################################################################
    # Helper methods
    ####################################################################
    def _receiveTests(self, recordSeperator, data):
        """ tests that data was parsed correctly """
        l = Lut(NumericField('Field1'), 
                StringField('Field2'), 
                StringField('Field3'))
        
    
        self._receiveData(l, recordSeperator, data)
        
        self.assertEqual(6, len(l))
        
        self.assertEqual(1, l[0]['Field1'])
        self.assertEqual(2, l[1]['Field1'])
        self.assertEqual(0, l[2]['Field1'])
        self.assertEqual(1092002930029939, l[3]['Field1'])
        self.assertEqual("Gourme't\"a\"whatfood", l[3]['Field3'])
        self.assertEqual(1.0409899, l[4]['Field1'])
        self.assertEqual(0, l[5]['Field1'])        

        self.assertEqual("test", l[0]['Field2'])
        
        self.assertEqual(',te,,s"t,', l[1]['Field2'])
        self.assertEqual(',te,,s"t,,field3', l[2]['Field2'])
        self.assertEqual("test's", l[3]['Field2'])
        self.assertEqual('field3', l[0]['Field3'])
        self.assertEqual('field3', l[1]['Field3'])
        self.assertEqual('', l[2]['Field3'])

    def _receiveData(self, lut, recordSeperator, data):
        for item in data:
            self.assertFalse(lut.receive(item + recordSeperator))
        self.assertTrue(lut.receive(recordSeperator))
        
        