#
# Copyright (c) 2011 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import unittest

import vocollect_lut_odr
from vocollect_lut_odr import lut_odr_encoder, lut_odr_decoder

class TestLutOdrCodec(unittest.TestCase):

    def setUp(self):
        # Ensure we are starting with UTF-8
        vocollect_lut_odr.codec = 'utf-8'

    def tearDown(self):
        # Ensure codec is UTF-8 when we leave
        vocollect_lut_odr.codec = 'utf-8'
        
    def testEncoder(self):
        # Test 1 - default UTF-8 Encodings
        test_string = '深入 Test String'
        byte_string = lut_odr_encoder(test_string)
        self.assertEquals(len(test_string), 14)
        self.assertEqual(len(byte_string), 18)
        self.assertEqual(byte_string, b'\xe6\xb7\xb1\xe5\x85\xa5 Test String')
        
        # Test 2 - change to cp1252
        cur_codec = vocollect_lut_odr.codec
        vocollect_lut_odr.codec = 'cp1252'
        test_string = 'säg klar för att fortsätta'
        cp1252_string = lut_odr_encoder(test_string)
        self.assertEqual(len(cp1252_string), 26)
        self.assertEqual(cp1252_string, b's\xe4g klar f\xf6r att forts\xe4tta')

        #Test 3 - pass in a string that the code page can't handle
        #         the encoder will simply return the string
        test_string = '深入 Test String'
        cp1252_string = lut_odr_encoder(test_string)
        self.assertEqual(len(byte_string), 18)
        self.assertEqual(byte_string, b'\xe6\xb7\xb1\xe5\x85\xa5 Test String')

        vocollect_lut_odr.codec = cur_codec
        
    def testDecoder(self):
        # Test 1 - do a round trip encoding/decoding of a UTF-8 string
        test_string = '深入 Test String'
        byte_string = lut_odr_encoder(test_string)
        test_string2 = lut_odr_decoder(byte_string)
        self.assertEquals(test_string, test_string2)
        
        # Test 2 - change to cp1252
        cur_codec = vocollect_lut_odr.codec
        vocollect_lut_odr.codec = 'cp1252'
        test_string = 'säg klar för att fortsätta'
        byte_string = lut_odr_encoder(test_string)
        test_string2 = lut_odr_decoder(byte_string)
        self.assertEquals(test_string, test_string2)

        # Test 3 - bad round trip
        #          encode a byte array using utf-8
        #          decode using cp1252 -- resulting 
        #          string should not equal what we started with
        vocollect_lut_odr.codec = 'utf-8'
        test_string = '深入 Test String'
        byte_string = lut_odr_encoder(test_string)
        vocollect_lut_odr.codec = 'cp1252'
        test_string2 = lut_odr_decoder(byte_string)
        self.assertNotEqual(test_string, test_string2)
        
        vocollect_lut_odr.codec = cur_codec

if __name__ == '__main__':
    unittest.main()