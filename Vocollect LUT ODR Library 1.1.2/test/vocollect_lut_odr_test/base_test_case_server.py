#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
from vocollect_core_test.base_test_case import BaseTestCaseCore

from vocollect_lut_odr_test.mock_server import MockServer, LUT_SERVER, ODR_SERVER, BOTH_SERVERS #@UnusedImport
from mock_catalyst import EndOfApplication #@UnusedImport

class BaseTestCaseServer(BaseTestCaseCore):
    '''This class is a base class to be used when creating test that contains common setup of test 
    as well as some helper methods for validating prompts and LUT/ODR requests''' 
    
    def __init__(self, methodName):
        super().__init__(methodName)

        #create simulated host server object
        self.mock_server = MockServer()
        
        # pass through functions to the mock server
        self.stop_server = self.mock_server.stop_server
        self.start_server = self.mock_server.start_server  
        self.set_server_response = self.mock_server.set_server_response
        self.load_server_responses = self.mock_server.load_server_responses
        
    def tearDown(self):
        self.stop_server(BOTH_SERVERS)
        super().tearDown()

    def clear(self):
        """ clear all data storage """
        super().clear()
        self.mock_server.clear()

    def set_manual_run(self):
        ''' Called from a test case to siwtch to manual mode so test can be 
        re-recorded '''
        super().set_manual_run()
        self.mock_server._use_std_in_out = True
        
