#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
"""LUT and ODR formatter objects."""

class RecordFormatter(object):
    """A stream-based formatter that separates fields and records, and 
    terminates recordsets.
    
        field_separator - character(s) to seperate fields with
        record_separator - character(s) to terminate a record
        record_set_terminator - character(s) to terminate a record set
    """
    def __init__(self, field_separator=',', 
                 record_separator=chr(10),
                 record_set_terminator=chr(10)):
        self._field_separator = field_separator
        self._record_separator = record_separator
        self._record_set_terminator = record_set_terminator

    def format_record(self, fields):
        """Format the record's fields, and terminate with a record separator.

            fields - fields that make up the record 
        """
        return self._field_separator.join(map(str,fields)) + self._record_separator
 
    def terminate_recordset(self):
        """Return the character sequence that marks the end of a recordset."""
        return self._record_set_terminator


