#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
"""LUT and ODR connection classes."""


import os
import pickle
import threading
import time
import traceback

import voice #@UnusedImport
import voice.atunload

from .formatters import RecordFormatter
from .receivers import Lut, StringField, WrongConfirmationError, NoConfirmationError

##############################################################################
# LUT Connection object and exceptions
##############################################################################

class TransmitPendingException(Exception):
    """This LUT Connection is already transmitting.  Wait for the data to be
        received before trying to re-transmit"""

class LutConnection(object):
    """
    Constructor Lut Connection Object.

        transport - transport for connections to host system
        output_formatter - record formatter for formatting output data
        data_receiver - object for receiving and parsing data from host
        auto_transmit - determines if transmit should occur on appends
    """
    _PRE_TRANSMIT = 1
    _TRANSMITTING = 2
    _FINISHED_TRANSMIT = 3
    
    def __init__(self, transport,
                 output_formatter=None,
                 data_receiver = None,
                 auto_transmit=True):
        #create formatter if one isn't specified
        if output_formatter is None:
            output_formatter = RecordFormatter()

        #get receiver if needed
        if data_receiver is None:
            self.lut_data = Lut(StringField('Record'))
        else:
            self.lut_data = data_receiver

        self._auto_transmit = auto_transmit
        self._output_formatter = output_formatter
        self._connection = transport.create_connection(None,
                                                       self.lut_data)
        self._current_record_set  = []

        self._state = LutConnection._PRE_TRANSMIT
        
    def transmit(self):
        """ All transmissions are asynchronous, access to the received data should
        occur through the data_receiver.  At data access time, the thread will block
        until data is ready to be read. """
        if self._state == LutConnection._TRANSMITTING:
            raise TransmitPendingException()
        self._state = LutConnection._TRANSMITTING
        self.lut_data._exception = None   #clear Any previous errors
        self.lut_data.clear_records()
        self.lut_data._transmit_done.clear()
        
        #format complete data string to send here
        output = ''
        for current_record in self._current_record_set:
            output += self._output_formatter.format_record(current_record)
        output += self._output_formatter.terminate_recordset()
        
        worker_thread = LutConnectionWorker(self._connection, output, self)
        worker_thread.start()
        
        self._current_record_set = []

    def wait_for_data(self):
        self.lut_data.wait_for_data()

    def data_ready(self):
        return self.lut_data.data_ready()
    
    def append (self, field_list):
        """
        Append a list of field to output record

           fields - data to append to output records
        """
        if type(field_list) != list:
            field_list = [str(field_list)]
        self._current_record_set.append(field_list)
        if self._auto_transmit:
            self.transmit()
        
    
class LutConnectionWorker(threading.Thread):

    def __init__(self,  
                 connection,
                 record_set,
                 lut_connection):
        """ constructor for LutConnection """
        threading.Thread.__init__(self)
        
        self._connection = connection
        self._current_record_set = record_set
        self._lut_connection = lut_connection

    def run(self):
        try:
            self._internal_transmit()
        except Exception as e:
            # Remove the traceback, as it references the frame, which
            # references the thread context, which will likely not
            # exist when it's rethrown
            e.__traceback__ = None
            self._lut_connection.lut_data._exception = e
        
        self._lut_connection._state = LutConnection._FINISHED_TRANSMIT
        self._lut_connection.lut_data._transmit_done.set()
        
    def _internal_transmit(self):
        """Send a LUT request."""
        self._connection.begin(self._current_record_set)
        self._connection.send(self._current_record_set) 
        self._connection.end()




##############################################################################
# ODR Connection object
##############################################################################
class OdrConnection(object):

    _connections = {}

    def __init__(self, 
                 archive_name, 
                 transport,
                 output_formatter = None,
                 data_receiver = None,
                 auto_transmit = True):
        """
        Create or return an ODR Connection.

        transport - transport for connections to host system
        output_formatter - record formatter for formating output data
        data_receiver - object for receiving and parsing data from host (Confirmation Byte)
        auto_transmit - determines if transmit should occur on appends

        OdrConnections are unique by name; if an connection with the
        given name already exists, that one will be returned instead of
        creating a new one.
        """
        
        #save constructor arguments
        self.__archive_name = archive_name
        self.__transport = transport
        self.__output_formatter = output_formatter
        self.__data_receiver = data_receiver
        self.__auto_transmit = auto_transmit
        if output_formatter is None:
            self.__output_formatter = RecordFormatter(',', chr(10))

        #create background archive thread
        self._connection = self.__get_archive()
        
        # Initialize private data
        self.__current_record_set  = []
        
    ############################################################
    # Methods to get archive, and create if needed
    ############################################################
    def __get_archive(self):
        '''Internal method to get a named ODR archive, or
        create it if none exists.
        '''
        if self.__archive_name in self._connections:
            return self._connections[self.__archive_name]
        else:
            archive = OdrArchive(self.__archive_name, 
                                 self.__transport, 
                                 self.__data_receiver, 
                                 self.__auto_transmit)
            self._connections[self.__archive_name] = archive
            return archive
        
    @classmethod
    def get_archive(cls, name):
        '''Return the named ODR archive, or None if no archive
        with that name exists.
        '''
        return cls._connections.get(name)
    
    ############################################################
    # Methods used for pickling save and restore
    ############################################################
    def __getstate__(self):
        ''' Removes properties that cannot be pickled '''
        state = self.__dict__.copy()
        state['_OdrConnection_connections'] = None
        state['_connection'] = None
        return state

    def __setstate__(self, state):
        ''' method called when restoring state from pickling '''
        self.__dict__ = state
        self._connection = self.__get_archive()
    
    ############################################################
    # Main methods to create and send ODR data
    ############################################################
    def append(self, field_list):
        ''' Appends a record to an ODR data set and automatically transmits it
        if auto_transmit is set to true '''

        #check if list was passed in if not convert to list
        if type(field_list) != list:
            field_list = [str(field_list)]

        #append record to current record set
        self.__current_record_set.append(self.__output_formatter.format_record(field_list))

        #if auto transmit is on, then transmit ODR
        if self.__auto_transmit:
            self.transmit()

    def transmit(self):
        ''' Add the current record set to pending list and call start send '''

        if len(self.__current_record_set) > 0:
            #Build data string to transmit
            data = ""
            for record in self.__current_record_set:
                data += record
            if self.__output_formatter.terminate_recordset():
                data += self.__output_formatter.terminate_recordset()
            
            self._connection.transmit(data, len(self.__current_record_set))

            #clear current record set
            self.__current_record_set = []
        
    def retry_transmit(self):
        ''' Immediately retry transmission if any records are pending. '''
        return self._connection.retry_transmit()

    ############################################################
    # Shutdown methods
    ############################################################
    @classmethod
    def close_all_connections(cls):
        ''' method registered with voice task unload to shut down threads 
        when python itself is shutdown '''
        voice.log_message('SHUTDOWN: ODR Shutdown start')
        connections = list(cls._connections.values())
        for conn in connections:
            conn.shutdown()
            del cls._connections[conn.archive_name]

        voice.log_message('SHUTDOWN: ODR Shutdown complete')
        
    ############################################################
    # Pass through methods to connection
    ############################################################
    ############################################################
    # Additional properties
    ############################################################
    @property
    def archive_name(self):
        '''Return the name under which the ODR will be persisted.
        This is a read-only property, set at creation time.'''
        return self._connection.archive_name

    @property
    def archive_path(self):
        """Return the full path to the the file where the ODR will
        be persisted."""
        return self._connection.archive_path

    def get_retry_period(self):
        '''Return the period, in seconds, after which the ODR will be
        re-sent if sending fails. This is a read-only property, set at
        creation time.'''
        return self._connection.get_retry_period()

    def set_retry_period(self, retry_period):
        '''This method sets the retry period for the ODR.
            Default is set to 10'''
        return self._connection.set_retry_period(retry_period)

    retry_period = property(get_retry_period, set_retry_period)
    
    def status(self):
        ''' returns status of ODR connection
        return a tuple of counts (current_record_set_len,
                                  pending_record_set_len,
                                  current_records_sent,
                                  current_record_sets_sent,
                                  current_error_count)
        '''
        return self._connection.status(len(self.__current_record_set))

    def get_times(self):
        ''' get statistics of last transmission
        returns tuple of times for (last_attempt, last_success, last_error)  
        '''
        return self._connection.get_times()
    
    def has_pending_records(self):
        ''' Return True if the connection has pending records; False otherwise.'''
        return self._connection.has_pending_records()

##############################################################################
# ODR Connection object
##############################################################################
class OdrArchive(object):
    # Error codes for callback
    ERROR_UNKNOWN = 0
    ERROR_CONNECTION = 1
    ERROR_TRANSMISSION = 2
    ERROR_NO_CONFIRMATION = 3
    ERROR_WRONG_CONFIRMATION = 4

    # Subdirectory for ODR data archives, must be in form
    # 'subdir/' (note trailing slash)
    ODR_SUBDIRECTORY = 'odr_archive/'

    def __init__(self, 
                 archive_name, 
                 transport,
                 data_receiver = None,
                 auto_transmit = True,
                 task_unload_prohibitor = voice.task_unload_prohibitor):
        """
        Create or return an ODR Connection.

        transport - transport for connections to host system
        output_formatter - record formatter for formating output data
        data_receiver - object for receiving and parsing data from host (Confirmation Byte)
        auto_transmit - determines if transmit should occur on appends

        OdrConnections are unique by name; if an connection with the
        given name already exists, that one will be returned instead of
        creating a new one.
        """

        self.__auto_transmit = auto_transmit
        self.__archive_name  = archive_name
        self.__retry_period = 10
        
        self._connection = transport.create_connection(None,
                                                       data_receiver)

        # Initialize private data
        self.__pending_record_sets = []
        self.__records_sent        = 0
        self.__record_sets_sent    = 0
        self.__error_count         = 0
        self.__last_attempt_time   = 0
        self.__last_success_time   = 0
        self.__last_error_time     = 0

        self.__task_unload_prohibitor = task_unload_prohibitor
        self.__transmit_thread_running = False
        self.__callbacks_lock = None
        self.__data_lock       = None
        self.__statistics_lock = None
        self.__thread_mgt_lock = None
        self.__background_transmit_event = None
        self.__background_shutdown_event = None
        self.__intialize_threading()

    ############################################################
    # Additional properties
    ############################################################
    @property
    def archive_name(self):
        '''Return the name under which the ODR will be persisted.
        This is a read-only property, set at creation time.'''
        return self.__archive_name

    @property
    def archive_path(self):
        """Return the full path to the the file where the ODR will
        be persisted."""
        return os.path.normpath(os.path.join(voice.get_persistent_store_path(),
                                             self.ODR_SUBDIRECTORY,
                                             self.archive_name))

    def get_retry_period(self):
        '''Return the period, in seconds, after which the ODR will be
        re-sent if sending fails. This is a read-only property, set at
        creation time.'''
        return self.__retry_period

    '''This method sets the retry period for the ODR.
        Default is set to 10'''
    def set_retry_period(self, retry_period):
        self.__retry_period = retry_period
        # don't allow sub-second retry periods
        if self.__retry_period < 1:
            raise ValueError('Illegal retry period')

    retry_period = property(get_retry_period, set_retry_period)

    ############################################################
    # Shutdown methods
    ############################################################
    def shutdown(self, okayToWait = True):
        ''' shut down an individual thread '''
        self._connection.close()
        self.__stop_background_send(okayToWait)


    ############################################################
    # Main methods to create and send ODR data
    ############################################################
    def transmit(self, data, record_count):
        ''' Add the current record set to pending list and call start send '''

        #append data to pending data
        self.__task_unload_prohibitor.prohibit()
        with self.__data_lock:
            self.__pending_record_sets.append((record_count, data))
            self.__preserve_data()

        #start thread if not already started, and wake it up
        self.__start_background_send()
        self.__background_transmit_event.set()

    def retry_transmit(self):
        self.__background_transmit_event.set()
        
    ############################################################
    # Methods used to persist ODR data until it is sent
    ############################################################
    def __create_archive_path(self):
        ''' checks if archive path exists and if not creates it '''
        archive_dir, archive_file = os.path.split(self.archive_path) #@UnusedVariable
        if not os.access(archive_dir, os.F_OK):
            os.makedirs(archive_dir)

    def __preserve_data(self):
        ''' saves the current ODR data to flash ''' 
        # CAUTION - assumes that self.__data_lock is acquired
        
        try:
            #If pending list is empty remove archive
            if len(self.__pending_record_sets) == 0:
                try:
                    if os.access(self.archive_path, os.F_OK):
                        os.remove(self.archive_path)
                except:
                    voice.log_message(
                        'OdrConnection failed to clear ODR data archive %s'
                        % self.archive_path())
            
            #otherwise save pending list to file
            else:
                #try an open file and if fail try again but second time create if needed
                try:
                    f = open(self.archive_path, 'wb')
                except:
                    self.__create_archive_path()
                    f = open(self.archive_path, 'wb')
    
                pickle.dump(self.__pending_record_sets, f)
                f.close()
            
        except:
            # pickle failed to save data - log
            voice.log_message('OdrConnection failed to save ODR data to %s' %
                              self.archive_path)

    def __restore_data(self):
        ''' restores previously saved ODR data that has not yet been sent '''
        # CAUTION - assumes that self.__data_lock is acquired
        self.__create_archive_path()
        if os.access(self.archive_path, os.R_OK):
            f = open(self.archive_path, 'rb')
            try:
                self.__pending_record_sets = pickle.load(f)
                for record in self.__pending_record_sets: #@UnusedVariable
                    self.__task_unload_prohibitor.prohibit()
                    
                voice.log_message('OdrConnection: Archive %s restored %s ODRs to queue that will be sent' % 
                                  (self.__archive_name, len(self.__pending_record_sets)))
            except:
                # pickle failed to reconstitute data - log and assume no data
                voice.log_message('OdrConnection failed to restore ODR '
                                  'data from %s' % self.archive_path)
            f.close()
        
    ############################################################
    # Methods for getting stats
    ############################################################
    def has_pending_records(self):
        ''' Return True if this archive has pending records; False otherwise.
        '''
        with self.__data_lock:
            return bool(self.__pending_record_sets)
        
    def status(self, current_record_set_len):
        ''' returns status of ODR connection
        return a tuple of counts (current_record_set_len,
                                  pending_record_set_len,
                                  current_records_sent,
                                  current_record_sets_sent,
                                  current_error_count)
        '''
        with self.__statistics_lock:
            current_records_sent = self.__records_sent
            current_record_sets_sent = self.__record_sets_sent
            current_error_count = self.__error_count
        with self.__data_lock:
            pending_record_set_len = len(self.__pending_record_sets)
        return (current_record_set_len, pending_record_set_len,
                current_records_sent, current_record_sets_sent,
                current_error_count)

    def get_times(self):
        ''' get statistics of last transmission
        returns tuple of times for (last_attempt, last_success, last_error)  
        '''
        with self.__statistics_lock:
            last_attempt = self.__last_attempt_time
            last_success = self.__last_success_time
            last_error = self.__last_error_time
        return (last_attempt, last_success, last_error)

    ############################################################
    # Threading Methods
    ############################################################
    def __intialize_threading(self):
        ''' initializes all the threading objects '''
        self.__callbacks_lock = threading.RLock()
        self.__data_lock       = threading.RLock()
        self.__statistics_lock = threading.RLock()
        self.__thread_mgt_lock = threading.RLock()
        self.__background_transmit_event = threading.Event()
        self.__background_shutdown_event = threading.Event()
        
        #log that archive has started
        voice.log_message('OdrConnection: Archive %s started' % (self.__archive_name))
        
        # Load archived data
        self.__restore_data()
        
        # Create and start background thread if there is old data
        if len(self.__pending_record_sets) > 0:
            self.__start_background_send()
        
    def __start_background_send (self):
        ''' Starts up the background thread for transmitting data '''
        with self.__thread_mgt_lock:
            if not self.__transmit_thread_running:                
                self.__background_transmit_thread = threading.Thread(
                    target = self.__background_transmit_thread_method,
                    name="ODR " + self.archive_name + " Thread")
                voice.atunload.register(OdrConnection.close_all_connections)
                self.__transmit_thread_running = True
                self.__background_transmit_thread.start()

    def __stop_background_send (self, okayToWait = True):
        ''' Stops the background thread '''
        voice.log_message('SHUTDOWN: Stopping ODR background thread - ' + self.__archive_name)
        if self.__transmit_thread_running:                
            with self.__thread_mgt_lock:
                self.__transmit_thread_running = False
            
            self.__background_transmit_event.set()
            self.__background_shutdown_event.wait(1)
        voice.log_message('SHUTDOWN: Stopped ODR background thread - ' + self.__archive_name)

    def __background_transmit_thread_method (self):  
        ''' Main method of background thread '''
        
        #run continuously until shutdown is called        
        while self.__transmit_thread_running:
            #if no more records wait until another record is ready for transmit
            #else wait for retry period
            try: 
                if len(self.__pending_record_sets) == 0:
                    self.__background_transmit_event.wait()
                else:
                    self.__background_transmit_event.wait(self.__retry_period)
                self.__background_transmit_event.clear()
                
                #if thread is not being shut down then transmit pending data
                if self.__transmit_thread_running:                                    
                    self.__background_transmit()                        

            except Exception as err:
                voice.log_message('ODR Error in "' + self.archive_name + '" thread: ' + str(err) + '\n' + str(traceback.extract_stack()))

        voice.log_message('SHUTDOWN: thread ended - ' + self.__archive_name)
        self.__background_shutdown_event.set()

    def __handle_transport_error (self, reason_code = ERROR_UNKNOWN, err = None):
        ''' Handle errors that occur during transmit '''
        #close connection
        self._connection.close()
        
        #update stats
        with self.__statistics_lock:
            self.__last_error_time = time.time()
            self.__error_count += 1

        status = self.status(0)
        err_msg = ''
        if err != None:
            err_msg = str(err)
            
        
        voice.log_message("ODR Transport Error: [errors = %d, sent = %d, pending = %d] (%s) %s " % 
                          (status[4], status[3], status[1], 
                          str(reason_code), err_msg))
        

    def __background_transmit (self):
        ''' Main method used to transmit data '''

        # terminate thread if no pending data
        if len(self.__pending_record_sets) == 0:
            return
        
        #update statistics
        with self.__statistics_lock:
            self.__last_attempt_time = time.time()
        
        # try to send pending data
        while len(self.__pending_record_sets) > 0:
            #Connect to host system
            try:
                self._connection.begin()
            except Exception as err:                
                self.__handle_transport_error(self.ERROR_CONNECTION, err)
                return
            
            # send data
            try:
                self._connection.send(self.__pending_record_sets[0][1])
            except:
                self.__handle_transport_error(self.ERROR_TRANSMISSION)
                return

            # Wait for confirmation byte and end connection
            try:
                self._connection.end()
            except WrongConfirmationError as err:
                self.__handle_transport_error(self.ERROR_WRONG_CONFIRMATION, err)
                return
            except NoConfirmationError as err:
                self.__handle_transport_error(self.ERROR_NO_CONFIRMATION, err)
                return
            except Exception as err:
                self.__handle_transport_error(self.ERROR_TRANSMISSION, err)
                return

            #update statistics
            with self.__statistics_lock:
                self.__last_success_time = time.time()
                self.__records_sent += self.__pending_record_sets[0][0]
                self.__record_sets_sent += 1
            
            #update pending record set and re-save data
            with self.__data_lock:
                self.__pending_record_sets = self.__pending_record_sets[1:]
                self.__preserve_data()

            #data sent so clear item from prohibitor
            self.__task_unload_prohibitor.allow()
