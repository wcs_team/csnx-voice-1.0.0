#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import csv
"""Data receiver objects for LutConnection and OdrConnection objects."""

import threading
import voice

from vocollect_lut_odr import lut_odr_decoder, lut_odr_encoder

log_received_lut_records = True
try:
    log_received_lut_records = voice.get_voice_application_property('Log Received LUT Records') in ['true', None]
except:
    pass #ignore error and leave logging on
voice.log_message('Logging of received LUT records set to %s' % log_received_lut_records)

##############################################################################
# ODR Confirmation Byte class and exceptions
##############################################################################
class WrongConfirmationError(Exception):
    """A received confirmation byte did not match the expected value."""

class NoConfirmationError(Exception):
    """A confirmation byte was expected, but not received."""

class OdrConfirmationByte(object):
    """A data receiver that checks for confirmation bytes."""
    def __init__(self, confirmation_byte=None):
        if type(confirmation_byte) == int:
            self.confirmation_byte = chr(confirmation_byte)
        elif type(confirmation_byte) == bytes:
            self.confirmation_byte = ''
            for byte in confirmation_byte:
                self.confirmation_byte += chr(byte)
        else:
            self.confirmation_byte = confirmation_byte

    def receive(self, data):
        """Handle returned data from an ODR connection and raise an exception
        if it doesn't match expectations.
        """

        # We always expect one byte
        if not data:
            raise NoConfirmationError
        
        data = lut_odr_decoder(data)
                
        voice.log_message('Data Received: %s' % (data))

        if len(data) != 1:
            raise WrongConfirmationError
        # If a byte was specified, ensure that it matches
        if self.confirmation_byte is not None and self.confirmation_byte != data[0]:
            raise WrongConfirmationError

        return True

##############################################################################
# Lut class, main class for lut data
##############################################################################
class RecordOutOfRange(Exception):
    """A received confirmation byte did not match the expected value."""

class InvalidRecordObject(Exception):
    """A confirmation byte was expected, but not received."""

class Lut(object):
    """
    Constructor for Receiver. This LUT Receiver emulates the Legacy
    Task builder LUTs and their definitions.

        field_definitions - List of field names and types for the LUT
            Name can be string
            Type is N for numeric, S for String
            Example: [['Field1', 'S'], ['Field1', 'N']]
    """
    def __init__(self, *fields):
        self._raw_data =  b""    # raw data received
        self._records = []        # dictionary of parse raw data
        self.fields = {}
        self._fully_received = False
        # Store any exceptions that get thrown during execution in another thread context.
        # We'll want to rethrow the exception if anyone attempts to access our data.
        # This field will be set by out LutConnection parent
        self._exception = None
        # this event will allow us to gracefully wait for transmission to finish
        # This field will only be modified by our LutConnection parent
        self._transmit_done  = threading.Event()
        self._transmit_done.set()

        self.terminators = [lut_odr_encoder('\n\n'),
                            lut_odr_encoder('\r\n\n\n'),
                            lut_odr_encoder('\r\n\n'),
                            lut_odr_encoder('\r\r'),
                            lut_odr_encoder('\r\n\r\n')]
        
        for i, field in enumerate(fields):
            field.index = i
            self.fields[field.name] = field

    def receive(self, data):
        """Receive, verify, and process LUT data."""
        if type(data) == str:
            self._raw_data = self._raw_data + lut_odr_encoder(data)
        else:    
            self._raw_data = self._raw_data + data
        
        try:
            # term is the transmission terminator
            term = None
            # rec_term is the record terminator
            rec_term = None
            # need a printable record terminator
            print_rec_term = None
    
            if self._raw_data[len(self._raw_data)-len(self.terminators[0]):] == self.terminators[0]:
                if self._raw_data[len(self._raw_data)-len(self.terminators[1]):] == self.terminators[1]:
                    term = '<LF><LF>'
                    rec_term = '\r\n'
                    print_rec_term = '<CR><LF>'
                elif self._raw_data[len(self._raw_data)-len(self.terminators[2]):] == self.terminators[2]:
                    term = '<LF>'
                    rec_term = '\r\n'
                    print_rec_term = '<CR><LF>'
                else:
                    term = '<LF>'
                    rec_term = '\n'
                    print_rec_term = '<LF>'
            elif self._raw_data[len(self._raw_data)-len(self.terminators[3]):] == self.terminators[3]:
                term = '<CR>'
                rec_term = '\r'
                print_rec_term = '<CR>'
            elif self._raw_data[len(self._raw_data)-len(self.terminators[4]):] == self.terminators[4]:
                term = '<CR><LF>'
                rec_term = '\r\n'
                print_rec_term = '<CR><LF>'
                    
            if term:
                #LUT logging for data received
                self._parse_data(lut_odr_decoder(self._raw_data).split(rec_term), print_rec_term, term)
                self._fully_received = True
        except:
            # decode error on string is not a problem - 
            pass
    
        return self._fully_received

    def _parse_data(self, records, print_rec_term, term):
        """ parse the received data. For core implementation records
        are ended with either a LF, CR or CR\LF, and fields must be
        separated with commas
        """
        #clear previous data
        self.clear_records()
        # We want the log file to indicate what characters are used, so
        # replace the character with a printable version
        # rec_term = rec_term.replace("\r", "<CR>").replace("\n", "<LF>")
        #Parse Records - last record is transmission terminator - so ignore it
        record_count = 0
        for record in records[:-1]:
            if (record != ""):
                record_count += 1
                self._records.append(LutRecord(self.fields, record))
                if log_received_lut_records:
                    voice.log_message('Data Received: %s%s' %(record, print_rec_term))

        if log_received_lut_records:
            voice.log_message('Data Received: %s' % term)

        voice.log_message('Data Records Received: %s ' % record_count)
        self._raw_data = b""

    def clear_records(self):
        """ clears the current record set and prepares this object to receive more data """
        self._records = []
        self._raw_data = b""
        self._fully_received = False

    def append(self, records):
        """ append records to end of record set """
        if isinstance(records, Lut):
            self._records.extend(records._records)
        elif isinstance(records, LutRecord):
            self._records.append(records)
        elif isinstance(records, list):
            #check all records are LUT Records
            for record in records:
                if not isinstance(record, LutRecord):
                    raise InvalidRecordObject
            self._records.extend(records)
        else:
            raise InvalidRecordObject

    def raise_if_necessary(self):
        if self._exception:
            raise self._exception
            
    def data_ready(self):
        self.raise_if_necessary()
        return self._fully_received

    def wait_for_data(self):
        self.raise_if_necessary()
        self._transmit_done.wait()
        self.raise_if_necessary()

    def __len__(self):
        self.wait_for_data()
        return len(self._records)

    def __getitem__(self, record):
        self.wait_for_data()
        """ to support direct sequence like access """
        if isinstance(record, slice):
            newLut = Lut()
            newLut.fields = self.fields
            newLut._records = self._records.__getitem__(record)
            return newLut
        elif record >= 0 and record < len(self._records):
            return self._records[record]
        else:
            raise RecordOutOfRange

    def __delitem__(self, record):
        self.wait_for_data()
        """ to support direct sequence like access """
        self._records.__delitem__(record)

    def __iter__(self):
        self.wait_for_data()
        return LutIter(self)

    def __str__(self):
        self.wait_for_data()
        """ string representation of data """
        sep = ", "
        return "[" + sep.join(map(str,self._records)) + "]"
    
    def __getstate__(self):
        state = self.__dict__.copy()
        state['_transmit_done'] = None
        state['_exception'] = None
        return state

    def __setstate__(self, state):
        self.__dict__ = state
        self._transmit_done = threading.Event()
        self._transmit_done.set()


##############################################################################
# LutRecord class, main class for lut record
##############################################################################
class LutRecord(object):

    """ Lut record object returned from Lut iterator """
    def __init__(self, fields, data):
        self._fields = fields
        self.fields_accessed = []
        self.__raw_data = None
         
        #if fields was an array, change to dictionary
        if type(fields) != dict:
            self._fields = {}
            for i, field in enumerate(fields):
                field.index = i
                self._fields[field.name] = field

        #Change to not save to temp but to _data
        if isinstance(data, list):
            self._data = data
        else:
            self._data = None
            self.__raw_data = str(data)
             
    def __setitem__(self, field, value):
        """ dictionary access method for setting a field value """    
        #check if record was parsed
        if self.__raw_data:
            self._data = self.__split_fields()
            
        index = self._fields[field].index
        self._data[index] = self._fields[field].convert(value)
        
    def __getitem__(self, field):
        """ dictionary access method for getting a field value """
        #check if record was parsed
        if self.__raw_data:
            self._data = self.__split_fields()
            
        index = self._fields[field].index

        #check if first access, if so then strip whitespace        
        if field not in self.fields_accessed:
            self.fields_accessed.append(field)
            if(isinstance(self._data[index], str)):
                self._data[index] = self._data[index].strip().strip("\"'")
                
        return self._fields[field].convert(self._data[index])

    def __str__(self):
        """ string representation of data """
        #check if record was parsed
        if self.__raw_data:
            self._data = self.__split_fields()

        return str(self._data)

    def __split_fields(self):
        """ Splits a record based on commas in the record. commas found
        within fields that are enclosed in single or double quotes are not split
        """
        if '"' not in self.__raw_data:
            fields = self.__raw_data.split(',')
        else:
            try:
                fields = next(csv.reader([self.__raw_data]))
            except:
                fields = next(csv.reader([self.__raw_data + '"']))
        
        #Added to pad list to length of field definitions
        for i in range(len(fields), len(self._fields)): #@UnusedVariable
            fields.append('')
        self.__raw_data = None
        
        return fields

##############################################################################
# LutIter class, main class for iterating over a lut
##############################################################################
class LutIter(object):
    """ iterator object for iterating over LUT Data """
    def __init__(self, lut):
        self._lut = lut
        self._index = -1

    def __next__(self):
        """ method for iterator interface """
        self._index += 1

        #Move to next record
        if self._index >= len(self._lut):
            raise StopIteration

        return self._lut[self._index]


##############################################################################
# LutField, StringField, NumericField classes, main class for defining lut fields
##############################################################################
class LutField(object):
    """ Base field definition for a LUT Field """
    def __init__(self, field_name):
        self.name = field_name

    def convert(self, value):
        return value


class StringField(LutField):
    """ String field, same as a base LUT field different name for readablity """
    pass

class NumericField(LutField):
    """ Numeric Lut field, converts string value to numeric """

    def convert(self, value):
        """convert value to a numeric"""
        if type(value) in [int, float]:
            return value

        temp = None
        try:
            temp = int(value)
        except:
            try:
                temp = float(value)
            except:
                #currently this is what task builder does
                temp = 0

        return temp;
