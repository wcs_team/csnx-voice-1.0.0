#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
import threading
"""Implementation of transports for ODRs and LUTs."""

from vocollect_lut_odr import lut_odr_encoder, lut_odr_decoder

import socket
import voice

class NoDataReceivedError(Exception):
    ''' Exception when 0 bytes received from host '''
    
    
class TransientSocketConnection(object):
    """A recordset transport that creates a TCP socket connection for each 
    transmission.
    
        host - host to connect to
        port - port to connect to
        timeout - timeout period for connections
        output_formatter - used for formatting output data
        data_receiver - used for managing data received
    """
    
    _connection_id = 0
    _error_count = 0
    # TODO: Replace if-checking for output_formatter and data_receiver with a 
    # NullObject pattern.
    def __init__(self, host, port, timeout=10, output_formatter=None, data_receiver=None):
        self._host = host
        self._port = port
        self._timeout = timeout
        self._output_formatter = output_formatter #
        self._data_receiver = data_receiver # default to no op for ODRs
        self._socket = None
        self.__data_lock = threading.RLock()
        self._current_connection_id = 0

        
    def begin(self, fields = []):
        '''
            creates connection to host. 
            
            Param Fields - Optional parameter for logging purposes only on 
                           connection failures. so log information is easier
                           to debug
        '''
        with self.__data_lock:
            TransientSocketConnection._connection_id += 1
            self._current_connection_id = TransientSocketConnection._connection_id
        
        """Begin transmitting a record set."""
        try:
            self.__log_message('Connecting')
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.settimeout(self._timeout)
            self._socket.connect((self._host, self._port))
            self.__log_message('Connected')

        except Exception as err:
            #Log error occurred and re-raise error
            with self.__data_lock:
                TransientSocketConnection._error_count += 1
            self.__log_message('Connection Failed (%s)' % str(err))
            
            #Log the data that would have been sent
            if self._output_formatter:
                data = self._output_formatter.format_record(fields)
            else:
                data = str(fields)
            self.__log_message('Data not sent: %s' % data)            
            
            raise err

    def end(self):
        """Terminate and finish transmitting a record set."""
        try:
            # send terminating 'extra' record separator
            if self._output_formatter:
                data = self._output_formatter.terminate_recordset()
                if data:
                    self._socket.send(lut_odr_encoder(data))
                    voice.log_message('Data Sent: %s' % data.replace("\r", "<CR>").replace("\n", "<LF>"))

            # get confirmation byte if required
            if self._data_receiver is not None:
                complete = False
                while not complete:
                    data = self._socket.recv(2048)
                    if len(data) <= 0:
                        raise NoDataReceivedError('Exception occurred on the socket, received 0 bytes')
                    
                    complete = self._data_receiver.receive(data)

            self.__log_message('Connection Closed')
            
        except Exception as err:
            #Log an error occurred and re-raise error
            with self.__data_lock:
                TransientSocketConnection._error_count += 1
            self.__log_message('Connection Error (%s)' % str(err))

            #Log the data received so far
            try:
                self.__log_message('Data Received, not processed: %s' % 
                                   lut_odr_decoder(self._data_receiver._raw_data))
            except:
                pass
            
            raise err
        
        finally:
            self.close()


    def send(self, fields):
        """Send a single record."""
        if self._output_formatter:
            data = self._output_formatter.format_record(fields)
        else:
            data = str(fields)
        self._socket.send(data.encode('utf-8'))
        
        #LUT logging for Data Sent
        voice.log_message('Data Sent: %s' % data.replace("\r", "<CR>").replace("\n", "<LF>"))
        
    def close(self):
        """Ensure any resources are released."""
        if self._socket:
            self._socket.close()
            self._socket = None

    def __log_message(self, message):
        voice.log_message('Transient Socket: [Host = %s, port = %d, Timeout = %d, Connection Count = %d, Error Count = %d] %s' % 
                          (self._host, 
                           self._port, 
                           self._timeout,
                           self._current_connection_id,
                           TransientSocketConnection._error_count,
                           message))
        
    def __getstate__(self):
        ''' Removes properties that cannot be pickled '''
        state = self.__dict__.copy()
        state['_TransientSocketConnection__data_lock'] = None
        return state

    def __setstate__(self, state):
        ''' method called when restoring state from pickling '''
        self.__dict__ = state
        self.__data_lock = threading.RLock()
        
class TransientSocketTransport(object):
    """A recordset transport that creates a TCP socket connection for each 
    transmission.
    
       Host - IP Address or DNS name to connect to
       Port - Port number to connect to
       Timeout - Connection Timeout in seconds
    """
    def __init__(self, host, port, timeout=10):
        self._host = host
        self._port = port
        self._timeout = timeout

        self._socket = None

    def create_connection(self, output_formatter=None, data_receiver=None):
        """Returns a new connection object for sending and receiving data
        
            output_formatter - used for formatting output data
            data_receiver - used for managing data received
        """
        return TransientSocketConnection(self._host, self._port, self._timeout, 
                                         output_formatter, data_receiver)
