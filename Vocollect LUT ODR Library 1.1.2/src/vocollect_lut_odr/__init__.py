import voice

# Get the application property LUTODRCodec
# If not defined - set the codec to "utf-8"

BEEP_PITCH = 400
BEEP_DURATION = 0.2

codec = 'utf-8'
try:
    codec = voice.get_voice_application_property('LUTODRCodec')

    if codec is None:
        codec = 'utf-8'
except:
    pass

def lut_odr_encoder(data):
    try:
        data = data.encode(codec)
    except:
        data = str(data)
    return data

def lut_odr_decoder(data):

    if type(data) == int:
        data = chr(data)
    elif type(data) != str:
        #first try to decode using the codec
        try:
            data = data.decode(codec)
        except:
            temp = ''
            for byte in data:
                temp += chr(byte)
            data = temp

    return data