#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
from vocollect_core.utilities.localization import itext
from voice import Dialog
import voice.audio
from vocollect_lut_odr import BEEP_DURATION, BEEP_PITCH
import os
import time

class PendingLUTs(Dialog):
    ''' Dialog class to wait for pending LUT's. This is a long running function, 
    so it is handled through a dialog instead of python so that
    a user may still use button presses while waiting LUT data
    '''    

    def __init__(self, connection, interval = 2, audio = (BEEP_PITCH, BEEP_DURATION)):
        Dialog.__init__(self, 'PendingLUTs')
        self.connection = connection
        self.count = 0
        self.interval = interval
        self.audio = audio
        self.nodes['CheckPending'].help_prompt = itext('generic.pending.luts')
        
    def has_no_pending_luts(self):
        ''' checking the connection to see if all the LUT processing is over'''
        if not self.connection.lut_data._exception:
            return self.connection.data_ready()
        else:
            return True

    def is_timedout(self):
        ''' check if waiting for more than interval, if so then return true
        to move to next node to either beep or prompt user
        '''
        return (self.interval > 0) and (self.nodes['CheckPending'].seconds_since_entry >= self.interval)
    
    def set_prompt_or_beep(self):
        '''beep every interval
        '''
        if isinstance(self.audio, str):
            wav_file = voice.get_persistent_store_path() + '\\' + self.audio
            if os.path.isfile(wav_file):
                voice.audio.play(wav_file)
            else:
                voice.log_message("Pending Lut Beep error - file does not exist: %s" %wav_file)
                voice.audio.beep(BEEP_PITCH, BEEP_DURATION)

        elif isinstance(self.audio, tuple):
            voice.audio.beep(*self.audio)
        else:                 
            voice.log_message("Pending Lut Beep Invalid parameter datatype - default beep invoked")
            voice.audio.beep(BEEP_PITCH, BEEP_DURATION)

            
def wait_for_lut_response(connection, interval = 2, audio = (BEEP_PITCH, BEEP_DURATION)):
    ''' call to wait for LUT response
    
        connection - lut connection object
        interval - time in seconds between beeps or wav file (default is 2 seconds) an interval value of 0 turns off beeping
        audio - either a string of a wav file to play (must already be extracted from VAD)
                or a tuple of pitch and duration.
                    Pitch defaults to 400 -- must be between 50 and 5000
                    duration defaults to 0.2 -- must be between 0.1 and 5.0
    '''
    #For performance reasons wait until first beep is needed  
    #before launching dialog
    start = time.clock()
    while time.clock() - start < (interval - 0.2) and not connection.data_ready():
        time.sleep(0.005)
        
    if not connection.data_ready(): 
        pendingLUTDialog = PendingLUTs(connection, interval, audio)
        pendingLUTDialog.run()

#pass through methods to dialog class        
def is_timedout():
    return voice.current_dialog().is_timedout()

def set_prompt_or_beep():
    voice.current_dialog().set_prompt_or_beep()
    
def has_no_pending_luts():
    return voice.current_dialog().has_no_pending_luts()
    
    
        
