#
# Copyright (c) 2010 Vocollect, Inc.
# Pittsburgh, PA 15235
# All rights reserved.
#
# This source code contains confidential information that is owned by
# Vocollect, Inc. and may not be copied, disclosed or otherwise used without
# the express written consent of Vocollect, Inc.
#
from voice import Dialog
from vocollect_lut_odr.connections import OdrConnection
import voice
from vocollect_core.utilities.localization import itext
from vocollect_lut_odr import BEEP_PITCH, BEEP_DURATION
import os
import time

class PendingODRs(Dialog):
    ''' Dialog class to wait for pending ODR's. Since this is a long
    running function it is handled through a dialog instead of python so that
    a user may still use button presses while waiting to send ODR's 
    '''    
    def __init__(self, archive_name, interval = 2, audio = (BEEP_PITCH, BEEP_DURATION)):
        Dialog.__init__(self, 'PendingODRs')
        self.archive_name = archive_name
        self.count = 0
        self.interval = interval
        self.audio = audio
        self.nodes['CheckPending'].help_prompt = itext('generic.pending.odrs')
        
    def has_no_pending_odrs(self):
        ''' check if archive exists, and if so if it has pending ODRs'''
        conn = OdrConnection.get_archive(self.archive_name)
        return not (conn and conn.has_pending_records())
    
    def is_timedout(self):
        ''' check if waiting for more than 2 seconds, if so then return true
        to move to next node to either beep or prompt user
        '''
        return (self.interval > 0) and (self.nodes['CheckPending'].seconds_since_entry >= self.interval)
    
    def set_prompt_or_beep(self):
        '''beep every 2 seconds, and every 20 second prompt that there 
        is pending ODRs
        '''
        self.current_node.prompt = ''
        if self.count < 10:

            if isinstance(self.audio, str):
                wav_file = voice.get_persistent_store_path() + '\\' + self.audio
                if os.path.isfile(wav_file):
                    voice.audio.play(wav_file)
                else:
                    voice.log_message("Pending ODR Beep error - file does not exist: %s" %wav_file)
                    voice.audio.beep(BEEP_PITCH, BEEP_DURATION)
    
            elif isinstance(self.audio, tuple):
                voice.audio.beep(*self.audio)
            else:                 
                voice.log_message("Pending ODR Beep Invalid parameter datatype - default beep invoked")
                voice.audio.beep(BEEP_PITCH, BEEP_DURATION)

            self.count += 1
        else:
            self.current_node.prompt = itext('generic.pending.odrs')
            self.count = 0
            
        conn = OdrConnection.get_archive(self.archive_name)
        if conn:
            conn.retry_transmit()

def wait_for_pending_odrs(archive_name, interval = 2, audio = (BEEP_PITCH, BEEP_DURATION)):
    ''' call to wait for pending ODRs on a specified archive
        archive_name - name of the odr archive
        interval - time in seconds between beeps or wav file (default is 2 seconds) an interval value of 0 turns off beeping
        audio - either a string of a wav file to play (must already be extracted from VAD)
                or a tuple of pitch and duration.
                    Pitch defaults to 400 -- must be between 50 and 500
                    duration defaults to 0.2 -- must be between 0.1 and 5.0
    '''
    conn = OdrConnection.get_archive(archive_name)
    if not conn:
        return
    
    #For performance reasons check if pending ODR's exist 
    #before launching dialog
    start = time.clock()
    while time.clock() - start < (interval - 0.2) and conn.has_pending_records():
        time.sleep(0.005)

    #If still pending records at this point then launch dialog to wait.
    if conn.has_pending_records():
        d = PendingODRs(archive_name, interval, audio)
        d.run()

#pass through methods to dialog class        
def is_timedout():
    return voice.current_dialog().is_timedout()

def set_prompt_or_beep():
    voice.current_dialog().set_prompt_or_beep()
    
def has_no_pending_odrs():
    return voice.current_dialog().has_no_pending_odrs()
    
